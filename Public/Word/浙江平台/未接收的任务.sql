select b.fmedianame '媒体名称',b.fid '媒体ID',a.fissue_date '任务日期',(case when a.fstatus = 0 then '待接收' when a.fstatus = 10 then '被退回待接收' when a.fstatus = 1 then '已接收' when a.fstatus = 4 then '关闭' end) '状态', (case when left(a.flog,instr(a.flog,"；") -1) <> '' then left(a.flog,instr(a.flog,"；") -1) else a.flog end) '最新日志', flog '完整日志'
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
where a.fissue_date between '2019-11-1' and '2019-11-30'
order by a.fstatus asc,b.media_region_id asc,b.fid ASC,a.fissue_date asc;

#所有
select count(*)
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
where a.fissue_date between '2019-11-1' and '2019-11-30';

#待接收
select count(*)
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
where a.fissue_date between '2019-11-1' and '2019-11-30' and a.fstatus = 0;

#接收
select count(*)
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
where a.fissue_date between '2019-11-1' and '2019-11-30' and a.fstatus = 1;

#退回
select count(*)
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
where a.fissue_date between '2019-11-1' and '2019-11-30' and a.fstatus = 10;

#异常
select count(*)
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
where a.fissue_date between '2019-11-1' and '2019-11-30' and a.fstatus = 4;

#已提交的
select count(*)
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
where a.fissue_date between '2019-11-1' and '2019-11-30' and a.fstatus = 1 and a.fid not in (
select a.fbiz_main_id from ttask a,ttask_flow b where a.fid = b.ftask_id and b.fstatus = 0 and fflow_code = 'mediaissue_ggfl' group by a.fid
);


select e.fmedianame,e.fid,a.fissuedate,c.fadname,a.fstarttime,a.fendtime,f.ffullname,l.fcreate_time
from ttvissue_2019 a
inner join ttvsample b on a.fsampleid = b.fid and b.fstate = 1
inner join tad c on b.fadid = c.fadid and c.fstate = 1
inner join tadowner d on c.fadowner = d.fid
inner join tmedia e on a.fmediaid = e.fid
inner join tadclass f on c.fadclasscode = f.fcode
inner join ttask_flow_log l on a.fid = l.fmain_id and l.fmedia_type = '01' and ftype = 3
inner join (select e.fid,a.fissue_date from tmediaissue a
	inner join ttask b on a.fid = b.fbiz_main_id
	inner join (select ftask_id from ttask_flow where fstatus = 0 and fflow_code<>'mediaissue_ggfl') d on b.fid = d.ftask_id
	inner join tmedia e on a.fmedia_id = e.fid
	inner join tregion f on e.media_region_id = f.fid) mm on a.fissuedate = mm.fissue_date and a.fmediaid = mm.fid
where a.fself = 1 and a.fidentify > 0 and a.fstate = -1 and l.fcreate_time between '2020-01-02' and '2020-01-03'
order by e.fmediaclassid asc,e.fid asc,a.fissuedate asc,a.fstarttime asc;


select e.fmedianame,e.fid,a.fissuedate,c.fadname,a.fstarttime,a.fendtime,f.ffullname,l.fcreate_time
from tbcissue_2019 a
inner join tbcsample b on a.fsampleid = b.fid and b.fstate = 1
inner join tad c on b.fadid = c.fadid and c.fstate = 1
inner join tadowner d on c.fadowner = d.fid
inner join tmedia e on a.fmediaid = e.fid
inner join tadclass f on c.fadclasscode = f.fcode
inner join ttask_flow_log l on a.fid = l.fmain_id and l.fmedia_type = '02' and ftype = 3
inner join (select e.fid,a.fissue_date from tmediaissue a
	inner join ttask b on a.fid = b.fbiz_main_id
	inner join (select ftask_id from ttask_flow where fstatus = 0 and fflow_code<>'mediaissue_ggfl') d on b.fid = d.ftask_id
	inner join tmedia e on a.fmedia_id = e.fid
	inner join tregion f on e.media_region_id = f.fid) mm on a.fissuedate = mm.fissue_date and a.fmediaid = mm.fid
where a.fself = 1 and a.fidentify > 0 and a.fstate = -1 and l.fcreate_time between '2020-01-02' and '2020-01-03'
order by e.fmediaclassid asc,e.fid asc,a.fissuedate asc,a.fstarttime asc;
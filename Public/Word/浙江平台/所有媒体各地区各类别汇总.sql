SELECT k.fname,i.ffullname adclassname, COUNT(*) ztiaoci, SUM(CASE WHEN a.fillegaltypecode = 30 THEN 1 ELSE 0 END) wftiaoci 
FROM 
(
	(
	select a.fmediaid,a.fissuedate,a.fstarttime,a.fendtime,b.fillegaltypecode,b.fadid from ttvissue_2019 a
	INNER JOIN ttvsample b ON a.fsampleid = b.fid
	where  a.fstate = 1 AND b.fstate = 1
	)
	union all
	(
	select a.fmediaid,a.fissuedate,a.fstarttime,a.fendtime,b.fillegaltypecode,b.fadid from tbcissue_2019 a
	INNER JOIN tbcsample b ON a.fsampleid = b.fid
	where  a.fstate = 1 AND b.fstate = 1
	)
) a
INNER JOIN tmedia f ON a.fmediaid = f.fid AND f.fid = f.main_media_id AND f.fstate = 1
INNER JOIN tad h ON a.fadid = h.fadid and h.fstate in(1,2,9)
INNER JOIN tadclass i ON i.fcode = LEFT(h.fadclasscode,2)
INNER JOIN tmediaissue me ON a.fissuedate = me.fissue_date AND a.fmediaid = me.fmedia_id AND me.fstatus = 1
inner join tregion k on concat(left(f.media_region_id,4),'00') = k.fid
WHERE 1=1 AND YEAR(a.fissuedate)=2019 AND MONTH(a.fissuedate)=11
GROUP BY i.fcode,left(f.media_region_id,4)
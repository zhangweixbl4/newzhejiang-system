select f.ffullname,e.fmedianame,e.fid,a.fissue_date from tmediaissue a
inner join ttask b on a.fid = b.fbiz_main_id
inner join (select ftask_id from ttask_flow where fstatus = 0 and fflow_code<>'mediaissue_ggfl') d on b.fid = d.ftask_id
inner join tmedia e on a.fmedia_id = e.fid
inner join tregion f on e.media_region_id = f.fid
order by f.fid asc,e.fid asc,a.fissue_date asc
select h.fid,h.fname,h.ffullname,count(*) zcount,sum(case when a.fillegaltypecode = 30 then 1 else 0 end) wfcount from 
(
(select a.*,b.fillegaltypecode,b.fadid from ttvissue_2019 a,ttvsample b where a.fsampleid = b.fid and b.fstate = 1 and a.fstate = 1 and  a.fissuedate between '2019-12-01' and '2019-12-25')
union ALL
(select a.*,b.fillegaltypecode,b.fadid from tbcissue_2019 a,tbcsample b where a.fsampleid = b.fid and b.fstate = 1 and a.fstate = 1 and a.fissuedate between '2019-12-01' and '2019-12-25')
) a
inner join tad c on a.fadid = c.fadid and c.fstate in(1,2,9)
inner join tadowner d on c.fadowner = d.fid
inner join tmedia e on a.fmediaid = e.fid
inner join tadclass f on c.fadclasscode = f.fcode
inner join tmediaissue g on a.fmediaid = g.fmedia_id and a.fissuedate = g.fissue_date and g.fstatus = 1
inner join tregion h on h.fid = e.media_region_id
group by h.fid
order by h.fid asc


select b.fmedianame '媒体名称',b.fid '媒体ID',a.fissue_date '发布时间',(case when a.fstatus = 0 then '待接收' when a.fstatus = 10 then '被退回待接收' when a.fstatus = 1 then '已接收' when a.fstatus = 4 then '异常' end) '状态', left(a.flog,instr(a.flog,"；") -1) '最新日志', flog '完整日志',mcount
from tmediaissue a
inner join tmedia b on a.fmedia_id = b.fid 
left join (
	(
	select e.fid,a.fissuedate,count(*) mcount
from tbcissue_2019 a
inner join tbcsample b on a.fsampleid = b.fid and b.fstate = 1
inner join tad c on b.fadid = c.fadid and c.fstate = 1
inner join tadowner d on c.fadowner = d.fid
inner join tmedia e on a.fmediaid = e.fid
inner join tadclass f on c.fadclasscode = f.fcode
inner join ttask_flow_log l on a.fid = l.fmain_id and l.fmedia_type = '01' and ftype = 2
inner join (select e.fid,a.fissue_date from tmediaissue a
	inner join ttask b on a.fid = b.fbiz_main_id
	inner join (select ftask_id from ttask_flow where fstatus = 0 and fflow_code<>'mediaissue_ggfl') d on b.fid = d.ftask_id
	inner join tmedia e on a.fmedia_id = e.fid
	inner join tregion f on e.media_region_id = f.fid) mm on a.fissuedate = mm.fissue_date and a.fmediaid = mm.fid
where a.fself = 1 and a.fidentify >0 and a.fstate = 1 
group by e.fid,a.fissuedate
	)
	union all 
	(
	select e.fid,a.fissuedate,count(*) mcount
from ttvissue_2019 a
inner join ttvsample b on a.fsampleid = b.fid and b.fstate = 1
inner join tad c on b.fadid = c.fadid and c.fstate = 1
inner join tadowner d on c.fadowner = d.fid
inner join tmedia e on a.fmediaid = e.fid
inner join tadclass f on c.fadclasscode = f.fcode
inner join ttask_flow_log l on a.fid = l.fmain_id and l.fmedia_type = '01' and ftype = 2
inner join (select e.fid,a.fissue_date from tmediaissue a
	inner join ttask b on a.fid = b.fbiz_main_id
	inner join (select ftask_id from ttask_flow where fstatus = 0 and fflow_code<>'mediaissue_ggfl') d on b.fid = d.ftask_id
	inner join tmedia e on a.fmedia_id = e.fid
	inner join tregion f on e.media_region_id = f.fid) mm on a.fissuedate = mm.fissue_date and a.fmediaid = mm.fid
where a.fself = 1 and a.fidentify >0 and a.fstate = 1 
group by e.fid,a.fissuedate
	)
) mm on b.fid = mm.fid and mm.fissuedate = a.fissue_date
where a.fissue_date between '2019-11-1' and '2019-11-30'
order by a.fstatus asc,b.media_region_id asc,b.fid ASC,a.fissue_date asc;
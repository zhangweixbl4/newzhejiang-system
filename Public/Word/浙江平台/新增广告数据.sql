select g.ffullname,g.fid regionid,e.fmedianame,e.fid mediaid,c.fadname,a.fstarttime,a.fendtime,f.ffullname fadclassname,a.fcreator,a.fautotime
from ttvissue_2019 a
inner join ttvsample b on a.fsampleid = b.fid and b.fstate = 1
inner join tad c on b.fadid = c.fadid and c.fstate in(1,2,9)
inner join tadowner d on c.fadowner = d.fid
inner join tmedia e on a.fmediaid = e.fid
inner join tadclass f on c.fadclasscode = f.fcode
inner join tregion g on e.media_region_id = g.fid
where a.fstate =1 and a.fissuedate = '2019-11-01' and a.fidentify = 0 
order by g.fid asc
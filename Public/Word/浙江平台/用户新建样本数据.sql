select tm.fmedianame,tm.fid,c.fadname,g.ffullname,a.fstarttime,a.fendtime,a.flength,d.fname ownername,(case when b.fillegaltypecode = 30 then "违法" else "不违法" end) wf,b.fillegalcontent,b.fexpressioncodes,b.fexpressions,b.fcreator from ttvissue_2019 a
inner join ttvsample b on a.fsampleid = b.fid and b.fstate = 1
inner join tmedia tm on a.fmediaid = tm.fid
inner join tad c on b.fadid = c.fadid
inner join tadclass g on c.fadclasscode = g.fcode
inner join tadowner d on c.fadowner = d.fid
inner join (
select a.fmedia_id,a.fissue_date from tmediaissue a
inner join ttask b on a.fid = b.fbiz_main_id
inner join ttask_flow c on b.fid = c.ftask_id
where a.fissue_date = '2019-11-1'  and b.fstatus = 1 and c.fflow_code = 'mediaissue_ggjc' and c.fstatus = 2
) e on a.fissuedate = e.fissue_date and a.fmediaid = e.fmedia_id
where b.fidentify = 0 
order by a.fmediaid desc,a.fstarttime asc
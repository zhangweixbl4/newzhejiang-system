select d.ffullname,d.fcode,count(*) zcount,sum(case when b.fillegaltypecode = 30 then 1 else 0 end) illcount
from ttvissue_2019 a
inner join ttvsample b on a.fsampleid = b.fid
inner join tad c on b.fadid = c.fadid
inner join tadclass d on left(c.fadclasscode,2) = d.fcode
where a.fmediaid in (11000010002385,11000010002386,11000010002382,11000010002387,11000010002359,11000010004170)
and a.fissuedate between '2019-11-1' and '2019-11-30'
and a.fsendstate = 20
and c.fadclasscode=1213
group by d.fcode
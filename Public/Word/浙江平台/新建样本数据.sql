SELECT
	tm.fmedianame,
	tm.fid,
	c.fadname,
	g.ffullname,
	a.fstarttime,
	a.fendtime,
	a.flength,
	d.fname ownername,
	(
		CASE
		WHEN b.fillegaltypecode = 30 THEN
			"违法"
		ELSE
			"不违法"
		END
	) wf,
	b.fillegalcontent,
	b.fexpressioncodes,
	b.fexpressions,
	b.fcreator
FROM
	ttvissue_2019 a
INNER JOIN ttvsample b ON a.fsampleid = b.fid
AND b.fstate = 1
INNER JOIN tmedia tm ON a.fmediaid = tm.fid
INNER JOIN tad c ON b.fadid = c.fadid
INNER JOIN tadclass g ON c.fadclasscode = g.fcode
INNER JOIN tadowner d ON c.fadowner = d.fid
INNER JOIN (
	SELECT
		a.fmedia_id,
		a.fissue_date
	FROM
		tmediaissue a
	INNER JOIN ttask b ON a.fid = b.fbiz_main_id
	INNER JOIN ttask_flow c ON b.fid = c.ftask_id
	WHERE
		a.fissue_date = '2019-11-1'
	AND b.fstatus = 1
	AND c.fflow_code = 'mediaissue_ggjc'
	AND c.fstatus = 2
) e ON a.fissuedate = e.fissue_date
AND a.fmediaid = e.fmedia_id
WHERE
	b.fidentify = 0
ORDER BY
	a.fmediaid DESC,
	a.fstarttime ASC
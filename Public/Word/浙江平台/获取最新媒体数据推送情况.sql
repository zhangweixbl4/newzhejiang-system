select c.ffullname,b.fmedianame,b.fid,d.fissue_date,(case when d.fstatus = 1 then '接收成功' when d.fstatus = 2 then '接收失败' when d.fstatus = 0 then '未推送' end) '状态',(case when e.fmedialabelid is null then '否' else '是' end) iscb,d.fupdate_time,(case when instr(d.flog,'；')>0 then left(d.flog,instr(d.flog,"；")) else '' end) '日志'
from tregulatormedia a
inner join tmedia b on a.fmediaid = b.fid
inner join tregion c on b.media_region_id = c.fid
left join (select * from tmediaissue_pushtask where fissue_date between '2019-11-01' and '2019-11-02') d on b.fid = d.fmedia_id
left join (select * from tmedialabel xx where xx.flabelid in(446,449)) e on b.fid = e.fmediaid
where a.fregulatorcode = '20330000' and a.fcustomer = 330000 and a.fstate = 1 and left(b.fmediaclassid,2) in('01','02') and d.fupdate_time between '2019-11-26' and '2019-11-27' 
order by c.fid asc,d.fstatus asc
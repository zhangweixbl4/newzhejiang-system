select e.fmedianame,a.fissuedate,c.fadname,a.fstarttime,a.fendtime,f.ffullname from ttvissue_2019 a
inner join ttvsample b on a.fsampleid = b.fid and b.fstate = 1
inner join tad c on b.fadid = c.fadid and c.fstate = 1
inner join tadowner d on c.fadowner = d.fid
inner join tmedia e on a.fmediaid = e.fid
inner join tadclass f on c.fadclasscode = f.fcode
where a.fself = 1 and a.fidentify = 0
order by e.fmediaclassid asc,e.fid asc,a.fissuedate asc,a.fstarttime asc
<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: table_store.proto

namespace Aliyun\OTS\ProtoBuffer\Protocol;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 *&#47;&#42; #############################################  UpdateTable  ############################################# *&#47;
 *
 * Generated from protobuf message <code>aliyun.OTS.ProtoBuffer.Protocol.UpdateTableRequest</code>
 */
class UpdateTableRequest extends \Aliyun\OTS\ProtoBuffer\Protocol\Message
{
    /**
     * Generated from protobuf field <code>required string table_name = 1;</code>
     */
    private $table_name = '';
    private $has_table_name = false;
    /**
     * Generated from protobuf field <code>optional .aliyun.OTS.ProtoBuffer.Protocol.ReservedThroughput reserved_throughput = 2;</code>
     */
    private $reserved_throughput = null;
    private $has_reserved_throughput = false;
    /**
     * Generated from protobuf field <code>optional .aliyun.OTS.ProtoBuffer.Protocol.TableOptions table_options = 3;</code>
     */
    private $table_options = null;
    private $has_table_options = false;

    public function __construct() {
        \GPBMetadata\TableStore::initOnce();
        parent::__construct();
    }

    /**
     * Generated from protobuf field <code>required string table_name = 1;</code>
     * @return string
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * Generated from protobuf field <code>required string table_name = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setTableName($var)
    {
        GPBUtil::checkString($var, True);
        $this->table_name = $var;
        $this->has_table_name = true;

        return $this;
    }

    public function hasTableName()
    {
        return $this->has_table_name;
    }

    /**
     * Generated from protobuf field <code>optional .aliyun.OTS.ProtoBuffer.Protocol.ReservedThroughput reserved_throughput = 2;</code>
     * @return \Aliyun\OTS\ProtoBuffer\Protocol\ReservedThroughput
     */
    public function getReservedThroughput()
    {
        return $this->reserved_throughput;
    }

    /**
     * Generated from protobuf field <code>optional .aliyun.OTS.ProtoBuffer.Protocol.ReservedThroughput reserved_throughput = 2;</code>
     * @param \Aliyun\OTS\ProtoBuffer\Protocol\ReservedThroughput $var
     * @return $this
     */
    public function setReservedThroughput($var)
    {
        GPBUtil::checkMessage($var, \Aliyun\OTS\ProtoBuffer\Protocol\ReservedThroughput::class);
        $this->reserved_throughput = $var;
        $this->has_reserved_throughput = true;

        return $this;
    }

    public function hasReservedThroughput()
    {
        return $this->has_reserved_throughput;
    }

    /**
     * Generated from protobuf field <code>optional .aliyun.OTS.ProtoBuffer.Protocol.TableOptions table_options = 3;</code>
     * @return \Aliyun\OTS\ProtoBuffer\Protocol\TableOptions
     */
    public function getTableOptions()
    {
        return $this->table_options;
    }

    /**
     * Generated from protobuf field <code>optional .aliyun.OTS.ProtoBuffer.Protocol.TableOptions table_options = 3;</code>
     * @param \Aliyun\OTS\ProtoBuffer\Protocol\TableOptions $var
     * @return $this
     */
    public function setTableOptions($var)
    {
        GPBUtil::checkMessage($var, \Aliyun\OTS\ProtoBuffer\Protocol\TableOptions::class);
        $this->table_options = $var;
        $this->has_table_options = true;

        return $this;
    }

    public function hasTableOptions()
    {
        return $this->has_table_options;
    }

}


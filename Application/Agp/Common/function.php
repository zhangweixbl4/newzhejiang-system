<?php
    //获取参数
    function getconfig($cgname){

        if($cgname == 'ALL'){
            $cg = config_array($fcustomer);
        }else{
            $cg = config_array($fcustomer)[$cgname];

            if($cgname == 'web_loginwz' || $cgname == 'web_logowz' || $cgname == 'web_name'){
                $cg = htmlspecialchars_decode($cg);
            }
        }

        return $cg;
    }

    //获取配置
    function config_array(){
        header("Content-type: text/html; charset=utf-8");
        $FORCE_NUM_CONFIG = C('FORCE_NUM_CONFIG');
        $config_url = [];//定义客户域名数组
     
        $map['_string'] = 'cr_num = "'.$FORCE_NUM_CONFIG.'" OR cr_url = "'.$FORCE_NUM_CONFIG.'"';
        //查询客户表
        $config_url = M('customer')->field("cr_id,cr_url,cr_num")
            ->where([
                'cr_status'=>1,
                'cr_id'=>["NEQ",1]
            ])
            ->where($map)
            ->order('cr_id')
            ->find();
      
        if(!empty($config_url)){
            //给当前域名下的用户配置参数
            $config_fields = M('customer_systemset')->cache(true,120)
                ->field("
                cst_name,
                cst_val,
                cst_type
                ")
                ->where(['cst_crid'=>$config_url['cr_id']])
                ->select();
            //查询默认配置
            $default_config_fields = M('customer_systemset')->cache(true,600)
                ->field("
                cst_name,
                cst_val,
                cst_type
                ")
                ->where(['cst_crid'=>1])
                ->select();

            $config_fields[] = ['cst_name' => 'system_num','cst_val' => $config_url['cr_num'], 'cst_type' => 'int'];//客户id

            //循环配置参数
            $config_data = [];//清空数组
            $config_fields_array = [];//参数名称数组
            foreach ($config_fields as $config_key=>$config_value){
                $val = $config_value['cst_val'];
                if($config_value['cst_type'] == 'int'){
                    $val = intval($val);
                }
                $config_fields_array[] = $config_value['cst_name'];
                C('GET_DMOAIN_CONFIG.'.$config_value['cst_name'],$val);
            }
            //获取默认配置
            foreach ($default_config_fields as $default_config_key=>$default_config_value){
                if(!in_array($default_config_value['cst_name'],$config_fields_array)){
                    $default_val = $default_config_value['cst_val'];
                    if($default_config_value['cst_type'] == 'int'){
                        $default_val = intval($default_val);
                    }
                    C('GET_DMOAIN_CONFIG.'.$default_config_value['cst_name'],$default_val);
                }
            }
        }
        return C('GET_DMOAIN_CONFIG');

    }

/**
 * 获取发布表名
 * by zw
 */
function gettable($tb,$tm) { 
    if($tb == '01'){
        return 'ttvissue_' . date('Y',$tm);
    }elseif($tb == '02'){
        return 'tbcissue_' . date('Y',$tm);
    }elseif($tb == '03' || $tb == '04'){
        return 'tpaperissue_' . date('Y',$tm);
    }
}

/**
 * 获取最近七天所有日期，$n获取几天，$d截止时间几天前，默认获取7天，截止日期为昨天
 */
function get_weeks($time = '', $format='Y-m-d',$n = 7,$d = 1){
  $time = $time != '' ? $time : time();
  //组合数据
  $date = [];
  for ($i=0; $i<=$n-1; $i++){
    $date[$i] = date($format ,strtotime( '+' . $i-($n-1+$d) .' days', $time));
  }
  return $date;
}

function get_gjj_label_media(){
    $gjj_label_media_map = [];
    $gjj_label_media_map['tmedialabel.flabelid'] = ['IN',[243,244,245,248]];
    $gjj_label_media_map['tregion.flevel'] = ['IN',[1,2,3]];
    $gjj_label_media = M('tmedialabel')
        ->join("tmedia on tmedia.fid = tmedialabel.fmediaid and tmedia.fstate = 1")
        ->join("tregion on tregion.fid = tmedia.media_region_id")
        ->where($gjj_label_media_map)
        ->group('fmediaid')
        ->getField('fmediaid',true);
    $gjj_label_media = array_unique($gjj_label_media);

    return $gjj_label_media;
}

function get_owner_media_ids($level = -1,$media_type = '',$out_media_type = ''){
    $system_num = getconfig('system_num');
    $map = [];

    if(!empty($media_type)){
        $map['tmedia.fmediaclassid'] = ['like',"$media_type%"];
    }

    if(!empty($out_media_type)){
        $map['tmedia.fmediaclassid'] = ['notlike',"$out_media_type%"];
    }

    if($level != -1){
        if(is_array($level)){
            $map['tregion.flevel'] = ['IN',$level];
        }else{
            $map['tregion.flevel'] = $level;
        }
    }
    $media_ids = M('tmedia_temp')
        ->join("tmedia on tmedia.fid = tmedia_temp.fmediaid and tmedia.fstate = 1")
        ->join("tregion on tregion.fid = tmedia.media_region_id")
        ->where([
            'tmedia_temp.ftype' => 1,
            'tmedia_temp.fcustomer' => $system_num,
            'tmedia_temp.fuserid' => session('regulatorpersonInfo.fid')
        ])
        ->where($map)
        ->getField('tmedia_temp.fmediaid',true);

    return $media_ids;
}

function getimages($str)
{
    $res = preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i',$str,$match);
    if($res){
        return $match[1];
    }else{
        return false;
    }
}

function arr_page($arr,$p = 1,$count = 10){
    $start = ($p-1)*$count;//设置开始地点
    //循环获取
    for ($i=$start;$i<$start+$count;$i++){

        if (!empty($arr[$i])){

            $new_arr[$i] = $arr[$i];
        }
    }
    //返回
    return $new_arr;
}

    function table_exist($table_name){
        $table_exist = M('')->query('show tables like "'.$table_name.'"');
        if(empty($table_exist)){
            return false;
        }else{
            return true;
        };
    }

    //判断某个链接是否是图片链接by yjn
    function is_img_link($url){
        //图片的链接地址
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); //是否跟着爬取重定向的页面
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //将curl_exec()获取的值以文本流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_HEADER,  1); // 启用时会将头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); //设置超时时间
        curl_setopt($ch, CURLOPT_NOBODY,true);//设置只获取头信息
        curl_setopt($ch, CURLOPT_URL, $url);  //设置URL
        $content = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  //curl的httpcode
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE); //获取头大小
        curl_close($ch);
        $headers = substr($content, 0, $headerSize); //根据头大小截取头信息
        $head_data=preg_split('/\n/',$headers);  //逐行放入数组中
        $head_data = array_filter($head_data);  //过滤空数组
        $headers_arr = [];
        foreach($head_data as $val){  //按:分割开
            list($k,$v) = explode(":",$val); //:前面的作为key，后面的作为value，放入数组中
            $headers_arr[$k] = $v;
        }
        $img_type = explode("/",trim($headers_arr['Content-Type']));  //然后将获取到的Content-Type中的值用/分隔开
        if ($httpcode == 200 && strcasecmp($img_type[0],'image') == 0) {//如果httpcode为200，并且Content-type前面的部分为image，则说明该链接可以访问成功，并且是一个图片类型的
            return true;
        } else {
            return false;
        }


    }

    //强制转字符串，报告专用
    function to_string($array){

        if(is_array($array) && !empty($array)){
            foreach ($array as $key => $val){
                foreach ($val as $val_k => $val_v){
                    $array[$key][$val_k] = strval($val_v);
                }
            }
            return $array;
        }else{
            return [];
        }
    }

    //生成报告所需数组
    function create_report_array($type = '',$bookmark = '',$data_class = 'text',$data = ''){
        $array = [
            "type" => $type,
            "bookmark" => $bookmark,
            $data_class => $data
        ];
        return $array;
    }

    /**
     * 数字转换为中文
     * @param  integer  $num  目标数字 yjn
     */
    function number2chinese($num)
    {
        $num = intval($num);
        $char = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
        $unit = ['', '十', '百', '千', '万'];
        $return = '';
        if ($num < 10) {
            $return = $char[$num];
        } elseif ($num%10 == 0) {
            $firstNum = substr($num, 0, 1);
            if ($num != 10) $return .= $char[$firstNum];
            $return .= $unit[strlen($num) - 1];
        } elseif ($num < 20) {
            $return = $unit[substr($num, 0, -1)]. $char[substr($num, -1)];
        } else {
            $numData = str_split($num);
            $numLength = count($numData) - 1;
            foreach ($numData as $k => $v) {
                if ($k == $numLength) continue;
                $return .= $char[$v];
                if ($v != 0) $return .= $unit[$numLength - $k];
            }
            $return .= $char[substr($num, -1)];
        }
        return $return;

    }

    /*
     * 返回地域id末尾不含0的字符串*/
    function wipe_regionid_zero($regionid=''){
        if(empty($regionid)){
            $regionid = session('regulatorpersonInfo.regionid');//默认获取当前登录的客户地域
        }
        $fregionid = rtrim($regionid,0);//去除为0的后缀
        //如果是国家局这种的特殊机构补0
        if(strlen($fregionid) == 1){
            $fregionid = $fregionid.'0';
        }
        return $fregionid;
    }

    /*
    * 接收头信息
    * by zw
    */
     function em_getallheaders()
    {
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
           }
       }
       return $headers;
    }

    /**
    * 判断是否有权限访问
    * by zw
    */
    function Check_QuanXian($modes,$returntype = 'ajax'){
        $is_menujurisdiction = getconfig('is_menujurisdiction');
        if(!empty($is_menujurisdiction)){
            $menujurisdiction = S(session('regulatorpersonInfo.fcode').'_menujurisdiction');
            $jn = false;
            if(is_array($modes)){
                foreach ($modes as $key => $value) {
                    if(in_array($value, $menujurisdiction)){
                        $jn = true;
                        break;
                    }
                }
            }else{
                if(in_array($modes, $menujurisdiction)){
                    $jn = true;
                }
            }
            if($returntype == 'ajax'){
                if(!$jn){
                    echo json_encode(array('code'=>401,'msg'=>'权限不足'));
                    die;
                }
            }else{
                return $jn;
            }
        }
        
    }

    //通过违法表现代码获取法条$data_type 1字符串 2数组
    function get_confirmation_by_code_string($code_string,$separator = ';',$data_type = 1){

        if(!empty(trim($code_string))){
            $codes = explode($separator,$code_string);
            $confirmations = M('tillegal')->where(['fcode'=>['IN',$codes],'fstate'=>1])->getField('fconfirmation',true);
            if(!empty($confirmations)){
                switch ($data_type){
                    case 1:
                        return implode($confirmations,';');
                        break;
                    case 2:
                        return $confirmations;
                        break;
                }
            }
        }
        switch ($data_type){
            case 1:
                return '';
                break;
            case 2:
                return [];
                break;
            default:
                return '';
        }
    }

     /*******文件生成类方法开始*****/
    function word_start() {
        ob_start();
        echo '<html xmlns:o="urn:schemas-microsoft-com:office:office"
        xmlns:w="urn:schemas-microsoft-com:office:word"
        xmlns="http://www.w3.org/TR/REC-html40">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
        table {border-collapse:collapse;border:none;}
        .tb tr td{ background:#ffffff; text-align:center;border:solid #000 1px;}
        </style>';
    }

    function word_save($path) {
        echo "</html>";
        $data = ob_get_contents();

        ob_end_clean();
        word_wirtefile ($path,$data);
    }

    function word_wirtefile ($fn,$data) {
        $fp=fopen($fn,"wa+");
        fwrite($fp,$data);
        fclose($fp);
    }


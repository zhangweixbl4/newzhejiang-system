<?php
//登录管理控制器
namespace Agp\Controller;
use Think\Controller;
class LoginController extends Controller {

    public function index(){
    	header('Location: http://'.$_SERVER['HTTP_HOST'].'/Index/#/login');
	}
	
	/*退出登录*/
	public function out_login(){
		session('regulatorpersonInfo',null);
		session_unset();
        session_destroy();
		D('Function')->write_log('用户退出',0,'退出成功');
		$this->ajaxReturn(array('code'=>0,'msg'=>'退出成功'));
	}
	
	/*登录处理*/
	public function ajax_login(){
		$fcode = I('post.fcode');//获取登录用户名
		$fpassword = I('post.fpassword');//获取登录密码
		$verify = I('post.verify');//获取验证码
		$login_type = I('post.login_type')?I('post.login_type'):'';//登录方式
		$phoneverify = I('post.phoneverify');//获取手机验证码
		
		if($login_type != 'smallprogram'){
			$fcode = RAS_openssl($fcode,'decode');
        	$fpassword = RAS_openssl($fpassword,'decode');
		}
		A('Base')->login_action($fcode,$fpassword,$verify,$phoneverify,$login_type);
	}

	/*
	* 判断用户是否需要短信验证
	* by zw
	*/
	public function get_checkphoneverify(){
		$fcode = I('post.fcode');//获取登录用户名
		$fcode = RAS_openssl($fcode,'decode');
		$data['pv'] = false;
		$data['nv'] = false;
		if(!empty($fcode)){
			$agp_url = C('AGP_SIGNJURISDICTION_URL');
			$userip = getRealIp();//当前用户IP
			$userdata = M('tregulatorperson')->field('fid,fmobile')->where(['fcode|fmobile|fmail'=>$fcode])->find();
			if(in_array($userip,$agp_url)||!empty(S('admin'.cookie('adminphone')))){
				$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
			}
			if(strlen($userdata['fmobile']) == 11 && is_numeric($userdata['fmobile'])){
				$userip = getRealIp();//当前用户IP
				$logincount = M('f_log_manage')->where(['type'=>'用户登录','status'=>1,'user_ip'=>$userip,'fuserid'=>$userdata['fid']])->count();
				if($logincount<=0){
					$data['pv'] = true;
				}
			}
			if (S('error_count'.$fcode) >= 2){
				$data['nv'] = true;
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/*
	* 发送短信验证码
	* by zw
	*/
	public function send_phoneverify(){
		$fcode = I('post.fcode');//获取登录用户名
		$fcode = RAS_openssl($fcode,'decode');
		$userdata = M('tregulatorperson')->field('fid,fmobile,fcode')->where(['fcode|fmobile|fmail'=>$fcode])->find();
		if(strlen($userdata['fmobile']) == 11 && is_numeric($userdata['fmobile'])){
			$userip = getRealIp();//当前用户IP
			$logincount = M('f_log_manage')->where(['type'=>'用户登录','status'=>1,'user_ip'=>$userip,'fuserid'=>$userdata['fid']])->count();
			if($logincount<=0){
				$tel_verify = rand(100000,999999);
				S('phoneverify'.$userdata['fcode'],$tel_verify,1800);
				A('Common/Alitongxin','Model')->identifying_sms($userdata['fmobile'],$tel_verify,'监测平台登录');
				$this->ajaxReturn(array('code'=>0,'msg'=>'发送成功'));
			}
		}
		$this->ajaxReturn(array('code'=>-1,'msg'=>'发送失败'));
	}

	/*登录验证码*/
	public function verify(){
		$Verify = new \Think\Verify(array(
				'useCurve'	=>	false,
				'useNoise'	=>	false,
				'length'	=>	4,
		));
		$Verify->entry();
	}
	
	/*验证码验证*/
	function check_verify($code, $id = ''){
		$verify = new \Think\Verify();
		return $verify->check($code, $id);
	}

	/*
	* 手机重置密码
	* by zw
	*/
	function verifygmm(){
		$fpassword2 = I('fpassword2');//密码
		$fpassword3 = I('fpassword3');//确认密码
		$auto_num = I('auto_num');//手机验证码
		$fpassword2 = RAS_openssl($fpassword2,'decode');
		$fpassword3 = RAS_openssl($fpassword3,'decode');

		if($auto_num != session('user_yz.auto_num')){
			$this->ajaxReturn(array('code'=>1,'msg'=>'手机验证码输入有误'));
		}

		if(empty($fpassword2)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码输入有误'));
		}

		if(judgemimaqiangdu($fpassword2)<=2){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码强度太弱，不能纯数字、纯字母'));
		}

		if($fpassword2 != $fpassword3){
			$this->ajaxReturn(array('code'=>1,'msg'=>'两次密码输入不一致'));
		}

		$where_tn['fmobile'] = session('user_yz.phone');
		$do_tn = M('tregulatorperson')->where($where_tn)->find();
		if(!empty($do_tn)){
			$data_tn['fpassword'] 	= md5($fpassword2);
			$data_tn['flogincount'] = 0;
			$data_tn['fstate'] = 1;
			M('tregulatorperson')->where($where_tn)->save($data_tn);
			D('Function')->write_log('密码找回',1,'密码找回成功','tregulatorperson',$do_tn['fid'],M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'密码找回成功，请妥善保管密码'));
		}else{
			D('Function')->write_log('密码找回',0,'修改失败','tregulatorperson',$do_tn['fid'],M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码找回失败'));
		}
	}

	//获取密码找回手机验证码
	public function getgmyz(){
		$phone = I('post.phone');//手机号
		$verify = I('post.verify');//验证码
		$phone = RAS_openssl($phone,'decode');
		
		if(!A('Login')->check_verify($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));//返回ajax
		}

		$where_tn['fmobile'] = $phone;
		$do_tn = M('tregulatorperson')->where($where_tn)->find();
		if(empty($do_tn)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'用户不存在'));
		}

		$auto_num = make_num(6);
		$user_yz['phone'] = $phone;
		$user_yz['auto_num'] = $auto_num;
		session('user_yz',$user_yz);
		$sms_return = A('Common/Alitongxin','Model')->identifying_sms($phone,$auto_num,'密码找回');
		if(empty($sms_return['code'])){
        	$this->ajaxReturn(array('code'=>0,'msg'=>'验证码已发送'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码发送失败，请重新点击发送'));
		}
	}

	// public function test(){
	// 	$str = '封装测试';
	// 	$cdata = RAS_openssl($str);
	// 	var_dump($cdata);
	// 	$ddata = RAS_openssl($cdata,'decode');
	// 	var_dump($ddata);
	// }
	
}
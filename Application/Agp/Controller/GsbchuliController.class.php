<?php
namespace Agp\Controller;
use Think\Controller;

import('Vendor.PHPExcel');

/**
 * 国家局处理结果上报
 * by zw
 */

class GsbchuliController extends BaseController{

  /**
  * 案件列表
  * by zw
  */
  public function anjian_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $system_num = getconfig('system_num');
    $illadTimeoutTips = getconfig('illadTimeoutTips');//线索处理超时提示，0无，1有
    $againIssueTips = getconfig('againIssueTips');//  上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $illSupervise = getconfig('illSupervise');// 是否需要案件处理的执法监督功能，0不需要，1需要
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $iscontain = I('iscontain');//是否包含下属地区
    $area = I('area');//所属地区

    $outtype = I('outtype');//导出类型
    $selfid = I('selfid');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    $where_tia['_string'] = '1=1';

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['tor.fname'] = array('like','%'.$search_val.'%');
      }
    }

    $where_tia['a.fstatus']   = 0;

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }
    
    $where_tia['a.fcustomer']  = $system_num;

    if(!empty($area)){//所属地区
      if($area != '100000'){
        if(!empty($iscontain)){
          $tregion_len = get_tregionlevel($area);
          if($tregion_len == 1){//国家级
            $where_tia['_string'] .= ' and fregion_id ='.$area;  
          }elseif($tregion_len == 2){//省级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,2).'%"';
          }elseif($tregion_len == 4){//市级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,4).'%"';
          }elseif($tregion_len == 6){//县级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,6).'%"';
          }
          $tmediatempstr = '0,1,2';
        }else{
          $where_tia['fregion_id']  = $area;
        }
      }
    }

    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
    //勾选导出
    if(!empty($outtype) && !empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,a.fmedia_id, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fadowner as fadownername,tn.ffullname mediaregion,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,ifnull(a.fexaminetime,a.create_time) fsend_time,a.adowner_regionid,a.fsample_id,ifnull(db.dbcount,0) dbcount')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join('(select count(*) dbcount,fillegal_ad_id from tbn_illegal_ad_attach where fstate = 0 and ftype = 30 group by fillegal_ad_id) db on a.fid = db.fillegal_ad_id','left')
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

      foreach ($do_tia as $key => $value) {
        $do_tia[$key]['adownerregion'] = M('tregion')->cache(true,86400)->where(['fid'=>$value['adowner_regionid']])->getField('ffullname');

        //判断案件未处理是否超过90天
        if(!empty($value['fsend_time']) && !empty($illadTimeoutTips) && empty($outtype) && empty($value['fstatus'])){
          if((time()-strtotime($value['fsend_time']))>90*86400){
            $do_tia[$key]['showTimeout'] = 1;
          }else{
            $do_tia[$key]['showTimeout'] = 0;
          }
        }else{
          $do_tia[$key]['showTimeout'] = 0;
        }

        //判断上个月有播放并处理了的，本月还在播放的违法广告
        if(!empty($againIssueTips) && empty($value['fstatus'])){
          $where_issue['a.fcustomer'] = $system_num;
          $where_issue['a.fsample_id'] = $value['fsample_id'];
          $where_issue['a.fstatus'] = 10;
          $where_issue['b.fissue_date'] = ['between',[date('Y-m-01',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 month')),date('Y-m-d',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 day'))]];
          $againIssue = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
            ->where($where_issue)
            ->count();
          if(!empty($againIssue)){
            $do_tia[$key]['againIssue'] = 1;
          }else{
            $do_tia[$key]['againIssue'] = 0;
          }
        }else{
          $do_tia[$key]['againIssue'] = 0;
        }

      }

      if(!empty($outtype)){
        if(empty($do_tia)){
          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }

        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-案件线索列表';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'mediaregion',
          '发布媒体'=>'fmedianame',
          '媒体分类'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网'],
            ]
          ],
          '广告类别'=>'fadclass',
          '广告名称'=>'fad_name',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '派发日期'=>'fexaminetime',
          '查看状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fview_status} == 0','未查看'],
              ['{fview_status} == 10','已查看'],
            ]
          ],
          '处理状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus} == 0','未处理'],
              ['{fstatus} == 10','已上报'],
              ['{fstatus} == 30','数据复核'],
            ]
          ],
          '处理结果'=>'fresult',
          '处理时间'=>'fresult_datetime',
          '处理机构'=>'fresult_unit',
          '处理人'=>'fresult_user',
          '立案状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus2} == 15','待处理'],
              ['{fstatus2} == 16','已处理'],
              ['{fstatus2} == 17','处理中'],
              ['{fstatus2} == 20','撤消'],
            ]
          ],
          '复核状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus3} == 10','复核中'],
              ['{fstatus3} == 20','复核失败'],
              ['{fstatus3} == 30','复核通过'],
            ]
          ],
        ];
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        D('Function')->write_log('案件线索列表',1,'导出成功');
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

      }else{
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
      }
  }

  /**
  * 查看状态更改
  * by zw
  */
  public function anjian_look(){
    $system_num = getconfig('system_num');
    if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel')==30){
      $this->ajaxReturn(array('code'=>0,'msg'=>'处理完成'));
    }
    $fid   = I('fid');//违法广告ID

    $where_tia['fid'] = $fid;
    $where_tia['fview_status'] = 0;
    $where_tia['fcustomer']  = $system_num;

    $data_tia['fview_status'] = 10;
    $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
    $this->ajaxReturn(array('code'=>0,'msg'=>'处理完成'));
  }

  /**
  * 查看案件详情
  * by zw
  */
  public function anjian_view(){
    $system_num = getconfig('system_num');
    $scsavetime = getconfig('scsavetime');

    $fid = I('fid');//违法广告ID
    $mclass = I('mclass');//媒介类别
    $sendstatus = I('sendstatus',0);//发布状态
    $fissuedatest   = I('fissuedatest');//发布时间起
    $fissuedateed   = I('fissuedateed');//发布时间止
    $dit_id = I('dit_id');//检查计划ID

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $times_table = I('times_table');
    $table_condition = I('table_condition');//选择时间
    if(!empty($times_table) && !empty($table_condition)){
      if($times_table == 'tbn_ad_summary_day'){
          $timetypes = 0;
          $timeval = $table_condition;
      }elseif($times_table == 'tbn_ad_summary_week'){
          $timetypes = 10;
          $timeval = $table_condition;
      }elseif($times_table == 'tbn_ad_summary_half_month'){
          $timetypes = 20;
          $table_condition2 = explode('-', $table_condition);
          $table_condition3 = (int)$table_condition2[0]*2;
          $table_condition4 = ((int)$table_condition2[1])==1?0:1;
          $timeval = $table_condition3+$table_condition4;
      }elseif($times_table == 'tbn_ad_summary_month'){
          $timetypes = 30;
          $table_condition2 = explode('-', $table_condition);
          $timeval = (int)$table_condition2[0];
      }elseif($times_table == 'tbn_ad_summary_quarter'){
          $timetypes = 40;
          if($table_condition == '01-01'){
              $timeval = 1;
          }elseif($table_condition == '04-01'){
              $timeval = 2;
          }elseif($table_condition == '07-01'){
              $timeval = 3;
          }else{
              $timeval = 4;
          }
      }elseif($times_table == 'tbn_ad_summary_half_year'){
          $timetypes = 50;
          if($table_condition == '01-01'){
              $timeval = 1;
          }else{
              $timeval = 2;
          }
      }elseif($times_table == 'tbn_ad_summary_year'){
          $timetypes = 60;
          $timeval = 1;
      }
    }

    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$sendstatus);
    
    $where_time .= ' and a.fid = '.$fid;
    $where_time .= ' and a.fmedia_class = '.$mclass;

    if(!empty($fissuedatest) && !empty($fissuedateed)){
      $where_time .= ' and fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
    }

    if($mclass==3){//报纸详情读取
      $do_tia = M('tbn_illegal_ad')
        ->alias('a')
        ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fjpgfilename,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fsample_id,x.fpage,a.fadowner as fadownername')
        ->join('tadclass b on a.fad_class_code=b.fcode')
        ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
        ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek,fpage from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id and a.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
        ->where($where_tia)
        ->order('x.fstarttime desc')
        ->find();
      if(!empty($do_tia)){
        $where_te['a.fid'] = $do_tia['fsample_id'];
        $do_te = M('tpapersample')
          ->alias('a')
          ->field('b.furl,a.fconfirmations')
          ->join('tpapersource b on a.sourceid=b.fid')
          ->where($where_te)
          ->find();
        $do_tia['fjpgfilename2'] = $do_te['furl'];
        $do_tia['fconfirmations'] = $do_te['fconfirmations'];
      }
    }elseif($mclass==1 || $mclass == 2){//电视或广播
      $do_tia = M('tbn_illegal_ad')
        ->alias('a')
        ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.favifilename,a.fadowner as fadownername,a.fsample_id')
        ->join('tadclass b on a.fad_class_code=b.fcode')
        ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
        ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue b,tbn_illegal_ad a where '.$where_time.' and a.fid = b.fillegal_ad_id and a.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
        ->where($where_tia)
        ->find();

      $where_te['_string'] = $where_time;
      $do_te = M('tbn_illegal_ad_issue')
        ->alias('b')
        ->field('b.fid as tid,b.fmedia_id,fstarttime,fendtime,(case when fissue_date>DATE_SUB(CURDATE(), INTERVAL '.$scsavetime.' MONTH) then 1 else 0 end) isplay')
        ->join('tbn_illegal_ad a on a.fid = b.fillegal_ad_id')
        ->where($where_te)
        ->order('fstarttime desc')
        ->select();
      $do_tia['issue_list'] = $do_te;

      $where2['source_type'] = 20;
      $where2['source_tid'] = $fid;
      $where2['source_mediaclass'] = sprintf("%02d", $mclass);
      $data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->order('source_id desc')->find();
      if(!empty($data2)){
        $isurl = getHttpStatus($data2['source_url']);
        if($isurl == 200){
          $do_tia['source_url'] = $data2['source_url'];
          $do_tia['source_id']  = $data2['source_id'];
          $do_tia['source_state'] = $data2['source_state'];
        }else{
          $do_tia['source_state'] = $data2['source_state'];
        }
      }

      if(!empty($do_tia)){
        if($mclass == 1){
          $tb_str = 'tv';
        }else{
          $tb_str = 'bc';
        }
        $fconfirmations = M('t'.$tb_str.'sample')
          ->where(['fid'=>$do_tia['fsample_id']])
          ->getField('fconfirmations');
        $do_tia['fconfirmations'] = $fconfirmations;
      }

    }
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tia));
  }

  /**
  * 获取视频地址
  * by zw
  */
  public function get_favifilename(){
    $fmedia_id  = I('fmedia_id');//主媒体ID
    $fstarttime = I('fstarttime');//播放开始时间
    $fendtime   = I('fendtime');//播放结束时间
    $favifilelist = [];
    if(empty($fstarttime) || empty($fendtime)){
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>0,'data'=>[]));
    }
    $favifiledata['favifilename'] = A('Common/Media','Model')->get_m3u8($fmedia_id,strtotime($fstarttime),strtotime($fendtime));
    if(!empty($favifiledata['favifilename'])){
      $favifiledata['fmedia_id'] = $fmedia_id;
      $favifilelist[] = $favifiledata;
    }

    $where_ta['main_media_id'] = $fmedia_id;
    $where_ta['fid'] = array('neq',$fmedia_id);
    $where_ta['fstate'] = 1;
    $do_ta = M('tmedia')->field('fid')->where($where_ta)->select();
    foreach ($do_ta as $key => $value) {
      $favifiledata['favifilename'] = A('Common/Media','Model')->get_m3u8($value['fid'],strtotime($fstarttime),strtotime($fendtime));

      if(!empty($favifiledata['favifilename'])){
        $favifiledata['fmedia_id'] = $value['fid'];
        $favifilelist[] = $favifiledata;
      }
    }

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>count($favifilelist),'data'=>$favifilelist));

  }

  /**
  * 案件上报
  * by zw
  */
  public function anjian_sb(){
    $system_num = getconfig('system_num');
    $issbfile   = getconfig('issbfile');//违法广告处理时，上报文件是否必传，0非必须，10必须
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $selfresult = I('selfresult');//处理结果
    $attach     = [];//预添加的附件信息
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo) && !empty($issbfile)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件材料'));
    }
    if(empty($selfresult)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择处理结果'));
    }

    $prvupids = [];//用于更新上一派发记录的处理状态更新
    foreach ($selfid as $value) {
      $where_tia['fid']     = $value;
      $where_tia['fstatus'] = 0;
      $where_tia['fcustomer'] = $system_num;
      $where_tia['_string'] = 'fid in (select fillegal_ad_id from tbn_case_send where fstatus = 0 and frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').')';

      $data_tia['fstatus']          = 10;//已上报
      $data_tia['fview_status']     = 10;//已查看
      $data_tia['fresult']          = $selfresult;//处理结果
      $data_tia['fresult_datetime'] = date('Y-m-d H:i:s');//上报时间
      $data_tia['fresult_unit']     = session('regulatorpersonInfo.regulatorpname');//上报机构
      $data_tia['fresult_unitid']     = session('regulatorpersonInfo.fregulatorpid');//上报机构
      $data_tia['fresult_user']     = session('regulatorpersonInfo.fname');//上报人
      $data_tia['fresult_userid']     = session('regulatorpersonInfo.fid');//上报人
      //判断是否有立案调查，如有：添加立案调查记录
      if(strpos($selfresult,'立案')!==false){
        $data_tia['fstatus2']   = 17;//立案待处理
      }
      $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);

      if(!empty($do_tia)){
        $prvupids[] = $value;

        //上传附件
        $attach_data['ftype']    = 0;//类型
        $attach_data['fillegal_ad_id']    = $value;//违法广告
        $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
        $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattachname'];
          $attach_data['fattach_url'] = $value2['fattachurl'];
          array_push($attach,$attach_data);
        }
      }
    }
    //更新上一派发记录的处理状态
    if(!empty($prvupids)){
      $where_prv['fstatus'] = 0;
      $where_prv['frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
      $where_prv['fillegal_ad_id'] = ['in',$prvupids];
      $data_prv['fstatus'] = 10;
      $data_prv['freceiver'] = session('regulatorpersonInfo.fname');
      $data_prv['frece_time'] = date('Y-m-d H:i:s');
      M('tbn_case_send')->where($where_prv)->save($data_prv);
    }

    //有上传附件时需要添加附件记录
    if(!empty($attach)){
      M('tbn_illegal_ad_attach')->addAll($attach);
    }
    D('Function')->write_log('处理结果上报',1,'上报成功','tbn_illegal_ad',$do_tia,M('tbn_illegal_ad')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
  }

  /**
  * 案件派发
  * by zw
  */
  public function anjian_pf(){
    $system_num = getconfig('system_num');
    $selfid     = I('selfid');//违法广告ID组
    $tregionid  = I('tregionid');//行政区划ID

    $adddata    = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($tregionid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择需要派发的机构'));
    }
    if($tregionid == session('regulatorpersonInfo.regionid')){
      $this->ajaxReturn(array('code'=>1,'msg'=>'无法向本机构派发'));
    }
    $where_tr['fregionid'] = $tregionid;
    $where_tr['_string'] = 'fstate=1 and ftype=20 and fkind=1';
    $do_tr = M('tregulator')->field('fid,fname')->where($where_tr)->find();
    if(empty($do_tr)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'该区域还未建立相应机构'));
    }

    $prvupids = [];//用于更新上一派发记录的处理状态更新
    foreach ($selfid as $key => $value) {
      $prvupids[] = $value;

      //将需要添加的派发记录放到数组中
      $data_tcs['fillegal_ad_id'] = $value;
      $data_tcs['frece_reg_id']   = $do_tr['fid'];
      $data_tcs['frece_reg']      = $do_tr['fname'];
      $data_tcs['fsendtype']      = 1;
      $data_tcs['fsend_reg_id']   = session('regulatorpersonInfo.fregulatorpid');
      $data_tcs['fsend_reg']      = session('regulatorpersonInfo.regulatorpname');
      $data_tcs['fsender']        = session('regulatorpersonInfo.fname');
      $data_tcs['fsend_time']     = date('Y-m-d H:i:s');
      array_push($adddata, $data_tcs);
    }
    if(!empty($adddata)){
      //更新上一派发记录的处理状态
      if(!empty($prvupids)){
        $where_prv['fstatus'] = 0;
        $where_prv['frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
        $where_prv['fillegal_ad_id'] = ['in',$prvupids];
        $data_prv['fstatus'] = 20;
        $data_prv['freceiver'] = session('regulatorpersonInfo.fname');
        $data_prv['frece_time'] = date('Y-m-d H:i:s');
        M('tbn_case_send')->where($where_prv)->save($data_prv);
      }

      //添加新派发记录
      M('tbn_case_send')->addAll($adddata);

      D('Function')->write_log('处理结果上报',1,'派发成功','tbn_illegal_ad',0,implode(",",$selfid));
      $this->ajaxReturn(array('code'=>0,'msg'=>'派发成功'));
    }else{
      D('Function')->write_log('处理结果上报',0,'派发失败','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'派发失败'));
    }
  }

  /**
  * 取消案件派发
  * by zw
  */
  public function anjian_unpf(){
    $system_num = getconfig('system_num');
    $selfid     = I('selfid');//违法广告ID组

    $adddata    = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    foreach ($selfid as $key => $value) {
      $where_csd['fillegal_ad_id'] = $value;
      $where_csd['fstatus'] = 0;
      $where_csd['fsendtype'] = 1;
      $where_csd['fsend_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
      $do_csd = M('tbn_case_send')->where($where_csd)->delete();//删除当前机构的派发记录
      if(!empty($do_csd)){
        $where_csd2['fillegal_ad_id'] = $value;
        $where_csd2['fstatus'] = 20;
        $where_csd2['frece_reg_id'] = session('regulatorpersonInfo.fregulatorpid');
        $do_csd2 = M('tbn_case_send')->where($where_csd2)->save(['fstatus'=>0,'frece_time'=>date('Y-m-d H:i:s')]);//更新上一派发记录
      }
      D('Function')->write_log('取消案件派发',1,'取消成功','tbn_illegal_ad',$value,M('tbn_illegal_ad')->getlastsql());
    }
    
    $this->ajaxReturn(array('code'=>0,'msg'=>'取消成功'));
  }

  /**
  * 接收案件列表
  * by zw
  */
  public function jsanjian_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $system_num = getconfig('system_num');
    $illadTimeoutTips = getconfig('illadTimeoutTips');//线索处理超时提示，0无，1有
    $againIssueTips = getconfig('againIssueTips');//  上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $isdudao = getconfig('isdudao');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $outtype = I('outtype');//导出类型
    $selfid = I('selfid');//勾选项
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['tor.fname'] = array('like','%'.$search_val.'%');
      }
    }

    $where_tia['a.fstatus']   = 0;
    $where_tia['a.fcustomer'] = $system_num;

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }

    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
    //勾选导出
    if(!empty($outtype) && !empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 1 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,a.fmedia_id, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fadowner as fadownername,tn.ffullname mediaregion,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,a.create_time,d.fsend_time,a.adowner_regionid,a.fsample_id,ifnull(db.dbcount,0) dbcount')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 1 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join('(select count(*) dbcount,fillegal_ad_id from tbn_illegal_ad_attach where fstate = 0 and ftype = 30 group by fillegal_ad_id) db on a.fid = db.fillegal_ad_id','left')
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    foreach ($do_tia as $key => $value) {
      $do_tia[$key]['adownerregion'] = M('tregion')->cache(true,86400)->where(['fid'=>$value['adowner_regionid']])->getField('ffullname');

      //判断案件未处理是否超过90天
      if(!empty($value['fsend_time']) && !empty($illadTimeoutTips) && !empty($outtype) && empty($value['fstatus'])){
        if((time()-strtotime($value['fsend_time']))>90*86400){
          $do_tia[$key]['showTimeout'] = 1;
        }else{
          $do_tia[$key]['showTimeout'] = 0;
        }
      }else{
        $do_tia[$key]['showTimeout'] = 0;
      }

      //判断上个月有播放并处理了的，本月还在播放的违法广告
      if(!empty($againIssueTips) && empty($value['fstatus'])){
        $where_issue['a.fcustomer'] = $system_num;
        $where_issue['a.fsample_id'] = $value['fsample_id'];
        $where_issue['a.fstatus'] = 10;
        $where_issue['b.fissue_date'] = ['between',[date('Y-m-01',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 month')),date('Y-m-d',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 day'))]];
        $againIssue = M('tbn_illegal_ad')
          ->alias('a')
          ->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
          ->where($where_issue)
          ->count();
        if(!empty($againIssue)){
          $do_tia[$key]['againIssue'] = 1;
        }else{
          $do_tia[$key]['againIssue'] = 0;
        }
      }else{
        $do_tia[$key]['againIssue'] = 0;
      }
    }

    if(empty($outtype) && !empty($isdudao)){
      foreach ($do_tia as $key => $value) {
        $where_dd['_string'] = 'a.fcustomer = "'.$system_num.'" and a.fresult_unit = "'.session('regulatorpersonInfo.regulatorpname').'" and a.fid<>'.$value['fid'].' and a.fmedia_id = '.$value['fmedia_id'].' and a.fstatus = 10 and b.fissue_date<"'.date('Y-m-d',strtotime($value['fendtime']. ' -5 day')).'" and a.fad_name = "'.$value['fad_name'].'"';
        $count_dd = M('tbn_illegal_ad')
          ->alias('a')
          ->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
          ->where($where_dd)
          ->count();
        $do_tia[$key]['isdudao'] = $count_dd;
      }
    }
    
    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }
      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-案件线索接收列表';//文档内部标题名称
      $outdata['datalie'] = [
        '序号'=>'key',
        '地域'=>'mediaregion',
        '发布媒体'=>'fmedianame',
        '媒体分类'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1','电视'],
            ['{fmedia_class} == 2','广播'],
            ['{fmedia_class} == 3','报纸'],
            ['{fmedia_class} == 13','互联网'],
          ]
        ],
        '广告类别'=>'fadclass',
        '广告名称'=>'fad_name',
        '播出总条次'=>'fcount',
        '播出周数'=>'diffweek',
        '播出时间段'=>[
          'type'=>'zwhebing',
          'data'=>'{fstarttime}~{fendtime}'
        ],
        '违法表现代码'=>'fillegal_code',
        '违法表现'=>'fexpressions',
        '涉嫌违法原因'=>'fillegal',
        '证据下载地址'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
            ['','{fjpgfilename}']
          ]
        ],
        '派发时间'=>'fsend_time',
        '查看状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fview_status} == 0','未查看'],
            ['{fview_status} == 10','已查看'],
          ]
        ],
        '处理状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus} == 0','未处理'],
            ['{fstatus} == 10','已上报'],
            ['{fstatus} == 30','数据复核'],
          ]
        ],
        '处理结果'=>'fresult',
        '处理时间'=>'fresult_datetime',
        '处理机构'=>'fresult_unit',
        '处理人'=>'fresult_user',
        '立案状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus2} == 15','待处理'],
            ['{fstatus2} == 16','已处理'],
            ['{fstatus2} == 17','处理中'],
            ['{fstatus2} == 20','撤消'],
          ]
        ],
        '复核状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus3} == 10','复核中'],
            ['{fstatus3} == 20','复核失败'],
            ['{fstatus3} == 30','复核通过'],
          ]
        ],
      ];
      $outdata['lists'] = $do_tia;
      $ret = A('Api/Function')->outdata_xls($outdata);

      D('Function')->write_log('案件线索接收列表',1,'生成成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }

  }

  /**
  * 派发案件列表
  * by zw
  */
  public function pfanjian_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $system_num = getconfig('system_num');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
  
    $outtype = I('outtype');//导出类型
    $selfid = I('selfid');//勾选项
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页
      $pp = 20;//每页显示多少记录
    }

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['tor.fname'] = array('like','%'.$search_val.'%');
      }
    }

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }
    $where_tia['a.fstatus']   = 0;
    $where_tia['a.fcustomer'] = $system_num;

    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
    //勾选导出
    if(!empty($outtype) && !empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fsendtype = 1 and d.fstatus=0 and d.fsend_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,d.frece_reg,a.fadowner as fadownername,tn.ffullname,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,d.fsend_time')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fsendtype = 1 and d.fstatus=0 and d.fsend_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-案件线索派发列表';//文档内部标题名称
      $outdata['datalie'] = [
        '序号'=>'key',
        '地域'=>'ffullname',
        '发布媒体'=>'fmedianame',
        '媒体分类'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1','电视'],
            ['{fmedia_class} == 2','广播'],
            ['{fmedia_class} == 3','报纸'],
            ['{fmedia_class} == 13','互联网'],
          ]
        ],
        '广告类别'=>'fadclass',
        '广告名称'=>'fad_name',
        '播出总条次'=>'fcount',
        '播出周数'=>'diffweek',
        '播出时间段'=>[
          'type'=>'zwhebing',
          'data'=>'{fstarttime}~{fendtime}'
        ],
        '违法表现代码'=>'fillegal_code',
        '违法表现'=>'fexpressions',
        '涉嫌违法原因'=>'fillegal',
        '证据下载地址'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
            ['','{fjpgfilename}']
          ]
        ],
        '派发时间'=>'fsend_time',
        '查看状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fview_status} == 0','未查看'],
            ['{fview_status} == 10','已查看'],
          ]
        ],
        '处理状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus} == 0','未处理'],
            ['{fstatus} == 10','已上报'],
            ['{fstatus} == 30','数据复核'],
          ]
        ],
        '处理结果'=>'fresult',
        '处理时间'=>'fresult_datetime',
        '处理机构'=>'fresult_unit',
        '处理人'=>'fresult_user',
        '立案状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus2} == 15','待处理'],
            ['{fstatus2} == 16','已处理'],
            ['{fstatus2} == 17','处理中'],
            ['{fstatus2} == 20','撤消'],
          ]
        ],
        '复核状态'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fstatus3} == 10','复核中'],
            ['{fstatus3} == 20','复核失败'],
            ['{fstatus3} == 30','复核通过'],
          ]
        ],
      ];
      $outdata['lists'] = $do_tia;
      $ret = A('Api/Function')->outdata_xls($outdata);

      D('Function')->write_log('案件线索派发列表',1,'导出成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }
  }

  /**
  * 本省数据复核列表
  * by zw
  */
  public function bsfuhe_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $system_num = getconfig('system_num');
    $isexamine = getconfig('isexamine');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['tor.fname'] = array('like','%'.$search_val.'%');
      }
    }
    
    $where_tia['a.fstatus']     = 0;

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }
    $where_tia['a.fcustomer']  = $system_num;

    $where_tia['_string'] = '1=1';
    if($system_num == '100000'){
      if(session('regulatorpersonInfo.fregulatorlevel') == 10 || session('regulatorpersonInfo.fregulatorlevel') == 0){
        $where_tia['_string'] .= ' and a.fad_name not in (SELECT fad_name FROM tbn_illegal_ad a inner join tregion b on a.fregion_id=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and a.fcustomer='.$system_num.' and left(a.fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2) .'00" and left(a.fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,4).'" GROUP BY a.fad_name) ';

      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 20){
        $where_tia['_string'] .= ' and a.fad_name not in (select fad_name from tbn_illegal_ad a inner join tregion b on a.fregion_id=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' and left(fregion_id,2)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2).'" GROUP BY fad_name)';
      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 30){
        $where_tia['_string'] .= ' and a.fad_name not in (select fad_name from (select a.fad_name from tbn_illegal_ad a inner join tregion b on a.fregion_id=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having count(1)>1) ';
      }
    }
    
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fadowner as fadownername')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 跨地域数据复核列表
  * by zw
  */
  public function ksfuhe_list(){
    Check_QuanXian(['sbchuli']);
    session_write_close();
    $system_num = getconfig('system_num');
    $isexamine = getconfig('isexamine');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
  
    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }
    }

    $where_tia['a.fstatus']     = 0;

    if($system_num == '100000'){
      if(session('regulatorpersonInfo.fregulatorlevel') == 10 || session('regulatorpersonInfo.fregulatorlevel') == 0){
        $joinstr = 'inner join (select fad_name,(count(1)+1) as fregion_count,concat("'.session('regulatorpersonInfo.regionname1').',",group_concat(fname1)) as fregionname,sum(yds) as ydscount from (select a.fad_name,b.fname1,(case when left(a.fregion_id,2) <> "'.substr(session('regulatorpersonInfo.regionid'),0,2).'" then 1 else 0 end) as yds from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2) .'00" and left(fregion_id,4)<>"'.substr(session('regulatorpersonInfo.regionid'),0,4).'" group by a.fregion_id,a.fad_name ) as a group by a.fad_name) k on a.fad_name = k.fad_name';
      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 20){
        $joinstr = 'inner join (select fad_name,(count(1)+1) as fregion_count,concat("'.session('regulatorpersonInfo.regionname1').',",group_concat(fname1)) as fregionname,count(1) as ydscount from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' and left(fregion_id,2)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2).'" group by a.fregion_id,a.fad_name ) as a group by a.fad_name) k on a.fad_name = k.fad_name';
      }elseif(session('regulatorpersonInfo.fregulatorlevel') == 30){
        $joinstr = 'inner join (select fad_name,count(1) as fregion_count,group_concat(fname1) as fregionname,count(1) as ydscount from (select a.fad_name,b.fname1 from tbn_illegal_ad a inner join tregion b on concat(left(a.fregion_id,2),"0000")=b.fid and b.flevel in (1,2,3) inner join tbn_illegal_ad_issue c on a.fid = c.fillegal_ad_id where '.$where_time.' and fcustomer = '.$system_num.' group by left(a.fregion_id,2),a.fad_name ) as a group by a.fad_name having fregion_count>1) k on a.fad_name = k.fad_name';
      }
      $fieldstr = ',fregion_count,fregionname,ydscount';
    }else{
      $where_tia['_string'] = '1=0';
    }
    
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
  
    $where_tia['a.fcustomer']  = $system_num;

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join($joinstr)
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek'.$fieldstr)
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      // ->join('(select distinct(fmediaid) from tmedia_temp where ftype in(0,1) and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on c.fid=ttp.fmediaid')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join($joinstr)
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 提交复核申请
  * by zw
  */
  public function fhshuju(){
    
    $system_num = getconfig('system_num');
    $datacheck_isfile = getconfig('datacheck_isfile');//数据复核时是否强制上传文件，0否，1强制所有级别用户，2强制非顶级用户
    $datacheck_istext  = getconfig('datacheck_istext ');//数据复核是否有意见输入，0没有，1所有都有，2顶级有，3所有都有并强制要求输入，4顶级有并强制要求输入
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $kystatus   = I('kystatus');//是否跨域（0否，10是）
    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    $tregionlevel = getconfig('tregionlevel');//到客户机构等级终止提交复核
    $fregulatorppid = session('regulatorpersonInfo.fregulatorppid');
    $selnote = I('selnote');//审核意见
    $send_upids = [];//需要更新的派发记录ID

    if(strlen($fregulatorppid) == 8){
      $fregulatorppregion = substr($fregulatorppid,2,6);
      $tregion_len = get_tregionlevel($fregulatorppregion);
      if($tregion_len == 1){//国家级
        $fregulatorppregion = 30;
      }elseif($tregion_len == 2){//省级
        $fregulatorppregion = 20;
      }elseif($tregion_len == 4){//市级
        $fregulatorppregion = 10;
      }elseif($tregion_len == 6){//县级
        $fregulatorppregion = 0;
      }
    }else{
      $fregulatorppregion = $fregulatorlevel + 10;
    }

    $attach     = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo)){
      if($datacheck_isfile == 1 || ($datacheck_isfile == 2 && $tregionlevel != ($fregulatorlevel + 10))){
        $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
      }
    }
    if(empty($selnote)){
      if($datacheck_istext == 3 || ($datacheck_istext == 4 && $tregionlevel == ($fregulatorlevel + 10))){
        $this->ajaxReturn(array('code'=>1,'msg'=>'请输入审核意见'));
      }
    }
    $where_tia['fstatus']     = 0;
    foreach ($selfid as $key => $value) {
      $where_tia['fid']         = $value;
      $where_tia['fcustomer']  = $system_num;
   
      $data_tia['fstatus']      = 30;//复核状态
      $data_tia['fstatus3']     = 10;//复核申请
      $data_tia['fview_status'] = 10;//已查看
      $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);//判断用户是否有该信息处理的权限，并保存相应状态
      if(!empty($do_tia)){
        $send_upids[] = $value;
        $data_tdc['fillegal_ad_id'] = $value;
        $data_tdc['tcheck_createtime']  = date('Y-m-d H:i:s');
        $data_tdc['tregionid'] = substr(session('regulatorpersonInfo.regionid'),0,2);
        if($fregulatorlevel == 30){//国家级
          $data_tdc['tgj_treid']     = session('regulatorpersonInfo.fregulatorpid');
          $data_tdc['tgj_trename']   = session('regulatorpersonInfo.regulatorpname');
          $data_tdc['tgj_username']  = session('regulatorpersonInfo.fname');
          $data_tdc['tgj_time']      = date('Y-m-d H:i:s');
          $data_tdc['tgj_result']    = $selnote?$selnote:'同意';
          $data_tdc['tlook_status']   = $fregulatorlevel;
          $data_tdc['tkuayu_status']  = 10;
          $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
          $data_tdc['tcheck_status']  = 10;

          M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$value);//复核成功
          $weihuphone = getconfig('fuhephone');
          $fhphone = S('fh'.$weihuphone);
          if(empty($fhphone)){
            S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
            $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
          }
        }elseif($fregulatorlevel == 20){//省级
          if(empty($kystatus) || $tregionlevel==30){
            $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshengj_result']    = $selnote?$selnote:'同意';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 10;

            M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$value);//复核成功
            $weihuphone = getconfig('fuhephone');
            $fhphone = S('fh'.$weihuphone);
            if(empty($fhphone)){
              S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
              $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
            }
          }else{
            $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshengj_result']    = $selnote?$selnote:'申请复核';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 0;
          }
        }elseif($fregulatorlevel == 10){//市级
          if($tregionlevel==20){
            $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshij_result']    = $selnote?$selnote:'同意';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 10;

            M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$value);//复核成功
            $weihuphone = getconfig('fuhephone');
            $fhphone = S('fh'.$weihuphone);
            if(empty($fhphone)){
              S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
              $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
            }
          }else{
            $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshij_result']    = $selnote?$selnote:'申请复核';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 0;
          }
        }else{//区级
          if($tregionlevel==10){
            $data_tdc['tquj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tquj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tquj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tquj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tquj_result']    = $selnote?$selnote:'同意';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 10;
          }else{
            $data_tdc['tquj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tquj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tquj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tquj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tquj_result']    = $selnote?$selnote:'申请复核';
            $data_tdc['tlook_status']   = $fregulatorlevel;
            $data_tdc['tkuayu_status']  = $kystatus;
            $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            $data_tdc['tcheck_status']  = 0;
          }
        }
        $do_tdc = M('tbn_data_check')->add($data_tdc);

        //上传附件
        $attach_data['fillegal_ad_id']    = $value;//违法广告
        $attach_data['fcheck_id']         = $do_tdc;//复核表Id
        $attach_data['fupload_trelevel']  = $fregulatorlevel;//上传机构级别
        $attach_data['fupload_treid']     = session('regulatorpersonInfo.fregulatorpid');//机构ID
        $attach_data['fupload_trename']   = session('regulatorpersonInfo.regulatorpname');//机构名
        $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
        $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattachname'];
          $attach_data['fattach_url'] = $value2['fattachurl'];
          array_push($attach,$attach_data);
        }
      }
    }
    if(!empty($do_tdc)){
      if(!empty($send_upids)){
        M('tbn_case_send')->where(['fstatus'=>0,'fillegal_ad_id'=>['in',$send_upids]])->save(['freceiver'=>session('regulatorpersonInfo.fname'),'frece_time'=>date('Y-m-d H:i:s'),'fstatus'=>30]);
      }
      M('tbn_data_check_attach')->addAll($attach);
      D('Function')->write_log('数据复核',1,'操作成功','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
    }else{
      D('Function')->write_log('数据复核',0,'操作失败','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
    }
  }

  /**
  * 违法广告线索审核列表
  * by zw
  */
  public function filladlist(){
    session_write_close();
    $system_num = getconfig('system_num');
    $isrelease = getconfig('isrelease');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $outtype = I('outtype');//导出类型
    $selfid = I('selfid');//勾选项
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页
      $pp = 20;//每页显示多少记录
    }
    
    $fregionid = I('fregionid');//行政区划
    $fexamine = I('fexamine')?I('fexamine'):0;//线索审核状态

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    if($isrelease == 2){
      $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',-1);
    }else{
      $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');
    }

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['tor.fname'] = array('like','%'.$search_val.'%');
      }
    }

    // $where_tia['a.fstatus']     = 0;//处理状态，未处理

    if(empty($fregionid)){
      if(session('regulatorpersonInfo.fregulatorlevel')==0){
        $where_tia['a.fregion_id']  = session('regulatorpersonInfo.regionid');
      }elseif(session('regulatorpersonInfo.fregulatorlevel')==10){
        $where_tia['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
      }elseif(session('regulatorpersonInfo.fregulatorlevel')==20){
        $where_tia['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
      }
    }else{
      $where_tia['a.fregion_id']  = $fregionid;
    }

    $where_tia['a.fexamine'] = $fexamine;

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
    //勾选导出
    if(!empty($outtype) && !empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }
  
    $where_tia['a.fcustomer'] = $system_num;

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    
    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fexamine,(case when a.fexamine=0 then "未审核" else "已审核" end) as fexaminename,(case when a.fmedia_class=1 then "电视" when a.fmedia_class=2 then "广播" when a.fmedia_class=3 then "报纸" when a.fmedia_class=13 then "互联网" end) as fmedia_classname,a.fadowner as fadownername,a.fexaminetime,dck.tcheck_status,dck.tcheck_fhreson,dck.tshengj_result,dck.tshij_result,dck.tquj_result,tn.ffullname')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
      ->join('(select * from tbn_data_check where id in (select max(id) from tbn_data_check group by fillegal_ad_id)) dck on a.fid = dck.fillegal_ad_id','left')
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();
      
    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-违法广告线索审核列表';//文档内部标题名称
      $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'ffullname',
          '广告名称'=>'fad_name',
          '广告类别'=>'fadclass',
          '发布媒体'=>'fmedianame',
          '媒体类别'=>'fmedia_classname',
          '广告主'=>'fadownername',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '状态'=>'fexaminename',
        ];
      $outdata['lists'] = $do_tia;
      $ret = A('Api/Function')->outdata_xls($outdata);
      D('Function')->write_log('违法广告线索审核列表',1,'导出成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }
  }

  /**
  * 违法广告线索审核
  * by zw
  */
  public function filladexamine(){
    $isexamine = getconfig('isexamine');
    $selfid     = I('selfid');//违法广告ID组
    $selfresult = I('selfresult');//审核状态
    $selnote = I('selnote')?I('selnote'):"同意";//审核意见
    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($selfresult)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择审核状态'));
    }
    
    $where_tia['fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
    $where_tia['fid']      = array('in',$selfid);

    $data_tia['fexamine'] = $selfresult;
    $data_tia['fexaminetime'] = date('Y-m-d H:i:s');

    if(in_array($isexamine, [20,40,50])){
      //审核不通过的数据直接申请复核
      if($selfresult == 20){
        $data_tia['fstatus'] = 30;
        $data_tia['fstatus3'] = 30;
        M('tbn_case_send')->where(['fillegal_ad_id'=>['in',$selfid],'fstatus'=>0])->save(['fstatus'=>30]);
        M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
      }else{
        foreach ($selfid as $key => $value) {
          $nowcount = M('tbn_illegal_ad')->where(['fid'=>$value,'fexamine'=>20])->count();
          if(!empty($nowcount)){
            M()->execute('update tbn_illegal_ad_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//上报文件失效
            M()->execute('update tbn_data_check set tcheck_status=20 where fillegal_ad_id='.$value);//复核记录退回
            M()->execute('update tbn_data_check_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//复核文件失效
            M()->execute('update tbn_case_send set fstatus=0 where fstatus<>0 and fillegal_ad_id='.$value.' and fid in(select fid from (select max(fid) fid from tbn_case_send where fstatus<>0 and fillegal_ad_id='.$value.' ) a)');//分派记录重置
            M('tbn_illegal_ad')->where(['fid'=>$value])->save(['fstatus'=>0,'fstatus3'=>0,'fexamine'=>$selfresult,'fexaminetime'=>date('Y-m-d H:i:s')]);
          }else{
            M('tbn_illegal_ad')->where(['fid'=>$value])->save($data_tia);
          }
        }
      }
    }else{
      M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
    }

    if($selfresult == 20 && $isexamine == 20){
    	$data_tdc = [];
      $data_tdc['tcheck_createtime']  = date('Y-m-d H:i:s');
      $data_tdc['tregionid'] = substr(session('regulatorpersonInfo.regionid'),0,2);
    	foreach ($selfid as $key => $value) {
    		$data_tdc['fillegal_ad_id'] = $value;
        if($fregulatorlevel == 30){//国家级
	        $data_tdc['tgj_treid']     = session('regulatorpersonInfo.fregulatorpid');
	        $data_tdc['tgj_trename']   = session('regulatorpersonInfo.regulatorpname');
	        $data_tdc['tgj_username']  = session('regulatorpersonInfo.fname');
	        $data_tdc['tgj_time']      = date('Y-m-d H:i:s');
	        $data_tdc['tgj_result']    = $selnote;
	        $data_tdc['tlook_status']   = $fregulatorlevel;
	        $data_tdc['tkuayu_status']  = 10;
	        $data_tdc['tcheck_jingdu']  = 40;
	        $data_tdc['tcheck_status']  = 10;
	      }elseif($fregulatorlevel == 20){//省级
	        $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
	        $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
	        $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
	        $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
	        $data_tdc['tshengj_result']    = $selnote;
	        $data_tdc['tlook_status']   = $fregulatorlevel;
	        $data_tdc['tkuayu_status']  = 0;
	        $data_tdc['tcheck_jingdu']  = 30;
	        $data_tdc['tcheck_status']  = 10;
	      }elseif($fregulatorlevel == 10){//市级
	        $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
	        $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
	        $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
	        $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
	        $data_tdc['tshij_result']    = $selnote;
	        $data_tdc['tlook_status']   = $fregulatorlevel;
	        $data_tdc['tkuayu_status']  = 0;
	        $data_tdc['tcheck_jingdu']  = 20;
	        $data_tdc['tcheck_status']  = 10;
	      }else{//区级
	        $data_tdc['tquj_treid']     = session('regulatorpersonInfo.fregulatorpid');
	        $data_tdc['tquj_trename']   = session('regulatorpersonInfo.regulatorpname');
	        $data_tdc['tquj_username']  = session('regulatorpersonInfo.fname');
	        $data_tdc['tquj_time']      = date('Y-m-d H:i:s');
	        $data_tdc['tquj_result']    = $selnote;
	        $data_tdc['tlook_status']   = $fregulatorlevel;
	        $data_tdc['tkuayu_status']  = 0;
	        $data_tdc['tcheck_jingdu']  = 10;
	        $data_tdc['tcheck_status']  = 10;
	  		}
	  		$data_tdcs[] = $data_tdc;
    	}
  		M('tbn_data_check')->addAll($data_tdcs);

      $weihuphone = getconfig('fuhephone');
      $fhphone = S('fh'.$weihuphone);
      if(empty($fhphone)){
          S('fh'.$weihuphone,date('Y-m-d H:i:s'),43200);//半天一提醒
          $sms_return = A('Common/Alitongxin','Model')->usertask_sms($weihuphone,'复核任务');
      }
  	}

    D('Function')->write_log('违法广告线索审核',1,'操作完成>'.$selfid,'tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'操作完成'));
   
  }

}
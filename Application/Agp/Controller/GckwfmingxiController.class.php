<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;

import('Vendor.PHPExcel');

/**
 * 违法广告明细数据
 */

class GckwfmingxiController extends BaseController
{
  /**
   * by zw
   */
  public function index()
  {
    session_write_close();
    header("Content-type:text/html;charset=utf-8");
    ini_set('memory_limit','1024M');
    ini_set('max_execution_time', '120');//设置超时时间

    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }

    $system_num = getconfig('system_num');
    $iscontain        = I('iscontain');//是否包含下属地区
    $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含，2不包含，0未定义
    $area = I('area');

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    if($is_show_fsend==1){
      $is_show_fsend = -2;
    }else{
      $is_show_fsend = 0;
    }
    $where_time = gettimecondition($years,$timetypes,$timeval,'a.fissue_date',$is_show_fsend);
    $where_tia['_string'] = $where_time;

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['b.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['c.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['b.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['d.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['b.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['tor.fname'] = array('like','%'.$search_val.'%');
      }
    }

    if(!empty($netadtype)){
      $where_tia['b.fplatform']  = $netadtype;
    }

    if(( !empty($fad_class_code)) && empty($area)){
      if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel')==30){
        $area = '100000';
      }else{
        $area = session('regulatorpersonInfo.regionid');
      }
    }

    if(!empty($area)){//所属地区
      if($area != '100000'){
        if(!empty($iscontain)){
          $tregion_len = get_tregionlevel($area);
          if($tregion_len == 1){//国家级
            $where_tia['_string'] .= ' and b.fregion_id ='.$area;  
          }elseif($tregion_len == 2){//省级
            $where_tia['_string'] .= ' and b.fregion_id like "'.substr($area,0,2).'%"';
          }elseif($tregion_len == 4){//市级
            $where_tia['_string'] .= ' and b.fregion_id like "'.substr($area,0,4).'%"';
          }elseif($tregion_len == 6){//县级
            $where_tia['_string'] .= ' and b.fregion_id like "'.substr($area,0,6).'%"';
          }
        }else{
          $where_tia['b.fregion_id']  = $area;
        }
      }
    }

    //国家局系统只显示有打国家局标签媒体的数据
    if($system_num == '100000'){
      $wherestr = ' and e.flevel in (1,2,3)';
    }

    $where_tia['b.fcustomer']  = $system_num;
    
    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['b.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(d.fmediaclassid,2)'] = ['in',$media_class];
    }

    $count = M('tbn_illegal_ad_issue')
      ->alias('a')
      ->join('tbn_illegal_ad b on a.fillegal_ad_id = b.fid')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1 ')
      ->join('tregion e on b.fregion_id=e.fid')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on b.fid = snd.illad_id')
      ->where($where_tia)
      ->count();

    $do_tia = M('tbn_illegal_ad_issue')
      ->alias('a')
      ->field('a.fid,b.fad_name,c.ffullname,d.fmedianame,b.fmedia_class,a.fissue_date,b.fadowner,b.fillegal_code,b.fillegal,a.fstarttime,a.fendtime,(unix_timestamp(a.fendtime)-unix_timestamp(a.fstarttime)) as fad_lenth,fpage,ftarget_url,e.ffullname regionname,b.fexpressions')
      ->join('tbn_illegal_ad b on a.fillegal_ad_id = b.fid')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1 ')
      ->join('tregion e on b.fregion_id=e.fid')
      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on b.fid = snd.illad_id')
      ->where($where_tia)
      ->order('a.fissue_date desc')
      ->page($p,$pp)
      ->select();

    if(!empty($outtype)){
      if(empty($do_tia)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-违法广告明细';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '广告名称'=>'fad_name',
          '广告主'=>'fadowner',
          '地区'=>'regionname',
          '广告内容类别'=>'ffullname',
          '发布媒体'=>'fmedianame',
          '媒体类型'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网']
            ]
          ],
          '发布日期'=>'fissue_date',
          '开始时间'=>'fstarttime',
          '结束时间'=>'fendtime',
          '开始时间'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{fstarttime}']
            ]
          ],
          '结束时间'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{fendtime}']
            ]
          ],
          '时长/版面'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{fad_lenth}秒'],
              ['{fmedia_class} == 3','{fpage}'],
              ['{fmedia_class} == 13','{ftarget_url}']
            ]
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法内容'=>'fillegal',

        ];
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        D('Function')->write_log('违法广告清单',1,'导出成功');
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
      
    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
    }
  }

  /**
   * 获取监测数据详情
   * by zw
   */
  public function getmonitoringview()
  {
    $system_num = getconfig('system_num');
    $use_open_search = getconfig('use_open_search');
    $sid    = I('sid')?I('sid'):29622689;//违法广告发布记录ID
    if(empty($sid)){
      $this->error('参数缺失!');
    }

    $where['a.fid'] = $sid;
    $data = M('tbn_illegal_ad_issue')
      ->alias('a')
      ->field('a.fid as tid,b.fad_name as fadname,c.ffullname as fadclass, (case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,d.fid, fissue_date as fissuedate, a.fstarttime as fstarttime2, a.fendtime as fendtime2, a.fstarttime, a.fendtime,a.fmedia_class,fsample_id,e.fname as fmediaownername,fpage,ftarget_url')
      ->join('tbn_illegal_ad b on a.fillegal_ad_id = b.fid')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on a.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')
      ->join('tmediaowner e on d.fmediaownerid=e.fid','left')
      ->where($where)
      ->find();
    $data['fillegaltype'] = '违法';
    if(!empty($use_open_search)){
      $tb_tvsam = 'ttvsample_'.$system_num;
      $tb_bcsam = 'tbcsample_'.$system_num;
      $tb_papersam = 'tpapersample_'.$system_num;
      $tb_netsam = 'tnetissue';
    }else{
      $tb_tvsam = 'ttvsample';
      $tb_bcsam = 'tbcsample';
      $tb_papersam = 'tpapersample';
      $tb_netsam = 'tnetissue';
    }
    if($data['fmedia_class'] == 1 || $data['fmedia_class'] == 2){
      $data['issueurl'] = A('Common/Media','Model')->get_m3u8($data['fid'],strtotime($data['fstarttime']),strtotime($data['fendtime']));
      $data['fstarttime'] = date('H:i:s',strtotime($data['fstarttime']));
      $data['fendtime'] = date('H:i:s',strtotime($data['fendtime']));
    }

    if($data['fmedia_class'] == 1){
      $where_sam['a.fid'] = $data['fsample_id'];
      $do_sma = M($tb_tvsam)
        ->alias('a')
        ->field('tad.fbrand, ifnull(tadowner.fname,"广告主不详") as fadowner_name, fversion,"tv" as mclass, fadlen, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone , "" as fsize, favifilepng,favifilename')
        ->join('tad on a.fadid=tad.fadid and tad.fadid<>0')
        ->join('tadclass on tad.fadclasscode=tadclass.fcode')
        ->join('tadowner on tad.fadowner=tadowner.fid ')
        ->where($where_sam)
        ->find();
      if(!empty($do_sma)){
        $data = array_merge_recursive($data,$do_sma);
      }
      $where2['source_type'] = 10;
      $where2['source_tid'] = $data['tid'];
      $where2['source_mediaclass'] = 'w01';
      $data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
      if(!empty($data2)){
        $data['source_url'] = $data2['source_url'];
        $data['source_id']  = $data2['source_id'];
        $data['source_state'] = $data2['source_state'];
      }
    }elseif($data['fmedia_class'] == 2){
      $where_sam['a.fid'] = $data['fsample_id'];
      $do_sma = M($tb_bcsam)
        ->alias('a')
        ->field('tad.fbrand, ifnull(tadowner.fname,"广告主不详") as fadowner_name, fversion,"bc" as mclass, fadlen, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone , "" as fsize, favifilepng,favifilename')
        ->join('tad on a.fadid=tad.fadid and tad.fadid<>0')
        ->join('tadclass on tad.fadclasscode=tadclass.fcode')
        ->join('tadowner on tad.fadowner=tadowner.fid ')
        ->where($where_sam)
        ->find();
      if(!empty($do_sma)){
        $data = array_merge_recursive($data,$do_sma);
      }

      $where2['source_type'] = 10;
      $where2['source_tid'] = $data['tid'];
      $where2['source_mediaclass'] = 'w02';
      $data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
      if(!empty($data2)){
        $data['source_url'] = $data2['source_url'];
        $data['source_id']  = $data2['source_id'];
        $data['source_state'] = $data2['source_state'];
      }
    }elseif($data['fmedia_class'] == 3){
      $where_sam['a.fpapersampleid'] = $data['fsample_id'];
      $do_sma = M($tb_papersam)
        ->alias('a')
        ->field('tad.fbrand, ifnull(tadowner.fname,"广告主不详") as fadowner_name, fversion,"paper" as mclass, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone , a.fjpgfilename as favifilepng,a.fjpgfilename as favifilename')
        ->join('tad on a.fadid=tad.fadid and tad.fadid<>0')
        ->join('tadclass on tad.fadclasscode=tadclass.fcode')
        ->join('tadowner on tad.fadowner=tadowner.fid ')
        ->where($where_sam)
        ->find();
      if(!empty($do_sma)){
        $data = array_merge_recursive($data,$do_sma);
      }
    }elseif($data['fmedia_class'] == 13){
      $where_sam['a.major_key'] = $data['fsample_id'];
      $do_sma = M($tb_netsam)
        ->alias('a')
        ->field('ifnull(tadowner.fname,"广告主不详") as fadowner_name, "net" as mclass, a.major_key,a.net_id,a.net_url,a.net_old_url,a.thumb_url_true,a.net_platform,a.net_original_url,a.net_target_url,a.net_created_date,a.net_advertiser,a.fmediaid,a.net_snapshot,a.fbrand,a.fadowner,a.fadclasscode,a.fadclasscode_v2,a.fspokesman,a.fapprovalunit,a.fillegaltypecode,a.fillegalcontent,a.fexpressioncodes,a.fexpressions,a.fconfirmations,a.fadmanuno,a.fmanuno,a.fadapprno,a.fapprno,a.fadent,a.fent,a.fentzone,a.ftype')
        ->join('tadclass on a.fadclasscode=tadclass.fcode')
        ->join('tadowner on a.fadowner=tadowner.fid ','left')
        ->where($where_sam)
        ->find();
      if(!empty($do_sma)){
        $data = array_merge_recursive($data,$do_sma);
      }
    }
    $data['fillegalcontent'] = htmlspecialchars_decode($data['fillegalcontent']);
    
    if(!empty($data)){
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'无数据','data'=>$data));
    }
  }

}
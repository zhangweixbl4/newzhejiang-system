<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 常见问题
 * by zw
 */

class CommonquestionController extends BaseController{

  /**
 * 问题列表
 * by zw
 */
  public function index1(){
      session_write_close();
    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $ftitle     = I('ftitle');// 标题
    $system_num = getconfig('system_num');

    if (!empty($ftitle)) {
      $where_tia['cqn_title'] = array('like', '%' . $ftitle . '%');//标题
    }

    if($system_num == '100000'){
      $where_tia['cqn_type'] = 10;
    }else{
      $where_tia['cqn_type'] = 0;
    }

    $where_tia['_string'] = '(cqn_treid=0 or cqn_treid='.session('regulatorpersonInfo.fregulatorpid').') and (cqn_level=-1 or cqn_level='.session('regulatorpersonInfo.fregulatorlevel').')';

    $count = M('common_question')
      ->alias('a')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('common_question')
      ->alias('a')
      ->field('cqn_title,cqn_content,cqn_recontent')
      ->where($where_tia)
      ->order('a.cqn_id desc')
      ->page($p,$pp)
      ->select();
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));

  }

  public function index(){
        session_write_close();
        $system_num = getconfig('system_num');
        $p = I('page',1);
        $keyword = trim(I('keyword'));
        $user = session('regulatorpersonInfo');//fregulatorid机构ID

        $level = M('tregion')->where(['fid'=>$user['regionid']])->getField('flevel');
        $map['cqn_fcustomer'] = array(array('eq',$system_num),array('eq','-1'), 'or');
        if($system_num != '100000'){
            $map['cqn_level'] = [['like','%|'.$level.'|%'],['eq','|-1|'],'or'];
        }
        $map['cqn_deleteuserid'] = ['EQ',0];
        if($keyword != ''){
            $map['cqn_title|cqn_content'] = ['like','%'.$keyword.'%'];
        }
        $data = M('common_question')
            ->where($map)
            ->page($p.',20')
            ->order('cqn_createtime desc')
            ->select();
        $count = M('common_question')->where($map)->count();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));

    }

}
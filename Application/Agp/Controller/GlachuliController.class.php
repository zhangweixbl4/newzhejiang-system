<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局立案处理
 * by zw
 */

class GlachuliController extends BaseController{
  /**
  * 处理结果列表
  * by zw
  */
  public function lajieguo_list(){
    Check_QuanXian(['lachuli']);
    session_write_close();
    $system_num = getconfig('system_num');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $outtype = I('outtype');//导出类型
    $selfid = I('selfid');//勾选项
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
    //勾选导出
    if(!empty($outtype) && !empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }
    $where_tia['a.fstatus']   = 10;
    $where_tia['a.fresult']   = array('like','%立案%');
    $where_tia['a.fstatus2']  = array('in',array(15,17));
    $where_tia['a.fcustomer']  = $system_num;

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 '.$wheres)
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 10 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name, (case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,ifnull(datediff(now(),a.fresult_datetime),0) as ladiffday,a.fadowner as fadownername,tn.ffullname,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,ifnull(db.dbcount,0) dbcount')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 '.$wheres)
      ->join('tregion tn on tn.fid=a.fregion_id')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 10 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
      ->join('(select count(*) dbcount,fillegal_ad_id from tbn_illegal_ad_attach where fstate = 0 and ftype = 30 group by fillegal_ad_id) db on a.fid = db.fillegal_ad_id','left')
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();
 
    if(!empty($outtype)){
        if(empty($do_tia)){
          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }

        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-立案调查处理列表';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'ffullname',
          '发布媒体'=>'fmedianame',
          '媒体分类'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网'],
            ]
          ],
          '广告类别'=>'fadclass',
          '广告名称'=>'fad_name',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '查看状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fview_status} == 0','未查看'],
              ['{fview_status} == 10','已查看'],
            ]
          ],
          '处理状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus} == 0','未处理'],
              ['{fstatus} == 10','已上报'],
              ['{fstatus} == 30','数据复核'],
            ]
          ],
          '处理结果'=>'fresult',
          '处理时间'=>'fresult_datetime',
          '处理机构'=>'fresult_unit',
          '处理人'=>'fresult_user',
          '立案状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus2} == 15','待处理'],
              ['{fstatus2} == 16','已处理'],
              ['{fstatus2} == 17','处理中'],
              ['{fstatus2} == 20','撤消'],
            ]
          ],
          '复核状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus3} == 10','复核中'],
              ['{fstatus3} == 20','复核失败'],
              ['{fstatus3} == 30','复核通过'],
            ]
          ],
        ];
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        D('Function')->write_log('立案调查处理列表',1,'导出成功');
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

      }else{
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
      }
  }

  /**
  * 上报立案处理结果
  * by zw
  */
  public function sczhengju(){
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $submit     = I('submit');//保存是1，否则为空
    $system_num = getconfig('system_num');
    $islafile   = getconfig('islafile');
    $attach     = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo) && !empty($islafile)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
    }
    $where_tia['fstatus']   = 10;
    $where_tia['fstatus2']  = array('in',array(15,17));
    $where_tia['fresult']   = array('like','%立案%');
    $where_tia['fcustomer']  = $system_num;
    foreach ($selfid as $key => $value) {
      $where_tia['fid']   = $value;
      $do_tia = M('tbn_illegal_ad')->field('fstatus2')->where($where_tia)->find();
      if(!empty($do_tia)){
        $data_tia['flatime'] = date('Y-m-d H:i:s');//立案处理时间
        if(empty($submit)){
          $data_tia['fstatus2']   = 16;//已处理
        }
        M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
        //上传附件
        $attach_data['ftype']  = 20;//类型
        $attach_data['fillegal_ad_id']  = $value;//违法广告
        $attach_data['fuploader']       = session('regulatorpersonInfo.fid');//上传人
        $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattachname'];
          $attach_data['fattach_url'] = $value2['fattachurl'];
          array_push($attach,$attach_data);
        }
      }
    }
    if(!empty($attach)){
      M('tbn_illegal_ad_attach')->addAll($attach);
    }
    D('Function')->write_log('立案调查处理',1,'操作成功','tbn_illegal_ad',0);
    $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
  }

  /**
  * 查看案件证据
  * by zw
  */
  public function fjjieguo(){
    $system_num = getconfig('system_num');

    $fid = I('fid');//违法广告ID
    $where_tia['a.fillegal_ad_id'] = $fid;
    $where_tia['a.fstate']         = 0;
    $where_tia['a.ftype']         = ['in',[0,10,20]];

    //证据附件
    $do_tia = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')->where($where_tia)->order('a.fid asc')->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','zjfjdata'=>$do_tia,'lafjdata'=>$do_tia));
  }

  /**
  * 撤消立案处理结果
  * by zw
  */
  public function cxjieguo(){
    $system_num = getconfig('system_num');
    $selfid     = I('fid');//违法广告ID组
    $where_tia['fstatus']   = 10;
    $where_tia['fstatus2']  = 17;
    $where_tia['fresult']   = array('like','%立案%');
    $data_tia['fstatus']        = 0;
    $data_tia['fstatus2']       = 0;
    $data_tia['fstatus3']       = 0;
    $data_tia['fview_status']   = 0;
    $data_tia['fresult']        = '';
    $data_tia['flatime']        = null;
    $data_tia['fresult_datetime']  = null;
    $data_tia['fresult_unit']   = '';
    $data_tia['fresult_user']   = '';
    
    foreach ($selfid as $key => $value) {
      $do_send = M('tbn_case_send')
        ->where('fillegal_ad_id = '.$value)
        ->find();

      $where_tia['fid'] = $value;
      $where_tia['fcustomer']  = $system_num;
      $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);//更改状态
      if(!empty($do_tia)){
        M()->execute('update tbn_illegal_ad_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);
        M()->execute('update tbn_data_check_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);
        M()->execute('update tbn_case_send set fstatus=0 where fstatus<>0 and fillegal_ad_id='.$value.' and fid in(select fid from (select max(fid) fid from tbn_case_send where fstatus<>0 and fillegal_ad_id='.$value.' ) a)');
      }
    }
    if(!empty($do_tia)){
      D('Function')->write_log('立案调查处理',1,'撤消成功','tbn_illegal_ad',$do_tia);
      $this->ajaxReturn(array('code'=>0,'msg'=>'撤消成功'));
    }else{
      D('Function')->write_log('立案调查处理',0,'撤消失败','tbn_illegal_ad',0,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>1,'msg'=>'撤消失败'));
    }
  }


}
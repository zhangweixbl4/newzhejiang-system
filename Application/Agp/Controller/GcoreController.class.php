<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局公共API接口
 * by zw
 */

class GcoreController extends BaseController {

	/**
	 * 按媒体类别获取用户媒介，不含已交办的媒体
	 * by zw
	 */
	public function user_medialist(){
		$system_num = getconfig('system_num');
		$mclass = I('mclass');//媒体类型

		if($mclass == 1){
			$where['a.fmediaclassid'] = array('like','01%');
		}elseif($mclass == 2){
			$where['a.fmediaclassid'] = array('like','02%');
		}elseif($mclass == 3){
			$where['a.fmediaclassid'] = array('like','03%');
		}elseif($mclass == 4){
			$where['a.fmediaclassid'] = array('like','13%');
		}
		$where['b.ftype'] = 1;
		$where['a.fstate'] = 1;
		$where['b.fcustomer'] = $system_num;
		$where['b.fuserid'] = session('regulatorpersonInfo.fid');
		$where['_string'] = 'a.fid not in (select fmedia_id from tbn_media_grant where fgrant_reg_id='.session('regulatorpersonInfo.fregulatorpid').')';
		$do_ma = M('tmedia')
			->alias('a')
			->field('a.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
			->join('tmedia_temp b on a.fid = b.fmediaid')
			->where($where)
			->select();

		if(!empty($do_ma)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ma));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无媒介信息'));
		}
	}

	/**
	 * 按媒体类别和行政区划获取媒体列表
	 * by zw
	 */
	public function user_medialist2(){
		$system_num = getconfig('system_num');
		$mclass = I('mclass');//媒体类别组
		$tregionids  = I('tregionids');//行政区划ID
		$searchtype = I('searchtype',0);//搜索类型
		
		if(!empty($mclass)){
			$where['left(fmediaclassid,2)'] = array('in',$mclass);
		}

		if(!empty($searchtype)){
			if(!empty($tregionids)){
				$where['c.fregionid'] 	= $tregionids;
			}

			$system_num = getconfig('system_num');
            $where['a.fcustomer'] = $system_num;
			$where['a.fgrant_reg_id'] 	= session('regulatorpersonInfo.fregulatorpid');
			$do_ma = M('tbn_media_grant')
				->alias('a')
				->field('b.fid, (case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) as fmedianame')
				->join('tmedia b on b.fid=a.fmedia_id and b.fid=b.main_media_id and b.fstate = 1 ')
				->join('tmedia_temp ttp on b.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tregulator c on a.freg_id=c.fid')
				->where($where)
				->order('left(b.fmediaclassid,2) asc,c.fregionid asc,b.fmedianame asc')
				->select();
		}else{
			$where['a.fstate'] = 1;
			$do_ma = M('tmedia')
				->alias('a')
				->field('a.fid, (case when instr(a.fmedianame,"（") > 0 then left(a.fmedianame,instr(a.fmedianame,"（") -1) else a.fmedianame end) as fmedianame')
				->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->where($where)
				->order('left(fmediaclassid,2) asc,a.media_region_id asc,a.fmedianame asc')
				->select();
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ma));
		
	}

	/**
	 * 通过行政区划获取机构
	 * by zw
	 */
	public function tregion_tregulator(){
		$tregionid = I('tregionid');
		$where_tr['fregionid'] = $tregionid;
		$where_tr['_string'] = 'fstate=1 and ftype=20 and fkind=1';
		$do_tr = M('tregulator')->field('fid,fname')->where($where_tr)->find();
		if(!empty($do_tr)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_tr));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'该区域还未建立相应机构'));
		}
	}


    /**
     * 按媒体类别获取当前用户可交办案件的媒介
     * by yjn
     */
    public function user_medialist_for_grant(){
        $system_num = getconfig('system_num');
        $mclass = I('mclass',1);//媒体类型
        $grant_medias = M('tbn_media_grant')->where(['fgrant_reg_id' => session('regulatorpersonInfo.fregulatorpid'),'fcustomer'=>100000])->getField('fmedia_id',true);
        //显示非备用媒体的数据
        $where['_string'] = 'b.fid=b.main_media_id';

        if($mclass == 1){
            $where['b.fmediaclassid'] = array('like','01%');
        }elseif($mclass == 2){
            $where['b.fmediaclassid'] = array('like','02%');
        }elseif($mclass == 3){
            $where['b.fmediaclassid'] = array('like','03%');
        }elseif($mclass == 13){
            $where['b.fmediaclassid'] = array('like','13%');
        }

        $where['a.ftype'] = ['IN',[0,1]];
        $where['a.fcustomer'] = $system_num;
        $where['a.fstate'] = 1;
        $where['a.fuserid'] = session('regulatorpersonInfo.fid');
        if(!empty($grant_medias)){
	        $where['a.fmediaid'] = ['notin',$grant_medias];
        }
        $do_ma = M('tmedia_temp')
        	->alias('a')
        	->field('b.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
        	->join('tmedia b on a.fmediaid = b.fid and b.fstate = 1')
            ->where($where)
            ->group('b.fid')
            ->select();
        if(!empty($do_ma)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ma));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'无媒介信息'));
        }
    }

}
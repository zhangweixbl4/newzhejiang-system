<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

class IndexController extends BaseController
{

	public function ghome()
    {
    	session_write_close();
    	Check_QuanXian('ghome');
    	$system_num = getconfig('system_num');
    	$ischeck = getconfig('ischeck');

    	$mclasss = ['01','02','03','04','05','13'];
    	foreach ($mclasss as $value) {
			$data[$value]['count'] = 0;//媒体数量
			$data[$value]['isxinyongcount'] = 0;//信用评价数量
			$data[$value]['notxinyongcount'] = 0;//非信用评价数量
    	}

		$do_ta = M('tmedia')
			->master(true)
			->alias('a')
			->field('fmediaclassid,a.fisxinyong')
			->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fstate = 1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->select();
		foreach ($do_ta as $key => $value) {
			$mediatp = substr($value['fmediaclassid'],0,2);
			$data[$mediatp]['count'] += 1;
			if(!empty($value['fisxinyong'])){
				$data[$mediatp]['isxinyongcount'] += 1;
			}else{
				$data[$mediatp]['notxinyongcount'] += 1;
			}
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
    }

	/**
	* 获取首页的统计数据
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function getindexdata(){
		session_write_close();
		Check_QuanXian('home');
		$isillegal_summary = getconfig('isillegal_summary');
		$system_num = getconfig('system_num');
        $media_class = json_decode(getconfig('media_class'));
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    	$media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

		$where_ta['fmediaownerid'] 	= array('neq',0);
		$where_ta['_string'] 		= 'tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1';
		$do_ta = M('tmedia')
			->master(true)
			->field('fmediaclassid')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fstate = 1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where_ta)
			->count();

		$data['mediacount'] 	= $do_ta;//媒体数量

		$daytime = date('Y-m-d');
		$tb_tvissue_nowmonth = gettable('01',strtotime($daytime));//电视本月
        $tb_bcissue_nowmonth = gettable('02',strtotime($daytime));//广播本月
        $tb_paperissue_nowmonth = gettable('03',strtotime($daytime));//广播本月

        $till_tvcount = 0;//电视总违法
        $till_bccount = 0;//广播总违法
        $till_papercount = 0;//报纸总违法
        $data_arr = [];//七天数据集

        //电视近7天违法量和总量
        $tb_tvsql = "select a.fissuedate,sum(fquantity) as tecount,sum(case when b.fillegaltypecode>0 then 1 else 0 end) as tillcount
        	from $tb_tvissue_nowmonth a
        	inner join ttvsample b on b.fid = a.fsampleid and b.fstate in(0,1)
        	inner join tmedia f on a.fmediaid=f.fid and f.main_media_id=f.fid and f.fstate = 1
        	inner join tad c on b.fadid=c.fadid and c.fadid<>0
        	inner join tmediaissue me on a.fissuedate = me.fissue_date and a.fmediaid = me.fmedia_id
			inner join ttask tk on me.fid = tk.fbiz_main_id and tk.fbiz_def_id = 1 and tk.fstatus = 5 
			inner join (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = '".$system_num."' and fuserid=".session('regulatorpersonInfo.fid')." ) as ttp on f.fid=ttp.fmediaid 
        	where a.fissuedate between UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and  UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY))
        	group by a.fissuedate
         ";
        $tvdata = M()->query($tb_tvsql);
        foreach ($tvdata as $key => $value) {
        	if(!empty($value)){
        		$data['tetvcount'] += $value['tecount'];
        	}
        }
        $data_arr[] = $tvdata;

        //广播近7天违法量和总量
        $tb_bcsql = "select a.fissuedate,sum(fquantity) as tecount,sum(case when b.fillegaltypecode>0 then 1 else 0 end) as tillcount
        	from $tb_bcissue_nowmonth a
        	inner join ttvsample b on b.fid = a.fsampleid and b.fstate in(0,1)
        	inner join tmedia f on a.fmediaid=f.fid and f.main_media_id=f.fid and f.fstate = 1
        	inner join tad c on b.fadid=c.fadid and c.fadid<>0
        	inner join tmediaissue me on a.fissuedate = me.fissue_date and a.fmediaid = me.fmedia_id
			inner join ttask tk on me.fid = tk.fbiz_main_id and tk.fbiz_def_id = 1 and tk.fstatus = 5 
			inner join (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = '".$system_num."' and fuserid=".session('regulatorpersonInfo.fid')." ) as ttp on f.fid=ttp.fmediaid 
        	where a.fissuedate between UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and  UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY))
        	group by a.fissuedate
         ";
        $bcdata = M()->query($tb_bcsql);
        foreach ($bcdata as $key => $value) {
        	if(!empty($value)){
        		$data['tebccount'] += $value['tecount'];
        	}
        }
        $data_arr[] = $bcdata;

        //报纸近7天违法量和总量
        $tb_papersql = "select a.fissuedate,sum(fquantity) as tecount,sum(case when b.fillegaltypecode>0 then 1 else 0 end) as tillcount
        	from $tb_paperissue_nowmonth a
        	inner join ttvsample b on b.fid = a.fsampleid and b.fstate in(0,1)
        	inner join tmedia f on a.fmediaid=f.fid and f.main_media_id=f.fid and f.fstate = 1
        	inner join tad c on b.fadid=c.fadid and c.fadid<>0
        	inner join tmediaissue me on a.fissuedate = me.fissue_date and a.fmediaid = me.fmedia_id
			inner join ttask tk on me.fid = tk.fbiz_main_id and tk.fbiz_def_id = 1 and tk.fstatus = 5 
			inner join (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = '".$system_num."' and fuserid=".session('regulatorpersonInfo.fid')." ) as ttp on f.fid=ttp.fmediaid 
        	where a.fissuedate between UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and  UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 1 DAY))
        	group by a.fissuedate
         ";
        $paperdata = M()->query($tb_papersql);
        foreach ($paperdata as $key => $value) {
        	if(!empty($value)){
        		$data['tepapercount'] += $value['tecount'];
        	}
        }
        $data_arr[] = $paperdata;

		$data['issuecount'] = 0;
		$timedata = get_weeks();
		foreach ($timedata as $key => $value) {
			$data['sevendaydata'][$key]['nowdate'] = $value;//近7天时间
			$data['sevendaydata'][$key]['nowcount'] = 0;//近7天每天发布量
			$data['sevendaydata'][$key]['nowcount2'] = 0;//近7天每天违法量
			foreach ($data_arr as $key3 => $value3) {
				foreach ($value3 as $key2 => $value2) {
					if($value2['fissuedate'] == $value){
						if(!empty($value2['tecount'])){
							$data['sevendaydata'][$key]['nowcount'] += $value2['tecount'];
							$data['issuecount'] += $value2['tecount'];
							$data['sevendaydata'][$key]['nowcount2'] += $value2['tillcount'];
						}
					}
				}
			}
		}

		$wherestr2 = ' and a.fissuedate between "'.date('Y-m-01').'" and "'.date('Y-m-d').'"';
		//电视当月违法总量
        $tb_tvsql2 = "select sum(fquantity) as tecount
        	from $tb_tvissue_nowmonth a
        	inner join ttvsample b on b.fid = a.fsampleid and b.fstate in(0,1)
        	inner join tmedia f on a.fmediaid=f.fid and f.main_media_id=f.fid and f.fstate = 1
        	inner join tad c on b.fadid=c.fadid and c.fadid<>0
        	inner join tmediaissue me on a.fissuedate = me.fissue_date and a.fmediaid = me.fmedia_id
			inner join ttask tk on me.fid = tk.fbiz_main_id and tk.fbiz_def_id = 1 and tk.fstatus = 5 
			inner join (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = '".$system_num."' and fuserid=".session('regulatorpersonInfo.fid')." ) as ttp on f.fid=ttp.fmediaid 
        	where b.fillegaltypecode > 0 $wherestr2
         ";
        $tvdata2 = M()->query($tb_tvsql2);
        $till_tvcount = $tvdata2[0]['tecount'];
        //广播当月违法总量
        $tb_bcsql2 = "select sum(fquantity) as tecount
        	from $tb_bcissue_nowmonth a
        	inner join tbcsample b on b.fid = a.fsampleid and b.fstate in(0,1)
        	inner join tmedia f on a.fmediaid=f.fid and f.main_media_id=f.fid and f.fstate = 1
        	inner join tad c on b.fadid=c.fadid and c.fadid<>0
        	inner join tmediaissue me on a.fissuedate = me.fissue_date and a.fmediaid = me.fmedia_id
			inner join ttask tk on me.fid = tk.fbiz_main_id and tk.fbiz_def_id = 1 and tk.fstatus = 5 
			inner join (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = '".$system_num."' and fuserid=".session('regulatorpersonInfo.fid')." ) as ttp on f.fid=ttp.fmediaid 
        	where b.fillegaltypecode > 0 $wherestr2
         ";
        $bcdata2 = M()->query($tb_bcsql2);
        $till_bccount = $bcdata2[0]['tecount'];
        //报纸当月的违法总量
        $tb_papersql2 = "select sum(fquantity) as tecount
        	from $tb_paperissue_nowmonth a
        	inner join ttvsample b on b.fid = a.fsampleid and b.fstate in(0,1)
        	inner join tmedia f on a.fmediaid=f.fid and f.main_media_id=f.fid and f.fstate = 1
        	inner join tad c on b.fadid=c.fadid and c.fadid<>0
        	inner join tmediaissue me on a.fissuedate = me.fissue_date and a.fmediaid = me.fmedia_id
			inner join ttask tk on me.fid = tk.fbiz_main_id and tk.fbiz_def_id = 1 and tk.fstatus = 5 
			inner join (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = '".$system_num."' and fuserid=".session('regulatorpersonInfo.fid')." ) as ttp on f.fid=ttp.fmediaid 
        	where b.fillegaltypecode > 0 $wherestr2
         ";
        $paperdata2 = M()->query($tb_papersql2);
        $till_papercount = $paperdata2[0]['tecount'];
       
        $data['tillegaladcount'] = $till_tvcount+$till_bccount+$till_papercount+$till_netcount;//违法量总数
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	* 获取媒体列表
	 * by zw
	 */
	public function get_medialist(){
		Check_QuanXian(['home','ghome']);
		$system_num = getconfig('system_num');

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录

		$where_ta['_string'] = '1=1';
		$mclass 	= I('mclass');//媒体类型
		$netadtype  = I('netadtype');//平台类别
		$medianame	= I('medianame');//媒体名称
		$fmedia_group 	= I('fmedia_group');//集团名称
		$regionid 	= I('regionid');//区划ID
		$status 	= I('status');//状态
		$fisxinyong = I('fisxinyong') == ''?-1:I('fisxinyong');//是否信用评价，-1全部默认，0否，1是
		if(!empty($mclass)){
			$where_ta['_string'] .= ' and left(fmediaclassid,2)="'.$mclass.'"';
		}
		if(!empty($netadtype)){
			if($netadtype==1){
				$where_ta['_string'] .= ' and left(fmediaclassid,4)="1301"';
			}elseif($netadtype==2){
				$where_ta['_string'] .= ' and left(fmediaclassid,4)="1302"';
			}elseif($netadtype==9){
				$where_ta['_string'] .= ' and left(fmediaclassid,4)="1303"';
			}
		}

		if(!empty($medianame)){
			$where_ta['tmedia.fmedianame'] = array('like','%'.$medianame.'%');
		}
		if(!empty($fmedia_group)){
			$where_ta['tmedia.fmedia_group'] = array('like','%'.$fmedia_group.'%');
		}
		if(!empty($regionid)){
			$where_ta['tmedia.media_region_id'] = $regionid;
		}
		$where_ta['tmedia.fmediaownerid'] = array('neq',0);
		if($status == 20){
			$where_ta['ttp.fstate'] = 0;
		}else{
			$where_ta['ttp.fstate'] = 1;
		}
		if($fisxinyong == 0){
			$where_ta['fisxinyong'] = 0;
		}elseif($fisxinyong == 1){
			$where_ta['fisxinyong'] = 1;
		}
		$where_ta['_string'] .= ' and tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1';
		$count = M('tmedia')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tregion on tmedia.media_region_id=tregion.fid')
			->where($where_ta)
			->count();// 查询满足要求的总记录数

		$data_ta = M('tmedia')
			->field('tmedia.fid,tmedia.fmediacode,tmedia.fdpid,(case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,tmedia.fcollecttype,tregion.ffullname,(case when left(fmediaclassid,4)="1301" then "PC端" when left(fmediaclassid,4)="1302" then "移动端" when left(fmediaclassid,4)="1303" then "微信端" end) as mediaclassname,left(fmediaclassid,2) mclass,fisxinyong,replace(replace(fmedia_group,"GD",""),"BZ","") fmedia_group')
			->join('tmedia_temp ttp on tmedia.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->join('tregion on tmedia.media_region_id=tregion.fid')
			->where($where_ta)
			->order('tmedia.media_region_id asc,CONVERT(tmedia.fmedianame using gbk) asc')
			->page($p,$pp)
			->select();//查询复核数据
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data_ta)));

	}

	//获取待办任务
	public function getwaittask(){
		session_write_close();
		$gettypes = I('gettypes');//ghome 默认,home
		$system_num = getconfig('system_num');
		$isrelease = getconfig('isrelease');
    	$systemtips = getconfig('systemtips');
        $isexamine = getconfig('isexamine');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    	$media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    	$taskdata = [];
    	$menujurisdiction = S(session('regulatorpersonInfo.fcode').'_menujurisdiction');
    	if(!empty($gettypes)){
			$taskdata[] = $gettypes;
    	}else{
    		$taskdata = json_decode($systemtips);
    	}

    	$where_childtask = '1=1';
		if(!empty($isrelease) || $system_num == '100000'){
			$where_childtask   .= ' and fsend_status=2';
		}

		//案件待办数量
		$tkname = 'sbchuli|1';
		if((in_array($tkname,$taskdata) || in_array(explode('|',$tkname)[0],$taskdata)) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['a.fstatus']   = 0;
		    $where_task['a.fcustomer']  = $system_num;

    		if($isexamine == 10 || $isexamine == 20){
		      $where_task['a.fexamine'] = 10;
		    }
		    if(!empty($illDistinguishPlatfo)){
		      $where_task['left(c.fmediaclassid,2)'] = ['in',$media_class];
		    }

		    $count_task = M('tbn_illegal_ad')
		      ->master(true)
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
		      ->join('tregion tn on tn.fid=a.fregion_id')
		      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_childtask.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 0 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
		      ->where($where_task)
		      ->count();
			D('Function')->update_task($tkname,$count_task.'条线索任务待办',$count_task);
		}

		//案件接收待办数量
		$tkname = 'sbchuli|2';
		if((in_array($tkname,$taskdata) || in_array(explode('|',$tkname)[0],$taskdata)) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['a.fstatus']   = 0;
		    $where_task['a.fcustomer']  = $system_num;

		    if($isexamine == 10 || $isexamine == 20){
		      $where_task['a.fexamine'] = 10;
		    }
		    if(!empty($illDistinguishPlatfo)){
		      $where_task['left(c.fmediaclassid,2)'] = ['in',$media_class];
		    }

		    $count_task = M('tbn_illegal_ad')
		      ->master(true)
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
		      ->join('tregion tn on tn.fid=a.fregion_id')
		      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_childtask.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 0 and d.fsendtype = 1 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
		      ->join($jointor)
		      ->where($where_task)
		      ->count();


			D('Function')->update_task($tkname,$count_task.'条线索接收任务待办',$count_task);
		}

		//立案调查待办数量
		$tkname = 'lachuli|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if(!empty($illDistinguishPlatfo)){
		      $where_task['left(c.fmediaclassid,2)'] = ['in',$media_class];
		    }
			$where_task['a.fstatus']   = 10;
			$where_task['a.fstatus2']   = array('in',array(15,17));
		    $where_task['a.fresult']   = array('like','%立案%');
		    $where_task['a.fcustomer']  = $system_num;
			$count_task = M('tbn_illegal_ad')
			  ->master(true)
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
		      ->join('tregion tn on tn.fid=a.fregion_id')
		      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_childtask.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
		      ->join('tbn_case_send d on d.fillegal_ad_id=a.fid and d.fstatus = 10 and d.frece_reg_id='.session('regulatorpersonInfo.fregulatorpid'))
		      ->where($where_task)
		      ->count();
		    D('Function')->update_task($tkname,$count_task.'条立案调查处理任务待办',$count_task);
		}

		//线索登记待办数量
		$tkname = 'fjcluelist|1';
		if((in_array($tkname,$taskdata) || in_array(explode('|',$tkname)[0],$taskdata)) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if(!empty($illDistinguishPlatfo)){
		      $where_task['left(c.fmediaclassid,2)'] = ['in',$media_class];
		    }
			$where_task['a.fstatus']   = 0;
			$where_task['a.fregisterid'] 	= 0;
		    $where_task['a.fcustomer']  = $system_num;

		    $count_task = M('tbn_illegal_ad')
				->alias('a')
				->join('tmedia d on  a.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')//媒介信息
				->join('tmedia_temp ttp on d.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
				->join('tadclass e on a.fad_class_code=e.fcode ')//广告内容列别
				->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime from tbn_illegal_ad_issue,tbn_illegal_ad where tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
				->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条线索登记任务待办',$count_task);
		}

		 //数据复核数量
		$tkname = 'ginspectresult2|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['fcustomer'] = $system_num;
			$where_task['fcheckorg'] = session('regulatorpersonInfo.regionid');
			$where_task['fstate'] = array('in',array(0,2));
			$count_task = M('tbn_data_check_plan')
			  ->master(true)
			  ->where($where_task)
			  ->count();// 查询满足要求的总记录数
	        D('Function')->update_task($tkname,$count_task.'条数据复核任务待办',$count_task);
	    }

		//数据复核待办数量
		$tkname = 'sjfuhe|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if(!empty($illDistinguishPlatfo)){
		      $where_task['left(d.fmediaclassid,2)'] = ['in',$media_class];
		    }
		    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
		    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
		    if($fregulatorlevel == 30){
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }else{
		      $where_task['a.tregionid'] = $regionid;//区划
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }

		    $where_task['a.tcheck_status'] = 0;//处理状态
		    $where_task['a.tkuayu_status'] = 0;//是否跨域
		    $where_task['b.fstatus'] = 30;
		    $where_task['b.fstatus3'] = 10;

		    $count_task = M('tbn_data_check')
		      ->master(true)
		      ->alias('a')
		      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
		      ->join('tadclass c on b.fad_class_code=c.fcode')
		      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')
		      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_childtask.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
		      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条数据复核任务待办',$count_task);
		}

	     //跨地域数据复核待办数量
		$tkname = 'sjfuhe|2';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			if(!empty($illDistinguishPlatfo)){
		      $where_task['left(d.fmediaclassid,2)'] = ['in',$media_class];
		    }
		    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
		    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
		    if($fregulatorlevel == 30){
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }else{
		      $where_task['a.tregionid'] = $regionid;//区划
		      $where_task['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
		    }

		    $where_task['a.tcheck_status'] = 0;//处理状态
		    $where_task['a.tkuayu_status'] = 10;//是否跨域
		    $count_task = M('tbn_data_check')
		      ->master(true)
		      ->alias('a')
		      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
		      ->join('tadclass c on b.fad_class_code=c.fcode')
		      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')
		      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_childtask.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
		      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条跨地域数据复核任务待办',$count_task);
		 }

		 //重点案件线索上报待办数量
		$tkname = 'sbzdanjian|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

		    $where_task['a.fstatus']       = 20;
		    $where_task['a.fdomain_isout'] = 0;
		    $where_task['a.fcustomer'] = $system_num;
		    $where_task['a.fwaitregid']    = session('regulatorpersonInfo.fregulatorpid');
		    $where_task['_string']         = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0';

		    $count_task = M('tbn_illegal_zdad')
		      ->master(true)
		      ->alias('a')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条重点线索任务待办',$count_task);
		 }

		  //跨地域重点案件线索上报待办数量
		$tkname = 'kdyjsanjian|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

		    $where_task['a.fstatus']       = 20;
		    $where_task['a.fdomain_isout'] = 10;
		    $where_task['a.fcustomer'] = $system_num;
		    $where_task['a.fwaitregid']    = session('regulatorpersonInfo.fregulatorpid');
		    $where_task['_string']         = 'locate('.session('regulatorpersonInfo.fregulatorpid').',fparticipate_regids)>0';

		    $count_task = M('tbn_illegal_zdad')
		      ->master(true)
		      ->alias('a')
		      ->where($where_task)
		      ->count();
		     D('Function')->update_task($tkname,$count_task.'条跨地域重点线索任务待办',$count_task);
		 }

		 //近三天通报消息数量
	 	$tkname = 'tbqingkuanglb|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['mon_sendtreid'] 	= session('regulatorpersonInfo.fregulatorpid');
			$where_task['mon_senduserid'] 	= session('regulatorpersonInfo.fid');
			$where_task['monb_customer'] 	= $system_num;
			$where_task['_string'] 			= 'date_sub(curdate(), INTERVAL 3 DAY) <= date(`mon_send_time`)';
			$count_task = M('tbn_monitor')
			  ->cache(120)
			  ->alias('m')
			  ->join('tbn_monitor_bureau as mb on m.mon_id = mb.monb_monid','LEFT')//发送消息
			  ->where($where_task)
			  ->count();// 查询满足要求的总记录数
			 D('Function')->update_task($tkname,$count_task.'条最新通报消息',$count_task);
		}

		 //近三天互动消息数量
		$tkname = 'xxlbjiaoliu|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_task['mrr_receivetreid'] = session('regulatorpersonInfo.fregulatorpid');
	      	$where_task['_string'] 			= 'date_sub(curdate(), INTERVAL 3 DAY) <= date(`me_sendtime`)';
			$count_task = M('tbn_message_receiveuser')
			  ->cache(120)
			  ->alias('mr')
			  ->join('tbn_message as m on mr.mrr_meid = m.me_id ', 'LEFT')//接收消息
			  ->where($where_task)
			  ->count();// 查询满足要求的总记录数
	        D('Function')->update_task($tkname,$count_task.'条最新互动信息',$count_task);
	    }

		//地方数据复核待办
		$tkname = 'clueTask|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

	      	$where_task['a.tregionid'] 		= substr(session('regulatorpersonInfo.regionid'),0,2);//机构权限
	      	$where_task['a.tcheck_jingdu'] 	= session('regulatorpersonInfo.fregulatorlevel');//复核进度
		    $where_task['a.tcheck_status'] 	= 0;//处理状态
			$where_task['a.fsample_type'] 	= 'tv';
	    	$count_task = M('fj_data_check')
	    		->master(true)
	    		->alias('a')
	    		->join('ttvsample g on g.fid=a.fsample_id and g.fstate in(0,1)')
	    		->join('tad b on g.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1')
		    	->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条电视类型复核任务待办',$count_task,'{"fmediaclass":"tv"}');

			$where_task['a.fsample_type'] 	= 'bc';
	    	$count_task = M('fj_data_check')
	    		->master(true)
	    		->alias('a')
	    		->join('tbcsample g on g.fid=a.fsample_id and g.fstate in(0,1)')
	    		->join('tad b on g.fadid = b.fadid and b.fadid<>0')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1')
		    	->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条广播类型复核任务待办',$count_task,'{"fmediaclass":"bc"}');

			$where_task['a.fsample_type'] 	= 'paper';
	    	$count_task = M('fj_data_check')
	    		->master(true)
	    		->alias('a')
	    		->join('tbcsample g on g.fid=a.fsample_id and g.fstate in(0,1)')
	    		->join('tad b on g.fadid = b.fadid and b.fadid<>0 ')//广告
				->join('tadclass c on b.fadclasscode=c.fcode ')//广告内容类别
				->join('tmedia f on g.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1')
		    	->where($where_task)
				->count();
			D('Function')->update_task($tkname,$count_task.'条报纸类型复核任务待办',$count_task,'{"fmediaclass":"paper"}');
		}

		//线索处理待办
		$tkname = 'fjresrecheck|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$fjxsgl = getconfig('fjxsgl');
			if(!empty($fjxsgl)){
				if($system_num == 350000){
					$tregister_jurisdiction = M('fj_tregisterjur')->where('ftr_type=10 and ftr_objid='.session('regulatorpersonInfo.fid'))->getField('ftr_jurid',true);//线索管理人员权限
					if(!empty($tregister_jurisdiction)){
						$where_task['b.fstate'] = array('in',$tregister_jurisdiction);
						$where_task['fsendstatus'] = 0;
						$where_task['fcreateregualtorpersonid'] = session('regulatorpersonInfo.fid');
						$count_task = M('fj_tregister')
							// ->master(true)
							->alias('a')
							->join('fj_tregisterflow b on a.fid=b.fregisterid')
							->where($where_task)
							->count();
					}
					D('Function')->update_task($tkname,$count_task.'条线索处理任务待办',$count_task);
				}
			}
		}

		//线索处理待办
		$tkname = 'filladlist|1';
		if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
			$count_task = 0;
			unset($where_task);

			$where_time = '1=1';

			if(session('regulatorpersonInfo.fregulatorlevel')==0){
				$where_task['a.fregion_id']  = session('regulatorpersonInfo.regionid');
			}elseif(session('regulatorpersonInfo.fregulatorlevel')==10){
				$where_task['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
			}elseif(session('regulatorpersonInfo.fregulatorlevel')==20){
				$where_task['a.fregion_id']  = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
			}
		    $where_task['a.fexamine'] = 0;
		    $where_task['a.fcustomer'] = $system_num;
		    if(!empty($illDistinguishPlatfo)){
		      $where_task['left(c.fmediaclassid,2)'] = ['in',$media_class];
		    }

		    $count_task = M('tbn_illegal_ad')
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
		      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
		      ->join('(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id')
		      ->where($where_task)
		      ->count();//查询满足条件的总记录数
		    D('Function')->update_task($tkname,$count_task.'条线索审核任务待办',$count_task);
		}

		//线索派发待办
        $tkname = 'gdatasend|1';
        if(in_array($tkname,$taskdata) && in_array(explode('|',$tkname)[0],$menujurisdiction)){
            $count_task = 0;
            unset($where_task);

            $wheres1 = '1=1';
          
            if($isrelease == 2){
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status in(0,1) ';
            }else{
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = 1 ';
            }

            if($isexamine == 10 || $isexamine == 20){
              $wheres1 .= ' and tbn_illegal_ad.fexamine = 10';
            }

            if($system_num == '100000'){
                $where_level = ' AND tbn_illegal_ad.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
            }
            if(!empty($illDistinguishPlatfo)){
		      $where2 = ' and left(b.fmediaclassid,2) in('.implode(',', $media_class).')';
		    }

            //查询总记录数
            $count_task = M()->query('
               SELECT count(*) as fadcount
                FROM
                 (
                SELECT fad_name,fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class
                FROM tbn_illegal_ad_issue,tbn_illegal_ad,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd 
                WHERE tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fcustomer="'.$system_num.'" '.$where_level.'  and '.$wheres1.'
                GROUP BY tbn_illegal_ad.fad_name,tbn_illegal_ad_issue.fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class) a,tmedia b,tmediaowner c,tregion d,tadclass f,tbn_illegal_ad e left join tbn_data_inspectlog dig on e.fid=dig.dig_lid
                WHERE e.fmedia_id=b.fid AND b.fid=b.main_media_id and b.fstate = 1 AND e.fad_class_code=f.fcode AND b.fmediaownerid=c.fid AND c.fregionid=d.fid AND a.fillegal_ad_id=e.fid '.$where2);

            D('Function')->update_task($tkname,$count_task[0]['fadcount'].'条线索派发任务待办',$count_task[0]['fadcount']);
        }

		//待办任务
		$where_wk['fuserid'] = session('regulatorpersonInfo.fid');
		$where_wk['ftaskcount'] = array('gt',0);
		$where_wk['fcustomer'] = $system_num;
		$data_wk = M('tbn_waittask')->master(true)->where($where_wk)->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data_wk));

	}

	//判断当前用户行政等级,并返回
    public function judgment_region($regionid){
        $level = M('tregion')->where(['fid'=>$regionid])->getField('flevel');
        return $level;
    }

	//近三十天违法广告情况热力图
    public function thermal_map_30(){
        $system_num = getconfig('system_num');
        //浏览权限设置start
        $region_id = I('region_id','');//点击的区域ID
        $region_level = I('region_level')?I('region_level'):6;//区域等级

        $user = session('regulatorpersonInfo');//获取用户信息
        $re_level = $this->judgment_region($user['regionid']);//当前用户行政等级
        $zxs = ['110000','120000','310000','500000'];//直辖市
        $tregion_map = [];
        $map = [];
        $group_filed = 'tbn_illegal_ad.fregion_id';
        $where_str = '1=1';

        $r_name = 'fname1';
        switch ($region_level){
            case '2':
                $r_name = 'fname';
                if($re_level == 1){
                    $map['tregion.fid'] = ['LIKE',substr($region_id,0,2).'%'];
                    $tregion_map['fid'] = ['LIKE',substr($region_id,0,2).'%'];
                }
                $tregion_map['flevel'] = 1;
                $where_str .= ' and e.flevel = 1';
                break;
            case '4':
                $map['tregion.fpid'] = ['LIKE',substr($region_id,0,2).'%'];
                $tregion_map['fpid'] = ['LIKE',substr($region_id,0,2).'%'];
                if(!in_array($region_id,$zxs)){
                	$map['tregion.flevel'] = ['in',[2,3,4]];
                    $tregion_map['flevel'] = ['in',[2,3,4]];
                    $where_str .= ' and e.flevel in(2,3,4)';
                }else{
                    $map['tregion.flevel'] = 5;
                    $tregion_map['flevel'] = 5;
                    $where_str .= ' and e.flevel = 5';
                }
                break;
            case '6':
                $map['tregion.fpid'] = ['LIKE',substr($region_id,0,4).'%'];
                $map['flevel'] = 5;
                $tregion_map['fpid'] = ['LIKE',substr($region_id,0,4).'%'];
                $tregion_map['flevel'] = 5;
                $zero = '';
                break;
        }

        $map['_string'] = '1=1';
        //是否显示发布前数据
        $isrelease = getconfig('isrelease');
        if(!empty($isrelease)){
            if($is_show_fsend == 1){
                $map['tbn_illegal_ad_issue.fsend_status'] = ['IN',[1,2]];
            }else{
                $map['tbn_illegal_ad_issue.fsend_status'] = 2;
            }
        }

        $map['_string'] .= ' and tmedia.fid = tmedia.main_media_id and tmedia.fstate = 1';
        $map['_string'] .= " and tbn_illegal_ad.fcustomer='".$system_num."'";
        $map['tbn_illegal_ad.fstatus3'] = ['NOT IN',[30,40]];
        $tregion_mod = M('tregion')->field('substr(fid,1,'.$region_level.') as fid_sub,fid,'.$r_name)->where($tregion_map)->select();
        //查询符合条件的数据
        $s_date = date('Y-m-d',time()-(86400*30));
        $e_date = date('Y-m-d',time());
        $map['tbn_illegal_ad_issue.fissue_date'] = ['BETWEEN',[$s_date,$e_date]];

        $province_illegal_count = M('tbn_illegal_ad_issue')
            ->field("
                substr(tregion.fid,1,$region_level) as fregion_id,
                count(1) as count
           ")
            ->join("
                    tbn_illegal_ad on 
                    tbn_illegal_ad_issue.fillegal_ad_id = tbn_illegal_ad.fid"
            )
            ->join("
                tregion on 
                tregion.fid = tbn_illegal_ad.fregion_id"
            )
            ->join("
                tmedia on 
                tmedia.fid = tbn_illegal_ad_issue.fmedia_id"
            )
            ->group($group_filed)
            ->where($map)
            ->select();//条数
        if(!empty($province_illegal_count)){
        	$province_illegal_data = array_column($province_illegal_count, 'count','fregion_id');
        }

        //发布总量
        $tb_tvissue = gettable('01',strtotime($e_date));
        $tb_bcissue = gettable('02',strtotime($e_date));
        $tb_paperissue = gettable('03',strtotime($e_date));
        $tb_tvissue2 = gettable('01',strtotime($s_date));
        $tb_bcissue2 = gettable('02',strtotime($s_date));
        $tb_paperissue2 = gettable('03',strtotime($s_date));
        $where_str .= ' and a.fstate = 1 and a.fsendstate = 20 and a.fissuedate between "'.$s_date.'" and "'.$e_date.'"';
		$joins = '
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1
			inner join tregion e on f.media_region_id = e.fid
		';

        $issuecountsql = 'select substr(media_region_id,1,'.$region_level.') fid_sub,sum(acount) as acount from (
	    	(select f.media_region_id,sum(fquantity) as acount 
	    		from '.$tb_tvissue.' a
				'.$joins.'
				where '.$where_str.' group by f.media_region_id)
			union all (select f.media_region_id,sum(fquantity) as acount 
				from '.$tb_bcissue.' a
				'.$joins.'
				where '.$where_str.' group by f.media_region_id)
			union all (select f.media_region_id,sum(fquantity) as acount 
				from '.$tb_paperissue.' a 
				'.$joins.'  
				where '.$where_str.' group by f.media_region_id)
			) as a
			group by media_region_id
		';
		$issuecount = M()->query($issuecountsql);
		if(!empty($issuecount)){
			$issueData1 = array_column($issuecount, 'acount','fid_sub');
		}

		if($tb_tvissue != $tb_tvissue2){
			$issuecountsql2 = 'select substr(media_region_id,1,'.$region_level.') fid_sub,sum(acount) as acount from (
		    	(select f.media_region_id,sum(fquantity) as acount 
		    		from '.$tb_tvissue2.' a
					'.$joins.'
					where '.$where_str.' group by f.media_region_id)
				union all (select f.media_region_id,sum(fquantity) as acount 
					from '.$tb_bcissue2.' a
					'.$joins.'
					where '.$where_str.' group by f.media_region_id)
				union all (select f.media_region_id,sum(fquantity) as acount 
					from '.$tb_paperissue2.' a 
					'.$joins.'  
					where '.$where_str.' group by f.media_region_id)
				) as a
				group by media_region_id
			';
			$issuecount2 = M()->query($issuecountsql2);
			if(!empty($issuecount2)){
				$issueData2 = array_column($issuecount2, 'acount','fid_sub');
			}
		}

        $data = [];
        foreach ($tregion_mod as $tregion_mod_key=>$tregion_mod_val){
        	$data[$tregion_mod_val['fid_sub']] = [
                'id'=>$tregion_mod_val['fid'],
                'name'=>$tregion_mod_val[$r_name],
            ];
            $data[$tregion_mod_val['fid_sub']]['issuecount'] = $province_illegal_data[$tregion_mod_val['fid_sub']]?$province_illegal_data[$tregion_mod_val['fid_sub']]:0;//违法量
            $iscount1 = $issueData1[$tregion_mod_val['fid_sub']]?$issueData1[$tregion_mod_val['fid_sub']]:0;
            $iscount2 = $issueData2[$tregion_mod_val['fid_sub']]?$issueData2[$tregion_mod_val['fid_sub']]:0;
            $data[$tregion_mod_val['fid_sub']]['value'] = $iscount1 + $iscount2;//发布量
        }
        $data = array_values($data);
        $this->ajaxReturn($data);
    }

}

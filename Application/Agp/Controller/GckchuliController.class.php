<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局处理结果查看
 * by zw
 */

class GckchuliController extends BaseController{
  /**
  * 处理结果列表
  * by zw
  */
  public function cljieguo_list(){
    Check_QuanXian(['ckchuli']);
    session_write_close();
    $outtype = I('outtype');//导出类型
    $selfid = I('fid');//勾选项
    if(!empty($outtype) || !empty($mediaownerid)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    $where_tia['_string'] = '1=1';

    $system_num = getconfig('system_num');
    $illadTimeoutTips = getconfig('illadTimeoutTips');//线索处理超时提示，0无，1有
    $againIssueTips = getconfig('againIssueTips');//  上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）\

    $iscontain        = I('iscontain');//是否包含下属地区
    $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含，2不包含，0未定义
    $area = I('area');
    $fstate = I('fstate');
    $fstate2 = I('fstate2');
    $fresultdatetime = I('fresultdatetime');//处理时间
    $fsenddatetime = I('fsend_datetime');//派发时间
    $mediaownerid = I('mediaownerid');//媒体机构ID

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    
    if($is_show_fsend==1){
      $is_show_fsend = -2;
    }else{
      $is_show_fsend = 0;
    }
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$is_show_fsend);

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['a.fadowner'] = array('like','%'.$search_val.'%');
      }
    }

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }

    if(( !empty($fad_class_code)) && empty($area)){
      if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel')==30){
        $area = '100000';
      }else{
        $area = session('regulatorpersonInfo.regionid');
      }
    }

    $where_tia['a.fstatus3'] = array('not in',array(30,40));
    if($fstate != -1){//违法广告处理状态
      if($fstate == 0){
        $where_tia['a.fstatus'] = 0 ;
        if($fstate2 == 10){
          $where_tia['a.fview_status'] = 0 ;
        }elseif($fstate2 == 20){
          $where_tia['a.fview_status'] = 10 ;
        }
      }elseif($fstate == 10){
        $where_tia['a.fstatus'] = 10 ;
        if($fstate2 == 10){
          $where_tia['a.fresult'] = array('like','%责令停止发布%') ;
        }elseif($fstate2 == 20){
          $where_tia['a.fresult'] = array('like','%立案%') ;
        }elseif($fstate2 == 30){
          $where_tia['a.fresult'] = array('like','%立案%') ;
          $where_tia['a.fstatus2'] = 17 ;
        }elseif($fstate2 == 40){
          $where_tia['a.fresult'] = array('like','%立案%') ;
          $where_tia['a.fstatus2'] = 16 ;
        }elseif($fstate2 == 50){
          $where_tia['a.fresult'] = array('like','%移送司法%') ;
        }elseif($fstate2 == 60){
          $where_tia['a.fresult'] = array('like','%其他%') ;
        }
      }elseif($fstate == 30){
        $where_tia['a.fstatus'] = 30 ;
      }
    }

    $tmediatempstr = '0,1';
    if(!empty($area)){//所属地区
      if($area != '100000'){
        if(!empty($iscontain)){
          $tregion_len = get_tregionlevel($area);
          if($tregion_len == 1){//国家级
            $where_tia['_string'] .= ' and fregion_id ='.$area;  
          }elseif($tregion_len == 2){//省级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,2).'%"';
          }elseif($tregion_len == 4){//市级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,4).'%"';
          }elseif($tregion_len == 6){//县级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,6).'%"';
          }
          $tmediatempstr = '0,1,2';
        }else{
          $where_tia['fregion_id']  = $area;
        }
      }
    }

    //国家局系统只显示有打国家局标签媒体的数据
    if($system_num == '100000'){
      $wherestr = ' and tn.flevel in (1,2,3)';
    }

    $where_tia['a.fcustomer']  = $system_num;
    
    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }

    if(!empty($fresultdatetime)){
      $where_tia['a.fresult_datetime'] = ['between',[$fresultdatetime[0],date('Y-m-d',strtotime($fresultdatetime[1])).' 23:59:59']];
    }
    if(!empty($fsenddatetime)){
      $where_tia['a.fsend_datetime'] = ['between',[$fsenddatetime[0],date('Y-m-d',strtotime($fsenddatetime[1])).' 23:59:59']];
    }
    if(!empty($mediaownerid)){
      $where_tia['c.fmediaownerid'] = $mediaownerid;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }
    //勾选导出
    if(!empty($outtype) && !empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数
    
    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fregion_id,a.fadowner as fadownername,tn.ffullname mediaregion,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,a.fsend_datetime,ifnull(a.fexaminetime,a.create_time) fsend_time,a.adowner_regionid,a.fsample_id')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    foreach ($do_tia as $key => $value) {
      $do_tia[$key]['adownerregion'] = M('tregion')->cache(true,86400)->where(['fid'=>$value['adowner_regionid']])->getField('ffullname');

      $do_tia[$key]['frece_reg'] = M('tbn_case_send')->cache(true,2)->where(['fillegal_ad_id'=>$value['fid']])->order('fstatus asc,fid desc')->getField('frece_reg');//处理机构

      //判断案件未处理是否超过90天
      if(!empty($value['fsend_time']) && !empty($illadTimeoutTips) && empty($outtype) && empty($value['fstatus'])){
        if((time()-strtotime($value['fsend_time']))>90*86400){
          $do_tia[$key]['showTimeout'] = 1;
        }else{
          $do_tia[$key]['showTimeout'] = 0;
        }
      }else{
        $do_tia[$key]['showTimeout'] = 0;
      }

      //判断上个月有播放并处理了的，本月还在播放的违法广告
      if(!empty($againIssueTips) && empty($value['fstatus'])){
        $where_issue['a.fcustomer'] = $system_num;
        $where_issue['a.fsample_id'] = $value['fsample_id'];
        $where_issue['a.fstatus'] = 10;
        $where_issue['b.fissue_date'] = ['between',[date('Y-m-01',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 month')),date('Y-m-d',strtotime(date('Y-m-01',strtotime($value['fstarttime'])) . ' -1 day'))]];
        $againIssue = M('tbn_illegal_ad')
          ->alias('a')
          ->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
          ->where($where_issue)
          ->count();
        if(!empty($againIssue)){
          $do_tia[$key]['againIssue'] = 1;
        }else{
          $do_tia[$key]['againIssue'] = 0;
        }
      }else{
        $do_tia[$key]['againIssue'] = 0;
      }
    }

    if(!empty($outtype)){
        if(empty($do_tia)){
          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }

        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-违法广告清单';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'mediaregion',
          '发布媒体'=>'fmedianame',
          '媒体分类'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网'],
            ]
          ],
          '广告类别'=>'fadclass',
          '广告名称'=>'fad_name',
          '广告主'=>'fadownername',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '派发日期'=>'fsend_datetime',
          '查看状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fview_status} == 0','未查看'],
              ['{fview_status} == 10','已查看'],
            ]
          ],
          '处理状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus} == 0','未处理'],
              ['{fstatus} == 10','已上报'],
              ['{fstatus} == 30','数据复核'],
            ]
          ],
          '处理结果'=>'fresult',
          '处理时间'=>'fresult_datetime',
          '处理机构'=>'fresult_unit',
          '处理人'=>'fresult_user',
          '立案状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus2} == 15','待处理'],
              ['{fstatus2} == 16','已处理'],
              ['{fstatus2} == 17','处理中'],
              ['{fstatus2} == 20','撤消'],
            ]
          ],
          '复核状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus3} == 10','复核中'],
              ['{fstatus3} == 20','复核失败'],
              ['{fstatus3} == 30','复核通过'],
            ]
          ],
        ];
        if($isexamine == 0){
          unset($outdata['datalie']['下发时间']);
        }
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        D('Function')->write_log('违法广告清单',1,'导出成功');
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

      }else{
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
      }
  }

  /**
  * 案件继续处理
  * by zw
  */
  public function anjian_cxsb(){
    Check_QuanXian(['sbchuli','ckchuli']);
    $system_num = getconfig('system_num');
    $fid        = I('fid');//违法广告ID
    $attachinfo = I('attachinfo');//上传的文件信息
    $selfresult = I('selfresult');//处理结果
    $attach     = [];//预添加的附件信息
    $attach2    = [];//预删除的附件信息

    if(!empty($selfresult)){
      $where_tia['fid']     = $fid;
      $where_tia['fcustomer']  = $system_num;
      $where_tia['_string'] = 'fid in (select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').')';

      $data_tia['fstatus']          = 10;//已上报
      $data_tia['fview_status']     = 10;//已查看
      $data_tia['fresult']          = $selfresult;//处理结果
      $data_tia['fresult_datetime'] = date('Y-m-d H:i:s');//上报时间

      //判断是否有立案调查，如有：添加立案调查记录
      if(strpos($selfresult,'立案')!==false){
        $data_tia['fstatus2']   = 17;//立案待处理
      }
      $do_tia = M('tbn_illegal_ad')->where($where_tia)->save($data_tia);
      if(!empty($do_tia)){
        //上传附件
        $attach_data['ftype']    = 0;//类型
        $attach_data['fillegal_ad_id']    = $fid;//违法广告
        $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
        $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattach'];
          $attach_data['fattach_url'] = $value2['fattach_url'];
          if($value2['fattach_status'] == 2){
            array_push($attach,$attach_data);
          }elseif($value2['fattach_status'] == 0){
            array_push($attach2,$value2['fattach_url']);
          }
        }
      }
      if(!empty($attach)){
        M('tbn_illegal_ad_attach')->addAll($attach);
      }
      if(!empty($attach2)){
        $where_att['fattach_url'] = array('in',$attach2);
        M('tbn_illegal_ad_attach')->where($where_att)->delete();
      }
      D('Function')->write_log('违法广告清单',1,'继续处理成功','tbn_illegal_ad',$do_tia,M('tbn_illegal_ad')->getlastsql());
      $this->ajaxReturn(array('code'=>0,'msg'=>'处理成功'));
    }else{
      D('Function')->write_log('违法广告清单',0,'请选择处理结果','tbn_illegal_ad',0);
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择处理结果'));
    }
  }

  /**
  * 查看案件证据
  * by zw
  */
  public function fjjieguo(){
    $system_num = getconfig('system_num');

    $fid = I('fid');//违法广告ID
    $where_tia['a.fillegal_ad_id'] = $fid;
    $where_tia['a.fstate']         = 0;
    $where_tia['a.ftype']         = 0;

    //证据附件
    $do_tia = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')->where($where_tia)->order('a.fid asc')->select();

    $where_tia['a.ftype']         = ['in',[10,20]];
    //立案附件
    $do_tia2 = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url,a.ftype')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')->where($where_tia)->order('a.fid asc')->select();

    $where_tia['a.ftype']         = 30;
    //督办附件
    $do_tia3 = M('tbn_illegal_ad_attach')->field('a.fattach,a.fattach_url,a.ftype')->alias('a')->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')->where($where_tia)->order('a.fid asc')->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','zjfjdata'=>$do_tia,'lafjdata'=>$do_tia2,'dbfjdata'=>$do_tia3));
  }

  /**
  * 撤消处理结果
  * by zw
  */
  public function cxjieguo(){
    Check_QuanXian(['sbchuli','ckchuli']);
    $system_num = getconfig('system_num');
    $selfid     = I('fid');//违法广告ID组
    
    $data_tia['fstatus']        = 0;
    $data_tia['fstatus2']       = 0;
    $data_tia['fstatus3']       = 0;
    $data_tia['fview_status']   = 0;
    $data_tia['fresult']        = '';
    $data_tia['flatime']        = null;
    $data_tia['fresult_datetime']  = null;
    $data_tia['fresult_unit']   = '';
    $data_tia['fresult_user']   = '';

    $where_tia['fstatus']   = ['in',[10,30]];
    $where_tia['fcustomer'] = $system_num;
    foreach ($selfid as $key => $value) {
      $where_tia['fid'] = $value;
      $do_tia = M('tbn_illegal_ad')
        ->where($where_tia)
        ->save($data_tia);//更改状态
      if(!empty($do_tia)){
        M()->execute('update tbn_illegal_ad_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//上报文件失效
        M()->execute('update tbn_data_check set tcheck_status=20 where fillegal_ad_id='.$value);//复核记录退回
        M()->execute('update tbn_data_check_attach set fstate=10 where fstate=0 and fillegal_ad_id='.$value);//复核文件失效
        M()->execute('update tbn_case_send set fstatus=0 where fstatus<>0 and fillegal_ad_id='.$value.' and fid in(select fid from (select max(fid) fid from tbn_case_send where fstatus<>0 and fillegal_ad_id='.$value.' ) a)');//分派记录重置
      }
    }
    
    D('Function')->write_log('案件撤消',1,'撤消完成','tbn_illegal_ad',$do_tia,M('tbn_illegal_ad')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'撤消完成'));
    
  }

  /**
  * 提交督办材料
  * by zw
  */
  public function scjiandu(){
    $selfid     = I('selfid');//违法广告ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $system_num = getconfig('system_num');
    $islafile   = getconfig('islafile');
    $attach     = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo) && !empty($islafile)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
    }

    foreach ($selfid as $key => $value) {
      //上传附件
      $attach_data['ftype']  = 30;//类型
      $attach_data['fillegal_ad_id']  = $value;//违法广告
      $attach_data['fuploader']       = session('regulatorpersonInfo.fid');//上传人
      $attach_data['fupload_time']    = date('Y-m-d H:i:s');//上传时间
      foreach ($attachinfo as $key2 => $value2){
        $attach_data['fattach']     = $value2['fattachname'];
        $attach_data['fattach_url'] = $value2['fattachurl'];
        array_push($attach,$attach_data);
      }
    }
    if(!empty($attach)){
      M('tbn_illegal_ad_attach')->addAll($attach);
    }
    D('Function')->write_log('执法监督',1,'操作成功','tbn_illegal_ad',0);
    $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
  }

  /**
  * 获取违法广告监控情况
   * by zw
   */
  public function illCondition(){
    session_write_close();
    $showType = I('showType')?I('showType'):0;//展现方式，0汇总，1列表
    $dataType = I('dataType')?I('dataType'):[];//需要获取的数据，数组，1重复播放的，2已停播的，3新增的，空为所有
    $outtype = I('outtype');//导出类型

    $system_num = getconfig('system_num');
    $ischeck = getconfig('ischeck');
    $nowdata = '2019-06-01';
    // $nowdata = date('Y-m-01');

    $where_tia['_string'] = '1=1';
    
    //国家局系统只显示有打国家局标签媒体的数据
    if($system_num == '100000'){
      $wherestr = ' and tn.flevel in (1,2,3)';
    }
    $where_tia['a.fstatus3'] = array('not in',array(30,40));
    $where_tia['a.fcustomer']  = $system_num;
    
    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }

    //本月的数据
    $where_time2 = gettimecondition(date('Y',strtotime($nowdata)),30,date('m',strtotime($nowdata)),'fissue_date',-1);
    $samples1 = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->getField('fsample_id',true);

    //上月的数据
    $where_time3 = gettimecondition(date('Y',strtotime($nowdata. ' -1 month')),30,date('m',strtotime($nowdata. ' -1 month')),'fissue_date',-1);
    $samples2 = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time3.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->group('a.fsample_id')
      ->getField('fsample_id',true);

    $noPlay = [];//已停播
    $newPlay = [];//新播出
    $cfIll = array_intersect($samples1,$samples2);//重复播放的样本
    foreach ($samples1 as $value) {
      if(!in_array($samples2,$value)){
        $newPlay[] = $value;
      }
    }

    foreach ($samples2 as $value) {
      if(!in_array($samples1,$value)){
        $noPlay[] = $value;
      }
    }

    if(empty($showType)){
      if(empty($dataType) || in_array(1,$dataType)){
        if(!empty($cfIll)){
          //重复播放的违法广告量
          $where_tia['fsample_id'] = ['in',$cfIll];
          $cfIllCount = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->count();
          $data['cfIllCount'] = $cfIllCount?$cfIllCount:0;
        }else{
          $data['cfIllCount'] = 0;
        }
      }

      if(empty($dataType) || in_array(2,$dataType)){
        if(!empty($noPlay)){
          //已停播的违法广告量
          $where_tia['fsample_id'] = ['in',$noPlay];
          $noPlayCount = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time3.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->count();
          $data['noPlayCount'] = $noPlayCount?$noPlayCount:0;
        }else{
          $data['noPlayCount'] = 0;
        }
      }
      
      if(empty($dataType) || in_array(3,$dataType)){
        if(!empty($newPlay)){
          //新播的违法广告量
          $where_tia['fsample_id'] = ['in',$newPlay];
          $newPlayCount = M('tbn_illegal_ad')
            ->alias('a')
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->count();
          $data['newPlayCount'] = $newPlayCount?$newPlayCount:0;
        }else{
          $data['newPlayCount'] = 0;
        }
      }
    }else{
      $fields = 'a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus2,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fstatus,a.fregion_id';
      if(empty($dataType) || in_array(1,$dataType)){
        if(!empty($cfIll)){
          //重复播放的违法广告量
          $where_tia['fsample_id'] = ['in',$cfIll];
          $cfIllData = M('tbn_illegal_ad')
            ->alias('a')
            ->field($fields)
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->select();
          $data['cfIllData'] = $cfIllData?$cfIllData:[];
        }else{
          $data['cfIllData'] = [];
        }
        $waitData = $data['cfIllData'];
      }

      if(empty($dataType) || in_array(2,$dataType)){
        if(!empty($noPlay)){
          //已停播的违法广告量
          $where_tia['fsample_id'] = ['in',$noPlay];
          $noPlayData = M('tbn_illegal_ad')
            ->alias('a')
            ->field($fields)
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time3.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->select();
          $data['noPlayData'] = $noPlayData?$noPlayData:[];
        }else{
          $data['noPlayData'] = [];
        }
        $waitData = $data['noPlayData'];
      }
      
      if(empty($dataType) || in_array(3,$dataType)){
        if(!empty($newPlay)){
          //新播的违法广告量
          $where_tia['fsample_id'] = ['in',$newPlay];
          $newPlayData = M('tbn_illegal_ad')
            ->alias('a')
            ->field($field)
            ->join('tadclass b on a.fad_class_code=b.fcode')
            ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
            ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
            ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.$where_time2.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
            ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
            ->where($where_tia)
            ->select();
          $data['newPlayData'] = $newPlayData?$newPlayData:[];
        }else{
          $data['newPlayData'] = [];
        }
        $waitData = $data['newPlayData'];
      }
      
    }
    
    if(!empty($outtype)){
      if(empty($waitData)){
        $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
      }

      $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-数据汇总违法条数列表';//文档内部标题名称
      $outdata['datalie'] = [
        '序号'=>'key',
        '媒体分类'=>[
          'type'=>'zwif',
          'data'=>[
            ['{fmedia_class} == 1','电视'],
            ['{fmedia_class} == 2','广播'],
            ['{fmedia_class} == 3','报纸'],
            ['{fmedia_class} == 13','互联网'],
          ]
        ],
        '广告类别'=>'fadclass',
        '广告名称'=>'fad_name',
        '发布媒体'=>'fmedianame',
        '播出总条次'=>'fcount',
        '播出周数'=>'diffweek',
        '播出时间段'=>[
          'type'=>'zwhebing',
          'data'=>'{fstarttime}~{fendtime}'
        ],
        '违法表现代码'=>'fillegal_code',
        '违法表现'=>'fexpressions',
        '涉嫌违法原因'=>'fillegal',
      ];
      $outdata['lists'] = $waitData;
      $ret = A('Api/Function')->outdata_xls($outdata);

      D('Function')->write_log('数据汇总违法条数',1,'导出成功');
      $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
    }else{
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
    }
  }

  /**
   * 获取案件线索分析数据
   * by zw
   */
  public function allIllAnalysis(){
    session_write_close();
    $system_num = getconfig('system_num');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $illadTypes[] = '平台监测';
    $illadTypes2 = getconfig('illadTypes')?json_decode(getconfig('illadTypes')):[];//重点案件线索类型（例：投诉举报，上级交办，部门移交，其他）
    $illadTypes = array_merge($illadTypes, $illadTypes2);

    //时间条件筛选
    $issueDate    = I('issueDate');//选择时间
    if(!empty($issueDate)){
      $where_time = gettimecondition(date('Y-m-d',strtotime($issueDate[0])),0,implode(',', $issueDate),'fissue_date',-1);
    }else{
      $where_time = '1=1';
    }
    $where_tia['_string'] = '1=1';
    
    $where_tia['a.fstatus3'] = array('not in',array(30,40));
    $where_tia['a.fcustomer']  = $system_num;
   
    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(c.fmediaclassid,2)'] = ['in',$media_class];
    }

    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fregion_id,tn.fname regionname,fview_status,fstatus,fresult')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->order('a.fregion_id asc')
      ->select();

    $hejidata['diquzongCount'] = 0;
    foreach ($illadTypes as $illadType) {
      $hejidata['xiansuodata'][$illadType]['fenleizongCount'] = 0;//线索量
      $hejidata['xiansuodata'][$illadType]['chakanCount'] = 0;//查看量
      $hejidata['xiansuodata'][$illadType]['chuliCount'] = 0;//处理量
      $hejidata['xiansuodata'][$illadType]['zhetingCount'] = 0;//责停量
      $hejidata['xiansuodata'][$illadType]['lianCount'] = 0;//立案量
      $hejidata['xiansuodata'][$illadType]['shifaCount'] = 0;//移交司法量
      $hejidata['xiansuodata'][$illadType]['qitaCount'] = 0;//其他量
    }

    //平台监测
    foreach ($do_tia as $key => $value) {
      if(empty($data[$value['regionname']])){
        $data[$value['regionname']]['diquzongCount'] = 0;//地区总线索量
        foreach ($illadTypes as $illadType) {
          $data[$value['regionname']]['xiansuodata'][$illadType]['fenleizongCount'] = 0;//线索量
          $data[$value['regionname']]['xiansuodata'][$illadType]['chakanCount'] = 0;//查看量
          $data[$value['regionname']]['xiansuodata'][$illadType]['chuliCount'] = 0;//处理量
          $data[$value['regionname']]['xiansuodata'][$illadType]['zhetingCount'] = 0;//责停量
          $data[$value['regionname']]['xiansuodata'][$illadType]['lianCount'] = 0;//立案量
          $data[$value['regionname']]['xiansuodata'][$illadType]['shifaCount'] = 0;//移交司法量
          $data[$value['regionname']]['xiansuodata'][$illadType]['qitaCount'] = 0;//其他量
        }
      }

      //合计数据
      $hejidata['diquzongCount'] += 1;
      $hejidata['xiansuodata'][$illadTypes[0]]['fenleizongCount'] += 1;

      $data[$value['regionname']]['diquzongCount'] += 1;
      $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['fenleizongCount'] += 1;
      if(!empty($value['fview_status'])){
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['chakanCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['chakanCount'] += 1;
      }
      if(!empty($value['fstatus'])){
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['chuliCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['chuliCount'] += 1;
      }
      if(strpos($value['fresult'],'责令停止发布') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['zhetingCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['zhetingCount'] += 1;
      }
      if(strpos($value['fresult'],'立案') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['lianCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['lianCount'] += 1;
      }
      if(strpos($value['fresult'],'移送司法') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['shifaCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['shifaCount'] += 1;
      }
      if(strpos($value['fresult'],'其他') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$illadTypes[0]]['qitaCount'] += 1;
        $hejidata['xiansuodata'][$illadTypes[0]]['qitaCount'] += 1;
      }
    }

    $where_tizd['fcustomer'] = $system_num;
    if(!empty($issueDate)){
      $where_tizd['a.fsend_time'] = ['between',$issueDate];
    }
    $do_tizd = M('tbn_illegal_zdad')
      ->alias('a')
      ->field('a.fregion_id,tn.fname regionname,fview_status,fstatus,fresult,fill_type')
      ->join('tregion tn on a.fregion_id=tn.fid')
      ->where($where_tizd)
      ->order('a.fregion_id asc')
      ->select();
    foreach ($do_tizd as $key => $value) {
      if(empty($data[$value['regionname']])){
        $data[$value['regionname']]['diquzongCount'] = 0;
        foreach ($illadTypes as $illadType) {
          $data[$value['regionname']]['xiansuodata'][$illadType]['fenleizongCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['chakanCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['chuliCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['zhetingCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['lianCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['shifaCount'] = 0;
          $data[$value['regionname']]['xiansuodata'][$illadType]['qitaCount'] = 0;
        }
      }

      //合计数据
      $hejidata['diquzongCount'] += 1;
      $hejidata['xiansuodata'][$value['fill_type']]['fenleizongCount'] += 1;

      $data[$value['regionname']]['diquzongCount'] += 1;
      $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['fenleizongCount'] += 1;
      if(!empty($value['fview_status'])){
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['chakanCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['chakanCount'] += 1;
      }
      if($value['fstatus'] == 30){
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['chuliCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['chuliCount'] += 1;
      }
      if(strpos($value['fresult'],'责令停止发布') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['zhetingCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['zhetingCount'] += 1;
      }
      if(strpos($value['fresult'],'立案') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['lianCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['lianCount'] += 1;
      }
      if(strpos($value['fresult'],'移送司法') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['shifaCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['shifaCount'] += 1;
      }
      if(strpos($value['fresult'],'其他') !== false){ 
        $data[$value['regionname']]['xiansuodata'][$value['fill_type']]['qitaCount'] += 1;
        $hejidata['xiansuodata'][$value['fill_type']]['qitaCount'] += 1;
      }
    }

    foreach ($hejidata['xiansuodata'] as $key => $value) {
      $hejidata['xiansuodata'][$key]['chakanlv'] = ($value['chakanCount']==0 || $value['fenleizongCount'] == 0)?'0.00':round($value['chakanCount']/$value['fenleizongCount']*100,2).'%';
      $hejidata['xiansuodata'][$key]['chulilv'] = ($value['chuliCount']==0 || $value['fenleizongCount'] == 0)?'0.00':round($value['chuliCount']/$value['fenleizongCount']*100,2).'%';
    }

    foreach ($data as $key => $value) {
      foreach ($value['xiansuodata'] as $key2 => $value2) {
        $data[$key]['xiansuodata'][$key2]['chakanlv'] = ($value2['chakanCount']==0 || $value2['fenleizongCount'] == 0)?'0.00%':round($value2['chakanCount']/$value2['fenleizongCount']*100,2).'%';
        $data[$key]['xiansuodata'][$key2]['chulilv'] = ($value2['chuliCount']==0 || $value2['fenleizongCount'] == 0)?'0.00%':round($value2['chuliCount']/$value2['fenleizongCount']*100,2).'%';
      }
    }

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data,'hejidata'=>$hejidata));
  }

  /*
  * 导出案件表单
  * by zw
  */
  public function outBiaoDan(){
    //引入类库
    Vendor('mpdf.mpdf');
    //设置中文编码
    $selfid = I('selfid');//勾选项
    // $selfid = [17,18];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'未选择导出项'));
    }

    $where_tia['_string'] = '1=1';

    $system_num = getconfig('system_num');
    $illadTimeoutTips = getconfig('illadTimeoutTips');//线索处理超时提示，0无，1有
    $againIssueTips = getconfig('againIssueTips');//  上月已做案件处罚后，本月还在继续播放的，是否需要进行提醒，0不需要，1需要
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）\

    if($is_show_fsend==1){
      $is_show_fsend = -2;
    }else{
      $is_show_fsend = 0;
    }
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',$is_show_fsend);

    $where_tia['a.fstatus3'] = array('not in',array(30,40));
    $where_tia['a.fcustomer']  = $system_num;
    
    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }
    $where_tia2 = $where_tia;

    if(!empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }
    
    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fwenshu_num,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,x.fstarttime,x.fendtime,a.fwenshu_num')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
      ->join('tregion tn on a.fregion_id=tn.fid')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on a.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->select();

    if(!empty($do_tia)){
      //获取每年度最高的文书号
      $do_tia2 = M('tbn_illegal_ad')
        ->alias('a')
        ->field('DATE_FORMAT(x.fstarttime,"%Y%m") wenshuyear,max(a.fwenshu_num) fwenshu_num')
        ->join('tadclass b on a.fad_class_code=b.fcode')
        ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
        ->join('tregion tn on a.fregion_id=tn.fid')
        ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
        ->where($where_tia2)
        ->group('DATE_FORMAT(x.fstarttime,"%Y%m")')
        ->select();
      $wsNumArr = array_column($do_tia2, 'fwenshu_num','wenshuyear');
    }

    foreach ($do_tia as $key => $value) {
      $mpdf=new \mPDF('zh-cn','A4', 0, '宋体', 0, 0);

      $year = date('Y',strtotime($value['fstarttime']));
      $month = date('m',strtotime($value['fstarttime']));
      if(!empty($value['fwenshu_num'])){
        $number = sprintf("%04d",(int)$value['fwenshu_num']);
      }else{
        if(!empty($wsNumArr[$year.$month])){
          $wsNumArr[$year.$month] = (int)$wsNumArr[$year.$month] + 1;
          $number = sprintf("%04d",$wsNumArr[$year.$month]);
        }else{
          $wsNumArr[$year.$month] = 1;
          $number = '0001';
        }
      }

      $strContent = '
      <html>
      <head>
      <style>
        body {
          font-family: "宋体";
        }
        .csbox1 {
          padding:10px 50px;
        }
        .csbox1 div {
          line-height:48px;
        }
        .cs1 {
          font-size:50px;
          font-weight:bold;
          text-align:center;
          padding:0 50px;
          color:#FF0000;
        }
        .cshr {
          height:5px;
          background-color:#ff0000;
          margin-top:16px;
        }
        .cs2 {
          font-size:28px;
          text-align:center;
          padding-top:16px;
        }
        .cs3 {
          font-size:18px;
          text-align:center;
          padding-top:12px;
          padding-bottom:25px;
        }
        .cs4 {
          font-size:20px;
          text-align:left;
        }
        .cs4_1 {
          font-size:20px;
          text-align:left;
          text-indent:38px;
        }
        .cs5 {
          padding-top:40px;
          font-size:20px;
          text-align:right;
        }
        .cs6 {
          font-size:20px;
          text-align:right;
        }
      </style>
      </head>
      <body>
        <div class="cs1">
          <span>浙江省广告监测中心</span>
          <div class="cshr"></div>
        </div>
        <div class="cs2">涉嫌违法广告线索</div>
        <div class="cs3">浙广监线【'.$year.'】'.$month.$number.'号</div>
        <div class="csbox1">
          <div class="cs4">一、广告名称：'.$value['fad_name'].'</div>
          <div class="cs4">二、广告发布者：'.$value['fmedianame'].'</div>
          <div class="cs4">三、发布时间：'.date('Y 年 m 月 d 日',strtotime($value['fstarttime'])).' - '.date('Y 年 m 月 d 日',strtotime($value['fendtime'])).'</div>
          <div class="cs4">四、广告内容：</div>
          <div class="cs4_1">'.$value['fillegal'].'</div>
          <div class="cs4">五、涉嫌违反以下规定：</div>
          <div class="cs4_1">'.$value['fexpressions'].'</div>
          <div class="cs5">浙江省广告监测中心</div>
          <div class="cs6">'.date('Y 年 m 月 d 日').'</div>
        </div>
      </body>
      </html>';

      $mpdf->WriteHTML($strContent);

      $filenames = $year.$month.$number.'.pdf';
      $savefile = './LOG/'.$filenames;

      $mpdf->Output($savefile);
      $ret = A('Common/AliyunOss','Model')->file_up('tem/'.$filenames,$savefile,array('Content-Disposition' => 'attachment;filename='.$filenames));//上传云
      unlink($savefile);//删除文件
      if(!empty($ret['url']) && empty($value['fwenshu_num'])){
        M('tbn_illegal_ad')->where(['fid'=>$value['fid']])->save(['fwenshu_num'=>(int)$number]);
      }
      $fileUrl[] = $ret['url'];
    }

    $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$fileUrl));
    // $mpdf->Output();
  }
}
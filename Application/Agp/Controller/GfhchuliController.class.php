<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局复核处理
 * by zw
 */

class GfhchuliController extends BaseController{
  /**
  * 本省数据复核处理列表
  * by zw
  */
  public function fhchuli_list(){
    Check_QuanXian(['sjfuhe']);
    session_write_close();
    $system_num = getconfig('system_num');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
    if($fregulatorlevel == 30){
      $where_tia['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
    }else{
      $where_tia['a.tregionid'] = $regionid;//区划
      $where_tia['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
    }
    $where_tia['a.tcheck_status'] = 0;//处理状态
    $where_tia['a.tkuayu_status'] = 0;//是否跨域
    $where_tia['b.fstatus'] = 30;//复核
    $where_tia['b.fstatus3'] = 10;//复核
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(d.fmediaclassid,2)'] = ['in',$media_class];
    }
  
    $count = M('tbn_data_check')
      ->alias('a')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_data_check')
      ->alias('a')
      ->field('a.id as fid,a.tgj_result,a.tshengj_result,a.tshij_result,a.tquj_result,b.fmedia_class,c.ffullname as fadclass,b.fad_name, (case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,b.fillegal_code,b.fillegal,b.fexpressions,b.favifilename,b.fjpgfilename,a.tgj_username,a.tgj_time,a.tshengj_username,a.tshengj_time,a.tshij_username,a.tshij_time,a.tquj_username,a.tquj_time,b.fid as sid,b.fadowner as fadownername')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->order('a.id desc')
      ->page($p,$pp)
      ->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 跨省复核处理结果列表
  * by zw
  */
  public function ksfhchuli_list(){
    Check_QuanXian(['sjfuhe']);
    session_write_close();
    $system_num = getconfig('system_num');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    
    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
    if($fregulatorlevel == 30){
      $where_tia['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
    }else{
      $where_tia['a.tregionid']     = $regionid;//区划
      $where_tia['a.tcheck_jingdu'] = $fregulatorlevel;//复核进度
    }
    $where_tia['a.tcheck_status'] = 0;//处理状态
    $where_tia['a.tkuayu_status'] = 10;//是否跨域
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(d.fmediaclassid,2)'] = ['in',$media_class];
    }

    $count = M('tbn_data_check')
      ->alias('a')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
      ->join('tbn_case_send snd on snd.fillegal_ad_id=b.fid and snd.fstatus = 30')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数

    $do_tia = M('tbn_data_check')
      ->alias('a')
      ->field('a.id as fid,a.tgj_result,a.tshengj_result,a.tshij_result,a.tquj_result,b.fmedia_class,c.ffullname as fadclass,b.fad_name, (case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,b.fillegal_code,b.fillegal,b.fexpressions,b.favifilename,b.fjpgfilename,a.tgj_username,a.tgj_time,a.tshengj_username,a.tshengj_time,a.tshij_username,a.tshij_time,a.tquj_username,a.tquj_time,b.fid as sid,b.fadowner as fadownername')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
      ->join('tbn_case_send snd on snd.fillegal_ad_id=b.fid and snd.fstatus = 30')
      ->where($where_tia)
      ->order('a.id desc')
      ->page($p,$pp)
      ->select();
    
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 复核处理
  * by zw
  */
  public function fhchuli(){
    $tregionlevel = getconfig('tregionlevel');//到客户机构等级终止提交复核
    $system_num = getconfig('system_num');
    $datacheck_isfile = getconfig('datacheck_isfile');//数据复核时是否强制上传文件，0否，1强制所有级别用户，2强制非顶级用户
    $datacheck_istext = getconfig('datacheck_istext');//数据复核是否有意见输入，0没有，1所有都有，2顶级有，3所有都有并强制要求输入，4顶级有并强制要求输入

    $selfid     = I('selfid');//复核表ID组
    $attachinfo = I('attachinfo');//上传的文件信息
    $result     = I('result');//处理结果
    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    $regionid = substr(session('regulatorpersonInfo.regionid'),0,2);
    $selnote = I('selnote');//审核意见
    
    $fregulatorppid = session('regulatorpersonInfo.fregulatorppid');
    if(strlen($fregulatorppid) == 8){
      $fregulatorppregion = substr($fregulatorppid,2,6);
      $tregion_len = get_tregionlevel($fregulatorppregion);
      if($tregion_len == 1){//国家级
        $fregulatorppregion = 30;
      }elseif($tregion_len == 2){//省级
        $fregulatorppregion = 20;
      }elseif($tregion_len == 4){//市级
        $fregulatorppregion = 10;
      }elseif($tregion_len == 6){//县级
        $fregulatorppregion = 0;
      }
    }else{
      $fregulatorppregion = $fregulatorlevel + 10;
    }

    $attach     = [];
    if(empty($selfid)){
      $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
    }
    if(empty($attachinfo)){
      if($datacheck_isfile == 1 || ($datacheck_isfile == 2 && $tregionlevel != ($fregulatorlevel + 10))){
        $this->ajaxReturn(array('code'=>1,'msg'=>'请上传文件'));
      }
    }
    if(empty($selnote)){
      if(($datacheck_istext == 3 || ($datacheck_istext == 4 && $tregionlevel == ($fregulatorlevel + 10))) && $result == '不同意'){
        $this->ajaxReturn(array('code'=>1,'msg'=>'请输入审核意见'));
      }
      $selnote = $result;//审核意见
    }
    foreach ($selfid as $key => $value) {
      $where_tia['id'] = $value;
      if($fregulatorlevel == 30){
        $where_tia['tcheck_jingdu'] = $fregulatorlevel;//复核进度
      }else{
        $where_tia['tregionid']     = $regionid;//区划
        $where_tia['tcheck_jingdu'] = $fregulatorlevel;//复核进度
      }
      $where_tia['tcheck_status'] = 0;//处理状态

      $do_tia = M('tbn_data_check')->field('fillegal_ad_id,tkuayu_status,fad_name,fregion_id')->join('tbn_illegal_ad on tbn_illegal_ad.fid=tbn_data_check.fillegal_ad_id and fcustomer="'.$system_num.'"')->where($where_tia)->find();//是否有权限处理
      if(!empty($do_tia)){
        if($fregulatorlevel == 30){//国家级
          $data_tdc['tgj_treid']     = session('regulatorpersonInfo.fregulatorpid');
          $data_tdc['tgj_trename']   = session('regulatorpersonInfo.regulatorpname');
          $data_tdc['tgj_username']  = session('regulatorpersonInfo.fname');
          $data_tdc['tgj_time']      = date('Y-m-d H:i:s');
          $data_tdc['tgj_result']    = $selnote;
          $data_tdc['tcheck_jingdu']  = $fregulatorppregion;

          if($result == '同意'){
            $data_tdc['tcheck_status']  = 10;
            M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核成功
            //跨域数据的复核同意需要将其他未处理的违法广告数据一起进行复核处理
            // if($do_tia['tkuayu_status']==10){
            //   M()->execute('update tbn_illegal_ad set fview_status=10,fstatus3=30,fstatus=30,fparticipate_regids='.session('regulatorpersonInfo.fregulatorpid').' where fad_name="'.$do_tia['fad_name'].'" and fstatus=0 and fcustomer="'.$system_num.'"');//复核成功
            // }
          }else{
            $data_tdc['tcheck_status']  = 20;
            M()->execute('update tbn_illegal_ad set fstatus3=20,fstatus=0 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核失败
            M()->execute('update tbn_case_send set fstatus = 0 where fillegal_ad_id = '.$do_tia['fillegal_ad_id'].' and fstatus = 30');
          }
          M('tbn_data_check')->where($where_tia)->save($data_tdc);
        }elseif($fregulatorlevel == 20){//省级
          //查看广告是否跨省级地域，跨省需要交由总局处理
          $where_tia2['_string'] = ' fcustomer = "'.$system_num.'" and left(fregion_id,2)<>"'.substr(session('regulatorpersonInfo.regionid'),0,2).'" and fad_name = "'.$do_tia['fad_name'].'" ';
          $do_tia2 = M('tbn_illegal_ad')
            ->where($where_tia2)
            ->find();
          if(!empty($do_tia2)){
            $isksdy = 10;//跨了省
          }else{
            $isksdy = 0;//未跨省
          }
          
          if($tregionlevel==30 || $isksdy == 0){
            $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshengj_result']    = $selnote;
            $data_tdc['tcheck_jingdu']     = $fregulatorppregion;
            if($result == '同意'){
              $data_tdc['tcheck_status']  = 10;
              M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核成功
            }else{
              $data_tdc['tcheck_status']  = 20;
              M()->execute('update tbn_illegal_ad set fstatus3=20,fstatus=0 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核失败
              M()->execute('update tbn_case_send set fstatus = 0 where fillegal_ad_id = '.$do_tia['fillegal_ad_id'].' and fstatus = 30');
            }
          }else{
            $data_tdc['tshengj_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshengj_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshengj_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshengj_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshengj_result']    = $selnote;
            if($result == '不同意'){
              $data_tdc['tcheck_status']  = 20;
              M()->execute('update tbn_illegal_ad set fstatus3=20,fstatus=0 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核失败
              M()->execute('update tbn_case_send set fstatus = 0 where fillegal_ad_id = '.$do_tia['fillegal_ad_id'].' and fstatus = 30');
            }else{
              $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            }
          }
          M('tbn_data_check')->where($where_tia)->save($data_tdc);
        }elseif($fregulatorlevel == 10){//市级
          if($tregionlevel==20){
            $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshij_result']    = $selnote;
            $data_tdc['tcheck_jingdu']   = $fregulatorppregion;
            if($result == '同意'){
              $data_tdc['tcheck_status']  = 10;
              M()->execute('update tbn_illegal_ad set fstatus3=30 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核成功
            }else{
              $data_tdc['tcheck_status']  = 20;
              M()->execute('update tbn_illegal_ad set fstatus3=20,fstatus=0 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核失败
              M()->execute('update tbn_case_send set fstatus = 0 where fillegal_ad_id = '.$do_tia['fillegal_ad_id'].' and fstatus = 30');
            }
          }else{
            $data_tdc['tshij_treid']     = session('regulatorpersonInfo.fregulatorpid');
            $data_tdc['tshij_trename']   = session('regulatorpersonInfo.regulatorpname');
            $data_tdc['tshij_username']  = session('regulatorpersonInfo.fname');
            $data_tdc['tshij_time']      = date('Y-m-d H:i:s');
            $data_tdc['tshij_result']    = $selnote;
            if($result == '不同意'){
              $data_tdc['tcheck_status']  = 20;
              M()->execute('update tbn_illegal_ad set fstatus3=20,fstatus=0 where fid='.$do_tia['fillegal_ad_id'].' and fcustomer="'.$system_num.'"');//复核失败
              M()->execute('update tbn_case_send set fstatus = 0 where fillegal_ad_id = '.$do_tia['fillegal_ad_id'].' and fstatus = 30');
            }else{
              $data_tdc['tcheck_jingdu']  = $fregulatorppregion;
            }
          }
          
          M('tbn_data_check')->where($where_tia)->save($data_tdc);
        }

        //上传附件
        $attach_data['fillegal_ad_id']    = $do_tia['fillegal_ad_id'];//违法广告
        $attach_data['fcheck_id']         = $value;//复核表Id
        $attach_data['fupload_trelevel']  = $fregulatorlevel;//上传机构级别
        $attach_data['fupload_treid']     = session('regulatorpersonInfo.fregulatorpid');//机构ID
        $attach_data['fupload_trename']   = session('regulatorpersonInfo.regulatorpname');//机构名
        $attach_data['fuploader']         = session('regulatorpersonInfo.fname');//上传人
        $attach_data['fupload_time']      = date('Y-m-d H:i:s');//上传时间
        foreach ($attachinfo as $key2 => $value2){
          $attach_data['fattach']     = $value2['fattachname'];
          $attach_data['fattach_url'] = $value2['fattachurl'];
          array_push($attach,$attach_data);
        }
      }
    }
    if(!empty($attach)){
      M('tbn_data_check_attach')->addAll($attach);
    }
    D('Function')->write_log('数据复核处理',1,'复核成功','tbn_data_check',0,M('tbn_data_check')->getlastsql());
    $this->ajaxReturn(array('code'=>0,'msg'=>'复核成功'));
  }

  /**
  * 查看复核证据
  * by zw
  */
  public function fjjieguo(){
    $fid = I('fid');//复核表ID
    $fupload_trelevel = I('fupload_trelevel');//机构级别

    $where_tdca['id'] = $fid;
    $do_tdca = M('tbn_data_check')
      ->field('tgj_result,tshengj_result,tshij_result,tquj_result')
      ->where($where_tdca)
      ->find();
    if($fupload_trelevel == 0){
      $result = $do_tdca['tquj_result'];
    }elseif($fupload_trelevel == 10){
      $result = $do_tdca['tshij_result'];
    }elseif($fupload_trelevel == 20){
      $result = $do_tdca['tshengj_result'];
    }elseif($fupload_trelevel == 30){
      $result = $do_tdca['tgj_result'];
    }

    $where_tia['a.fcheck_id']         = $fid;
    $where_tia['a.fstate']            = 0;
    $where_tia['a.fupload_trelevel']  = $fupload_trelevel;

    //复核证据附件
    $do_tia = M('tbn_data_check_attach')->field('a.fattach,a.fattach_url')->alias('a')->where($where_tia)->order('a.fid asc')->select();

    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','zjfjdata'=>$do_tia,'result'=>$result));
  }
}
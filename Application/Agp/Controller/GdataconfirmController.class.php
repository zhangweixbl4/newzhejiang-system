<?php
namespace Agp\Controller;
use Think\Controller;

class GdataconfirmController extends BaseController{

    /**
     * 按地区数据发布确认列表
     * by zw
     */
    public function dq_confirm_list(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $system_num = getconfig('system_num');
        $isexamine = getconfig('isexamine');
        $isrelease = getconfig('isrelease');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

        $area           = I('area');//所属地区，例110000
        $mclass         = I('mclass')?I('mclass'):0;//媒介类型，0全部，1电视，2广播，3报纸
        $medianame      = I('medianame');//媒体名称，例CCTV-1
        $sendstatus     = I('sendstatus')?I('sendstatus'):1;//发布状态，1待确认，2已确认
        $fadname        = I('fadname');//广告名称，例三鹿
        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $fadclasscode   = I('fadclasscode');// 广告内容类别组

        $wheres1 = '1=1';


        if(!empty($area)){
            $wheres1 .= ' and tbn_illegal_ad.fregion_id = '.$area;
        }

        if(!empty($mclass)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fmedia_class = '.$mclass;
        }

        if(!empty($medianame)){
            $wheres1 .= ' and tmedia.fmedianame like "%'.$medianame.'%"';
        }

        if(!empty($fadclasscode)){
            $fadclass = [];
            foreach ($fadclasscode as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $wheres1 .= ' and left(tbn_illegal_ad.fad_class_code,2) in('.implode(',', $fadclass).')';
        }

        if($sendstatus >=1 && $sendstatus <= 3){
            if($sendstatus == 1){
                if($isrelease == 2){
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status in(0,1) ';
                }else{
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
                }
            }else{
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
            }
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'参数错误'));
        }
        if(!empty($fadname)){
            $wheres1 .= ' and tbn_illegal_ad.fad_name like "%'.$fadname.'%"';
        }
        if($isexamine == 10 || $isexamine == 20){
          $wheres1 .= ' and tbn_illegal_ad.fexamine = 10';
        }
        if(!empty($illDistinguishPlatfo)){
          $wheres1 .= ' and left(tmedia.fmediaclassid,2) in('.implode(',', $media_class).')';
        }
        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }

        if($system_num == '100000'){
            $where_level = ' AND tbn_illegal_ad.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
        }
        //查询列表信息
        $data = M()->query('
            SELECT adissuecount,mediacount,b.fid,fname1
            FROM (
            SELECT fregion_id, sum(fquantity) AS adissuecount, COUNT(DISTINCT(tbn_illegal_ad.fmedia_id)) AS mediacount
            FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" AND
            tmedia.fid=tbn_illegal_ad_issue.fmedia_id AND tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1 '.$where_level.' and '.$wheres1.'
            GROUP BY fregion_id) a,tregion b
            WHERE
             a.fregion_id=b.fid
            ORDER BY b.fid ASC
        ');

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
    }

    /**
     * 按媒体数据发布确认列表
     * by zw
     */
    public function mt_confirm_list(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $system_num = getconfig('system_num');
        $isexamine = getconfig('isexamine');
        $isrelease = getconfig('isrelease');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    
        $p = I('page', 1);//当前第几页
        $pp = 20;//每页显示多少记录

        $area           = I('area');//所属地区，例110000
        $mclass         = I('mclass')?I('mclass'):0;//媒介类型，0全部，1电视，2广播，3报纸
        $medianame      = I('medianame');//媒体名称，例CCTV-1
        $sendstatus     = I('sendstatus')?I('sendstatus'):1;//发布状态，1待确认，2已确认
        $fadname        = I('fadname');//广告名称，例三鹿
        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $fadclasscode   = I('fadclasscode');// 广告内容类别组

        $wheres1 = '1=1';
        $wheres2 = '1=1';

        if(!empty($area)){
            $wheres1 .= ' and tbn_illegal_ad.fregion_id = '.$area;
        }

        if(!empty($mclass)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fmedia_class = '.$mclass;
        }

        if(!empty($medianame)){
            $wheres2 .= ' and b.fmedianame like "%'.$medianame.'%"';
        }
        if(!empty($illDistinguishPlatfo)){
          $wheres2 .= ' and left(b.fmediaclassid,2) in('.implode(',', $media_class).')';
        }

        if(!empty($fadclasscode)){
            $fadclass = [];
            foreach ($fadclasscode as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $wheres1 .= ' and left(tbn_illegal_ad.fad_class_code,2) in('.implode(',', $fadclass).')';
        }

        if($sendstatus >=1 && $sendstatus <= 3){
            if($sendstatus == 1){
                if($isrelease == 2){
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status in(0,1) ';
                }else{
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
                }
            }else{
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
            }
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'参数错误'));
        }

        if(!empty($fadname)){
            $wheres1 .= ' and tbn_illegal_ad.fad_name like "%'.$fadname.'%"';
        }

        if($isexamine == 10 || $isexamine == 20){
          $wheres1 .= ' and tbn_illegal_ad.fexamine = 10';
        }

        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }
        if($system_num == '100000'){
            $where_level = ' AND tbn_illegal_ad.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
        }

        //查询总量
        $count = M()->query('
            SELECT count(*) as mediacount
            FROM (
            SELECT tbn_illegal_ad_issue.fmedia_id,tbn_illegal_ad_issue.fmedia_class
            FROM tbn_illegal_ad_issue,tbn_illegal_ad,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" '.$where_level.' and '.$wheres1.'
            GROUP BY tbn_illegal_ad_issue.fmedia_id,tbn_illegal_ad_issue.fmedia_class) a,tmedia b,tregion d
            WHERE a.fmedia_id=b.fid AND b.fid=b.main_media_id and b.fstate = 1 AND b.media_region_id=d.fid  and '.$wheres2.'
        ');

        //查询列表信息
        $data = M()->query('
            SELECT a.*, (case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) as fmedianame,d.fname1
            FROM (
            SELECT tbn_illegal_ad_issue.fmedia_id,tbn_illegal_ad_issue.fmedia_class, sum(fquantity) as adissuecount
            FROM tbn_illegal_ad_issue,tbn_illegal_ad,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" '.$where_level.' and '.$wheres1.'
            GROUP BY tbn_illegal_ad_issue.fmedia_id,tbn_illegal_ad_issue.fmedia_class) a,tmedia b,tregion d
            WHERE a.fmedia_id=b.fid AND b.fid=b.main_media_id and b.fstate = 1 AND b.media_region_id=d.fid and '.$wheres2.'
            ORDER BY d.fid asc,b.fmediaclassid asc
            LIMIT '.($p-1)*$pp.','.$pp
        );

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data,'count'=>$count[0]['mediacount']));
    }

    /**
     * 按广告数据发布确认列表
     * by zw
     */
    public function gg_confirm_list(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $system_num = getconfig('system_num');
        $isexamine = getconfig('isexamine');
        $isrelease = getconfig('isrelease');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

        $p = I('page', 1);//当前第几页
        $pp = 20;//每页显示多少记录

        $area           = I('area');//所属地区，例110000
        $mclass         = I('mclass')?I('mclass'):0;//媒介类型，0全部，1电视，2广播，3报纸
        $medianame      = I('medianame');//媒体名称，例CCTV-1
        $sendstatus     = I('sendstatus')?I('sendstatus'):1;//发布状态，1待确认，2已确认
        $fstate         = I('fstate')?I('fstate'):0;//检查状态，0未检查，10无异议，20有异议
        $fadname        = I('fadname');//广告名称，例三鹿
        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $fadclasscode   = I('fadclasscode');// 广告内容类别组

        $wheres1 = '1=1';
        $wheres2 = '1=1';

        if(!empty($area)){
            $wheres1 .= ' and tbn_illegal_ad.fregion_id = '.$area;
        }

        if(!empty($mclass)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fmedia_class = '.$mclass;
        }

        if(!empty($medianame)){
            $wheres2 .= ' and b.fmedianame like "%'.$medianame.'%"';
        }
        if(!empty($illDistinguishPlatfo)){
          $wheres2 .= ' and left(b.fmediaclassid,2) in('.implode(',', $media_class).')';
        }

        if(!empty($fadclasscode)){
            $fadclass = [];
            foreach ($fadclasscode as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $wheres1 .= ' and left(tbn_illegal_ad.fad_class_code,2) in('.implode(',', $fadclass).')';
        }

        if($sendstatus >=1 && $sendstatus <= 3){
            if($sendstatus == 1){
                if($isrelease == 2){
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status in(0,1) ';
                }else{
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
                }
            }else{
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
            }
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'参数错误'));
        }

        if(!empty($fadname)){
            $wheres1 .= ' and tbn_illegal_ad.fad_name like "%'.$fadname.'%"';
        }

        if($isexamine == 10 || $isexamine == 20){
          $wheres1 .= ' and tbn_illegal_ad.fexamine = 10';
        }

        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }

        if($system_num == '100000'){
            $where_level = ' AND tbn_illegal_ad.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
        }
       
        //查询总记录数
        $count = M()->query('
           SELECT count(*) as fadcount
            FROM
             (
            SELECT fad_name,fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class
            FROM tbn_illegal_ad_issue,tbn_illegal_ad,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" '.$where_level.'  and '.$wheres1.'
            GROUP BY tbn_illegal_ad.fad_name,tbn_illegal_ad_issue.fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class) a,tmedia b,tregion d,tadclass f,tbn_illegal_ad e left join tbn_data_inspectlog dig on e.fid=dig.dig_lid
            WHERE e.fmedia_id=b.fid AND b.fid=b.main_media_id and b.fstate = 1 AND e.fad_class_code=f.fcode AND b.media_region_id=d.fid AND a.fillegal_ad_id=e.fid  and '.$wheres2.'
        ');
        
        //查询列表信息
        $data = M()->query('
            SELECT a.*,d.fname1, (case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) as fmedianame,f.ffullname as fadclass,e.fillegal_code,e.fillegal,e.fexpressions,ifnull(dig.dig_status,0) as dig_status,dig.dig_content,'.$sendstatus.' as sendstatus,fstarttime,fendtime
            FROM
             (
            SELECT fad_name,fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class, sum(fquantity) AS adissuecount,DATE_FORMAT(min(tbn_illegal_ad_issue.fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(tbn_illegal_ad_issue.fissue_date),"%Y-%m-%d") as fendtime
            FROM tbn_illegal_ad_issue,tbn_illegal_ad,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" '.$where_level.' and '.$wheres1.'
            GROUP BY tbn_illegal_ad.fad_name,tbn_illegal_ad_issue.fillegal_ad_id,tbn_illegal_ad_issue.fmedia_class) a,tmedia b,tregion d,tadclass f,tbn_illegal_ad e left join tbn_data_inspectlog dig on e.fid=dig.dig_lid
            WHERE e.fmedia_id=b.fid AND b.fid=b.main_media_id and b.fstate = 1 AND e.fad_class_code=f.fcode AND b.media_region_id=d.fid AND a.fillegal_ad_id=e.fid  and '.$wheres2.'
            ORDER BY d.fid ASC,b.fmediaclassid ASC
            LIMIT '.($p-1)*$pp.','.$pp
        );

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data,'count'=>$count[0]['fadcount']));
    }

    public function logstr($fsendstatus){
        //发布操作记录
        if($fsendstatus == 2){
            $fsendstatuslog = '确认生效';
        }elseif($fsendstatus == 3){
            $fsendstatuslog = '确认忽略';
        }
        $logstr = '【'.session('regulatorpersonInfo.regulatorpname').'】'.session('regulatorpersonInfo.fname').'(ID:'.session('regulatorpersonInfo.fid').') 于'.date('Y-m-d H:i:s').$fsendstatuslog.'；';
        return $logstr;
    }

    /**
     * 按地区发布确认
     * by zw
     */
    public function dq_confirm_action(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $system_num = getconfig('system_num');
        $isexamine = getconfig('isexamine');
        $isrelease = getconfig('isrelease');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

        $area           = I('area');//所属地区，例110000
        $mclass         = I('mclass')?I('mclass'):0;//媒介类型，0全部，1电视，2广播，3报纸
        $medianame      = I('medianame');//媒体名称，例CCTV-1
        $fadname        = I('fadname');//广告名称，例三鹿
        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $fadclasscode   = I('fadclasscode');// 广告内容类别组

        $fsendstatus    = I('fsendstatus');//发布状态，2确认发布，3确认不发布
        $selarea        = I('selarea');//选择地区
        $selstatus      = I('selstatus');//全选状态，0不全选，1全选
        $selstatus2     = I('selstatus2');//是否排除，0不排除，1排除

        if($fsendstatus!=2 && $fsendstatus!=3){
            $this->ajaxReturn(array('code'=>1,'msg'=>'处理失败'));
        }

        $wheres1 = '1=1 and fsend_status<>'.$fsendstatus;
        $wheres2 = '1=1';

        if(!empty($area)){
            $wheres1 .= ' and tbn_illegal_ad.fregion_id = '.$area;
        }

        if(!empty($mclass)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fmedia_class = '.$mclass;
        }

        if(!empty($medianame)){
            $wheres1 .= ' and tmedia.fmedianame like "%'.$medianame.'%"';
        }

        if(!empty($fadclasscode)){
            $fadclass = [];
            foreach ($fadclasscode as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $wheres1 .= ' and left(tbn_illegal_ad.fad_class_code,2) in('.implode(',', $fadclass).')';
        }

        if(!empty($fadname)){
            $wheres1 .= ' and tbn_illegal_ad.fad_name like "%'.$fadname.'%"';
        }

        if($isexamine == 10 || $isexamine == 20){
          $wheres1 .= ' and tbn_illegal_ad.fexamine = 10';
        }

        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }
        if(!empty($illDistinguishPlatfo)){
          $wheres1 .= ' and left(tmedia.fmediaclassid,2) in('.implode(',', $media_class).')';
        }

        if(empty($selstatus)){
            if(empty($selarea)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
            }
            if(empty($selstatus2)){
                $wheres2 .= ' and tregion.fid in ('.implode(',', $selarea).')';
            }else{
                $wheres2 .= ' and tregion.fid not in ('.implode(',', $selarea).')';
            }
        }
        if($system_num == '100000'){
            $where_level = ' AND tbn_illegal_ad.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
        }
        
        $isreleasesms = getconfig('isreleasesms');
        if(!empty($isreleasesms)){
            $smssql = 'select c.fgetsmstype,c.fmobile from (select tregion.fid
                FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,tregion,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" AND
                tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1 AND
                tmedia.fid=tbn_illegal_ad_issue.fmedia_id AND
                tregion.fid=tbn_illegal_ad.fregion_id '.$where_level.' and '.$wheres2.' and '.$wheres1.' group by tregion.fid) a,tregulator b,tregulatorperson c
                where a.fid = b.fregionid and b.fid = c.fregulatorid and length(c.fmobile) = 11 and fgetsmstype <> ""';

            $smsdata = M()->query($smssql);
            if(!empty($smsdata)){
                $smsphone = [];
                foreach ($smsdata as $key => $value) {
                    if(in_array(1,explode('|',$value['fgetsmstype']))){
                        $smsphone[] = $value['fmobile'];
                    }
                }
                $this->user_sendsms($smsphone,'你单位在省级广告监测平台中有涉嫌违法广告线索，请登录查看处理。');
            }
        }

        //更新违法表数据的派发时间
        $sendtimesql = 'update tbn_illegal_ad set fsend_datetime = "'.date('Y-m-d H:i:s').'" where fsend_datetime is null and fid in(select fid from (SELECT tbn_illegal_ad.fid
            FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,tregion,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd 
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" AND
            tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1 AND
            tmedia.fid=tbn_illegal_ad_issue.fmedia_id AND
            tregion.fid=tbn_illegal_ad.fregion_id '.$where_level.' and '.$wheres2.' and '.$wheres1.' group by tbn_illegal_ad.fid) a)';
        $dosend = M()->execute($sendtimesql);

        //更新违法发布表的发布状态
        if(!empty($dosend)){
            M()->execute('update tbn_illegal_ad_issue set fsend_status = '.$fsendstatus.',fsend_log = CONCAT(fsend_log,"'.$this->logstr($fsendstatus).'") where fid in
                (select fid from (SELECT tbn_illegal_ad_issue.fid
                FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,tregion,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id and tbn_illegal_ad.fcustomer="'.$system_num.'" AND
                tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1 AND
                tmedia.fid=tbn_illegal_ad_issue.fmedia_id AND
                tregion.fid=tbn_illegal_ad.fregion_id '.$where_level.' and '.$wheres2.' and '.$wheres1.'
            ) as a)');
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'处理成功'));
    }

    /**
     * 按媒体发布确认
     * by zw
     */
    public function mt_confirm_action(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $system_num = getconfig('system_num');
        $isexamine = getconfig('isexamine');
        $isrelease = getconfig('isrelease');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

        $area           = I('area');//所属地区，例110000
        $mclass         = I('mclass')?I('mclass'):0;//媒介类型，0全部，1电视，2广播，3报纸
        $medianame      = I('medianame');//媒体名称，例CCTV-1
        $fadname        = I('fadname');//广告名称，例三鹿
        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $fadclasscode   = I('fadclasscode');// 广告内容类别组

        $fsendstatus    = I('fsendstatus');//发布状态，2确认发布，3确认不发布
        $selmedia       = I('selmedia');//选择媒体
        $selstatus      = I('selstatus');//全选状态，0不全选，1全选
        $selstatus2     = I('selstatus2');//是否排除，0不排除，1排除

        if($fsendstatus!=2 && $fsendstatus!=3){
            $this->ajaxReturn(array('code'=>1,'msg'=>'处理失败'));
        }

        $wheres1 = '1=1 and fsend_status<>'.$fsendstatus;

        if(!empty($area)){
            $wheres1 .= ' and a2.fregion_id = '.$area;
        }

        if(!empty($mclass)){
            $wheres1 .= ' and a.fmedia_class = '.$mclass;
        }

        if(!empty($medianame)){
            $wheres1 .= ' and b.fmedianame like "%'.$medianame.'%"';
        }

        if(!empty($fadclasscode)){
            $fadclass = [];
            foreach ($fadclasscode as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $wheres1 .= ' and left(tbn_illegal_ad.fad_class_code,2) in('.implode(',', $fadclass).')';
        }

        if(!empty($fadname)){
            $wheres1 .= ' and a2.fad_name like "%'.$fadname.'%"';
        }

        if($isexamine == 10 || $isexamine == 20){
          $wheres1 .= ' and a2.fexamine = 10';
        }

        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and a.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }

        if(empty($selstatus)){
            if(empty($selmedia)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
            }
            if(empty($selstatus2)){
                $wheres1 .= ' and a.fmedia_id in ('.implode(',', $selmedia).')';
            }else{
                $wheres1 .= ' and a.fmedia_id not in ('.implode(',', $selmedia).')';
            }
        }
        if($system_num == '100000'){
            $where_level = ' AND a2.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
        }
        if(!empty($illDistinguishPlatfo)){
          $wheres1 .= ' and left(b.fmediaclassid,2) in('.implode(',', $media_class).')';
        }

        $isreleasesms = getconfig('isreleasesms');
        if(!empty($isreleasesms)){
            $smssql = 'select c.fgetsmstype,c.fmobile from (select d.fid
                FROM tbn_illegal_ad_issue a,tbn_illegal_ad a2,tmedia b,tregion d,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE a2.fid=a.fillegal_ad_id and a.fmedia_id=a2.fmedia_id and a2.fid = snd.illad_id and a2.fcustomer="'.$system_num.'" AND
                b.fid=b.main_media_id and b.fstate = 1 AND
                a.fmedia_id=b.fid AND
                a2.fregion_id=d.fid '.$where_level.' and '.$wheres1.' group by d.fid) a,tregulator b,tregulatorperson c where a.fid = b.fregionid and b.fid = c.fregulatorid and length(c.fmobile) = 11 and fgetsmstype <> ""';
            $smsdata = M()->query($smssql);
            if(!empty($smsdata)){
                $smsphone = [];
                foreach ($smsdata as $key => $value) {
                    if(in_array(1,explode('|',$value['fgetsmstype']))){
                        $smsphone[] = $value['fmobile'];
                    }
                }
                $this->user_sendsms($smsphone,'你单位在省级广告监测平台中有涉嫌违法广告线索，请登录查看处理。');
            }
        }

        //更新违法表数据的派发时间
        $sendtimesql = 'update tbn_illegal_ad set fsend_datetime = "'.date('Y-m-d H:i:s').'" where fsend_datetime is null and fid in
                (select fid from (select a2.fid
                FROM tbn_illegal_ad_issue a,tbn_illegal_ad a2,tmedia b,tregion d,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE a2.fid=a.fillegal_ad_id and a.fmedia_id=a2.fmedia_id and a2.fid = snd.illad_id and a2.fcustomer="'.$system_num.'" AND
                b.fid=b.main_media_id and b.fstate = 1 AND
                a.fmedia_id=b.fid AND
                a2.fregion_id=d.fid '.$where_level.' and '.$wheres1.' group by a2.fid) as x)';
        $dosend = M()->execute($sendtimesql);

        //更新违法发布表的发布状态
        if(!empty($dosend)){
            M()->execute('update tbn_illegal_ad_issue set fsend_status = '.$fsendstatus.',fsend_log = CONCAT(fsend_log,"'.$this->logstr($fsendstatus).'") where fid in
                (select fid from (select a.fid
                FROM tbn_illegal_ad_issue a,tbn_illegal_ad a2,tmedia b,tregion d,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE a2.fid=a.fillegal_ad_id and a.fmedia_id=a2.fmedia_id and a2.fid = snd.illad_id and a2.fcustomer="'.$system_num.'" AND
                b.fid=b.main_media_id and b.fstate = 1 AND
                a.fmedia_id=b.fid AND
                a2.fregion_id=d.fid '.$where_level.' and '.$wheres1.') as x)');
        }
        $this->ajaxReturn(array('code'=>0,'msg'=>'处理成功'));

    }

     /**
     * 按广告发布确认
     * by zw
     */
    public function gg_confirm_action(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $system_num = getconfig('system_num');
        $isexamine = getconfig('isexamine');
        $isrelease = getconfig('isrelease');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

        $area           = I('area');//所属地区，例110000
        $mclass         = I('mclass')?I('mclass'):0;//媒介类型，0全部，1电视，2广播，3报纸
        $medianame      = I('medianame');//媒体名称，例CCTV-1
        $fadname        = I('fadname');//广告名称，例三鹿
        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $fadclasscode   = I('fadclasscode');// 广告内容类别组

        $fsendstatus    = I('fsendstatus');//发布状态，2确认发布，3确认不发布
        $selfadid       = I('selfadid');//选择广告id
        $selstatus      = I('selstatus');//全选状态，0不全选，1全选
        $selstatus2     = I('selstatus2');//是否排除，0不排除，1排除

        if($fsendstatus!=2 && $fsendstatus!=3){
            $this->ajaxReturn(array('code'=>1,'msg'=>'处理失败'));
        }

        $wheres1 = '1=1 and fsend_status<>'.$fsendstatus;
      
        if(!empty($area)){
            $wheres1 .= ' and a2.fregion_id = '.$area;
        }

        if(!empty($mclass)){
            $wheres1 .= ' and a.fmedia_class = '.$mclass;
        }

        if(!empty($medianame)){
            $wheres1 .= ' and b.fmedianame like "%'.$medianame.'%"';
        }

        if(!empty($fadclasscode)){
            $fadclass = [];
            foreach ($fadclasscode as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $wheres1 .= ' and left(tbn_illegal_ad.fad_class_code,2) in('.implode(',', $fadclass).')';
        }

        if(!empty($fadname)){
            $wheres1 .= ' and a2.fad_name like "%'.$fadname.'%"';
        }

        if($isexamine == 10 || $isexamine == 20){
          $wheres1 .= ' and a2.fexamine = 10';
        }

        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and a.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }

        if(empty($selstatus)){
            if(empty($selfadid)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'请选择项'));
            }
            if(empty($selstatus2)){
                $wheres1 .= ' and a2.fid in ('.implode(',',$selfadid).')';
            }else{
                $wheres1 .= ' and a2.fid not in ('.implode(',',$selfadid).')';
            }
        }

        if($system_num == '100000'){
            $where_level = ' AND a2.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
        }

        if(!empty($illDistinguishPlatfo)){
          $wheres1 .= ' and left(b.fmediaclassid,2) in('.implode(',', $media_class).')';
        }

        $isreleasesms = getconfig('isreleasesms');
        if(!empty($isreleasesms)){
            $smssql = 'select c.fgetsmstype,c.fmobile from (select d.fid
                FROM tbn_illegal_ad_issue a,tbn_illegal_ad a2,tmedia b,tregion d,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE a2.fid=a.fillegal_ad_id and a.fmedia_id=a2.fmedia_id and a2.fid = snd.illad_id and a2.fcustomer="'.$system_num.'" AND
                b.fid=b.main_media_id and b.fstate = 1 AND
                a.fmedia_id=b.fid AND
                a2.fregion_id=d.fid '.$where_level.' and '.$wheres1.' group by d.fid) a,tregulator b,tregulatorperson c
                where a.fid = b.fregionid and b.fid = c.fregulatorid and length(c.fmobile) = 11 and fgetsmstype <> ""';
            $smsdata = M()->query($smssql);
            if(!empty($smsdata)){
                $smsphone = [];
                foreach ($smsdata as $key => $value) {
                    if(in_array(1,explode('|',$value['fgetsmstype']))){
                        $smsphone[] = $value['fmobile'];
                    }
                }
                $this->user_sendsms($smsphone,'你单位在省级广告监测平台中有涉嫌违法广告线索，请登录查看处理。');
            }
        }

        //更新违法表数据的派发时间
        $sendtimesql = 'update tbn_illegal_ad set fsend_datetime = "'.date('Y-m-d H:i:s').'" where fsend_datetime is null and fid in
                (select fid from (select a2.fid
                FROM tbn_illegal_ad_issue a,tbn_illegal_ad a2,tmedia b,tregion d,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE a2.fid=a.fillegal_ad_id and a.fmedia_id=a2.fmedia_id and a2.fid = snd.illad_id and a2.fcustomer="'.$system_num.'" AND
                b.fid=b.main_media_id and b.fstate = 1 AND
                a.fmedia_id=b.fid AND
                a2.fregion_id=d.fid '.$where_level.' and '.$wheres1.' group by a2.fid) as x)';
        $dosend = M()->execute($sendtimesql);

        //更新违法发布表的发布状态
        if(!empty($dosend)){
            M()->execute('update tbn_illegal_ad_issue set fsend_status = '.$fsendstatus.',fsend_log = CONCAT(fsend_log,"'.$this->logstr($fsendstatus).'") where fid in
                (select fid from (select a.fid
                FROM tbn_illegal_ad_issue a,tbn_illegal_ad a2,tmedia b,tregion d,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
                WHERE a2.fid=a.fillegal_ad_id and a.fmedia_id=a2.fmedia_id and a2.fid = snd.illad_id and a2.fcustomer="'.$system_num.'" AND
                b.fid=b.main_media_id and b.fstate = 1 AND
                a.fmedia_id=b.fid AND
                a2.fregion_id=d.fid '.$where_level.' and '.$wheres1.') as x)');
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'处理成功'));

    }

    /**
     * 发布记录查询
     * by zw
     */
    public function tiaoci_list(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $system_num = getconfig('system_num');
        $isexamine = getconfig('isexamine');
        $isrelease = getconfig('isrelease');
        $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
        $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    
        $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
        $p = I('page', 1);//当前第几页
        $pp = 10;//每页显示多少记录

        $area           = I('area');//所属地区，例110000
        $mclass         = I('mclass')?I('mclass'):0;//媒介类型，0全部，1电视，2广播，3报纸
        $medianame      = I('medianame');//媒体名称，例CCTV-1
        $sendstatus     = I('sendstatus')?I('sendstatus'):1;//发布状态，1待确认，2已确认
        $fadname        = I('fadname');//广告名称，例三鹿
        $fissuedatest   = I('fissuedatest');//发布时间起
        $fissuedateed   = I('fissuedateed');//发布时间止
        $mediaid        = I('mediaid');//媒体ID
        $adid           = I('adid');//违法广告ID
        $fadclasscode   = I('fadclasscode');// 广告内容类别组

        $wheres1 = '1=1';


        if(empty($area)){
            if($fregulatorlevel == 20){
                $wheres1 .= ' and tbn_illegal_ad.fregion_id like "'.substr(session('regulatorpersonInfo.regionid'),0,2).'%"';
            }elseif($fregulatorlevel == 10){
                $wheres1 .= ' and tbn_illegal_ad.fregion_id like "'.substr(session('regulatorpersonInfo.regionid'),0,4).'%"';
            }elseif($fregulatorlevel == 0){
                $wheres1 .= ' and tbn_illegal_ad.fregion_id like "'.substr(session('regulatorpersonInfo.regionid'),0,6).'%"';
            }
        }else{
            $wheres1 .= ' and tbn_illegal_ad.fregion_id = '.$area;
        }

        if(!empty($mclass)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fmedia_class = '.$mclass;
        }

        if(!empty($medianame)){
            $wheres1 .= ' and tmedia.fmedianame like "%'.$medianame.'%"';
        }

        if(!empty($fadclasscode)){
            $fadclass = [];
            foreach ($fadclasscode as $key => $value) {
                if($value<10){
                    $fadclass[] = '0'.$value;
                }else{
                    $fadclass[] = $value;
                }
            }
            $wheres1 .= ' and left(tbn_illegal_ad.fad_class_code,2) in('.implode(',', $fadclass).')';
        }

        if(!empty($mediaid)){
            $wheres1 .= ' and tmedia.fid='.$mediaid;
        }

        if(!empty($adid)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fillegal_ad_id='.$adid;
        }

        if($sendstatus >=1 && $sendstatus <= 3){
            if($sendstatus == 1){
                if($isrelease == 2){
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status in(0,1) ';
                }else{
                    $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
                }
            }else{
                $wheres1 .= ' and tbn_illegal_ad_issue.fsend_status = '.$sendstatus;
            }
        }else{
            $this->ajaxReturn(array('code'=>-1,'msg'=>'参数错误'));
        }

        if(!empty($fadname)){
            $wheres1 .= ' and tbn_illegal_ad.fad_name like "%'.$fadname.'%"';
        }

        if($isexamine == 10 || $isexamine == 20){
          $wheres1 .= ' and tbn_illegal_ad.fexamine = 10';
        }

        if(!empty($fissuedatest) && !empty($fissuedateed)){
            $wheres1 .= ' and tbn_illegal_ad_issue.fissue_date between "'.$fissuedatest.'" and "'.$fissuedateed.'"';
        }
        if($system_num == '100000'){
            $where_level = ' AND tbn_illegal_ad.fregion_id in (select fid from tregion where flevel in (1,2,3)) ';
        }
        if(!empty($illDistinguishPlatfo)){
          $wheres1 .= ' and left(tmedia.fmediaclassid,2) in('.implode(',', $media_class).')';
        }

        //查询总量
        $count = M()->query('
            SELECT count(*) as tccount
            FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,tadclass,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fcustomer="'.$system_num.'" AND
            tmedia.fid=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id AND
            tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1 AND
            tbn_illegal_ad.fad_class_code = tadclass.fcode '.$where_level.' and '.$wheres1.'
        ');

        //查询列表信息
        $data = M()->query('
            SELECT tbn_illegal_ad.fid as sid,
                tbn_illegal_ad.fmedia_class,
                tadclass.ffullname as fadclass,
                tbn_illegal_ad.fad_name,
                 (case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,
                tbn_illegal_ad_issue.fstarttime,
                tbn_illegal_ad_issue.fendtime,
                tbn_illegal_ad_issue.fpage,
                tbn_illegal_ad.fillegal,
                tbn_illegal_ad.fexpressions,
                tbn_illegal_ad.fillegal_code,
                tbn_illegal_ad.favifilename,
                tbn_illegal_ad.fjpgfilename,
                (UNIX_TIMESTAMP(tbn_illegal_ad_issue.fendtime)-UNIX_TIMESTAMP(tbn_illegal_ad_issue.fstarttime)) as difftime
            FROM tbn_illegal_ad,tbn_illegal_ad_issue,tmedia,tadclass,(select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd
            WHERE tbn_illegal_ad.fid=tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fmedia_id=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fcustomer="'.$system_num.'" AND
            tmedia.fid=tbn_illegal_ad_issue.fmedia_id and tbn_illegal_ad.fid = snd.illad_id AND
            tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1 AND
            tbn_illegal_ad.fad_class_code = tadclass.fcode '.$where_level.' and '.$wheres1.'
            ORDER BY fstarttime desc
            LIMIT '.($p-1)*$pp.','.$pp);

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data,'count'=>$count[0]['tccount']));
    }

    // /**
    //  * 随机生成日期
    //  * by zw
    //  */
    // public function create_rndday2(){
    //     $cccount = I('cccount');//抽取天数
    //     $gdtime = I('gdtime')?I('gdtime'):[];//固定时间
    //     $sttime = I('sttime')?I('sttime'):date('Y-m-01');//抽查开始时间
    //     $edtime = I('edtime')?I('edtime'):date('Y-m-d',strtotime($sttime.' +1 month -1 day'));//抽查结束时间

    //     $stday = date('d',strtotime($sttime));
    //     $edday = date('d',strtotime($edtime));
    //     $maxday = get_day_diff($edtime,$sttime);
    //     $oldcount = $cccount;
    //     $rndarr = [];

    //     if(empty($oldcount)){
    //         $oldcount = $maxday;
    //     }

    //     if($maxday < $oldcount || $oldcount < 0 || $maxday <= 0){
    //         $this->ajaxReturn([
    //             'code' => 1,
    //             'msg' => '按照'.$sttime.'至'.$edtime.'的时间区间，最高仅能抽取'.$maxday.'天'
    //         ]);
    //     }

    //     $dayarr = [];
    //     for ($i=($stday-1); $i < $edday; $i++) {
    //         if($i<9){
    //             $daytimes = date('Y-m',strtotime($sttime)).'-0'.($i+1);
    //         }else{
    //             $daytimes = date('Y-m',strtotime($sttime)).'-'.($i+1);
    //         }
    //         if(in_array($daytimes, $gdtime)){
    //             $oldcount = $oldcount-1;
    //             $rndarr[] = $daytimes;
    //         }else{
    //             $dayarr[] = $daytimes;
    //         }
    //     }

    //     $nowcount = count($dayarr);
    //     while ($nowcount > 0 && $oldcount>0){
    //         $nowcount = count($dayarr);
    //         if($nowcount<=0) break;
    //         $nownum = mt_rand(1,$nowcount);//随机数组指针
    //         $rndarr[] = $dayarr[$nownum-1];//将抽取的指针数据存入
    //         array_splice($dayarr, ($nownum-1), 1);//移除并重排数组
    //         $oldcount--;//抽取天数减1
    //     }

    //     if(count($rndarr) != $cccount){
    //         $this->ajaxReturn([
    //             'code' => 1,
    //             'msg' => '抽查天数与实际抽查日期数不符'
    //         ]);
    //     }
    //     sort($rndarr);
    //     $this->ajaxReturn(['code' => 0,'msg' => '获取成功','data'=>$rndarr]);
    // }

    public function create_rndday(){
        $cccount = I('cccount');//抽取天数
        $gdtime = I('gdtime')?I('gdtime'):[];//固定时间
        $sttime = I('sttime')?I('sttime'):date('Y-n-1');//抽查开始时间
        $edtime = I('edtime')?I('edtime'):date('Y-n-j',strtotime($sttime.' +1 month -1 day'));//抽查结束时间
        $rndurl = 'http://47.96.182.117:8002/day_rand';//随机日期接口地址

        // $cccount = 4;
        // $gdtime = ['27','26'];
        // $sttime = '2019-04-26';
        // $edtime = '2019-04-30';

        $stday = (int)date('j',strtotime($sttime));
        $maxday = get_day_diff($edtime,$sttime);
        $rndarr = [];

        if($maxday < $cccount || $cccount <= 0 || $maxday <= 0){
            $this->ajaxReturn([
                'code' => 1,
                'msg' => '按照'.$sttime.'至'.$edtime.'的时间区间，最高仅能抽取'.$maxday.'天'
            ]);
        }

        //获取随机日期
        $params['rand_count'] = $cccount;
        $params['month_count'] = $maxday;
        if(!empty($gdtime)){
            foreach ($gdtime as $key => $value) {
                $gdtime[$key] = $value - $stday +1;
            }
            $params['chosen_day'] = implode(',', $gdtime);
        }
        $rndnum = http($rndurl, $params, 'GET', false, 2);

        $rndnum = str_replace(']', '',str_replace('[', '', $rndnum));
        $dayarr = explode(',',$rndnum);
        foreach ($dayarr as $key => $value) {
            $daytime = $value-1+$stday;
            $daytimes = date('Y-n',strtotime($sttime)).'-'.$daytime;
            $rndarr[] = $daytimes;
        }

        if(count($rndarr) != $cccount){
            $this->ajaxReturn([
                'code' => 1,
                'msg' => '抽查天数与实际抽查日期数不符'
            ]);
        }

        $this->ajaxReturn(['code' => 0,'msg' => '获取成功','data'=>$rndarr]);
    }

    /**
     * 随机生成日期
     * by zw
     */
    public function create_rndday2(){
        $cccount = I('cccount');//抽取天数
        $gdtime = I('gdtime')?I('gdtime'):[];//固定时间
        $sttime = I('sttime')?I('sttime'):date('Y-n-1');//抽查开始时间
        $edtime = I('edtime')?I('edtime'):date('Y-n-j',strtotime($sttime.' +1 month -1 day'));//抽查结束时间

        // $cccount = 15;
        // $gdtime = ['2019-04-01','2019-04-02'];
        // $sttime = '2019-04-01';
        // $edtime = '2019-04-30';

        $stday = (int)date('j',strtotime($sttime));
        $edday = (int)date('j',strtotime($edtime));
        $maxday = get_day_diff($edtime,$sttime);
        $oldcount = $cccount;
        $rndarr = [];

        $zu_count = intval($maxday/$cccount);

        if(empty($oldcount)){
            $oldcount = $maxday;
        }

        if($maxday < $oldcount || $oldcount < 0 || $maxday <= 0){
            $this->ajaxReturn([
                'code' => 1,
                'msg' => '按照'.$sttime.'至'.$edtime.'的时间区间，最高仅能抽取'.$maxday.'天'
            ]);
        }

        $dayarr = [];
        for ($i=($stday-1); $i < $edday; $i++) {
            $daytimes = date('Y-n',strtotime($sttime)).'-'.($i+1);
            $dayarr[] = $daytimes;
        }

        $zu_arr = [];
        foreach ($dayarr as $key => $value) {
            if((($key+1)/$zu_count>$cccount)){
                $zu_arr[$cccount-1][] = $value;
            }else{
                $zu_arr[intval($key/$zu_count)][] = $value;
            }
        }

        foreach ($gdtime as $key => $value) {
            foreach ($zu_arr as $key2 => $value2) {
                if(in_array($value, $value2)){
                    array_splice($zu_arr, $key2, 1);
                }
            }
            $rndarr[] = $value;//固定数据存入数组
            $oldcount --;//抽取天数减1
        }

        $nowcount = count($zu_arr);
        while($nowcount > 0 && $oldcount>0){
            $nowcount = count($zu_arr);
            if($nowcount<=0) break;
            $nownum = mt_rand(1,$nowcount);//随机数组指针
            $nowcount2 = count($zu_arr[$nownum-1]);//计算子数组的数据个数
            $nownum2 = mt_rand(1,$nowcount2);//随机数组指针
            $rndarr[] = $zu_arr[$nownum-1][$nownum2-1];//将随机出的数据存入
            array_splice($zu_arr, ($nownum-1), 1);//重新排序
            $oldcount--;//抽取天数减1
        }

        if(count($rndarr) != $cccount){
            $this->ajaxReturn([
                'code' => 1,
                'msg' => '抽查天数与实际抽查日期数不符'
            ]);
        }
        sort($rndarr);
        $this->ajaxReturn(['code' => 0,'msg' => '获取成功','data'=>$rndarr]);
    }

    /**
     * 创建抽查计划
     * by zw
     */
    public function create_rnplan(){
        if(IS_POST){
            $jingbanren = I('jingbanren');//经办人
            $taskno = I('taskno')?I('taskno'):'广监任'.date('Y年').'第〔 1 〕号';//任务单号
            $fstarttime = I('fstarttime');//开始时间
            $fendtime = I('fendtime');//结束时间
            $gdtime = I('gdtime');//固定时间
            $month = date('Y-m-01',strtotime($fstarttime));//自动换取月份
            $condition = I('condition');//抽查时间，json数据
            // $condition = [
            //     "tv" => ["2019-01-01","2019-01-02","2019-01-03","2019-01-04","2019-01-05"],
            //     "bc" => ["2019-01-01","2019-01-02","2019-01-03","2019-01-04","2019-01-05"],
            //     "paper" => ["2019-01-01","2019-01-02","2019-01-03","2019-01-04","2019-01-15"]
            // ];

            $gdtime2 = [];
            if(!empty($gdtime)){
                foreach ($gdtime as $key => $value) {
                    $gdtime2[] = (string)((int)$value);
                }
                sort($gdtime2);
            }

            foreach ($condition as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $timedata[$key][] = date('j',strtotime($value2));
                }
                sort($timedata[$key]);
                $timedata[$key.'2'] = $gdtime2;
            }

            $cn_arr = json_encode($timedata,true);

            $hasMonth = M('zongju_data_confirm')->where(['fmonth' => ['EQ', $month]])->getField('fid');

            M()->startTrans();
            if (!$hasMonth){
                $data = [
                    'fmonth' => $month
                ];
                $res = M('zongju_data_confirm')->add($data);
                $hasMonth = $res;
            }

            if(!empty($hasMonth)){
                $where_zdc['_string'] = '(fstarttime<="'.$fstarttime.'" and fendtime>="'.$fstarttime.'" or fstarttime<="'.$fendtime.'" and fendtime>="'.$fendtime.'")';
                $do_zdc = M('zongju_data_childconfirm')
                    ->where($where_zdc)
                    ->find();
                if(empty($do_zdc)){
                    $data = [
                        'fstarttime' => $fstarttime,
                        'fendtime' => $fendtime,
                        'fadduser' => session('regulatorpersonInfo.fid'),
                        'jingbanren' => $jingbanren,
                        'taskno' => $taskno,
                        'fcreattime' => date('Y-m-d'),
                        'condition' => $cn_arr,
                        'dmp_confirm_state' => 0,
                        'zj_confirm_state' => 2
                    ];
                    $do_zdc2 = M('zongju_data_childconfirm')->add($data);
                }else{
                    $code = 1;
                    $msg = '计划时间错误';
                }
            }

            if($do_zdc2 && !empty($hasMonth)){
                M()->commit();
                $code = 0;
                $msg = '创建计划成功';

                //任务钉钉提醒
                $pattern='/.*〔(.*?)〕.*/i';
                preg_match_all($pattern,$taskno,$no);//获取单号
                if(!empty($no[1][0])){
                    $nostr = '国家局'.date('Y年',strtotime($fstarttime)).$no[1][0].'号任务单提醒';
                }else{
                    $nostr = '国家局最新任务单提醒';
                }
                $token = '5469d7c29290a65a5bb3356d2219d833181377e689004490861239e1afc63e64';
                $smsstr .= "**计划时间：**".date('Y-n-j',strtotime($fstarttime))."至".date('n-j',strtotime($fendtime))."\n\n";
                $smsstr .= "**电视：**".implode($timedata['tv'], '、')."。\n\n";
                $smsstr .= "**广播：**".implode($timedata['bc'], '、')."。\n\n";
                $smsstr .= "**报纸：**".implode($timedata['paper'], '、')."。\n\n";
                $smsphone = ['18606713323','13758156171','13588258695','18109017112','15867123863'];//提醒用户，纪莉、赵国琪、曹勇、曹席亮、程晋烨
                $ret = push_ddtask("最新推送任务","#### ".$nostr."\n\n > 国家工商总局：\n\n".$smsstr,$smsphone,$token,'markdown');
                D('Function')->write_log('创建抽查计划',1,'创建成功','zongju_data_childconfirm',$do_zdc2,M('zongju_data_childconfirm')->getlastsql());
            }else{
                M()->rollback();
                $code = 1;
                $msg = '计划创建失败，计划范围重叠';
                D('Function')->write_log('创建抽查计划',1,$msg,'zongju_data_childconfirm',$do_zdc2,M('zongju_data_childconfirm')->getlastsql());
            }

            $this->ajaxReturn(compact('code', 'msg'));
        }else{
            $do_zdc = M('zongju_data_childconfirm') ->field('fendtime,taskno,jingbanren') -> order('fendtime desc') -> find();
            if(!empty($do_zdc)){
                $data['fstarttime'] = date("Y-m-d",strtotime("+1 day",strtotime($do_zdc['fendtime'])));
                $data['fendtime'] = date("Y-m-d",strtotime("-1 day",strtotime(date('Y-m-01',strtotime($data['fstarttime'].' +1 month')))));
                $data['jingbanren'] = $do_zdc['jingbanren']?$do_zdc['jingbanren']:'';

                $pattern='/.*〔(.*?)〕.*/i';
                preg_match_all($pattern,$do_zdc['taskno'],$no);//获取单号
                if(!empty($no[1][0])){
                    $data['taskno'] = ((int)$no[1][0])+1;
                }else{
                    $data['taskno'] = 1;
                }
                $this->ajaxReturn($data);
            }else{
                $data['fstarttime'] = date("Y-m-01");
                $data['fendtime'] = date("Y-m-d",strtotime(date('Y-m-01'). ' +1 month -1 day'));
                $data['taskno'] = 1;
                $data['jingbanren'] = '';
                $this->ajaxReturn($data);
            }
        }

    }

    /**
     * 删除抽查计划
     * by zw
     */
    // public function del_rnplan(){
    //     $fid = I('fid');//计划ID

    //     $do_zdc = M('zongju_data_childconfirm')->field('fstarttime')->where(['fid' => $fid,'dmp_confirm_state'=> 0])->find();
    //     if (!empty($do_zdc)){
    //         M()->execute('delete from zongju_data_childconfirm where fid = '.$fid);

    //         //判断是否还有抽查记录，没有的话删除主记录
    //         $fmonth = date('Y-m-01',strtotime($do_zdc['fstarttime']));
    //         $do_count = M('zongju_data_childconfirm')->where(['fid' => ['neq',$fid], 'from_unixtime(fstarttime)' => $fmonth])->count();
    //         if(empty($do_count)){
    //             M()->execute('delete from zongju_data_confirm where fmonth = "'.$fmonth.'"');
    //         }
    //         $this->ajaxReturn([
    //             'code' => 0,
    //             'msg' => '删除成功'
    //         ]);
    //     }else{
    //         $this->ajaxReturn([
    //             'code' => 1,
    //             'msg' => '删除失败'
    //         ]);
    //     }
    // }

    /**
     * 抽查计划列表
     * by zw
     */
    public function ccday_list(){
        Check_QuanXian(['gdatasend']);
        session_write_close();
        $p = I('page', 1);//当前第几页
        $pp = 20;//每页显示多少记录

        $where_zdc['dmp_confirm_state'] = ['neq', 1];
        $where_zdc['fmonth'] = ['egt','2018-07-01'];
        $where_zdc['_string'] = 'fmonth in(select date_format(fstarttime,"%Y-%m-01") from zongju_data_childconfirm where (dmp_confirm_state = 2 or fadduser = '.session('regulatorpersonInfo.fid').'))';

        $count = M('zongju_data_confirm')
            ->alias('a')
            ->where($where_zdc)
            ->count();//查询满足条件的总记录数

        $do_zdc = M('zongju_data_confirm')
            ->alias('a')
            ->field('fid,fmonth,dmp_confirm_state,condition')
            ->where($where_zdc)
            ->order('a.fmonth desc')
            ->page($p,$pp)
            ->select();

        $z_data = [];
        foreach ($do_zdc as $key => $value) {
            $data = [];
            $data['fid'] = $value['fid'];
            $data['fmonth'] = $value['fmonth'];

            $where_dcm['date_format(fstarttime,"%Y-%m-01")'] = $value['fmonth'];
            $where_dcm['zj_confirm_time'] = 0;
            $where_dcm['_string'] = 'dmp_confirm_state = 2 or fadduser = '.session('regulatorpersonInfo.fid');
            $do_dcm = M('zongju_data_childconfirm')
                ->where($where_dcm)
                ->order('fendtime asc')
                ->select();
            $data_child = [];
            if(!empty($do_dcm)){
                foreach ($do_dcm as $key2 => $value2) {
                    $condition2 = json_decode($value2['condition'],true);
                    $data_child[$key2]['tv'] = $condition2['tv']?$condition2['tv']:[];
                    $data_child[$key2]['bc'] = $condition2['bc']?$condition2['bc']:[];
                    $data_child[$key2]['paper'] = $condition2['paper']?$condition2['paper']:[];
                    $data_child[$key2]['tv2'] = $condition2['tv2']?$condition2['tv2']:[];
                    $data_child[$key2]['bc2'] = $condition2['bc2']?$condition2['bc2']:[];
                    $data_child[$key2]['paper2'] = $condition2['paper2']?$condition2['paper2']:[];
                    $data_child[$key2]['stime'] = $value2['fstarttime'];
                    $data_child[$key2]['etime'] = $value2['fendtime'];
                    $data_child[$key2]['state'] = $value2['dmp_confirm_state'];
                    $data_child[$key2]['createtime'] = $value2['fcreattime'];
                    $data_child[$key2]['jingbanren'] = $value2['jingbanren'];
                    $data_child[$key2]['taskno'] = $value2['taskno'];
                }
            }else{
                $condition = json_decode($value['condition'],true);
                $data_child[0]['tv'] = $condition['tv']?$condition['tv']:[];
                $data_child[0]['bc'] = $condition['bc']?$condition['bc']:[];
                $data_child[0]['paper'] = $condition['paper']?$condition['paper']:[];
                $data_child[0]['stime'] = $value['fmonth'];
                $data_child[0]['etime'] = date("Y-m-d",strtotime("-1 day",strtotime($value['fmonth'].' +1 month')));
                $data_child[0]['state'] = 2;
            }
            $data['condition'] = $data_child;
            $z_data[] = $data;
        }
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$z_data)));
    }

    /**
     * 发送短信通知各媒介机构
     */
    public function user_sendsms($phonearr = [],$smscontent = ''){
        if(empty($phonearr) || empty($smscontent)){
            return false;
        }

        foreach ($phonearr as $key => $value) {
            $fhphone = S('fbsj'.$value);
            if(empty($fhphone)){
               S('fbsj'.$value,date('Y-m-d H:i:s'),43200);//半天一提醒
               $url = 'http://10.0.101.102:8888/esb/SMESSAGE/0?authcode=QUhfQU5HR0pDI0BhdXRoQCN6ZGJBUUpsWQ';
               $params['phone'] = $value;
               $params['content'] = $smscontent;
               http($url,json_encode($params),'POST',array('Content-Type:application/json; charset=utf-8'),1);
            }
        }
        return true;
    }

    /*
    *生成任务计划单
    *by zw
    */
    public function create_planword(){
        $taskno = I('taskno')?I('taskno'):'广监任'.date('Y年').'第〔 1 〕号';//任务单号
        $fstarttime = I('fstarttime');//开始时间
        $fendtime = I('fendtime');//结束时间
        $jingbanren = I('jingbanren');//经办人
        $condition = I('condition');//抽查时间，json数据
        $riqi = I('createtime')?date('Y年n月j日',strtotime(I('createtime'))):date('Y年n月j日');
        $jihuashijian = date('Y年n月j日',strtotime($fstarttime)).'至'.date('n月j日',strtotime($fendtime));

        $dianshi = $condition['tv']?$condition['tv']:'未制定';
        $guangbo = $condition['bc']?$condition['bc']:'未制定';
        $baozhi = $condition['paper']?$condition['paper']:'未制定';

        //读取模板
        $file = './Public/Word/ggjcrwdmb.xml';
        $xml = file_get_contents($file);

        $xml = str_replace('{{taskno}}', $taskno, $xml);//任务单号
        $xml = str_replace('{{jihuashijian}}', $jihuashijian, $xml);//计划时间区间
        $xml = str_replace('{{dianshi}}', $dianshi, $xml);//电视抽查时间
        $xml = str_replace('{{guangbo}}', $guangbo, $xml);//广播抽查时间
        $xml = str_replace('{{baozhi}}', $baozhi, $xml);//报纸抽查时间
        $xml = str_replace('{{jingbanren}}', $jingbanren, $xml);//经办人
        $xml = str_replace('{{riqi}}', $riqi, $xml);//当前日期
        file_put_contents("./Public/Word/ggjcrwdmb.doc",$xml);

        $date = date('Ymdhis');
        $savefilename = $date.'.doc';
        $savefile = './Public/Word/ggjcrwdmb.doc';

        $pattern='/.*〔(.*?)〕.*/i';
        preg_match_all($pattern,$taskno,$no);//获取单号
        $downfilename = '广告监测任务单'.$no[1][0].'号';

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$savefilename,$savefile,array('Content-Disposition' => 'attachment;filename='.$downfilename.'.doc'));//上传云
        unlink($savefile);//删除文件
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

    }

}

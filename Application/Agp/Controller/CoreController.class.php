<?php
namespace Agp\Controller;
use Think\Controller;

class CoreController extends BaseController {

	public function get_webname(){
		$ALL_CONFIG = getconfig('ALL');
		$web_name = $ALL_CONFIG['web_name']; 
		$web_logowz = $ALL_CONFIG['web_logowz']; 
		$web_loginwz = $ALL_CONFIG['web_loginwz'];
		$web_loginurl = $ALL_CONFIG['web_loginurl'];
		$web_loginurl2 = $ALL_CONFIG['web_loginurl2'];
		$system_num = $ALL_CONFIG['system_num'];
		$tdata['cljgsb_name'] = $ALL_CONFIG['cljgsb_name'];
		$this->ajaxReturn(array('code'=>0,'data'=>$web_name,'logowz'=>$web_logowz,'loginwz'=>$web_loginwz,'loginurl'=>$web_loginurl,'loginurl2'=>$web_loginurl2,'system_num'=>$system_num,'tdata'=>$tdata));
	}
	
	/*检索 发布媒体*/
	public function get_tregulator_media(){
		$system_num = getconfig('system_num');
		if(I('fmedianame')){
			$where['fmedianame'] = array('like','%'.I('fmedianame').'%');
		}
		$where['a.fstate'] = 1;
		$where['_string'] = ' a.fid = a.main_media_id';

		$data= M('tmedia')
			->alias('a')
			->field('fid as fmediaid,  (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as value')
			->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where)
			->order('a.media_region_id asc,fmediaclassid asc,fmedianame asc')
			->select();
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

	/*获取用户媒体*/
	public function get_user_media(){
		$system_num = getconfig('system_num');

		$mclass=I('mclass');
		if($mclass==1){
			$where['fmediaclassid'] = array('like','01%');
		}elseif($mclass==2){
			$where['fmediaclassid'] = array('like','02%');
		}elseif($mclass==3){
			$where['fmediaclassid'] = array('like','03%');
		}elseif($mclass==4){
			$where['fmediaclassid'] = array('like','13%');
		}
		$where['a.fstate'] = 1;
		$where['_string'] = ' a.fid = a.main_media_id';
		$data = M('tmedia')
			->alias('a')
			->field('a.fid,  (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end)  as value,fmediaclassid')
			->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where)
			->order('a.media_region_id,fmediaclassid asc,fmedianame asc')
			->select();
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

	/*获取该线索的附件*/
	public function get_tillegaladattach(){
		if(I('fregisterid')) {
			$fregisterid = I('fregisterid');//线索ID
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fregisterid不存在！'));
		}
		$where=array();
		$where['fregisterid'] = $fregisterid;
		$data= M('tregisterfile')->where($where)->select();
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

	/**
	 * 获取当前下级机构
	 * @return array|string $count 总数  $data 下级数据
	 * by hs
	 */
	public function get_lower_tregulator(){
		$data = D('Function')->get_lower_tregulatoraction(session('regulatorpersonInfo.fregulatorpid'));
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

	/**
	 * 获取同一机构下所有部门
	 * @return array|string $data 下级数据
	 * by zw
	 */
	public function get_same_tregulator(){
		$data = D('Function')->get_same_tregulatoraction(session('regulatorpersonInfo.fregulatorpid'));
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

	/**
	 * 获取部门下面的人员
	 * by zw
	 */
	public function get_tregulator_person(){
		$treid = I('treid');//部门ID
		$data = M('tregulatorperson')->field('fid,fname')->where(['fregulatorid'=>$treid])->select();
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

	/**
	 * 获取当前机构下的行政区划
	 * $fid 机构ID
	 * @return array|string $data 下级数据
	 * by zw
	 */
	public function get_jigou_nextregion(){
		$fid = I('fid');
		$fregionid = D('Function')->cache(true,600)->get_xzquhua(D('Function')->get_tregulatoraction($fid));
		$data = M('tregion')
			->field('fid,fname1,fpid')
			->where(array('fpid'=>$fregionid,'fstate'=>1))
			->select();
		$this->ajaxReturn(array('code'=>0,'data'=>$data));
	}

	/**
	 * 单个文件上传
	 * @param array $fregisterid 线索ID，$fflowname 流程名 $attachinfo 附件信息
	 * @return array|string $count 总数  $data 数据
	 * by hs
	 */
	public function clue_upload_file(){
		if(I('fregisterid')) {
			$fregisterid = I('fregisterid');//线索ID
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'fregisterid不存在！'));
		}
		if(I('fflowname')) {
			$fflowname = I('fflowname');//流程名
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'$fflowname不存在！'));
		}
			$attachinfo=I('attachinfo');
			foreach($attachinfo as $k =>$v){
				$attachinfo['fattachname']=$v['fattachname'];
				$attachinfo['fattachurl']=$v['fattachurl'];
			}
			$attach_data['fregisterid'] = $fregisterid;//线索id
			$attach_data['fuploadtime'] = date('Y-m-d H:i:s',time());
			$attach_data['fattachuser'] = 1;//1:工商附件 2.媒体附件
			$attach_data['fcreateregualtorid'] = session('regulatorpersonInfo.fregulatorid');//上传机构id
			$attach_data['fflowname'] =$fflowname;//上传机构id
			$attach_data['is_file'] =1;//1，真文件 2 假文件
			$attach_data['is_submit'] =1;//1保存  2提交
			$attach_data['fattachname'] = $attachinfo['fattachname'];
			$attach_data['fattachurl'] = $attachinfo['fattachurl'];
			$attach_data['ffilename'] = preg_replace('/\..*/','',$attachinfo['fattachurl']);
			$attach_data['ffiletype'] = preg_replace('/.*\./','',$attachinfo['fattachname']);
			$attachid = M('tregisterfile')->add($attach_data);
		if($attachid){
			$this->ajaxReturn(array('code'=>0,'msg'=>'操作成功！'));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'操作失败！'));
		}
	}
	
	/**
	 * 按行政区划获取用户媒介
	 * $regionid 行政区划ID组
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function user_medialist(){
		$system_num = getconfig('system_num');
		$regionid 	= I('regionid');
		$iscontain 	= I('iscontain');//是否包含下属地区
		$mclass 	= I('mclass');//媒体类型

		$where['_string'] = '1=1';
		
		if(!empty($mclass)){
			$mclassarr = explode(',', $mclass);
			$where['left(a.fmediaclassid,2)'] = array('in',$mclassarr);
		}
		
		if(!empty($regionid)){
			if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($regionid);
				if($tregion_len == 1){//国家级
					$where['_string'] .= ' and a.media_region_id ='.$regionid;	
				}elseif($tregion_len == 2){//省级
					$where['_string'] .= ' and a.media_region_id like "'.substr($regionid,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$where['_string'] .= ' and a.media_region_id like "'.substr($regionid,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$where['_string'] .= ' and a.media_region_id like "'.substr($regionid,0,6).'%"';
				}
			}else{
				$where['_string'] .= ' and a.media_region_id ='.$regionid;
			}
			
		}else{
			$where['a.media_region_id'] = $regionid;
		}
		$where['_string'] .= ' and a.fid=a.main_media_id';
		$where['a.fstate'] = 1;
		$do_ma = M('tmedia')
			->alias('a')
			->field('a.fid, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
			->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where)
			->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
			->select();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ma));
	}

	/**
	 * 退出登录
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function quietlogin(){
		D('Function')->write_log('用户退出',1,'退出成功','tregulatorperson',session('regulatorpersonInfo.fid')?session('regulatorpersonInfo.fid'):0);
		session_unset();
		session_destroy();
		$this->ajaxReturn(array('code'=>0,'msg'=>'退出成功'));
	}

	/**
	* 同步省级机构的媒体权限
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function tongbumedia(){
		$fid 							= I('fid');
		$where['fpid'] 					= 20100000;
		$where['fkind'] 				= 1;
		$where['fstate'] 				= 1;
		$where['menu_jurisdiction'] 	= '';
		$where['media_jurisdiction'] 	= '';
		if(!empty($fid)){
			$where['fid'] 				= $fid;
		}
		$do_tr = M('tregulator')->field('fid,fcode')->where($where)->select();
		foreach ($do_tr as $key => $value) {
			$menu_jurisdiction 	= D('Function')->get_allmenu2(0,$value['fid']);
			$media_jurisdiction = D('Function')->get_allmedia2($value['fid']);
			if(!empty($menu_jurisdiction)){
				$data['menu_jurisdiction'] = json_encode($menu_jurisdiction);
			}else{
				$data['menu_jurisdiction'] = '';
			}
			if(!empty($media_jurisdiction)){
				$data['media_jurisdiction'] = json_encode($media_jurisdiction);
			}else{
				$data['media_jurisdiction'] = '';
			}
			M('tregulator')->where('fid='.$value['fid'])->save($data);
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'更新成功'));
	}

	/**
	* 获取广告类别列表
	 * by hs
	 */
    public function get_adclass_list(){
		
		$fcode = I('fcode','');//获取fcode
		if($fcode == 0) $fcode = '';
		$adclassList = M('tadclass')->cache(true,86400)->field('fcode,fadclass')->where(array('fpcode'=>$fcode,'fcode'=>array('neq','0')))->select();//查询广告分类列表
		
		$adclassDetails = M('tadclass')->cache(true,86400)->field('fcode,left(fadclass,4) as fadclass,ffullname')->where(array('fcode'=>$fcode))->find();//查询广告分类详情
		
		if(!$adclassDetails) $adclassDetails = array('fcode'=>'','fadclass'=>'全部','ffullname'=>'全部');
		$this->ajaxReturn(array('code'=>0,'adclassList'=>$adclassList,'adclassDetails'=>$adclassDetails));
	}

	/**
	* 一次性获取广告列别列表
	 * by hs
	 */
	public function get_all_adclass_list(){
		$adclassList = M('tadclass')->cache(true,86399)->field('fcode,fpcode,fadclass')->where(array('fstate'=>1))->select();//所有列表
		$this->ajaxReturn(array('code'=>0,'list'=>$adclassList));
	}

	/**
	 * 获取违法类型
	 * by hs
	 */
    public function get_tillegaltype(){
		$data = M('tillegaltype')->field('fcode,fillegaltype')->cache(true,86401)->where(array('fstate' => 1))->select();//查询违法类型
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	 * 根据授权媒体权限获取地区
	 * by zw
	 */
	public function get_tregion(){
		$system_num = getconfig('system_num');

		$where['flevel'] 	= array('in',array(1,2,3,4,5));
		if($system_num == '100000'){
			if(session('regulatorpersonInfo.fregulatorlevel')!=30){
				$where_ms['a.fstate'] = 1;
				$do_ms = M('tmedia')
					->alias('a')
					->field('a.media_region_id')
					->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
					->where($where_ms)
					->group('a.media_region_id')
					->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
					->select();
				$regionids = [];
				$regionid  = [];
				foreach ($do_ms as $key => $value) {
					$regionid = $this->get_upregion($value['media_region_id']);
					$regionids = array_merge($regionids,$regionid);
				}
				if(!empty($regionids)){
					$where['fid'] = array('in',$regionids);
				}else{
					$where['fid'] = session('regulatorpersonInfo.regionid');
				}
			}else{
				$where['flevel'] = array('in',array(0,1,2,3));
			}
		}else{
			if(session('regulatorpersonInfo.fregulatorlevel') == 20){
				$where['fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
			}elseif(session('regulatorpersonInfo.fregulatorlevel') == 10){
				$where['fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
			}elseif(session('regulatorpersonInfo.fregulatorlevel') == 0){
				$where['fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,6).'%');
			}
		}
		$where['fstate'] = 1;
		$data = M('tregion')->field('fid,fpid,fname1 as fname,ffullname')->cache(true,60)->where($where)->order('fid asc')->select();//查询地区
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	 * 获取地域列表
	 * by zw
	 */
	public function tregionList(){
	    $system_num = getconfig('system_num');

	    $regionid = session('regulatorpersonInfo.regionid');//机构行政区划ID
	    $tregion_len = get_tregionlevel($regionid);
	    $res = [];
	    $res1 = [];
	    $res2 = [];
	    $wheres1 = ' and 1=1';
	    if($tregion_len == 1){//国家级
	      $res = M('tregion')->field('fname,fid,fpid')->where('fid='.$regionid.$wheres1)->select();//国家
	      $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid='.$regionid.$wheres1)->select();//国家下面的所有省
	      $res3 = array_merge($res,$res1);
	    }elseif($tregion_len == 2){//省级
	      $res = M('tregion')->field('fname,fid,fpid')->where('fid=100000'.$wheres1)->select();//国家;
	      $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid=100000'.$wheres1)->select();//所有省
	      $res2 = M('tregion')->field('fname,fid,fpid')->where('fpid='.$regionid.$wheres1)->select();//本省下面的所有市
	      $res3 = array_merge($res,$res1,$res2);
	    }elseif($tregion_len == 4){//市级
	      $res = M('tregion')->field('fname,fid,fpid')->where('fid='.substr($regionid , 0 , 2).'0000'.$wheres1)->select();//上级所属省
	      $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid='.substr($regionid , 0 , 2).'0000'.$wheres1)->select();//同级市
	      $res2 = M('tregion')->field('fname,fid,fpid')->where('fpid='.$regionid)->select();//本市下面的所有区县
	      $res3 = array_merge($res,$res1,$res2);
	    }elseif($tregion_len == 6){//县级
	      $res = M('tregion')->field('fname,fid,fpid')->where('fid='.substr($regionid , 0 , 4).'00'.$wheres1)->select();//上级所属市
	      $res1 = M('tregion')->field('fname,fid,fpid')->where('fpid='.substr($regionid , 0 , 4).'00'.$wheres1)->select();//同级区县
	      $res3 = array_merge($res,$res1);
	    }
	    $res3 = !empty($res3)?$res3:[];
	    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$res3));
	}

	/**
	 * 获取全国地区
	 * by zw
	 */
	public function get_tregion2(){

		$where['fstate'] 	= 1;
		$where['flevel'] 	= array('in',array(1,2,3,4,5));
		$where['fpid'] 		= array('neq',-1);
		$data = M('tregion')->field('fid,fpid,fname1 as fname,ffullname')->cache(true,60)->where($where)->order('fid asc')->select();//查询地区
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	 * 获取本机构下的所有地区
	 * by zw
	 */
	public function get_tregion3(){

		$where['fstate'] 	= 1;
		$where['flevel'] 	= array('in',array(1,2,3,4,5));
		$where['fpid'] 		= array('neq',-1);
		if(session('regulatorpersonInfo.fregulatorlevel') == 20){
			$where['fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 10){
			$where['fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
		}elseif(session('regulatorpersonInfo.fregulatorlevel') == 0){
			$where['fid'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,6).'%');
		}
		$data = M('tregion')->field('fid,fpid,fname1 as fname,ffullname')->cache(true,60)->where($where)->order('fid asc')->select();//查询地区
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	 * 通过媒体机构获取地区
	 * by zw
	 */
	public function get_mediatregion(){
		$system_num = getconfig('system_num');

		$where_ms['a.fstate'] = 1;
		$do_ms = M('tmedia')
			->alias('a')
			->field('a.media_region_id')
			->join('tmedia_temp ttp on a.fid=ttp.fmediaid and ttp.ftype=1 and ttp.fcustomer = "'.$system_num.'" and ttp.fuserid='.session('regulatorpersonInfo.fid'))
			->where($where_ms)
			->group('a.media_region_id')
			->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
			->select();
		$regionids = [];
		$regionid  = [];
		foreach ($do_ms as $key => $value) {
			$regionid = $this->get_upregion($value['media_region_id']);
			$regionids = array_merge($regionids,$regionid);
		}
		if(!empty($regionids)){
			$do_ma = M('tregion')
			->alias('a')
			->field('fid,fpid,fname1 as fname')
			->where(array('fid'=>array('in',$regionids)))
			->select();
		}
		$do_ma = $do_ma?$do_ma:[];
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ma));
	}

	/**
	 * 通过媒体机构获取地区，媒体包括交办的
	 * by zw
	 */
	public function get_mediatregion2(){
		$system_num = getconfig('system_num');

		$where_ms['a.fstate'] = 1;
		$do_ms = M('tmedia')
			->alias('a')
			->field('a.media_region_id')
			->join('(select distinct(fmediaid) from tmedia_temp where ftype in(0,1) and tmedia_temp.fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on a.fid=ttp.fmediaid')
			->where($where_ms)
			->group('a.media_region_id')
			->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
			->select();
		$regionids = [];
		$regionid  = [];
		foreach ($do_ms as $key => $value) {
			$regionid = $this->get_upregion($value['media_region_id']);
			$regionids = array_merge($regionids,$regionid);
		}
		if(!empty($regionids)){
			$do_ma = M('tregion')
			->alias('a')
			->field('fid,fpid,fname1 as fname')
			->where(array('fid'=>array('in',$regionids)))
			->select();
		}
		$do_ma = $do_ma?$do_ma:[];
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ma));
	}

	/**
	 * 通过媒体机构获取地区，媒体包括交办和下级的
	 * by zw
	 */
	public function get_mediatregion3(){
		$system_num = getconfig('system_num');

		$where_ms['a.fstate'] = 1;
		$do_ms = M('tmedia')
			->alias('a')
			->field('a.media_region_id')
			->join('(select distinct(fmediaid) from tmedia_temp where ftype in(0,1,2) and tmedia_temp.fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on a.fid=ttp.fmediaid')
			->where($where_ms)
			->group('a.media_region_id')
			->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
			->select();
		$regionids = [];
		$regionid  = [];
		foreach ($do_ms as $key => $value) {
			$regionid = $this->get_upregion($value['media_region_id']);
			$regionids = array_merge($regionids,$regionid);
		}
		if(!empty($regionids)){
			$do_ma = M('tregion')
			->alias('a')
			->field('fid,fpid,fname1 as fname')
			->where(array('fid'=>array('in',$regionids)))
			->select();
		}
		$do_ma = $do_ma?$do_ma:[];
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_ma));
	}

	/**
	 * 获取指定地区的上一级地区（树型）
	 * by zw
	 */
	public function get_upregion($fid,$regionids = []){
		$userlevel = session('regulatorpersonInfo.fregulatorlevel');
		$do_tn = M('tregion')->cache(60)->field('flevel,fpid')->where(['fid'=>$fid,'flevel'=>['gt',0]])->find();
		if(!empty($do_tn)){
			if($do_tn['flevel'] == 1){
				$level = 20;
			}elseif($do_tn['flevel'] == 2 ||$do_tn['flevel'] == 3 ||$do_tn['flevel'] == 4){
				$level = 10;
			}else{
				$level = 0;
			}

			if($userlevel == 20 || $userlevel == 30){
				$userlevel = 20;
			}else{
				$userlevel += 10;
			}

			if($level<=$userlevel){
				$regionids[] = $fid;
				$regionids = $this->get_upregion($do_tn['fpid'],$regionids);
			}
		}
		return $regionids;

	}

	/**
	 * 上传附件参数
	 * by hs
	 */
	public function get_file_up(){
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>file_up()));
	}

	/**
	 * 获取当前用户信息（包含菜单权限）
	 * @return array|string  $data 数据
	 * by zw
	 */
	public function user_menujurisdiction(){
		session_write_close();
		
		$menu = session('regulatorpersonInfo.menujurisdiction');
		if(!empty($menu)){
			$regionid 	= session('regulatorpersonInfo.regionid');
			$ALL_CONFIG = getconfig('ALL',$fcustomer);
			$fjxsgl 	= $ALL_CONFIG['fjxsgl'];//是否线索管理权限，0没有，10有
			$system_num = $ALL_CONFIG['system_num'];//客户ID
			$iswarning = $ALL_CONFIG['iswarning'];//是否含预警功能
			$ischazheng = $ALL_CONFIG['ischazheng'];//是否含查证功能
			$is_show_tj = $ALL_CONFIG['is_show_tj'];//是否查看同级数据
			$is_show_sj = $ALL_CONFIG['is_show_sj'];//是否查看省级数据
			$is_show_count = $ALL_CONFIG['is_show_count'];//是否显示条数相关
			$is_show_clue = $ALL_CONFIG['is_show_clue'];//是否显示立案处理情况
			$isoutdata = $ALL_CONFIG['isoutdata'];//是否展示外部数据
			$jishuzc = $ALL_CONFIG['jishuzc'];//是否有技术支持
			$menuagain = $ALL_CONFIG['menuagain'];//菜单名称和地址重定义
			$generate_report = $ALL_CONFIG['generate_report'];//是否显示生成报告功能
			$isexamine = $ALL_CONFIG['isexamine'];//是否需要数据复核过才能下发
			$index_sub_map = $ALL_CONFIG['index_sub_map'];//首页地图直接显示下级区域（0否1是）
			$isdistribute = $ALL_CONFIG['isdistribute'];//案件线索是否有派发功能，1有，0没有
			$isrelease = $ALL_CONFIG['isrelease'];//是否开启数据发布功能，1开启，0关闭，2开启但不需要抽查计划功能
			$thirdlink = $ALL_CONFIG['thirdlink'];//第三方链接
			$tregionlevel = $ALL_CONFIG['tregionlevel'];//平台级别
			$datacheck_isfile = $ALL_CONFIG['datacheck_isfile'];//数据复核时是否强制上传文件，0否，1强制所有级别用户，2强制非顶级用户
			$illadprint = $ALL_CONFIG['illadprint'];//是否有违法广告打印功能，0无，1有
			$isillegal_summary = $ALL_CONFIG['isillegal_summary'];//首页是否有违法线索量，0无（默认），1有 
			$use_open_search = $ALL_CONFIG['use_open_search'];//是否固定数据，0默认否，10是
			$huizong_iszhdf = $ALL_CONFIG['huizong_iszhdf'];//汇总内是否有综合得分项，1有，0无
			$datacheck_istext = $ALL_CONFIG['datacheck_istext'];//数据复核是否有意见输入，0没有，1所有都有，2顶级有，3所有都有并强制要求输入，4顶级有并强制要求输入
			$media_class = $ALL_CONFIG['media_class']?$ALL_CONFIG['media_class']:[];//平台拥有的媒体类型
			$illadTypes = $ALL_CONFIG['illadTypes']?$ALL_CONFIG['illadTypes']:[];//重点案件线索类型（例：投诉举报，上级交办，部门移交，其他）
			$isShowRegion = $ALL_CONFIG['isShowRegion']?$ALL_CONFIG['isShowRegion']:0;//违法广告线索列表是否展示地区 0不展示，1全部展示，2展示媒体地区，3展示广告主地区
			$illSupervise = $ALL_CONFIG['illSupervise']?$ALL_CONFIG['illSupervise']:0;//是否需要案件处理的执法监督功能，0不需要，1需要
			$monitorCondition	 = $ALL_CONFIG['monitorCondition']?$ALL_CONFIG['monitorCondition']:0;//地方局的首页是否显示线索监管情况，0不显示，1显示
			$stepRegionOrder = $ALL_CONFIG['stepRegionOrder']?$ALL_CONFIG['stepRegionOrder']:0;//线索汇总是否展示跨地域排名，0不显示，1显示，2顶级显示

			if($headers['login_type'] != 'smallprogram'){//小程序登录验证
				$menuagain = json_decode($menuagain,true);
				$data = D('Function')->get_allmenu3($menu);//菜单权限
				$menuallstr = [];
				foreach ($data as $key => $value) {
					if(!empty($menuagain[$value['menu_id']])){
						if(!empty($menuagain[$value['menu_id']]['menu_name'])){
							$data[$key]['menu_name'] = $menuagain[$value['menu_id']]['menu_name'];
						}
						if(!empty($menuagain[$value['menu_id']]['menu_url'])){
							$data[$key]['menu_url'] = $menuagain[$value['menu_id']]['menu_url'];
						}
					}
					if(!empty($value['menu_code']) && $value['menu_url'] != '#'){
						$menuallstr[$value['menu_url']][] = $data[$key]['menu_name']?$data[$key]['menu_name']:$value['menu_name'];
					}
					if($value['menu_url'] == "#"){
						if(count($value['list'])>0){
							foreach ($value['list'] as $key2 => $value2) {
								if(!empty($menuagain[$value2['menu_id']])){
									if(!empty($menuagain[$value2['menu_id']]['menu_name'])){
										$data[$key]['list'][$key2]['menu_name'] = $menuagain[$value2['menu_id']]['menu_name'];
									}
									if(!empty($menuagain[$value2['menu_id']]['menu_url'])){
										$data[$key]['list'][$key2]['menu_url'] = $menuagain[$value2['menu_id']]['menu_url'];
									}
								}
								if(!empty($value2['menu_code']) && $value2['menu_url'] != '#'){
									$menuallstr[$value2['menu_url']][] = $data[$key]['menu_name'];
									$menuallstr[$value2['menu_url']][] = $data[$key]['list'][$key2]['menu_name']?$data[$key]['list'][$key2]['menu_name']:$value2['menu_name'];
								}
							}
						}else{
							unset($data[$key]);
						}
					}
				}
				if(!empty($data)){
					array_values($data);
				}
			}

			if(!empty($thirdlink)){
				$thirdlink = json_decode($thirdlink,true);
			}else{
				$thirdlink = [];
			}

			$userdata['fid'] 			= session('regulatorpersonInfo.fid');//用户id
			$userdata['fname'] 			= session('regulatorpersonInfo.fname');//用户姓名
			$userdata['fregulatorid'] 	= session('regulatorpersonInfo.fregulatorid');//直接机构
			$userdata['fregulatorpid'] 	= session('regulatorpersonInfo.fregulatorpid');//直接机构
			$userdata['fregulatorppid'] = session('regulatorpersonInfo.fregulatorppid');//间接机构
			$userdata['regionid'] 		= session('regulatorpersonInfo.regionid');//行政区划
			$userdata['regionpname1'] 	= session('regulatorpersonInfo.regionpname');//父级的行政区划全名
			$userdata['regionpname'] 	= session('regulatorpersonInfo.regionpname1');//父级的行政区划简名
			$userdata['regionname1'] 	= session('regulatorpersonInfo.regionname');//行政区划简名
			$userdata['regionname'] 	= session('regulatorpersonInfo.regionname1');//行政区划全名
			$userdata['regulatorname'] 	= session('regulatorpersonInfo.regulatorname');//当前部门名称
			$userdata['regulatorpname'] = session('regulatorpersonInfo.regulatorpname');//当前机构名称
			$userdata['fregulatorlevel'] = session('regulatorpersonInfo.fregulatorlevel');//当前机构级别
			$userdata['flevel'] 		= session('regulatorpersonInfo.flevel');//当前地域等级
			$userdata['userphone'] 		= session('regulatorpersonInfo.userphone');//手机号
			$userdata['role'] 			= session('regulatorpersonInfo.fisadmin');//用户角色
			$userdata['media_class'] 	= json_decode($media_class);
			$userdata['illadTypes'] 	= json_decode($illadTypes);
			$userdata['illSupervise'] 	= $illSupervise;
			$userdata['monitorCondition'] 	= $monitorCondition;
			$userdata['havenet'] 		= $havenet;
			$userdata['fjxsgl'] 		= $fjxsgl;
			$userdata['isillegal_summary']= $isillegal_summary;
			$userdata['system_num'] 	= $system_num;
			$userdata['iswarning'] 		= $iswarning;
			$userdata['ischazheng'] 	= $ischazheng;
			$userdata['is_show_tj'] 	= $is_show_tj;
			$userdata['is_show_sj'] 	= $is_show_sj;
			$userdata['is_show_clue'] 	= $is_show_clue;
			$userdata['is_show_count'] 	= $is_show_count;
			$userdata['isoutdata'] 		= $isoutdata;
			$userdata['jishuzc'] 		= $jishuzc;
			$userdata['isexamine'] 		= $isexamine;
			$userdata['index_sub_map'] 	= $index_sub_map;
			$userdata['generate_report'] = $generate_report;
			$userdata['isdistribute'] = $isdistribute;
			$userdata['isrelease'] = $isrelease;
			$userdata['thirdlink'] = $thirdlink;
			$userdata['datacheck_isfile'] = $datacheck_isfile;
			$userdata['tregionlevel'] = $tregionlevel;
			$userdata['illadprint'] = $illadprint;
			$userdata['use_open_search'] = $use_open_search;
			$userdata['isdudao'] = $isdudao;
			$userdata['huizong_iszhdf'] = $huizong_iszhdf;
			$userdata['datacheck_istext'] = $datacheck_istext;
			$userdata['isShowRegion'] = $isShowRegion;
			$userdata['stepRegionOrder'] = $stepRegionOrder;

			$menujurisdiction = [];
			foreach ($menuallstr as $key => $value) {
				$menujurisdiction[] = $key;
			}
			S(session('regulatorpersonInfo.fcode').'_menujurisdiction',$menujurisdiction,86400);
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data,'userdata'=>$userdata,'menudata' => $menuallstr));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败'));
		}
	}

}
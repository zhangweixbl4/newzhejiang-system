<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局处理结果查看
 * by zw
 */

class GckhuizongController extends BaseController{
  /**
  * 处理结果列表
  * by zw
  */
  public function cljieguo_list(){
    Check_QuanXian(['ckchuli']);
    session_write_close();
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）

    $outtype = I('outtype');//导出类型
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }

    $system_num = getconfig('system_num');
    $iscontain        = I('iscontain');//是否包含下属地区
    $is_show_fsend    = I('is_show_fsend');//是否包含确认广告，1包含，2不包含，0未定义
    $area = I('area');
    $fstate = I('fstate');
    $fstate2 = I('fstate2');
    $fresultdatetime = I('fresultdatetime');//处理时间
    $fsenddatetime = I('fsenddatetime');//派发时间

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    
    if($is_show_fsend==1){
      $is_show_fsend = -2;
    }else{
      $is_show_fsend = 0;
    }
    $where_time = gettimecondition($years,$timetypes,$timeval,'d.fissue_date',$is_show_fsend);
    $where_tia['_string'] = '1=1';

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['_string'] .= ' and a.fmedia_class='.$search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['_string'] .= ' and b.ffullname like "%'.$search_val.'%"';
      }elseif($search_type == 30){//广告名称
        $where_tia['_string'] .= ' and a.fad_name like "%'.$search_val.'%"';
      }elseif($search_type == 40){//发布媒体
        $where_tia['_string'] .= ' and c.fmedianame like "%'.$search_val.'%"';
      }elseif($search_type == 50){//违法原因
        $where_tia['_string'] .= ' and a.fillegal like "%'.$search_val.'%"';
      }elseif($search_type == 60){//广告主
        $where_tia['_string'] .= ' and a.fadowner like "%'.$search_val.'%"';
      }
    }

    if(!empty($netadtype)){
      $where_tia['_string'] .= ' and a.fplatform='.$netadtype;
    }

    if(( !empty($fad_class_code)) && empty($area)){
      if($system_num == '100000' && session('regulatorpersonInfo.fregulatorlevel')==30){
        $area = '100000';
      }else{
        $area = session('regulatorpersonInfo.regionid');
      }
    }

    $where_tia['_string'] .= ' and a.fstatus3 not in (30,40)';
    if($fstate != -1){//违法广告处理状态
      if($fstate == 0){
        $where_tia['_string'] .= ' and a.fstatus = 0';
        if($fstate2 == 10){
          $where_tia['_string'] .= ' and a.fview_status = 0';
        }elseif($fstate2 == 20){
          $where_tia['_string'] .= ' and a.fview_status = 10';
        }
      }elseif($fstate == 10){
        $where_tia['_string'] .= ' and a.fstatus = 10';
        if($fstate2 == 10){
          $where_tia['_string'] .= ' and a.fresult like "%责令停止发布%"';
        }elseif($fstate2 == 20){
          $where_tia['_string'] .= ' and a.fresult like "%立案%"';
        }elseif($fstate2 == 30){
          $where_tia['_string'] .= ' and a.fresult like "%立案%"';
          $where_tia['_string'] .= ' and a.fstatus2 = 17';
        }elseif($fstate2 == 40){
          $where_tia['_string'] .= ' and a.fresult like "%立案%"';
          $where_tia['_string'] .= ' and a.fstatus2 = 16';
        }elseif($fstate2 == 50){
          $where_tia['_string'] .= ' and a.fresult like "%移送司法%"';
        }elseif($fstate2 == 60){
          $where_tia['_string'] .= ' and a.fresult like "%其他%"';
        }
      }elseif($fstate == 30){
        $where_tia['_string'] .= ' and a.fstatus = 30';
      }
    }

    if(!empty($area)){//所属地区
      if($area != '100000'){
        if(!empty($iscontain)){
          $tregion_len = get_tregionlevel($area);
          if($tregion_len == 1){//国家级
            $where_tia['_string'] .= ' and fregion_id ='.$area;  
          }elseif($tregion_len == 2){//省级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,2).'%"';
          }elseif($tregion_len == 4){//市级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,4).'%"';
          }elseif($tregion_len == 6){//县级
            $where_tia['_string'] .= ' and fregion_id like "'.substr($area,0,6).'%"';
          }
        }else{
          $where_tia['_string']  .= ' and fregion_id = '.$area;
        }
      }
    }

    //国家局系统只显示有打国家局标签媒体的数据
    if($system_num == '100000'){
      $wherestr = ' and tn.flevel in (1,2,3)';
    }

    $where_tia['_string'] .= ' and a.fcustomer = "'.$system_num.'"';
    if(!empty($illDistinguishPlatfo)){
      $where_tia['_string'] .= ' and left(c.fmediaclassid,2) in('.implode(',', $media_class).')';
    }

    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['_string'] .= ' and a.fexamine = 10';
    }

    if(!empty($fresultdatetime)){
      $where_tia['_string'] .= ' and a.fresult_datetime between "'.$fresultdatetime[0].'" and "'.date('Y-m-d',strtotime($fresultdatetime[1])).' 23:59:59"';
    }
    if(!empty($fsenddatetime)){
      $where_tia['_string'] .= ' and a.fsend_datetime between "'.$fsenddatetime[0].'" and "'.date('Y-m-d',strtotime($fsenddatetime[1])).' 23:59:59"';
    }
    $where_tia['_string'] .= ' and '.$where_time;

    $countsql = '
      select count(*) count from (
        select a.fad_name,a.fmedia_id from tbn_illegal_ad a
          inner join tadclass b on a.fad_class_code=b.fcode
          inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1
          inner join tregion tn on a.fregion_id=tn.fid'.$wherestr.'
          inner join tbn_illegal_ad_issue d on a.fid = d.fillegal_ad_id
          inner join (select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id
          where '.$where_tia['_string'].'
          group by a.fad_name,a.fmedia_id
      ) xx
    ';
    $count = M()->query($countsql)[0]['count'];

    $datasql = '
      select a.fmedia_id,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fadowner as fadownername,tn.ffullname,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek,ifnull(sum(fquantity),0) fcount,ifnull(sum(d.fendtime-d.fstarttime),0) fsccount
      from tbn_illegal_ad a
      inner join tadclass b on a.fad_class_code=b.fcode
      inner join tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1
      inner join tregion tn on a.fregion_id=tn.fid'.$wherestr.'
      inner join tbn_illegal_ad_issue d on a.fid = d.fillegal_ad_id
      inner join (select fillegal_ad_id illad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) snd on a.fid = snd.illad_id
      where '.$where_tia['_string'].'
      group by a.fad_name,a.fmedia_id
      limit '.($p-1)*$pp.','.$pp.'
    ';
    $do_tia = M()->query($datasql);

    if(!empty($outtype)){
        if(empty($do_tia)){
          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }

        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-违法广告清单';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'ffullname',
          '发布媒体'=>'fmedianame',
          '媒体分类'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网'],
            ]
          ],
          '广告类别'=>'fadclass',
          '广告名称'=>'fad_name',
          '播出周数'=>'diffweek',
          '播出总条次'=>'fcount',
          '播出总时长'=>'fsccount',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
        ];
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        D('Function')->write_log('违法广告清单',1,'导出成功');
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

      }else{
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
      }
  }

}
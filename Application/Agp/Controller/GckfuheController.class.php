<?php
namespace Agp\Controller;
use Think\Controller;

/**
 * 国家局复核处理结果查看
 * by zw
 */

class GckfuheController extends BaseController{
  /**
  * 复核处理结果列表
  * by zw
  */
  public function fhjieguo_list(){
    Check_QuanXian(['sjfuhejg']);
    session_write_close();
    $system_num = getconfig('system_num');
    $illDistinguishPlatfo = getconfig('illDistinguishPlatfo');//同一客户的案件线索是否根据平台配置进行区分，0不作区分，1区分
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）
    
    $p  = I('page', 1);//当前第几页
    $pp = 20;//每页显示多少记录
    $area = I('area');
    $thisorg = I('thisorg');
    $fhstatus = I('fhstatus');//复核状态
    $ad_name = I('ad_name');//广告名称
    $re_name = I('re_name');//地域名称

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date');

    $fregulatorlevel = session('regulatorpersonInfo.fregulatorlevel');//机构级别
    //仅显示本机构数据
    if(!empty($thisorg)){
      if($fregulatorlevel == 30){
        $where_tia['a.tgj_treid'] = session('regulatorpersonInfo.fregulatorpid');//国家局参与复核数据
      }elseif($fregulatorlevel == 20){
        $where_tia['a.tshengj_treid'] = session('regulatorpersonInfo.fregulatorpid');//省级参与复核数据
      }elseif($fregulatorlevel == 10){
        $where_tia['a.tshij_treid'] = session('regulatorpersonInfo.fregulatorpid');//市级参与复核数据
      }elseif($fregulatorlevel == 0){
        $where_tia['a.tquj_treid'] = session('regulatorpersonInfo.fregulatorpid');//区县级参与复核数据
      }
    }else{
      if($fregulatorlevel!=30){
        if(empty($area)){
          if($fregulatorlevel == 20){
            $where_tia['b.fregion_id'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,2).'%');
          }elseif($fregulatorlevel == 10){
            $where_tia['b.fregion_id'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,4).'%');
          }elseif($fregulatorlevel == 0){
            $where_tia['b.fregion_id'] = array('like',substr(session('regulatorpersonInfo.regionid'),0,6).'%');
          }
        }else{
          $where_tia['b.fregion_id'] = $area;
        }
      }else{
        if($area != 100000 && !empty($area)){
          $where_tia['b.fregion_id'] = $area;
        }
      }
    }

    //广告名称查询
    if(!empty($ad_name)){
      $where_tia['b.fad_name'] = array('like','%'.$ad_name.'%');
    }

    //地域名称查询
    if(!empty($re_name)){
      $where_tia['e.fname'] = array('like','%'.$re_name.'%');
    }

    if($fhstatus == 10){
      $where_tia['a.tcheck_status'] = 0;
      $where_tia['b.fstatus'] = 30;
    }elseif($fhstatus == 20){
      $where_tia['a.tcheck_status'] = 20;
    }elseif($fhstatus == 30){
      $where_tia['a.tcheck_status'] = 10;
    }elseif($fhstatus == 40){
      $where_tia['a.tcheck_status'] = 30;
    }elseif($fhstatus == 50){
      $where_tia['a.tcheck_status'] = 40;
    }
    if(!empty($illDistinguishPlatfo)){
      $where_tia['left(d.fmediaclassid,2)'] = ['in',$media_class];
    }
    $where_tia['e.fstate']        = 1;
    $where_tia['d.fstate']        = 1;

    $count = M('tbn_data_check')
      ->alias('a')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1 ')
      ->join('tregion e on b.fregion_id=e.fid')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数
    
    $do_tia = M('tbn_data_check')
      ->alias('a')
      ->field('a.id as fid,a.tgj_result,a.tshengj_result,a.tshij_result,a.tquj_result,b.fmedia_class,c.ffullname as fadclass,b.fad_name, (case when instr(d.fmedianame,"（") > 0 then left(d.fmedianame,instr(d.fmedianame,"（") -1) else d.fmedianame end) as fmedianame,b.fillegal_code,b.fillegal,b.fexpressions,b.favifilename,b.fjpgfilename,a.tgj_username,a.tgj_time,a.tshengj_username,a.tshengj_time,a.tshij_username,a.tshij_time,a.tquj_username,a.tquj_time,b.fid as sid,b.fstatus3,a.tkuayu_status,b.fadowner as fadownername')
      ->join('tbn_illegal_ad b on b.fid=a.fillegal_ad_id and fcustomer="'.$system_num.'"')
      ->join('tadclass c on b.fad_class_code=c.fcode')
      ->join('tmedia d on b.fmedia_id=d.fid and d.fid=d.main_media_id and d.fstate = 1 ')
      ->join('tregion e on b.fregion_id=e.fid')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on b.fid=x.fillegal_ad_id')
      ->join('(select fillegal_ad_id from tbn_case_send where frece_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' or fsend_reg_id = '.session('regulatorpersonInfo.fregulatorpid').' group by fillegal_ad_id) csd on b.fid = csd.fillegal_ad_id')
      ->where($where_tia)
      ->order('a.id desc')
      ->page($p,$pp)
      ->select();
  
    $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
  }

  /**
  * 复核结果处理记录
  * by zw
  */
  public function fuhe_cllist(){
    $fid = I('fid');//违法广告ID

    //读取原始违法广告数据
    $where_tid['a.fid'] = $fid;
    $where_tid['a.fcustomer'] = $system_num;
    $do_tid = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fad_name,fillegal,fillegal_code,fillegal,fexpressions,fillegaltypecode,(case when a.fillegaltypecode > 0 then "违法" else "不违法" end) as fillegaltype')
      ->where($where_tid)
      ->find();

    if(!empty($do_tid)){
      //读取修改记录
      $where_tiad['a.illegal_ad_id'] = $fid;
      $do_tiad = M('tbn_illegal_ad_record')
        ->field('a.fadname,a.fexpressioncodes,a.remark,a.fillegalcontent,a.fexpressions,a.fillegaltypecode,update_time,(case when a.fillegaltypecode > 0 then "违法" else "不违法" end) as fillegaltype')
        ->alias('a')
        ->where($where_tiad)
        ->order('update_time asc')
        ->select();

      foreach ($do_tiad as $key => $value) {
        if($value['remark'] == '剪辑错误' || $value['remark'] == '通过复核'){
          $editarr[$key]['remark'] = '剪辑错误，已删除';
          $editarr[$key]['update_time'] = $value['update_time'];
          $editarr[$key]['list'] = [];
        }else{
          $editarr[$key]['update_time'] = $value['update_time'];
          $editarr[$key]['list'] = [];
          if($value['fadname']!=$do_tid['fad_name'] && !empty($value['fadname'])){
            $addarr = [];
            $addarr['title'] = '广告名称';
            $addarr['olddata'] = $do_tid['fad_name'];
            $addarr['newdata'] = $value['fadname'];
            $do_tid['fad_name'] = $value['fadname'];
            $editarr[$key]['list'][] = $addarr;
          }

          if($value['fillegaltypecode'] != $do_tid['fillegaltypecode']){
            $addarr = [];
            $addarr['title'] = '违法类型';
            $addarr['olddata'] = $do_tid['fillegaltype'];
            $addarr['newdata'] = $value['fillegaltype'];
            $do_tid['fillegaltypecode'] = $value['fillegaltypecode'];
            $do_tid['fillegaltype'] = $value['fillegaltype'];
            $editarr[$key]['list'][] = $addarr;

            $addarr = [];
            $addarr['title'] = '违法表现代码';
            $addarr['olddata'] = $do_tid['fillegal_code'];
            $addarr['newdata'] = $value['fexpressioncodes'];
            $do_tid['fillegal_code'] = $value['fexpressioncodes'];
            $editarr[$key]['list'][] = $addarr;

            $addarr = [];
            $addarr['title'] = '违法表现';
            $addarr['olddata'] = $do_tid['fexpressions'];
            $addarr['newdata'] = $value['fexpressions'];
            $do_tid['fexpressions'] = $value['fexpressions'];
            $editarr[$key]['list'][] = $addarr;

            $addarr = [];
            $addarr['title'] = '违法描述';
            $addarr['olddata'] = $do_tid['fillegal'];
            $addarr['newdata'] = $value['fillegalcontent'];
            $do_tid['fillegal'] = $value['fillegalcontent'];
            $editarr[$key]['list'][] = $addarr;
          }
        }
        
      }
      $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$editarr));
    }else{
      $this->ajaxReturn(array('code'=>1,'msg'=>'信息不存在'));
    }
    
  }

}
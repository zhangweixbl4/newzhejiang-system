<?php
namespace Agp\Controller;
use Think\Controller;
class NoticeController extends BaseController {

	/*
	* by zw
	* 获取公告列表
	*/
	public function noticeList(){
		$systemtype = C('FORCE_NUM_CONFIG');
		$pageIndex = I('pageIndex')?I('pageIndex') : 1;//当前页
        $pageSize = I('pageSize')?I('pageSize') : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
		$nTitle = I('nTitle');//标题
		$nType = I('nType');//类型，1安全，2通知，3升级
		$issueDate = I('issueDate');//日期范围，数组
		$flooktype = I('flooktype');//查看权限

		if(!empty($nType)){
			$where_nc['a.ftype'] = $nType;
		}else{
			$where_nc['a.ftype'] = ['between',[1,9]];
		}

		if(!empty($nTitle)){
			$where_nc['a.ftitle'] = ['like','%'.$nTitle.'%'];
		}

		if(!empty($issueDate)){
			$where_nc['a.fissue_date'] = ['between',[$issueDate[0],$issueDate[1]]];
		}

		if($flooktype != ''){
			$where_nc['a.flooktype'] = $flooktype;
		}

		$where_nc['a.fcreate_regid'] = session('regulatorpersonInfo.fregulatorpid');

		$where_nc['a.fsystem_type'] = $systemtype;

		$count = M('tnotice a')
        	->join('tregion b on RPAD(a.fregionid, 6, 0) = b.fid')
			->where($where_nc)
        	->count();

		$data = M('tnotice a')
			->field('a.fid,a.flooktype,a.ftype,a.ftitle,a.fissue_date,a.fcontent,a.fiscontain,RPAD(a.fregionid, 6, 0) fregionid,b.fname1 regionname')
			->join('tregion b on RPAD(a.fregionid, 6, 0) = b.fid')
			->where($where_nc)
			->order('a.fissue_date desc,a.fid desc')
			->limit($limitIndex,$pageSize)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	}

	/*
	* by zw
	* 获取公告列表
	*/
	public function noticeSelf(){
		$systemtype = C('FORCE_NUM_CONFIG');
		$pageIndex = I('pageIndex')?I('pageIndex') : 1;//当前页
        $pageSize = I('pageSize')?I('pageSize') : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;

        $regionid = session('regulatorpersonInfo.regionid');

        $where_nc['fregionid'] = ['in',[$regionid,substr($regionid, 0,2),substr($regionid, 0,4)]];

        $lookType[] = 0;
        if(session('regulatorpersonInfo.fregulatorkind') == 1 || session('regulatorpersonInfo.fregulatorkind') == 2){
        	$lookType[] = 1;
        }else{
        	$lookType[] = session('regulatorpersonInfo.fregulatorkind');
        }
		$where_nc['flooktype'] = ['in',$lookType];

		$where_nc['ftype'] = ['between',[1,9]];

		$where_nc['fsystem_type'] = $systemtype;

		$where_nc['fissue_date'] = ['elt',date('Y-m-d')];

		$count = M('tnotice')
        	->where($where_nc)
        	->count();

		$data = M('tnotice')
			->field('fid,ftype,ftitle,fissue_date,fcontent,flooktype')
			->where($where_nc)
			->order('fissue_date desc')
			->limit($limitIndex,$pageSize)
			->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	}

	/*
	* by zw
	* 公告查看
	*/
	public function noticeView(){
		$fid = I('fid');//ID

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

		$data = M('tnotice a')
			->field('a.fid,a.flooktype,a.ftype,a.ftitle,a.fissue_date,a.fcontent,a.fiscontain,RPAD(a.fregionid, 6, 0) fregionid')
			->join('tregion b on RPAD(a.fregionid, 6, 0) = b.fid')
			->where(['a.fid'=>$fid])
			->find();
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，信息不存在'));
		}
	}

	/*
	* by zw
	* 公告添加
	*/
	public function noticeCreate(){
		$systemtype = C('FORCE_NUM_CONFIG');
		$nTitle = I('nTitle');//标题
		$nType = I('nType');//类型，1安全，2通知，3升级
		$flooktype = I('flooktype');//查看权限
		$nContent = $_POST['nContent'];//内容
		$issueDate = I('issueDate')?I('issueDate'):date('Y-m-d');//公告日期
		$area = I('area')?I('area'):330000;//行政区划
        $iscontain = I('iscontain') != ''?I('iscontain'):1;//是否包含下属地区，1包含，0不包含，不传默认1

		if(empty($nTitle) || empty($nType) || empty($nContent)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'信息填写不完整'));
		}

		//地区查看权限
		if(!empty($iscontain)){
			$region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
	        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断国家
	        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
	        $data_nc['fregionid'] = $region_id_rtrim;
		}else{
			$data_nc['fregionid'] = $area;
		}

		$data_nc['fiscontain'] = $iscontain;
		$data_nc['ftitle'] = $nTitle;
		$data_nc['ftype'] = $nType;
		$data_nc['flooktype'] = $flooktype?$flooktype:0;
		$data_nc['fcontent'] = $nContent;
		$data_nc['fissue_date'] = $issueDate;
		$data_nc['fsystem_type'] = $systemtype;
		$data_nc['fcreate_regid'] = session('regulatorpersonInfo.fregulatorpid');
		$data_nc['fcreate_time'] = date('Y-m-d H:i:s');
		$data_nc['fcreate_userid'] = session('regulatorpersonInfo.fid');
		$data_nc['fcreate_user'] = session('regulatorpersonInfo.fname');
		$do_nc = M('tnotice')->add($data_nc);
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/*
	* by zw
	* 公告修改
	*/
	public function noticeSave(){
		$fid = I('fid');//ID
		$nTitle = I('nTitle');//标题
		$nType = I('nType');//类型，1安全，2通知，3升级
		$flooktype = I('flooktype');//查看权限
		$nContent = $_POST['nContent'];//内容
		$issueDate = I('issueDate')?I('issueDate'):date('Y-m-d');//公告日期
		$area = I('area')?I('area'):330000;//行政区划
        $iscontain = I('iscontain') != ''?I('iscontain'):1;//是否包含下属地区，1包含，0不包含，不传默认1

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}
		if(empty($nTitle) || empty($nType) || empty($nContent)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'信息填写不完整'));
		}

		//地区查看权限
		if(!empty($iscontain)){
			$region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
	        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断国家
	        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
	        $data_nc['fregionid'] = $region_id_rtrim;
		}else{
			$data_nc['fregionid'] = $area;
		}

		$data_nc['fiscontain'] = $iscontain;
		$data_nc['ftitle'] = $nTitle;
		$data_nc['ftype'] = $nType;
		$data_nc['flooktype'] = $flooktype?$flooktype:0;
		$data_nc['fcontent'] = $nContent;
		$data_nc['fissue_date'] = $issueDate;
		$data_nc['fmodify_time'] = date('Y-m-d H:i:s');
		$data_nc['fmodify_userid'] = session('regulatorpersonInfo.fid');
		$data_nc['fmodify_user'] = session('regulatorpersonInfo.fname');
		$do_nc = M('tnotice')->where(['fid'=>$fid])->save($data_nc);
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'保存成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，信息不存在'));
		}
	}

	/*
	* by zw
	* 公告删除
	*/
	public function noticeDel(){
		$fid = I('fid');//ID

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

		$do_nc = M('tnotice')->where(['fid'=>$fid])->delete();
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，信息不存在'));
		}
	}

}
<?php
namespace Agp\Controller;
use Think\Controller;
use Think\Exception;

import('Vendor.PHPExcel');

/**
 * 监测数据
 */

class FmonitoringController extends BaseController
{
	/**
	 * 获取监测数据
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（count-记录总数，list-列表数据）
	 * by zw
	 */
	public function index()
	{
		session_write_close();
		Check_QuanXian(['home','ghome','adinquire','publishlist','illegaltadFJ','televisionFJ','broadcastFJ','newspaperFJ','tadsearchFJ','monitoringData','monitoringData2']);
		header("Content-type:text/html;charset=utf-8");
		ini_set('memory_limit','2048M');
		ini_set('max_execution_time', 0);//设置超时时间
        $media_class = json_decode(getconfig('media_class'));
        $system_num = getconfig('system_num');
		$outtype = I('outtype');//导出类型

		$p = I('page', 1);//当前第几页
		$pp = 20;//每页显示多少记录
		if(empty($outtype)){
			$limitstr = ' limit '.($p-1)*$pp.','.$pp;
		}

		$pxtype 	= I('pxtype')?I('pxtype'):2;//排序方式，0默认
		$pxval 		= I('pxval')?I('pxval'):0;//排序值，0默认
		$fadname 			= I('fadname');// 广告名称
		$fadclasscode 		= I('fadclasscode');// 广告内容类别组
		$area 				= I('area');// 行政区划
		$media 				= I('media');//媒体组
		$mclass 			= I('mclass')?I('mclass'):'';//媒体类型
		$fillegaltypecode 	= I('fillegaltypecode');// 违法类型
		$issuedate 			= I('issuedate');//发布时间
		$iscontain 			= I('iscontain');//是否包含下属地区
		$version 			= I('version');//版本说明
		if(strlen($fillegaltypecode)==0){
			$fillegaltypecode = -1;
		}

		if(empty($area)){
			$area = session('regulatorpersonInfo.regionid');
		}

        $where_str = '1=1';

        $tb_tvissue = gettable('01',strtotime($issuedate[0]));
        $tb_bcissue = gettable('02',strtotime($issuedate[0]));
        $tb_paperissue = gettable('03',strtotime($issuedate[0]));
		$tb_tvsam = 'ttvsample';
		$tb_bcsam = 'tbcsample';
		$tb_papersam = 'tpapersample';

		//验证表是否存在
		if(empty(S('isTable_'.$tb_tvissue))){
			D('Common/Function','Model')->isIssueTable($tb_tvissue,'01');
			S('isTable_'.$tb_tvissue,'1',86400);
		}
		if(empty(S('isTable_'.$tb_bcissue))){
			D('Common/Function','Model')->isIssueTable($tb_bcissue,'02');
			S('isTable_'.$tb_bcissue,'1',86400);
		}
		if(empty(S('isTable_'.$tb_paperissue))){
			D('Common/Function','Model')->isIssueTable($tb_paperissue,'03');
			S('isTable_'.$tb_paperissue,'1',86400);
		}

		$orders = '';
		if($pxtype==1){//1时按发布媒体排序
			if($pxval==0){
				$orders = $orders?$orders.',fmedianame desc':' fmedianame desc';
			}else{
				$orders = $orders?$orders.',fmedianame asc':' fmedianame asc';
			}
		}elseif($pxtype==2){//2时按发布时间排序
			if($pxval==0){
				$orders = $orders?$orders.',a.fissuedate desc':' a.fissuedate desc';
			}else{
				$orders = $orders?$orders.',a.fissuedate asc':' a.fissuedate asc';
			}
		}else{
			if($pxval==0){
				$orders = $orders?$orders.',a.fissuedate desc':' a.fissuedate desc';
			}
		}

		if (!empty($fadname)) {
			$where_str .= ' and fadname like "%'.$fadname.'%"';
		}

		if(!empty($media)){
			$where_str .= ' and f.fid in ('.implode(',', $media).')';
		}
		//获取非备用媒体数据
		$where_str .= ' and a.fstate = 1 and a.fsendstate = 20';

		$arr_code = [];
		if(is_array($fadclasscode)){
			foreach ($fadclasscode as $key => $value) {
				$codes = D('Common/Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
				if(!empty($codes)){
					$arr_code = array_merge($arr_code,$codes);
				}else{
					array_push($arr_code,$value);
				}
			}
			$where_str 	.= ' and fadclasscode  in('.implode(',', $arr_code).')';
		}
		
		if($fillegaltypecode>0){//违法类型
			$where_str .= ' and b.fillegaltypecode > 0';
		}elseif($fillegaltypecode == 0){
			$where_str .= ' and b.fillegaltypecode <= 0';
		}

		if(!empty($mclass)){
			$where_str .= ' and left(f.fmediaclassid,2) = "'.$mclass.'"';
		}else{
			$where_str .= ' and left(f.fmediaclassid,2) in("'.implode('","',$media_class).'")';
		}

		if(!empty($issuedate)){
			$where_str .= ' and a.fissuedate between "'.$issuedate[0].'" and "'.$issuedate[1].'"';
		}
		if(!empty($version)){
			$where_str .= ' and b.fversion like "%'.$version.'%"';
		}

		if(!empty($iscontain)){
			$tregion_len = get_tregionlevel($area);
			if($tregion_len == 1){//国家级
				$where_str .= ' and f.media_region_id ='.$area;	
			}elseif($tregion_len == 2){//省级
				$where_str .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
			}elseif($tregion_len == 4){//市级
				$where_str .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
			}elseif($tregion_len == 6){//县级
				$where_str .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
			}
		}else{
			$where_str .= ' and f.media_region_id ='.$area;
		}
		$where_str .= ' and a.fmediaid in (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').' )';

		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9)
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1';

	    //根据不同的媒体类型，搜索不同的表
	    if($mclass=='01'){
	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_tvissue.' a
				inner join '.$tb_tvsam.' b on a.fsampleid=b.fid and b.fstate in(0,1)
				'.$joins.' 
				where '.$where_str;
			$count = M()->query($countsql);

			$datasql = 'select left(f.fmediaclassid,2) mclass,a.fmediaid,e.ffullname as fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype, "" as fpage,a.fissuedate,a.fstarttime,a.fendtime, flength, b.fillegalcontent, b.fexpressioncodes,a.fid as sid,c.fadname,c.fadclasscode,d.fname as fadownername,b.favifilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_tvissue.' a
				inner join '.$tb_tvsam.' b on a.fsampleid=b.fid and b.fstate in(0,1)
				'.$joins.' 
				where '.$where_str.'
				ORDER BY '.$orders.$limitstr;
	    }elseif($mclass=='02'){
	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_bcissue.' a
				inner join '.$tb_bcsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'
				where '.$where_str;
			$count = M()->query($countsql);

			$datasql = 'select left(f.fmediaclassid,2) mclass,a.fmediaid,e.ffullname as fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,a.fissuedate,a.fstarttime,a.fendtime,"" as fpage, flength, b.fillegalcontent, b.fexpressioncodes,a.fid as sid,c.fadname,c.fadclasscode,d.fname as fadownername,b.favifilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_bcissue.' a
				inner join '.$tb_bcsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'
				where '.$where_str.' 
				ORDER BY '.$orders.$limitstr;
	    }elseif($mclass=='03' || $mclass == '04'){
	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_paperissue.' a
				inner join '.$tb_papersam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$where_str;
			$count = M()->query($countsql);

			$datasql = 'select left(f.fmediaclassid,2) mclass,a.fmediaid,e.ffullname as fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype, fpage,a.fissuedate,"" fstarttime,"" fendtime, fpage as flength, b.fillegalcontent, b.fexpressioncodes,a.fid as sid,c.fadname,c.fadclasscode,d.fname as fadownername,b.fjpgfilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_paperissue.' a
				inner join '.$tb_papersam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'
				where '.$where_str.' and left(f.fmediaclassid,2) = "'.$mclass.'"
				ORDER BY '.$orders.$limitstr;
	    }else{
	    	$countsql = 'select sum(acount) as acount from (
	    	(select count(*) as acount 
	    		from '.$tb_tvissue.' a
				inner join '.$tb_tvsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'
				where '.$where_str.')
			union all (select count(*) as acount 
				from '.$tb_bcissue.' a
				inner join '.$tb_bcsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'
				where '.$where_str.')
			union all (select count(*) as acount 
				from '.$tb_paperissue.' a 
				inner join '.$tb_papersam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'  
				where '.$where_str.')
			) as a';
			$count = M()->query($countsql);

			$datasql = 'select * from (
			(select left(f.fmediaclassid,2) mclass,a.fmediaid,e.ffullname as fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,"" as fpage,a.fissuedate,a.fstarttime,a.fendtime, flength, b.fillegalcontent, b.fexpressioncodes,a.fid as sid,c.fadname,c.fadclasscode,d.fname as fadownername,b.favifilename ysscurl,"" fbyurl,"" ldyurl 
				from '.$tb_tvissue.' a
				inner join '.$tb_tvsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$where_str.') 
			union all (select left(f.fmediaclassid,2) mclass,a.fmediaid,e.ffullname as fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype,"" as fpage,a.fissuedate,a.fstarttime,a.fendtime, flength, b.fillegalcontent, b.fexpressioncodes,a.fid as sid,c.fadname,c.fadclasscode,d.fname as fadownername,b.favifilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_bcissue.' a
				inner join '.$tb_bcsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$where_str.') 
			union all (select left(f.fmediaclassid,2) mclass,a.fmediaid,e.ffullname as fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.media_region_id as fregionid,(case when fillegaltypecode>0 then "违法" else "不违法" end ) as fillegaltype, fpage,a.fissuedate,"" fstarttime,"" fendtime, fpage as flength, b.fillegalcontent, b.fexpressioncodes,a.fid as sid,c.fadname,c.fadclasscode,d.fname as fadownername,b.fjpgfilename ysscurl,"" fbyurl,"" ldyurl
				from '.$tb_paperissue.' a
				inner join '.$tb_papersam.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$where_str.') 
			) as a ORDER BY '.$orders.$limitstr;
	    }

		$data = M()->query($datasql);

		foreach ($data as $key => $value) {
			$data[$key]['regionname'] = M('tregion')->cache(true,600)->where(['fid'=>$value['fregionid']])->getfield('ffullname');
		}
		
		if(!empty($outtype)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
			if($outtype=='xls'){
		        $outdata['datalie'] = [
		          '序号'=>'key',
		          '广告名称'=>'fadname',
		          '广告主'=>'fadownername',
		          '广告内容类别'=>'fadclass',
		          '发布媒体'=>'fmedianame',
		          '媒体类型'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == "01"','电视'],
		              ['{mclass} == "02"','广播'],
		              ['{mclass} == "03"','报纸'],
		              ['{mclass} == "04"','期刊'],
		            ]
		          ],
		          '违法类型'=>'fillegaltype',
		          '发布时间'=>'fissuedate',
		          '结束时间'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == "01" || {mclass} == "02"','{fendtime}'],
		              ['',''],
		            ]
		          ],
		          '时长/版面'=>[
		            'type'=>'zwif',
		            'data'=>[
		              ['{mclass} == "01" || {mclass} == "02"','{flength}'],
		              ['{mclass} == "03" || {mclass} == "04"','{fpage}'],
		            ]
		          ],
		          '违法代码'=>'fexpressioncodes',
		          '涉嫌违法内容'=>'fillegalcontent',
		        ];
		        if($mclass == '01' || $mclass == '02' || $mclass == '03' || $mclass == '04'){
		        	$outdata['datalie']['素材地址'] = 'ysscurl';
		        }elseif($mclass == 'net'){
		        	$outdata['datalie']['发布页'] = 'fbyurl';
		        	$outdata['datalie']['落地页'] = 'ldyurl';
		        }else{
		        	$outdata['datalie']['素材地址'] = 'ysscurl';
		        	$outdata['datalie']['发布页'] = 'fbyurl';
		        	$outdata['datalie']['落地页'] = 'ldyurl';
		        }

		        $outdata['lists'] = $data;
		        $ret = A('Api/Function')->outdata_xls($outdata);

		        D('Function')->write_log('监测数据',1,'excel导出成功');
		        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

			}elseif($outtype=='xml'){
				$this->downloadXml($data);
			}elseif($outtype=='txt'){
				$this->downloadTxt($data);
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'请选择导出类型'));
			}
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count[0]['acount'],'list'=>$data)));
		}
	}

	//导出xml
	public function downloadXml($data)
	{
		Check_QuanXian(['home','ghome','adinquire','publishlist','illegaltadFJ','televisionFJ','broadcastFJ','newspaperFJ','tadsearchFJ','monitoringData','monitoringData2']);
		$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		$xml .= "<article>\n";
		$arr = ['01'=>'电视','02'=>'广播','03'=>'报纸','04'=>'期刊','net'=>'互联网','od'=>'户外'];
		foreach ($data as $key =>$value) {
			$xml .= "<item>\n";
			$xml .= "<xuhao>" . ($key+1) . "</xuhao>\n";
			$xml .= "<ggmingcheng>" . $value['fadname'] . "</ggmingcheng>\n";
			$xml .= "<ggzhu>" . $value['fadownername'] . "</ggzhu>\n";
			$xml .= "<ggnrleibie>" . $value['fadclass'] . "</ggnrleibie>\n";
			$xml .= "<meiti>" . $value['fmedianame'] . "</meiti>\n";
			$xml .= "<mtleixing>" . $arr[$value['mclass']] . "</mtleixing>\n";
			$xml .= "<wfleixing>" . $value['fillegaltype'] . "</wfleixing>\n";
			$xml .= "<fbshijian>" . $value['fissuedate'] . "</fbshijian>\n";
			$xml .= "<jsshijian>" . $value['fendtime'] . "</jsshijian>\n";
			$xml .= "<scbanmian>" . $value['flength'] . "</scbanmian>\n";
			$xml .= "<wfdaima>" . $value['fexpressioncodes'] . "</wfdaima>\n";
			$xml .= "<wfyuanyin>" . $value['fillegalcontent'] . "</wfyuanyin>\n";
			$xml .= "</item>\n";
		}
		$xml .= "</article>\n";
		
		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.xml';
		file_put_contents($savefile,$xml);//生成本地文件
		D('Function')->write_log('监测数据',1,'xml导出成功');

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.xml',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
		unlink($savefile);//删除文件

		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	//导出txt
	public function downloadTxt($data)
	{
		Check_QuanXian(['home','ghome','adinquire','publishlist','illegaltadFJ','televisionFJ','broadcastFJ','newspaperFJ','tadsearchFJ','monitoringData','monitoringData2']);
		$txttitle = "序号  广告名称  广告内容类别  发布媒体  媒体类型  违法类型  发布时间  结束时间  时长/版面  违法代码  违法原因\n";
		$txtcontent = '';
		$arr = ['01'=>'电视','02'=>'广播','03'=>'报纸','03'=>'期刊','net'=>'互联网','od'=>'户外'];
		foreach ($data as $key => $value) {	
			$txtcontent .=($key+1).'.  ';
			$txtcontent .=$value['fadname'].'  ';
			$txtcontent .=$value['fadownername'].'  ';
			$txtcontent .=$value['fadclass'].'  ';
			$txtcontent .=$value['fmedianame'].'  ';
			$txtcontent .=$arr[$value['mclass']].'  ';
			$txtcontent .=$value['fillegaltype'].'  ';
			$txtcontent .=$value['fissuedate'].'  ';
			$txtcontent .=$value['fendtime'].'  ';
			$txtcontent .=$value['flength'].'  ';
			$txtcontent .=$value['fexpressioncodes'].'  ';
			$txtcontent .=$value['fillegalcontent'].'  ';
			$txtcontent .="\n";
		}

		$date = date('Ymdhis');
		$savefile = './Public/Xls/'.$date.'.txt';
		file_put_contents($savefile,$txttitle.$txtcontent);
		D('Function')->write_log('监测数据',1,'txt导出成功');

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$date.'.txt',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
		unlink($savefile);//删除文件
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	/**
	 * 获取监测数据详情
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据
	 * by zw
	 */
	public function getmonitoringview()
	{
		$system_num = getconfig('system_num');

		$sid 		= I('sid');//广告记录id
		$mclass 	= I('mclass');//媒体类型
		$fissuedatest 	= I('tbname');//获取表名用的时间

		if(empty($sid)||empty($mclass)){
			$this->error('参数缺失!');
		}

        $tb_tvissue = gettable('01',strtotime($fissuedatest));
        $tb_bcissue = gettable('02',strtotime($fissuedatest));
        $tb_paperissue = gettable('03',strtotime($fissuedatest));
		$tb_tvsam = 'ttvsample';
		$tb_bcsam = 'tbcsample';
		$tb_papersam = 'tpapersample';

		$where['b.fstate'] = 1;
		$where['a.fid'] = $sid;

		if($mclass== '01'){
			$data = M($tb_tvissue)
				->field('a.fid as tid,
					c.fadname, e.ffullname as fadclass, (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.fid ,c.fbrand, ifnull(d.fname,"广告主不详") as fadowner_name, fversion, fadlen ,(case when fillegaltypecode>0 then "违法" else "不违法" end ) fillegaltype,b.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, a.fissuedate, fstarttime, fendtime, "" as fpage, "" as fsize, favifilepng,favifilename
				')
				->alias('a')
				->join($tb_tvsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) ')
				->join('tad c on b.fadid=c.fadid and c.fstate in(1,2,9)')
				->join('tadowner d on c.fadowner=d.fid ')
				->join('tadclass e on c.fadclasscode=e.fcode')
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1')
				->where($where)
				->find();

			$data['issueurl'] = A('Common/Media','Model')->get_m3u8($data['fid'],strtotime($data['fstarttime']),strtotime($data['fendtime']));
			$data['fstarttime2'] = $data['fstarttime'];
			$data['fendtime2'] = $data['fendtime'];
			$data['fstarttime'] = date('H:i:s',strtotime($data['fstarttime']));
			$data['fendtime'] = date('H:i:s',strtotime($data['fendtime']));

			$where2['source_type'] = 10;
			$where2['source_tid'] = $data['tid'];
			$where2['source_mediaclass'] = 1;
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$data['source_url'] = $data2['source_url'];
				$data['source_id'] 	= $data2['source_id'];
				$data['source_state'] = $data2['source_state'];
			}
		}elseif($mclass== '02'){
			$data = M($tb_bcissue)
				->field('a.fid as tid,
					c.fadname, e.ffullname as fadclass, (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.fid ,c.fbrand, ifnull(d.fname,"广告主不详") as fadowner_name, fversion, fadlen,(case when fillegaltypecode>0 then "违法" else "不违法" end ) fillegaltype,b.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, a.fissuedate, fstarttime, fendtime, "" as fpage, "" as fsize, favifilepng,favifilename
				')
				->alias('a')
				->join($tb_bcsam.' b on a.fsampleid=b.fid and b.fstate in(0,1) ')
				->join('tad c on b.fadid=c.fadid and c.fstate in(1,2,9)')
				->join('tadowner d on c.fadowner=d.fid ')
				->join('tadclass e on c.fadclasscode=e.fcode')
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1')
				->where($where)
				->find();

			$data['issueurl'] = A('Common/Media','Model')->get_m3u8($data['fid'],strtotime($data['fstarttime']),strtotime($data['fendtime']));
			$data['fstarttime2'] = $data['fstarttime'];
			$data['fendtime2'] = $data['fendtime'];
			$data['fstarttime'] = date('H:i:s',strtotime($data['fstarttime']));
			$data['fendtime'] = date('H:i:s',strtotime($data['fendtime']));

			$where2['source_type'] = 10;
			$where2['source_tid'] = $data['tid'];
			$where2['source_mediaclass'] = 2;
			$data2 = M('source_make')->field('source_url,source_id,source_state')->where($where2)->find();
			if(!empty($data2)){
				$data['source_url'] = $data2['source_url'];
				$data['source_id'] 	= $data2['source_id'];
				$data['source_state'] = $data2['source_state'];
			}
		}elseif($mclass== '03' || $mclass== '04'){
			$data = M($tb_paperissue)
				->field('a.fid as tid,
					c.fadname, e.ffullname as fadclass, (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame ,c.fbrand, ifnull(d.fname,"广告主不详") as fadowner_name, fversion , (case when fillegaltypecode>0 then "违法" else "不违法" end ) fillegaltype,b.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, a.fissuedate, fpage,fpagetype,fproportion,fprice,fissuetype, fsize, b.fjpgfilename as favifilepng,b.fjpgfilename as favifilename
				')
				->alias('a')
				->join($tb_papersam.' b on a.fsampleid=b.fid and b.fstate in(0,1) ')
				->join('tad c on b.fadid=c.fadid and c.fstate in(1,2,9)')
				->join('tadowner d on c.fadowner=d.fid ')
				->join('tadclass e on c.fadclasscode=e.fcode')
				->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 and left(f.fmediaclassid,2) = "'.$mclass.'"')
				->where($where)
				->find();
		}
		$data['fissuedate'] = date('Y-m-d',strtotime($data['fissuedate']));
		$data['mclass'] = $mclass;
		$data['fillegalcontent'] = htmlspecialchars_decode($data['fillegalcontent']);
		
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据','data'=>$data));
		}
	}

	/**
	 * 获取监测素材列表
	 * by zw
	 */
	public function source_make_list(){
		session_write_close();
		
		$p 				= I('page', 1);//当前第几页
		$pp 			= 20;//每页显示多少记录
		$source_name 	= I('source_name');

		$where['source_name'] 	= array('like','%'.$source_name.'%');
		$where['creater_id']	= session('regulatorpersonInfo.fid');
		$where['source_type']	= array('in',array(0,10)) ;

		$count = M('source_make')
			->where($where)
			->count();// 查询满足要求的总记录数

		
		$data = M('source_make')
			->field('source_make.*,(case when instr(a.fmedianame,"（") > 0 then left(a.fmedianame,instr(a.fmedianame,"（") -1) else a.fmedianame end) as fmedianame')
			->join('tmedia a on source_make.media_id=a.fid and a.fid=a.main_media_id and a.fstate = 1')
			->where($where)
			->order('source_id desc')
			->page($p,$pp)
			->select();//查询复核数据
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/**
	 * 删除监测素材
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function source_make_del(){
		$source_id = I('source_id');//素材ID
		$do_sme = M('source_make')->where('source_id='.$source_id)->delete();
		if(!empty($do_sme)){
			D('Function')->write_log('监测素材',1,'删除成功','source_make',$source_id,M('source_make')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			D('Function')->write_log('监测素材',1,'删除失败','source_make',$source_id,M('source_make')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}

	/**
	 * 监测素材生成状态
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function get_source_make_state(){
		$selsource_id = I('selsource_id');//获取选中项
		$db_sme = M('source_make');
		if(!empty($selsource_id)){
			$where['source_id'] = array('in',$selsource_id);
			$where['source_state'] = 2;
			$do_sme = $db_sme->field('source_id,source_url')->where($where)->select();
			if(!empty($do_sme)){
				$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_sme));
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
			}
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
		}
		
	}

	/**
	 * 监测素材生成状态超时
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function get_source_make_state2(){
		$sqlstr = 'select count(*) AS cc from source_make where (unix_timestamp(now())-unix_timestamp(create_time))>(unix_timestamp(end_time)-unix_timestamp(start_time)) and source_state=1';
		$do_sme = M()->query($sqlstr);
		if($do_sme[0]['cc']>0){
			M()->execute('update source_make set source_state=-1 where (unix_timestamp(now())-unix_timestamp(create_time))>(unix_timestamp(end_time)-unix_timestamp(start_time)) and source_state=1');
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
		}
		
	}

	/**
	 * 创建生成素材任务
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by cy
	 */
	public function add_source_make(){
		session_write_close();
		$media_id 		= I('media_id');//媒介id
		$source_name 	= I('source_name');//素材名称
		$source_type 	= I('source_type')?I('source_type'):0;//生成类型
		$tid 			= I('tid')?I('tid'):0;//相关表ID
		$Sstart 		= I('Sstart');//开始时间
		$Send 			= I('Send');//结束时间

		// $cut_url = 'http://'.$_SERVER['HTTP_HOST'].'/Api/Media/add_source_make';//剪辑任务提交地址
        $pushData = [
            'mediaId'=>$media_id,
            'source_name'=>$source_name,
            'source_type'=>$source_type,
            'Sstart'=>$Sstart,
            'Send'=>$Send,
            'tid'=>$tid,
            'creater_id'=>session('regulatorpersonInfo.fid')
        ];
        $ret = A('Api/Media')->add_source_make($pushData,1);
        // $res = json_decode(http($cut_url,json_encode($pushData),'POST',false,0),true);
        $this->ajaxReturn($ret);
		
	}
	
	/**
  * 监管结果
  * by zw
  */
  public function jianGuanJieGuo(){
    session_write_close();
    $outtype = I('outtype');//导出类型
    $selfid = I('fid');//勾选项
    if(!empty($outtype)){
      $p  = I('page', 1);//当前第几页ks
      $pp = 50000;//每页显示多少记录
    }else{
      $p  = I('page', 1);//当前第几页ks
      $pp = 20;//每页显示多少记录
    }
    $where_tia['_string'] = '1=1';

    $system_num = getconfig('system_num');
    $media_class = json_decode(getconfig('media_class'));//平台拥有的媒体类型（01电视、02广播、03报纸、05户外、13互联网）\

    $fstate = I('fstate');
    $fstate2 = I('fstate2');

    //时间条件筛选
    $years      = I('years');//选择年份
    $timetypes  = I('timetypes');//选择时间段
    $timeval    = I('timeval');//选择时间
    $where_time = gettimecondition($years,$timetypes,$timeval,'fissue_date',0);

    $netadtype  = I('netadtype');//平台类别
    $search_type  = I('search_type');//搜索类别
    $search_val   = I('search_val');//搜索值
    if(!empty($search_val)){
      if($search_type == 10){//媒体分类
        $where_tia['a.fmedia_class'] = $search_val;
      }elseif($search_type == 20){//广告类别
        $where_tia['b.ffullname'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 30){//广告名称
        $where_tia['a.fad_name'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 40){//发布媒体
        $where_tia['c.fmedianame'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 50){//违法原因
        $where_tia['a.fillegal'] = array('like','%'.$search_val.'%');
      }elseif($search_type == 60){//广告主
        $where_tia['a.fadowner'] = array('like','%'.$search_val.'%');
      }
    }

    if(!empty($netadtype)){
      $where_tia['a.fplatform']  = $netadtype;
    }

    $where_tia['a.fstatus3'] = array('not in',array(30,40));
    if($fstate != -1){//违法广告处理状态
      if($fstate == 0){
        $where_tia['a.fstatus'] = 0 ;
        if($fstate2 == 10){
          $where_tia['a.fview_status'] = 0 ;
        }elseif($fstate2 == 20){
          $where_tia['a.fview_status'] = 10 ;
        }
      }elseif($fstate == 10){
        $where_tia['a.fstatus'] = 10 ;
        if($fstate2 == 10){
          $where_tia['a.fresult'] = array('like','%责令停止发布%') ;
        }elseif($fstate2 == 20){
          $where_tia['a.fresult'] = array('like','%立案%') ;
        }elseif($fstate2 == 30){
          $where_tia['a.fresult'] = array('like','%立案%') ;
          $where_tia['a.fstatus2'] = 17 ;
        }elseif($fstate2 == 40){
          $where_tia['a.fresult'] = array('like','%立案%') ;
          $where_tia['a.fstatus2'] = 16 ;
        }elseif($fstate2 == 50){
          $where_tia['a.fresult'] = array('like','%移送司法%') ;
        }elseif($fstate2 == 60){
          $where_tia['a.fresult'] = array('like','%其他%') ;
        }
      }elseif($fstate == 30){
        $where_tia['a.fstatus'] = 30 ;
      }
    }

    $where_tia['a.fcustomer']  = $system_num;
    
    $isexamine = getconfig('isexamine');
    if($isexamine == 10 || $isexamine == 20){
      $where_tia['a.fexamine'] = 10;
    }

    //勾选导出
    if(!empty($outtype) && !empty($selfid)){
      $where_tia['a.fid'] = ['in',$selfid];
    }

    $count = M('tbn_illegal_ad')
      ->alias('a')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1')
      ->join('tregion tn on a.fregion_id=tn.fid')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select distinct(fmediaid) from tmedia_temp where ftype = 1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on a.fmedia_id=ttp.fmediaid')
      ->where($where_tia)
      ->count();//查询满足条件的总记录数
    
    $do_tia = M('tbn_illegal_ad')
      ->alias('a')
      ->field('a.fid,a.fmedia_class,b.ffullname as fadclass,a.fad_name,(case when instr(c.fmedianame,"（") > 0 then left(c.fmedianame,instr(c.fmedianame,"（") -1) else c.fmedianame end) as fmedianame,a.fillegal_code,a.fillegal,a.fexpressions,a.fresult,a.flatime,a.fview_status,a.fstatus,a.fstatus2,a.fstatus3,ifnull(x.fcount,0) as fcount,x.fstarttime,x.fendtime,diffweek,a.fregion_id,a.fadowner as fadownername,tn.ffullname mediaregion,a.favifilename,a.fjpgfilename,a.fresult_datetime,a.fresult_unit,a.fresult_user,a.fsend_datetime,ifnull(a.fexaminetime,a.create_time) fsend_time,a.adowner_regionid,a.fsample_id')
      ->join('tadclass b on a.fad_class_code=b.fcode')
      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id and c.fstate = 1 ')
      ->join('tregion tn on a.fregion_id=tn.fid')
      ->join('(select sum(fquantity) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
      ->join('(select distinct(fmediaid) from tmedia_temp where ftype = 1 and fcustomer = "'.$system_num.'" and fuserid='.session('regulatorpersonInfo.fid').') as ttp on a.fmedia_id=ttp.fmediaid')
      ->where($where_tia)
      ->order('x.fstarttime desc')
      ->page($p,$pp)
      ->select();

    if(!empty($outtype)){
        if(empty($do_tia)){
          $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
        }

        $outdata['title'] = session('regulatorpersonInfo.regulatorpname').'广告监管平台-违法广告清单';//文档内部标题名称
        $outdata['datalie'] = [
          '序号'=>'key',
          '地域'=>'mediaregion',
          '发布媒体'=>'fmedianame',
          '媒体分类'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1','电视'],
              ['{fmedia_class} == 2','广播'],
              ['{fmedia_class} == 3','报纸'],
              ['{fmedia_class} == 13','互联网'],
            ]
          ],
          '广告类别'=>'fadclass',
          '广告名称'=>'fad_name',
          '广告主'=>'fadownername',
          '播出总条次'=>'fcount',
          '播出周数'=>'diffweek',
          '播出时间段'=>[
            'type'=>'zwhebing',
            'data'=>'{fstarttime}~{fendtime}'
          ],
          '违法表现代码'=>'fillegal_code',
          '违法表现'=>'fexpressions',
          '涉嫌违法原因'=>'fillegal',
          '证据下载地址'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fmedia_class} == 1 || {fmedia_class} == 2','{favifilename}'],
              ['','{fjpgfilename}']
            ]
          ],
          '处理状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus} == 0','未处理'],
              ['{fstatus} == 10','已上报'],
              ['{fstatus} == 30','数据复核'],
            ]
          ],
          '处理结果'=>'fresult',
          '处理时间'=>'fresult_datetime',
          '处理机构'=>'fresult_unit',
          '立案状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus2} == 15','待处理'],
              ['{fstatus2} == 16','已处理'],
              ['{fstatus2} == 17','处理中'],
              ['{fstatus2} == 20','撤消'],
            ]
          ],
          '复核状态'=>[
            'type'=>'zwif',
            'data'=>[
              ['{fstatus3} == 10','复核中'],
              ['{fstatus3} == 20','复核失败'],
              ['{fstatus3} == 30','复核通过'],
            ]
          ],
        ];
        $outdata['lists'] = $do_tia;
        $ret = A('Api/Function')->outdata_xls($outdata);

        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));

      }else{
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$do_tia)));
      }
  }

}
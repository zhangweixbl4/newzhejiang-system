<?php
namespace Agp\Controller;
use Think\Controller;
class ReportController extends BaseController {

	/*
	* by zw
	* 获取个人报告列表
	*/
	public function reportList(){
		$systemtype = C('FORCE_NUM_CONFIG');
		$pageIndex = I('pageIndex')?I('pageIndex') : 1;//当前页
        $pageSize = I('pageSize')?I('pageSize') : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;

        $regionid = session('regulatorpersonInfo.regionid');

        $where_nc['fregionid'] = ['in',[$regionid,substr($regionid, 0,2),substr($regionid, 0,4)]];

        //查看权限
        $lookType[] = 0;
        if(session('regulatorpersonInfo.fregulatorkind') == 1 || session('regulatorpersonInfo.fregulatorkind') == 2){
        	$lookType[] = 1;
        }else{
        	$lookType[] = session('regulatorpersonInfo.fregulatorkind');
        }
		$where_nc['flooktype'] = ['in',$lookType];

		$where_nc['a.ftype'] = ['between',[11,20]];

		$where_nc['fsystem_type'] = $systemtype;

		$where_nc['fissue_date'] = ['elt',date('Y-m-d')];

		$count = M('tnotice a')
        	->where($where_nc)
        	->count();

		$data = M('tnotice a')
			->field('fid,ftype,ftitle,fissue_date,fcontent,flooktype,ffiledowntype,ffiledown_regionid,ffiles')
			->where($where_nc)
			->order('fissue_date desc')
			->limit($limitIndex,$pageSize)
			->select();

		//有权下载的用户类别
        $downType[] = 0;
		if(session('regulatorpersonInfo.fregulatorkind') == 1 || session('regulatorpersonInfo.fregulatorkind') == 2){
        	$downType[] = 1;
        }else{
        	$downType[] = session('regulatorpersonInfo.fregulatorkind');
        }

        //有权下载的地区
        $downRegion = [$regionid,substr($regionid, 0,2),substr($regionid, 0,4)];
		foreach ($data as $key => $value) {
			if(!in_array($value['ffiledowntype'], $downType) || !in_array($value['ffiledown_regionid'], $downRegion)){
				$data[$key]['ffiles'] = '';
			}
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	}

	/*
	* by zw
	* 公告查看
	*/
	public function reportView(){
		$systemtype = C('FORCE_NUM_CONFIG');
		$fid = I('fid');//ID

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

		$where_nc['a.ftype'] = ['between',[11,20]];
		$where_nc['a.fid'] = $fid;
		$where_nc['a.fsystem_type'] = $systemtype;

		$data = M('tnotice a')
			->field('a.fid,a.flooktype,a.ftype,a.ftitle,a.fissue_date,a.fcontent,a.fiscontain,RPAD(a.fregionid, 6, 0) fregionid')
			->join('tregion b on RPAD(a.fregionid, 6, 0) = b.fid')
			->where($where_nc)
			->find();
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，信息不存在'));
		}
	}

}
<?php
namespace Agp\Model;

class GfunctionModel{

	/**
	 * 获取下级行政机构
	 * $rid 行政区划ID
	 * by zw
	 */
	public function get_region($rid,$regionarr = []){
		$where_tn['fpid'] = $rid;
		$where_tn['fstate'] = 1;
		$do_tn = M('tregion')->alias('a')->cache(true,86400)->join('(select fpid as bfpid,count(*) as fcount from tregion group by fpid) b on a.fid=b.bfpid','left')->where($where_tn)->select();
		foreach ($do_tn as $key => $value) {
			array_push($regionarr, $value['fid']);
			if(!empty($value['fcount'])){
				$regionarr = $this->get_region($value['fid'],$regionarr);
			}
		}
		return $regionarr;
	}

}
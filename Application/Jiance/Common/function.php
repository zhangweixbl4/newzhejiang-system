<?php
    //获取参数
    function getconfig($cgname){

        if($cgname == 'ALL'){
            $cg = config_array($fcustomer);
        }else{
            $cg = config_array($fcustomer)[$cgname];

            if($cgname == 'web_loginwz' || $cgname == 'web_logowz' || $cgname == 'web_name'){
                $cg = htmlspecialchars_decode($cg);
            }
        }

        return $cg;
    }

    //获取配置
    function config_array(){
        header("Content-type: text/html; charset=utf-8");
        $FORCE_NUM_CONFIG = C('FORCE_NUM_CONFIG_JIANCE');
        $config_url = [];//定义客户域名数组
     
        $map['_string'] = 'cr_num = "'.$FORCE_NUM_CONFIG.'" OR cr_url = "'.$FORCE_NUM_CONFIG.'"';
        //查询客户表
        $config_url = M('customer')->field("cr_id,cr_url,cr_num")
            ->where([
                'cr_status'=>1,
                'cr_id'=>["NEQ",1]
            ])
            ->where($map)
            ->order('cr_id')
            ->find();
      
        if(!empty($config_url)){
            //给当前域名下的用户配置参数
            $config_fields = M('customer_systemset')->cache(true,120)
                ->field("
                cst_name,
                cst_val,
                cst_type
                ")
                ->where(['cst_crid'=>$config_url['cr_id']])
                ->select();
            //查询默认配置
            $default_config_fields = M('customer_systemset')->cache(true,600)
                ->field("
                cst_name,
                cst_val,
                cst_type
                ")
                ->where(['cst_crid'=>1])
                ->select();

            $config_fields[] = ['cst_name' => 'system_num','cst_val' => $config_url['cr_num'], 'cst_type' => 'int'];//客户id

            //循环配置参数
            $config_data = [];//清空数组
            $config_fields_array = [];//参数名称数组
            foreach ($config_fields as $config_key=>$config_value){
                $val = $config_value['cst_val'];
                if($config_value['cst_type'] == 'int'){
                    $val = intval($val);
                }
                $config_fields_array[] = $config_value['cst_name'];
                C('GET_DMOAIN_CONFIG.'.$config_value['cst_name'],$val);
            }
            //获取默认配置
            foreach ($default_config_fields as $default_config_key=>$default_config_value){
                if(!in_array($default_config_value['cst_name'],$config_fields_array)){
                    $default_val = $default_config_value['cst_val'];
                    if($default_config_value['cst_type'] == 'int'){
                        $default_val = intval($default_val);
                    }
                    C('GET_DMOAIN_CONFIG.'.$default_config_value['cst_name'],$default_val);
                }
            }
        }
        return C('GET_DMOAIN_CONFIG');

    }


<?php
namespace Jiance\Model;

class ZjMediaIssueModel{

    /**
     * 对广告串播单进行重新排序
     * by zw
     */
    public function adListOrder($adData = [], $issueDate = [],$limitIndex = 20,$pageSize = 1,$adData2 = [],$again = 0,$orderType = 0){
        $adData = pxsf($adData,'fstarttime',false);
        if(!empty($adData)){
            $adList = [];
            foreach($adData as $key=>$row){
                if($key == 0){//第一条
                    $max_endtime = $row['fendtime'];
                    $interval = strtotime($row['fstarttime']) - strtotime($issueDate[0]); //与起始时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $issueDate[0],
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }elseif($key > 0 && $key <= (count($adData)-1)){
                    //两个广告时间间隔大于3秒,则创建一个空时间段
                    if($max_endtime<=$adData[$key-1]['fendtime']){
                        $max_endtime = $adData[$key-1]['fendtime'];
                    }
                   
                    $interval = strtotime($row['fstarttime']) - strtotime($max_endtime);
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $max_endtime,
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
                $row['nevtime'] = $max_endtime;
                array_push($adList,$row);
                //最后一条后的空时间段
                if($key == (count($adData)-1)){
                    $interval = strtotime($issueDate[1]) - strtotime($row['fendtime']); //与结束时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $row['fendtime'],
                            'fendtime'      => $issueDate[1],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
            }
        }else{
            $adNull = [
                'adType'        => 0,
                'fadname'       => '',
                'fadclasscode'  => 0,
                'fstarttime'    => $issueDate[0],
                'fendtime'      => $issueDate[1],
                'flength'       => $interval
            ];
            array_push($adList,$adNull);
        }
        

        //如果有节目的话
        if(!empty($adData2)){
            $adData2 = pxsf($adData2,'fendtime',false);
            $addData = [];
            if(!empty($adList)){
                foreach ($adList as $key => $value) {
                    if(!empty($value['adType'])){//非空时段跳出本次循环
                        continue;
                    }else{
                        unset($adList[$key]);
                    }
                    foreach ($adData2 as $key2 => $value2) {
                        $timeContain = timeContain(strtotime($value['fstarttime']),strtotime($value['fendtime']),strtotime($value2['fstarttime']),strtotime($value2['fendtime']));
                        if(!empty($timeContain['use']) && ($timeContain['ed']-$timeContain['st'])>=3){
                            $longData = $value2;
                            $longData['fstarttime_long'] = $value2['fstarttime'];
                            $longData['fendtime_long'] = $value2['fendtime'];
                            $longData['fstarttime'] = date('Y-m-d H:i:s',$timeContain['st']);
                            $longData['fendtime'] = date('Y-m-d H:i:s',$timeContain['ed']);
                            $longData['flength'] = $timeContain['ed']-$timeContain['st'];
                            $addData[] = $longData;
                        }

                        // if($value['fstarttime']>=$value2['fstarttime'] && $value['fendtime']<=$value2['fendtime']){
                        //     $longData = $value2;
                        //     $longData['fstarttime_long'] = $value2['fstarttime'];
                        //     $longData['fendtime_long'] = $value2['fendtime'];
                        //     $longData['fstarttime'] = $value['fstarttime'];
                        //     $longData['fendtime'] = $value['fendtime'];
                        //     $longData['flength'] = strtotime($value['fendtime'])-strtotime($value['fstarttime']);
                        //     $addData[] = $longData;
                        //     continue;
                        // }
                        // if($value['fstarttime']<=$value2['fstarttime'] && $value['fendtime']>=$value2['fendtime']){
                        //     $longData = $value2;
                        //     $longData['fstarttime_long'] = $value2['fstarttime'];
                        //     $longData['fendtime_long'] = $value2['fendtime'];
                        //     $longData['fstarttime'] = $value2['fstarttime'];
                        //     $longData['fendtime'] = $value2['fendtime'];
                        //     $longData['flength'] = strtotime($value2['fendtime'])-strtotime($value2['fstarttime']);
                        //     $addData[] = $longData;
                        // }

                        // //重置开始结束时间
                        // if($value['fstarttime']<$value2['fstarttime'] && $value2['fstarttime']<$value['fendtime']){
                        //     $value['fendtime'] = $value2['fstarttime'];
                        // }
                        // if($value['fstarttime']<$value2['fendtime'] && $value2['fendtime']<$value['fendtime']){
                        //     $value['fstarttime'] = $value2['fendtime'];
                        // }
                    }
                }
                $adList = array_merge($adList,$addData);
                if(empty($again)){
                    $adList = $this->adListOrder($adList,$issueDate,0,0,[],1);
                }
            }else{
                $adList = $adData2;
            }
        }

        if(empty($again)){
            $adList = $this->adPxsf($adList,$orderType);
            
            $data['jiemucount'] = count($adData2);
            $data['guanggaocount'] = count($adData);
            $data['count'] = count($adList);
            $data['resultList'] = array_slice($adList,$limitIndex,$pageSize);
        }else{
            return $adList;
        }
        return $data;
    }

    /**
     * 对广告串播单进行重新排序
     * by zw
     */
    public function adListOrder_20191209($adData = [], $issueDate = [],$limitIndex = 20,$pageSize = 1,$orderType = 0){
        $adData = pxsf($adData,'fstarttime',false);
        $prgCount = 0;//节目数量
        $adCount = 0;//广告数量
        if(!empty($adData)){
            $adList = [];
            foreach($adData as $key=>$row){
                if($key == 0){//第一条
                    $max_endtime = $row['fendtime'];
                    $interval = strtotime($row['fstarttime']) - strtotime($issueDate[0]); //与起始时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $issueDate[0],
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }elseif($key > 0 && $key <= (count($adData)-1)){
                    //两个广告时间间隔大于3秒,则创建一个空时间段
                    if($max_endtime<=$adData[$key-1]['fendtime']){
                        $max_endtime = $adData[$key-1]['fendtime'];
                    }
                   
                    $interval = strtotime($row['fstarttime']) - strtotime($max_endtime);
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $max_endtime,
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
                $row['nevtime'] = $max_endtime;
                array_push($adList,$row);
                if($row['adType'] == 1){
                    $adCount += 1;
                }elseif($row['adType'] == 2){
                    $prgCount += 1;
                }
                //最后一条后的空时间段
                if($key == (count($adData)-1)){
                    $interval = strtotime($issueDate[1]) - strtotime($row['fendtime']); //与结束时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $row['fendtime'],
                            'fendtime'      => $issueDate[1],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
            }
        }else{
            $adNull = [
                'adType'        => 0,
                'fadname'       => '',
                'fadclasscode'  => 0,
                'fstarttime'    => $issueDate[0],
                'fendtime'      => $issueDate[1],
                'flength'       => $interval
            ];
            array_push($adList,$adNull);
        }
        
        $adList = $this->adPxsf($adList,$orderType);
        
        $data['jiemucount'] = $prgCount;
        $data['guanggaocount'] = $adCount;
        $data['count'] = count($adList);
        $data['resultList'] = array_slice($adList,$limitIndex,$pageSize);
        
        return $data;
    }

	/*
	* 广告排序
	*/
	public function adPxsf($data,$orderType){
		switch ($orderType) {
    		case 1:
    			$data = pxsf($data,'fstarttime',true);
    			break;
    		case 10:
    			$data = pxsf($data,'fadname',false,'cn');
    			break;
    		case 11:
    			$data = pxsf($data,'fadname',true,'cn');
    			break;
    		case 20:
    			$data = pxsf($data,'fadclasscode',false);
    			break;
    		case 21:
    			$data = pxsf($data,'fadclasscode',true);
    			break;
    		case 30:
    			$data = pxsf($data,'flength',false);
    			break;
    		case 31:
    			$data = pxsf($data,'flength',true);
    			break;
            case 40:
                $data = pxsf($data,'fillegaltypecode',false);
                break;
            case 41:
                $data = pxsf($data,'fillegaltypecode',true);
                break;
    		default:
    			$data = pxsf($data,'fstarttime',false);
    			break;
    	}
    	return $data;
	}

    /*
    * by zw
    * 交叉数据获取,
    * $data1 需要比较的开始、结束、节目类别数据，$data2 所有数据的集合
    */
    public function overlappingData($data1 = [],$data2 = [],$startname,$endname){
        foreach ($data2 as $key => $value) {
            $ischeck = true;
            //修改的，属于当前修改的记录不需要验证
            if(!empty($value['issueid'])){
                if(($data1['issueid'] == $value['issueid'] && $data1['adType'] == $value['adType'])){
                    $ischeck = false;
                }
            }

            $baohan = isTimeContain(strtotime($data1[$startname]),strtotime($data1[$endname]),strtotime($value[$startname]),strtotime($value[$endname]));

            if($baohan>0 && $ischeck){
                return $value;
            }
        }
        return false;
    }
}
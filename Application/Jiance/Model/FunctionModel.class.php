<?php
namespace Jiance\Model;

class FunctionModel{

	/**
	 * 获取机构区划
	 * $fid 机构ID
	 * @return array|string data-数据
	 * by zw
	 */
	public function get_xzquhua($fid){
		$db_tr = M('tregulator');
		$do_tr = $db_tr->field('fregionid')->where(['fid'=>$fid])->find();
		return $do_tr['fregionid'];
	}

	/**
	 * 获取地区下所有区域
	 * $fid 行政区划ID
	 * @return array|string data-数据
	 * by zw
	 */
	public function get_nextquhua($fid,$data = []){
		$db_tn = M('tregion');
		$do_tn = $db_tn->alias('a')->field('a.fid,b.bcount')->join('(select fpid,count(*) as bcount from tregion group by fpid) as b on a.fid=b.fpid','left')->where(['a.fpid'=>$fid])->select();
		if(!empty($do_tn)){
			foreach ($do_tn as $key => $value) {
				array_push($data,$value['fid']);
				if($do_tn['bcount']>0){
					$data = $this->get_nextquhua($value['fid'],$data);;
				}
			}
		}
		return $data;
	}

	/**
	 * 机构下部门遍历
	 * $pid 所属机构ID
	 * @return array|string data-数据
	 * by zw
	 */
	public function get_alljgbumen($pid = 0){
		$menutype = getconfig('menutype');
		$system_num = getconfig('system_num');

		$datalist = M('tregulator')
			->field('tregulator.fid,tregulator.fpid,tregulator.fname,tregulator.fcode,tregulator.fkind,tregulator.fdescribe,a.acount')
			->join('(select fpid as pid,count(*) as acount from tregulator group by fpid) a on tregulator.fid=a.pid','left')
			->where('tregulator.fpid='.$pid.' and tregulator.fstate=1')
			->order('tregulator.fkind desc,tregulator.fregionid asc')
			->select();
		foreach ($datalist as $key => $value) {
			$datas[$key] 	= $value;
			$media_jurisdiction = M('tregulatormedia')->join('tmedia on tregulatormedia.fmediaid = tmedia.fid and tmedia.fstate in(0,1)')->where(' fregulatorcode="'.$value['fid'].'" and fisoutmedia = 0 and tregulatormedia.fcustomer = "'.$system_num.'"')->getField('fmediaid',true);
			$menu_jurisdiction = M('tregulatormenu')->join('new_agpmenu on new_agpmenu.menu_id=tregulatormenu.fmenuid')->where('fmenutype='.$menutype.' and fstate=1 and fregulatorid='.$value['fid'].' and tregulatormenu.fcustomer = "'.$system_num.'"')->getField('menu_id',true);
			$datas[$key]['media_jurisdiction'] 	= json_encode($media_jurisdiction);
			$datas[$key]['menu_jurisdiction'] 	= json_encode($menu_jurisdiction);
			if(!empty($value['acount'])){
				$data 		= $this->get_alljgbumen($value['fid']);
				if(!empty($data)){
					$datas[$key]['list'] = $data;
				}
			}
		}
		return $datas;
	}

	/**
	 * 获取当前同级机构动作
	 * $fid 机构父ID
	 * @return array  $data 数据
	 * by zw
	 */
	public function get_same_tregulatoraction($fid,$data = []){
		$where['fpid'] = $fid;
		$where['_string'] = 'ftype=20 and fstate=1';
		$datatl=M('tregulator')->field('fid,fpid,fname')->where($where)->select();
		if(!empty($datatl)){
			foreach ($datatl as $key => $value) {
				array_push($data,$value);
				$data = $this->get_same_tregulatoraction($value['fid'],$data);;
			}
		}
		return $data;
	}

	/**
	 * 获取当前下级机构动作
	 * $fid 机构父ID
	 * @return array  $data 数据
	 * by zw
	 */
	public function get_lower_tregulatoraction($fid){
		$where['fpid'] = $fid;
		$where['_string'] = 'fkind = 1 and fstate=1';
		$data=M('tregulator')->field('fid,fpid,fname')->where($where)->select();
		return $data;
	}

	/**
	 * 获取当前单位所属机构
	 * $fid 机构id
	 * by zw
	 */
	public function get_tregulatoraction($fid){
		$where['fid'] = $fid;
		$where['fstate'] = 1;
		$datatl=M('tregulator')->field('fpid,fkind')->where($where)->find();
		if($datatl['fkind']==1){
			$data = $fid;
		}else{
			$data = $this->get_tregulatoraction($datatl['fpid']);
		}
		return $data;
	}

	/**
	 * 获取当前单位所属机构名称
	 * $fid 机构id
	 * by zw
	 */
	public function get_tregulatornameaction($fid){
		$where['fid'] = $fid;
		$where['fstate'] = 1;
		$datatl=M('tregulator')->field('fpid,fkind,fname')->where($where)->find();
		if($datatl['fkind']==1){
			$data = $datatl['fname'];
		}else{
			$data = $this->get_tregulatornameaction($datatl['fpid']);
		}
		return $data;
	}

	/**
	 * 获取当前单位所属机构的所属机构
	 * $fid 机构id
	 * @return array|string $count 总数  $data 下级数据
	 * by zw
	 */
	public function get_tregulatoractionpid($fid){
		if(!empty($fid)){
			$where['tregulator.fid'] = $fid;
			$where['tregulator.fstate'] = 1;
			$where['a.fstate'] = 1;
			$datatl=M('tregulator')->field('tregulator.fpid,a.fkind,a.fid')->join('tregulator a on tregulator.fpid=a.fid')->where($where)->find();
			if($datatl['fkind']==1){
				$data = $datatl['fid'];
			}else{
				$data = $this->get_tregulatoractionpid($datatl['fpid']);
			}
		}else{
			$data = 0;
		}
		
		return $data;
	}

	/**
	 * 菜单遍历
	 * $pid 所属父菜单ID
	 * @return array|string data-数据
	 * by zw
	 */
	public function get_allmenu($pid = 0,$fregulatorid = 0){
		$menutype = getconfig('menutype');

		$datalist = M('new_agpmenu')
			->field('menu_id,menu_name,menu_url,parent_id,icon,menu_code,acount')
			->join('(select parent_id as pid,count(*) as acount from new_agpmenu group by parent_id) a on new_agpmenu.menu_id=a.pid','left')
			->where(' menu_state=0 and menu_type='.$menutype.' and parent_id='.$pid)
			->select();
		foreach ($datalist as $key => $value) {
			$datas[$key] = $value;
			if(!empty($value['acount'])){
				$data 		= $this->get_allmenu($value['menu_id'],$fregulatorid);
				if(!empty($data)){
					$datas[$key]['list'] = $data;
				}
			}
		}
		return $datas;
	}

	/**
	 * 菜单遍历
	 * $pid 所属父菜单ID
	 * by zw
	 */
	public function get_allmenu2($pid = 0,$fregulatorid = 0,$menuarr = []){
		$menutype = getconfig('menutype');

		$datalist = M('new_agpmenu')
			->field('menu_id,menu_code,menu_name,acount')
			->join('(select parent_id as pid,count(*) as acount from new_agpmenu group by parent_id) a on new_agpmenu.menu_id=a.pid','left')
			->where(' menu_state=0 and menu_type='.$menutype.' and parent_id='.$pid)
			->select();
		foreach ($datalist as $key => $value) {
			array_push($menuarr, $value['menu_id']);
			if(!empty($value['acount'])){
				$menuarr = $this->get_allmenu2($value['menu_id'],$fregulatorid,$menuarr);
			}
		}
		return $menuarr;
	}

	/**
	 * 用户的菜单权限遍历
	 * $menuarr as array 用户的菜单权限
	 * by zw
	 */
	public function get_allmenu3($menuarr = []){
		$menutype = getconfig('menutype');
		
		if(!empty($menuarr)){
			$where['_string'] = 'menu_id in (select parent_id from new_agpmenu where parent_id>0 and menu_type='.$menutype.' and menu_state=0 and menu_id in ('.implode(',', $menuarr).') group by parent_id) or menu_id in (select menu_id from new_agpmenu where parent_id=0 and menu_type='.$menutype.' and menu_state=0 and menu_id in ('.implode(',', $menuarr).'))';
			$datalist = M('new_agpmenu')
				->field('menu_id,menu_name,parent_id,menu_code,icon,menu_url')
				->where($where)
				->order('menu_sort asc,menu_id asc')
				->select();//获取有子栏目的父目录信息
			foreach ($datalist as $key => $value) {
				if($value['menu_url'] == "#"){
					$where2['menu_id'] 	= array('in',$menuarr);
					$where2['menu_state'] 	= 0;
					$where2['menu_type'] 	= $menutype;
					$where2['parent_id'] 	= $value['menu_id'];
					$datalist[$key]['list'] = M('new_agpmenu')
						->field('menu_id,menu_name,menu_code,icon,menu_url')
						->where($where2)
						->order('menu_sort asc,menu_id asc')
						->select();
					if(empty($datalist[$key]['list'])){
						unset($datalist[$key]);
					}
				}
			}
			foreach ($datalist as $key => $value) {
				$datalist2[] = $value;
			}
		}

		return $datalist2;
	}

	/**
	 * 获取当前机构的最终菜单权限，用于登录
	 * $fregulatorid机构ID，$menu_jurisdiction原菜单权限
	 * by zw
	 */
	public function get_menujurisdiction($fregulatorid,$menu_jurisdiction = []){
		$menutype = getconfig('menutype');
		$system_num = getconfig('system_num');

		$do_tr = M('tregulator')->field('fpid,fkind')->where(['fid'=>$fregulatorid])->find();

		//菜单权限
    	$do_tatre = M('tregulatormenu') ->where(' fmenutype='.$menutype.' and fregulatorid='.$fregulatorid.' and fcustomer = "'.$system_num.'"') ->getField('fmenuid',true);
    	if(empty($do_tatre)&&$do_tr['fkind']==1){
    		$do_nau = M('new_agpmenu')->field('menu_id,parent_id')->where('parent_id<>0 and menu_default=0 and menu_state=0 and menu_type='.$menutype)->select();
    		$adddata = [];
    		$do_tatre = [];
    		foreach ($do_nau as $key => $value) {
				$adddata[$key]['fregulatorid'] 	= $fregulatorid;
				$adddata[$key]['fmenutype'] 	= $menutype;
				$adddata[$key]['fmenuid'] 		= $value['menu_id'];
				$adddata[$key]['fmenupid'] 		= $value['parent_id'];
				$adddata[$key]['fcreator'] 		= session('regulatorpersonInfo.fid');
				$adddata[$key]['fcreatetime'] 	= date('Y-m-d H:i:s');
				$adddata[$key]['fstate'] 		= 1;
				$adddata[$key]['fstate'] 		= $system_num;
				array_push($do_tatre, $value['menu_id']);
			}
			if(!empty($adddata)){
				M('tregulatormenu')->addAll($adddata);
			}
    	}

    	if(empty($menu_jurisdiction)){
    		$menu_jurisdiction = $do_tatre;
    	}

    	$a = $do_tatre;
		$b = $menu_jurisdiction;
		$c = array_merge(array_diff($a,array_diff($a, $b)));
    	if($do_tr['fkind']==2){
			$c = $this->get_menujurisdiction($do_tr['fpid'],$c);
    	}else{
    		if(empty($c)){
    			$c = [];
    		}else{
    			// $c = M('new_agpmenu') ->where(array('menu_id'=>array('in',$c))) ->getField('menu_code',true);
    		}
    	}
    	
    	return $c;

	}

	/**
	 * 获取机构下的媒体
	 * $fid 机构ID
	 * by zw
	 */
	public function get_allmedia($fid){
		$system_num = getconfig('system_num');
		if($system_num == '100000') {
			$datalist = M('tregulator') 
				->alias('a') 
				->field('b.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame') 
				->join('tmedia b on a.fregionid=b.media_region_id and b.fid=b.main_media_id and b.fstate = 1')  
				->where('b.fmedianame<>" " and a.fmediaid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") and a.fid ='.$fid) 
				->select();
		}else{
			$datalist = M('tregulatormedia') 
				->alias('a') 
				->field('b.fid,b.fmedianame') 
				->join('tmedia b on a.fmediaid=b.fid and b.fid=b.main_media_id and b.fstate = 1')  
				->where('b.fmedianame<>" " and fisoutmedia = 0 and fregulatorcode='.$fid.' and a.fcustomer = "'.$system_num.'"') 
				->select();
		}
		return $datalist;
	}

	/**
	 * 获取机构下的媒体
	 * $fid 机构ID
	 * by zw
	 */
	public function get_allmedia2($fid){
		$system_num = getconfig('system_num');
		if($system_num == '100000') {
			$datalist = M('tregulator')  
				->alias('a') 
				->join('tmedia b on a.fregionid=b.media_region_id and b.fid=b.main_media_id and b.fstate = 1')
				->where('b.fmedianame<>" " and a.fmediaid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") and a.fid ='.$fid) 
				->getField('main_media_id',true);
		}else{
			$datalist = M('tregulatormedia') 
				->alias('a') 
				->field('a.fmediaid') 
				->join('tmedia b on a.fmediaid=b.fid and b.fid=b.main_media_id and b.fstate = 1')  
				->where('b.fmedianame<>" " and fisoutmedia = 0 and fregulatorcode='.$fid.' and a.fcustomer = "'.$system_num.'"') 
				->getField('fmediaid',true);
		}

		return $datalist;
	}

	/**
	 * 获取所选部门所在机构的媒介权限
	 * $fpid 机构ID
	 * by zw
	 */
	public function get_bumenmedia(){
		$mediaarr = M('tmedia')
			->alias('a')
			->field('a.fid,concat((case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end),"【",b.fname,(case when left(fmediaclassid,2) = "01" then "，电视" when left(fmediaclassid,2) = "02" then "，广播" when left(fmediaclassid,2) = "03" then "，报纸" when left(fmediaclassid,2) = "04" then "，期刊" when left(fmediaclassid,2) = "05" then "，户外" when left(fmediaclassid,2) = "13" then "，互联网" end),"】") as fmedianame')
			->join('tregion b on a.media_region_id = b.fid')
			->where(['a.fstate'=>1])
			->order('media_region_id asc,fmediaclassid asc,fmedianame asc')
			->select();

		return $mediaarr;
	}

	/**
	 * 获取当前机构的最终媒介权限，用于登录
	 * $fregulatorid机构ID，$media_jurisdiction用户媒介权限
	 * by zw
	 */
	public function get_mediajurisdiction($fregulatorid,$jgmedia = [],$media_jurisdiction = [],$se = 0){
		$system_num = getconfig('system_num');
		if(empty($jgmedia)){
    		$trepid 	= $this->get_tregulatoraction($fregulatorid);
    		$jgmedia 	= $this->get_parentmedia($trepid);
    	}
		$do_tr = M('tregulator')->field('fpid,fkind,fregionid')->where('fid='.$fregulatorid)->find();

		if(!empty($se)){
			if($system_num == '100000') {
				$media_jurisdiction = M('tmedia')
					->alias('a')
			    	->where('a.fid=a.main_media_id and a.fstate = 1 and a.fmedianame<>" " and a.fid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") and a.media_region_id='.$do_tr['fregionid'])
			    	->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
			    	->getField('main_media_id',true);
			}else{
				$media_jurisdiction = M('tregulatorpersonmedia')
					->alias('a')
					->join('tmedia b on a.fmediaid=b.fid and b.fid=b.main_media_id and b.fstate = 1') 
					->where(' b.fmedianame<>" " and fpersonid='.$se.' and a.fcustomer = "'.$system_num.'"') 
					->order('media_region_id asc,fmediaclassid asc,fmedianame asc')
					->getField('fmediaid',true);
			}
			
		}

		//媒体权限
		if($system_num == '100000') {
			$do_tatre = M('tmedia')
				->alias('a')
		    	->where('a.fid=a.main_media_id and a.fstate = 1 and a.fmedianame<>" " and a.fid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") and a.media_region_id='.$do_tr['fregionid'])
		    	->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
		    	->getField('main_media_id',true);
		}else{
			$do_tatre = M('tregulatormedia')
		    	->alias('a')
		    	->join('tmedia b on b.fid=a.fmediaid and b.fid=b.main_media_id and b.fstate = 1')
		    	->where('a.fregulatorcode='.$fregulatorid.' and b.fmedianame<>" " and fisoutmedia = 0 and a.fcustomer = "'.$system_num.'"')
		    	->order('media_region_id asc,fmediaclassid asc,fmedianame asc')
		    	->getField('fmediaid',true);
		}

    	if(empty($do_tatre)){
    		$do_tatre = $jgmedia;
    	}

    	//如果未授权则继承所属单位权限
    	if(empty($media_jurisdiction) && !empty($se)){
    		$media_jurisdiction = $do_tatre;
    	}

    	$a 	= $do_tatre;
		$b 	= $media_jurisdiction;
		$c 	= array_merge(array_diff($a,array_diff($a, $b)));
    	if($do_tr['fkind']==2){
			$c = $this->get_mediajurisdiction($do_tr['fpid'],$jgmedia,$c,0);
    	}
    	
    	return $c;
	}

	/**
	 * 获取机构的媒介权限，用于登录
	 * $fregulatorid机构ID，$media_jurisdiction用户媒介权限
	 * by zw
	 */
	public function get_parentmedia($fregulatorid){
		$system_num = getconfig('system_num');
		$do_tr = M('tregulator')->field('fregionid')->where(['fid'=>$fregulatorid])->find();
		//媒体权限
		if($system_num == '100000') {
			$do_tatre = M('tmedia')
				->alias('a')
		    	->where('a.fid=a.main_media_id and a.fstate = 1 and a.fmedianame<>" " and a.fid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") and a.media_region_id='.$do_tr['fregionid'])
		    	->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
		    	->getField('main_media_id',true);
		}else{
			$do_tatre = M('tregulatormedia')
		    	->alias('a')
		    	->join('tmedia b on b.fid=a.fmediaid and b.fid=b.main_media_id and b.fstate = 1')
		    	->where('a.fregulatorcode='.$fregulatorid.' and fisoutmedia = 0 and b.fmedianame<>" " and a.fcustomer = "'.$system_num.'"')
		    	->order('media_region_id asc,fmediaclassid asc,fmedianame asc')
		    	->getField('fmediaid',true);
		}
    	if(empty($do_tatre)){
    		$do_tatre2=[];
    		if($system_num == '100000') {
    			$do_ta = M('tmedia')
					->alias('a')
			    	->where('a.fid=a.main_media_id and a.fstate = 1 and a.fmedianame<>" " and a.fid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") and a.media_region_id='.$do_tr['fregionid'])
			    	->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
			    	->getField('main_media_id',true);
    		}else{
				$do_ta = M('tmedia')
					->field('tmedia.fid')
					->where('tmedia.fid=tmedia.main_media_id and tmedia.fmedianame<>" " and tmedia.fstate = 1 and tmedia.media_region_id='.$do_tr['fregionid'])
					->order('media_region_id asc,fmediaclassid asc,fmedianame asc')
					->select();
    		}
    		
    		foreach ($do_ta as $key => $value) {
    			array_push($do_tatre2, $value['fid']);
    		}
    		$do_tatre = $do_tatre2;
    	}
    	
    	return $do_tatre;
	}

	/**
	 * 获取用户的媒介列表，用在用户权限选择
	 * $treid 机构ID
	 * by zw
	 */
    public function m_alljgmedialist($treid,$mediaarr = []){
    	$system_num = getconfig('system_num');
    	$do_tr = M('tregulator')->field('fpid,fkind,fregionid')->where(['fid'=>$treid])->find();
    	if($system_num == '100000') {
			$do_ta = M('tmedia')
				->alias('a')
		    	->where('a.fid=a.main_media_id and a.fstate = 1 and a.fmedianame<>" " and a.fid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") and a.media_region_id='.$do_tr['fregionid'])
		    	->order('a.media_region_id asc,a.fmediaclassid asc,a.fmedianame asc')
		    	->getField('main_media_id',true);
    	}else{
    		$do_ta = M('tregulatormedia')
		    	->alias('a')
		    	->join('tmedia b on b.fid=a.fmediaid and b.fid=b.main_media_id and b.fstate = 1 ')
		    	->where('a.fregulatorcode='.$treid.' and b.fmedianame<>" " and fisoutmedia = 0 and a.fcustomer = "'.$system_num.'"')
		    	->order('media_region_id asc,fmediaclassid asc,fmedianame asc')
		    	->getField('fmediaid',true);
    	}
    	
    	if(!empty($do_ta)){
    		if(empty($mediaarr)){
    			$mediaarr = $do_ta;
    		}
    	}
    	$a 	= $do_ta;
		$b 	= $mediaarr;
		$c 	= array_merge(array_diff($a,array_diff($a, $b)));
    	if($do_tr['fkind']==2){
			$mediaarr = $this->m_alljgmedialist($do_tr['fpid'],$c);
    	}else{
    		$mediaarr = $c;
    		if(!empty($mediaarr)){
    			if($system_num == '100000') {
    				$mediaarr = M('tmedia')
    					->alias('a')
    					->field('fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
    					->where(array('a.fid'=>array('in',$mediaarr),'fmedianame'=>array('neq'," "),'_string'=>'a.fid=a.main_media_id and a.fstate = 1 and a.fid in (select fmediaid from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'")'))
    					->order('media_region_id asc,fmediaclassid asc,fmedianame asc')
    					->select();
    			}else{
    				$mediaarr = M('tmedia')->field('fid,fmedianame')->where(array('fid'=>array('in',$mediaarr),'fmedianame'=>array('neq'," "),'_string'=>'tmedia.fid=tmedia.main_media_id and tmedia.fstate = 1'))->order('media_region_id asc,fmediaclassid asc,fmedianame asc')->select();
    			}
    		}
    	}
    	
    	return $mediaarr;
    }

	/**
	 * 获取默认菜单
	 * by zw
	 */
	public function get_menudefault(){
		$menutype = getconfig('menutype');
		$do_nau = M('new_agpmenu')->field('menu_id')->where('parent_id<>0 and menu_default=0 and menu_state=0 and menu_type='.$menutype)->select();
		$menuarr = [];
		return $menuarr;
	}

	/**
	 * 操作日志
	 * by zw
	 * type 操作类型，status 状态，remark 备注，cor_table 对应表，cor_table_id 表ID，code_sql 代码
	 */
	public function write_log($type='',$status = 1,$remark = '',$cor_table = '',$cor_table_id = 0,$code_sql = ''){
		$system_num = getconfig('system_num');
		$adddata['type'] 			= $type;
		$adddata['status'] 			= $status;
		$adddata['remark'] 			= $remark;
		$adddata['cor_table'] 		= $cor_table;
		$adddata['cor_table_id'] 	= $cor_table_id;
		$adddata['code_sql'] 		= $code_sql;
		$adddata['fcustomer'] 		= $system_num;
		$adddata['user_ip'] 		= getRealIp();
		$adddata['record_date'] 	= date('Y-m-d H:i:s');
		$adddata['fregulatorpid'] 	= session('regulatorpersonInfo.fregulatorpid')?session('regulatorpersonInfo.fregulatorpid'):0;
		$adddata['fregulatorid'] 	= session('regulatorpersonInfo.fregulatorid')?session('regulatorpersonInfo.fregulatorid'):0;
		$adddata['fuserid'] 		= session('regulatorpersonInfo.fid')?session('regulatorpersonInfo.fid'):0;
		$adddata['fregulatorpname'] = session('regulatorpersonInfo.regulatorpname')?session('regulatorpersonInfo.regulatorpname'):'';
		$adddata['fregulatorname']  = session('regulatorpersonInfo.regulatorname')?session('regulatorpersonInfo.regulatorname'):'';
		$adddata['fusername'] 		= session('regulatorpersonInfo.fname')?session('regulatorpersonInfo.fname'):'';
		M('f_log_manage')->add($adddata);
	}

	/**
	 * 更新待办任务状态
	 * by zw
	 */
	public function update_task($url,$name,$count,$cs = ''){
		$system_num = getconfig('system_num');
		$eurl = explode('|', $url)[0];
		$menutype = getconfig('menutype');
		$menu_code = M('new_agpmenu')->where(['menu_url'=>$eurl,'menu_type'=>$menutype])->getfield('menu_code');
		$menu_code = $menu_code?$menu_code:'';

		$where_wk['fuserid'] 	= session('regulatorpersonInfo.fid');
		$where_wk['furl'] 		= $url;
		$where_wk['fcanshu'] 	= $cs;
		$where_wk['fmenucode'] 	= $menu_code;
		$where_wk['fcustomer'] 	= $system_num;
		$do_wk = M('tbn_waittask')->field('ftaskcount')->where($where_wk)->find();
		if(!empty($do_wk)){
			if($do_wk['do_wk'] != $count){
				$data_wk['fname'] 		= $name;
				$data_wk['ftaskcount'] 	= $count;
				$data_wk['fupdatetime'] = date('Y-m-d H:i:s');
				M('tbn_waittask')->where($where_wk)->save($data_wk);
			}
		}else{
			$data_wk['fname'] 		= $name;
			$data_wk['furl'] 		= $url;
			$data_wk['fcanshu'] 	= $cs;
			$data_wk['fmenucode'] 	= $menu_code;
			$data_wk['ftrepid'] 	= session('regulatorpersonInfo.fregulatorid');
			$data_wk['fuserid'] 	= session('regulatorpersonInfo.fid');
			$data_wk['fupdatetime'] = date('Y-m-d H:i:s');
			$data_wk['ftaskcount'] 	= $count;
			$data_wk['fcustomer'] 	= $system_num;
			M('tbn_waittask')->add($data_wk);
		}
	}

}
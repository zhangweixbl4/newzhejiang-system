<?php
namespace Jiance\Model;

class MediaIssueModel{

    /*
    * by zw
    * 更改任务状态
    */
    public function updateTaskStatus($pushData){
    	$saveData['fstatus'] = $pushData['fstatus'];
    	$saveData['fupdate_userid'] = $pushData['userData']['fupdate_userid']?$pushData['userData']['fupdate_userid']:0;
    	$saveData['fupdate_user'] = $pushData['userData']['fupdate_user']?$pushData['userData']['fupdate_user']:'AI';
    	$saveData['fupdate_time'] = date('Y-m-d H:i:s');
    	$saveData['flog'] = ['exp','CONCAT("'.$pushData['returnmsg'].'",flog)'];
        $ret = M('tmediaissue')
        	->where(['fid'=>$pushData['taskid']])
        	->save($saveData);
        return $ret;
    }
	
}
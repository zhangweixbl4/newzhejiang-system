<?php
namespace Jiance\Model;



class MenuModel{


    /*获取菜单*/
    public function menu_list($menu_id){
		$my_fid = session('regulatorpersonInfo.fid');
		 $myInfo = M('tregulatorperson')->cache(true,600)
							->field('tregulatorperson.*,tregulator.ffullname as org_fullname,tregion.ffullname as region_fullname')
							->join('tregulator on tregulator.fid = tregulatorperson.fregulatorid')
							->join('tregion on tregion.fid = tregulatorperson.fregionid','LEFT')
							->where(array('tregulatorperson.fid'=>$my_fid))->find();
		$special_menu = json_decode($myInfo['special_menu'],true);		
		if($special_menu) return $special_menu;
							
							
		$menu_1 = M('gongshang_menu')->cache(true,600)->where(array('parent_id'=>$menu_id,'menu_state'=>0))->order('menu_sort asc')->select();//查询一级菜单
		
		 foreach($menu_1 as $key => $menu){//遍历一级菜单
			$menu_list[$key] = $menu;

			 $menu_list[$key]['menu2'] = M('gongshang_menu')->cache(true,600)->where(array('parent_id'=>$menu['menu_id'],'menu_state'=>0))->order('menu_sort asc')->select();//查询二级菜单
			
			foreach($menu_list[$key]['menu2'] as $key2 => $menu2){//遍历二级菜单
				$menu_list[$key]['menulist'][$key2] = $menu2;
				
				$menu_list[$key]['menulist'][$key2]['menu3_list'] = M('gongshang_menu')->cache(true,600)->where(array('parent_id'=>$menu2['menu_id'],'menu_state'=>0))->order('menu_sort asc')->select();//查询三级菜单
			
			}//遍历二级菜单结束
			
			$menu_list[$key]['menu2'] = null;//销毁原二级菜单

		}//遍历一级菜单结束 

		return $menu_list;
    }



}
<?php
namespace Jiance\Model;
use Think\Model;

/**
 * Class MenuListModel
 * @package Agp\Model
 * 工商菜单
 * by hs
 */



class MenuListModel extends Model{

    protected $tableName  = 'gongshang_menu';

    /**
     * @param int $menu_id 指定的父级id
     * @return mixed
     * 获取下面的菜单
     * by  hs
     */
    public function clist($menu_id = 0,$role='gongshang'){
        $model=M($role.'_menu');
							
		$menu = $model->where(array('parent_id'=>$menu_id,'menu_state'=>0))->order('menu_sort asc')->select();//查询一级菜单

		 foreach($menu as $key => $value){//获取二级菜单
			 $menu[$key]['menulist'] = $model->where(array('parent_id'=>$value['menu_id'],'menu_state'=>0))->order('menu_sort asc')->select();//查询二级菜单
			 $data=$menu[$key]['menulist'];
             if($data){
                 foreach($data as $k => $v){//获取三级菜单
                     $data[$k]['menulist'] = $model->where(array('parent_id'=>$v['menu_id'],'menu_state'=>0))->order('menu_sort asc')->select();//查询三级菜单
                 }
             }
			 $menu[$key]['menulist']=$data;
		}
		return $menu;
    }


}
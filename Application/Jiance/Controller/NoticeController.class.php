<?php
namespace Jiance\Controller;
use Think\Controller;
class NoticeController extends BaseController {

	/*
	* by zw
	* 获取公告列表
	*/
	public function noticeList(){
		$getData = $this->oParam;
		$systemtype = C('FORCE_NUM_CONFIG_JIANCE');
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
		$nTitle = $getData['nTitle'];//标题
		$nType = $getData['nType'];//类型，1安全，2通知，3升级
		$issueDate = $getData['issueDate'];//日期范围，数组

		if(!empty($nType)){
			$where_nc['ftype'] = $nType;
		}
		if(!empty($nTitle)){
			$where_nc['ftitle'] = ['like','%'.$nTitle.'%'];
		}
		if(!empty($issueDate)){
			$where_nc['fissue_date'] = ['between',[$issueDate[0],$issueDate[1]]];
		}
		$where_nc['fsystem_type'] = $systemtype;
		$count = M('tnotice')
        	->where($where_nc)
        	->count();

		$data = M('tnotice')
			->field('fid,ftype,ftitle,fissue_date,fcontent')
			->where($where_nc)
			->order('fissue_date desc')
			->limit($limitIndex,$pageSize)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	}

	/*
	* by zw
	* 公告查看
	*/
	public function noticeView(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//ID

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

		$data = M('tnotice')->where(['fid'=>$fid])->find();
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，信息不存在'));
		}
	}

	/*
	* by zw
	* 公告添加
	*/
	public function noticeCreate(){
		$getData = $this->oParam;
		$systemtype = C('FORCE_NUM_CONFIG_JIANCE');
		$nTitle = $getData['nTitle'];//标题
		$nType = $getData['nType'];//类型，1安全，2通知，3升级
		$nContent = $getData['nContent'];//内容
		$issueDate = $getData['issueDate']?$getData['issueDate']:date('Y-m-d');//公告日期

		if(empty($nTitle) || empty($nType) || empty($nContent)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'信息填写不完整'));
		}

		$data_nc['ftitle'] = $nTitle;
		$data_nc['ftype'] = $nType;
		$data_nc['fcontent'] = $nContent;
		$data_nc['fissue_date'] = $issueDate;
		$data_nc['fsystem_type'] = $systemtype;
		$data_nc['fcreate_time'] = date('Y-m-d H:i:s');
		$data_nc['fcreate_user'] = session('personData.fname');
		$do_nc = M('tnotice')->add($data_nc);
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/*
	* by zw
	* 公告修改
	*/
	public function noticeSave(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//ID
		$nTitle = $getData['nTitle'];//标题
		$nType = $getData['nType'];//类型，1安全，2通知，3升级
		$nContent = $getData['nContent'];//内容
		$issueDate = $getData['issueDate']?$getData['issueDate']:date('Y-m-d');//公告日期

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}
		if(empty($nTitle) || empty($nType) || empty($nContent)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'信息填写不完整'));
		}

		$data_nc['ftitle'] = $nTitle;
		$data_nc['ftype'] = $nType;
		$data_nc['fcontent'] = $nContent;
		$data_nc['fissue_date'] = $issueDate;
		$data_nc['fmodify_time'] = date('Y-m-d H:i:s');
		$data_nc['fmodify_user'] = session('personData.fname');
		$do_nc = M('tnotice')->where(['fid'=>$fid])->save($data_nc);
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'保存成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，信息不存在'));
		}
	}

	/*
	* by zw
	* 公告删除
	*/
	public function noticeDel(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//ID

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

		$do_nc = M('tnotice')->where(['fid'=>$fid])->delete();
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，信息不存在'));
		}
	}

}
<?php
namespace Jiance\Controller;
use Think\Controller;
class AdController extends BaseController {

	/*
	* by zw
	* 获取广告列表
	*/
	public function adList(){
		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
		$adName = $getData['adName'];//广告名
		$status = $getData['status'] == ''?1:$getData['status'];//状态，0无效，其他值有效
		$adClass = $getData['adClass'];//广告类别，数组类型
		$adOwner = $getData['adOwner'];//广告主
		$brand = $getData['brand'];//品牌
		$orderType = $getData['orderType'] == ""?-1:$getData['orderType'];//排序方式，0、1广告名，10、11广告类别，20、21广告主，30、31品牌

		switch ($orderType) {
			case 0:
    			$order = 'CONVERT(a.fadname using gbk)  asc';
    			break;
    		case 1:
    			$order = 'CONVERT(a.fadname using gbk)  desc';
    			break;
    		case 10:
    			$order = 'a.fadclasscode asc';
    			break;
    		case 11:
    			$order = 'a.fadclasscode desc';
    			break;
    		case 20:
    			$order = 'CONVERT(b.fname using gbk)  asc';
    			break;
    		case 21:
    			$order = 'CONVERT(b.fname using gbk)  desc';
    			break;
    		case 30:
    			$order = 'CONVERT(a.fbrand using gbk)  asc';
    			break;
    		case 31:
    			$order = 'CONVERT(a.fbrand using gbk)  desc';
    			break;
    		default:
    			$order = 'a.fadid desc';
    			break;
    	}

		if(!empty($adName)){
			$where_ad['a.fadname'] = ['like',$adName];
		}

		if(!empty($brand)){
			$where_ad['a.fbrand'] = ['like','%'.$brand.'%'];
		}

		if(!empty($adOwner)){
			$where_ad['b.fname'] = ['like','%'.$adOwner.'%'];
		}

		if(is_array($adClass) && !empty($adClass)){
			$arr_code = [];
			foreach ($adClass as $key => $value) {
				$codes = D('Common/Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
				if(!empty($codes)){
					$arr_code = array_merge($arr_code,$codes);
				}else{
					array_push($arr_code,$value);
				}
			}
			$where_ad['a.fadclasscode'] = ['in',$arr_code];
		}

		if($status == 1){
			$where_ad['a.fstate'] = ['gt',0];
		}else{
			$where_ad['a.fstate'] = 0;
		}

		$count = M('tad a')
			->join('tadowner b on a.fadowner = b.fid')
			->join('tadclass c on a.fadclasscode = c.fcode')
        	->where($where_ad)
        	->count();

		$data = M('tad a')
			->field('a.fadid,a.fadname,a.fbrand,a.fadowner,a.fadclasscode,a.fstate,c.ffullname,b.fname,a.fmodifier,a.fmodifytime')
			->join('tadowner b on a.fadowner = b.fid')
			->join('tadclass c on a.fadclasscode = c.fcode')
			->where($where_ad)
			->order($order)
			->limit($limitIndex,$pageSize)
			->select();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	}

	/*
	* by zw
	* 获取广告修改
	*/
	public function adAction(){
		$getData = $this->oParam;
		$adId = $getData['adId'];//广告ID
		$adName = $getData['adName'];//广告名
		$status = $getData['status'];//状态，0无效，其他值有效
		$adClass = $getData['adClass'];//广告类别，数组类型
		$adOwner = $getData['adOwner'];//广告主
		$brand = $getData['brand'];//品牌

		$oldAd = M('tad')
			->where(['fadid'=>$adId])
			->find();
		if(empty($oldAd)){
			$this->ajaxReturn(['code'=>1,'msg'=>'广告名不存在']);
		}

		$adOwnerId = M('tadowner')->where(['fname'=>$adOwner])->getField('fid');
		$adOwnerId = $adOwnerId?$adOwnerId:0;

		$saveAdData['fadname'] = $adName;
		$saveAdData['fadclasscode'] = $adClass;
		$saveAdData['fadowner'] = $adOwnerId;
		$saveAdData['fbrand'] = $brand;
		$saveAdData['fstate'] = $status;
		$checkEditData = A('Function')->checkEditData($oldAd,$saveAdData,['fadname','fadclasscode','fadowner','fbrand','fstate']);//获取修改前后差异数据

		if(!empty($checkEditData)){
			$saveAdData['fself'] = 1;
			$saveAdData['fmodifier'] = session('personData.fname');
			$saveAdData['fmodifytime'] = date('Y-m-d H:i:s');
			$saveAdData['comment'] = ['exp','CONCAT("操作时间：'.date('Y-m-d H:i:s').' 操作人：'.session('personData.fname').' 事件：'.$checkEditData.'；",comment)'];

			M('tad')->where(['fadid'=>$adId])->save($saveAdData);

			$this->ajaxReturn(['code'=>0,'msg'=>'修改成功']);
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'修改失败']);
		}

	}

}
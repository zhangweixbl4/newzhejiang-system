<?php
namespace Jiance\Controller;
use Think\Controller;
class PlayManageController extends BaseController {

	/*
	* by zw
	* 获取串播单列表
	*/
	public function playList(){
		session_write_close();
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');
		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $isNotBusiness = $getData['isNotBusiness']?$getData['isNotBusiness']:0;//是否包含非商业类
        $limitIndex = ($pageIndex-1)*$pageSize;
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$issueTime = $getData['issueTime'];//时间范围，数组
		$isCreate = $getData['isCreate'];//重新生成，1生成，0或空不生成
		$outType = $getData['outType'];//导出类型

		if(empty($outType)){
			$limitstr = ' limit '.($pageIndex-1)*$pageSize.','.$pageSize;
		}
		$whereStr = '1=1';
		$whereStr1 = '1=1';
		$whereStr2 = '1=1';

		// $mediaId = 11000010002362;
		// $issueDate = ['2019-11-1','2019-11-3'];
		// if(!empty($issueTime)){
		// 	$issueTime = ['00:00:00','23:59:59'];
		// }

		if(empty($mediaId)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请输入需要查询的媒体'));
		}
		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}
		$whereMedia['a.fid'] = $mediaId;
		$whereMedia['a.fstate'] = 1;
		$whereMedia['a.media_region_id'] = ['like','33%'];
		$doMedia = M('tmedia')
			->alias('a')
			->field('a.fmediaclassid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
			->join('tmedialabel b on a.fid = b.fmediaid and b.flabelid in(446,449)')
			->where($whereMedia)
			->find();
		if(empty($doMedia)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'非串播单媒体'));
		}

		$mclass = substr($doMedia['fmediaclassid'],0,2);
		if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'不支持该媒体类型'));
        }

		if(!empty($issueDate)){
			$whereStr1 .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
			$whereStr2 .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		}
		if(!empty($issueTime)){
			$whereStr1 .= ' and DATE_FORMAT(a.fstarttime,"%H:%i:%s") between "'.$issueTime[0].'" and "'.$issueTime[1].'"';
			$whereStr2 .= ' and DATE_FORMAT(a.fstart,"%H:%i:%s") between "'.$issueTime[0].'" and "'.$issueTime[1].'"';
		}
		if(!empty($mediaId)){
			$whereStr1 .= ' and a.fmediaid = '.$mediaId;
			$whereStr2 .= ' and a.fmediaid = '.$mediaId;
		}
		if(empty($isNotBusiness)){
			$whereStr1 .= ' and a.fissuetype <>""';
		}

		// if(!empty($isCreate)){
		// 	$this->createPlayIssueData($mclass,$mediaId,$issueDate);
		// }

		$whereStr1 .= ' and a.fstate = 1 and b.fstate = 1 and a.fsendstate in(10,20)';
		$whereStr2 .= ' and a.fstate = 1';
		$countSql = '
			select count(*) acount from (
				(
					select 1 adtype 
					from t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' a
					inner join t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)
					inner join tad c on b.fadid = c.fadid and c.fstate in(1,2,9)
					where '.$whereStr1.'
				)
				union all 
				(
					select 2 adtype
					from t'.$tb_str.'prog_detail a
					inner join tprogclass b on a.fclasscode = b.fprogclasscode
					where '.$whereStr2.'
				)
			) a
			where '.$whereStr.'
		';
		$count = M()->query($countSql);

		$sqlStr = '
			select *,date_format(a.fstarttime,"%H:%i:%s") sttime,date_format(a.fendtime,"%H:%i:%s") edtime from (
				(
					select 1 adtype,a.fmediaid,a.fissuedate,a.fstarttime,a.fendtime,a.flength,c.fadname title,a.fissuetype,a.fadsort
					from t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' a
					inner join t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)
					inner join tad c on b.fadid = c.fadid and c.fstate in(1,2,9)
					where '.$whereStr1.'
				)
				union all 
				(
					select 2 adtype,a.fmediaid,a.fissuedate,a.fstart fstarttime,a.fend fendtime,(unix_timestamp(a.fend)-unix_timestamp(a.fstart)) flength,a.fcontent title,"" fissuetype,"" fadsort
					from t'.$tb_str.'prog_detail a
					inner join tprogclass b on a.fclasscode = b.fprogclasscode
					where '.$whereStr2.'
				)
			) a
			where '.$whereStr.'
			order by fissuedate asc,fstarttime asc
			'.$limitstr.'
		';

		$data = M()->query($sqlStr);
		if(!empty($outType)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}else{
				$adCount = 0;//广告条次
	        	$flengths = 0;//广告总条次
				foreach ($data as $key => $value) {
					$data[$key]['fissuedate'] = date('Y-m-d',strtotime($value['fissuedate']));
					if($value['adtype'] == 1){
						$data[$key]['title'] = '  '.$value['title'];

						$adCount += 1;
						$data[$key]['flength'] = $this->chuanBoYuanZheng($value['flength']);
		        		$flengths += $data[$key]['flength'];
					}
				}
			}

	        $outdata['datalie'] = [
	          '日期', '开始', '结束', '时长', '节目/广告名称', '类型', '位置'
	        ];
	        $outdata['title'][] = $doMedia['fmedianame'].'节目广告串播单明细报告';
	        $outdata['title'][] = '频道：'.$doMedia['fmedianame'];
	        $timestr = '日期段：'.$issueDate[0].'至'.$issueDate[1];//时间段
	        if(!empty($issueTime)){
	        	$timestr .= '  时间段：'.$issueTime[0].'至'.$issueTime[1];
	        }else{
	        	$timestr .= '  时间段：00:00:00至23:59:59';
	        }
	        $outdata['title'][] = $timestr;
	        $outdata['title'][] = '数量：'.$adCount.'；时长：'.$flengths.'秒';
	        $outdata['title'][] = '制表时间：'.date('Y-m-d H:i:s');
	        $outdata['lists'] = $data;
			$ret = A('Jiance/outReport')->playReport($outdata);
	        // $ret['url'] = A('Jiance/playReport')->playReport($outdata);
	        // $ret = A('Api/Function')->outdata_xls($outdata);

	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count[0]['acount']?$count[0]['acount']:0,'data'=>$data));
		}
	}

	/*
	* by zw
	* 串播单园整
	*/
	public function chuanBoYuanZheng($flength = 0){
		if(empty($flength)){
			return 0;
		}
		$num1 = intval($flength/10)*10;
		$num2 = $flength - $num1;
		switch ($num2) {
			case 4:
				$num2 = 5;
				break;
			case 6:
				$num2 = 5;
				break;
			case 1:
				$num2 = 0;
				break;
			case 9:
				$num2 = 10;
				break;
		}
		return $num1+$num2;
	}

	/*
	* by zw
	* 获取监播单列表
	*/
	public function ppPlayList(){
		ini_set('max_execution_time', 600);//设置超时时间
		session_write_close();
		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $isNotBusiness = $getData['isNotBusiness']?$getData['isNotBusiness']:0;//是否包含非商业类
        $limitIndex = ($pageIndex-1)*$pageSize;
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$issueTime = $getData['issueTime'];//时间范围，数组
		$isCreate = $getData['isCreate'];//重新生成，1生成，0或空不生成
		$adClass = $getData['adClass'];//广告类别
		$brand = $getData['brand'];//品牌
		$adName = $getData['adName'];//广告名
		$outType = $getData['outType'];//导出类型

		if(empty($outType)){
			$limitstr = ' limit '.($pageIndex-1)*$pageSize.','.$pageSize;
		}
		$whereStr1 = '1=1';

		// $mediaId = 11000010002362;
		// $issueDate = ['2019-11-1','2019-11-3'];
		// $issueTime = ['20:00:00','23:00:00'];

		if(empty($mediaId)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请输入需要查询的媒体'));
		}
		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}
		$whereMedia['a.fid'] = $mediaId;
		$whereMedia['a.fstate'] = 1;
		$whereMedia['a.media_region_id'] = ['like','33%'];
		$doMedia = M('tmedia')
			->alias('a')
			->field('a.fmediaclassid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
			->join('tmedialabel b on a.fid = b.fmediaid and b.flabelid in(446,449)')
			->where($whereMedia)
			->find();
		if(empty($doMedia)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'非串播单媒体'));
		}

		$mclass = substr($doMedia['fmediaclassid'],0,2);
		if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'不支持该媒体类型'));
        }

		if(!empty($issueDate)){
			$whereStr1 .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		}
		if(!empty($issueTime)){
			$whereStr1 .= ' and DATE_FORMAT(a.fstarttime,"%H:%i:%s") between "'.$issueTime[0].'" and "'.$issueTime[1].'"';
		}
		if(!empty($mediaId)){
			$whereStr1 .= ' and a.fmediaid = '.$mediaId;
		}
		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}
		if(!empty($codes)){
			$whereStr1 .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}
		if(!empty($brand)){
			$whereStr1 .= ' and c.fbrand like "'.$brand.'"';
		}
		if(!empty($adName)){
			$whereStr1 .= ' and c.fadName like "'.$adName.'"';
		}
		if(empty($isNotBusiness)){
			$whereStr1 .= ' and a.fissuetype <>""';
		}

		// if(!empty($isCreate)){
		// 	$this->createPlayIssueData($mclass,$mediaId,$issueDate);
		// }

		$whereStr1 .= ' and a.fstate = 1 and b.fstate = 1 and a.fsendstate in(10,20)';
		$countSql = '
			select count(*) acount
			from t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' a
			inner join t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)
			inner join tad c on b.fadid = c.fadid and c.fstate in(1,2,9)
			where '.$whereStr1.'
		';
		$count = M()->query($countSql);

		$sqlStr = '
			select a.fmediaid,a.fissuedate,a.fstarttime,a.fendtime,a.flength,c.fadname title,a.fissuetype,a.fadsort,a.fpriorad,a.fnextad,a.fpriorprg,a.fnextprg,DAYOFWEEK(DATE_SUB(a.fissuedate,INTERVAL 1 DAY)) weekday 
			from t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' a
			inner join t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)
			inner join tad c on b.fadid = c.fadid and c.fstate in(1,2,9)
			where '.$whereStr1.'
			order by a.fissuedate asc,a.fstarttime asc
			'.$limitstr.'
		';
		$data = M()->query($sqlStr);
		if(!empty($outType)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
			$flengths = 0;//合计秒数
	        foreach ($data as $key => $value) {
        		$data[$key]['flength'] = $this->chuanBoYuanZheng($value['flength']);
        		$flengths += $data[$key]['flength'];
	        }
			
	        $outdata['datalie'] = [
	          '日期', '星期', '广告内容', '广告前栏目', '广告后栏目', '前广告', '后广告', '规格', '类型', '序号'
	        ];

	        $outdata['title'][] = '浙广资字[  ]号';
	        $outdata['title'][] = '媒体名称：'.$doMedia['fmedianame'];
	        $outdata['title'][] = '广告总数：'.($count[0]['acount']?$count[0]['acount']:0);
	        $outdata['title'][] = '广告秒数合计：'.$flengths.'秒';
	        $outdata['title'][] = $issueTime?'时间段：'.$issueTime[0].'至'.$issueTime[1]:'时间段：00:00:00至23:59:59';//时间段
	        $outdata['title'][] = '日期段：'.$issueDate[0].'至'.$issueDate[1];//日期段
	        $outdata['title'][] = '制表时间：'.date('Y-m-d H:i:s');
	        
	        $outdata['lists'] = $data;

	        // $ret = A('Api/Function')->outdata_xls($outdata);
	        $ret = A('Jiance/outReport')->ppPlayReport($outdata);

	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count[0]['acount']?$count[0]['acount']:0,'data'=>$data));
		}
	}

	/**
	 * 轮循生成串播单数据，按天2s/c
	 * by zw
	 */
	public function loopCheckPlayData(){
		$mediaId = I('mediaId');//媒体ID
		$issueDate = I('issueDate');//日期
		$rt = I('rt');//重置
		if(!empty($rt)){
			S('getplayMedia',null);
			S('playMedia',null);
		}

		if(!empty($mediaId) && !empty($issueDate)){
			$doMedia = M('tmediaissue')
                ->alias('a')
                ->field('b.fmediaclassid')
                ->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
                ->where(['a.fmedia_id'=>$mediaId,'a.fissue_date'=>$issueDate,'a.fstatus'=>1])
                ->find();
            if(!empty($doMedia)){
            	$ret = A('Api/Function')->createPlayIssueData(substr($doMedia['fmediaclassid'], 0,2),$mediaId,[$issueDate,date('Y-m-d',strtotime($issueDate)). '23:59:59']);//重新生成监播单
            	dump($ret);
            }else{
            	echo '失败';
            }
		}else{
			if(!empty(S('playMedia'))){
				$doMedia = S('playMedia');
			}else{
				if(empty(S('getplayMedia'))){
					$doMedia = M('tmediaissue')
		                ->alias('a')
		                ->field('b.fmediaclassid,a.fmedia_id,a.fissue_date')
		                ->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
		                ->join('tmedialabel c on c.fmediaid = b.fid and c.flabelid in(449)')
		                ->where(['a.fstatus'=>1,'a.fissue_date'=>['between',[date('Y-m-01',strtotime("-1 month")),date('Y-m-d')]]])
		                ->select();
		            S('playMedia',$doMedia,7200);
		            S('getplayMedia',1,7200);
				}
			}
			if(!empty($doMedia)){
				A('Api/Function')->createPlayIssueData(substr($doMedia[0]['fmediaclassid'], 0,2),$doMedia[0]['fmedia_id'],[$doMedia[0]['fissue_date'],date('Y-m-d',strtotime($doMedia[0]['fissue_date'])). ' 23:59:59']);//重新生成监播单
				unset($doMedia[0]);
				$doMedia = array_values($doMedia);
				S('playMedia',$doMedia,7200);
			}
			dump($doMedia);
		}
	}

}
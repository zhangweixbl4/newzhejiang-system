<?php
namespace Jiance\Controller;
use Think\Controller;
class SampleManageController extends BaseController {

	/*
	* by zw
	* 获取样本列表
	*/
	public function sampleList(){
		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
		$adName = $getData['adName'];//广告名
		$mediaName = $getData['mediaName'];//媒体名称
		$mediaId = $getData['mediaId'];//媒体Id
		$mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03'）
		$issueDate = $getData['issueDate'];//日期范围，数组
		$adClass = $getData['adClass'];//广告类别
		$illType = $getData['illType'] != ''?$getData['illType']:-1;//违法类型，默认-1 全部
		$area = $getData['area'];//行政区划
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$adClass = $getData['adClass'];//广告类别
		$outType = $getData['outType'];//导出类别
		$orderType = $getData['orderType'];//排序方式
		if(!empty($outType)){
			$pageSize = 50000;
		}

		if($mediaClass == '01'){
			$tbHead = 'tv';
		}elseif($mediaClass == '02'){
			$tbHead = 'bc';
		}elseif($mediaClass == '03' || $mediaClass == '04'){
			$tbHead = 'paper';
			$where_sam['left(e.fmediaclassid,2)'] = $mediaClass;
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'不支持该类型']);
		}

		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}

		if(!empty($codes)){
			$where_sam['b.fadclasscode'] = ['in',$codes];
		}
		if(!empty($adName)){
			$where_sam['b.fadname'] = ['like','%'.$adName.'%'];
		}
		if(!empty($issueDate)){
			$where_sam['a.fissuedate'] = ['between',[$issueDate[0],$issueDate[1]]];
			$yearDate = date('Y',strtotime($issueDate[0]));
		}else{
			$yearDate = date('Y');
		}
		if(!empty($mediaName)){
			$where_sam['e.fmedianame'] = ['like','%'.$mediaName.'%'];
		}
		if(!empty($mediaId)){
			$where_sam['e.fid'] = $mediaId;
		}
		if((int)$illType != -1){
			$where_sam['a.fillegaltypecode'] = $illType;
		}
		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$where_sam['e.media_region_id'] = ['like',substr($area,0,2).'%'];
				}elseif($tregion_len == 4){//市级
					$where_sam['e.media_region_id'] = ['like',substr($area,0,4).'%'];
				}elseif($tregion_len == 6){//县级
					$where_sam['e.media_region_id'] = ['like',substr($area,0,6).'%'];
				}
			}else{
				$where_sam['e.media_region_id'] = $area;
			}
        }

        switch ($orderType) {
        	case 0:
    			$order = 'CONVERT(e.fmedianame using gbk)  asc';
    			break;
    		case 1:
    			$order = 'CONVERT(e.fmedianame using gbk) desc';
    			break;
    		case 10:
    			$order = 'CONVERT(b.fadname using gbk)  asc';
    			break;
    		case 11:
    			$order = 'CONVERT(b.fadname using gbk) desc';
    			break;
    		case 20:
				$order = 'a.fissuedate asc';
    			break;
    		case 21:
				$order = 'a.fissuedate desc';
    			break;
    		case 30:
    			$order = 'b.fadclasscode asc';
    			break;
    		case 31:
    			$order = 'b.fadclasscode desc';
    			break;
    		case 40:
    			$order = 'a.fillegaltypecode asc';
    			break;
    		case 41:
    			$order = 'a.fillegaltypecode desc';
    			break;
    		case 50:
    			$order = 'a.fadlen asc';
    			break;
    		case 51:
    			$order = 'a.fadlen desc';
    			break;
    		default:
				$order = 'a.fissuedate desc';
    			break;
    	}

		$where_sam['a.fstate'] = ['in',[0,1]];
		$count = M('t'.$tbHead.'sample')
			->alias('a')
        	->join('tad b on b.fadid = a.fadid and b.fstate in(1,2,9)')
			->join('tadowner c on c.fid = b.fadowner')
			->join('tadclass d on d.fcode = b.fadclasscode')
			->join('tmedia e on a.fmediaid = e.fid and e.fstate = 1 and e.fid = e.main_media_id')
			->where($where_sam)
			->count();

		$data = M('t'.$tbHead.'sample')
			->alias('a')
			->field('a.*,b.fadname,c.fname as adownername,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,d.ffullname adclassname,left(e.fmediaclassid,2) mediaclass')
        	->join('tad b on b.fadid = a.fadid and b.fstate in(1,2,9)')
			->join('tadowner c on c.fid = b.fadowner')
			->join('tadclass d on d.fcode = b.fadclasscode')
			->join('tmedia e on a.fmediaid = e.fid and e.fstate = 1 and e.fid = e.main_media_id')
			->where($where_sam)
			->order($order)
			->limit($limitIndex,$pageSize)
			->select();

		if(!empty($outType)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
	        $outdata['datalie'] = [
	          '序号'=>'key',
	          '发布媒体'=>'fmedianame',
	          '广告名称'=>'fadname',
	          '发布日期'=>'fissuedate',
	          '广告主'=>'adownername',
	          '广告内容类别'=>'adclassname',
	          '违法类型'=>[
	            'type'=>'zwif',
	            'data'=>[
	              ['{fillegaltypecode} == 0','不违法'],
	              ['{fillegaltypecode} == 30','违法'],
	              ['{fillegaltypecode} == -30','待核实'],
	            ]
	          ],
	          '违法表现代码'=>'fexpressioncodes',
	          '违法表现'=>'fexpressions',
	          '涉嫌违法内容'=>'fillegalcontent',
	          '素材地址'=>[
	            'type'=>'zwif',
	            'data'=>[
	              ['{mediaclass} == "01" || {mediaclass} == "02"','{favifilename}'],
	              ['{mediaclass} == "03" || {mediaclass} == "04"','{fjpgfilename}'],
	            ]
	          ],
	          '时长'=>[
	            'type'=>'zwif',
	            'data'=>[
	              ['{mediaclass} == "01" || {mediaclass} == "02"','{fadlen}'],
	            ]
	          ],
	          '审批号'=>'fapprovalid',
	          '审批单位'=>'fapprovalunit',
	          '代言人'=>'fspokesman',
	          '版本号'=>'fversion',
	        ];

	        $outdata['lists'] = $data;
	        $ret = A('Api/Function')->outdata_xls($outdata);

	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
		}
	}

	/*
	* by zw
	* 获取样本详情
	*/
	public function sampleDetail(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//样本ID
		$mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）

		$fields = 'a.fid,b.fadid,b.fadname,c.fname adownername,b.fadclasscode adclasscode,d.ffullname adclassname,b.fbrand,
			a.fspokesman,a.fversion,a.fapprovalid,a.fapprovalunit,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,a.fmediaid,a.fadlen,a.fillegaltypecode,
			a.fillegalcontent,a.fexpressioncodes,a.fexpressions,a.favifilename,a.fstate,a.fissuedate';
		if($mediaClass == '01'){
			$tbHead = 'tv';
		}elseif($mediaClass == '02'){
			$tbHead = 'bc';
		}elseif($mediaClass == '03' || $mediaClass == '04'){
			$tbHead = 'paper';
			$fields = 'a.fid,b.fadid,b.fadname,c.fname adownername,b.fadclasscode adclasscode,d.ffullname adclassname,b.fbrand,
			a.fspokesman,a.fversion,a.fapprovalid,a.fapprovalunit,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,a.fmediaid,a.fillegaltypecode,
			a.fillegalcontent,a.fexpressioncodes,a.fexpressions,a.fjpgfilename,a.fstate,a.fissuedate';
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'不支持该类型']);
		}

		$sampleDetail = M('t'.$tbHead.'sample')
			->alias('a')
			->field("left(e.fmediaclassid,2) mediaClass,".$fields)
        	->join('tad b on b.fadid = a.fadid and b.fstate in(1,2,9)')
			->join('tadowner c on c.fid = b.fadowner')
			->join('tadclass d on d.fcode = b.fadclasscode')
			->join('tmedia e on a.fmediaid = e.fid and e.fstate = 1 and e.fid = e.main_media_id')
			->where(['a.fid'=>$fid])
			->find();
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$sampleDetail));
	}

    /**
     * 样本媒体发布列表
     * User: Limboy
     * Time: 2020/4/27 13:40
     */
	public function sampleMediaIssueList()
    {
        $getData = $this->oParam;
        $fid = $getData['fid'];//样本ID
        $pageIndex = $getData['pageIndex'] ? $getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize'] ? $getData['pageSize'] : 20;//每页显示数
        $pageOffset = ($pageIndex - 1) * $pageSize;
        $mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）

        if(empty($fid)){
        	$this->ajaxReturn(['code' => 1, 'msg' => '参数有误']);
        }

        if ($mediaClass == '01') {
            $tbHead = 'tv';
        } elseif ($mediaClass == '02') {
            $tbHead = 'bc';
        } elseif ($mediaClass == '03' || $mediaClass == '04') {
            $tbHead = 'paper';
        } else {
            $this->ajaxReturn(['code' => 1, 'msg' => '不支持该类型']);
        }

        // 获取样本信息
        $sampleInfo = M('t'.$tbHead.'sample')
            ->alias('a')
            ->field('a.fid, a.fissuedate')
            ->where(['a.fid' => $fid])
            ->find();
        if (empty($sampleInfo)) {
            $this->ajaxReturn(['code' => 1, 'msg' => '样本不存在']);
        }

        // 获取分表年份时间参数
        $yearArr = [];
        $startYear = date('Y', strtotime($sampleInfo['fissuedate']));
        $currentYear = date('Y');
        if ($currentYear > $startYear) {
            for($startYear; $startYear <= $currentYear; $startYear++) {
                $yearArr[] = $startYear;
            }
        } else {
            $yearArr[] = $currentYear;
        }

        //拼接sql语句
        $tbArr = [];//参与的表
        $fields = 'fsampleid, fmediaid, fstarttime, fendtime, flength';
        $whereStr = " fsampleid = {$fid} ";
        $tbMedia = "t{$tbHead}issue";
        foreach ($yearArr as $key => $year) {
            $tbArr[] = "
            (
                select {$fields}
                from {$tbMedia}_{$year} 
                where {$whereStr}
            )
        ";
        }

        $tableSql =  implode(" union all ", $tbArr);

        // 总记录
        $sqlCountStr = "select count(*) as total from ({$tableSql}) a";
        $countResult = M()->query($sqlCountStr);
        $count = !empty($countResult[0]['total']) ? $countResult[0]['total'] : 0;

        // 分页数据
        $sqlStr = "
			select a.* 
			from ({$tableSql}) a
			order by a.fstarttime desc
			limit {$pageOffset}, {$pageSize} 
		";
        $data = M()->query($sqlStr);

        // 获取列表
        $this->ajaxReturn(['code' => 0, 'msg' => '获取成功', 'count' => $count, 'data' => $data]);
    }

	/*
	* by zw
	* 样本修改
	*/
	public function sampleSave(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//样本ID
		$adName = $getData['adName'];//广告名
		$brand = $getData['brand'];// 广告品牌
		$adClass = $getData['adClass'];//广告分类code
		$adOwnerName = $getData['adOwnerName'];//广告主
		$version = $getData['version'];//版本
		$issueDate = $getData['issueDate'];//发布日期
		$spokesMan = $getData['spokesMan'];//代言人
		$adLen = $getData['adLen']?$getData['adLen']:0;//样本长度
		$illegalTypeCode = $getData['illegalTypeCode'];//违法类型，0不违法，30违法
		$illegalContent = $getData['illegalContent'];//违法内容
		$expressionCodes = $getData['expressionCodes'];//违法代码
		$approvalId = $getData['approvalId'];//审批号
		$approvalUnit = $getData['approvalUnit'];//审批单位
		$mediaId = $getData['mediaId'];//媒体ID
		$state = $getData['state'];//状态
		$aviFileName = $getData['aviFileName'];//上传视频

		$mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）
		if($mediaClass == '01'){
			$tbHead = 'tv';
		}elseif($mediaClass == '02'){
			$tbHead = 'bc';
		}elseif($mediaClass == '03' || $mediaClass == '04'){
			$tbHead = 'paper';
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'不支持该类型']);
		}

		if(M('tadclass')->where(array('fcode'=>$adClass))->count() == 0){
			$this->ajaxReturn( array('code'=>1,'msg'=>'广告类别错误'));
		} 
		if(empty($aviFileName)){
			$this->ajaxReturn( array('code'=>1,'msg'=>'视频素材路径错误'));
		}
		if(empty($issueDate)){
			$this->ajaxReturn( array('code'=>1,'msg'=>'发布日期错误'));
		}
		if(empty($mediaId)){
			$this->ajaxReturn( array('code'=>1,'msg'=>'请选择播出媒介'));
		}
		if(empty($adName)){
			$this->ajaxReturn( array('code'=>1,'msg'=>'请输入广告名称'));
		}
		if($illegalTypeCode > 0 && (empty($expressionCodes) || empty($illegalContent))){
			$this->ajaxReturn( array('code'=>1,'msg'=>'违法信息填写不完整'));
		}

		if(!empty($expressionCodes)){
			$illegalData = D('Common/Function')->sampleillegal($expressionCodes);
		}

		$adId = $this->getAdId($adName,$brand,$adClass,$adOwnerName,$mediaId,session('personData.fname'));//获取广告ID

		$a_e_data['fissuedate'] = $issueDate;//发布日期
		$a_e_data['fadid'] = $adId;//广告ID
		$a_e_data['fversion'] = $version;//版本说明
		$a_e_data['fspokesman'] = $spokesMan;//代言人
		$a_e_data['fadlen'] = $adLen;//样本长度
		$a_e_data['fillegaltypecode'] = $illegalTypeCode;//违法类型
		if($illegalTypeCode <= 0){
			$a_e_data['fillegalcontent'] = $illegalContent;//涉嫌违法内容
			$a_e_data['fexpressioncodes'] = '';//违法表现代码
			$a_e_data['fexpressions'] = '';//违法表现
			$a_e_data['fconfirmations'] = '';//认定依据
		}else{
			$a_e_data['fillegalcontent'] = $illegalContent;//涉嫌违法内容
			$a_e_data['fexpressioncodes'] = $expressionCodes;//违法表现代码
			$a_e_data['fexpressions'] = $illegalData['fexpressions'];//违法表现
			$a_e_data['fconfirmations'] = $illegalData['fconfirmations'];//认定依据
		}
		$a_e_data['fapprovalid'] = $approvalId;//审批号
		$a_e_data['fapprovalunit'] = $approvalUnit;//审批单位
		$a_e_data['fmediaid'] = $mediaId;//播出媒体
		$a_e_data['favifilename'] = $aviFileName;//视频文件路径
		$a_e_data['fstate'] = $state;//状态（-1删除，0无效，1-有效，2抽查）
		$a_e_data['fself'] = 1;//私有
		$a_e_data['fmodifier'] = session('personData.fname');//修改人
		$a_e_data['fmodifytime'] = date('Y-m-d H:i:s');//修改时间
		$rr = M('t'.$tbHead.'sample')->where(array('fid'=>$fid))->save($a_e_data);//修改数据
		if(!empty($rr)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'修改失败,原因未知'));
		}
	}

	 /*获取广告ID*/
    private function getAdId($adName,$brand,$adClassCode,$adOwnerName,$mediaId = 0,$creator = ''){
		$adName = str_replace(array('  '),array(' '),$adName); #替换不合法字符
		$adName = strtoupper(trim($adName));//广告名称
        $brand = strtoupper(trim($brand));//品牌名称
        $adOwnerName = strtoupper(trim($adOwnerName));//广告主
		$doAd = M('tad')->master(true)->where(array('fadname'=>$adName))->find();//通过广告名称查询广告信息

		if(M('tadclass')->cache(true,3600)->where(array('fcode'=>$adClassCode))->count() == 0){
            $adClassCode = '2301';
        }

        //广告主信息
        if(empty($adOwnerName) || $adOwnerName == '广告主不详'){
        	$adOwnerId = 0;
        }else{
        	//获取广告主ID
        	$adOwnerId = M('tadowner')->where(array('fname'=>$adOwnerName))->getField('fid');
        	//获取广告主所在地区
        	if(!empty($mediaId)){
        		$fregionid = M('tmedia')->where(['fid'=>$mediaId,'fstate'=>1])->getField('media_region_id');
        	}
        	//不存在广告主时添加广告主
        	if(empty($adOwnerId)){
        		$addAdOwner['fname'] = $adOwnerName;
        		$addAdOwner['fregionid'] = $fregionid?$fregionid:0;
        		$addAdOwner['fcreator'] = $actionuser?$actionuser:'系统';
        		$addAdOwner['fcreatetime'] = date('Y-m-d H:i:s');
        		$addAdOwner['fidentify'] = 0;
        		$adOwnerId = M('tadowner')->add($addAdOwner);
        	}
        }

		if(!empty($doAd)){
			$doAd['fbrand'] = $brand;
			$doAd['fadclasscode'] = $adClassCode;
			$doAd['fadowner'] = $adOwnerId;
			M('tad')->add($doAd,[],true);//修改广告
			$ad_id = $doAd['fadid'];
		}else{
			$addData = array(
                'fadname'			=> $adName,//广告名称
                'fbrand'			=> $brand,//品牌
                'fadclasscode'		=> $adClassCode,//广告类别代码
                'fadowner'			=> $adOwnerId,//广告主ID
                'fcreator' 			=> $creator,//创建人
                'fcreatetime' 		=> date('Y-m-d H:i:s'),//创建时间
                'fstate'			=> 1,
            );
			$ad_id = M('tad')->add($addData);//添加广告
		}

		return $ad_id;
    }

    /**
	* 验证广告信息是否有更改
	* by zw
	*/
	public function checkAdDetail(){
		$getData = $this->oParam;
		$adName = $getData['adName'];
		$adId = $getData['adId'];
		$brand = $getData['brand'];
		$adClass = $getData['adClass'];
		$adOwnerName = $getData['adOwnerName']?$getData['adOwnerName']:'广告主不详';

		if(!empty($adId)){
			$whereAd['fadid'] = $adId;
		}else{
			$whereAd['fadname'] = $adName;
			$whereAd['fadid'] = ['neq',0];
		}
		$doAd = M('tad')
			->alias('a')
			->field('a.fbrand,a.fadowner,a.fadclasscode,b.ffullname adclassname,c.fname adownername')
			->join('tadclass b on a.fadclasscode = b.fcode')
			->join('tadowner c on c.fid = a.fadowner')
			->where($whereAd)
			->find();
		if(!empty($doAd)){
			$error = [];
			$msgstr = '';
			if($doAd['fbrand'] != $brand){
				$error['brand'] = '品牌不符（原：'.$doAd['fbrand'].'）';
				$msgstr .= '与原品牌不符（原：'.$doAd['fbrand'].'）;';
			}
			if($doAd['adownername'] != $adOwnerName){
				$error['adOwnerName'] = '广告主不符（原：'.$doAd['adownername'].'）';
				$msgstr .= '与原广告主不符（原：'.$doAd['adownername'].'）;';
			}
			if($doAd['fadclasscode'] != $adClass){
				$error['adClass'] = '广告类别不符（原：'.$doAd['adclassname'].'）';
				$msgstr .= '与原广告类别不符（原：'.$doAd['adclassname'].'）;';
			}
			if(!empty($error)){
				$this->ajaxReturn(array('code'=>0,'msg'=>'广告信息与信息库中不匹配，请确认后再操作。'.$msgstr.'如您确认本次操作，所有与该广告信息有关的样本，都将使用该广告信息，您是否还要继续此操作？','data'=>$error));
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}

	/**
	* 样本参照列表
	* by zw
	*/
	public function sampleLookList(){

        $getData        = $this->oParam;//接收参数

        $adName         = empty($getData['adName']) ? $this->ajaxReturn(['code'=>1,'msg'=>'广告名称不能为空']) : $getData['adName'];//广告名称

        $mediaId        = empty($getData['mediaId']) ? $this->ajaxReturn(['code'=>1,'msg'=>'媒体参数有误']) : $getData['mediaId'];//媒体ID

        $issueTimeStart = $getData['issueTimeStart'];//发布时间开始
        $issueTimeEnd   = $getData['issueTimeEnd'];//发布时间结束

        $pageIndex      = $getData['pageIndex'] ? $getData['pageIndex'] : 1;//当前页
        $pageSize       = $getData['pageSize'] ? $getData['pageSize'] : 20;//每页显示数
        $limitIndex     = ($pageIndex-1) * $pageSize;

        $mediaClass = M('tmedia')->cache(true,3600)->where(['fid'=>$mediaId])->getField('fmediaclassid');

        //匹配媒体类别
        switch (substr($mediaClass, 0,2)){
            case '01' :
                $sampleTable = 'ttvsample';
                break;
            case '02' :
                $sampleTable = 'tbcsample';
                break;
            case '03' :
                $sampleTable = 'tpapersample';
                break;
            case '04' :
                $sampleTable = 'tpapersample';
                break;
            default:
                $this->ajaxReturn([
                    'code'=>1,
                    'msg'=>'未知类别！'
                ]);
        }

        $map = [];

        //媒体id
        if(!empty($mediaId)) $map['a.fmediaid'] = $mediaId;

        //只看违法
        if(!empty($getData['isOnlyIll']) && $getData['isOnlyIll'] == 1) $map['a.fillegaltypecode'] = ['GT',0];

        //广告名称
        if(!empty($adName)) $map['b.fadname'] = ['like',$adName];

        //发布时间
        if(!empty($issueTimeStart) && !empty($issueTimeStart)) $map['a.fissuedate'] = ['BETWEEN',[$issueTimeStart,$issueTimeEnd]];

        $map['a.fstate'] = ['in',[0,1]];

        $count = M($sampleTable)
            ->alias('a')
            ->join('tad b on b.fadid = a.fadid and b.fstate in(1,2,9)')
            ->join('tadowner c on c.fid = b.fadowner')
            ->join('tadclass d on d.fcode = b.fadclasscode')
            ->join('tmedia e on a.fmediaid = e.fid and e.fstate = 1 and e.fid = e.main_media_id')
            ->where($map)
            ->count();//总条数

        //列表
        $data = M($sampleTable)
            ->alias('a')
            ->field('a.fid,a.fissuedate,a.fcreatetime,b.fadname,d.ffullname adclassname,a.fillegalcontent,a.fexpressioncodes,fexpressions')
            ->join('tad b on b.fadid = a.fadid and b.fstate in(1,2,9)')
            ->join('tadowner c on c.fid = b.fadowner')
            ->join('tadclass d on d.fcode = b.fadclasscode')
            ->join('tmedia e on a.fmediaid = e.fid and e.fstate = 1 and e.fid = e.main_media_id')
            ->where($map)
            ->order('a.fcreatetime desc')
            // ->limit($limitIndex,$pageSize)
            ->select();
        
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功！','count'=>$count,'data'=>$data));

	}

	/**
	* 推送样本识别
	* by zw
	*/
	public function sampleDetect($pubData = [],$retType = 0){
		if(empty($pubData)){
			return A('Api/Function')->selfAjaxReturn(['code'=>1,'msg'=>'参数有误'],$retType);
		}

		$gourl = 'http://118.31.55.177:8002/fast/add_view_result';//推送识别任务接口
		$res = json_decode(http($gourl,$pubData,'POST',false,10),true);
		if(!empty($res) && empty($res['code'])){
			return A('Api/Function')->selfAjaxReturn(['code'=>0,'msg'=>$res['msg'],'data'=>$res['edit_uuid']],$retType);
		}else{
			return A('Api/Function')->selfAjaxReturn(['code'=>1,'msg'=>$res['msg']],$retType);
		}
	}

	/**
	* 获取样本识别结果
	* by zw
	*/
	public function sampleDetectResult($mediaId = 0,$issueDate = '',$uuid = '',$retType = 0){
		// $uuid = '79bacd88-521e-11ea-8f6c-00163e084d98';
		$getData = $this->oParam;//接收参数
		$mediaId = $mediaId?$mediaId:$getData['mediaId'];
		$issueDate = $issueDate?$issueDate:$getData['issueDate'];
		$uuid = $uuid?$uuid:$getData['uuid'];

		if(empty(C('SAMPLEDETECT'))){//样本识别状态关闭
			$data['haveTask'] = 0;//无识别任务
			$data['haveUpdate'] = 0;//无更新内容
			$data['code'] = 0;
			$data['msg'] = '媒体编号有误';
			return A('Api/Function')->selfAjaxReturn($data,$retType);
		}

		if(!empty($mediaId)){
			$fmediaclassid = M('tmedia')->cache(true,600)->where(['fid'=>$mediaId])->getField('fmediaclassid');
			$mclass = $fmediaclassid?substr($fmediaclassid,0,2):$fmediaclassid;
			if($mclass == '01'){
				$tb_str = 'tv';
			}elseif($mclass == '02'){
				$tb_str = 'bc';
			}else{
				return A('Api/Function')->selfAjaxReturn(['code'=>1,'msg'=>'媒体编号有误'],$retType);
			}
		}

		$whereSam = '1=1';
		if(!empty($mediaId)){
			$whereSam .= ' and fmediaid = '.$mediaId;
		}
		if(!empty($issueDate)){
			$whereSam .= ' and fissuedate = '.$issueDate;
		}
		if(!empty($uuid)){
			$whereSam .= ' and uuid = "'.$uuid.'"';
		}else{
			$whereSam .= ' and fdetectstatus = 1';
		}
		$whereSam .= ' and fstate in(0,1)';
		if(empty($tb_str)){
			$sqlstr = '
				select a.* from (
					select fid,uuid,fissuedate,fmediaid,fdetecttime,"01" mclass from ttvsample where '.$whereSam.'
					union all
					select fid,uuid,fissuedate,fmediaid,fdetecttime,"02" mclass from tbcsample where '.$whereSam.'
				) a
				order by fdetecttime asc
				limit 1
			';
		}else{
			$sqlstr = '
				select a.fid,uuid,fissuedate,fmediaid,fdetecttime,"'.$mclass.'" mclass from t'.$tb_str.'sample a
				where '.$whereSam.'
				order by fdetecttime asc
				limit 1
			';
		}
		$doSample = M()->query($sqlstr);
		if(!empty($doSample)){
			$mclass = $doSample[0]['mclass'];
			if($mclass == '01'){
				$tb_str = 'tv';
			}elseif($mclass == '02'){
				$tb_str = 'bc';
			}
			$data['haveTask'] = 1;//有识别任务

			//获取数据
			$gourl = 'http://118.31.55.177:10023/audio_detect/result';
			$pubData['id'] = $doSample[0]['uuid'];
			$res = json_decode(http($gourl,$pubData,'GET',false,10),true);
			if($res['state'] == 2){
				$data['haveUpdate'] = 1;//有更新内容
				$smData['fdetectstatus'] = 2;//识别状态为成功

				$where_issue['a.fissuedate'] = $doSample[0]['fissuedate'];
				$where_issue['a.fmediaid'] = $doSample[0]['fmediaid'];
				$where_issue['a.fstate'] = 1;
				$issue = M('t'.$tb_str.'issue_'.date('Y',strtotime($doSample[0]['fissuedate'])))
					->alias('a')
					->field('a.fid issueid,a.fissuedate,a.fsampleid,b.fadid,b.fillegaltypecode,b.fillegalcontent,b.fexpressioncodes,b.fapprno,b.fmanuno,b.fspokesman,b.fexpressions,b.fversion,c.fadowner,c.fadname,c.fbrand,c.fadclasscode,d.ffullname fadclassname,e.fname fadownername,a.fself,b.fissuedate sampleissuedate,b.fcreatetime samplecreatetime,a.fendtime,a.flength,b.is_long_ad,a.fstarttime,b.favifilename')
					->join('t'.$tb_str.'sample b on a.fsampleid = b.fid')
					->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
					->join('tadclass d on c.fadclasscode = d.fcode')
					->join('tadowner e on c.fadowner = e.fid')
					->where($where_issue)
					->select();
				$addIssue = [];
				foreach ($res['results'] as $value) {
					if($value[0]<strtotime($doSample[0]['fissuedate']) || $value[0]>(strtotime($doSample[0]['fissuedate'])+86399)){//非当天的不作处理
						continue;
					}
					$isAdd = 1;//是否添加该数据，默认1是
					foreach ($issue as $key2 => $value2) {
						$checkData = timeContain((int)$value[0],(int)$value[1],strtotime($value2['fstarttime']),strtotime($value2['fendtime']));//验证时段是否包含及交叉
						if($checkData['use'] == 2){//包含情况
							if($checkData['maxnum'] == 1){//识别结果大的情况
								if((strtotime($value2['fendtime'])-strtotime($value2['fstarttime']))/($value[1]-$value[0])>0.7){//如果原广告时长占比的70%时放弃识别数据
									$isAdd = 0;
									continue;
								}
							}elseif($checkData['maxnum'] == 2){//原数据大的情况
								if(($value[1]-$value[0])/(strtotime($value2['fendtime'])-strtotime($value2['fstarttime']))>0.7){//如果识别结果占比大于70%时放弃识别数据
									$isAdd = 0;
									continue;
								}
							}
						}elseif($checkData['use'] == 1){//交叉情况
							if($checkData['length']>3){//交叉时长超过3秒，直接放弃
								$isAdd = 0;
								continue;
							}else{
								if($checkData['maxnum'] == 1){//识别结果大的情况
									$value[0] = strtotime($value2['fendtime']);
								}elseif($checkData['maxnum'] == 2){//原数据大的情况
									$value[1] = strtotime($value2['fstarttime']);	
								}else{//丢弃该数据
									$isAdd = 0;
									continue;
								}
							}
						}elseif($checkData['use'] == 3){//相同情况
							$isAdd = 0;
							continue;
						}
					}

					//确认要添加的情况
					if(!empty($isAdd)){
						$addIssue[] = [
							'fsampleid'=>$doSample[0]['fid'],
							'fmediaid'=>$doSample[0]['fmediaid'],
							'fissuedate'=>$doSample[0]['fissuedate'],
							'fstarttime'=>date('Y-m-d H:i:s',$value[0]),
							'fstarttime2'=>date('Y-m-d H:i:s',$value[0]),
							'fendtime'=>date('Y-m-d H:i:s',$value[1]),
							'fendtime2'=>date('Y-m-d H:i:s',$value[1]),
							'flength'=>((int)$value[1]-(int)$value[0]),
							'fcreator'=>'AI_Detect',
							'fstate'=>1,
							'fidentify'=>(int)$value[0],
							'fself'=>1,
							'fsendstate'=>10,
						];
					}
				}

				//添加通过的发布数据
				if(!empty($addIssue)){
					M('t'.$tb_str.'issue_'.date('Y',strtotime($doSample[0]['fissuedate'])))->addAll($addIssue);
				}
			}elseif($res['state'] == 3 || $res['state'] == 4){
				$data['haveUpdate'] = 0;//无更新内容
				$smData['fdetectstatus'] = 3;//识别状态为失败
			}else{
				$data['haveUpdate'] = 0;//无更新内容
			}
			$smData['fdetecttime'] = time();
			M('t'.$tb_str.'sample')->where(['uuid'=>$doSample[0]['uuid'],'fdetectstatus'=>1])->save($smData);

			$data['code'] = 0;
			$data['msg'] = '识别任务推送成功';
			return A('Api/Function')->selfAjaxReturn($data,$retType);
		}else{
			$data['haveTask'] = 0;//无识别任务
			$data['haveUpdate'] = 0;//无更新内容

			$data['code'] = 0;
			$data['msg'] = '未找到样本识别任务';
			return A('Api/Function')->selfAjaxReturn($data,$retType);
		}
		
	}

	/**
	* 提交当前媒体的确认样本
	* by zw
	*/
	public function isTrueNewSample($mediaId = 0,$issueDate = '',$retType = 0){
		if(!empty($mediaId)){
			$fmediaclassid = M('tmedia')->cache(true,600)->where(['fid'=>$mediaId])->getField('fmediaclassid');
			$mclass = $fmediaclassid?substr($fmediaclassid,0,2):$fmediaclassid;
			if($mclass == '01'){
				$tb_str = 'tv';
			}elseif($mclass == '02'){
				$tb_str = 'bc';
			}
		}
		if(empty($tb_str)){
			return A('Api/Function')->selfAjaxReturn(['code'=>1,'msg'=>'媒体编号有误'],$retType);
		}

		$whereSam = '1=1';
		if(!empty($mediaId)){
			$whereSam .= ' and fmediaid = '.$mediaId;
		}
		if(!empty($issueDate)){
			$whereSam .= ' and fissuedate = "'.$issueDate.'"';
		}
		$whereSam .= ' and fstate in(0,1)';
		$whereSam2 = $whereSam.' and fdetectstatus = 1';
		$whereSam .= ' and fdetectstatus = 2';

		$uuids = M('t'.$tb_str.'sample')->where($whereSam)->getField('uuid',true);
		if(!empty($uuids)){
			//推送已确认样片
			$gourl = 'http://118.31.55.177:10023/editor/confirm_samples';
			$pubData['detect_ids'] = $uuids;
			$res = json_decode(http($gourl,json_encode($pubData),'POST',false,10),true);
			if($res['code'] == 200){
				$data['code'] = 0;
				$data['msg'] = '推送成功';
				M()->execute('update t'.$tb_str.'sample set fdetectstatus = 4 where '.$whereSam);//推送成功后将已识别的标记为已确认的样本
				M()->execute('update t'.$tb_str.'sample set fdetectstatus = 3 where '.$whereSam2);//推送成功后将还未得到识别结果的标记为识别失败任务
			}else{
				$data['code'] = 1;
				$data['msg'] = '推送失败';
			}
			return A('Api/Function')->selfAjaxReturn($data,$retType);
		}else{
			$data['code'] = 0;
			$data['msg'] = '推送成功';
		}
		
		return A('Api/Function')->selfAjaxReturn($data,$retType);
	}

}
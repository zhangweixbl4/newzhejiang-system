<?php
namespace Jiance\Controller;
use Think\Controller;

class CoreController extends Controller {

	public function createTaskFlowLog($data) {
		$data['fcreate_userid'] = session('personData.fid');
        $data['fcreate_user'] = session('personData.fname');
        $logId = M('ttask_flow_log')->add($data);
        return $logId;
	}

	/**
	 * 获取浙江所有地区
	 * by zw
	 */
	public function getRegionList($parent = 33){
		$where['fstate'] 	= 1;
		$where['fid'] = array('like',$parent.'%');
		$data = M('tregion')->field('fid,fpid,fname1 as fname,ffullname')->cache(true,60)->where($where)->order('fid asc')->select();//查询地区
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}
}
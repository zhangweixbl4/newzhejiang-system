<?php
namespace Jiance\Controller;
use Think\Controller;
import('Vendor.PHPExcel');

/**
 * 报告导出
 * by zw
 */

class OutReportController extends BaseController{

	/*
	* 串播单导出
	* by zw
	*/
	public function playReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);//默认字体大小
		// $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);//所有单元格（行）默认高度
    	$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(12);//所有单元格（列）默认宽度
    	$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
    	);

        $sheet = $objPHPExcel->setActiveSheetIndex(0);
    	$objPHPExcel->getActiveSheet()->getColumnDimension($cellKey[4])->setWidth(40);//设置列宽度
        
        //合并单元格
        for ($i=0; $i < 5; $i++) { 
        	$sheet->mergeCells($cellKey[0].($i+1).':'.$cellKey[5].($i+1));//合并
        	$sheet->setCellValue($cellKey[0].($i+1),$data['title'][$i]);//赋值
        	if($i == 0){
	        	$sheet->getStyle($cellKey[0].($i+1))->getFont()->setBold(true);//加粗
        	}
        }

        //设置单元格标题
        foreach ($data["datalie"] as $key => $value) {
        	$sheet->setCellValue($cellKey[$key].'6',$value);
        }
        for ($i=0; $i < 7; $i++) { 
        	$sheet->getStyle($cellKey[$i].'6')->getFont()->setBold(true);//加粗
        }

        foreach ($data['lists'] as $key => $value) {
        	if($key == 0){
        		for ($i=0; $i < 7; $i++) { 
		        	$sheet->getStyle($cellKey[$i].'7')->getFont()->setBold(true);//加粗
		        }
        	}
            $sheet->setCellValue($cellKey[0].($key+7),$value['fissuedate']);
            $sheet->setCellValue($cellKey[1].($key+7),$value['sttime']);
            $sheet->setCellValue($cellKey[2].($key+7),$value['edtime']);
            $sheet->setCellValue($cellKey[3].($key+7),$value['flength']);
            $sheet->getStyle($cellKey[3].($key+7))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
            $sheet->setCellValue($cellKey[4].($key+7),$value['title']);
            $sheet->getStyle($cellKey[4].($key+7))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue($cellKey[5].($key+7),$value['fissuetype']);
            $sheet->setCellValue($cellKey[6].($key+7),$value['fadsort']);
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $reportName = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$reportName;
        $objWriter->save($savefile);
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$reportName,$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$reportName;
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
	* 监播单导出
	* by zw
	*/
	public function ppPlayReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
		// $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);//所有单元格（行）默认高度
    	$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
    	// $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
    	);

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //合并居中
        $sheet->mergeCells('A1:J1');//合并
        $sheet->mergeCells('A2:C4');//合并
        $sheet->mergeCells('D2:D4');//合并
        $sheet->mergeCells('E2:F4');//合并
        $sheet->mergeCells('G2:J2');//合并
        $sheet->mergeCells('G3:J3');//合并
        $sheet->mergeCells('G4:J4');//合并
        $sheet->getStyle('A1')->getFont()->setSize(18);//字体大小
        $sheet->getStyle('A2')->getFont()->setSize(12);//字体大小
        $sheet->getStyle('D2')->getFont()->setSize(12);//字体大小
        $sheet->getStyle('E2')->getFont()->setSize(12);//字体大小
        $sheet->getStyle('G2')->getFont()->setSize(12);//字体大小
        $sheet->getStyle('G3')->getFont()->setSize(12);//字体大小
        $sheet->getStyle('G4')->getFont()->setSize(12);//字体大小
        $sheet->getStyle('A1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
        $sheet->getStyle('D2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
        $sheet->getStyle('E2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
        $sheet->getStyle('G2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
        $sheet->getStyle('G3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
        $sheet->getStyle('G4')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $sheet->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $sheet->getStyle('D2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $sheet->getStyle('E2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $sheet->getStyle('G2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $sheet->getStyle('G3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $sheet->getStyle('G4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中

        $sheet->setCellValue('A1',$data['title'][0]);//赋值
        $sheet->setCellValue('A2',$data['title'][1]);//赋值
        $sheet->setCellValue('D2',$data['title'][2]);//赋值
        $sheet->setCellValue('E2',$data['title'][3]);//赋值
        $sheet->setCellValue('G2',$data['title'][4]);//赋值
        $sheet->setCellValue('G3',$data['title'][5]);//赋值
        $sheet->setCellValue('G4',$data['title'][6]);//赋值

        //设置单元格标题
        foreach ($data["datalie"] as $key => $value) {
        	$sheet->setCellValue($cellKey[$key].'5',$value);
        }
        for ($i=0; $i < 10; $i++) { 
        	$sheet->getStyle($cellKey[$i].'5')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);//设置填充颜色
        	$sheet->getStyle($cellKey[$i].'5')->getFill()->getStartColor()->setARGB('CCCCCC');//设置填充颜色
        	$sheet->getStyle($cellKey[$i].'5')->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(24);//所有单元格（列）默认宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);//所有单元格（列）默认宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);//所有单元格（列）默认宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);//所有单元格（列）默认宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);//所有单元格（列）默认宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);//所有单元格（列）默认宽度
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);//所有单元格（列）默认宽度

        foreach ($data['lists'] as $key => $value) {
			$sheet->getStyle($cellKey[0].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[1].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[2].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[3].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[4].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[5].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[6].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[7].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[8].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);
			$sheet->getStyle($cellKey[9].($key+6))->getBorders()->applyFromArray(['allborders'=>['style'=>\PHPExcel_Style_Border::BORDER_THIN,'color'=>['rgb' => '333333']]]);

            $sheet->setCellValue($cellKey[0].($key+6),$value['fstarttime']);
            $sheet->setCellValue($cellKey[1].($key+6),$value['weekday']);
            $sheet->setCellValue($cellKey[2].($key+6),$value['title']);
            $sheet->getStyle($cellKey[2].($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue($cellKey[3].($key+6),$value['fpriorprg']);
            $sheet->getStyle($cellKey[3].($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue($cellKey[4].($key+6),$value['fnextprg']);
            $sheet->getStyle($cellKey[4].($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue($cellKey[5].($key+6),$value['fpriorad']);
            $sheet->getStyle($cellKey[5].($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue($cellKey[6].($key+6),$value['fnextad']);
            $sheet->getStyle($cellKey[6].($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue($cellKey[7].($key+6),$value['flength']);
            $sheet->setCellValue($cellKey[8].($key+6),$value['fissuetype']);
            $sheet->setCellValue($cellKey[9].($key+6),$value['fadsort']);
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $reportName = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$reportName;
        $objWriter->save($savefile);
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$reportName,$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$reportName;
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * 报纸广告费用排名--投放明细
    * by zw
    */
    public function bzTouFangMingXiReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $mediaCount = count($data['mediaIdArr']);
        if(empty($mediaCount)){
            return false;
        }

        switch ($data['dataType']) {
            case 1:
                $fieldname = 'fprices';//费用
                $titlestr2 = '广告投放细目--费用';
                break;
            case 2:
                $fieldname = 'fproportions';//面积
                $titlestr2 = '广告投放细目--面积';
                break;
            case 3:
                $fieldname = 'fquantitys';//次数
                $titlestr2 = '广告投放细目--次数';
                break;
            default:
                return false;
                break;
        }

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
        $objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);//垂直居中
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(14.25);//所有单元格（行）默认高度
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
        // $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //行1
        $sheet->mergeCells('A1:'.$cellKey[$mediaCount+1].'1');//合并
        $sheet->setCellValue('A1','ASC浙江广告监测咨询中心版权所有');//赋值
        $sheet->getStyle('A1')->getFont()->setBold(true);//字体加粗
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14.25);//设置行高

        //行2
        $sheet->mergeCells('A2:'.$cellKey[$mediaCount+1].'2');//合并

        $titlestr1 = $data['regionName']?$data['regionName']:'各';
        $sheet->setCellValue('A2',$titlestr1.'地区主要报纸'.$titlestr2);//赋值

        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居下
        $sheet->getStyle('A2')->getFont()->setSize(16);//字体大小
        $sheet->getStyle('A2')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(36);//设置行高

        //行3
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//设置行高
        $sheet->mergeCells('A3:B3');//合并
        $sheet->setCellValue('A3','商品类别：'.$data['adClassName']);//赋值
        $sheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
        $sheet->getStyle('A3')->getFont()->setBold(true);//字体加粗

        $sheet->setCellValue($cellKey[$mediaCount].'3','时间：'.$data['issueDate'][0].'----'.$data['issueDate'][1]);//赋值
        $sheet->getStyle($cellKey[$mediaCount].'3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $sheet->getStyle($cellKey[$mediaCount].'3')->getFont()->setBold(true);//字体加粗
        
        //行4
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(14.25);//设置行高

        //行5
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);//设置列宽
        $sheet->setCellValue('A5','广告名称');//赋值
        $objPHPExcel->getActiveSheet() ->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(14.25);//设置行高
        foreach ($data['mediaIdArr'] as $key => $value) {
            $sheet->setCellValue($cellKey[$key+1].'5',$data['medianameArr'][$key]);//赋值
            $objPHPExcel->getActiveSheet()->getColumnDimension($cellKey[$key+1])->setWidth(10);//设置列宽
            $objPHPExcel->getActiveSheet() ->getStyle($cellKey[$key+1].'5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线
        }
        $sheet->setCellValue($cellKey[$mediaCount+1].'5','小计');//赋值
        $objPHPExcel->getActiveSheet() ->getStyle($cellKey[$mediaCount+1].'5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $adData = pxsf(array_values($data['adData']),$fieldname,true);//按价格重新排序

        //数据处理
        foreach ($adData as $key => $value) {
            $xiaoji = 0;//初始化小计
            $sheet->getStyle('A'.($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key+6),$value['adname']);//赋值
            foreach ($data['mediaIdArr'] as $key2 => $value2) {
                
                if($fieldname == 'fprices'){
                    $nowVal2 = $value['mediaData'][(string)$value2][$fieldname]?round($value['mediaData'][(string)$value2][$fieldname]/10000,2):0;
                }else{
                    $nowVal2 = $value['mediaData'][(string)$value2][$fieldname]?$value['mediaData'][(string)$value2][$fieldname]:0;
                }
                $sheet->setCellValue($cellKey[$key2+1].($key+6),$nowVal2);//赋值
                $xiaoji += $nowVal2;
                
                $heJiMediaData[(string)$value2] += $nowVal2;//计算每个媒体的合计
            }
            $sheet->setCellValue($cellKey[$mediaCount+1].($key+6),$xiaoji);//小计赋值

            //最后一条数据添加合计记录
            if(count($adData) == ($key+1)){
                $sheet->setCellValue('A'.($key+7),'合计');//赋值
                $sheet->getStyle('A'.($key+7))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
                foreach ($data['mediaIdArr'] as $key2 => $value2) {
                    $nowVal2 = $heJiMediaData[(string)$value2]?$heJiMediaData[(string)$value2]:0;
                    $sheet->setCellValue($cellKey[$key2+1].($key+7),$nowVal2);//赋值

                    $heJiData += $nowVal2;//合计所有数据
                }
                $sheet->setCellValue($cellKey[$mediaCount+1].($key+7),$heJiData);//总计赋值
            }
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$date;
        $reportName = $titlestr1.'地区主要报纸'.$titlestr2.$date;

        $objWriter->save($savefile);
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * 报纸广告费用排名--品牌构成
    * by zw
    */
    public function bzPingPaiGouChengReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
        $objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);//垂直居中
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(14.25);//所有单元格（行）默认高度
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
        // $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //行1
        $sheet->mergeCells('A1:H1');//合并
        $sheet->setCellValue('A1','ASC浙江广告监测咨询中心版权所有');//赋值
        $sheet->getStyle('A1')->getFont()->setBold(true);//字体加粗
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14.25);//设置行高

        //行2
        $sheet->mergeCells('A2:H2');//合并

        $titlestr1 = $data['regionName']?$data['regionName']:'各';
        $sheet->setCellValue('A2',$titlestr1.'地区主要报纸品牌构成');//赋值

        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居下
        $sheet->getStyle('A2')->getFont()->setSize(16);//字体大小
        $sheet->getStyle('A2')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(36);//设置行高

        //行3
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//设置行高
        $sheet->mergeCells('A3:C3');//合并
        $sheet->setCellValue('A3','商品类别：'.$data['adClassName']);//赋值
        $sheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
        $sheet->getStyle('A3')->getFont()->setBold(true);//字体加粗

        $sheet->mergeCells('D3:H3');//合并
        $sheet->setCellValue('D3','时间：'.$data['issueDate'][0].'----'.$data['issueDate'][1]);//赋值
        $sheet->getStyle('D3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $sheet->getStyle('D3')->getFont()->setBold(true);//字体加粗
        
        //行4
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(14.25);//设置行高

        //行5
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(14.25);//设置行高
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(23);//设置列宽
        $sheet->setCellValue('A5','广告名称');//赋值
        $sheet->getStyle('A5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);//设置列宽
        $sheet->setCellValue('B5','费用');//赋值
        $sheet->getStyle('B5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('B5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);//设置列宽
        $sheet->setCellValue('C5','费用比重（%）');//赋值
        $sheet->getStyle('C5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('C5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);//设置列宽
        $sheet->setCellValue('D5','面积');//赋值
        $sheet->getStyle('D5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('D5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);//设置列宽
        $sheet->setCellValue('E5','面积比重（%）');//赋值
        $sheet->getStyle('E5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('E5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);//设置列宽
        $sheet->setCellValue('F5','次数');//赋值
        $sheet->getStyle('F5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('F5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);//设置列宽
        $sheet->setCellValue('G5','次数比重（%）');//赋值
        $sheet->getStyle('G5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('G5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);//设置列宽
        $sheet->setCellValue('H5','广告主');//赋值
        $sheet->getStyle('H5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('H5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $adData = $data['adData'];
        foreach ($adData as $key => $value) {
            $adData[$key]['fpricesBL'] = round($value['fprices']/$data['fprices']*100,2);
            $adData[$key]['fproportionsBL'] = round($value['fproportions']/$data['fproportions']*100,2);
            $adData[$key]['fquantitysBL'] = round($value['fquantitys']/$data['fquantitys']*100,2);
        }
        $adData = pxsf(array_values($adData),'fpricesBL',true);//按价格重新排序

        //数据处理
        foreach ($adData as $key => $value) {
            $sheet->getStyle('A'.($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key+6),$value['adname']);//广告

            $sheet->setCellValue('B'.($key+6),round($value['fprices']/10000,2));//费用

            $sheet->setCellValue('C'.($key+6),$value['fpricesBL']);//费用比

            $sheet->setCellValue('D'.($key+6),$value['fproportions']);//面积

            $sheet->setCellValue('E'.($key+6),$value['fproportionsBL']);//面积比

            $sheet->setCellValue('F'.($key+6),$value['fquantitys']);//次数

            $sheet->setCellValue('G'.($key+6),$value['fquantitysBL']);//次数比
            
            $sheet->setCellValue('H'.($key+6),$value['adownername']);//广告主
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$date;
        $reportName = $titlestr1.'地区主要报纸品牌构成'.$date;

        $objWriter->save($savefile);
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * 报纸监测媒体广告市场情况
    * by zw
    */
    public function bzMeiTiShiChangReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
        $objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);//垂直居中
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(14.25);//所有单元格（行）默认高度
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
        // $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //行1
        $sheet->mergeCells('A1:G1');//合并
        $sheet->setCellValue('A1','ASC浙江广告监测咨询中心版权所有');//赋值
        $sheet->getStyle('A1')->getFont()->setBold(true);//字体加粗
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14.25);//设置行高

        //行2
        $sheet->mergeCells('A2:G2');//合并

        $titlestr1 = $data['regionName']?$data['regionName']:'各地区';
        $sheet->setCellValue('A2',$titlestr1.'报纸监测媒体广告市场情况');//赋值

        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居下
        $sheet->getStyle('A2')->getFont()->setSize(16);//字体大小
        $sheet->getStyle('A2')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(36);//设置行高

        //行3
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//设置行高
        $sheet->mergeCells('A3:C3');//合并
        $sheet->setCellValue('A3','商品类别：'.$data['adClassName']);//赋值
        $sheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
        $sheet->getStyle('A3')->getFont()->setBold(true);//字体加粗

        $sheet->mergeCells('D3:G3');//合并
        $sheet->setCellValue('D3','时间：'.$data['issueDate'][0].'----'.$data['issueDate'][1]);//赋值
        $sheet->getStyle('D3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $sheet->getStyle('D3')->getFont()->setBold(true);//字体加粗
        
        //行4
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(14.25);//设置行高

        //行5
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(14.25);//设置行高
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(23);//设置列宽
        $sheet->setCellValue('A5','媒体名称');//赋值
        $sheet->getStyle('A5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);//设置列宽
        $sheet->setCellValue('B5','费用');//赋值
        $sheet->getStyle('B5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('B5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);//设置列宽
        $sheet->setCellValue('C5','费用比重（%）');//赋值
        $sheet->getStyle('C5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('C5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);//设置列宽
        $sheet->setCellValue('D5','面积');//赋值
        $sheet->getStyle('D5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('D5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);//设置列宽
        $sheet->setCellValue('E5','面积比重（%）');//赋值
        $sheet->getStyle('E5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('E5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);//设置列宽
        $sheet->setCellValue('F5','次数');//赋值
        $sheet->getStyle('F5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('F5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);//设置列宽
        $sheet->setCellValue('G5','次数比重（%）');//赋值
        $sheet->getStyle('G5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('G5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        //数据处理
        $adData = $data['adData'];
        foreach ($adData as $key => $value) {
            $adData[$key]['fpricesBL'] = round($value['fprices']/$data['fprices']*100,2);
            $adData[$key]['fproportionsBL'] = round($value['fproportions']/$data['fproportions']*100,2);
            $adData[$key]['fquantitysBL'] = round($value['fquantitys']/$data['fquantitys']*100,2);
        }
        $adData = pxsf(array_values($adData),'fpricesBL',true);//按价格重新排序

        foreach ($adData as $key => $value) {
            $sheet->getStyle('A'.($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key+6),$value['medianame']);//媒体

            $sheet->setCellValue('B'.($key+6),round($value['fprices']/10000,2));//费用

            $sheet->setCellValue('C'.($key+6),$value['fpricesBL']);//费用比

            $sheet->setCellValue('D'.($key+6),$value['fproportions']);//面积

            $sheet->setCellValue('E'.($key+6),$value['fproportionsBL']);//面积比

            $sheet->setCellValue('F'.($key+6),$value['fquantitys']);//次数

            $sheet->setCellValue('G'.($key+6),$value['fquantitysBL']);//次数比
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$date;
        $reportName = $titlestr1.'报纸监测媒体广告市场情况'.$date;

        $objWriter->save($savefile);
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * 报纸广告收入周时分布
    * by zw
    */
    public function bzZhouShiReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
        $objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);//垂直居中
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(14.25);//所有单元格（行）默认高度
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
        // $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //行1
        $sheet->mergeCells('A1:I1');//合并
        $sheet->setCellValue('A1','ASC浙江广告监测咨询中心版权所有');//赋值
        $sheet->getStyle('A1')->getFont()->setBold(true);//字体加粗
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14.25);//设置行高

        //行2
        $sheet->mergeCells('A2:I2');//合并

        $titlestr1 = $data['regionName']?$data['regionName']:'各地区';
        $sheet->setCellValue('A2',$titlestr1.'报纸广告刊登收入的周时分布');//赋值

        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居下
        $sheet->getStyle('A2')->getFont()->setSize(16);//字体大小
        $sheet->getStyle('A2')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(36);//设置行高

        //行3
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//设置行高
        $sheet->mergeCells('A3:C3');//合并
        $sheet->setCellValue('A3','商品类别：'.$data['adClassName']);//赋值
        $sheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
        $sheet->getStyle('A3')->getFont()->setBold(true);//字体加粗

        $sheet->mergeCells('D3:H3');//合并
        $sheet->setCellValue('D3','时间：'.$data['issueDate'][0].'----'.$data['issueDate'][1]);//赋值
        $sheet->getStyle('D3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $sheet->getStyle('D3')->getFont()->setBold(true);//字体加粗
        
        //行4
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(14.25);//设置行高

        //行5
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(14.25);//设置行高
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(23);//设置列宽
        $sheet->setCellValue('A5','媒体名称');//赋值
        $sheet->getStyle('A5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);//设置列宽
        $sheet->setCellValue('B5','周一');//赋值
        $sheet->getStyle('B5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('B5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);//设置列宽
        $sheet->setCellValue('C5','周二');//赋值
        $sheet->getStyle('C5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('C5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);//设置列宽
        $sheet->setCellValue('D5','周三');//赋值
        $sheet->getStyle('D5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('D5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);//设置列宽
        $sheet->setCellValue('E5','周四');//赋值
        $sheet->getStyle('E5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('E5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);//设置列宽
        $sheet->setCellValue('F5','周五');//赋值
        $sheet->getStyle('F5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('F5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);//设置列宽
        $sheet->setCellValue('G5','周六');//赋值
        $sheet->getStyle('G5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('G5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);//设置列宽
        $sheet->setCellValue('H5','周日');//赋值
        $sheet->getStyle('H5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('H5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);//设置列宽
        $sheet->setCellValue('I5','总计');//赋值
        $sheet->getStyle('I5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('I5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        //数据处理
        $adData = pxsf(array_values($data['adData']),'fprices',true);//按价格重新排序

        foreach ($adData as $key => $value) {
            $sheet->getStyle('A'.($key*2+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key*2+6),$value['medianame']);//媒体
            $sheet->getStyle('A'.($key*2+7))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key*2+7),'占总量百分比%');//赋值

            for ($i=0; $i < 7; $i++) { 
                $nowVal2 = $value['mediaData'][$i]['fprices']?round($value['mediaData'][$i]['fprices']/10000,2):0;
                $sheet->setCellValue($cellKey[$i+1].($key*2+6),$nowVal2);//赋值

                $nowVal3 = $value['fprices']?round($value['mediaData'][$i]['fprices']/$value['fprices']*100,2):0;
                $sheet->setCellValue($cellKey[$i+1].($key*2+7),$nowVal3);//赋值

                //最后一条数据添加合计记录
                if(count($adData) == ($key+1)){
                    $nowVal2 = $data[$i]['fprices']?round($data[$i]['fprices']/10000,2):0;
                    $sheet->setCellValue($cellKey[$i+1].($key*2+8),$nowVal2);//总计赋值

                    $nowVal3 = $data['fprices']?round($data[$i]['fprices']/$data['fprices']*100,2):0;
                    $sheet->setCellValue($cellKey[$i+1].($key*2+9),$nowVal3);//总计赋值
                }
            }
            if(count($adData) == ($key+1)){
                $sheet->setCellValue('A'.($key*2+8),'合计');//赋值
                $sheet->getStyle('A'.($key*2+8))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左

                $sheet->setCellValue('I'.($key*2+8),round($data['fprices']/10000,2));//总计

                $sheet->setCellValue('A'.($key*2+9),'占总量百分比%');//赋值
                $sheet->getStyle('A'.($key*2+9))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            }
            
            $sheet->setCellValue('I'.($key*2+6),round($value['fprices']/10000,2));//总计
        }
        

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$date;
        $reportName = $titlestr1.'报纸广告刊登收入的周时分布'.$date;

        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * 报纸广告量规格分布
    * by zw
    */
    public function bzGuiGeReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $ggCount = count($data['guiGeData']);

        switch ($data['dataType']) {
            case 7:
                $fieldname = 'fprices';//费用
                $titlestr2 = '收入';
                break;
            case 8:
                $fieldname = 'fquantitys';//次数
                $titlestr2 = '次数';
                break;
            case 9:
                $fieldname = 'fproportions';//面积
                $titlestr2 = '面积';
                break;
            default:
                return false;
                break;
        }

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
        $objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);//垂直居中
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(14.25);//所有单元格（行）默认高度
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
        // $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //行1
        $sheet->mergeCells('A1:'.$cellKey[$ggCount+1].'1');//合并
        $sheet->setCellValue('A1','ASC浙江广告监测咨询中心版权所有');//赋值
        $sheet->getStyle('A1')->getFont()->setBold(true);//字体加粗
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14.25);//设置行高

        //行2
        $sheet->mergeCells('A2:'.$cellKey[$ggCount+1].'2');//合并

        $titlestr1 = $data['regionName']?$data['regionName']:'各';
        $sheet->setCellValue('A2',$titlestr1.'地区报纸广告规格的'.$titlestr2.'分布');//赋值

        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居下
        $sheet->getStyle('A2')->getFont()->setSize(16);//字体大小
        $sheet->getStyle('A2')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(36);//设置行高

        //行3
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//设置行高
        $sheet->mergeCells('A3:C3');//合并
        $sheet->setCellValue('A3','商品类别：'.$data['adClassName']);//赋值
        $sheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
        $sheet->getStyle('A3')->getFont()->setBold(true);//字体加粗

        $sheet->mergeCells('D3:'.$cellKey[$ggCount].'3');//合并
        $sheet->setCellValue('D3','时间：'.$data['issueDate'][0].'----'.$data['issueDate'][1]);//赋值
        $sheet->getStyle('D3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $sheet->getStyle('D3')->getFont()->setBold(true);//字体加粗
        
        //行4
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(14.25);//设置行高

        //行5
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(14.25);//设置行高
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(23);//设置列宽
        $sheet->setCellValue('A5','媒体名称');//赋值
        $sheet->getStyle('A5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        foreach ($data['guiGeData'] as $key => $value) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($cellKey[$key+1])->setWidth(15);//设置列宽
            $sheet->setCellValue($cellKey[$key+1].'5',$value);//赋值
            $sheet->getStyle($cellKey[$key+1].'5')->getFont()->setBold(true);//字体加粗
            $objPHPExcel->getActiveSheet() ->getStyle($cellKey[$key+1].'5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线
        }
        
        $objPHPExcel->getActiveSheet()->getColumnDimension($cellKey[$ggCount+1])->setWidth(15);//设置列宽
        $sheet->setCellValue($cellKey[$ggCount+1].'5','合计');//赋值
        $sheet->getStyle($cellKey[$ggCount+1].'5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle($cellKey[$ggCount+1].'5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        //数据处理
        $adData = pxsf(array_values($data['adData']),$fieldname,true);//按价格重新排序

        foreach ($adData as $key => $value) {
            $sheet->getStyle('A'.($key*2+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key*2+6),$value['medianame']);//媒体
            $sheet->getStyle('A'.($key*2+7))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key*2+7),'占总量百分比%');//赋值

            foreach ($data['guiGeData'] as $key2 => $value2) {

                if($fieldname == 'fprices'){
                    $nowVal2 = $value['mediaData'][$value2][$fieldname]?round($value['mediaData'][$value2][$fieldname]/10000,2):0;
                    $sheet->setCellValue($cellKey[$key2+1].($key*2+6),$nowVal2);//赋值
                }else{
                    $nowVal2 = $value['mediaData'][$value2][$fieldname]?$value['mediaData'][$value2][$fieldname]:0;
                    $sheet->setCellValue($cellKey[$key2+1].($key*2+6),$nowVal2);//赋值
                }
                $nowVal3 = $value[$fieldname]?round($value['mediaData'][$value2][$fieldname]/$value[$fieldname]*100,2):0;
                $sheet->setCellValue($cellKey[$key2+1].($key*2+7),$nowVal3);//赋值

                //最后一条数据添加合计记录
                if(count($adData) == ($key+1)){
                    if($fieldname == 'fprices'){
                        $nowVal2 = $data[$value2][$fieldname]?round($data[$value2][$fieldname]/10000,2):0;
                    }else{
                        $nowVal2 = $data[$value2][$fieldname]?$data[$value2][$fieldname]:0;
                    }
                    $sheet->setCellValue($cellKey[$key2+1].($key*2+8),$nowVal2);//总计赋值
                }
            }
            if($fieldname == 'fprices'){
                $sheet->setCellValue($cellKey[$ggCount+1].($key*2+6),round($value[$fieldname]/10000,2));//总计
            }else{
                $sheet->setCellValue($cellKey[$ggCount+1].($key*2+6),$value[$fieldname]);//总计
            }

            //总计
            if(count($adData) == ($key+1)){
                $sheet->setCellValue('A'.($key*2+8),'合计');//赋值
                $sheet->getStyle('A'.($key*2+8))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
                if($fieldname == 'fprices'){
                    $sheet->setCellValue($cellKey[$ggCount+1].($key*2+8),round($data[$fieldname]/10000,2));//总计
                }else{
                    $sheet->setCellValue($cellKey[$ggCount+1].($key*2+8),$data[$fieldname]);//总计
                }
            }
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$date;
        $reportName = $titlestr1.'地区报纸广告规格的'.$titlestr2.'分布'.$date;

        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * 报纸广告套色分布
    * by zw
    */
    public function bzTaoSeReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $taoSeArr = ['黑白','套红','彩色'];
        $tsCount = count($taoSeArr);

        switch ($data['dataType']) {
            case 10:
                $fieldname = 'fprices';//费用
                $titlestr2 = '收入';
                break;
            case 11:
                $fieldname = 'fquantitys';//次数
                $titlestr2 = '次数';
                break;
            case 12:
                $fieldname = 'fproportions';//面积
                $titlestr2 = '面积';
                break;
            default:
                return false;
                break;
        }

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
        $objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);//垂直居中
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(14.25);//所有单元格（行）默认高度
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
        // $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //行1
        $sheet->mergeCells('A1:'.$cellKey[$tsCount+1].'1');//合并
        $sheet->setCellValue('A1','ASC浙江广告监测咨询中心版权所有');//赋值
        $sheet->getStyle('A1')->getFont()->setBold(true);//字体加粗
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14.25);//设置行高

        //行2
        $sheet->mergeCells('A2:'.$cellKey[$tsCount+1].'2');//合并

        $titlestr1 = $data['regionName']?$data['regionName']:'各地区';
        $sheet->setCellValue('A2',$titlestr1.'报纸'.$titlestr2.'套色分布');//赋值

        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居下
        $sheet->getStyle('A2')->getFont()->setSize(16);//字体大小
        $sheet->getStyle('A2')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(36);//设置行高

        //行3
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//设置行高
        $sheet->mergeCells('A3:B3');//合并
        $sheet->setCellValue('A3','商品类别：'.$data['adClassName']);//赋值
        $sheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
        $sheet->getStyle('A3')->getFont()->setBold(true);//字体加粗

        $sheet->mergeCells('C3:'.$cellKey[$tsCount+1].'3');//合并
        $sheet->setCellValue('C3','时间：'.$data['issueDate'][0].'----'.$data['issueDate'][1]);//赋值
        $sheet->getStyle('C3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $sheet->getStyle('C3')->getFont()->setBold(true);//字体加粗
        
        //行4
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(14.25);//设置行高

        //行5
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(14.25);//设置行高
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(29);//设置列宽
        $sheet->setCellValue('A5','媒体名称');//赋值
        $sheet->getStyle('A5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        foreach ($taoSeArr as $key => $value) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($cellKey[$key+1])->setWidth(15);//设置列宽
            $sheet->setCellValue($cellKey[$key+1].'5',$value);//赋值
            $sheet->getStyle($cellKey[$key+1].'5')->getFont()->setBold(true);//字体加粗
            $objPHPExcel->getActiveSheet() ->getStyle($cellKey[$key+1].'5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线
        }
        
        $objPHPExcel->getActiveSheet()->getColumnDimension($cellKey[$tsCount+1])->setWidth(15);//设置列宽
        $sheet->setCellValue($cellKey[$tsCount+1].'5','合计');//赋值
        $sheet->getStyle($cellKey[$tsCount+1].'5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle($cellKey[$tsCount+1].'5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        //数据处理
        $adData = pxsf(array_values($data['adData']),$fieldname,true);//按价格重新排序

        foreach ($adData as $key => $value) {
            $sheet->getStyle('A'.($key*2+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key*2+6),$value['medianame']);//媒体
            $sheet->getStyle('A'.($key*2+7))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key*2+7),'占总量百分比%');//赋值

            foreach ($taoSeArr as $key2 => $value2) {

                if($fieldname == 'fprices'){
                    $nowVal2 = $value['mediaData'][$value2][$fieldname]?round($value['mediaData'][$value2][$fieldname]/10000,2):0;
                    $sheet->setCellValue($cellKey[$key2+1].($key*2+6),$nowVal2);//赋值
                }else{
                    $nowVal2 = $value['mediaData'][$value2][$fieldname]?$value['mediaData'][$value2][$fieldname]:0;
                    $sheet->setCellValue($cellKey[$key2+1].($key*2+6),$nowVal2);//赋值
                }
                $nowVal3 = $value[$fieldname]?round($value['mediaData'][$value2][$fieldname]/$value[$fieldname]*100,2):0;
                $sheet->setCellValue($cellKey[$key2+1].($key*2+7),$nowVal3);//赋值

                //最后一条数据添加合计记录
                if(count($adData) == ($key+1)){
                    if($fieldname == 'fprices'){
                        $nowVal2 = $data[$value2][$fieldname]?round($data[$value2][$fieldname]/10000,2):0;
                    }else{
                        $nowVal2 = $data[$value2][$fieldname]?$data[$value2][$fieldname]:0;
                    }
                    $sheet->setCellValue($cellKey[$key2+1].($key*2+8),$nowVal2);//总计赋值
                }
            }
            if($fieldname == 'fprices'){
                $sheet->setCellValue($cellKey[$tsCount+1].($key*2+6),round($value[$fieldname]/10000,2));//总计
            }else{
                $sheet->setCellValue($cellKey[$tsCount+1].($key*2+6),$value[$fieldname]);//总计
            }

            //总计
            if(count($adData) == ($key+1)){
                $sheet->setCellValue('A'.($key*2+8),'合计');//赋值
                $sheet->getStyle('A'.($key*2+8))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
                if($fieldname == 'fprices'){
                    $sheet->setCellValue($cellKey[$tsCount+1].($key*2+8),round($data[$fieldname]/10000,2));//总计
                }else{
                    $sheet->setCellValue($cellKey[$tsCount+1].($key*2+8),$data[$fieldname]);//总计
                }
            }
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$date;
        $reportName = $titlestr1.'报纸'.$titlestr2.'套色分布'.$date;

        $objWriter->save($savefile);

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * 报纸广告行业分布
    * by zw
    */
    public function bZHangYeReport($data = []) {
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('宋体');//默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);//默认字体大小
        $objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);//垂直居中
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//水平居中
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(14.25);//所有单元格（行）默认高度
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10);//所有单元格（列）默认宽度
        // $objPHPExcel->getDefaultStyle()->getBorders()->getAllBorders()->setBorderstyle(\PHPExcel_Style_Border::BORDER_THIN)->getColor()->setARGB('33330000');
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $cellKey = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        //行1
        $sheet->mergeCells('A1:G1');//合并
        $sheet->setCellValue('A1','ASC浙江广告监测咨询中心版权所有');//赋值
        $sheet->getStyle('A1')->getFont()->setBold(true);//字体加粗
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14.25);//设置行高

        //行2
        $sheet->mergeCells('A2:G2');//合并

        $titlestr1 = $data['regionName']?$data['regionName']:'各地区';
        $sheet->setCellValue('A2',$titlestr1.'报纸刊登广告行业费用面积次数分布');//赋值

        $sheet->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居下
        $sheet->getStyle('A2')->getFont()->setSize(16);//字体大小
        $sheet->getStyle('A2')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(36);//设置行高

        //行3
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(12);//设置行高
        $sheet->mergeCells('A3:C3');//合并
        $sheet->setCellValue('A3','商品类别：'.$data['adClassName']);//赋值
        $sheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
        $sheet->getStyle('A3')->getFont()->setBold(true);//字体加粗

        $sheet->mergeCells('E3:G3');//合并
        $sheet->setCellValue('E3','时间：'.$data['issueDate'][0].'----'.$data['issueDate'][1]);//赋值
        $sheet->getStyle('E3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//水平居右
        $sheet->getStyle('E3')->getFont()->setBold(true);//字体加粗
        
        //行4
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(14.25);//设置行高

        //行5
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(26);//设置列宽
        $sheet->setCellValue('A5','广告类别');//赋值
        $objPHPExcel->getActiveSheet() ->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(14.25);//设置行高
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);//设置列宽
        $sheet->setCellValue('B5','费用');//赋值
        $sheet->getStyle('B5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('B5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);//设置列宽
        $sheet->setCellValue('C5','费用比率（%）');//赋值
        $sheet->getStyle('C5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('C5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);//设置列宽
        $sheet->setCellValue('D5','面积');//赋值
        $sheet->getStyle('D5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('D5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);//设置列宽
        $sheet->setCellValue('E5','面积比重（%）');//赋值
        $sheet->getStyle('E5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('E5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);//设置列宽
        $sheet->setCellValue('F5','次数');//赋值
        $sheet->getStyle('F5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('F5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);//设置列宽
        $sheet->setCellValue('G5','次数比重（%）');//赋值
        $sheet->getStyle('G5')->getFont()->setBold(true);//字体加粗
        $objPHPExcel->getActiveSheet() ->getStyle('G5')->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);//下划线

        $adData = pxsf(array_values($data['adData']),'fprices',true);//按价格重新排序

        //数据处理
        foreach ($adData as $key => $value) {
            $sheet->getStyle('A'.($key+6))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
            $sheet->setCellValue('A'.($key+6),$value['fadclassname']);//赋值

            $nowVal2 = $value['fprices']?round($value['fprices']/10000,2):0;
            $sheet->setCellValue('B'.($key+6),$nowVal2);//赋值

            $nowVal2 = $data['fprices']?round($value['fprices']/$data['fprices']*100,2):0;
            $sheet->setCellValue('C'.($key+6),$nowVal2);//赋值

            $nowVal2 = $value['fproportions']?$value['fproportions']:0;
            $sheet->setCellValue('D'.($key+6),$nowVal2);//赋值

            $nowVal2 = $value['fproportions']?round($value['fproportions']/$data['fproportions']*100,2):0;
            $sheet->setCellValue('E'.($key+6),$nowVal2);//赋值

            $nowVal2 = $value['fquantitys']?$value['fquantitys']:0;
            $sheet->setCellValue('F'.($key+6),$nowVal2);//赋值

            $nowVal2 = $value['fquantitys']?round($value['fquantitys']/$data['fquantitys']*100,2):0;
            $sheet->setCellValue('G'.($key+6),$nowVal2);//赋值
            
            //最后一条数据添加合计记录
            if(count($adData) == ($key+1)){
                $sheet->setCellValue('A'.($key+7),'合计');//赋值
                $sheet->getStyle('A'.($key+7))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//水平居左
                
                $nowVal2 = $data['fprices']?round($data['fprices']/10000,2):0;
                $sheet->setCellValue('B'.($key+7),$nowVal2);//赋值

                $sheet->setCellValue('C'.($key+7),'100');//赋值

                $nowVal2 = $data['fproportions']?$data['fproportions']:0;
                $sheet->setCellValue('D'.($key+7),$nowVal2);//赋值

                $sheet->setCellValue('E'.($key+7),'100');//赋值

                $nowVal2 = $data['fquantitys']?$data['fquantitys']:0;
                $sheet->setCellValue('F'.($key+7),$nowVal2);//赋值

                $sheet->setCellValue('G'.($key+7),'100');//赋值
            }
        }

        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis').rand(1000,9999).'.xlsx';
        $savefile = './Public/Word/'.$date;
        $reportName = $titlestr1.'报纸刊登广告行业费用面积次数分布'.$date;

        $objWriter->save($savefile);
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * by zw
    * 电视广告播出记录单
    */
    public function dSBoChuReport($data = []){
        session_write_close();

        //读取模板
        $file = './Public/Word/dSBoChuReport.xml';
        $xml = file_get_contents($file);

        $xmlData['xmlStr'] = '';//xml最终字串
        $xmlData['xmlChildStr'] = '';//xml子字串
        $xmlData['nowPage'] = 0;//栏
        $xmlData['nowRow'] = 1;//行
        foreach ($data['adData'] as $key => $value) {
            $value['fadname'] = str_replace('&', '&amp;', $value['fadname']);

            //第一行加上行头
            if($xmlData['nowRow'] == 1){
                $xmlData['xmlChildStr'] = $this->dSBoChuReportRow1($value);
                $xmlData['nowRow'] += 1;
                $xmlData['isHead'] = 1;
            }

            //广告行
            if($value['adType'] == 1){
                $xmlData['xmlChildStr'] .= $this->dSBoChuReportRow2($value);
                $xmlData['nowRow'] += 1;
                $xmlData = $this->dSBoChuReportCheckRow($xmlData);
            }

            //节目行
            if($value['adType'] == 2){
                //行1
                $xmlData['xmlChildStr'] .= $this->dSBoChuReportRow3($value);
                $xmlData['nowRow'] += 1;
                $xmlData = $this->dSBoChuReportCheckRow($xmlData);

                if(empty($xmlData['isHead'])){//如果上一行是末尾行的情况下需要新建头
                    $xmlData['xmlChildStr'] = $this->dSBoChuReportRow1($value);
                    $xmlData['nowRow'] += 1;
                    $xmlData['isHead'] = 1;
                }

                //行2
                $xmlData['xmlChildStr'] .= $this->dSBoChuReportRow4($value);
                $xmlData['nowRow'] += 1;
                $xmlData = $this->dSBoChuReportCheckRow($xmlData);
            }

            if(!empty($data['adData'][$key+1])){//判断下一记录日期和当前记录日期是否不同，不同时换栏
                if(date('Y-m-d',strtotime($value['fstart'])) <> date('Y-m-d',strtotime($data['adData'][$key+1]['fstart']))){
                    $xmlData = $this->dSBoChuReportCheckRow($xmlData,1);
                }
            }else{//最后一条记录
                $xmlData = $this->dSBoChuReportCheckRow($xmlData,1);
            }
        }

        $xml = str_replace('{{xml}}',$xmlData['xmlStr'], $xml);//替换文档内容

        $date = date('Ymdhis').rand(1000,9999).'.doc';
        $savefile = './Public/Word/'.$date;
        file_put_contents($savefile,$xml);

        $reportName = '广告播出记录单'.$date;

        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date,$savefile,array('Content-Disposition' => 'attachment;filename='.$reportName));//上传云
        $ret['reportName'] = $reportName;

        //无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/Public/Word/'.$date.'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

        return $ret;
    }

    /*
    * by zw
    * 验证xml文档是否需要换栏
    */
    private function dSBoChuReportCheckRow($xmlData,$isEnd = 0){
        if(($xmlData['nowRow'] > 37 || !empty($isEnd)) && !empty($xmlData['xmlChildStr'])){//大于37行分栏或结尾行
            $xmlData['xmlStr'] .= $this->dSBoChuReportBody($xmlData['xmlChildStr'],$xmlData['nowPage']);
            $xmlData['xmlChildStr'] = '';
            $xmlData['nowPage'] += 1;
            $xmlData['nowRow'] = 1;
            $xmlData['isHead'] = 0;
        }
        return $xmlData;
    }

    /*
    * by zw
    * xml文档body
    */
    private function dSBoChuReportBody($xml = '',$nowPage = 1){
        if(!empty($xml)){
            if(!empty($nowPage)){
                $brStr = '<w:r>
                    <w:br w:type="column"/>
                </w:r>';
            }
            $str = '
                <w:p w:rsidR="001855D5" w:rsidRPr="00EE3EAE" w:rsidRDefault="00154018" w:rsidP="001907D7">
                    '.$brStr.'
                    <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                    <w:bookmarkEnd w:id="0"/>
                </w:p>
                <w:tbl>
                    <w:tblPr>
                        <w:tblStyle w:val="a3"/>
                        <w:tblW w:w="5000" w:type="pct"/>
                        <w:tblInd w:w="3" w:type="dxa"/>
                        <w:tblLook w:val="04A0"/>
                    </w:tblPr>
                    <w:tblGrid>
                        <w:gridCol w:w="901"/>
                        <w:gridCol w:w="999"/>
                        <w:gridCol w:w="2267"/>
                        <w:gridCol w:w="474"/>
                        <w:gridCol w:w="493"/>
                    </w:tblGrid>'
                    .$xml
                .'</w:tbl>'
                ;
        }
        return $str;
    }

    /*
    * by zw
    * xml文档行1
    */

    private function dSBoChuReportRow1($data = ''){
        if(!empty($data)){
            $str = '<w:tr w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidTr="0018008A">
                <w:trPr>
                    <w:trHeight w:hRule="exact" w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="5000" w:type="pct"/>
                        <w:gridSpan w:val="5"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:br w:type="column"/>
                        </w:r>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>播出日期：</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>'.date('Y-m-d',strtotime($data['fstart'])).'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>';
        }
        return $str;
    }

    /*
    * by zw
    * xml广告行
    */
    private function dSBoChuReportRow2($data = ''){
        if(!empty($data)){
            $str = '<w:tr w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidTr="0018008A">
                <w:trPr>
                    <w:trHeight w:hRule="exact" w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="877" w:type="pct"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>'.$data['fadsort'].'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="973" w:type="pct"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>'.date('H:i:s',strtotime($data['fstart'])).'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2208" w:type="pct"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>'.$data['fadname'].'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="462" w:type="pct"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>'.$data['flength'].'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="480" w:type="pct"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                                <w:szCs w:val="18"/>
                            </w:rPr>
                            <w:t>'.$data['fissuetype'].'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>';
        }
        return $str;
    }

    /*
    * by zw
    * xml节目行1
    */
    private function dSBoChuReportRow3($data = ''){
        if(!empty($data)){
            $str = '<w:tr w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidTr="0018008A">
                <w:trPr>
                    <w:trHeight w:hRule="exact" w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="5000" w:type="pct"/>
                        <w:gridSpan w:val="5"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>'.$data['fadname'].'  '.date('H:i',strtotime($data['fstart'])).'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>';
        }
        return $str;
    }

    /*
    * by zw
    * xml节目行2
    */
    private function dSBoChuReportRow4($data = ''){
        if(!empty($data)){
            $str = '<w:tr w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidTr="0018008A">
                <w:trPr>
                    <w:trHeight w:hRule="exact" w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="5000" w:type="pct"/>
                        <w:gridSpan w:val="5"/>
                        <w:tcMar>
                            <w:left w:w="57" w:type="dxa"/>
                            <w:right w:w="57" w:type="dxa"/>
                        </w:tcMar>
                    </w:tcPr>
                    <w:p w:rsidR="001855D5" w:rsidRPr="00417812" w:rsidRDefault="001855D5" w:rsidP="0018008A">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00417812">
                            <w:rPr>
                                <w:rFonts w:asciiTheme="minorEastAsia" w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>'.$data['fadname'].'  '.date('H:i',strtotime($data['fend'])).'</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>';
        }
        return $str;
    }

}
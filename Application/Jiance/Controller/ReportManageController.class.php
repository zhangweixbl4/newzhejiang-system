<?php
namespace Jiance\Controller;
use Think\Controller;
class ReportManageController extends BaseController {

	/*
	* by zw
	* 报表列表
	*/
	public function reportList(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");

        $system_num = getconfig('system_num');

        $p                  = I('page', 1);//当前第几页
        $pp                 = 20;//每页显示多少记录
        $pnname             = I('pnname');// 报告名称
        $pntype             = I('pntype');// 报告类型
        $pnendtime          = I('pnendtime');//报告时间

        if (!empty($pnname)) {
            $where['pnname'] = array('like', '%' . $pnname . '%');//报告名称
        }

        if (!empty($pntype)) {
            $where['pntype'] = $pntype;//报告类型
        }

        if(!empty($pnendtime)){
            $where['pncreatetime'] = array('between',array($pnendtime[0],$pnendtime[1].' 23:59:59'));
        }   

        $where['fcustomer']  = $system_num;

        $count = M('tpresentation')
            ->where($where)
            ->count();
        
        $data = M('tpresentation')
            ->where($where)
            ->order('pncreatetime desc')
            ->page($p,$pp)
            ->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
	}

	/*
	* by zw
	* 报表列表
	*/
	public function reportDel(){
		session_write_close();
		$getData = $this->oParam;
		$pnid = $getData['pnid'];//报表ID

        $where['pnid']  = $pnid;
        
        $delPN = M('tpresentation')
            ->where($where)
            ->delete();

        if(!empty($delPN)){
	        $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
        }else{
        	$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
        }
	}

	/*
	* by zw
	* 报表生成
	*/
	public function bZReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');

		$getData = $this->oParam;
		$dataType = $getData['dataType'];//报表类别，1报纸广告投放明细（费用），2报纸广告投放明细（面积），3报纸广告投放明细（条次），4报纸品牌构成，5报纸监测媒体广告市场情况，6报纸广告收入周时分布，7报纸广告规格分布（收入），8报纸广告规格分布（次数），9报纸广告规格分布（面积），10报纸广告套色分布（收入），11报纸广告套色分布（次数），12报纸广告套色分布（面积）,13报纸广告行业分布，101电视广告播出记录单

		if(in_array($dataType, [1,2,3,4])){//报纸广告投放明细
			$this->bZShiChangReport();
		}elseif(in_array($dataType, [5])){//报纸监测媒体广告市场情况
			$this->bzMeiTiShiChangReport();
		}elseif(in_array($dataType, [6])){//报纸广告收入周时分布
			$this->bzZhouShiReport();
		}elseif(in_array($dataType, [7,8,9])){//报纸广告规格分布
			$this->bzGuiGeReport();
		}elseif(in_array($dataType, [10,11,12])){//报纸广告套色分布
			$this->bzTaoSeReport();
		}elseif(in_array($dataType, [13])){//报纸广告行业分布
			$this->bzHangYeReport();
		}elseif(in_array($dataType, [101])){//电视广告播出记录单
			$this->dSBoChuReport();
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
		}

	}

	/*
	* by zw
	* 报纸市场篇生成
	*/
	public function bZShiChangReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');

		$FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

		$getData = $this->oParam;

		// //测试参数
		// $getData['issueDate'] = ['2019-11-1','2019-11-30'];
		// $getData['area'] = 330000;
		// $getData['iscontain'] = 0;
		// $getData['dataType'] = 4;

		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$adClass = $getData['adClass'];//广告类别
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$dataType = $getData['dataType'];//报表类别，1投放明细--费用，2投放明细--面积，3投放明细--条次，4品牌构成

		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}

		$wherestr = '1=1 ';
		$wherestr .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		$wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';

		//媒体ID
		if (!empty($mediaId)) {
			$wherestr .= ' and a.fmediaid = '.$mediaId;
		}

		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$wherestr .= ' and f.media_region_id = '.$area;
			}
        }

		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}
		if(!empty($codes)){
			$wherestr .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}

		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 and left(f.fmediaclassid,2) = "03"
		';

		$datasql = '
		select * from (
			select c.fadid,c.fadname,a.fmediaid,f.fmedianame,d.fname fadownername,sum(fproportion) fproportions,sum(fprice) fprices,sum(fquantity) fquantitys 
    		from tpaperissue_'.date('Y',strtotime($issueDate[0])).' a 
			inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1) 
			'.$joins.' 
			where '.$wherestr.'
			group by c.fadid,a.fmediaid
			ORDER BY CONVERT(c.fadname using gbk)  asc
		) a';
		$data = M()->query($datasql);

		$mediaIdArr = [];//媒体id组
		$medianameArr = [];//媒体组
		foreach ($data as $key => $value) {
			if(!in_array($value['fmediaid'], $mediaIdArr)){
				$mediaIdArr[] = $value['fmediaid'];
				$medianameArr[] = $value['fmedianame'];
			}

			$adData[(string)$value['fadid']]['fadid'] = $value['fadid'];//广告id
			$adData[(string)$value['fadid']]['adname'] = $value['fadname'];//广告名
			$adData[(string)$value['fadid']]['adownername'] = $value['fadownername'];//广告主
			$adData[(string)$value['fadid']]['fprices'] += $value['fprices'];//广告总价格
			$adData[(string)$value['fadid']]['fproportions'] += $value['fproportions'];//广告总面积
			$adData[(string)$value['fadid']]['fquantitys'] += $value['fquantitys'];//广告总条次

			$outData['fprices'] += $value['fprices'];//总价格
			$outData['fproportions'] += $value['fproportions'];//总面积
			$outData['fquantitys'] += $value['fquantitys'];//总条次

			$adData[(string)$value['fadid']]['mediaData'][(string)$value['fmediaid']]['fprices'] = $value['fprices'];//媒体各广告总价格
			$adData[(string)$value['fadid']]['mediaData'][(string)$value['fmediaid']]['fproportions'] = $value['fproportions'];//媒体各广告总面积
			$adData[(string)$value['fadid']]['mediaData'][(string)$value['fmediaid']]['fquantitys'] = $value['fquantitys'];//媒体各广告总条次
		}

        //广告类别
        if(!empty($adClass)){
            $adClassName = M('tadclass')->where(['fcode'=>$adClass])->getField('fadclass');
        }else{
            $adClassName = '全部';
        }

        //所属地区
        if(!empty($area)){
            $regionName = M('tregion')->where(['fid'=>$area])->getField('fname1');
        }

		$outData['dataType'] = $dataType;
		$outData['issueDate'] = $issueDate;
		$outData['adClassName'] = $adClassName;
		$outData['regionName'] = $regionName;
		$outData['adData'] = $adData;//统计数据
		$outData['mediaIdArr'] = $mediaIdArr;//媒体ID组
		$outData['medianameArr'] = $medianameArr;//媒体名称组

		if(in_array($dataType, [1,2,3])){
			$ret = A('Jiance/outReport')->bzTouFangMingXiReport($outData);
		}else{
			$ret = A('Jiance/outReport')->bzPingPaiGouChengReport($outData);
		}
		
		if(!empty($ret)){
			//将生成记录保存
            $data['pnname']             = $ret['reportName'];
            $data['pntype']             = $dataType;
            $data['pnstarttime']        = $issueDate[0];
            $data['pncreatetime']       = $issueDate[1];
            $data['pnurl']              = $ret['url'];
            $data['pnlog']          = json_encode($outData);
            $data['pncreatepersonid']   = session('personData.fid');
            $data['fcustomer']  = $FORCE_NUM_CONFIG_JIANCE;
            $do_tn = M('tpresentation')->add($data);
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
		}
	}

	/*
	* by zw
	* 报纸监测媒体广告市场情况
	*/
	public function bzMeiTiShiChangReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');

		$FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

		$getData = $this->oParam;

		// //测试参数
		// $getData['issueDate'] = ['2019-11-1','2019-12-30'];
		// $getData['area'] = 330000;
		// $getData['iscontain'] = 0;
		// $getData['dataType'] = 5;

		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$adClass = $getData['adClass'];//广告类别
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$dataType = $getData['dataType'];//报表类别，5监测媒体广告市场情况

		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}

		$wherestr = '1=1 ';
		$wherestr2 = '1=1 ';
		$wherestr .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		$wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';

		//媒体ID
		if (!empty($mediaId)) {
			$wherestr .= ' and a.fmediaid = '.$mediaId;
			$wherestr2 .= ' and a.fid = '.$mediaId;
		}

		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$wherestr .= ' and f.media_region_id = '.$area;
				$wherestr2 .= ' and a.media_region_id = '.$area;
			}
        }

		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}
		if(!empty($codes)){
			$wherestr .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}

		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 and left(f.fmediaclassid,2) = "03"
		';

		$datasql = '
		select * from (
			select a.fmediaid,f.fmedianame,sum(fproportion) fproportions,sum(fprice) fprices,sum(fquantity) fquantitys 
    		from tpaperissue_'.date('Y',strtotime($issueDate[0])).' a 
			inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1) 
			'.$joins.' 
			where '.$wherestr.'
			group by a.fmediaid
		) a';
		$data = M()->query($datasql);

		foreach ($data as $key => $value) {
			$adData[(string)$value['fmediaid']]['fmediaid'] = $value['fmediaid'];//广告id
			$adData[(string)$value['fmediaid']]['medianame'] = $value['fmedianame'];//广告名
			$adData[(string)$value['fmediaid']]['fprices'] += $value['fprices'];//广告总价格
			$adData[(string)$value['fmediaid']]['fproportions'] += $value['fproportions'];//广告总面积
			$adData[(string)$value['fmediaid']]['fquantitys'] += $value['fquantitys'];//广告总条次

			$outData['fprices'] += $value['fprices'];//总价格
			$outData['fproportions'] += $value['fproportions'];//总面积
			$outData['fquantitys'] += $value['fquantitys'];//总条次
		}

		$adData2 = [];
		$medias = M('tmedia a')->field('fmedianame,fid')->where($wherestr2)->order('media_region_id asc,fid asc')->select();
		foreach ($medias as $key => $value) {
			if(!empty($adData[(string)$value['fid']])){
				$adData2[] = $adData[(string)$value['fid']];
			}else{
				$adData2[] = [
					'fmediaid'=>$value['fid'],
					'medianame'=>$value['fmedianame'],
					'fprices'=>0,
					'fproportions'=>0,
					'fquantitys'=>0,
					'mediaData'=>[],
				];
			}
		}

        //广告类别
        if(!empty($adClass)){
            $adClassName = M('tadclass')->where(['fcode'=>$adClass])->getField('fadclass');
        }else{
            $adClassName = '全部';
        }

        //所属地区
        if(!empty($area)){
            $regionName = M('tregion')->where(['fid'=>$area])->getField('fname1');
        }

		$outData['dataType'] = $dataType;
		$outData['issueDate'] = $issueDate;
		$outData['adClassName'] = $adClassName;
		$outData['regionName'] = $regionName;
		$outData['adData'] = $adData2;//统计数据

		$ret = A('Jiance/outReport')->bzMeiTiShiChangReport($outData);
		
		if(!empty($ret)){
			//将生成记录保存
            $data['pnname']             = $ret['reportName'];
            $data['pntype']             = $dataType;
            $data['pnstarttime']        = $issueDate[0];
            $data['pncreatetime']       = $issueDate[1];
            $data['pnurl']              = $ret['url'];
            $data['pnlog']          = json_encode($outData);
            $data['pncreatepersonid']   = session('personData.fid');
            $data['fcustomer']  = $FORCE_NUM_CONFIG_JIANCE;
            $do_tn = M('tpresentation')->add($data);
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
		}
	}

	/*
	* by zw
	* 报纸收入周时分布生成
	*/
	public function bzZhouShiReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');

		$FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

		$getData = $this->oParam;

		//测试参数
		$getData['issueDate'] = ['2019-11-1','2019-12-30'];
		$getData['area'] = 330000;
		$getData['iscontain'] = 0;
		$getData['dataType'] = 6;

		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$adClass = $getData['adClass'];//广告类别
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$dataType = $getData['dataType'];//报表类别，6报纸广告收入周时分布

		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}

		$wherestr = '1=1 ';
		$wherestr2 = '1=1 ';
		$wherestr .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		$wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';

		//媒体ID
		if (!empty($mediaId)) {
			$wherestr .= ' and a.fmediaid = '.$mediaId;
			$wherestr2 .= ' and a.fid = '.$mediaId;
		}

		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$wherestr .= ' and f.media_region_id = '.$area;
				$wherestr2 .= ' and a.media_region_id = '.$area;
			}
        }

		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}
		if(!empty($codes)){
			$wherestr .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}

		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 and left(f.fmediaclassid,2) = "03"
		';

		$datasql = '
		select * from (
			select a.fmediaid,f.fmedianame,WEEKDAY(a.fissuedate) weekkey,sum(fproportion) fproportions,sum(fprice) fprices,sum(fquantity) fquantitys 
    		from tpaperissue_'.date('Y',strtotime($issueDate[0])).' a 
			inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1) 
			'.$joins.' 
			where '.$wherestr.'
			group by a.fmediaid,WEEKDAY(a.fissuedate)
		) a';
		$data = M()->query($datasql);

		foreach ($data as $key => $value) {
			$adData[(string)$value['fmediaid']]['fmediaid'] = $value['fmediaid'];//广告id
			$adData[(string)$value['fmediaid']]['medianame'] = $value['fmedianame'];//广告名
			$adData[(string)$value['fmediaid']]['fprices'] += $value['fprices'];//媒体总价格
			$adData[(string)$value['fmediaid']]['fproportions'] += $value['fproportions'];//媒体总面积
			$adData[(string)$value['fmediaid']]['fquantitys'] += $value['fquantitys'];//媒体总条次

			$adData[(string)$value['fmediaid']]['mediaData'][$value['weekkey']]['fprices'] = $value['fprices'];//媒体星期总价格
			$adData[(string)$value['fmediaid']]['mediaData'][$value['weekkey']]['fproportions'] = $value['fproportions'];//媒体星期总面积
			$adData[(string)$value['fmediaid']]['mediaData'][$value['weekkey']]['fquantitys'] = $value['fquantitys'];//媒体星期总条次

			$outData[$value['weekkey']]['fprices'] += $value['fprices'];//广告总价格
			$outData[$value['weekkey']]['fproportions'] += $value['fproportions'];//广告总面积
			$outData[$value['weekkey']]['fquantitys'] += $value['fquantitys'];//广告总条次

			$outData['fprices'] += $value['fprices'];//总价格
			$outData['fproportions'] += $value['fproportions'];//总面积
			$outData['fquantitys'] += $value['fquantitys'];//总条次
		}

		$adData2 = [];
		$medias = M('tmedia a')->field('fmedianame,fid')->where($wherestr2)->order('media_region_id asc,fid asc')->select();
		foreach ($medias as $key => $value) {
			if(!empty($adData[(string)$value['fid']])){
				$adData2[] = $adData[(string)$value['fid']];
			}else{
				$adData2[] = [
					'fmediaid'=>$value['fid'],
					'medianame'=>$value['fmedianame'],
					'fprices'=>0,
					'fproportions'=>0,
					'fquantitys'=>0,
					'mediaData'=>[],
				];
			}
		}

        //广告类别
        if(!empty($adClass)){
            $adClassName = M('tadclass')->where(['fcode'=>$adClass])->getField('fadclass');
        }else{
            $adClassName = '全部';
        }

        //所属地区
        if(!empty($area)){
            $regionName = M('tregion')->where(['fid'=>$area])->getField('fname1');
        }

		$outData['dataType'] = $dataType;
		$outData['issueDate'] = $issueDate;
		$outData['adClassName'] = $adClassName;
		$outData['regionName'] = $regionName;
		$outData['adData'] = $adData2;//统计数据

		$ret = A('Jiance/outReport')->bzZhouShiReport($outData);
		
		if(!empty($ret)){
			//将生成记录保存
            $data['pnname']             = $ret['reportName'];
            $data['pntype']             = $dataType;
            $data['pnstarttime']        = $issueDate[0];
            $data['pncreatetime']       = $issueDate[1];
            $data['pnurl']              = $ret['url'];
            $data['pnlog']          = json_encode($outData);
            $data['pncreatepersonid']   = session('personData.fid');
            $data['fcustomer']  = $FORCE_NUM_CONFIG_JIANCE;
            $do_tn = M('tpresentation')->add($data);
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
		}
	}

	/*
	* by zw
	* 报纸广告量规格分布生成
	*/
	public function bzGuiGeReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');

		$FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

		$getData = $this->oParam;

		//测试参数
		// $getData['issueDate'] = ['2019-11-1','2019-12-30'];
		// $getData['area'] = 330000;
		// $getData['iscontain'] = 0;
		// $getData['dataType'] = 8;

		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$adClass = $getData['adClass'];//广告类别
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$dataType = $getData['dataType'];//报表类别，7广告规格的收入分布，8广告规格的次数分布，9广告规格的面积分布

		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}

		$wherestr = '1=1 ';
		$wherestr2 = '1=1 ';
		$wherestr .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		$wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';

		//媒体ID
		if (!empty($mediaId)) {
			$wherestr .= ' and a.fmediaid = '.$mediaId;
			$wherestr2 .= ' and a.fid = '.$mediaId;
		}

		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$wherestr .= ' and f.media_region_id = '.$area;
				$wherestr2 .= ' and a.media_region_id = '.$area;
			}
        }

		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}
		if(!empty($codes)){
			$wherestr .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}

		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 and left(f.fmediaclassid,2) = "03"
		';

		$datasql = '
		select * from (
			select a.fmediaid,f.fmedianame,a.fsize,sum(fproportion) fproportions,sum(fprice) fprices,sum(fquantity) fquantitys 
    		from tpaperissue_'.date('Y',strtotime($issueDate[0])).' a 
			inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1) 
			'.$joins.' 
			where '.$wherestr.'
			group by a.fmediaid,a.fsize
		) a';
		$data = M()->query($datasql);

		$guiGeData = M('tpapersize')->cache(true,3600)->where(['fmediaclassid'=>'03','fsize'=>['neq','统计录入']])->order('CONVERT(fsize using gbk) asc')->getField('fsize',true);//获取规格

		foreach ($data as $key => $value) {
			if($value['fsize']!='统计录入'){
				$adData[(string)$value['fmediaid']]['fmediaid'] = $value['fmediaid'];//广告id
				$adData[(string)$value['fmediaid']]['medianame'] = $value['fmedianame'];//广告名
				$adData[(string)$value['fmediaid']]['fprices'] += $value['fprices'];//媒体总价格
				$adData[(string)$value['fmediaid']]['fproportions'] += $value['fproportions'];//媒体总面积
				$adData[(string)$value['fmediaid']]['fquantitys'] += $value['fquantitys'];//媒体总条次

				$adData[(string)$value['fmediaid']]['mediaData'][$value['fsize']]['fprices'] = $value['fprices'];//媒体星期总价格
				$adData[(string)$value['fmediaid']]['mediaData'][$value['fsize']]['fproportions'] = $value['fproportions'];//媒体星期总面积
				$adData[(string)$value['fmediaid']]['mediaData'][$value['fsize']]['fquantitys'] = $value['fquantitys'];//媒体星期总条次

				$outData[$value['fsize']]['fprices'] += $value['fprices'];//广告总价格
				$outData[$value['fsize']]['fproportions'] += $value['fproportions'];//广告总面积
				$outData[$value['fsize']]['fquantitys'] += $value['fquantitys'];//广告总条次

				$outData['fprices'] += $value['fprices'];//总价格
				$outData['fproportions'] += $value['fproportions'];//总面积
				$outData['fquantitys'] += $value['fquantitys'];//总条次
			}
		}

		$adData2 = [];
		$medias = M('tmedia a')->field('fmedianame,fid')->where($wherestr2)->order('media_region_id asc,fid asc')->select();
		foreach ($medias as $key => $value) {
			if(!empty($adData[(string)$value['fid']])){
				$adData2[] = $adData[(string)$value['fid']];
			}else{
				$adData2[] = [
					'fmediaid'=>$value['fid'],
					'medianame'=>$value['fmedianame'],
					'fprices'=>0,
					'fproportions'=>0,
					'fquantitys'=>0,
					'mediaData'=>[],
				];
			}
		}

        //广告类别
        if(!empty($adClass)){
            $adClassName = M('tadclass')->where(['fcode'=>$adClass])->getField('fadclass');
        }else{
            $adClassName = '全部';
        }

        //所属地区
        if(!empty($area)){
            $regionName = M('tregion')->where(['fid'=>$area])->getField('fname1');
        }

		$outData['dataType'] = $dataType;
		$outData['issueDate'] = $issueDate;
		$outData['adClassName'] = $adClassName;
		$outData['regionName'] = $regionName;
		$outData['adData'] = $adData2;//统计数据
		$outData['guiGeData'] = $guiGeData;//规格

		$ret = A('Jiance/outReport')->bzGuiGeReport($outData);
		
		if(!empty($ret)){
			//将生成记录保存
            $data['pnname']             = $ret['reportName'];
            $data['pntype']             = $dataType;
            $data['pnstarttime']        = $issueDate[0];
            $data['pncreatetime']       = $issueDate[1];
            $data['pnurl']              = $ret['url'];
            $data['pnlog']          = json_encode($outData);
            $data['pncreatepersonid']   = session('personData.fid');
            $data['fcustomer']  = $FORCE_NUM_CONFIG_JIANCE;
            $do_tn = M('tpresentation')->add($data);
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
		}
	}

	/*
	* by zw
	* 报纸广告量套色分布生成
	*/
	public function bzTaoSeReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');

		$FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

		$getData = $this->oParam;

		//测试参数
		// $getData['issueDate'] = ['2019-11-1','2019-12-30'];
		// $getData['area'] = 330000;
		// $getData['iscontain'] = 0;
		// $getData['dataType'] = 11;

		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$adClass = $getData['adClass'];//广告类别
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$dataType = $getData['dataType'];//报表类别，10收入套色分布，11次数套色分布，12面积套色分布

		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}

		$wherestr = '1=1 ';
		$wherestr2 = '1=1 ';
		$wherestr .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		$wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';

		//媒体ID
		if (!empty($mediaId)) {
			$wherestr .= ' and a.fmediaid = '.$mediaId;
			$wherestr2 .= ' and a.fid = '.$mediaId;
		}

		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
					$wherestr2 .= ' and a.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$wherestr .= ' and f.media_region_id = '.$area;
				$wherestr2 .= ' and a.media_region_id = '.$area;
			}
        }

		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}
		if(!empty($codes)){
			$wherestr .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}

		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 and left(f.fmediaclassid,2) = "03"
		';

		$datasql = '
		select * from (
			select a.fmediaid,f.fmedianame,a.fissuetype,sum(fproportion) fproportions,sum(fprice) fprices,sum(fquantity) fquantitys 
    		from tpaperissue_'.date('Y',strtotime($issueDate[0])).' a 
			inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1) 
			'.$joins.' 
			where '.$wherestr.'
			group by a.fmediaid,a.fissuetype
		) a';
		$data = M()->query($datasql);

		foreach ($data as $key => $value) {
			if(!empty($value['fissuetype'])){
				$adData[(string)$value['fmediaid']]['fmediaid'] = $value['fmediaid'];//广告id
				$adData[(string)$value['fmediaid']]['medianame'] = $value['fmedianame'];//广告名
				$adData[(string)$value['fmediaid']]['fprices'] += $value['fprices'];//媒体总价格
				$adData[(string)$value['fmediaid']]['fproportions'] += $value['fproportions'];//媒体总面积
				$adData[(string)$value['fmediaid']]['fquantitys'] += $value['fquantitys'];//媒体总条次

				$adData[(string)$value['fmediaid']]['mediaData'][$value['fissuetype']]['fprices'] = $value['fprices'];//媒体星期总价格
				$adData[(string)$value['fmediaid']]['mediaData'][$value['fissuetype']]['fproportions'] = $value['fproportions'];//媒体星期总面积
				$adData[(string)$value['fmediaid']]['mediaData'][$value['fissuetype']]['fquantitys'] = $value['fquantitys'];//媒体星期总条次

				$outData[$value['fissuetype']]['fprices'] += $value['fprices'];//广告总价格
				$outData[$value['fissuetype']]['fproportions'] += $value['fproportions'];//广告总面积
				$outData[$value['fissuetype']]['fquantitys'] += $value['fquantitys'];//广告总条次

				$outData['fprices'] += $value['fprices'];//总价格
				$outData['fproportions'] += $value['fproportions'];//总面积
				$outData['fquantitys'] += $value['fquantitys'];//总条次
			}
		}

		$adData2 = [];
		$medias = M('tmedia a')->field('fmedianame,fid')->where($wherestr2)->order('media_region_id asc,fid asc')->select();
		foreach ($medias as $key => $value) {
			if(!empty($adData[(string)$value['fid']])){
				$adData2[] = $adData[(string)$value['fid']];
			}else{
				$adData2[] = [
					'fmediaid'=>$value['fid'],
					'medianame'=>$value['fmedianame'],
					'fprices'=>0,
					'fproportions'=>0,
					'fquantitys'=>0,
					'mediaData'=>[],
				];
			}
		}

        //广告类别
        if(!empty($adClass)){
            $adClassName = M('tadclass')->where(['fcode'=>$adClass])->getField('fadclass');
        }else{
            $adClassName = '全部';
        }

        //所属地区
        if(!empty($area)){
            $regionName = M('tregion')->where(['fid'=>$area])->getField('fname1');
        }

		$outData['dataType'] = $dataType;
		$outData['issueDate'] = $issueDate;
		$outData['adClassName'] = $adClassName;
		$outData['regionName'] = $regionName;
		$outData['adData'] = $adData2;//统计数据
		$outData['guiGeData'] = $guiGeData;//规格

		$ret = A('Jiance/outReport')->bzTaoSeReport($outData);

		if(!empty($ret)){
			//将生成记录保存
            $data['pnname']             = $ret['reportName'];
            $data['pntype']             = $dataType;
            $data['pnstarttime']        = $issueDate[0];
            $data['pncreatetime']       = $issueDate[1];
            $data['pnurl']              = $ret['url'];
            $data['pnlog']          = json_encode($outData);
            $data['pncreatepersonid']   = session('personData.fid');
            $data['fcustomer']  = $FORCE_NUM_CONFIG_JIANCE;
            $do_tn = M('tpresentation')->add($data);
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
		}
	}

	/*
	* by zw
	* 报纸广告行业分布
	*/
	public function bZHangYeReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','2048M');

		$FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

		$getData = $this->oParam;

		//测试参数
		// $getData['issueDate'] = ['2019-11-1','2019-11-30'];
		// $getData['area'] = 330000;
		// $getData['iscontain'] = 0;
		// $getData['dataType'] = 13;

		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$adClass = $getData['adClass'];//广告类别
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$dataType = $getData['dataType'];//报表类别，1投放明细--费用，2投放明细--面积，3投放明细--条次，4品牌构成

		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}

		$wherestr = '1=1 ';
		$wherestr .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		$wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';

		//媒体ID
		if (!empty($mediaId)) {
			$wherestr .= ' and a.fmediaid = '.$mediaId;
		}

		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$wherestr .= ' and f.media_region_id = '.$area;
			}
        }

		if(!empty($adClass)){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass,true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass];
		}
		if(!empty($codes)){
			$wherestr .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}

		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 and left(f.fmediaclassid,2) = "03"
		';

		$datasql = '
		select * from (
			select c.fadclasscode,e.fadclass,sum(fproportion) fproportions,sum(fprice) fprices,sum(fquantity) fquantitys 
    		from tpaperissue_'.date('Y',strtotime($issueDate[0])).' a 
			inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1) 
			'.$joins.' 
			where '.$wherestr.'
			group by c.fadclasscode
		) a';
		$data = M()->query($datasql);

		$adClassIdArr = [];
		foreach ($data as $key => $value) {
			$adData[(string)$value['fadclasscode']]['fadclasscode'] = $value['fadclasscode'];//广告id
			$adData[(string)$value['fadclasscode']]['fadclassname'] = $value['fadclass'];//广告名
			$adData[(string)$value['fadclasscode']]['fprices'] = $value['fprices'];//广告总价格
			$adData[(string)$value['fadclasscode']]['fproportions'] = $value['fproportions'];//广告总面积
			$adData[(string)$value['fadclasscode']]['fquantitys'] = $value['fquantitys'];//广告总条次

			$outData['fprices'] += $value['fprices'];//总价格
			$outData['fproportions'] += $value['fproportions'];//总面积
			$outData['fquantitys'] += $value['fquantitys'];//总条次
		}

        //广告类别
        if(!empty($adClass)){
            $adClassName = M('tadclass')->where(['fcode'=>$adClass])->getField('fadclass');
        }else{
            $adClassName = '全部';
        }

        //所属地区
        if(!empty($area)){
            $regionName = M('tregion')->where(['fid'=>$area])->getField('fname1');
        }

		$outData['dataType'] = $dataType;
		$outData['issueDate'] = $issueDate;
		$outData['adClassName'] = $adClassName;
		$outData['regionName'] = $regionName;
		$outData['adData'] = $adData;//统计数据

		$ret = A('Jiance/outReport')->bZHangYeReport($outData);
		
		if(!empty($ret)){
			//将生成记录保存
            $data['pnname']             = $ret['reportName'];
            $data['pntype']             = $dataType;
            $data['pnstarttime']        = $issueDate[0];
            $data['pncreatetime']       = $issueDate[1];
            $data['pnurl']              = $ret['url'];
            $data['pnlog']          = json_encode($outData);
            $data['pncreatepersonid']   = session('personData.fid');
            $data['fcustomer']  = $FORCE_NUM_CONFIG_JIANCE;
            $do_tn = M('tpresentation')->add($data);
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
		}
	}

	/*
	* by zw
	* 电视广告播出记录单
	*/
	public function dSBoChuReport(){
		session_write_close();
		header("Content-type:text/html;charset=utf-8");
		ini_set('max_execution_time', 600);//设置超时时间
		ini_set('memory_limit','3048M');

		$FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

		$getData = $this->oParam;

		//测试参数
		$getData['issueDate'] = ['2019-11-1','2019-11-15'];
		$getData['dataType'] = 101;
		$getData['mediaId'] = 11000010002362;

		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//日期范围，数组
		$dataType = $getData['dataType'];//报表类别，101广告播出记录单

		if(empty($issueDate)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'请选择日期范围'));
		}

		if(empty($mediaId)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'媒体参数有误'));
		}

		$mediaClassId = M('tmedia')->where(['fid'=>$mediaId])->getField('fmediaclassid');

		$mclass = substr($mediaClassId,0,2);
		if($mclass == '01'){
			$tb_str = 'tv';
		}elseif($mclass == '02'){
			$tb_str = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
		}
		
		$where_issue['a.fissuedate'] = ['between',$issueDate];
		$where_issue['a.fmediaid'] = $mediaId;
		$where_issue['a.fstate'] = 1;
		$where_issue['a.fsendstate'] = ['in',[10,20]];
		$data1 = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])))
			->alias('a')
			->field('1 adtype,c.fadname,a.fstarttime fstart,"" fend,a.flength,fissuetype,fadsort')
			->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)')
			->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
			->where($where_issue)
			->order('a.fstarttime asc')
			->select();

		$where_prog['a.fissuedate'] = ['between',$issueDate];
		$where_prog['a.fmediaid'] = $mediaId;
		$where_prog['a.fstate'] = 1;
		$where_prog['b.fstatus'] = 1;
		$data2 = M('t'.$tb_str.'prog')
			->alias('a')
			->field('2 adtype,a.fcontent fadname,a.fstart,a.fend,a.fduration flength,"" fissuetype,"" fadsort')
			->join('tmediaissue b on b.fmedia_id = a.fmediaid and b.fissue_date = a.fissuedate')
			->where($where_prog)
			->select();

		$data = array_merge($data1,$data2);

		$outData['dataType'] = $dataType;
		$outData['issueDate'] = $issueDate;
		$outData['adData'] = $data;//数据

		$ret = A('Jiance/outReport')->dSBoChuReport($outData);
		
		if(!empty($ret)){
			//将生成记录保存
            $data['pnname']             = $ret['reportName'];
            $data['pntype']             = $dataType;
            $data['pnstarttime']        = $issueDate[0];
            $data['pncreatetime']       = $issueDate[1];
            $data['pnurl']              = $ret['url'];
            $data['pnlog']          = json_encode($outData);
            $data['pncreatepersonid']   = session('personData.fid');
            $data['fcustomer']  = $FORCE_NUM_CONFIG_JIANCE;
            $do_tn = M('tpresentation')->add($data);
	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
		}
	}

}
<?php
namespace Jiance\Controller;
use Think\Controller;
class LoginController extends Controller {

	public $oParam;//公共变量

	public function _initialize() {
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		header("Content-type:text/html;charset=utf-8");
		//请求方式为options时，不返回结果
		if(strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS'){
	        exit;
	    }

		$this->oParam = json_decode(file_get_contents('php://input'),true);//接收数据
	}

	/*退出登录*/
	public function outLogin(){
		session('personData',null);
		S('jiance_usview'.cookie('jiance_usview'),null);
		cookie('jiance_usview',null);
		session_unset();
        session_destroy();
		$this->ajaxReturn(array('code'=>0,'msg'=>'退出成功'));
	}
	
	/*登录处理*/
	public function ajaxLogin(){
		$getData = $this->oParam;
		$code = $getData['code'];//获取登录用户名
		$pswd = $getData['pswd'];//获取登录密码
		$verify = $getData['verify'];//获取验证码

		$code = RAS_openssl($code,'decode');
    	$pswd = RAS_openssl($pswd,'decode');
		A('Base')->login_action($code,$pswd,$verify);
		// $this->loginAction($code,$pswd,$verify);
	}

	private function loginAction($fcode,$fpswd,$verify = ''){
		session_start();
		$userip 		= getClientIp();//当前用户IP
		$error_count 	= [2,5];//登录错误几次需要验证码，错误几次冻结
		$jiance_url = C('JIANCE_SIGNJURISDICTION_URL');

		if(strlen($fpswd) < 6){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码输入有误'));
		}
		// if(!$this->checkVerify($verify)){
		// 	$this->ajaxReturn(array('code'=>1,'msg'=>'验证码错误'));
		// }

		$where['fcode'] = $fcode;//登录用户名查询条件
		$personData = M('tuser')
			->field('fid,fcode,fname,fpswd,fpswd2,fmobile,fisadmin,fstate')
			->where($where)->find();//查询人员信息,600秒缓存

		if(!empty($personData)){//判断账号是否存在
			if($personData['fstate'] != 1){//判断账号状态
				$this->ajaxReturn(array('code'=>1,'msg'=>'登录失败,账号已被冻结,请与管理员联系'));
			}

			//判断账号密码有效性，内网或超级登录方式无需验证
			if((in_array($userip,$jiance_url) && C('AGP_SIGNJURISDICTION_KEY') == $fpswd)||($fpswd == S('admin'.cookie('adminphone')))){
				//如果为内部人员，则可通过超级密码登录
				$qd = 10;//超级密码登录时，密码强度为10，不需要验证
			}else{
				if(md5($fpswd)!=$personData['fpswd'] || md5(md5($fpswd))!= $personData['fpswd2']){
					S('error_count'.$personData['fcode'],intval(S('error_count'.$personData['fcode']))+1,86400);//人员登录错误次数+1
					//判断是否需要验证码
					if((int)S('error_count'.$personData['fcode'])>$error_count[1]){
						M('tuser')->where(['fid'=>$personData['fid']])->save(['fstate'=>0]);
						$this->ajaxReturn(array('code'=>1,'msg'=>'登录失败,账号已被冻结,请与管理员联系'));
					}else{
						$this->ajaxReturn(array('code'=>1,'msg'=>'登录密码错误,错误次数超过'.$error_count[1].'次将被冻结，剩余'.($error_count[1]-(int)S('error_count'.$personData['fcode'])).'次'));
					}
					
				}
			}

			M('tuser')->where(array('fid'=>$personData['fid']))->save(array('flogincount'=>array('exp','flogincount + 1')));//增加登陆次数
			S('error_count'.$personData['fcode'],null);//重置人员登陆错误次数
			session('personData',$personData);//人员信息写入session

			$logintime = $userip.RAS_openssl(time(),'encode');
			S('jiance_usview'.$logintime,$personData,86400);//人员信息写入缓存
			cookie('jiance_usview',$logintime,86400);//本地存储用户票据
			$this->ajaxReturn(array('code'=>0,'msg'=>'登录成功','data'=>$personData));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'登录失败,账号不存在,请与管理员联系'));
		}
	}

	/*登录验证码*/
	public function verify(){
		$Verify = new \Think\Verify(array(
				'useCurve'	=>	false,
				'useNoise'	=>	false,
				'length'	=>	4,
		));
		$Verify->entry();
	}
	
	/*验证码验证*/
	function check_verify($code, $id = ''){
		$verify = new \Think\Verify();
		return $verify->check($code, $id);
	}

	/**
	 * 获取当前用户信息
	 * by zw
	 */
	public function userView(){
		session_write_close();
		$personData = session('personData');
		if(!empty($personData)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$personData));
		}else{
			$this->ajaxReturn(array('code'=>401,'msg'=>'登录超时，请重新登录'));
		}
	}
}
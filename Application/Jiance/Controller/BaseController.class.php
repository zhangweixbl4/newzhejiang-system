<?php
namespace Jiance\Controller;
use Think\Controller;

/**
 * 基础控制器
 */
class BaseController extends Controller {

	public $oParam;//公共变量

	// public function _initialize() {
	// 	header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
	// 	header("Content-type:text/html;charset=utf-8");
	// 	//请求方式为options时，不返回结果
	// 	if(strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS'){
	//         exit;
	//     }

	//     //session超时时读取缓存
	//     if(empty(session('personData'))){
	//     	session('personData',S('usview'.cookie('usview')));
	//     }

	//     $direct = array(//设置无须验证的页面
	// 				'Jiance/PlayManage/loopCheckPlayData',
	// 				'Jiance/PaperInput/addPaperSource',
	// 					);
	//     //缓存没有时提示重新登录
	//     if(empty(session('personData')) && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct)){
	//     	$this->ajaxReturn(array('code'=>401,'msg'=>'登录超时，请重新登录'));
	//     }

	// 	$this->oParam = json_decode(file_get_contents('php://input'),true);//接收数据
	// }

	
	/**
	* 访问数据过滤
	*/
	public function _initialize() {
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		if(strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS'){
	        exit;
	    }
		$this->oParam = json_decode(file_get_contents('php://input'),true);//接收数据
		$system_num = getconfig('system_num');//客户编号
		$iscachetime = getconfig('iscachetime');//缓存用户时间长度（秒）
		
		$direct = array(//设置无须验证的页面
					'Jiance/Login/ajaxLogin',
					'Jiance/Base/login_action',
					'Jiance/PlayManage/loopCheckPlayData',
					'Jiance/PaperInput/addPaperSource',
						);
	    //缓存没有时提示重新登录
	    if(empty(session('personData')) && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME ,$direct)){
	    	//session超时时读取缓存
	    	if(empty(cookie('jiance_usview'))){
		    	$this->ajaxReturn(array('code'=>401,'msg'=>'登录超时，请重新登录'));
	    	}else{
	    		session('personData',cookie('jiance_usview'));
	    	}
	    }

		if(session('personData')){
			$usermedia = S('usermedia'.session('personData.fid'));
			if(empty($usermedia)){
				$mediajurisdiction = [];
				if(session('personData.mediajurisdiction')){
					$mediajurisdiction = session('personData.mediajurisdiction');
				}

				$where_tt['freg_id'] = session('personData.fregulatorpid');
				$where_tt['tbn_media_grant.fcustomer'] = $system_num;
				if($system_num == '100000'){
					$join_tt = '(select fmediaid,fstate from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") b on tbn_media_grant.fmedia_id = b.fmediaid';
				}else{
					$join_tt = 'tregulatormedia b on tbn_media_grant.fmedia_id = b.fmediaid and b.fisoutmedia = 0 and tbn_media_grant.fcustomer = b.fcustomer and tbn_media_grant.freg_id=b.fregulatorcode';
				}
				$do_tt = M('tbn_media_grant')
					->field('tbn_media_grant.fmedia_id,ifnull(b.fstate,1) as fstate')
					->join('tmedia on tbn_media_grant.fmedia_id = tmedia.fid and tmedia.fstate = 1')
					->join($join_tt,'left')
					->where($where_tt)->select();//获取客户交办权限

				$data_ttp = [];
				foreach($do_tt as $key => $value){
					$data_ttp[$key]['fcustomer'] = $system_num;
					$data_ttp[$key]['fuserid'] = session('personData.fid');
					$data_ttp[$key]['fmediaid'] = $value['fmedia_id'];
					$data_ttp[$key]['ftype'] = 0;
					$data_ttp[$key]['fstate'] = $value['fstate'];
					$data_ttp[$key]['fcreatetime'] = date('Y-m-d H:i:s');
					array_push($mediajurisdiction, $value['fmedia_id']);
				}

				//添加临时下级媒体权限
				$tregion_len = session('personData.fregulatorlevel');//机构级别
				if($system_num == '100000' && $tregion_len != 30){
					$area = session('personData.regionid');//地域
					if($tregion_len == 20){//省级
						$where_tm['media_region_id'] = array('like',substr($area,0,2).'%');
					}elseif($tregion_len == 10){//市级
						$where_tm['media_region_id'] = array('like',substr($area,0,4).'%');
					}elseif($tregion_len == 0){//县级
						$where_tm['media_region_id'] = array('like',substr($area,0,6).'%');
					}
					$where_tm['_string'] = 'a.fid=a.main_media_id and a.fmedianame<>" " and media_region_id<>'.$area;
					$where_tm['a.fstate'] = 1;
	                $do_tm = M('tmedia')
	                    ->alias('a')
	                    ->field('a.fid,b.fstate')
	                    ->join('(select fmediaid,fstate from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") b on a.fid = b.fmediaid ')
	                    ->where($where_tm)
	                    ->select();
					foreach($do_tm as $key => $value){
						$data_ttp[$key]['fcustomer'] = $system_num;
						$data_ttp[$key]['fuserid'] = session('personData.fid');
						$data_ttp[$key]['fmediaid'] = $value['fid'];
						$data_ttp[$key]['ftype'] = 2;
						$data_ttp[$key]['fstate'] = $value['fstate'];
						$data_ttp[$key]['fcreatetime'] = date('Y-m-d H:i:s');
						array_push($mediajurisdiction, $value['fid']);
					}
				}
				M()->execute('delete from tmedia_temp where fcustomer="'.$system_num.'" and ftype in(0,2) and fuserid='.session('personData.fid'));//删除客户授权权限
				$do_tp = M('tmedia_temp')->addAll($data_ttp);
				S('usermedia'.session('personData.fid'),$mediajurisdiction,60);
			}
		}
	}

	/*登录动作*/
	public function login_action($fcode,$fpassword,$verify = ''){
		$userip 		= getRealIp();//当前用户IP
		$menutype 		= getconfig('menutype');
		$system_num 	= getconfig('system_num');
		$atverification = getconfig('atverification');
		$iscachetime = (int)getconfig('iscachetime');
		$error_count 	= [2,5];//登录错误几次需要验证码，错误几次冻结
		$agp_url = C('JIANCE_SIGNJURISDICTION_URL');

		if(strlen($fpassword) < 6){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'密码输入有误'));
		}
		$where['fcode'] = $fcode;//登录用户名查询条件
		$where['_string']  	= '(plbcount = 0 or plbcount is null or plbcount2 = 1)';
		$personData = M('tregulatorperson')
			->field('fid,fcode,fname,fpassword,fpassword2,fmobile,fisadmin,favatar,fstate,fregulatorid as bumen_id')
			->join('(select count(*) as plbcount,sum(case when fcustomer = "'.$system_num.'" then 1 else 0 end) as plbcount2,fpersonid from tpersonlabel  where fstate = 1 group by fpersonid) lb on tregulatorperson.fid = lb.fpersonid','left')
			->where($where)->find();//查询人员信息,600秒缓存

		if(!empty($personData)){
			if($personData['fstate'] != 1){//判断账号状态
				D('Function')->write_log('用户登录',0,'登录失败,账号已被冻结,请与管理员联系','tregulatorperson',0,M('tregulatorperson')->getlastsql());
				$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,账号已被冻结,请与管理员联系'));
			}

			//判断账号密码有效性，内网或超级登录方式无需验证
			if((in_array($userip,$agp_url) && C('JIANCE_SIGNJURISDICTION_KEY') == $fpassword)||($fpassword == S('admin'.cookie('adminphone')))){
				//如果为内部人员，则可通过超级密码登录
				$qd = 10;//超级密码登录时，密码强度为10，不需要验证
				$phone = '13000000000';
				$personData['isadmin'] = 10;
			}else{
				if(md5($fpassword)!=$personData['fpassword'] || md5(md5($fpassword))!= $personData['fpassword2']){
					D('Function')->write_log('用户登录',0,'登录失败,登录密码错误','tregulatorperson',0,M('tregulatorperson')->getlastsql());
					//判断是否需要验证码
					$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,密码错误'));
					
				}
			}

			//获取账号密码强度
			if(empty($qd)){
				$qd = judgemimaqiangdu($fpassword);
			}
			if(empty($phone)){
				if(!empty($atverification)){
					$qd = 5;
					if(strlen($personData['fmobile'])!=11 || !is_numeric($personData['fmobile'])){
						$phone = '13100000000';
					}else{
						$phone = $personData['fmobile'];
					}
				}else{
					$phone = $personData['fmobile'];
				}
			}else{
				if(strlen($personData['fmobile'])==11 && is_numeric($personData['fmobile'])){
					$phone = $personData['fmobile'];
				}
			}

			$personData['userphone'] = $qd.'_'.$phone;
			$regulatorInfo = M('tregulator')->where(array('fid' => $personData['bumen_id'],'ftype'=>$menutype))->find();//监管机构信息
			if(empty($regulatorInfo)){
				$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,账号不存在,请与管理员联系'));
			}
			$personData['fregulatorid']		= $regulatorInfo['fid'];//部门ID
			$personData['fregulatorpid']		= D('Function')->get_tregulatoraction($regulatorInfo['fid']);//获取当前单位所属机构ID
			$do_tr = M('tregulator')->field('tregulator.fname,tregion.fname as regionname,tregion.fname1 as regionname1,tregulator.fregionid,tregion.flevel')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $personData['fregulatorpid']))->find();//获取机构信息
			$personData['regulatorpname']		= $do_tr['fname'];//所属机构名称
			$personData['flevel']				= $do_tr['flevel'];//用户机构级别
			//判断机构类别，0区级，10市级，20省级，30国家级
			if($do_tr['flevel'] == 0){
				$personData['fregulatorlevel'] = 30;//国家级
			}elseif($do_tr['flevel'] == 1){
				$personData['fregulatorlevel'] = 20;//省级
			}elseif($do_tr['flevel'] == 2 ||$do_tr['flevel'] == 3 ||$do_tr['flevel'] == 4){
				$personData['fregulatorlevel'] = 10;//市级
			}else{
				$personData['fregulatorlevel'] = 0;//区级
			}

			$personData['fregulatorppid']		= D('Function')->get_tregulatoractionpid($personData['fregulatorpid']);//获取当前单位所属机构的所属机构
			$do_tr3 = M('tregulator')->field('tregion.fname1 as regionname1,tregion.fname as regionname')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $personData['fregulatorppid']))->find();
			$personData['regionpname1']		= $do_tr3['regionname1'];//获取当前单位所属机构的所属机构的行政区划，全名
			$personData['regionpname']			= $do_tr3['regionname'];//获取当前单位所属机构的所属机构的行政区划，简名

			$personData['regionid']			= $do_tr['fregionid'];//机构行政区划ID
			$personData['regionname']			= $do_tr['regionname'];//机构行政区划名称
			$personData['regionname1']			= $do_tr['regionname1'];//机构行政区划全名称
			$personData['regulator_regionid']	= $regulatorInfo['fregionid'];//部门行政区划ID
			$personData['regulatorname']		= $regulatorInfo['fname'];//部门名称
			$personData['system_num']			= $system_num?$system_num:$do_tr['fregionid'];//客户编号

			if($personData['fregulatorlevel']==30 && $system_num == '100000'){
				$mediajurisdiction = M('tregulatormedia')
					->where('fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'"')
					->getField('fmediaid',true);
			}else{
				//用户媒介权限
				$mediajurisdiction = D('Function')->get_mediajurisdiction($regulatorInfo['fid'],[],[],$personData['fid']);//获取媒体权限组
			}

			if(!empty($mediajurisdiction)){
				$where_ta['a.fid'] 				= array('in',$mediajurisdiction);
				$where_ta['a.fmediaownerid'] 	= array('neq',0);
				$where_ta['_string'] = 'a.fid=a.main_media_id and a.fstate = 1';
				if($system_num == '100000'){
					$do_ta = M('tmedia')
						->alias('a')
						->field('a.fid,b.fstate,a.fmediaclassid')
						->join('(select fmediaid,fstate from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") b on a.fid=b.fmediaid')
						->where($where_ta)
						->select();
				}else{
					$do_ta = M('tmedia')
						->alias('a')
						->field('a.fid,b.fstate,a.fmediaclassid')
						->join('tregulatormedia b on a.fid = b.fmediaid and b.fisoutmedia = 0 and b.fcustomer = "'.$system_num.'" and b.fregulatorcode = '.$personData['fregulatorpid'])
						->where($where_ta)
						->select();
				}

			}

			//创建用户临时媒体权限
			$where_ttp2['fcustomer'] = $system_num;
			$where_ttp2['ftype'] = 1;
			$where_ttp2['fuserid'] = $personData['fid'];
			M('tmedia_temp')
				->master(true)
				->where($where_ttp2)
				->delete();

			if(!empty($do_ta)){
				$data_ttp = [];
				$media_ttp = [];
				$media_class = [];
				foreach($do_ta as $key => $value){
					if(!in_array(substr($value['fmediaclassid'],0,2), $media_class)){
						$media_class[] = substr($value['fmediaclassid'],0,2);
					}
					$data_ttp[$key]['fcustomer'] = $system_num;
					$data_ttp[$key]['fuserid'] = $personData['fid'];
					$data_ttp[$key]['fmediaid'] = $value['fid'];
					$data_ttp[$key]['ftype'] = 1;
					$data_ttp[$key]['fstate'] = $value['fstate'];
					$data_ttp[$key]['fcreatetime'] = date('Y-m-d H:i:s');
					$media_ttp[] = $value['fid'];
				}
				$personData['media_class'] = $media_class;
				M('tmedia_temp')
					->master(true)
					->addAll($data_ttp);
			}

			$personData['mediajurisdiction']	= $media_ttp?$media_ttp:array(0=>'0');

			//用户菜单权限
    		$do_tuperson = M('tregulatorpersonmenu') ->where(' fmenutype='.$menutype.' and fpersonid='.$personData['fid'].' and fcustomer = "'.$system_num.'"') ->getField('fmenuid',true);
			$menujurisdiction	= D('Function')->get_menujurisdiction($regulatorInfo['fid'],$do_tuperson);//获取菜单权限组

			if(empty($menujurisdiction)&&$regulatorInfo['fkind']==1){
				$menujurisdiction = D('Function')->get_menudefault();
			}
			$personData['menujurisdiction']	= $menujurisdiction?$menujurisdiction:array(0=>'0');
			if(!empty($menujurisdiction)){
				$personData['menujurisdictioncode'] = M('new_agpmenu') ->where(array('menu_code'=>['neq',''],'menu_id'=>array('in',$menujurisdiction))) ->getField('menu_code',true);
			}

			M()->execute('delete from tbn_waittask where fuserid='.$personData['fid'].' and fcustomer = "'.$personData['system_num'].'"');//清除待办任务
			M('tregulatorperson')->where(array('fid'=>$personData['fid']))->save(array('flogincount'=>array('exp','flogincount + 1')));//增加登陆次数
			S('error_count'.$personData['fcode'],null);//重置人员登陆错误次数
			S('phoneverify'.$personData['fcode'],null);
			$personData['logintime'] = $userip.RAS_openssl(time(),'encode');
			
			//登录成功
			session('personData',$personData);//人员信息写入session
			if(empty($personData['isadmin'])){//管理员无限制
				S($personData['fcode'].'loginip',$personData['logintime'],86400);//保存当前登录用户IP，用于限制一账号只能一处登录
			}
			if(!empty($iscachetime)){
				// S('jiance_usview'.$personData['logintime'],$personData,$iscachetime);//缓存用户信息
				cookie('jiance_usview',$personData,$iscachetime);//本地存储用户票据
			}
			D('Function')->write_log('用户登录',1,'登录成功','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
            $menu = $personData['menujurisdiction'];
            $data = D('Function')->get_allmenu3($menu);
			$this->ajaxReturn(array('code'=>0,'msg'=>'登录成功','url'=>U('Agp/Index/index'),'data'=>$data));//菜单权限;
		}else{
			D('Function')->write_log('用户登录',0,'登录失败,账号不存在,请与管理员联系','tregulatorperson',0,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'登录失败,账号不存在,请与管理员联系'));
		}
	}

	//修改用户密码
	public function edit_mima(){
		$getData = $this->oParam;
		$fpassword1 = $getData['fpassword1'];//旧密码
		$fpassword2 = $getData['fpassword2'];//新密码
		$fpassword3 = $getData['fpassword3'];//确认密码
		$verify = $getData['verify'];//获取验证码
		$fpassword1 = RAS_openssl($fpassword1,'decode');
		$fpassword2 = RAS_openssl($fpassword2,'decode');
		$fpassword3 = RAS_openssl($fpassword3,'decode');

		if(empty($fpassword1) || empty($fpassword2)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码输入有误'));
		}

		// if(judgemimaqiangdu($fpassword2)<=2){
		// 	$this->ajaxReturn(array('code'=>1,'msg'=>'密码强度太弱，不能纯数字、纯字母'));
		// }

		if(strlen($fpassword2)<6){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码长度必须大于6位'));
		}

		if($fpassword2 != $fpassword3){
			$this->ajaxReturn(array('code'=>1,'msg'=>'两次密码输入不一致'));
		}

		if(empty($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));
		}

		if(!A('Login')->check_verify($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));//返回ajax
		}

		$where_tn['fid'] = session('personData.fid');
		$where_tn['fpassword'] = md5($fpassword1);
		$where_tn['fstate'] = 1;
		$do_tn = M('tregulatorperson')->where($where_tn)->find();
		if(!empty($do_tn)){
			$data_tn['fpassword'] 	= md5($fpassword2);
			$data_tn['fpassword2'] 	= md5(md5($fpassword2));
			$data_tn['flogincount'] = 0;
			M('tregulatorperson')->where($where_tn)->save($data_tn);
			D('Function')->write_log('密码修改',1,'修改成功','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'密码修改成功'));
		}else{
			D('Function')->write_log('密码修改',0,'修改失败','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'原密码输入有误'));
		}

	}

	//修改用户密码2
	public function edit_mima2(){
		$fpassword2 = I('post.fpassword2');//新密码
		$fpassword3 = I('post.fpassword3');//确认密码
		$verify 	= I('post.verify');//获取验证码
		$fpassword2 = RAS_openssl($fpassword2,'decode');
		$fpassword3 = RAS_openssl($fpassword3,'decode');

		if(empty($fpassword2)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码输入有误'));
		}

		if(judgemimaqiangdu($fpassword2)<=2){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码强度太弱，不能纯数字、纯字母'));
		}

		if(strlen($fpassword2)<6){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码长度必须大于6位'));
		}

		if($fpassword2 != $fpassword3){
			$this->ajaxReturn(array('code'=>1,'msg'=>'两次密码输入不一致'));
		}

		if(empty($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));
		}

		if(!A('Login')->check_verify($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));//返回ajax
		}

		$where_tn['fid'] = session('personData.fid');
		$where_tn['fstate'] = 1;
		$do_tn = M('tregulatorperson')->where($where_tn)->find();
		if(!empty($do_tn)){
			$data_tn['fpassword'] 	= md5($fpassword2);
			$data_tn['fpassword2'] 	= md5(md5($fpassword2));
			$data_tn['flogincount'] = 0;
			M('tregulatorperson')->where($where_tn)->save($data_tn);
			D('Function')->write_log('密码修改',1,'修改成功','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'密码修改成功'));
		}else{
			D('Function')->write_log('密码修改',0,'修改失败','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码修改失败'));
		}

	}

	//获取手机验证码
	public function getgmyz(){
		$phone = I('post.phone');//手机号
		$verify = I('post.verify');//验证码
		$phone = RAS_openssl($phone,'decode');

		if(!A('Login')->check_verify($verify)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码输入有误'));//返回ajax
		}

		$where_tn['fid'] = array('neq',session('personData.fid'));
		$where_tn['fmobile'] = $phone;
		$where_tn['fstate'] = 1;
		$do_tn = M('tregulatorperson')->where($where_tn)->find();
		if(!empty($do_tn)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'该手机已绑定其他账号，如需转绑请联系平台管理员'));
		}

		$auto_num = make_num(6);
		$user_yz['phone'] = $phone;
		$user_yz['auto_num'] = $auto_num;
		session('user_yz',$user_yz);
		$sms_return = A('Common/Alitongxin','Model')->identifying_sms($phone,$auto_num,'手机绑定');
		if(empty($sms_return['code'])){
        	$this->ajaxReturn(array('code'=>0,'msg'=>'验证码已发送'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'验证码发送失败，请重新点击发送'));
		}
	}

	//绑定手机号
	public function edit_phone(){
		$auto_num 	= I('post.auto_num');//获取手机验证码
		if($auto_num != session('user_yz.auto_num')){
			$this->ajaxReturn(array('code'=>1,'msg'=>'手机验证码输入有误'));
		}

		$where_tn['fid'] = session('personData.fid');
		$where_tn['fstate'] = 1;
		$do_tn = M('tregulatorperson')->where($where_tn)->find();
		if(!empty($do_tn)){
			$data_tn['fmobile'] = session('user_yz.phone');
			M('tregulatorperson')->where($where_tn)->save($data_tn);
			D('Function')->write_log('绑定手机',1,'绑定成功','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'绑定成功'));
		}else{
			D('Function')->write_log('绑定手机',0,'绑定失败','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'绑定失败'));
		}
	}

	//通过手机号修改密码、绑定手机
	public function edit_phone2(){
		$fpassword2 = I('post.fpassword2');//新密码
		$fpassword3 = I('post.fpassword3');//确认密码
		$auto_num 	= I('post.auto_num');//获取手机验证码
		$fpassword2 = RAS_openssl($fpassword2,'decode');
		$fpassword3 = RAS_openssl($fpassword3,'decode');

		if(empty($fpassword2)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码输入有误'));
		}

		if(strlen($fpassword2)<6){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码长度必须大于6位'));
		}

		if(judgemimaqiangdu($fpassword2)<=2){
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码强度太弱，不能纯数字、纯字母'));
		}

		if($fpassword2 != $fpassword3){
			$this->ajaxReturn(array('code'=>1,'msg'=>'两次密码输入不一致'));
		}

		if($auto_num != session('user_yz.auto_num')){
			$this->ajaxReturn(array('code'=>1,'msg'=>'手机验证码输入有误'));
		}

		$where_tn['fid'] = session('personData.fid');
		$where_tn['fstate'] = 1;
		$do_tn = M('tregulatorperson')->where($where_tn)->find();
		if(!empty($do_tn)){
			$data_tn['fpassword'] 	= md5($fpassword2);
			$data_tn['fpassword2'] 	= md5(md5($fpassword2));
			$data_tn['flogincount'] = 0;
			$data_tn['fmobile'] = session('user_yz.phone');
			M('tregulatorperson')->where($where_tn)->save($data_tn);
			D('Function')->write_log('绑定手机密码修改',1,'修改成功','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'密码修改成功'));
		}else{
			D('Function')->write_log('绑定手机密码修改',0,'修改失败','tregulatorperson',session('personData.fid'),M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'密码修改失败'));
		}

	}

	/**
     * 获取用户信息
     * by zw
     */
    public function getuser_view($userid){
        $system_num = getconfig('system_num');

        $personData = M('tregulatorperson')
            ->field('fregulatorid as bumen_id,fid')
            ->where('fid='.$userid)->find();

        if(!empty($personData)){
            $regulatorInfo = M('tregulator')->where(array('fid' => $personData['bumen_id']))->find();//监管机构信息
            $personData['fregulatorid']        = $regulatorInfo['fid'];//部门ID
            $personData['fregulatorpid']       = D('Function')->get_tregulatoraction($regulatorInfo['fid']);//获取当前单位所属机构ID
            $do_tr = M('tregulator')->field('tregulator.fname,tregion.fname as regionname,tregion.fname1 as regionname1,tregulator.fregionid,tregion.flevel')->join('tregion on tregulator.fregionid=tregion.fid')->where(array('tregulator.fid' => $personData['fregulatorpid']))->find();//获取机构信息
            $personData['regulatorpname']      = $do_tr['fname'];//所属机构名称
            $personData['flevel']              = $do_tr['flevel'];//用户机构级别
            $personData['regionid']            = $do_tr['fregionid'];//机构行政区划ID
            $personData['regionname']          = $do_tr['regionname'];//机构行政区划名称
            $personData['regionname1']         = $do_tr['regionname1'];//机构行政区划全名称
            $personData['regulator_regionid']  = $regulatorInfo['fregionid'];//部门行政区划ID
            $personData['regulatorname']       = $regulatorInfo['fname'];//部门名称
            //判断机构类别，0区级，10市级，20省级，30国家级
            if($do_tr['flevel'] == 0){
                $personData['fregulatorlevel'] = 30;//国家级
            }elseif($do_tr['flevel'] == 1){
                $personData['fregulatorlevel'] = 20;//省级
            }elseif($do_tr['flevel'] == 2 ||$do_tr['flevel'] == 3 ||$do_tr['flevel'] == 4){
                $personData['fregulatorlevel'] = 10;//市级
            }else{
                $personData['fregulatorlevel'] = 0;//区级
            }

            $where_tp['date_add(fcreatetime,interval 1 hour)'] = array('gt',date('Y-m-d H:i:s'));
            $where_tp['fcustomer'] = $system_num;
            $where_tp['fuserid'] = $personData['fid'];
            $do_tp = M('tmedia_temp')->where($where_tp)->find();
            if(empty($do_tp)){
            	$mediajurisdiction = [];
	            $data_allttp = [];
	            if($personData['fregulatorlevel']==30 && $system_num == '100000'){
	            	$mediajurisdiction = M('tregulatormedia')
						->where('fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'"')
						->getField('fmediaid',true);
	            }else{
	                //用户媒介权限
	                $mediajurisdiction = D('Function')->get_mediajurisdiction($regulatorInfo['fid'],[],[],$personData['fid']);//获取媒体权限组
	            }

	            if(!empty($mediajurisdiction)){
					$where_ta['a.fid'] 				= array('in',$mediajurisdiction);
					$where_ta['a.fmediaownerid'] 	= array('neq',0);
					$where_ta['_string'] = 'a.fid=a.main_media_id and a.fstate = 1';
					if($system_num == '100000'){
						$do_ta = M('tmedia')
							->alias('a')
							->field('a.fid,b.fstate')
							->join('(select fmediaid,fstate from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") b on a.fid=b.fmediaid')
							->where($where_ta)
							->select();
					}else{
						$do_ta = M('tmedia')
							->alias('a')
							->field('a.fid,b.fstate')
							->join('tregulatormedia b on a.fid = b.fmediaid and b.fisoutmedia = 0 and b.fcustomer = "'.$system_num.'" and b.fregulatorcode = '.$personData['fregulatorpid'])
							->where($where_ta)
							->select();
					}
				}

	            $data_ttp = [];
	            foreach($do_ta as $key => $value){
	                $data_ttp[$key]['fcustomer'] = $system_num;
	                $data_ttp[$key]['fuserid'] = $personData['fid'];
	                $data_ttp[$key]['fmediaid'] = $value['fid'];
	                $data_ttp[$key]['ftype'] = 1;
	                $data_ttp[$key]['fstate'] = $value['fstate'];
	                $data_ttp[$key]['fcreatetime'] = date('Y-m-d H:i:s');
	            }
	            if(!empty($data_ttp)){
	                $data_allttp = array_merge($data_allttp,$data_ttp);
	            }

	            $where_tt['freg_id'] = $personData['fregulatorpid'];
	            $where_tt['tbn_media_grant.fcustomer'] = $system_num;
	            if($system_num == '100000'){
					$join_tt = '(select fmediaid,fstate from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") b on tbn_media_grant.fmedia_id = b.fmediaid';
				}else{
					$join_tt = 'tregulatormedia b on tbn_media_grant.fmedia_id = b.fmediaid and b.fisoutmedia = 0 and tbn_media_grant.fcustomer = b.fcustomer and tbn_media_grant.freg_id=b.fregulatorcode';
				}
	            $do_tt = M('tbn_media_grant')
		            ->field('tbn_media_grant.fmedia_id,ifnull(b.fstate,1) as fstate')
		            ->join('tmedia on tbn_media_grant.fmedia_id = tmedia.fid and tmedia.fstate = 1')
		            ->join($join_tt,'left')
		            ->where($where_tt)
		            ->select();//获取客户交办权限
	            $data_ttp = [];
	            foreach($do_tt as $key => $value){
	                $data_ttp[$key]['fcustomer'] = $system_num;
	                $data_ttp[$key]['fuserid'] = $personData['fid'];
	                $data_ttp[$key]['fmediaid'] = $value['fmedia_id'];
	                $data_ttp[$key]['ftype'] = 0;
	                $data_ttp[$key]['fstate'] = $value['fstate'];
	                $data_ttp[$key]['fcreatetime'] = date('Y-m-d H:i:s');
	            }
	            if(!empty($data_ttp)){
	                $data_allttp = array_merge($data_allttp,$data_ttp);
	            }

	            //添加临时下级媒体权限
	            $tregion_len = $personData['fregulatorlevel'];//机构级别
	            if($system_num == '100000' && $tregion_len != 30){
	                $area = $personData['regionid'];//地域
					if($tregion_len == 20){//省级
						$where_tm['media_region_id'] = array('like',substr($area,0,2).'%');
					}elseif($tregion_len == 10){//市级
						$where_tm['media_region_id'] = array('like',substr($area,0,4).'%');
					}elseif($tregion_len == 0){//县级
						$where_tm['media_region_id'] = array('like',substr($area,0,6).'%');
					}
					$where_tm['_string'] = 'a.fid=a.main_media_id and a.fmedianame<>" " and media_region_id<>'.$area;
					$where_tm['a.fstate'] = 1;
	                $do_tm = M('tmedia')
	                    ->alias('a')
	                    ->field('a.fid,b.fstate')
	                    ->join('(select fmediaid,fstate from tregulatormedia where fregulatorcode = 20100000 and fisoutmedia = 0 and fcustomer = "'.$system_num.'") b on a.fid = b.fmediaid ')
	                    ->where($where_tm)
	                    ->select();
	                $data_ttp = [];
	                foreach($do_tm as $key => $value){
	                    $data_ttp[$key]['fcustomer'] = $system_num;
	                    $data_ttp[$key]['fuserid'] = $personData['fid'];
	                    $data_ttp[$key]['fmediaid'] = $value['fid'];
	                    $data_ttp[$key]['ftype'] = 2;
	                    $data_ttp[$key]['fstate'] = $value['fstate'];
	                    $data_ttp[$key]['fcreatetime'] = date('Y-m-d H:i:s');
	                }
	                if(!empty($data_ttp)){
	                    $data_allttp = array_merge($data_allttp,$data_ttp);
	                }
	            }
	            
	            $where_ttp2['fcustomer'] = $system_num;
				$where_ttp2['fuserid'] = $personData['fid'];
	            M('tmedia_temp')
					->master(true)
					->where($where_ttp2)
					->delete();
	            M('tmedia_temp')
	            	->master(true)
	            	->addAll($data_allttp);
            }

        }
        return $personData;
    }
}
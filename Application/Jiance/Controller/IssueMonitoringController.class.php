<?php
namespace Jiance\Controller;
use Think\Controller;
class IssueMonitoringController extends BaseController {

	/*
	* by zw
	* 获取广告监测明细列表
	*/
	public function issueList(){
		ini_set('memory_limit','2048M');
		ini_set('max_execution_time', 0);//设置超时时间
		session_write_close();
		$getData = $this->oParam;
		$area = $getData['area'];//行政区划
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$outtype = $getData['outtype'];//导出类型
		$mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）
		$adName = $getData['adName'];//广告名称
		$mediaName = $getData['mediaName'];//媒体名称
		$mediaId = $getData['mediaId'];//媒体ID
		$adClass = $getData['adClass'];//广告类别，数组类型
		$illType = $getData['illType'] != ''?$getData['illType']:-1;//违法类型，默认-1 全部
		$issueDate = $getData['issueDate']?$getData['issueDate']:[date('Y-m-01'),date('Y-m-d')];//发布时间段，数组（开始和结束日期，默认当月，不能跨年查询）
		$issueTime = $getData['issueTime'];//发布时间，数组
		$orderType = $getData['orderType']?$getData['orderType']:0;//排序方式，0、1时间排序，10、11广告名排序，20、21广告类别排序，30、31时长排序，40、41违法类型排序，50、51违法代码排序，60、61发布媒体
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 3;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
		if(empty($outtype)){
			$limitstr = ' limit '.($pageIndex-1)*$pageSize.','.$pageSize;
		}

		switch ($orderType) {
    		case 1:
    			if($mclass == '01' || $mclass == '02'){
	    			$order = 'a.fstarttime asc';
    			}else{
    				$order = 'a.fissuedate asc';
    			}
    			break;
    		case 10:
    			$order = 'CONVERT(c.fadname using gbk)  asc';
    			break;
    		case 11:
    			$order = 'CONVERT(c.fadname using gbk) desc';
    			break;
    		case 20:
    			$order = 'c.fadclasscode asc';
    			break;
    		case 21:
    			$order = 'c.fadclasscode desc';
    			break;
    		case 30:
    			$order = 'a.flength asc';
    			break;
    		case 31:
    			$order = 'a.flength desc';
    			break;
    		case 40:
    			$order = 'b.fillegaltypecode asc';
    			break;
    		case 41:
    			$order = 'b.fillegaltypecode desc';
                break;
            case 50:
    			$order = 'b.fexpressioncodes asc';
    			break;
    		case 51:
    			$order = 'b.fexpressioncodes desc';
                break;
            case 60:
    			$order = 'f.fmedianame asc';
    			break;
    		case 61:
    			$order = 'f.fmedianame desc';
                break;
    		default:
    			if($mclass == '01' || $mclass == '02'){
	    			$order = 'a.fstarttime desc';
    			}else{
    				$order = 'a.fissuedate desc';
    			}
    			break;
    	}

		// $mediaClass = '';
		// $issueDate = ['2019-07-01','2019-09-26'];//测试

		$tb_tvissue = 'ttvissue_'.date('Y',strtotime($issueDate[0]));
		$tb_bcissue = 'tbcissue_'.date('Y',strtotime($issueDate[0]));
		$tb_paperissue = 'tpaperissue_'.date('Y',strtotime($issueDate[0]));

		//验证表是否存在
		if(empty(S('isTable_'.$tb_tvissue))){
			D('Common/Function','Model')->isIssueTable($tb_tvissue,'01');
			S('isTable_'.$tb_tvissue,'1',86400);
		}
		if(empty(S('isTable_'.$tb_bcissue))){
			D('Common/Function','Model')->isIssueTable($tb_bcissue,'02');
			S('isTable_'.$tb_bcissue,'1',86400);
		}
		if(empty(S('isTable_'.$tb_paperissue))){
			D('Common/Function','Model')->isIssueTable($tb_paperissue,'03');
			S('isTable_'.$tb_paperissue,'1',86400);
		}

		$wherestr = '1=1 ';
		//媒体名称
		if (!empty($mediaName)) {
			$wherestr .= ' and f.fmedianame like "%'.$mediaName.'%"';
		}
		//媒体ID
		if (!empty($mediaId)) {
			$wherestr .= ' and f.fid = '.$mediaId;
		}
		//违法类型
		if((int)$illType != -1){
			$wherestr .= ' and b.fillegaltypecode = '.$illType;
		}
		if(!empty($adName)){
			$wherestr .= ' and c.fadname like "%'.$adName.'%"';
		}
		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$wherestr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$wherestr .= ' and f.media_region_id = '.$area;
			}
        }

		if(!empty($adClass[0])){
			$codes = D('Common/Function')->get_next_tadclasscode($adClass[0],true);//获取下级广告类别代码
			$codes = $codes?$codes:[$adClass[0]];
		}
		if(!empty($codes)){
			$wherestr .= ' and c.fadclasscode  in('.implode(',', $codes).')';
		}

		if(!empty($issueTime) && (in_array($mediaClass, ['01','02']) || empty($mediaClass))){
			$wherestr2 .= ' and DATE_FORMAT(a.fstarttime,"%H:%i:%s") between "'.$issueTime[0].'" and "'.$issueTime[1].'"';
		}
		
		$wherestr .= ' and a.fissuedate between "'.$issueDate[0].'" and "'.$issueDate[1].'"';
		$wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';
		$joins = '
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
			inner join tadowner d on c.fadowner = d.fid
			inner join tadclass e on c.fadclasscode=e.fcode 
			inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1
		';

	    //根据不同的媒体类型，搜索不同的表
	    if(empty($mediaClass)){
	    	$countsql = 'select sum(acount) as acount from (
	    	(select count(*) as acount 
	    		from '.$tb_tvissue.' a 
				inner join ttvsample b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'
				where '.$wherestr.$wherestr2.')
			union all (select count(*) as acount 
				from '.$tb_bcissue.' a 
				inner join tbcsample b on a.fsampleid=b.fid and b.fstate in(0,1)
				'.$joins.' 
				where '.$wherestr.$wherestr2.')
			union all (select count(*) as acount 
				from '.$tb_paperissue.' a 
				inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1)
				'.$joins.' 
				where '.$wherestr.')
			) as a';
			$count = M()->query($countsql);

			$datasql = 'select * from (
	    	(select left(f.fmediaclassid,2) mediaclass,a.fmediaid,e.ffullname fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) fmedianame,b.fillegaltypecode,a.fissuedate, b.fillegalcontent, b.fexpressioncodes, b.fexpressions,a.fid issueid,c.fadname,c.fadclasscode,d.fname fadownername,b.fversion,b.fspokesman,a.fstarttime,a.fendtime,a.flength,"" fpage,"" fpagetype,"" fproportion,fprice,"" fsize,b.favifilename ysscurl 
	    		from '.$tb_tvissue.' a 
				inner join ttvsample b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.'
				where '.$wherestr.$wherestr2.')
			union all (select left(f.fmediaclassid,2) mediaclass,a.fmediaid,e.ffullname fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) fmedianame,b.fillegaltypecode,a.fissuedate, b.fillegalcontent, b.fexpressioncodes, b.fexpressions,a.fid issueid,c.fadname,c.fadclasscode,d.fname fadownername,b.fversion,b.fspokesman,a.fstarttime,a.fendtime,a.flength,"" fpage,"" fpagetype,"" fproportion,fprice,"" fsize,b.favifilename ysscurl  
				from '.$tb_bcissue.' a 
				inner join tbcsample b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$wherestr.$wherestr2.')
			union all (select left(f.fmediaclassid,2) mediaclass,a.fmediaid,e.ffullname as fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,b.fillegaltypecode,a.fissuedate, b.fillegalcontent, b.fexpressioncodes, b.fexpressions,a.fid as issueid,c.fadname,c.fadclasscode,d.fname as fadownername,b.fversion,b.fspokesman,"" fstarttime,"" fendtime,0 flength,a.fpage,a.fpagetype,fproportion,fprice,a.fsize,b.fjpgfilename ysscurl  
				from '.$tb_paperissue.' a 
				inner join tpapersample b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$wherestr.')
			) as a ORDER BY '.$order.$limitstr;
	    }else{
	    	if($mediaClass == '01'){
				$tb_issue = $tb_tvissue;
				$tb_sample = 'ttvsample';
				$fields = ',a.fstarttime,a.fendtime,a.flength,"" fpage,"" fpagetype,"" fproportion,fprice,"" fsize,b.favifilename ysscurl';
	    	}elseif($mediaClass == '02'){
	    		$tb_issue = $tb_bcissue;
	    		$tb_sample = 'tbcsample';
	    		$fields = ',a.fstarttime,a.fendtime,a.flength,"" fpage,"" fpagetype,"" fproportion,fprice,"" fsize,b.favifilename ysscurl';
	    	}elseif($mediaClass == '03' || $mediaClass == '04'){
	    		$tb_issue = $tb_paperissue;
	    		$tb_sample = 'tpapersample';
	    		$fields = ',"" fstarttime,"" fendtime,0 flength,a.fpage,a.fpagetype,fproportion,fprice,a.fsize,b.fjpgfilename ysscurl';
	    	}

	    	$wherestr .= ' and left(f.fmediaclassid,2) ="'.$mediaClass.'"';

	    	$countsql = 'select count(*) as acount 
	    		from '.$tb_issue.' a 
				inner join '.$tb_sample.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$wherestr.$wherestr2;
			$count = M()->query($countsql);

			$datasql = 'select left(f.fmediaclassid,2) mediaclass,a.fmediaid,e.ffullname fadclass,  (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) fmedianame,b.fillegaltypecode,a.fissuedate, b.fillegalcontent, b.fexpressioncodes, b.fexpressions,a.fid issueid,c.fadname,c.fadclasscode,d.fname fadownername,b.fversion,b.fspokesman'.$fields.' 
	    		from '.$tb_issue.' a 
				inner join '.$tb_sample.' b on a.fsampleid=b.fid and b.fstate in(0,1) 
				'.$joins.' 
				where '.$wherestr.$wherestr2.'
				ORDER BY '.$order.$limitstr;
	    }

		$data = M()->query($datasql);

		if(!empty($outtype)){
			if(empty($data)){
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
			}
	        $outdata['datalie'] = [
	          '序号'=>'key',
	          '广告名称'=>'fadname',
	          '广告主'=>'fadownername',
	          '广告内容类别'=>'fadclass',
	          '发布媒体'=>'fmedianame',
	          '媒体类型'=>[
	            'type'=>'zwif',
	            'data'=>[
	              ['{mediaclass} == "01"','电视'],
	              ['{mediaclass} == "02"','广播'],
	              ['{mediaclass} == "03"','报纸'],
	              ['{mediaclass} == "04"','期刊']
	            ]
	          ],
	          '违法类型'=>[
	            'type'=>'zwif',
	            'data'=>[
	              ['{fillegaltype} == 0','不违法'],
	              ['{fillegaltype} == 30','违法'],
	            ]
	          ],
	          '发布日期'=>'fissuedate',
	          '开始时间'=>'fstarttime',
	          '结束时间'=>'fendtime',
	          '版面'=>'',
	          '规格'=>'',
	          '违法表现代码'=>'fexpressioncodes',
	          '违法表现'=>'fexpressions',
	          '涉嫌违法内容'=>'fillegalcontent',
	          '素材地址'=>'ysscurl',
	        ];

	        if(in_array($mediaClass, ['01','02'])){
	        	unset($outdata['datalie']['版面']);
	        	$outdata['datalie']['规格'] = 'flength';
	        }elseif(in_array($mediaClass, ['03','04'])){
	        	unset($outdata['datalie']['开始时间']);
	        	unset($outdata['datalie']['结束时间']);
	        	$outdata['datalie']['版面'] = 'fpage';
	        	$outdata['datalie']['规格'] = 'fsize';
	        }else{
	        	$this->ajaxReturn(array('code'=>1,'msg'=>'媒体类别参数有误'));
	        }

	        $outdata['lists'] = $data;
	        $ret = A('Api/Function')->outdata_xls($outdata);

	        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count[0]['acount'],'data'=>$data));
		}
	}

	/**
	 * 获取监测明细详情
	 * by zw
	 */
	public function getIssueView() {
		$getData = $this->oParam;
		$issueId = $getData['issueId'];//发布记录ID
		$mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）
		$issueDate = $getData['issueDate'];//广告发布日期

		// $issueId = 39271;
		// $mediaClass = '01';
		// $issueDate = '2019-7-6';

		if(empty($issueId)||empty($mediaClass)||empty($issueDate)){
			$this->error('参数缺失!');
		}
		if($mediaClass == '01'){
	        $tb_str = 'tv';
		}elseif($mediaClass == '02'){
			$tb_str = 'bc';
		}elseif($mediaClass == '03' || $mediaClass == '04'){
			$tb_str = 'paper';
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'媒体类别有误'));
		}
		
        $where['_string'] = '1=1';
        $where['a.fid'] = $issueId;

		if($mediaClass == '01' || $mediaClass == '02'){
			$fields = 'a.fid as issueid,
				c.fadname, e.ffullname as fadclass, (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.fid mediaid ,c.fbrand, ifnull(d.fname,"广告主不详") as fadowner_name, fversion,b.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, a.fissuedate, fstarttime, fendtime, fstarttime as fstarttime2, fendtime as fendtime2, a.flength fadlen, favifilename,a.fsampleid';
		}elseif($mediaClass == '03' || $mediaClass == '04'){
			$fields = 'a.fid as issueid,
				c.fadname, e.ffullname as fadclass, (case when instr(f.fmedianame,"（") > 0 then left(f.fmedianame,instr(f.fmedianame,"（") -1) else f.fmedianame end) as fmedianame,f.fid mediaid ,c.fbrand, ifnull(d.fname,"广告主不详") as fadowner_name, fversion,b.fillegaltypecode, fillegalcontent, fexpressioncodes, fexpressions, fconfirmations, fspokesman, fadmanuno, fmanuno, fadapprno, fapprno, fadent, fent, fentzone, a.fissuedate, fpage,fpagetype,fproportion,fprice,fissuetype, fsize, b.fjpgfilename,a.fsampleid';
			$where['left(f.fmediaclassid,2)'] = $mediaClass;
		}
		$data = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))
			->field($fields)
			->alias('a')
			->join('t'.$tb_str.'sample b on a.fsampleid=b.fid and b.fstate in(0,1)')
			->join('tad c on b.fadid=c.fadid and c.fstate in(1,2,9)')
			->join('tadowner d on c.fadowner=d.fid ')
			->join('tadclass e on c.fadclasscode=e.fcode')
			->join('tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and b.fstate in(0,1)')
			->where($where)
			->find();

		$data['fissuedate'] = date('Y-m-d',strtotime($data['fissuedate']));
		$data['mclass'] = $mediaClass;
		$data['fillegalcontent'] = htmlspecialchars_decode($data['fillegalcontent']);
		if($mediaClass == '01' || $mediaClass == '02'){
			$data['issueurl'] = A('Common/Media','Model')->get_m3u8($data['mediaid'],strtotime($data['fstarttime']),strtotime($data['fendtime']));
			$data['fstarttime'] = date('H:i:s',strtotime($data['fstarttime']));
			$data['fendtime'] = date('H:i:s',strtotime($data['fendtime']));
		}
		
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'无数据','data'=>$data));
		}
	}
}
<?php
namespace Jiance\Controller;
use Think\Controller;
import('Vendor.PHPExcel');
class MediaGroupCreditController extends BaseController
{

    //更新相关时段的信用
    public function updateGroupCredit(){

        $getData = $this->oParam;
        $year = $getData['year'] ? $getData['year'] : $this->ajaxReturn(array('code'=>1,'msg'=>'年份必传！'));//年
        $month = $getData['month'] ? $getData['month'] : $this->ajaxReturn(array('code'=>1,'msg'=>'月份必传！'));//月

        M('')->execute(" call SetMediaGroupCredit(".$year.",".$month.") ");
        $this->ajaxReturn(array('code'=>0,'msg'=>'更新成功！'));

    }

    //统计显示相关时段的信用
    public function groupCreditList(){

        $getData = $this->oParam;
        $pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $orders = $getData['orders']?$getData['orders']:0;//0默认，10评价期初分升 11降，20本月信用评价扣分升 21降，30信用评价期末分升 31降
        
        switch ($orders) {
            case 10:
                $orderstr = 'XYPJQCF desc';
                break;
            case 11:
                $orderstr = 'XYPJQCF asc';
                break;
            case 20:
                $orderstr = 'BYXYPJKF desc';
                break;
            case 21:
                $orderstr = 'BYXYPJKF asc';
                break;
            case 30:
                $orderstr = 'XYPJQMF desc';
                break;
            case 31:
                $orderstr = 'XYPJQMF asc';
                break;
            default:
                $orderstr = 'CONVERT(JTMC USING GBK) asc';
                break;
        }

        $limitIndex = ($pageIndex-1)*$pageSize;

        $year = $getData['year'] ? $getData['year'] : $this->ajaxReturn(array('code'=>1,'msg'=>'年份必传！'));//年
        $month = $getData['month'] ? $getData['month'] : $this->ajaxReturn(array('code'=>1,'msg'=>'月份必传！'));//月

        $is_out_excel = $getData['is_out_excel'] ? $getData['is_out_excel'] : 2;//导出excel

        $group_name = $getData['group_name'] ? $getData['group_name'] : null;//集团名称

        $group_type = $getData['group_type'] ? $getData['group_type'] : null;//集团类别，GD广电、BZ报纸

        if($is_out_excel == 1) {
            $pageSize = 9999999999;
            ini_set('memory_limit','3072M');
            set_time_limit(0);
        }

        $map = [];

        $map["N"] = $year;
        $map["Y"] = $month;

        if(!empty($group_type)){
            $map["_string"] = ' JTMC like "'.$group_type.'%"';
        }

        if(!empty($group_name)){
            $map["JTMC"] = ['like',"%".$group_name."%"];
        }

        $count = M('tmedia_group_credit')
            ->where($map)
            ->count();

        $media_credit_list = M("tmedia_group_credit")
            ->field("
                MTLB,
                JTMC,
                MTS,
                N,
                Y,
                GGSL,
                WFGGSL,
                YZWFGGSL,
                GGSLWFL,
                CSWFLKF,
                WFGGSLKF,
                YZWFGGSLKF,
                GGJE,
                WFGGJE,
                GGJEWFL,
                JEWFLKF,
                WFGGJEKF,
                GGSCMJ,
                WFGGSCMJ,
                GGSCMJWFL,
                SCMJWFLKF,
                BYZKF,
                SYZKF,
                BYXYPJKF,
                XYPJQCF,
                XYPJQMF
            ")
            ->where($map)
            ->limit($limitIndex,$pageSize)
            ->order($orderstr.',tmedia_group_credit.ID desc')
            ->select();


        if($is_out_excel == 1){
            $this->ajaxReturn(array('code'=>0,'msg'=>'导出成功！','data'=> $this->out_excel($media_credit_list,$year,$month)));
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功！','count'=>$count,'data' => $media_credit_list));

    }

    //导出EXCEL
    public function out_excel($data,$year,$month){

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $sheet = $objPHPExcel->setActiveSheetIndex(0);


        $sheet ->mergeCells("A1:G1");//基本
        $sheet ->mergeCells("H1:N1");//条次
        $sheet ->mergeCells("O1:S1");//金额
        $sheet ->mergeCells("T1:W1");//时长面积
        $sheet ->mergeCells("X1:AB1");//结果

        $sheet ->setCellValue('A1','基本 ('.$year.'年'.$month.'月)');
        $sheet ->setCellValue('H1','条次');
        $sheet ->setCellValue('O1','金额');
        $sheet ->setCellValue('T1','时长面积');
        $sheet ->setCellValue('X1','结果');
        $sheet ->setCellValue('A2','序号');
        $sheet ->setCellValue('B2','媒体类别');
        $sheet ->setCellValue('C2','集团名称');
        $sheet ->setCellValue('D2','媒体数');
        $sheet ->setCellValue('E2','条次');
        $sheet ->setCellValue('F2','违法条次');
        $sheet ->setCellValue('G2','严重违法条次');
        $sheet ->setCellValue('H2','条次违法率');
        $sheet ->setCellValue('I2','条次违法率扣分');
        $sheet ->setCellValue('J2','违法条次扣分');
        $sheet ->setCellValue('K2','严重违法条次扣分');
        $sheet ->setCellValue('L2','金额');
        $sheet ->setCellValue('M2','违法金额');
        $sheet ->setCellValue('N2','金额违法率');
        $sheet ->setCellValue('O2','金额违法率扣分');
        $sheet ->setCellValue('P2','违法金额扣分');
        $sheet ->setCellValue('Q2','时长面积');
        $sheet ->setCellValue('R2','违法时长面积');
        $sheet ->setCellValue('S2','时长面积违法率');
        $sheet ->setCellValue('T2','时长面积违法率扣分');
        $sheet ->setCellValue('U2','本月总扣分');
        $sheet ->setCellValue('V2','上月总扣分');
        $sheet ->setCellValue('W2','本月评价扣分');
        $sheet ->setCellValue('X2','评价期初分');
        $sheet ->setCellValue('Y2','评价期末分');

        foreach ($data as $key => $value) {

            $sheet ->setCellValue('A'.($key+3),$key+1);//序号
            $sheet ->setCellValue('B'.($key+3),$value['MTLB']);//分类
            $sheet ->setCellValue('C'.($key+3),$value['JTMC']);//集团名称
            $sheet ->setCellValue('D'.($key+3),$value['MTS']);//媒体数
            $sheet ->setCellValue('E'.($key+3),$value['GGSL']);//条次
            $sheet ->setCellValue('F'.($key+3),$value['WFGGSL']);//违法条次
            $sheet ->setCellValue('G'.($key+3),$value['YZWFGGSL']);//严重违法条次
            $sheet ->setCellValue('H'.($key+3),$value['GGSLWFL']."%");//条次违法率
            $sheet ->setCellValue('I'.($key+3),$value['CSWFLKF']);//条次违法率扣分
            $sheet ->setCellValue('J'.($key+3),$value['WFGGSLKF']);//违法条次扣分
            $sheet ->setCellValue('K'.($key+3),$value['YZWFGGSLKF']);//严重违法条次扣分
            $sheet ->setCellValue('L'.($key+3),$value['GGJE']);//金额
            $sheet ->setCellValue('M'.($key+3),$value['WFGGJE']);//违法金额
            $sheet ->setCellValue('N'.($key+3),$value['GGJEWFL']."%");//金额违法率
            $sheet ->setCellValue('O'.($key+3),$value['JEWFLKF']);//金额违法率扣分
            $sheet ->setCellValue('P'.($key+3),$value['WFGGJEKF']);//违法金额扣分
            $sheet ->setCellValue('Q'.($key+3),$value['GGSCMJ']);//时长面积
            $sheet ->setCellValue('R'.($key+3),$value['WFGGSCMJ']);//违法时长面积
            $sheet ->setCellValue('S'.($key+3),$value['GGSCMJWFL']."%");//时长面积违法率
            $sheet ->setCellValue('T'.($key+3),$value['SCMJWFLKF']);//时长面积违法率扣分
            $sheet ->setCellValue('U'.($key+3),$value['BYZKF']);//本月总扣分
            $sheet ->setCellValue('V'.($key+3),$value['SYZKF']);//上月总扣分
            $sheet ->setCellValue('W'.($key+3),$value['BYXYPJKF']);//本月评价扣分
            $sheet ->setCellValue('X'.($key+3),$value['XYPJQCF']);//评价期初分
            $sheet ->setCellValue('Y'.($key+3),$value['XYPJQMF']);//评价期末分

        }


        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis');
        $savefile = './Public/Word/'.$date.'.xlsx';
        $savefile2 = '/Public/Word/'.$date.'.xlsx';
        $objWriter->save($savefile);
        //即时导出下载
        /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                    header('Cache-Control:max-age=0');
                    $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save( 'php://output');*/
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        return $ret['url'];
    }

    /**
     * 集团媒体信用列表
     * User: Limboy
     * Time: 2020/4/27 17:11
     */
    public function groupMediaCreditList()
    {
        $getData = $this->oParam;
        $group_name = $getData['group_name'] ? $getData['group_name'] : $this->ajaxReturn(['code' => 1, 'msg' => '集团名称必传']);//集团名称
        $year = $getData['year'] ? $getData['year'] : $this->ajaxReturn(array('code'=>1,'msg'=>'年份必传！'));//年
        $month = $getData['month'] ? $getData['month'] : $this->ajaxReturn(array('code'=>1,'msg'=>'月份必传！'));//月
        $pageIndex = $getData['pageIndex'] ? $getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize'] ? $getData['pageSize'] : 20;//每页显示数
        $pageOffset = ($pageIndex - 1) * $pageSize;
        $media_class = $getData['media_class'] ? $getData['media_class'] : null;//媒介类型
        $media_id = $getData['mediaId'] ? $getData['mediaId'] : null;//媒体id

        $map = [];
        $map["N"] = $year;
        $map["Y"] = $month;
        $map["b.fisxinyong"] = 1;
        if (!empty($media_class)) {
            $map["LEFT (b.fmediaclassid, 2)"] = $media_class;
        }

        if (!empty($media_id)) {
            $map["b.fid"] = $media_id;
        }

        if (!empty($group_name)) {
            $map["b.fmedia_group"] = $group_name;
        }

        // 总记录数
        $count = M('tmedia_credit a')
            ->join("tmedia b ON a.fmedia_id = b.fid and b.fstate = 1")
            ->where($map)
            ->order('a.id desc')
            ->count();
        $count = !empty($count) ? $count : 0;

        // 数据列表
        $data = M("tmedia_credit a")
            ->field("
                b.fid as MEDIAID,
                d.fclass as MEDIACLASS,
                (case when instr(fmedianame,'（') > 0 then left(fmedianame,instr(fmedianame,'（') -1) else fmedianame end) as MEDIANAME,
                c.fid as REGIONID,
                c.fname1 as REGIONNAME,
                b.fmedia_group as JTMC,
                a.N,
                a.Y,
                a.TJTS,
                a.ZHTS,
                a.GGSL,
                a.WFGGSL,
                a.YZWFGGSL,
                a.GGSLWFL,
                a.CSWFLKF,
                a.WFGGSLKF,
                a.YZWFGGSLKF,
                a.GGJE,
                a.WFGGJE,
                a.GGJEWFL,
                a.JEWFLKF,
                a.WFGGJEKF,
                a.GGSCMJ,
                a.WFGGSCMJ,
                a.GGSCMJWFL,
                a.SCMJWFLKF,
                a.BYZKF,
                a.SYZKF,
                a.BYXYPJKF,
                a.XYPJQCF,
                a.XYPJQMF
            ")
            ->join("tmedia b ON a.fmedia_id = b.fid and b.fstate = 1")
            ->join("tregion c ON c.fid = b.media_region_id")
            ->join("tmediaclass d ON d.fid = LEFT (b.fmediaclassid, 2)")
            ->where($map)
            ->limit($pageOffset, $pageSize)
            ->order('a.id desc')
            ->select();

        $this->ajaxReturn(['code' => 0, 'msg' => '获取成功！', 'count' => $count, 'data' => $data]);
    }

}

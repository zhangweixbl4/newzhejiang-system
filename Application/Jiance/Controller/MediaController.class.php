<?php
namespace Jiance\Controller;
use Think\Controller;
class MediaController extends BaseController
{

    //媒体列表
    public function mediaList(){

        $getData = $this->oParam;
        $pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $mediaType = $getData['mediaType']!=''?$getData['mediaType']:-1;//媒体所属（1串播单，2非串播单，3省级，4市级，5区县级,-1默认）
        $mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）
        $mediaName = $getData['mediaName'];//媒体名
        $area = $getData['area'];//行政区划
        $iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区，1包含，0不包含，不传默认1

        $where_mie['_string'] = '1=1';
        //媒体所属
        switch ((int)$mediaType) {
            case 0:
                $where_mie['_string'] .= ' and b.fid in (select fmediaid from tmedialabel where flabelid in(446,449))';
                break;
            case 1:
                $where_mie['_string'] .= ' and b.fid not in (select fmediaid from tmedialabel where flabelid in(446,449))';
                break;
            case 2:
                $where_mie['f.flevel'] = 1;
                break;
            case 3:
                $where_mie['f.flevel'] = ['in',[2,3,4]];
                break;
            case 4:
                $where_mie['f.flevel'] = 5;
                break;
        }
        //地域
        if(!empty($area)){
            if(!empty($iscontain)){
                $tregion_len = get_tregionlevel($area);
                if($tregion_len == 2){//省级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,2).'%'];
                }elseif($tregion_len == 4){//市级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,4).'%'];
                }elseif($tregion_len == 6){//县级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,6).'%'];
                }
            }else{
                $where_mie['b.media_region_id'] = $area;
            }
        }
        //媒体类型
        if(!empty($mediaClass)){
            $where_mie['left(b.fmediaclassid,2)'] = $mediaClass;
        }
        //媒体名称
        if(!empty($mediaName)){
            $where_mie['b.fmedianame'] = ['like','%'.$mediaName.'%'];
        }

        $where_mie['b.fstate'] = 1;

        $count = M('tmedia')
            ->alias('b')
            ->join('tregion f on b.media_region_id = f.fid')
            ->where($where_mie)
            ->count();

        $data = M('tmedia')
            ->alias('b')
            ->field('b.fid,(case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) fmedianame,left(b.fmediaclassid,2) media_class,f.fname regionname')
            ->join('tregion f on b.media_region_id = f.fid')
            ->where($where_mie)
            ->limit($limitstr)
            ->order('f.fid asc,left(b.fmediaclassid,2) asc,b.fid asc')
            ->select();
       
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
    }

    /**
    * 获取媒体列表
     * by zw
     */
    public function get_medialist(){
        $getData = $this->oParam;
        $system_num = getconfig('system_num');

        $p = $getData['page']?$getData['page']:1;//当前第几页
        $pp = 20;//每页显示多少记录
        $mclass = $getData['mclass'];//媒体类型
        $netadtype = $getData['netadtype'];//平台类别
        $medianame = $getData['medianame'];//媒体名称
        $fmedia_group = $getData['fmedia_group'];//集团名称
        $regionid = $getData['regionid'];//区划ID
        $fstate = $getData['fstate'];//状态
        $outtype = $getData['outtype'];//导出类型
        $orders = $getData['orders'];//排序方式
        $fisxinyong = $getData['fisxinyong'] == ''?-1:$getData['fisxinyong'];//是否信用评价，-1全部默认，0否，1是
        
        if(empty($outtype)){
            $limitstr = ($p-1)*$pp.','.$pp;
        }

        switch ($orders) {
            case 1:
                $order = 'a.media_region_id desc,left(fmediaclassid,2) asc';
                break;
            case 10:
                $order = 'CONVERT(a.fmedianame using gbk)  asc';
                break;
            case 11:
                $order = 'CONVERT(a.fmedianame using gbk) desc';
                break;
            case 20:
                $order = 'CONVERT(a.fmedia_group using gbk) asc';
                break;
            case 21:
                $order = 'CONVERT(a.fmedia_group using gbk) desc';
                break;
            case 30:
                $order = 'left(fmediaclassid,2) asc';
                break;
            case 31:
                $order = 'left(fmediaclassid,2) desc';
                break;
            case 40:
                $order = 'a.fisxinyong desc';
                break;
            case 41:
                $order = 'a.fisxinyong asc';
                break;
            case 50:
                $order = 'a.fstate desc';
                break;
            case 51:
                $order = 'a.fstate asc';
                break;
            default:
                $order = 'a.media_region_id asc,left(fmediaclassid,2) asc';
                break;
        }

        $where_ta['_string'] = '1=1';
        if(!empty($mclass)){
            $where_ta['_string'] .= ' and left(fmediaclassid,2)="'.$mclass.'"';
        }
        if($fstate != ''){
            $where_ta['a.fstate'] = $fstate;
        }
        if(!empty($netadtype)){
            if($netadtype==1){
                $where_ta['_string'] .= ' and left(fmediaclassid,4)="1301"';
            }elseif($netadtype==2){
                $where_ta['_string'] .= ' and left(fmediaclassid,4)="1302"';
            }elseif($netadtype==9){
                $where_ta['_string'] .= ' and left(fmediaclassid,4)="1303"';
            }
        }
        if(!empty($medianame)){
            $where_ta['a.fmedianame'] = array('like','%'.$medianame.'%');
        }
        if(!empty($fmedia_group)){
            $where_ta['a.fmedia_group'] = array('like','%'.$fmedia_group.'%');
        }
        if(!empty($regionid)){
            $where_ta['a.media_region_id'] = $regionid;
        }
        $where_ta['a.fmediaownerid'] = array('neq',0);
        if($fisxinyong == 0){
            $where_ta['fisxinyong'] = 0;
        }elseif($fisxinyong == 1){
            $where_ta['fisxinyong'] = 1;
        }
        $where_ta['_string'] .= ' and a.fid=a.main_media_id and a.fstate in(0,1)';
        
        $count = M('tmedia')
            ->alias('a')
            ->join('tregion c on a.media_region_id=c.fid')
            ->where($where_ta)
            ->count();// 查询满足要求的总记录数

        $data = M('tmedia')
            ->alias('a')
            ->field('a.fid,a.fmediacode,a.fdpid,(case when instr(a.fmedianame,"（") > 0 then left(a.fmedianame,instr(a.fmedianame,"（") -1) else a.fmedianame end) as fmedianame,a.fcollecttype,c.ffullname,(case when left(fmediaclassid,4)="1301" then "PC端" when left(fmediaclassid,4)="1302" then "移动端" when left(fmediaclassid,4)="1303" then "微信端" end) as mediaclassname,left(fmediaclassid,2) mclass,fisxinyong, fmedia_group,a.fstate, a.media_region_id')
            ->join('tregion c on a.media_region_id=c.fid')
            ->where($where_ta)
            ->order($order.',a.fid asc')
            ->limit($limitstr)
            ->select();//查询复核数据

        if(!empty($outtype)){
            if(empty($data)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
            }
            $outdata['datalie'] = [
              '序号'=>'key',
              '所属行政区划'=>'ffullname',
              '媒体名称'=>'fmedianame',
              '媒体代码'=>'fmediacode',
              '集团名称'=>'fmedia_group',
              '媒体类型'=>[
                'type'=>'zwif',
                'data'=>[
                  ['{mclass} == "01"','电视'],
                  ['{mclass} == "02"','广播'],
                  ['{mclass} == "03"','报纸'],
                  ['{mclass} == "04"','期刊']
                ]
              ],
              '信用评价'=>[
                'type'=>'zwif',
                'data'=>[
                  ['{fisxinyong} == "1"','是'],
                  ['{fisxinyong} == "0"','否'],
                ]
              ],
              '状态'=>[
                'type'=>'zwif',
                'data'=>[
                  ['{fstate} == "1"','有效'],
                  ['{fstate} == "0"','无效'],
                ]
              ],
            ];

            $outdata['lists'] = $data;
            $ret = A('Api/Function')->outdata_xls($outdata);
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('count'=>$count,'list'=>$data)));
        }

    }

    /**
    * 修改媒体
     * by zw
     */
    public function editMediaAction(){
        $getData = $this->oParam;

        $fid = $getData['fid'];//媒体ID
        $fisxinyong = $getData['fisxinyong'];//是否信用媒体
        $fmedia_group = $getData['fmedia_group'];//集团名称
        $fstate = $getData['fstate'];//是否有效
        $media_region_id = $getData['media_region_id'];//所属行政区划

        $saveMa = M('tmedia')->where(['fid' => $fid])->save(['fisxinyong' => $fisxinyong, 'fstate' => $fstate, 'fmedia_group' => $fmedia_group, 'media_region_id' => $media_region_id]);
        if (!empty($saveMa)) {
            $this->ajaxReturn(array('code' => 0, 'msg' => '修改成功'));
        } else {
            $this->ajaxReturn(array('code' => 0, 'msg' => '未作更改，修改失败'));
        }
    }

}

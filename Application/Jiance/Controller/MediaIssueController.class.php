<?php
namespace Jiance\Controller;
use Think\Controller;
class MediaIssueController extends BaseController {

	/*
	* 任务处理时的任务退回
	* by zw
	*/
	public function mediaIssueTaskBackAction(){
		$getData = $this->oParam;
        $mainId = $getData['mainId'];//任务主键（fid）
        $backContent = $getData['backContent'];//退回原因
        if(empty($backContent)){//退回验证
        	$this->ajaxReturn(array('code'=>1,'msg'=>'请输入退回原因'));
        }

        //流程关系
        $flowCode = [
        	'0' => 'mediaissue_ggfl',//广告分离环节直接退回
        	'mediaissue_ggfl' => 'mediaissue_ggjc',//广告监测环节直接退回
        	'mediaissue_ggjc' => 'mediaissue_ggfs',
        	'mediaissue_ggfs' => 'mediaissue_ggzs'
        ];
        //流程名
        $flowName = [
        	'mediaissue_ggfl' => '广告分离',
        	'mediaissue_ggjc' => '广告监测',
        	'mediaissue_ggfs' => '广告复审',
        	'mediaissue_ggzs' => '广告终审'
        ];

		$where_mie['a.fid'] = $mainId;
		$where_mie['a.fstatus'] = 1;
		
		$data = M('tmediaissue')
        	->alias('a')
        	->field('a.fid,a.fissue_date,a.fmedia_id,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,d.flowid,c.fid taskid,d.fflow_code,b.fmediaclassid')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('tregion f on b.media_region_id = f.fid')
        	->join('ttask c on a.fid = c.fbiz_main_id','left')
        	->join('(select fflow_code,fflow,ftask_id,fid flowid from ttask_flow where fstatus = 0) d on c.fid = d.ftask_id','left')
        	->where($where_mie)
        	->find();
        if(!empty($data)){
            $mclass = substr($data['fmediaclassid'], 0, 2);
        	$save_flow = M('ttask_flow')->where(['fid'=>$data['flowid']])->save(['fstatus'=>2,'ffinish_type'=>2,'frece_id'=>session('personData.fid'),'frece'=>session('personData.fname'),'frece_time'=>date('Y-m-d H:i:s')]);
    		$searchKey = array_keys($flowCode,$data['fflow_code']);
    		if(!empty($save_flow)){
				if(!empty($searchKey[0]) && !($searchKey[0] == 'mediaissue_ggfl' && ($mclass == '01' || $mclass == '02'))){//如果有上一流程时退回上一流程
					$newflow_data['ftask_id'] = $data['taskid'];
		        	$newflow_data['fflow_code'] = $searchKey[0];
		        	$newflow_data['fflow'] = $flowName[$searchKey[0]];
		        	$newflow_data['fsend_task_flow_id'] = $data['flowid'];
		        	$newflow_data['fsend_id'] = session('personData.fid');
		        	$newflow_data['fsend'] = session('personData.fname');
		        	$newflow_data['fsend_msg'] = $backContent;
		        	$newflow_data['fsend_time'] = date('Y-m-d H:i:s');
		        	$newflow_data['fstatus'] = 0;
		        	$add_newflow = M('ttask_flow')->add($newflow_data);
		        	$this->ajaxReturn(array('code'=>0,'msg'=>'已退回上一处理环节'));
	        	}else{
	        		$pushData['taskid'] = $mainId;
	        		$pushData['fstatus'] = 10;
	        		$pushData['userData'] = ['fupdate_userid'=>session('personData.fid'),'fupdate_user'=>session('personData.fname')];
	        		$pushData['returnmsg'] = session('personData.fname').'于'.date('Y-m-d H:i:s').'将数据退回，原因：'.$backContent.'；';
	        		$ret = D('MediaIssue','Model')->updateTaskStatus($pushData);//退回任务更新
                    if(!empty($ret)){
                        D('Common/Function')->adIssueSendState($data['fmedia_id'],$data['fissue_date'],0,0);//更改数据发布状态
                    }
                    //退回推送
                    $backurl = 'http://'.C('ZJServerUrl').'/Api/ZJMediaIssue/getBackData';
                    $actionData[$data['fmedianame']]['fmedia_id'] = $data['fmedia_id'];
                    $actionData[$data['fmedianame']]['fissue_date'][] = $data['fissue_date'];
                    $backData['taskData'] = $actionData;
                    $backData['backContent'] = $backContent;
                    http($backurl,json_encode($backData),'POST',false,0);
                    if(!empty($ret) && !in_array($_SERVER['HTTP_HOST'],C('TESTSERVER'))){
                        $smsphone = [13967174397,13717722033];//提醒人员，周佳牛、李伟
                        $token = 'f01671bd9464292ce29c4936336baa36e12b5a742f1cca55b6625d0c5fea8d27';
                        push_ddtask('浙江监测平台',"> 数据退回提醒：\n\n媒体".$data['fmedianame']."（".$data['fmedia_id']."）的".$data['fissue_date']."数据因".$backContent."被退回，请注意处理。\n\n",$smsphone,$token,'markdown');
                    }

                    //自动退回此后连续7天未处理的任务
                    $s_date = date('Y-m-d',(strtotime($data['fissue_date'])+86400*1));
                    $e_date = date('Y-m-d',(strtotime($data['fissue_date'])+86400*8));
                    if($mclass == '01' || $mclass == '02'){
                        $th_flowcode = ['mediaissue_ggfl','mediaissue_ggjc'];
                    }else{
                        $th_flowcode = ['mediaissue_ggfl'];
                    }
                    $doTask = M('tmediaissue')
                        ->alias('a')
                        ->field('a.fid,a.fmedia_id,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,a.fissue_date,d.flowid,c.fid taskid,d.fflow_code')
                        ->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
                        ->join('tregion f on b.media_region_id = f.fid')
                        ->join('ttask c on a.fid = c.fbiz_main_id','left')
                        ->join('(select fflow_code,fflow,ftask_id,fid flowid from ttask_flow where fstatus = 0) d on c.fid = d.ftask_id','left')
                        ->where(['d.fflow_code'=>['in',$th_flowcode],'a.fmedia_id'=>$data['fmedia_id'],'a.fissue_date'=>['between',[$s_date,$e_date]],'a.fstatus'=>1])
                        ->select();
                    $actionData[$data['fmedianame']]['fissue_date'] = [];
                    foreach ($doTask as $key => $value) {
                        $pushData['taskid'] = $value['fid'];
                        $pushData['fstatus'] = 10;
                        $pushData['userData'] = ['fupdate_userid'=>session('personData.fid'),'fupdate_user'=>session('personData.fname')];
                        $pushData['returnmsg'] = 'AI于'.date('Y-m-d H:i:s').'将数据退回，原因：'.$data['fissue_date'].'任务因'.$backContent.'被'.session('personData.fname').'退回，所以此后连续7天未处理的任务自动退回进行重新检查；';
                        $ret = D('MediaIssue','Model')->updateTaskStatus($pushData);//退回任务更新
                        if(!empty($ret)){
                            D('Common/Function')->adIssueSendState($value['fmedia_id'],$value['fissue_date'],0,0);//更改数据发布状态
                        }
                        $actionData[$data['fmedianame']]['fissue_date'][] = $value['fissue_date'];
                    }
                    $backData['taskData'] = $actionData;
                    $backData['backContent'] = 'AI于'.date('Y-m-d H:i:s').'将数据退回，原因：'.$data['fissue_date'].'任务因'.$backContent.'被'.session('personData.fname').'退回，所以此后连续7天未处理的任务自动退回进行重新检查；';
                    http($backurl,json_encode($backData),'POST',false,0);
                    if(!empty($actionData[$data['fmedianame']]['fissue_date']) && !in_array($_SERVER['HTTP_HOST'],C('TESTSERVER'))){
                        push_ddtask('浙江监测平台',"> 数据退回提醒：\n\n媒体".$data['fmedianame']."（".$data['fmedia_id']."）的".implode('、', $actionData[$data['fmedianame']]['fissue_date'])."数据因".$data['fissue_date']."任务出于".$backContent."原因被".session('personData.fname')."退回，所以此后连续7天未处理的任务自动退回进行重新检查。\n\n",$smsphone,$token,'markdown');
                    }
	        		
	        		$this->ajaxReturn(array('code'=>0,'msg'=>'已退回数据供应方'));
	        	}
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'退回失败，请勿重复操作'));
			}
			
        }else{
        	$this->ajaxReturn(array('code'=>1,'msg'=>'退回失败，请勿重复操作'));
        }
	}
	
	/*
	* by zw
	* 获取广告监测任务列表
	*/
	public function mediaIssueList(){
		$getData = $this->oParam;
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
        $mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）
        $mediaName = $getData['mediaName'];//媒体名
        $mediaId = $getData['mediaId'];//媒体ID
        $taskTime = $getData['taskTime'];//任务日期范围，数组（开始和结束日期）
        $taskType = $getData['taskType']?$getData['taskType']:'ggjc';//任务类别（传值，ggfl广告分离，ggjc 广告监测，ggfs 广告复审，ggzs广告终审,，bzfl 报纸分离）
        $area = $getData['area'];//行政区划
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
		$searchTaskType = $getData['searchTaskType'];//所在环节
        $outtype = $getData['outtype'];//导出类型
        $orders = $getData['orders'];//排序方式

        if(empty($taskType)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        }

        if(empty($outtype)){
            $limitstr = ($pageIndex-1)*$pageSize.','.$pageSize;
        }

        switch ($orders) {
            case 1:
                if(in_array($mediaClass, ['03','04'])){
                    $order = 'a.fissue_date desc';
                }else{
                    $order = 'a.fissue_date asc';
                }
                break;
            case 10:
                $order = 'CONVERT(b.fmedianame using gbk)  asc';
                break;
            case 11:
                $order = 'CONVERT(b.fmedianame using gbk) desc';
                break;
            case 20:
                $order = 'CONVERT(d.fflow using gbk)  asc';
                break;
            case 21:
                $order = 'CONVERT(d.fflow using gbk) desc';
                break;
            default:
                if(in_array($mediaClass, ['03','04'])){
                    $order = 'a.fissue_date asc';
                }else{
                    $order = 'a.fissue_date desc';
                }
                break;
        }

        //地域
        if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$where_mie['b.media_region_id'] = ['like',substr($area,0,2).'%'];
				}elseif($tregion_len == 4){//市级
					$where_mie['b.media_region_id'] = ['like',substr($area,0,4).'%'];
				}elseif($tregion_len == 6){//县级
					$where_mie['b.media_region_id'] = ['like',substr($area,0,6).'%'];
				}
			}else{
				$where_mie['b.media_region_id'] = $area;
			}
        }
        //媒体类型
        if(!empty($mediaClass)){
        	$where_mie['left(fmediaclassid,2)'] = $mediaClass;
        }
        //媒体名称
        if(!empty($mediaName)){
        	$where_mie['fmedianame'] = ['like','%'.$mediaName.'%'];
        }
        if(!empty($mediaId)){
        	$where_mie['b.fid'] = $mediaId;
		}
        //任务日期范围
        if(!empty($taskTime)){
        	$where_mie['fissue_date'] = ['between',[$taskTime[0],$taskTime[1]]];
        }

    	if($taskType == 'gghd'){
        	$where_mie['c.fstatus'] = ['in',[1,5]];
        	if(!empty($searchTaskType)){
        		if($searchTaskType == 'gghd'){
	        		$where_mie['c.fstatus'] = 5;
        		}else{
	        		$where_mie['d.fflow_code'] = 'mediaissue_'.$searchTaskType;
	        		$where_mie['c.fstatus'] = 1;
        		}
        	}
        }else{
	        $where_mie['c.fstatus'] = 1;
        	$where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        	$where_mie['d.fstatus'] = 0;
        }
        $where_mie['a.fstatus'] = 1;
        if(!empty(session('personData.fid'))){
            $where_mie['_string'] = ' b.fid in (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = "'.C('FORCE_NUM_CONFIG_JIANCE').'" and fuserid='.session('personData.fid').')';
        }

        $count = M('tmediaissue')
        	->alias('a')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('ttask c on a.fid = c.fbiz_main_id')
        	->join('(select * from ttask_flow n where fid = (select max(fid) fid from ttask_flow m where m.ftask_id = n.ftask_id group by ftask_id)) d on c.fid = d.ftask_id')
        	->where($where_mie)
        	->count();

        $data = M('tmediaissue')
        	->alias('a')
        	->field('a.fid,a.fmedia_id,a.fissue_date,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,left(b.fmediaclassid,2) fmediaclassid,d.fflow_code,d.fflow,c.fstatus')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('ttask c on a.fid = c.fbiz_main_id')
        	->join('(select * from ttask_flow n where fid = (select max(fid) fid from ttask_flow m where m.ftask_id = n.ftask_id group by ftask_id)) d on c.fid = d.ftask_id')
        	->where($where_mie)
        	->order($order.',a.fid desc')
        	->limit($limitstr)
        	->select();
        foreach ($data as $key => $value) {
        	if($value['fstatus'] == 5){
        		$data[$key]['fflow'] = '已归档';
        	}
        }

        if(!empty($outtype)){
            if(empty($data)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'暂无数据'));
            }
            $outdata['datalie'] = [
              '序号'=>'key',
              '媒体名称'=>'fmedianame',
              '任务环节'=>'fflow',
              '发布日期'=>'fissue_date',
              '媒体类型'=>[
                'type'=>'zwif',
                'data'=>[
                  ['{fmediaclassid} == "01"','电视'],
                  ['{fmediaclassid} == "02"','广播'],
                  ['{fmediaclassid} == "03"','报纸'],
                  ['{fmediaclassid} == "04"','期刊']
                ]
              ],
            ];

            $outdata['lists'] = $data;
            $ret = A('Api/Function')->outdata_xls($outdata);

            $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
        }
	}

	/*
	* by zw
	* 获取任务的发布数据列表
	*/
	public function issueDataList(){
		$getData = $this->oParam;
		$outtype = $getData['outtype']?$getData['outtype']:0;//导出类型
		if(empty($outtype)){
			$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        	$pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
		}else{
			$pageIndex = 1;
			$pageSize = 9999;
		}
		
        $limitIndex = ($pageIndex-1)*$pageSize;
        $mediaId = $getData['mediaId'];//媒体ID
        $issueDate = $getData['issueDate'];//任务日期，数组
        $taskType = $getData['taskType']?$getData['taskType']:'ggjc';//任务类别（传值，ggfl广告分离，ggjc 广告监测，ggfs 广告复审，ggzs广告终审,，bzfl 报纸分离）
        $dataType = $getData['dataType']?$getData['dataType']:1;//数据类别（传值，1 全部发布广告，2 播出广告，3 新广告及违法广告）
        $adType = $getData['adType']?$getData['adType']:-1;//节目类型，（传值，1 广告，2 节目，-1 全部）
        $orderType = $getData['orderType'];//排序方式，（传值，0默认时间，1 广告名，2 节目类别）
        // $mediaId = 11000010002362;
        // $issueDate = ['2019-10-08 00:00:00','2019-10-08 03:59:59'];

        //任务详情
        $where_mie['a.fmedia_id'] = $mediaId;
        $where_mie['a.fissue_date'] = date('Y-m-d',strtotime($issueDate[0]));
        if($taskType == 'gghd'){
        	$where_mie['c.fstatus'] = ['in',[1,5]];
        }else{
	        $where_mie['c.fstatus'] = 1;
        	$where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
            $where_mie['d.fstatus'] = 0;
        }
        $where_mie['a.fstatus'] = 1;
        $do_mie = M('tmediaissue')
        	->alias('a')
        	->field('b.fmediaclassid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('ttask c on a.fid = c.fbiz_main_id')
        	->join('ttask_flow d on c.fid = d.ftask_id')
        	->where($where_mie)
        	->find();

        if(empty($do_mie)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'任务不存在'));
        }

        //根据媒体类型获取相关表
        $mclass = substr($do_mie['fmediaclassid'],0,2);
		if($mclass == '01'){
			$tb_str = 'tv';
		}elseif($mclass == '02'){
			$tb_str = 'bc';
		}elseif($mclass == '03' || $mclass == '04'){
			$tb_str = 'paper';
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
		}

		switch ($orderType) {
    		case 1:
    			if($mclass == '01' || $mclass == '02'){
	    			$order = 'a.fstarttime desc';
    			}else{
    				$order = 'a.fissuedate desc';
    			}
    			break;
    		case 10:
    			$order = 'CONVERT(c.fadname using gbk)  asc';
    			break;
    		case 11:
    			$order = 'CONVERT(c.fadname using gbk) desc';
    			break;
    		case 20:
    			$order = 'c.fadclasscode asc';
    			break;
    		case 21:
    			$order = 'c.fadclasscode desc';
    			break;
    		case 30:
    			$order = 'a.flength asc';
    			break;
    		case 31:
    			$order = 'a.flength desc';
    			break;
    		case 40:
    			$order = 'b.fillegaltypecode asc';
    			break;
    		case 41:
    			$order = 'b.fillegaltypecode desc';
                break;
            case 50:
                $order = 'CONVERT(a.fpage using gbk) asc';
                break;
            case 51:
                $order = 'CONVERT(a.fpage using gbk) desc';
                break;
            case 60:
                $order = 'CONVERT(a.fpagetype using gbk) asc';
                break;
            case 61:
                $order = 'CONVERT(a.fpagetype using gbk) desc';
                break;
            case 70:
                $order = 'a.fquantity asc';
                break;
            case 71:
                $order = 'a.fquantity desc';
                break;
            case 80:
                $order = 'CONVERT(a.fissuetype using gbk) asc';
                break;
            case 81:
                $order = 'CONVERT(a.fissuetype using gbk) desc';
                break;
    		default:
    			if($mclass == '01' || $mclass == '02'){
	    			$order = 'a.fstarttime asc';
    			}else{
    				$order = 'a.fissuedate asc';
    			}
    			break;
    	}

		$where_issue['a.fstate'] = 1;//发布数据必须是有效的

		//需要读取的字段
		$fields = '1 adType,a.fid issueid,a.fissuedate,a.fsampleid,b.fadid,b.fillegaltypecode,b.fillegalcontent,b.fexpressioncodes,b.fapprno,b.fmanuno,b.fspokesman,b.fexpressions,b.fversion,c.fadowner,c.fadname,c.fbrand,c.fadclasscode,d.ffullname fadclassname,e.fname fadownername,a.fself,b.fissuedate sampleissuedate,b.fcreatetime samplecreatetime';
    	if($mclass == '03' || $mclass == '04'){
    		$fields2 = ',a.fpage,a.fpagetype,a.fsize,b.fjpgfilename,b.sourceid,a.fissuetype,a.fquantity,a.fproportion';
    	}else{
        	$fields2 = ',a.fendtime,a.flength,b.is_long_ad,a.fstarttime,b.favifilename';
    	}

        if($dataType == 1){
        	$count1 = 0;
        	$count2 = 0;
        	$data1 = [];
        	$data2 = [];
        	if($adType == -1 || $adType == 1){
        		if($mclass=='01' || $mclass == '02'){
					$where_issue['a.fstarttime'] = ['between',[$issueDate[0],$issueDate[1]]];
					$limits = "";
	        	}else{
	        		$limits = "$limitIndex,$pageSize";
	        	}
				$where_issue['a.fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
				$where_issue['a.fmediaid'] = $mediaId;
				$where_issue['a.fstate'] = 1;

				$count1 = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])))
		        	->alias('a')
		        	->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)')
		        	->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
		        	->join('tadclass d on c.fadclasscode = d.fcode')
		        	->join('tadowner e on c.fadowner = e.fid')
					->where($where_issue)
	        		->count();

				$data1 = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])))
					->alias('a')
					->field($fields.$fields2)
					->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)')
					->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
					->join('tadclass d on c.fadclasscode = d.fcode')
					->join('tadowner e on c.fadowner = e.fid')
					->where($where_issue)
					->order($order)
					->limit($limits)
					->select();
        	}
        	if(($adType == -1 || $adType == 2) && ($mclass=='01' || $mclass == '02')){
        		$where_prog['a.fstart'] = ['between',[$issueDate[0],$issueDate[1]]];
        		$where_prog['fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
        		$where_prog['fmediaid'] = $mediaId;
        		$where_prog['a.fstate'] = 1;
        		$count2 = M('t'.$tb_str.'prog')
		        	->alias('a')
		        	->join('tprogclass d on a.fclasscode = d.fprogclasscode')
					->where($where_prog)
	        		->count();

				$data2 = M('t'.$tb_str.'prog')
					->alias('a')
					->field('2 adType,a.fprogid issueid,a.fissuedate,0 fsampleid,0 fadid,0 fillegaltypecode,"" fillegalcontent,"" fexpressioncodes,"" fapprno,"" fmanuno,"" fspokesman,"" fexpressions,"" fversion,"" fadowner,fcontent fadname,"" fbrand,fclasscode fadclasscode,d.fprogclass fadclassname,"" fadownername,a.fend fendtime,(unix_timestamp(a.fend)-unix_timestamp(a.fstart)) flength,0 is_long_ad,a.fstart fstarttime,a.fsamplefilename favifilename,a.fself')
					->join('tprogclass d on a.fclasscode = d.fprogclasscode')
					->where($where_prog)
					->select();
        	}
        	$count = $count1+$count2;
        	$data = array_merge($data1,$data2);
        }elseif($dataType == 2){
        	$where_astr = ' fmediaid = "'.$mediaId.'" and fissuedate = "'.date('Y-m-d',strtotime($issueDate[0])).'"';

        	$count = M('t'.$tb_str.'sample')
				->alias('b')
				->field($fields.$fields2)
				->join('(select * from t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' where '.$where_astr.' group by fsampleid) a on a.fsampleid = b.fid and b.fstate in(0,1)')
				->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
				->join('tadclass d on c.fadclasscode = d.fcode')
				->join('tadowner e on c.fadowner = e.fid')
				->where($where_issue)
				->count();

        	$data = M('t'.$tb_str.'sample')
				->alias('b')
				->field($fields.$fields2)
				->join('(select * from  t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' where '.$where_astr.' group by fsampleid) a on a.fsampleid = b.fid and b.fstate in(0,1)')
				->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
				->join('tadclass d on c.fadclasscode = d.fcode')
				->join('tadowner e on c.fadowner = e.fid')
				->where($where_issue)
				->order($order)
				->limit($limitIndex,$pageSize)
				->select();
        }elseif($dataType == 3){
        	$where_astr = 'fmediaid = "'.$mediaId.'" and fissuedate = "'.date('Y-m-d',strtotime($issueDate[0])).'"';

        	$where_issue['a.fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
			$where_issue['a.fmediaid'] = $mediaId;
			if($mclass == '01' || $mclass == '02'){
				$where_issue['_string'] = 'b.fissuedate = "'.date('Y-m-d',strtotime($issueDate[0])).'" or b.fillegaltypecode > 0';
			}else{
				$where_issue['_string'] = 'b.fillegaltypecode > 0';
			}

			$count = M('t'.$tb_str.'sample')
				->alias('b')
				->field($fields.$fields2)
				->join('(select * from  t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' where '.$where_astr.' group by fsampleid) a on a.fsampleid = b.fid and b.fstate in(0,1)')
				->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
				->join('tadclass d on c.fadclasscode = d.fcode')
				->join('tadowner e on c.fadowner = e.fid')
				->where($where_issue)
				->count();

        	$data = M('t'.$tb_str.'sample')
				->alias('b')
				->field($fields.$fields2)
				->join('(select * from  t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])).' where '.$where_astr.' group by fsampleid) a on a.fsampleid = b.fid and b.fstate in(0,1)')
				->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
				->join('tadclass d on c.fadclasscode = d.fcode')
				->join('tadowner e on c.fadowner = e.fid')
				->where($where_issue)
				->order($order)
				->limit($limitIndex,$pageSize)
				->select();
        }

        if(!empty($outtype)){
        	$title = $do_mie['fmedianame'].'  '.date('Y-m-d',strtotime($issueDate[0]));
	    	if(date('H:i:s',strtotime($issueDate[0])) <> '00:00:00' || date('H:i:s',strtotime($issueDate[1])) <> '23:59:59'){
	    		$title .= '（'.date('H:i:s',strtotime($issueDate[0])).'~'.date('H:i:s',strtotime($issueDate[1])).'）';
	    	}
	    	if($adType == -1){
	        	$title .= '串播单';
	    	}elseif($adType == 1){
	    		if($dataType == 1){
	    			$title .= '广告清单';
	    		}elseif($dataType == 2){
	    			$title .= '播出广告';
	    		}elseif($dataType == 2){
	    			$title .= '新广告及违法广告';
	    		}
	    	}elseif($adType == 1){
	    		$title .= '节目清单';
	    	}
        }

        if($dataType == 1 && ($mclass == '01' || $mclass == '02')){
        	if($adType == -1){
	        	$resultdata = D('ZjMediaIssue','Model')->adListOrder($data1,$issueDate,$limitIndex,$pageSize,$data2,0,$orderType);
	        }else{
	        	$resultdata = D('ZjMediaIssue','Model')->adListOrder($data,$issueDate,$limitIndex,$pageSize,[],0,$orderType);
	        }
	        if(!empty($outtype)){
	        	$endtitle = '共'.$resultdata['count'].'条记录（包含空时段），其中广告'.$resultdata['guanggaocount'].'条，节目'.$resultdata['jiemucount'].'条';
	        	$this->createXls($resultdata['resultList'],$title,$endtitle);
		    }else{
		        $this->ajaxReturn(['code'=>0, 'msg'=>'成功', 'count'=>$resultdata['count'], 'jiemucount'=>$resultdata['jiemucount'], 'guanggaocount'=>$resultdata['guanggaocount'], 'data'=>$resultdata['resultList']?$resultdata['resultList']:[]]);
		    }
        }elseif($mclass == '03' || $mclass == '04'){
        	$where_psource['fmediaid'] = $mediaId;
        	$where_psource['fissuedate'] = date('Y-m-d',strtotime($issueDate[0]));
        	$do_psource = M('tpapersource')->field('fid,fpage,fpagetype,furl,fstatus fstate')->where($where_psource)->order('fpage asc')->select();
        	$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data?$data:[],'pagedata'=>$do_psource));
        }else{
        	if(!empty($outtype)){
	        	$this->createXls($data,$title,'');
		    }else{
				$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data?$data:[]));
		    }
        }
	}

	/*
	* 生成串播单xls
	* zw
	* $data 数据，$title 表头
	*/
	private function createXls($data,$title,$endtitle){
		foreach ($data as $key => $value) {
    		if($value['adType'] == 1){
    			$data[$key]['fadname'] = '  '.$value['fadname'];
    			$data[$key]['adTypeName'] = '广告';
    		}elseif($value['adType'] == 2){
    			$data[$key]['adTypeName'] = '节目';
    		}else{
    			$data[$key]['adTypeName'] = '空时段';
    		}
    		if($value['fillegaltypecode'] > 0){
    			$data[$key]['fillegaltype'] = '违法';
    		}elseif($value['fillegaltypecode'] <= 0 && $value['adType'] == 1){
    			$data[$key]['fillegaltype'] = '不违法';
    		}else{
    			$data[$key]['fillegaltype'] = '';
    		}
    	}
        $outdata['datalie'] = [
          '序号'=>'key',
          '广告/节目名称'=>'fadname',
          '广告/节目类别'=>'fadclassname',
          '开始时间'=>'fstarttime',
          '结束时间'=>'fendtime',
          '时长/版面'=>'flength',
          '品牌'=>'fbrand',
          '类型'=>'adTypeName',
          '广告主'=>'fadownername',
          '违法类型'=>'fillegaltype',
          '违法内容'=>'fillegalcontent',
          '违法表现代码'=>'fexpressioncodes',
          '违法表现'=>'fexpressions',
          '版本说明'=>'fversion',
          '生产批准文号'=>'fmanuno',
          '广告批准文号'=>'fapprno',
          '代言人'=>'fspokesman',
        ];
        if($mclass == '01' || $mclass == '02' || $mclass == '03'){
        	$outdata['datalie']['素材地址'] = 'ysscurl';
        }elseif($mclass == 'net'){
        	$outdata['datalie']['发布页'] = 'fbyurl';
        	$outdata['datalie']['落地页'] = 'ldyurl';
        }else{
        	$outdata['datalie']['素材地址'] = 'ysscurl';
        	$outdata['datalie']['发布页'] = 'fbyurl';
        	$outdata['datalie']['落地页'] = 'ldyurl';
        }

        $outdata['title'] = $title;
        $outdata['endtitle'] = $endtitle;
        $outdata['lists'] = $data;
        $ret = A('Api/Function')->outdata_xls($outdata);
        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
	}

	/*
	* by zw
	* 任务完成提交
	*/
	public function issueDataSubmit(){
		$getData = $this->oParam;
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//任务日期
		$taskType = $getData['taskType']?$getData['taskType']:'ggjc';//任务类别（传值，ggfl广告分离，ggjc 广告监测，ggfs 广告复审，ggzs广告终审,，bzfl 报纸分离）
		$nextTaskType = $getData['nextTaskType'];//下一任务环节（传值，ggjc 广告监测，ggfs 广告复审，bzfl 报纸分离）
// $mediaId = 11000010002362;
// $issueDate = '2019-11-1';
		//任务详情
        $where_mie['a.fmedia_id'] = $mediaId;
        $where_mie['a.fissue_date'] = $issueDate;
    	$where_mie['c.fstatus'] = 1;
    	$where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        $where_mie['d.fstatus'] = 0;
        $where_mie['a.fstatus'] = 1;
        $do_mie = M('tmediaissue')
        	->alias('a')
        	->field('b.fmediaclassid,d.fid flowid,c.fid ftaskid,c.fbiz_def_id,d.fflow_code,a.fis_credit')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('ttask c on a.fid = c.fbiz_main_id')
        	->join('ttask_flow d on c.fid = d.ftask_id')
        	->where($where_mie)
        	->find();
        if(empty($do_mie)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'任务不存在'));
        }

        //根据媒体类型获取相关表
        $mclass = substr($do_mie['fmediaclassid'],0,2);
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
        }

		A('Api/Function')->createPlayIssueData($mclass,$mediaId,[$issueDate,date('Y-m-d',strtotime($issueDate)). ' 23:59:59']);//重新生成监播单

        if($mclass == '01' || $mclass == '02'){
            if(!empty(C('SAMPLEDETECT'))){
                $res = A('Jiance/SampleManage')->isTrueNewSample($mediaId,$issueDate,1);//提交确认的样本
            }
            M()->execute("update t".$tb_str."issue_".date('Y',strtotime($issueDate))." set fprice = gettvprice(fmediaid,fstarttime,flength) where fmediaid = ".$mediaId." and fissuedate = '".$issueDate."'");//更新价格
        }else{
            M()->execute('update t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)).' a,tpapersize b set a.fprice = GetPaperPrice( fmediaid,fissuedate,fpage,b.fcode,fissuetype,1) where a.fsize = b.fsize and b.fmediaclassid = "'.$mclass.'" and a.fmediaid = '.$mediaId.' and a.fissuedate = "'.$issueDate.'"');//更新价格
        }

        //更新当前任务流程信息
        $flow_data['frece_id'] = session('personData.fid');
        $flow_data['frece'] = session('personData.fname');
        $flow_data['frece_time'] = date('Y-m-d H:i:s');
        $flow_data['fstatus'] = 2;
        $flow_data['ffinish_type'] = 1;
       	$do_flow = M('ttask_flow')->where(['fid'=>$do_mie['flowid'],'fstatus'=>['in',[0,1]]])->save($flow_data);
       	if(!empty($do_flow)){
            //广告终审或广告复审环节如果没有违法广告，直接将数据归档
            if($taskType == 'ggfs'){
                $wherestr = '1=1';
                $wherestr .= ' and a.fissuedate = "'.$issueDate.'"';
                $wherestr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';
                $wherestr .= ' and b.fillegaltypecode > 0';
                $wherestr .= ' and a.fmediaid = '.$mediaId;
                $countsql = 'select count(*) as acount 
                from t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)).' a 
                inner join t'.$tb_str.'sample b on a.fsampleid=b.fid and b.fstate in(0,1) 
                inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9) 
                inner join tadowner d on c.fadowner = d.fid
                inner join tadclass e on c.fadclasscode=e.fcode 
                inner join tmedia f on a.fmediaid=f.fid and f.fid=f.main_media_id and f.fstate = 1 
                where '.$wherestr;
                $count = M()->query($countsql);
                if(empty($count[0]['acount'])){
                    $nextTaskType = '';
                }
            }

            //获取下一流程
            if(!empty($nextTaskType)){
           		$do_bdf = M('tbiz_def_flow')->where(['fflow_code'=>'mediaissue_'.$nextTaskType,'fbiz_def_id'=>$do_mie['fbiz_def_id']])->find();
            }

	        if(!empty($do_bdf)){
	        	$newflow_data['ftask_id'] = $do_mie['ftaskid'];
	        	$newflow_data['fflow_code'] = $do_bdf['fflow_code'];
	        	$newflow_data['fflow'] = $do_bdf['fflow'];
	        	$newflow_data['fsend_task_flow_id'] = $do_mie['flowid'];
	        	$newflow_data['fsend_id'] = session('personData.fid');
	        	$newflow_data['fsend'] = session('personData.fname');
	        	$newflow_data['fsend_time'] = date('Y-m-d H:i:s');
	        	$newflow_data['fstatus'] = 0;
	        	$add_newflow = M('ttask_flow')->add($newflow_data);
	        }else{
                $do_task = M('ttask')->where(['fid'=>$do_mie['ftaskid'],'fstatus'=>1])->save(['fstatus'=>5,'ffinish_time'=>date('Y-m-d H:i:s')]);//更改任务完成状态
                if(!empty($do_task)){
                    A('Api/ZJMediaIssue')->createIllAd($mediaId,$issueDate);//生成违法广告数据
                    D('Common/Function')->adIssueSendState($mediaId,$issueDate,0,20);//更改数据发布状态为监管平台发布
                }
            }
    		$this->ajaxReturn(array('code'=>0,'msg'=>'提交完成'));
       	}else{
       		$this->ajaxReturn(array('code'=>1,'msg'=>'任务不存在'));
       	}
        
	}

	/*
	* by zw
	* 任务数据保存
	*/
	public function issueDataSave(){
		$getData = $this->oParam;
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//任务日期
		$taskType = $getData['taskType']?$getData['taskType']:'ggjc';//任务类别（传值，ggfl广告分离，ggjc 广告监测，ggfs 广告复审，ggzs广告终审,，bzfl 报纸分离）
		$issueId = $getData['issueid'];//发布记录ID
        $adType = $getData['adType'];//节目类型，1广告，2节目
		$sampleIssueDate = $getData['sampleIssueDate'];//样本发布日期
        $sampleIsCreate = $getData['sampleIsCreate']?$getData['sampleIsCreate']:0;//样本类型，1新样本，0原样本
        $logData['fcontent'] = [];//初始化日志内容

        if(!empty($getData['fstarttime'])){
            $fstarttime = $getData['fstarttime'];
            $fendtime = $getData['fendtime'];
            $getData['fstarttime'] = date('Y-m-d H:i:s',strtotime($getData['fstarttime']));
            $getData['fendtime'] = date('Y-m-d H:i:s',strtotime($getData['fendtime']));
        }

		// $adType = 2;
		// $issueId = 1548;
		if(empty($mediaId) || empty($issueDate) || empty($taskType) || empty($adType)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

        //广告名
        if(empty($getData['fadname']) || $getData['fadname'] == '未命名广告'){
        	$this->ajaxReturn(['code'=>1, 'msg'=>'请正确输入广告名']);
        }
        //广告类别
        if(empty($getData['fadclasscode'])){
        	$this->ajaxReturn(['code'=>1, 'msg'=>'请选择广告类别']);
        }
        if($getData['fillegaltypecode'] > 0 && (empty($getData['fillegalcontent']) || empty($getData['fexpressioncodes']) || empty($getData['fexpressions']))){
        	$this->ajaxReturn(['code'=>1, 'msg'=>'请选择违法表现']);
        }

		//任务详情
        $where_mie['a.fmedia_id'] = $mediaId;
        $where_mie['a.fissue_date'] = $issueDate;
        if($taskType == 'gghd'){
        	$where_mie['c.fstatus'] = ['in',[1,5]];
        }else{
	        $where_mie['c.fstatus'] = 1;
        	$where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        }
        $where_mie['d.fstatus'] = 0;
        $where_mie['a.fstatus'] = 1;
        $where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        $do_mie = M('tmediaissue')
        	->alias('a')
        	->field('b.fmediaclassid,b.media_region_id,d.fid flowid')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('ttask c on a.fid = c.fbiz_main_id')
        	->join('ttask_flow d on c.fid = d.ftask_id')
        	->where($where_mie)
        	->find();
        if(empty($do_mie)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'任务不存在'));
        }
        $mclass = substr($do_mie['fmediaclassid'],0,2);
        
        if($adType == 1){
        	//根据媒体类型获取相关表
			if($mclass == '01'){
                $tb_str = 'tv';
			}elseif($mclass == '02'){
                $tb_str = 'bc';
			}elseif($mclass == '03' || $mclass == '04'){
                $tb_str = 'paper';
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
			}

			//发布信息更新
			if($mclass == '01' || $mclass == '02'){
				//判断时长传参
				$length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
				if($length<1 || $length>86400){
		            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
		        }

				if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
					$save_issue['fstarttime'] = $getData['fstarttime'];
                    $save_issue['fendtime'] = $getData['fendtime'];
                    $save_issue['fstarttime2'] = $fstarttime;
					$save_issue['fendtime2'] = $fendtime;
					$save_issue['flength'] = $length;

					$adList = D('Common/Function','Model')->getAdList($mclass,$mediaId,$issueDate,',a.*',1);//获取节目与广告的合并数据
					$nowAdData['issueid'] = $issueId;
					$nowAdData['adtype'] = 1;
					$nowAdData['fstarttime'] = $getData['fstarttime'];
					$nowAdData['fendtime'] = $getData['fendtime'];
					$checkData = D('ZjMediaIssue','Model')->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
					if(!empty($checkData)){
			        	$this->ajaxReturn(array('code'=>1,'msg'=>'您提交的广告与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
					}
				}
			}else{
				$save_issue['fpage'] = trim($getData['fpage']);
                $save_issue['fsize'] = trim($getData['fsize']);
                $save_issue['fpagetype'] = trim($getData['fpagetype']);
				$save_issue['fproportion'] = trim($getData['fproportion']);
                $save_issue['fissuetype'] = $getData['fissuetype'];
                $save_issue['fquantity'] = $getData['fquantity'];
			}

			//广告主
			$adOwnerId = 0;
			if(!empty($getData['fadownername'])){
				$adOwnerId = M('tadowner')->where(['fname'=>trim($getData['fadownername'])])->getfield('fid');
		    	if(!isset($adOwnerId)){
		    		$add_owner['fname'] = $getData['fadownername'];
		    		$add_owner['fregionid'] = $do_mie['media_region_id'];
                    $add_owner['fcreatetime'] = date('Y-m-d H:i:s');
                    $add_owner['fcreator'] = session('personData.fname');
		    		$adOwnerId = M('tadowner')->add($add_owner);
		    	}
			}

			//广告名更新，新增的广告用新广告信息，原广告已有的话用原广告的信息
			$adId = 0;
			if(!empty($getData['fadname'])){
				$doAd = M('tad')->where(['fadname'=>trim($getData['fadname']),'fstate'=>['in',[1,2,9]]])->find();
		    	if(empty($doAd)){
		    		$add_ad['fadname'] = trim($getData['fadname']);
		    		$add_ad['fbrand'] = trim($getData['fbrand'])?trim($getData['fbrand']):'无突出品牌';
		    		$add_ad['fadowner'] = $adOwnerId;
		    		$add_ad['fadclasscode'] = $getData['fadclasscode'];
		    		$add_ad['fcreator'] = session('personData.fname');
		    		$add_ad['fcreatetime'] = date('Y-m-d H:i:s');
		    		$add_ad['fstate'] = 1;
		    		$add_ad['fself'] = 1;
		    		$adId = M('tad')->add($add_ad);
                    $doAd = $add_ad;
		    	}else{
                    $save_ad['fbrand'] = trim($getData['fbrand'])?trim($getData['fbrand']):'无突出品牌';
                    $save_ad['fadowner'] = $adOwnerId;
                    $save_ad['fadclasscode'] = $getData['fadclasscode'];
                    $save_ad['fmodifier'] = session('personData.fname');
                    $save_ad['fmodifytime'] = date('Y-m-d H:i:s');
                    $save_ad['fstate'] = 1;
                    $save_ad['fself'] = 1;

                    //广告信息更改对比
                    $checkEditData = A('Function')->checkEditData($doAd,$save_ad,['fbrand','fadowner','fadclasscode']);//获取修改前后差异数据
                    if (!empty($checkEditData)){
                        $logData['fcontent']['广告信息'] = $checkEditData;//广告信息日志赋值
                        M('tad')->where(['fadid'=>$doAd['fadid']])->save($save_ad);
                    }

                    $adId = $doAd['fadid'];
                }
			}

            //报纸版面获取
            $sourceid = 0;
            if(!empty($save_issue['fpage'])){
                $viewPse = M('tpapersource')->field('fid,furl')->where(['fpage'=>trim($save_issue['fpage']),'fmediaid'=>$mediaId,'fissuedate'=>$issueDate])->find();
                if(!empty($viewPse)){
                    $sourceid = $viewPse['fid'];
                    $sourceurl = $viewPse['furl'];
                }
            }

            $oldData['issue'] = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))->where(['fid'=>$issueId])->find();//原发布数据日志
            $oldData['sample'] = M('t'.$tb_str.'sample')->where(['fid'=>$oldData['issue']['fsampleid']])->find();//原样本日志

            if(!empty($sampleIssueDate) && strtotime($sampleIssueDate) == strtotime($oldData['sample']['fissuedate'])){
                $sampleIssueDate = '';
            }
            //样本发布日期修改时的操作
            if((!empty($sampleIssueDate) || !empty($sampleIsCreate)) && ($mclass == '01' || $mclass == '02')){
                $save_sample = $oldData['sample'];
                unset($save_sample['fid']);
            }

			//样本更新
			$save_sample['fadid'] = $adId;
			$save_sample['fversion'] = $getData['fversion'];
			$save_sample['fspokesman'] = $getData['fspokesman'];
			$save_sample['fillegaltypecode'] = $getData['fillegaltypecode'];
			if($getData['fillegaltypecode'] > 0){
                $illegalData = D('Common/Function')->sampleillegal($getData['fexpressioncodes']);//获取违法信息
                
                $save_sample['fexpressioncodes'] = $getData['fexpressioncodes'];
                $save_sample['fexpressions'] = $illegalData['fexpressions'];
                $save_sample['fconfirmations'] = $illegalData['fconfirmations'];
                $save_sample['fillegalcontent'] = $getData['fillegalcontent'];
			}else{
                $save_sample['fexpressioncodes'] = '';
                $save_sample['fexpressions'] = '';
                $save_sample['fconfirmations'] = '';
				$save_sample['fillegalcontent'] = $getData['fillegalcontent'];
			}
			$save_sample['fmanuno'] = $getData['fmanuno'];
			$save_sample['fapprno'] = $getData['fapprno'];
			if($mclass == '01' || $mclass == '02'){
				$save_sample['is_long_ad'] = $getData['is_long_ad'];
	    	}else{
                if(!empty($getData['fillegaltypecode']) && empty($sourceid)){
                    $this->ajaxReturn(array('code'=>1,'msg'=>'违法广告的版面图片必须上传'));
                }
	    		if(!empty($sourceurl)){
	    			$save_sample['fjpgfilename'] = $sourceurl;
	    		}
                $save_sample['sourceid'] = $sourceid;
	    	}

	    	$save_sample['fself'] = 1;
	    	$save_issue['fself'] = 1;
	    	$save_issue['fmodifier'] = session('personData.fname');
    		$save_issue['fmodifytime'] = date('Y-m-d H:i:s');

            //发布信息更改对比
            $checkEditData = A('Function')->checkEditData($oldData['issue'],$save_issue,['fstarttime','fendtime','flength','fpage','fsize','fpagetype','fissuetype','fquantity','fproportion']);//获取修改前后差异数据
            if (!empty($checkEditData)) $logData['fcontent']['发布信息'] = $checkEditData;//发布信息日志赋值

            //样本信息更改对比
            if(!empty($sampleIsCreate) && ($mclass == '01' || $mclass == '02')){ //如果样本发布日期调整时，新建样本，原样本的UUID变更
                $save_sample['fmodifier'] = session('personData.fname');
                $save_sample['fmodifytime'] = date('Y-m-d H:i:s');
                $save_sample['fcreator'] = session('personData.fname');
                $save_sample['fcreatetime'] = date('Y-m-d H:i:s');
                $save_sample['uuid'] = $oldData['sample']['uuid'].'-'.date('YmdHis');
                $save_sample['fissuedate'] = $issueDate;
                $save_sample['fidentify'] = 0;
                $save_sample['fself'] = 1;
                $newSampleId = M('t'.$tb_str.'sample')->add($save_sample);
                if(!empty($newSampleId)){
                    $save_issue['fsampleid'] = $newSampleId;
                    $logData['fcontent']['新建样本信息'] = $save_sample;
                }
                $checkEditData = A('Function')->checkEditData($oldData['sample'],$save_sample,['uuid']);//获取修改前后差异数据
            }elseif(!empty($sampleIssueDate) && ($mclass == '01' || $mclass == '02')){ //如果样本发布日期调整时，新建样本，原样本的UUID变更
                $save_sample['fmodifier'] = session('personData.fname');
                $save_sample['fmodifytime'] = date('Y-m-d H:i:s');
                $save_sample['fcreator'] = session('personData.fname');
                $save_sample['fcreatetime'] = date('Y-m-d H:i:s');
                $save_sample['fissuedate'] = $sampleIssueDate;
                $newSampleId = M('t'.$tb_str.'sample')->add($save_sample);
                if(!empty($newSampleId)){
                    $sampleIsSt = date('Y',strtotime($sampleIssueDate));
                    while($sampleIsSt<=date('Y')){
                        $issueids = M('t'.$tb_str.'issue_'.$sampleIsSt)->where(['fsampleid'=>$getData['fsampleid'],'fissuedate'=>['between',[$sampleIssueDate,date('Y-m-d')]]])->getfield('fid',true);
                        if(!empty($issueids)){
                            M('t'.$tb_str.'issue_'.$sampleIsSt)->where(['fsampleid'=>$getData['fsampleid'],'fissuedate'=>['between',[$sampleIssueDate,date('Y-m-d')]]])->save(['fsampleid'=>$newSampleId]);
                            $save_sample['issueids'] = $issueids;
                        }
                        $sampleIsSt += 1;
                    }
                    $logData['fcontent']['新建样本信息'] = $save_sample;
                }

                $save_sample = [];
                $save_sample['uuid'] = $oldData['sample']['uuid'].'-'.date('YmdHis');
                $save_sample['fidentify'] = 0;
                $save_sample['fself'] = 1;
                $checkEditData = A('Function')->checkEditData($oldData['sample'],$save_sample,['uuid']);//获取修改前后差异数据
            }else{
                $checkEditData = A('Function')->checkEditData($oldData['sample'],$save_sample,['fissuedate','fadid','fversion','fspokesman','fillegaltypecode','fillegalcontent','fexpressioncodes','fexpressions','fmanuno','fapprno','is_long_ad','fjpgfilename']);//获取修改前后差异数据
            }

            //违法判定日志记录
            if(!empty($checkEditData['fillegaltypecode'])){
                if($oldData['sample']['fillegaltypecode'] == -30){
                    $silData['违法类型'] = '待核实';
                }elseif($oldData['sample']['fillegaltypecode'] == 0){
                    $silData['违法类型'] = '不违法';
                }elseif($oldData['sample']['fillegaltypecode'] == 0){
                    $silData['违法类型'] = '违法';
                }

                if($save_sample['fillegaltypecode'] == -30){
                    $silData['违法类型'] .= ' >>调整为>> 待核实';
                }elseif($save_sample['fillegaltypecode'] == 0){
                    $silData['违法类型'] .= ' >>调整为>> 不违法';
                }elseif($save_sample['fillegaltypecode'] == 30){
                    $silData['违法类型'] .= ' >>调整为>> 违法';
                }
            }
            if(!empty($checkEditData['fexpressioncodes'])){
                $silData['违法表现代码'] = $checkEditData['fexpressioncodes'];
            }
            if(!empty($checkEditData['fexpressions'])){
                $silData['违法表现'] = $checkEditData['fexpressions'];
            }
            if(!empty($checkEditData['fillegalcontent'])){
                $silData['违法内容'] = $checkEditData['fillegalcontent'];
            }

            if (!empty($checkEditData)) $logData['fcontent']['样本信息'] = $checkEditData;//样本信息日志赋值
            if(!empty($checkEditData['fadid'])){//广告信息更改对比
                $oldAd = M('tad')->where(['fadid'=>$oldData['sample']['fadid']])->find();
                if(!empty($oldAd)){
                    $checkEditData = A('Function')->checkEditData($oldAd,$doAd,['fadname']);//获取修改前后差异数据
                    if (!empty($checkEditData)) $logData['fcontent']['广告信息'] = $checkEditData;//广告信息日志赋值
                }
            }

            if(!empty($logData['fcontent'])){
                //非新建样本情况下执行
                if(empty($sampleIsCreate)){
                    $do_sample = M('t'.$tb_str.'sample')
                        ->where(['fid'=>$oldData['issue']['fsampleid']])
                        ->save($save_sample);

                    if(!empty($silData)){
                        $silAdd['fmediaclass'] = $mclass;
                        $silAdd['fsampleid'] = $oldData['issue']['fsampleid'];
                        $silAdd['fcontent'] = json_encode($silData,JSON_UNESCAPED_UNICODE);
                        $silAdd['fcreaterid'] = session('personData.fid');;
                        $silAdd['fcreater'] = session('personData.fname');;
                        M('tsample_illegallog')->add($silAdd);
                    }
                }

                $do_issue = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))
                    ->where(['fid'=>$issueId])
                    ->save($save_issue);

                $logData['ftype'] = 2;
                $logData['fmain_id'] = $issueId;
                $logData['fcontent'] = json_encode($logData['fcontent'],JSON_UNESCAPED_UNICODE);
                $logData['ftask_flow_id'] = $do_mie['flowid'];
                $logData['fmedia_id'] = $mediaId;
                $logData['fmedia_type'] = $mclass;
                $logId = A('Core')->createTaskFlowLog($logData);
                $this->ajaxReturn(array('code'=>0,'msg'=>'已保存','log'=>$logId));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，未调整'));
            }
		}else{
			if($mclass == '01'){
				$tb_name = 'ttvprog';
			}elseif($mclass == '02'){
				$tb_name = 'tbcprog';
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，媒体类别有误'));
			}
            $oldData = M($tb_name)->where(['fprogid'=>$issueId])->find();//旧数据

			//判断时长传参
			$length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
			if($length<1 || $length>86400){
	            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
	        }

			if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
				$add_ad['fstart'] = $getData['fstarttime'];
				$add_ad['fend'] = $getData['fendtime'];
				$add_ad['fduration'] = strtotime($getData['fendtime'])-strtotime($getData['fstarttime']);

				$adList = D('Common/Function','Model')->getAdList($mclass,$mediaId,$issueDate,',a.*',2);//获取节目与广告的合并数据
				$nowAdData['issueid'] = $issueId;
				$nowAdData['adtype'] = 2;
				$nowAdData['fstarttime'] = $getData['fstarttime'];
				$nowAdData['fendtime'] = $getData['fendtime'];
				$checkData = D('ZjMediaIssue','Model')->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
				if(!empty($checkData)){
		        	$this->ajaxReturn(array('code'=>1,'msg'=>'您提交的节目与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
				}
			}
			
        	$add_ad['fcontent'] = $getData['fadname'];
    		$add_ad['fclasscode'] = $getData['fadclasscode'];
    		$add_ad['fself'] = 1;
    		$add_ad['fmodifier'] = session('personData.fname');
    		$add_ad['fmodifytime'] = date('Y-m-d H:i:s');

            $checkEditData = A('Function')->checkEditData($oldData,$add_ad,['fstart','fend','fduration','fcontent','fclasscode']);//获取修改前后差异数据
            if (!empty($checkEditData)) $logData['fcontent']['节目信息'] = $checkEditData;

            if(!empty($logData['fcontent'])){
                M($tb_name)->where(['fprogid'=>$issueId])->save($add_ad);

                //日志记录
                $logData['ftype'] = 12;
                $logData['fmain_id'] = $issueId;
                $logData['fcontent'] = json_encode($logData['fcontent'],JSON_UNESCAPED_UNICODE);
                $logData['ftask_flow_id'] = $do_mie['flowid'];
                $logData['fmedia_id'] = $mediaId;
                $logData['fmedia_type'] = $mclass;
                $logId = A('Core')->createTaskFlowLog($logData);
                $this->ajaxReturn(array('code'=>0,'msg'=>'已保存','log'=>$logId));
            }else{
                $this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，未调整'));
            }
		}
	}

	/*
	* by zw
	* 任务数据添加
	*/
	public function issueDataAdd(){
		$getData = $this->oParam;
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//任务日期
		$taskType = $getData['taskType']?$getData['taskType']:'ggjc';//任务类别（传值，ggfl广告分离，ggjc 广告监测，ggfs 广告复审，ggzs广告终审,，bzfl 报纸分离）
		$adType = $getData['adType'];//节目类型，1广告，2节目
        if(!empty($getData['fstarttime'])){
            $fstarttime = $getData['fstarttime'];
            $fendtime = $getData['fendtime'];
            $getData['fstarttime'] = date('Y-m-d H:i:s',strtotime($getData['fstarttime']));
            $getData['fendtime'] = date('Y-m-d H:i:s',strtotime($getData['fendtime']));
        }

		if(empty($mediaId) || empty($issueDate) || empty($taskType) || empty($adType)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

        //广告名
        if(empty($getData['fadname']) || $getData['fadname'] == '未命名广告'){
        	$this->ajaxReturn(['code'=>1, 'msg'=>'请正确输入广告名']);
        }
        //广告类别
        if(empty($getData['fadclasscode'])){
        	$this->ajaxReturn(['code'=>1, 'msg'=>'请选择广告类别']);
        }
        
		//任务详情
        $where_mie['a.fmedia_id'] = $mediaId;
        $where_mie['a.fissue_date'] = $issueDate;
        if($taskType == 'gghd'){
        	$where_mie['c.fstatus'] = ['in',[1,5]];
        }else{
	        $where_mie['c.fstatus'] = 1;
        	$where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        }
        $where_mie['d.fstatus'] = 0;
        $where_mie['a.fstatus'] = 1;
        $where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        $do_mie = M('tmediaissue')
        	->alias('a')
        	->field('b.fmediaclassid,b.media_region_id,d.fid flowid')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('ttask c on a.fid = c.fbiz_main_id')
        	->join('ttask_flow d on c.fid = d.ftask_id')
        	->where($where_mie)
        	->find();
        if(empty($do_mie)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'任务不存在'));
        }
        $mclass = substr($do_mie['fmediaclassid'],0,2);
        if($mclass == '03' || $mclass == '04'){
            if(empty(trim($getData['fpage'])) || empty($getData['fsize']) || empty($getData['fissuetype']) || empty($getData['fquantity'])){
                $this->ajaxReturn(['code'=>1, 'msg'=>'报纸必要参数有误']);
            }
        }

        if($adType == 1){
        	//根据媒体类型获取相关表
			if($mclass == '01'){
				$tb_issue = 'ttvissue_'.date('Y',strtotime($issueDate));
				$tb_sample = 'ttvsample';
			}elseif($mclass == '02'){
				$tb_issue = 'tbcissue_'.date('Y',strtotime($issueDate));
				$tb_sample = 'tbcsample';
			}elseif($mclass == '03' || $mclass == '04'){
				$tb_issue = 'tpaperissue_'.date('Y',strtotime($issueDate));
				$tb_sample = 'tpapersample';
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
			}

			//发布信息更新
			if($mclass == '01' || $mclass == '02'){
				//判断时长传参
				$length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
				if($length<1 || $length>86400){
		            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
		        }

				if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
					$save_issue['fstarttime'] = $getData['fstarttime'];
                    $save_issue['fendtime'] = $getData['fendtime'];
                    $save_issue['fstarttime2'] = $fstarttime;
					$save_issue['fendtime2'] = $fendtime;
					$save_issue['flength'] = $length;

					$adList = D('Common/Function','Model')->getAdList($mclass,$mediaId,$issueDate,',a.*',1);//获取节目与广告的合并数据
					$nowAdData['issueid'] = 0;
					$nowAdData['adtype'] = 1;
					$nowAdData['fstarttime'] = $getData['fstarttime'];
					$nowAdData['fendtime'] = $getData['fendtime'];
					$checkData = D('ZjMediaIssue','Model')->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
					if(!empty($checkData)){
			        	$this->ajaxReturn(array('code'=>1,'msg'=>'您提交的广告与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
					}
				}else{
					$this->ajaxReturn(array('code'=>1,'msg'=>'请选择播出开始和结束时间'));
				}
			}else{
				$save_issue['fpage'] = trim($getData['fpage']);
                $save_issue['fsize'] = trim($getData['fsize']);
                $save_issue['fpagetype'] = trim($getData['fpagetype']);
                $save_issue['fproportion'] = trim($getData['fproportion']);
                $save_issue['fissuetype'] = $getData['fissuetype'];
				$save_issue['fquantity'] = $getData['fquantity'];
			}

			//广告主
			$adOwnerId = 0;
			if(!empty($getData['fadownername'])){
				$adOwnerId = M('tadowner')->where(['fname'=>trim($getData['fadownername'])])->getfield('fid');
		    	if(!isset($adOwnerId)){
		    		$add_owner['fname'] = trim($getData['fadownername']);
                    $add_owner['fregionid'] = $do_mie['media_region_id'];
                    $add_owner['fcreatetime'] = date('Y-m-d H:i:s');
		    		$add_owner['fcreator'] = session('personData.fname');
		    		$adOwnerId = M('tadowner')->add($add_owner);
		    	}
			}

	    	//广告名更新
	    	$adId = 0;
			if(!empty($getData['fadname'])){
				$add_ad = M('tad')->where(['fadname'=>trim($getData['fadname']),'fstate'=>['in',[1,2,9]]])->find();
		    	if(empty($add_ad)){
		    		$add_ad['fadname'] = trim($getData['fadname']);
		    		$add_ad['fbrand'] = trim($getData['fbrand'])?trim($getData['fbrand']):'无突出品牌';
		    		$add_ad['fadowner'] = $adOwnerId;
		    		$add_ad['fadclasscode'] = $getData['fadclasscode'];
		    		$add_ad['fcreator'] = session('personData.fname');
		    		$add_ad['fcreatetime'] = date('Y-m-d H:i:s');
		    		$add_ad['fstate'] = 1;
		    		$add_ad['fself'] = 1;
		    		$adId = M('tad')->add($add_ad);
		    	}else{
                    $save_ad['fbrand'] = trim($getData['fbrand'])?trim($getData['fbrand']):'无突出品牌';
                    $save_ad['fadowner'] = $adOwnerId;
                    $save_ad['fadclasscode'] = $getData['fadclasscode'];
                    $save_ad['fmodifier'] = session('personData.fname');
                    $save_ad['fmodifytime'] = date('Y-m-d H:i:s');
                    $save_ad['fstate'] = 1;
                    $save_ad['fself'] = 1;

                    //广告信息更改对比
                    $checkEditData = A('Function')->checkEditData($add_ad,$save_ad,['fbrand','fadowner','fadclasscode']);//获取修改前后差异数据
                    if (!empty($checkEditData)){
                        $logData['fcontent']['广告信息'] = $checkEditData;//广告信息日志赋值
                        M('tad')->where(['fadid'=>$add_ad['fadid']])->save($save_ad);
                    }

                    $adId = $add_ad['fadid'];
                }


			}

            //报纸版面获取
            $sourceid = 0;
            if(!empty($save_issue['fpage'])){
                $viewPse = M('tpapersource')->field('fid,furl')->where(['fpage'=>trim($save_issue['fpage']),'fmediaid'=>$mediaId,'fissuedate'=>$issueDate])->find();
                if(!empty($viewPse)){
                    $sourceid = $viewPse['fid'];
                    $sourceurl = $viewPse['furl'];
                }
            }

			//样本更新
			$save_sample['fadid'] = $adId;
			$save_sample['fversion'] = $getData['fversion'];
			$save_sample['fspokesman'] = $getData['fspokesman'];
			$save_sample['fillegaltypecode'] = $getData['fillegaltypecode'];
			if($getData['fillegaltypecode'] > 0){
                $illegalData = D('Common/Function')->sampleillegal($getData['fexpressioncodes']);//获取违法信息

				$save_sample['fexpressioncodes'] = $getData['fexpressioncodes'];
                $save_sample['fexpressions'] = $illegalData['fexpressions'];
				$save_sample['fconfirmations'] = $illegalData['fconfirmations'];
                $save_sample['fillegalcontent'] = $getData['fillegalcontent'];
			}else{
				$save_sample['fexpressioncodes'] = '';
                $save_sample['fexpressions'] = '';
                $save_sample['fconfirmations'] = '';
                $save_sample['fillegalcontent'] = $getData['fillegalcontent'];
			}
			$save_sample['fmanuno'] = $getData['fmanuno'];
			$save_sample['fapprno'] = $getData['fapprno'];
			// $fuuid = strtolower(createNoncestr(8).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(4).'-'.createNoncestr(12));
			$save_sample['fmediaid'] = $mediaId;
            $save_sample['fstate'] = 1;
			if($mclass == '01' || $mclass == '02'){
				$save_sample['fadlen'] = $length;
				$save_sample['is_long_ad'] = $getData['is_long_ad'];
	    	}else{
	    		if($getData['fillegaltypecode'] > 0 && empty($sourceid)){
                    $this->ajaxReturn(array('code'=>1,'msg'=>'违法广告的版面图片必须上传'));
                }
                if(!empty($sourceurl)){
                    $save_sample['fjpgfilename'] = $sourceurl;
                }
                $save_sample['sourceid'] = $sourceid;
	    	}
            // $sample = M($tb_sample)->where($save_sample)->getfield('fid');
            // if(empty($sample)){
                $save_sample['fissuedate'] = $issueDate;
                $save_sample['fself'] = 1;
                $save_sample['fcreator'] = session('personData.fname');
                $save_sample['fcreatetime'] = date('Y-m-d H:i:s');
                M()->startTrans();
                $sample = M($tb_sample) ->add($save_sample);
                if($mclass == '01' || $mclass == '02'){
                    if(!empty(C('SAMPLEDETECT'))){//识别状态开启时
                        //样本识别
                        $detectPushData['channel'] = $mediaId;
                        $detectPushData['name'] = $add_ad['fadname'];
                        $detectPushData['start'] = strripos($fstarttime,".")?strtotime($fstarttime).substr($fstarttime,strripos($fstarttime,".")):strtotime($fstarttime);
                        $detectPushData['end'] = strripos($fendtime,".")?strtotime($fendtime).substr($fendtime,strripos($fendtime,".")):strtotime($fendtime);
                        $detectPushData['editor_id'] = 'zhejiang_'.session('personData.fid');
                        $detectPushData['ad_type'] = $add_ad['fadclasscode'];
                        $detectPushData['ad_brand'] = $add_ad['fbrand'];
                        $detectPushData['ad_owner'] = trim($getData['fadownername']);
                        $detectPushData['ad_in_show'] = false;
                        $detectPushData['ad_speaker'] = $getData['fspokesman'];
                        $detectPushData['ver_info'] = $getData['fversion'];
                        $resDetect = A('Jiance/SampleManage')->sampleDetect($detectPushData,1);
                        if(!empty($resDetect['code'])){
                            M()->rollback();
                            $this->ajaxReturn(array('code'=>1,'msg'=>$resDetect['msg']));
                        }else{
                            M()->commit();
                            M($tb_sample)->where(['fid'=>$sample])->save(['uuid'=>$resDetect['data'],'fdetectstatus'=>1,'fdetecttime'=>time()]);
                        }
                    }else{
                        M()->commit();
                    }

                    //剪辑任务
                    $pushData = [
                        'mediaId'=>$mediaId,
                        'source_name'=>'样本素材生成_'.$mediaId.'_'.$sample.'_'.$getData['fstarttime'].'~'.$getData['fendtime'],
                        'source_type'=>30,
                        'Sstart'=>$getData['fstarttime'],
                        'Send'=>$getData['fendtime'],
                        'tid'=>$sample
                    ];
                    $res = A('Api/Media')->add_source_make($pushData,1);
                }else{
                    M()->commit();
                }
            // }
	    	$newData['sample'] = $save_sample;//日志

			//发布表更新
			$save_issue['fsampleid'] = $sample;
			$save_issue['fmediaid'] = $mediaId;
			$save_issue['fissuedate'] = $issueDate;
			$save_issue['fself'] = 1;
			$save_issue['fcreator'] = session('personData.fname');
	    	$do_issue = M($tb_issue) ->add($save_issue);
	    	$newData['issue'] = $save_issue;//日志

			$logData['ftype'] = 1;
        	$logData['fmain_id'] = $do_issue;
        	$logData['fcontent'] = json_encode($newData,JSON_UNESCAPED_UNICODE);
		}else{
			if($mclass == '01'){
				$tb_name = 'ttvprog';
			}elseif($mclass == '02'){
				$tb_name = 'tbcprog';
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败，媒体类别有误'));
			}

			//判断时长传参
			$length = strtotime($getData['fendtime']) - strtotime($getData['fstarttime']);
			if($length<1 || $length>86400){
	            $this->ajaxReturn(['code'=>1, 'msg'=>'播出时段选择有误']);
	        }

			if(!empty($getData['fstarttime']) && !empty($getData['fendtime'])){
				$add_ad['fstart'] = $getData['fstarttime'];
				$add_ad['fend'] = $getData['fendtime'];
				$add_ad['fduration'] = $length;

				$adList = D('Common/Function','Model')->getAdList($mclass,$mediaId,$issueDate,',a.*',2);//获取节目与广告的合并数据
				$nowAdData['issueid'] = 0;
				$nowAdData['adtype'] = 2;
				$nowAdData['fstarttime'] = $getData['fstarttime'];
				$nowAdData['fendtime'] = $getData['fendtime'];
				$checkData = D('ZjMediaIssue','Model')->overlappingData($nowAdData,$adList,'fstarttime','fendtime');//验证时段是否包含及交叉
				if(!empty($checkData)){
		        	$this->ajaxReturn(array('code'=>1,'msg'=>'您提交的节目与【'.$checkData['title'].'（'.$checkData['fstarttime'].'-'.$checkData['fendtime'].'）】时段冲突'));
				}
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'请选择播出开始和结束时间'));
			}
			
        	$add_ad['fmediaid'] = $mediaId;
        	$add_ad['fissuedate'] = $issueDate;
        	$add_ad['fstate'] = 1;
        	$add_ad['fself'] = 1;
        	$add_ad['fcontent'] = $getData['fadname'];
    		$add_ad['fclasscode'] = $getData['fadclasscode'];
    		$add_ad['fcreator'] = session('personData.fname');
    		$add_ad['fcreatetime'] = date('Y-m-d H:i:s');
    		M($tb_name)->add($add_ad);

        	$logData['ftype'] = 11;
        	$logData['fmain_id'] = 0;
        	$logData['fcontent'] = json_encode($add_ad,JSON_UNESCAPED_UNICODE);
		}
		$logData['ftask_flow_id'] = $do_mie['flowid'];
        $logData['fmedia_id'] = $mediaId;
        $logData['fmedia_type'] = $mclass;
        $logId = A('Core')->createTaskFlowLog($logData);
		$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','log'=>$logId));
	}

	/*
	* by zw
	* 发布数据删除
	*/
	public function issueDataDel(){
		$getData = $this->oParam;
		$issueId = $getData['issueId'];//发布ID
		$adType = $getData['adType'];//节目类型，1广告，2节目
		$delData = $getData['delData']?$getData['delData']:[];//删除的数据
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//任务日期
		$taskType = $getData['taskType']?$getData['taskType']:'ggjc';//任务类别（传值，ggfl广告分离，ggjc 广告监测，ggfs 广告复审，ggzs广告终审,，bzfl 报纸分离）

		if(!empty($issueId)){
			$delData[] = [
				'adType'=>$adType,
				'issueId'=>$issueId,
			];
		}
		if(empty($delData) && !is_array($delData)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
		}

		//任务详情
        $where_mie['a.fmedia_id'] = $mediaId;
        $where_mie['a.fissue_date'] = $issueDate;
        if($taskType == 'gghd'){
        	$where_mie['c.fstatus'] = ['in',[1,5]];
        }else{
	        $where_mie['c.fstatus'] = 1;
        	$where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        }
        $where_mie['d.fstatus'] = 0;
        $where_mie['a.fstatus'] = 1;
        $where_mie['d.fflow_code'] = 'mediaissue_'.$taskType;
        $do_mie = M('tmediaissue')
        	->alias('a')
        	->field('b.fmediaclassid,d.fid flowid')
        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
        	->join('ttask c on a.fid = c.fbiz_main_id')
        	->join('ttask_flow d on c.fid = d.ftask_id')
        	->where($where_mie)
        	->find();

        if(empty($do_mie)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'任务不存在'));
        }

        //根据媒体类型获取相关表
        $mclass = substr($do_mie['fmediaclassid'],0,2);
		if($mclass == '01'){
			$tb_str = 'tv';
		}elseif($mclass == '02'){
			$tb_str = 'bc';
		}elseif($mclass == '03' || $mclass == '04'){
			$tb_str = 'paper';
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
		}

		foreach ($delData as $key => $value) {
			if($value['adType'] == 1){
				$oldData =  M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))->where(['fid'=>$value['issueId']])->find();//日志
				$logData['ftype'] = 3;
	        	
				$del_issue =  M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))->where(['fid'=>$value['issueId']])->save(['fstate'=>-1,'fself' =>1,'fmodifier'=>session('personData.fname'),'fmodifytime'=>date('Y-m-d H:i:s')]);
			}else{
				$oldData =  M('t'.$tb_str.'prog')->where(['fprogid'=>$value['issueId']])->find();//日志
				$logData['ftype'] = 13;

				$del_issue =  M('t'.$tb_str.'prog')->where(['fprogid'=>$value['issueId']])->save(['fstate'=>-1,'fself' =>1,'fmodifier'=>session('personData.fname'),'fmodifytime'=>date('Y-m-d H:i:s')]);
			}
			$logData['fmain_id'] = $value['issueId'];
	    	$logData['fcontent'] = json_encode($oldData,JSON_UNESCAPED_UNICODE);
			$logData['ftask_flow_id'] = $do_mie['flowid'];
	        $logData['fmedia_id'] = $mediaId;
	        $logData['fmedia_type'] = $mclass;
	        $logId = A('Core')->createTaskFlowLog($logData);
		}

		if(!empty(count($delData))){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，记录不存在，请刷新后重试'));
		}
	}

	/*
	* by zw
	* 获取指定媒体已存在的任务
	*/
	public function getMediaIssue(){
		$getData = $this->oParam;
		$mediaId = $getData['mediaId'];
		if(!empty($mediaId)){
			$do_mie = M('tmediaissue')->field('fmedia_id,fissue_date')->where(['fmedia_id'=>$mediaId])->select();
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>count($do_mie),'data'=>$do_mie));
	}

	/*
	* 时间偏差调整
	* by zw
	*/
	public function mediaIssueTimeEdit(){
		$getData = $this->oParam;
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//任务日期
		$issueData = $getData['issueData'];//发布数据
		$eType = $getData['eType'] === ''?-1:$getData['eType'];//调整方式，0前进，1后退
		$second = $getData['second'];//偏差秒数
		// $issueDate = '2019-11-1';
		// $mediaId = 11000010002389;
		// $issueData = [
		// 	["issueId"=>328060,"adType"=>1],
		// 	["issueId"=>328061,"adType"=>1],
		// 	["issueId"=>1284,"adType"=>1],
		// ];

		if(empty($second) || $eType == -1 || empty($issueData) || empty($issueDate) || empty($mediaId)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}
		if(!empty($eType)){
			$second = - (int)($second);
		}

		//任务详情
        $where_mie['a.fid'] = $mediaId;
        $where_mie['a.fstate'] = 1;
        $do_mie = M('tmedia')
        	->alias('a')
        	->where($where_mie)
        	->find();
        if(empty($do_mie)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
        }
        $mclass = substr($do_mie['fmediaclassid'],0,2);
		if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂不支持该媒体类型'));
        }

        $ids = [];
        foreach ($issueData as $key => $value) {
        	if($value['adType'] == 1){
	        	$ids[] = $value['issueId'];
        	}
        }
        if(!empty($ids)){
			$where_issue['a.fstate'] = 1;//发布数据必须是有效的
        	$where_issue['a.fid'] = ['in',$ids];
	        $where_issue['a.fmediaid'] = $mediaId;
	        $where_issue['a.fissuedate'] = $issueDate;
			$data1 = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))
				->alias('a')
				->field('a.fstarttime,a.fendtime,c.fadname,a.fid')
				->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)')
				->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
				->join('tadclass d on c.fadclasscode = d.fcode')
				->join('tadowner e on c.fadowner = e.fid')
				->where($where_issue)
				->select();

			$where_issue['a.fid'] = ['notin',$ids];
			$data2 = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))
				->alias('a')
				->field('a.fstarttime,a.fendtime,c.fadname,a.fid')
				->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)')
				->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
				->join('tadclass d on c.fadclasscode = d.fcode')
				->join('tadowner e on c.fadowner = e.fid')
				->where($where_issue)
				->select();

			$checkData = [];
			foreach ($data1 as $key => $value) {
				$isCheck = 0;
				foreach ($data2 as $key2 => $value2) {
					$isCheck = isTimeContain((strtotime($value['fstarttime'])+$second),(strtotime($value['fendtime'])+$second),strtotime($value2['fstarttime']),strtotime($value2['fendtime']));
					if(!empty($isCheck)){
						break;
					}
				}
				if(!empty($isCheck)){
					$checkData[] = $value;
				}
			}

			if(!empty(count($checkData))){
				$this->ajaxReturn(array('code'=>1,'msg'=>'调整失败，请检查调整后的时间是否与前后广告交叉','data'=>$checkData));
			}else{
				$do_save = M()->execute('update t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)).' set fstarttime = from_unixtime((unix_timestamp(fstarttime)+('.(int)$second.'))),fendtime = from_unixtime((unix_timestamp(fendtime)+('.(int)$second.'))),fmodifier = "时间偏差调整",fmodifytime = now(),fself = 1 where fmediaid = '.$mediaId.' and fissuedate = "'.$issueDate.'" and fid in('.implode(',', $ids).')');
				$this->ajaxReturn(array('code'=>0,'msg'=>'成功调整'.$do_save.'条'));
			}
        }else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'调整失败，节目不允许批量调整'));
        }

	}

    /*
    * by zw
    * 报纸版面确认
    */
    public function issuePageConfirm(){
        $getData = $this->oParam;
        $fid = $getData['fid'];//ID
        $state = $getData['state']?$getData['state']:0;//更改的状态，0待处理，1处理中，2已处理
        if(empty($fid)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        }

        $do_pse = M('tpapersource')->where(['fid'=>$fid])->save(['fstatus'=>$state]);
        if(!empty($do_pse)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'更新成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'更新失败'));
        }
    }

    /*
    * by zw
    * 样本违法判定记录
    */
    public function sampleillegalLog(){
        $getData = $this->oParam;
        $sid = $getData['sid'];//样本ID
        $mediaId = $getData['mediaId'];//媒体ID

        //获取媒体类别
        $where_mie['a.fid'] = $mediaId;
        $where_mie['a.fstate'] = 1;
        $do_mie = M('tmedia')
            ->alias('a')
            ->where($where_mie)
            ->find();
        if(empty($do_mie)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'媒体不存在'));
        }
        $mclass = substr($do_mie['fmediaclassid'],0,2);

        $doSil = M('tsample_illegallog')
            ->alias('a')
            ->field('fid,fcontent,fcreater fcreator,fcreatetime')
            ->where(['fsampleid'=>$sid,'fmediaclass'=>$mclass])
            ->order('fcreatetime desc')
            ->select();

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$doSil));

    }
}
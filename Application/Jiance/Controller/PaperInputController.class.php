<?php
namespace Jiance\Controller;
use Think\Controller;
class PaperInputController extends BaseController {

	/*
    * by zw
    * 报纸版面添加、修改
    */
    public function addPaperSource(){
    	$getData = $this->oParam;
    	// $userIp = getClientIp();
        $fid = $getData['fid'];//版面ID
        $mediaId = $getData['mediaId'];//媒体ID
        $issueDate = $getData['issueDate'];//发布日期
        $pageNum = $getData['pageNum'];//版面编号
        $pageType = $getData['pageType'];//版面类别
        $pageUrl = $getData['pageUrl'];//版面图片地址
        $subType = $getData['subType'];//提交类型，0默认不传，2强制覆盖
        if(!empty($fid)){
        	if(empty($pageUrl)){
        		$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        	}
        }else{
        	if(empty($mediaId) || empty($issueDate) || empty($pageNum) || empty($pageUrl)){
	            $this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
	        }
        }

        if(empty($fid)){
        	//检查是否已存在同名版面
	        $wherePse['fmediaid'] = $mediaId;
	        $wherePse['fissuedate'] = $issueDate;
	        $wherePse['fpage'] = $pageNum;
	        $viewPse = M('tpapersource')->where($wherePse)->find();
	        if(!empty($viewPse) && $subType != 2){
	        	$this->ajaxReturn(array('code'=>0,'msg'=>'已存在相同版面，是否需要覆盖原版面','subtype'=>2));
	        }

	        if(!empty($viewPse) && $subType == 2){//同名广告
	        	$savePse['fpagetype'] = $pageType;
	        	$savePse['furl'] = $pageUrl;
	        	$savePse['fmodifier'] = session('personData.fname')?session('personData.fname'):'扫码采集';
	        	$savePse['fmodifytime'] = date('Y-m-d H:i:s');
	        	$doActionPse = M('tpapersource')->where(['fid'=>$viewPse['fid']])->save($savePse);
	        	if(!empty($doActionPse)){
	        		M()->execute('update tpaperissue_'.date('Y',strtotime($issueDate)).' a,tpapersample b set fpage = "'.$pageNum.'",fpagetype = "'.$pageType.'",b.fjpgfilename = "'.$pageUrl.'" where a.fsampleid = b.fid and b.sourceid = '.$viewPse['fid']);
	        	}
	        }else{
	        	$addPse['fmediaid'] = $mediaId;
	        	$addPse['fissuedate'] = $issueDate;
	        	$addPse['fpage'] = $pageNum;
	        	$addPse['fpagetype'] = $pageType;
	        	$addPse['furl'] = $pageUrl;
	        	$addPse['fcreator'] = session('personData.fname')?session('personData.fname'):'扫码采集';
	        	$addPse['fcreatorid'] = session('personData.fid')?session('personData.fid'):0;
	        	$addPse['fcreatetime'] = date('Y-m-d H:i:s');
	        	$addPse['fstate'] = 2;
	        	$addPse['fstatus'] = 2;
	        	$addPse['fidentify'] = 0;
	        	$doActionPse = M('tpapersource')->add($addPse);
	        	if(!empty($doActionPse)){
	        		M()->execute('update tpaperissue_'.date('Y',strtotime($issueDate)).' a,tpapersample b set a.fpagetype = "'.$pageType.'",b.fjpgfilename = "'.$pageUrl.'",b.sourceid = '.$doActionPse.' where a.fsampleid = b.fid and a.fmediaid = '.$mediaId.' and a.fissuedate = "'.$issueDate.'" and a.fpage = "'.$pageNum.'"');
	        	}
	        }
        }else{
	        $wherePse['fid'] = ['neq',$fid];
	        $wherePse['fmediaid'] = $mediaId;
	        $wherePse['fissuedate'] = $issueDate;
	        $wherePse['fpage'] = $pageNum;
	        $countPse = M('tpapersource')->where($wherePse)->count();
	        if(!empty($countPse)){
	        	$this->ajaxReturn(array('code'=>1,'msg'=>'已存在相同版面'));
	        }

	        $savePse['fpage'] = $pageNum;
	        $savePse['fpagetype'] = $pageType;
	        $savePse['furl'] = $pageUrl;
        	$savePse['fmodifier'] = session('personData.fname')?session('personData.fname'):'扫码采集';
        	$savePse['fmodifytime'] = date('Y-m-d H:i:s');
        	$doActionPse = M('tpapersource')->where(['fid'=>$fid])->save($savePse);
        	if(!empty($doActionPse)){
        		M()->execute('update tpaperissue_'.date('Y',strtotime($issueDate)).' a,tpapersample b set a.fpage = "'.$pageNum.'",a.fpagetype = "'.$pageType.'",b.fjpgfilename = "'.$pageUrl.'" where a.fsampleid = b.fid and b.sourceid = '.$fid);
        	}
        }

        if(!empty($doActionPse)){
        	$this->ajaxReturn(array('code'=>0,'msg'=>'提交成功'));
        }else{
	        $this->ajaxReturn(array('code'=>1,'msg'=>'提交失败，请尝试重新提交'));
        }
    }

    /*
    * by zw
    * 报纸版面删除
    */
    public function delPaperSource(){
    	$getData = $this->oParam;
        $fid = $getData['fid'];//版面ID
        if(empty($fid)){
    		$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
        }

        $wherePse['fid'] = $fid;
        $delPse = M('tpapersource')->where($wherePse)->delete();
        if(empty($delPse)){
        	$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，版面不存在'));
        }else{
        	$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
        }
    }

    /*
    * 获取媒体规格
    * by zw
    */
    public function getPaperSize(){
        $getData = $this->oParam;
        $mediaId = $getData['mediaId'];//媒体ID
        $mclass = $getData['mclass']?$getData['mclass']:'03';//媒体类别
        if(empty($mediaId)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
        }
        $doPse = M('tpapersize')->field('fcode,fsize,fdaxiao,fmianji')->where(['fmediaclassid'=>$mclass,'fstate'=>1])->select();
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$doPse));
    }

    /*
    * 获取规格列表
    * by zw
    */
    public function paperSizeList(){
        $getData = $this->oParam;
        $pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $mediaClass = $getData['mclass'];//媒体类型（'01'，'02'，'03',''默认）
        $fsize = $getData['fsize'];//规格名
        $limitIndex = ($pageIndex-1)*$pageSize;

        $where_mie['_string'] = '1=1';
        //媒体类型
        if(!empty($mclass)){
            $where_mie['a.fmediaclassid'] = $mclass;
        }
        //规格
        if(!empty($fsize)){
            $where_mie['a.fsize'] = ['like','%'.$fsize.'%'];
        }

        $where_mie['a.fstate'] = 1;
        $count = M('tpapersize')
            ->alias('a')
            ->join('tmediaclass b on a.fmediaclassid = b.fid')
            ->where($where_mie)
            ->count();

        $data = M('tpapersize')
            ->alias('a')
            ->field('b.fclass,a.*')
            ->join('tmediaclass b on a.fmediaclassid = b.fid')
            ->where($where_mie)
            ->limit($limitIndex,$pageSize)
            ->order('a.fcreatetime desc')
            ->select();

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
    }

    /*
    * 添加规格
    * by zw
    */
    public function addPaperSize(){
        $getData = $this->oParam;
        $mclass = $getData['mclass'];//媒体类别
        $size = $getData['size'];//规格
        $daXiao = $getData['daXiao'];//大小
        $mianJi = $getData['mianJi'];//面积
        if(empty($size) || !is_numeric($daXiao) || !is_numeric($mianJi)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
        }

        $doPe = M('tpapersize')->where(['fmediaclassid'=>$mclass,'fsize'=>$size,'fstate'=>1])->order('fcode desc')->find();
        if(!empty($doPe)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'该媒体已存在同名规格'));
        }

        $addData['fcode'] = (int)$doPe['fcode']+1;
        $addData['fmediaclassid'] = $mclass;
        $addData['fsize'] = $size;
        $addData['fdaxiao'] = $daXiao;
        $addData['fmianji'] = $mianJi;
        $addData['fcreator'] = session('personData.fname');
        $addData['fcreatetime'] = date('Y-m-d H:i:s');
        $addData['fstate'] = 1;
        $addPe = M('tpapersize')->add($addData);
        if(!empty($addPe)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'添加失败，请尝试重新添加'));
        }
    }

    /*
    * 修改规格
    * by zw
    */
    public function editPaperSize(){
        $getData = $this->oParam;
        $fid = $getData['fid'];//规格ID
        $mclass = $getData['mclass']?$getData['mclass']:0;//媒体id
        $size = $getData['size'];//规格
        $daXiao = $getData['daXiao'];//大小
        $mianJi = $getData['mianJi'];//面积
        if(empty($fid) || empty($mclass)  || empty($size) || !is_numeric($daXiao) || !is_numeric($mianJi)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
        }

        $countPe = M('tpapersize')->where(['fmediaclassid'=>$mclass,'fsize'=>$size,'fid'=>['neq',$fid]])->count();
        if(!empty($countPe)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'该媒体已存在同名规格'));
        }

        $addData['fmediaclassid'] = $mclass;
        $addData['fsize'] = $size;
        $addData['fdaxiao'] = $daXiao;
        $addData['fmianji'] = $mianJi;
        $addData['fmodifier'] = session('personData.fname');
        $addData['fmodifytime'] = date('Y-m-d H:i:s');
        $addPe = M('tpapersize')->where(['fid'=>$fid])->save($addData);
        if(!empty($addPe)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'修改失败，请尝试重新修改'));
        }
    }

    /*
    * 删除规格
    * by zw
    */
    public function delPaperSize(){
        $getData = $this->oParam;
        $fid = $getData['fid'];//规格ID
        if(empty($fid)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'参数有误'));
        }

        $delPe = M('tpapersize')->where(['fid'=>$fid])->save(['fstate'=>0]);
        if(!empty($delPe)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
        }
    }

}
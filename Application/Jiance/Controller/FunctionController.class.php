<?php
namespace Jiance\Controller;
use Think\Controller;

class FunctionController extends Controller {

	/**
	 * 获取时间条件，$years年份 $timetypes时间分类，$timeval查询时间值，$zd检索时间字段
	 * by zw
	 */
	public function getTimeCondition($years = '', $timetypes = '', $timeval = '', $zd = 'fissue_date'){
	    $years      = $years?$years:'';
	    $timetypes  = $timetypes;
	    $timeval    = $timeval?$timeval:Date("m");
	    $zd = $zd?$zd:'fissue_date';
	    $where_time =' 1=1 ';
	    if(!empty($years)){
	        switch ($timetypes) {
	          case 0:
	            $timese = explode(',',$timeval);
	            $starttime  = $timese[0];
	            $endtime    = $timese[1];
	            $where_time .= ' and date('.$zd.') between "'.$starttime.'" and "'.$endtime.'"';
	            break;
	          case 10:
	            $where_time .=' and year('.$zd.')='.$years;
	            $where_time .= ' and weekofyear('.$zd.')='.$timeval;
	            break;
	          case 20:
	            $month = floor($timeval/2);
	            $ts = $timeval%2;
	            if($ts == 1){
	                $starttime  = $years.'-'.($month+1).'-01';
	                if(($month+1) == 2){
	                    $endtime    = $years.'-'.($month+1).'-14';
	                }else{
	                    $endtime    = $years.'-'.($month+1).'-15';
	                }
	                $where_time .= ' and date('.$zd.') between "'.$starttime.'" and "'.$endtime.'"';
	            }else{
	                if(($month+1) == 2){
	                    $starttime  = $years.'-'.$month.'-15';
	                }else{
	                    $starttime  = $years.'-'.$month.'-16';
	                }
	                $where_time .= ' and date('.$zd.') between "'.$starttime.'" and LAST_DAY("'.$starttime.'")';
	            }
	            break;
	          case 30:
	            $where_time .=' and year('.$zd.')='.$years;
	            $where_time .= ' and month('.$zd.')='.$timeval;
	            break;
	          case 40:
	            $where_time .=' and year('.$zd.')='.$years;
	            $where_time .= ' and quarter('.$zd.')='.$timeval;
	            break;
	          case 50:
	            if($timeval == 1){
	              $starttime  = $years.'-01-01';
	              $endtime    = $years.'-06-30';
	            }else{
	              $starttime  = $years.'-07-01';
	              $endtime    = $years.'-12-31';
	            }
	            $where_time .= ' and date('.$zd.') between "'.$starttime.'" and "'.$endtime.'"';
	            break;
	          case 60:
	            $where_time .=' and year('.$zd.')='.$years;
	            break;
	        }
	    }

	    return $where_time;
	}

	/**
	 * 相同值判断
	 * by zw
	 */
	public function checkEditData($data1,$data2,$keyname1 = [],$keyname2 = []){
		$keyname2 = $keyname2?$keyname2:$keyname1;
		$res = [];//返回内容
		foreach ($keyname1 as $key => $value) {
			if($data1[$keyname1[$key]] != $data2[$keyname2[$key]] && isset($data2[$keyname2[$key]]) && isset($data1[$keyname1[$key]])){
				$res[$value] = $data1[$keyname1[$key]].' >>调整为>> '.$data2[$keyname2[$key]];
			}
		}
		return $res;
	}
}
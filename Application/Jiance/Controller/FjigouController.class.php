<?php
namespace Jiance\Controller;
use Think\Controller;

/**
 * 机构管理
 */

class FjigouController extends BaseController
{
	/**
	 * 获取机构下部门列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（list-列表数据）
	 * by zw
	 */
	public function get_jigoulist()
	{
		$getData = $this->oParam;
		$menutype = getconfig('menutype');
		$system_num = getconfig('system_num');

		$fid = session('personData.fregulatorid');
		$data[0] = M('tregulator')
			->field('tregulator.fid,tregulator.fpid,tregulator.fname,tregulator.fcode,tregulator.fkind,tregulator.fdescribe,a.acount')
			->join('(select fpid as pid,count(*) as acount from tregulator group by fpid) a on tregulator.fid=a.pid','left')
			->where('fid='.$fid)
			->find();
		$media_jurisdiction = M('tregulatormedia')->where('fregulatorcode='.$data[0]['fid'].' and fisoutmedia = 0 and fcustomer = "'.$system_num.'"')->getField('fmediaid',true);
		$menu_jurisdiction = M('tregulatormenu')->join('new_agpmenu on new_agpmenu.menu_id=tregulatormenu.fmenuid')->where('fmenutype='.$menutype.' and fstate=1 and fregulatorid='.$data[0]['fid'].' and tregulatormenu.fcustomer = "'.$system_num.'"')->getField('menu_id',true);

		$data[0]['media_jurisdiction'] 	= json_encode($media_jurisdiction);
		$data[0]['menu_jurisdiction'] 	= json_encode($menu_jurisdiction);
		$data[0]['list'] = D('Function')->get_alljgbumen($fid);

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('list'=>$data)));
	}

	/**
	 * 获取机构的菜单列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（list-列表数据）
	 * by zw
	 */
	public function get_jigoumenulist() {
		$menutype = getconfig('menutype');
		$do_ta = M('new_agpmenu')
	    	->where('menu_type='.$menutype)
	    	->order('menu_sort asc,menu_id asc')
	    	->getField('menu_id',true);

		$data = D('Function')->get_allmenu3($do_ta);
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('list'=>$data?$data:[])));
	}

	/**
	 * 获取机构的媒介列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（list-列表数据）
	 * by zw
	 */
	public function get_jigoumedialist()
	{
		session_write_close();
		$data = D('Function')->get_bumenmedia();

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data?$data:[]));
	}

	/**
	 * 添加机构部门
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function create_jigou(){
		$getData = $this->oParam;
		$menutype = getconfig('menutype');
		$system_num = getconfig('system_num');

		$fpid 		= $getData['fpid'];//父ID
		$fcode 		= make_num(9);//机构编码
		$fname 		= $getData['fname'];//机构名称
		$fdescribe 	= $getData['fdescribe'];//说明
		$fkind 		= I('fkind')?I('fkind'):2;//机构类型
		$menu 		= $getData['menu'];//菜单权限
		$media_list = $getData['media_list'];//媒体权限

		$do_tr 		= M('tregulator')->where('fcode='.$fcode)->count();
		while(!empty($do_tr)){
			$fcode = make_num(9);
			$do_tr = M('tregulator')->where('fcode='.$fcode)->count();
		}

		$data_tr['fpid'] 		= $fpid;
		$data_tr['ftype'] 		= $menutype;
		$data_tr['fname'] 		= $fname;
		$data_tr['fcode'] 		= $fcode;
		$data_tr['ffullname'] 	= $fname;
		$data_tr['fdescribe'] 	= $fdescribe;
		$data_tr['fkind'] 		= $fkind;
		$data_tr['fcreator'] 	= session('personData.fid');
		$data_tr['fcreatetime'] = date('Y-m-d');
		$data_tr['fstate'] 		= 1;
		if($fkind==2){
			$fregionid = D('Function')->get_xzquhua(D('Function')->get_tregulatoraction($fpid));
			$data_tr['fregionid'] 	= $fregionid;
		}else{
			$data_tr['fregionid']	= $getData['fregionid'];
		}

		$do_tr2 = M('tregulator')->add($data_tr);
		if(!empty($do_tr2)){
			if(!empty($menu)){
				$do_nau = M('new_agpmenu')->field('menu_id,parent_id')->where(array('menu_id'=>array('in',$menu)))->select();
				$addarr = [];
				foreach ($do_nau as $key => $value) {
					$addarr[] = array(
						'fregulatorid'	=>$do_tr2,
						'fmenutype'		=>$menutype,
						'fmenuid'		=>$value['menu_id'],
						'fmenupid'		=>$value['parent_id'],
						'fcreator'		=>session('personData.fid'),
						'fcreatetime'	=>date('Y-m-d H:i:s'),
						'fstate'		=>1,
						'fcustomer'		=>$system_num,
					);
				}
				M('tregulatormenu')->addAll($addarr);
			}

			if(!empty($media_list)){
				$addarr2 = [];
				foreach ($media_list as $key => $value) {
					$addarr2[] = array(
						'fregulatorcode'=>$do_tr2,
						'fmediaid'		=>$value,
						'fcreator'		=>session('personData.fid'),
						'fcreatetime'	=>date('Y-m-d H:i:s'),
						'fstate'		=>1,
						'fcustomer'	=>$system_num,
					);
				}
				M('tregulatormedia')->addAll($addarr2);
			}

			D('Function')->write_log('机构管理',1,'添加成功','tregulator',$do_tr2,M('tregulator')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功','data'=>$do_tr2));
		}else{
			D('Function')->write_log('机构管理',0,'添加失败','tregulator',0,M('tregulator')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/**
	 * 修改机构部门
	 * by zw
	 */
	public function edit_jigou(){
		$getData = $this->oParam;
		$menutype = getconfig('menutype');
		$system_num = getconfig('system_num');

		$fid 		= $getData['fid'];//机构ID
		$fname 		= $getData['fname'];//机构名称
		$fdescribe 	= $getData['fdescribe'];//说明
		$menu 		= $getData['menu'];//菜单权限
		$media_list = $getData['media_list'];//媒体权限
		$do_trview = M('tregulator')->field('fkind,fname,fregionid')->where(['fid'=>$fid])->find();

		$where_tn['fmenutype'] = $menutype;
		$where_tn['fregulatorid'] = $fid;
		$where_tn['fcustomer'] = $system_num;
		M('tregulatormenu')->where($where_tn)->delete();
		if(!empty($menu)){
			$do_nau = M('new_agpmenu')->field('menu_id,parent_id')->where(array('menu_id'=>array('in',$menu)))->select();
			$addarr = [];
			foreach ($do_nau as $key => $value) {
				$addarr[] = array(
					'fregulatorid'	=>$fid,
					'fmenutype'		=>$menutype,
					'fmenuid'		=>$value['menu_id'],
					'fmenupid'		=>$value['parent_id'],
					'fcreator'		=>session('regulatorpersonInfo.fid'),
					'fcreatetime'	=>date('Y-m-d H:i:s'),
					'fstate'		=>1,
					'fcustomer'			=>$system_num,
				);
			}
			M('tregulatormenu')->addAll($addarr);
		}

		M('tregulatormedia')->where(['fregulatorcode'=>$fid,'fisoutmedia'=>0,'fcustomer'=>$system_num])->delete();
		if(!empty($media_list)){
			$addarr2 = [];
			foreach ($media_list as $key => $value) {
				$addarr2[] = array(
					'fregulatorcode'	=>$fid,
					'fmediaid'			=>$value,
					'fcreator'			=>session('regulatorpersonInfo.fid'),
					'fcreatetime'		=>date('Y-m-d H:i:s'),
					'fstate'			=>1,
					'fcustomer'			=>$system_num,
				);
			}
			
			M('tregulatormedia')->addAll($addarr2);
		}

		$data_tr['fname'] 		= $fname;
		$data_tr['ffullname'] 	= $fname;
		$data_tr['fdescribe'] 	= $fdescribe;
		$data_tr['fmodifier'] 	= session('regulatorpersonInfo.fid');
		$data_tr['fmodifytime'] = date('Y-m-d');

		$do_tr2 = M('tregulator')->where(['fid'=>$fid])->save($data_tr);
		D('Function')->write_log('机构管理',1,'修改成功','tregulator',$fid,M('tregulator')->getlastsql());
		$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
	}

	/**
	 * 删除机构部门
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function delete_jigou(){
		$getData = $this->oParam;
		$fid 	= $getData['fid'];//机构id

		$do_tr 	= M('tregulator')->where(['fid'=>$fid])->delete();
		if(!empty($do_tr)){
			D('Function')->write_log('机构管理',1,'删除成功','tregulator',$fid,M('tregulator')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			D('Function')->write_log('机构管理',0,'删除失败','tregulator',$fid,M('tregulator')->getlastsql());
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}

	/**
	 * 获取机构下人员列表
	 * @return array|string code-状态（0失败1成功），msg-提示信息，data-数据（list-列表数据）
	 * by zw
	 */
	public function get_renyuanlist(){
		$getData = $this->oParam;
		$menutype = getconfig('menutype');
		$fjxsgl = getconfig('fjxsgl');
		$system_num = getconfig('system_num');

		$fregulatorid = $getData['fregulatorid'];//所属机构ID

		$where_tn['tregulator.fid'] = $fregulatorid;
		$where_tn['_string'] = 'tregulatorperson.fstate<>-1 and (plbcount = 0 or plbcount is null or plbcount2 = 1)';
		$do_tn = M('tregulatorperson')
			->field('tregulatorperson.fid,tregulatorperson.fname,tregulatorperson.fcode,tregulatorperson.fmobile,tregulatorperson.fduties,tregulatorperson.fstate,tregulatorperson.fdescribe')
			->join('tregulator on tregulatorperson.fregulatorid=tregulator.fid')
			->join('(select count(*) as plbcount,sum(case when fcustomer = "'.$system_num.'" then 1 else 0 end) as plbcount2,fpersonid from tpersonlabel  where fstate = 1 group by fpersonid) lb on tregulatorperson.fid = lb.fpersonid','left')
			->where($where_tn)
			->order('tregulatorperson.fid desc')
			->select();
		if(!empty($do_tn)){
			foreach ($do_tn as $key => $value) {
				$media_jurisdiction = M('tregulatorpersonmedia')->where('fstate=1 and fpersonid="'.$value['fid'].'" and fcustomer = "'.$system_num.'"')->getField('fmediaid',true);
				$menu_jurisdiction = M('tregulatorpersonmenu')->join('new_agpmenu on new_agpmenu.menu_id=tregulatorpersonmenu.fmenuid')->where('fmenutype='.$menutype.' and fstate=1 and fpersonid='.$value['fid'].' and tregulatorpersonmenu.fcustomer = "'.$system_num.'"')->getField('menu_id',true);
				if(!empty($media_jurisdiction)){
					$do_tn[$key]['media_jurisdiction'] 	= json_encode($media_jurisdiction);
				}else{
					$do_tn[$key]['media_jurisdiction'] 	= [];
				}
				if(!empty($menu_jurisdiction)){
					$do_tn[$key]['menu_jurisdiction'] 	= json_encode($menu_jurisdiction);
				}else{
					$do_tn[$key]['menu_jurisdiction'] 	= [];
				}

				if(!empty($fjxsgl)){//判断是否线索管理权限的设置
					$tregister_jurisdiction = M('fj_tregisterjur')->where('ftr_type=10 and ftr_objid='.$value['fid'])->getField('ftr_jurid',true);//线索管理人员权限
					if(!empty($tregister_jurisdiction)){
						$do_tn[$key]['tregister_jurisdiction'] 	= json_encode($tregister_jurisdiction);
					}else{
						$do_tn[$key]['tregister_jurisdiction'] 	= [];
					}
				}
			}
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>array('list'=>$do_tn)));
	}

	/**
	 * 添加人员
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function create_renyuan() {
		$getData = $this->oParam;
		$menutype = getconfig('menutype');
		$system_num = getconfig('system_num');
		$fjxsgl = getconfig('fjxsgl');

		$fpassword = $getData['fpassword']?$getData['fpassword']:123456;
		$fmobile = $getData['fmobile']?$getData['fmobile']:$getData['fcode'];

		$data['fregulatorid'] 	= $getData['fregulatorid'];//机构ID
		$data['fcode'] 			= $getData['fcode'];//人员编码
		$data['fname'] 			= $getData['fname'];//人员名称
		$data['fduties'] 		= $getData['fduties'];//人员职务
		$data['fmobile'] 		= $fmobile;//联系手机
		$data['fisadmin'] 		= 0;//是否超级管理员 0=》不是，1=》是
		$data['fdescribe'] 		= $getData['fdescribe'];//说明
		$data['fstate'] 		= $getData['fstate']?$getData['fstate']:1;//状态，-1删除，0-无效，1-有效
		$data['fpassword']		 = md5($fpassword);//密码
		$data['fpassword2']		 = md5(md5($fpassword));//双重密码
		$data['fcreator'] 		= session('personData.fname');//创建人
		$data['fcreatetime'] 	= date('Y-m-d H:i:s');//创建时间
		$menu 		= $getData['menu'];//菜单权限
		$media_list = $getData['media_list'];//媒体权限
		$tregister_jurisdiction = $getData['tregister_jurisdiction'];//线索管理权限

		$count_fcode = M('tregulatorperson')->where(array('fcode'=>$data['fcode']))->count();//查询人员代码是否重复
		if(!empty($count_fcode)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'人员代码重复'));//返回人员代码重复
		}
		$count_fmobile = M('tregulatorperson')->where(array('fmobile'=>$data['fmobile']))->count();//查询联系手机是否重复
		if(!empty($count_fmobile)){
			$this->ajaxReturn(array('code'=>-1,'msg'=>'联系手机重复'));//返回联系手机重复
		}
		$res = M('tregulatorperson')->add($data);//添加数据
		if($res){
			//设置菜单权限
			if(!empty($menu)){
				$do_nau = M('new_agpmenu')->field('menu_id,parent_id')->where(array('menu_id'=>array('in',$menu)))->select();
				$addarr = [];
				foreach ($do_nau as $key => $value) {
					$addarr[] = array(
						'fpersonid'		=>$res,
						'fmenutype'		=>$menutype,
						'fmenuid'		=>$value['menu_id'],
						'fmenupid'		=>$value['parent_id'],
						'fcreator'		=>session('personData.fid'),
						'fcreatetime'	=>date('Y-m-d H:i:s'),
						'fstate'		=>1,
						'fcustomer'		=>$system_num,
					);
				}
				M('tregulatorpersonmenu')->addAll($addarr);
			}

			//设置媒体权限
			if(!empty($media_list)){
				$addarr2 = [];
				foreach ($getData['media_list'] as $key => $value) {
					$addarr2[] = array(
						'fpersonid'		=>$res,
						'fmediaid'		=>$value,
						'fcreator'		=>session('personData.fid'),
						'fcreatetime'	=>date('Y-m-d H:i:s'),
						'fstate'		=>1,
						'fcustomer'		=>$system_num,
					);
				}
				M('tregulatorpersonmedia')->addAll($addarr2);
			}

			//设置登记权限
			if(!empty($tregister_jurisdiction) && !empty($fjxsgl)){
				$fregulatorpid = D('Function')->get_tregulatoraction($data['fregulatorid']);
				$addarr3 = [];
				foreach ($tregister_jurisdiction as $key => $value) {
					$addarr3[] = array(
						'ftr_type'		=>10,
						'ftr_trepid'	=>$fregulatorpid,
						'ftr_objid'		=>$res,
						'ftr_jurid'		=>$value,
						'ftr_creator'		=>session('personData.fid'),
						'ftr_createtime'	=>date('Y-m-d H:i:s'),
					);
					if($value == 0){
						$addarr3[] = array(
							'ftr_type'		=>10,
							'ftr_trepid'	=>$fregulatorpid,
							'ftr_objid'		=>$res,
							'ftr_jurid'		=>12,
							'ftr_creator'		=>session('personData.fid'),
							'ftr_createtime'	=>date('Y-m-d H:i:s'),
						);
					}elseif($value==20){
						$addarr3[] = array(
							'ftr_type'		=>10,
							'ftr_trepid'	=>$fregulatorpid,
							'ftr_objid'		=>$res,
							'ftr_jurid'		=>22,
							'ftr_creator'		=>session('personData.fid'),
							'ftr_createtime'	=>date('Y-m-d H:i:s'),
						);
						$addarr3[] = array(
							'ftr_type'		=>10,
							'ftr_trepid'	=>$fregulatorpid,
							'ftr_objid'		=>$res,
							'ftr_jurid'		=>23,
							'ftr_creator'		=>session('personData.fid'),
							'ftr_createtime'	=>date('Y-m-d H:i:s'),
						);
					}elseif($value==30){
						$addarr3[] = array(
							'ftr_type'		=>10,
							'ftr_trepid'	=>$fregulatorpid,
							'ftr_objid'		=>$res,
							'ftr_jurid'		=>32,
							'ftr_creator'		=>session('personData.fid'),
							'ftr_createtime'	=>date('Y-m-d H:i:s'),
						);
						$addarr3[] = array(
							'ftr_type'		=>10,
							'ftr_trepid'	=>$fregulatorpid,
							'ftr_objid'		=>$res,
							'ftr_jurid'		=>33,
							'ftr_creator'		=>session('personData.fid'),
							'ftr_createtime'	=>date('Y-m-d H:i:s'),
						);
					}
				}
				M('fj_tregisterjur')->addAll($addarr3);
			}

			//设置用户分组
			$data_pl['fpersonid'] = $res;
			$data_pl['fcustomer'] = $system_num;
			$data_pl['fcreator'] = session('personData.fid');
			$data_pl['fcreatetime'] = date('Y-m-d H:i:s');
			$data_pl['fstate'] = 1;
			M('tpersonlabel') -> add($data_pl);

			D('Function')->write_log('人员管理',1,'添加成功','tregulatorperson',$res,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));//返回成功
		}else{
			D('Function')->write_log('人员管理',0,'添加失败','tregulatorperson',0, M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'添加失败'));//返回失败
		}

	}

	/**
	 * 修改人员
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function edit_renyuan() {
		$getData = $this->oParam;
		$menutype = getconfig('menutype');
		$system_num = getconfig('system_num');
		$fjxsgl = getconfig('fjxsgl');

		$personid 				= $getData['fid'];//人员ID
		$personid 				= intval($personid);//转为数字
		$data['fcode'] 			= $getData['fcode'];//人员编码
		$data['fname'] 			= $getData['fname'];//人员名称
		$data['fduties'] 		= $getData['fduties'];//人员职务
		$data['fmobile'] 		= $getData['fmobile'];//联系手机
		$data['fdescribe'] 		= $getData['fdescribe'];//说明
		$data['fstate'] 		= $getData['fstate']?$getData['fstate']:1;//状态，-1删除，0-无效，1-有效
		$data['fmodifier'] 		= session('personData.fname');//创建人
		$data['fmodifytime'] 	= date('Y-m-d H:i:s');//创建时间
		$media_list = $getData['media_list'];//媒体权限
		$menu = $getData['menu'];//菜单权限
		$tregister_jurisdiction = $getData['tregister_jurisdiction'];//线索管理权限
		$fpassword = $getData['fpassword'];//密码
		if (!empty($fpassword)){
			$data['fpassword'] = md5($fpassword);//密码
			$data['fpassword2']	= md5(md5($fpassword));//双重密码
		}

		$do_tpn = M('tregulatorperson')->field('fregulatorid')->where(['fid'=>$personid])->find();
		if(empty($do_tpn)){
			$this->ajaxReturn(array('code' => -1, 'msg' => '未知错误'));
		}
		$count_fcode = M('tregulatorperson')->where(array('fid' => array('neq', $personid), 'fcode' => $data['fcode']))->count();//查询人员代码是否重复
		if ($count_fcode > 0) {
			$this->ajaxReturn(array('code' => -1, 'msg' => '人员代码重复'));
		}//返回人员代码重复
		if(!empty($data['fmobile'])){
			$count_fmobile = M('tregulatorperson')->where(array('fid' => array('neq', $personid), 'fmobile' => $data['fmobile']))->count();//查询联系手机是否重复
			if ($count_fmobile > 0) {
				$this->ajaxReturn(array('code' => -1, 'msg' => '联系手机重复'));//返回联系手机重复
			}
		}

		M('tregulatorpersonmenu')->where(['fpersonid'=>$personid,'fmenutype'=>$menutype,'fcustomer'=>$system_num])->delete();
		if(!empty($menu)){
			$do_nau = M('new_agpmenu')->field('menu_id,parent_id')->where(array('menu_id'=>array('in',$getData['menu'])))->select();
			$addarr = [];
			foreach ($do_nau as $key => $value) {
				$addarr[] = array(
					'fpersonid'		=>$personid,
					'fmenutype'		=>$menutype,
					'fmenuid'		=>$value['menu_id'],
					'fmenupid'		=>$value['parent_id'],
					'fcreator'		=>session('personData.fid'),
					'fcreatetime'	=>date('Y-m-d H:i:s'),
					'fstate'		=>1,
					'fcustomer'		=>$system_num,
				);
			}
			M('tregulatorpersonmenu')->addAll($addarr);
		}

		M('tregulatorpersonmedia')->where(['fpersonid'=>$personid,'fcustomer'=>$system_num])->delete();
		if(!empty($media_list)){
			$addarr2 = [];
			foreach ($getData['media_list'] as $key => $value) {
				$addarr2[] = array(
					'fpersonid'		=>$personid,
					'fmediaid'		=>$value,
					'fcreator'		=>session('personData.fid'),
					'fcreatetime'	=>date('Y-m-d H:i:s'),
					'fstate'		=>1,
					'fcustomer'		=>$system_num,
				);
			}
			M('tregulatorpersonmedia')->addAll($addarr2);
		}

		if(!empty($tregister_jurisdiction) && !empty($fjxsgl)){
			M('fj_tregisterjur')->where(['ftr_type'=>10,'ftr_objid'=>$personid])->delete();
			$fregulatorpid = D('Function')->get_tregulatoraction($do_tpn['fregulatorid']);
			$addarr3 = [];
			foreach ($tregister_jurisdiction as $key => $value) {
				$addarr3[] = array(
					'ftr_type'		=>10,
					'ftr_trepid'	=>$fregulatorpid,
					'ftr_objid'		=>$personid,
					'ftr_jurid'		=>$value,
					'ftr_creator'		=>session('personData.fid'),
					'ftr_createtime'	=>date('Y-m-d H:i:s'),
				);
				if($value == 0){
					$addarr3[] = array(
						'ftr_type'		=>10,
						'ftr_trepid'	=>$fregulatorpid,
						'ftr_objid'		=>$personid,
						'ftr_jurid'		=>12,
						'ftr_creator'		=>session('personData.fid'),
						'ftr_createtime'	=>date('Y-m-d H:i:s'),
					);
				}elseif($value==20){
					$addarr3[] = array(
						'ftr_type'		=>10,
						'ftr_trepid'	=>$fregulatorpid,
						'ftr_objid'		=>$personid,
						'ftr_jurid'		=>22,
						'ftr_creator'		=>session('personData.fid'),
						'ftr_createtime'	=>date('Y-m-d H:i:s'),
					);
					$addarr3[] = array(
						'ftr_type'		=>10,
						'ftr_trepid'	=>$fregulatorpid,
						'ftr_objid'		=>$personid,
						'ftr_jurid'		=>23,
						'ftr_creator'		=>session('personData.fid'),
						'ftr_createtime'	=>date('Y-m-d H:i:s'),
					);
				}elseif($value==30){
					$addarr3[] = array(
						'ftr_type'		=>10,
						'ftr_trepid'	=>$fregulatorpid,
						'ftr_objid'		=>$personid,
						'ftr_jurid'		=>32,
						'ftr_creator'		=>session('personData.fid'),
						'ftr_createtime'	=>date('Y-m-d H:i:s'),
					);
					$addarr3[] = array(
						'ftr_type'		=>10,
						'ftr_trepid'	=>$fregulatorpid,
						'ftr_objid'		=>$personid,
						'ftr_jurid'		=>33,
						'ftr_creator'		=>session('personData.fid'),
						'ftr_createtime'	=>date('Y-m-d H:i:s'),
					);
				}
			}
			M('fj_tregisterjur')->addAll($addarr3);
		}
		$res = M('tregulatorperson')->where(array('fid'=>$personid))->save($data);//修改数据

		if($res){//判断是否修改成功
			S('error_count'.$data['fcode'],0,86400);
			D('Function')->write_log('人员管理',1,'修改成功','tregulatorperson',$personid,M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));//返回成功
		}else{
			D('Function')->write_log('人员管理',0,'修改失败','tregulatorperson',$personid, M('tregulatorperson')->getlastsql());
			$this->ajaxReturn(array('code'=>-1,'msg'=>'修改失败'));//返回失败
		}
	}

	/**
	 * 删除人员
	 * @return array|string code-状态（0失败1成功），msg-提示信息
	 * by zw
	 */
	public function delete_renyuan(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//人员ID
		M('tregulatorperson')->where(array('fid'=>$fid))->save(array('fstate'=>-1));
		D('Function')->write_log('人员管理',1,'删除成功','tregulatorperson',$fid,M('tregulatorperson')->getlastsql());
		$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
	}

}

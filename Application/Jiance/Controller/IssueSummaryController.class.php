<?php
namespace Jiance\Controller;
use Think\Controller;
class IssueSummaryController extends BaseController {

	/*
	* by zw
	* 广告汇总
	*/
	public function index(){

		$getData = $this->oParam;
		$mediaClass = $getData['mediaClass']?$getData['mediaClass']:'01';//媒体类型（'01'，'02'，'03',''默认）
		$dimension = $getData['dimension']?$getData['dimension']:1;//维度（1条次，2违法条次，3条次违法率，5时长，6违法时长，7时长违法率）
		$classify = $getData['classify']?$getData['classify']:1;//查询分类（1地域，2广告类别，3媒体，4媒介机构）

		//时间条件筛选
	    $years      = $getData['years']?$getData['years']:date('Y');//选择年份
	    $timeTypes  = $getData['timeTypes'];//选择时间段
	    $timeVal    = $getData['timeVal']?$getData['timeVal']:date('m');//选择时间
	    $where_time = A('Function')->getTimeCondition($years,$timeTypes,$timeVal,'a.fissuedate');

	    //维度排序
	    if(!empty($dimension)){
	    	switch ($dimension) {
	    		case 1:
	    			$orders = 'ztiaoci desc';
	    			break;
	    		case 2:
	    			$orders = 'wftiaoci desc';
	    			break;
	    		case 3:
	    			$orders = 'wfltiaoci desc';
	    			break;
	    		case 5:
	    			$orders = 'zshichang desc';
	    			break;
	    		case 6:
	    			$orders = 'wfshichang desc';
	    			break;
	    		case 7:
	    			$orders = 'wflshichang desc';
	    			break;
	    		default:
	    			$orders = 'ztiaoci desc';
	    			break;
	    	}
	    }else{
	    	$this->ajaxReturn(array('code'=>1,'msg'=>'参数错误'));
	    }

	    if($mediaClass == '01'){
	    	$tb_str = 'tv';
	    	$fields = ',SUM(a.flength) zshichang, SUM(CASE WHEN b.fillegaltypecode > 0 THEN a.flength ELSE 0 END) wfshichang,IFNULL(SUM(CASE WHEN b.fillegaltypecode > 0 THEN a.flength ELSE 0 END)/ SUM(a.flength),0) wflshichang';
	    }elseif($mediaClass == '02'){
	    	$tb_str = 'bc';
	    	$fields = ',SUM(a.flength) zshichang, SUM(CASE WHEN b.fillegaltypecode > 0 THEN a.flength ELSE 0 END) wfshichang,IFNULL(SUM(CASE WHEN b.fillegaltypecode > 0 THEN a.flength ELSE 0 END)/ SUM(a.flength),0) wflshichang';
	    }elseif($mediaClass == '03' || $mediaClass == '04'){
	    	$tb_str = 'paper';
	    }

	    $where_time .= ' and a.fstate = 1';
	    $where_time .= ' and a.fsendstate in(10,20)';
	    if($classify == 1){
	    	$datasql = '
				SELECT g.fname regionname,COUNT(*) ztiaoci, SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END) wftiaoci,IFNULL(SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END)/ COUNT(*),0) wfltiaoci'.$fields.'
				FROM t'.$tb_str.'issue_'.$years.' a
				INNER JOIN t'.$tb_str.'sample b ON a.fsampleid = b.fid and b.fstate in(0,1)
				INNER JOIN tmedia f ON a.fmediaid = f.fid and f.fid = f.main_media_id and f.fstate = 1
				INNER JOIN tregion g ON concat(left(f.media_region_id,4),"00") = g.fid
				WHERE '.$where_time.' 
				GROUP BY g.fid
				order by 
			'.$orders;
	    }elseif($classify == 2){
	    	$datasql = '
				SELECT i.ffullname adclassname,COUNT(*) ztiaoci, SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END) wftiaoci,IFNULL(SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END)/ COUNT(*),0) wfltiaoci'.$fields.'
				FROM t'.$tb_str.'issue_'.$years.' a
				INNER JOIN t'.$tb_str.'sample b ON a.fsampleid = b.fid and b.fstate in(0,1)
				INNER JOIN tad h on b.fadid = h.fadid
				INNER JOIN tadclass i ON i.fcode = left(h.fadclasscode,2)
				WHERE '.$where_time.' 
				GROUP BY i.fcode
				order by 
			'.$orders;
	    }elseif($classify == 3){
	    	$datasql = '
				SELECT (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame medianame,COUNT(*) ztiaoci, SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END) wftiaoci,IFNULL(SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END)/ COUNT(*),0) wfltiaoci'.$fields.'
				FROM t'.$tb_str.'issue_'.$years.' a
				INNER JOIN t'.$tb_str.'sample b ON a.fsampleid = b.fid and b.fstate in(0,1)
				INNER JOIN tmedia f ON a.fmediaid = f.fid and f.fid = f.main_media_id and f.fstate = 1
				WHERE '.$where_time.' 
				GROUP BY f.fid
				order by 
			'.$orders;
	    }elseif($classify == 4){
	    	$datasql = '
				SELECT j.fname mediaownername,COUNT(*) ztiaoci, SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END) wftiaoci,IFNULL(SUM(CASE WHEN b.fillegaltypecode > 0 THEN 1 ELSE 0 END)/ COUNT(*),0) wfltiaoci'.$fields.'
				FROM t'.$tb_str.'issue_'.$years.' a
				INNER JOIN t'.$tb_str.'sample b ON a.fsampleid = b.fid and b.fstate in(0,1)
				INNER JOIN tmedia f ON a.fmediaid = f.fid and f.fid = f.main_media_id and f.fstate = 1
				INNER JOIN tmediaowner j on f.fmediaownerid = j.fid
				WHERE '.$where_time.' 
				GROUP BY j.fid
				order by 
			'.$orders;
	    }else{
	    	$this->ajaxReturn(array('code'=>1,'msg'=>'参数错误'));
	    }
		$data = M()->query($datasql);

		foreach ($data as $key => $value) {
			$data[$key]['wfltiaoci'] = number_format($value['wfltiaoci']*100,2).'%';
			if($mediaClass == '01' || $mediaClass == '02'){
				$data[$key]['wflshichang'] = number_format($value['wflshichang']*100,2).'%';
			}
		}

		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>count($data),'data'=>$data));

	}

}
<?php
namespace Jiance\Controller;
use Think\Controller;
class FilterManageController extends BaseController {

	/*
	* 过滤规则列表
	* by zw
	*/
	public function filterList(){
		$getData = $this->oParam;
        $pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
        $medianame = $getData['medianame'];//媒体名称

        if(!empty($medianame)){
            $where_nc['b.fmedianame'] = ['like','%'.$medianame.'%'];
        }
        $where_nc['a.fstatus'] = 1;
        $count = M('tfilter')
            ->alias('a')
            ->join('tmedia b on a.fmediaid = b.fid')
            ->where($where_nc)
            ->count();

        $data = M('tfilter')
            ->alias('a')
            ->field('a.*,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,left(b.fmediaclassid,2) mclass')
            ->join('tmedia b on a.fmediaid = b.fid')
            ->where($where_nc)
            ->order('fcreatetime desc')
            ->limit($limitIndex,$pageSize)
            ->select();

        foreach ($data as $key => $value) {
            if(!empty($value['fadname'])){
                $data[$key]['other'] .= '广告名：'.$value['fadname'].'；';
            }
            if(!empty($value['fadclasscode'])){
                $adclassname = M('tadclass')->cache(true,3600)->where(['fcode'=>$value['fadclasscode']])->getField('ffullname');
                $data[$key]['other'] .= '广告类别：'.$adclassname.'；';
            }
            if(!empty($value['fstime']) && !empty($value['fetime'])){
                $data[$key]['other'] .= '发布开始时段：'.date('H:i:s',strtotime($value['fstime'])).'至'.date('H:i:s',strtotime($value['fetime'])).'；';
            }
            if(!empty($value['flength'])){
                $data[$key]['other'] .= '时长：'.$value['flength'].'秒内；';
            }
        }
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	}

    /*
    * 过滤规则添加、修改
    * by zw
    */
    public function filterCreate(){
        $getData = $this->oParam;
        $fid = $getData['fid'];//主键
        $mediaid = $getData['mediaid'];//媒体id
        $ftime = $getData['ftime'];//时段
        $fadname = $getData['fadname'];//广告名称
        $fadclasscode = $getData['fadclasscode'];//广告类别
        $flength = $getData['flength'];//时长

        if(empty($mediaid)){
            $this->ajaxReturn(array('code'=>1,'msg'=>'媒体必选'));
        }
        if(empty($ftime)){
            if(empty($fadname) && empty($fadclasscode) && empty($flength)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'请选择过滤规则'));
            }
        }else{
            if(strtotime($ftime[0])>=strtotime($ftime[1])){
                $this->ajaxReturn(array('code'=>1,'msg'=>'时间选择有误'));
            }
        }

        $addData['fmediaid'] = $mediaid?$mediaid:0;
        $addData['fstime'] = $ftime?$ftime[0]:null;
        $addData['fetime'] = $ftime?$ftime[1]:null;
        $addData['fadname'] = $fadname?$fadname:'';
        $addData['fadclasscode'] = $fadclasscode?$fadclasscode:'';
        $addData['flength'] = $flength?$flength:0;
        if(!empty($fid)){
            $addData['fedittime'] = date('Y-m-d H:i:s');
            $addData['fediter'] = session('personData.fname');
            $addFr = M('tfilter')->where(['fid'=>$fid])->save($addData);
        }else{
            $addData['fcreatetime'] = date('Y-m-d H:i:s');
            $addData['fcreater'] = session('personData.fname');
            $addFr = M('tfilter')->add($addData);
        }

        if(!empty($addFr)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'操作成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'操作失败'));
        }
    }

    /*
    * 过滤规则删除
    * by zw
    */
    public function filterDelete(){
        $getData = $this->oParam;
        $fid = $getData['fid'];//主键
        $delFr = M('tfilter')->where(['fid'=>$fid])->save(['fstatus'=>-1,'fedittime'=>date('Y-m-d H:i:s'),'fediter'=>session('personData.fname')]);
        if(!empty($delFr)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
        }
    }
	
}
<?php
namespace Jiance\Controller;
use Think\Controller;
class ReportController extends BaseController {

	/*
	* by zw
	* 获取报告列表
	*/
	public function reportList(){
		$getData = $this->oParam;
		$systemtype = C('FORCE_NUM_CONFIG');
		$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $limitIndex = ($pageIndex-1)*$pageSize;
        $nTitle = $getData['nTitle'];//标题
		$nType = $getData['nType'];//类型，11日报，12周报，13半月报，14月报，15专项报告，16季报，17半年报，18年报，19预警报告，20其他
		$issueDate = $getData['issueDate'];//日期范围，数组
		$flooktype = $getData['flooktype'];//查看权限
		$ffileDownType = $getData['ffileDownType'];//附件下载权限

		if(!empty($nType)){
			$where_nc['a.ftype'] = $nType;
		}else{
			$where_nc['a.ftype'] = ['between',[11,20]];
		}

		if(!empty($nTitle)){
			$where_nc['a.ftitle'] = ['like','%'.$nTitle.'%'];
		}

		if(!empty($issueDate)){
			$where_nc['a.fissue_date'] = ['between',[$issueDate[0],$issueDate[1]]];
		}

		if($flooktype != ''){
			$where_nc['a.flooktype'] = $flooktype;
		}

		$where_nc['a.fcreate_regid'] = session('personData.fregulatorpid');

		$where_nc['a.fsystem_type'] = $systemtype;

		$count = M('tnotice a')
        	->join('tregion b on RPAD(a.fregionid, 6, 0) = b.fid')
			->where($where_nc)
        	->count();

		$data = M('tnotice a')
			->field('a.fid,a.flooktype,a.ftype,a.ftitle,a.fissue_date,a.fcontent,a.fiscontain,RPAD(a.fregionid, 6, 0) fregionid,b.fname1 regionname,ffiledowntype,RPAD(a.ffiledown_regionid, 6, 0) ffiledown_regionid,c.fname1 downregionname,a.ffiledown_iscontain,a.ffiles')
			->join('tregion b on RPAD(a.fregionid, 6, 0) = b.fid')
			->join('tregion c on RPAD(a.ffiledown_regionid, 6, 0) = c.fid')
			->where($where_nc)
			->order('a.fissue_date desc,a.fid desc')
			->limit($limitIndex,$pageSize)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	}

	/*
	* by zw
	* 报告添加
	*/
	public function reportCreate(){
		$systemtype = C('FORCE_NUM_CONFIG');
		$getData = $this->oParam;
		$nTitle = $getData['nTitle'];//标题
		$nType = $getData['nType'];//类型，11日报，12周报，13半月报，14月报，15专项报告，16季报，17半年报，18年报，19预警报告，20其他
		$nContent = $getData['nContent'];//内容
		$issueDate = $getData['issueDate']?$getData['issueDate']:date('Y-m-d');//报告日期
		$flooktype = $getData['flooktype'];//查看权限
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区，1包含，0不包含，不传默认1
		$ffiledowntype = $getData['ffiledowntype'];//下载权限
		$ffiledown_regionid = $getData['ffiledown_regionid']?$getData['ffiledown_regionid']:330000;//行政区划
		$ffiledown_iscontain = $getData['ffiledown_iscontain'] != ''?$getData['ffiledown_iscontain']:1;//是否包含下属地区，1包含，0不包含，不传默认1
		$ffiles = $getData['ffiles'];//报告附件


		if(empty($nTitle) || empty($nType) || empty($nContent)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'信息填写不完整'));
		}

		//地区查看权限
		if(!empty($iscontain)){
			$region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
	        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断国家
	        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
	        $data_nc['fregionid'] = $region_id_rtrim;
		}else{
			$data_nc['fregionid'] = $area;
		}

		//地区下载权限
		if(!empty($ffiledown_iscontain)){
			$region_id_rtrim = rtrim($ffiledown_regionid,'00');//去掉地区后面两位0，判断区
	        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断国家
	        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
	        $data_nc['ffiledown_regionid'] = $region_id_rtrim;
		}else{
			$data_nc['ffiledown_regionid'] = $ffiledown_regionid;
		}

		$data_nc['fiscontain'] = $iscontain;
		$data_nc['flooktype'] = $flooktype?$flooktype:0;
		$data_nc['ffiledown_iscontain'] = $ffiledown_iscontain;
		$data_nc['ffiledowntype'] = $ffiledowntype?$ffiledowntype:0;
		$data_nc['ftitle'] = $nTitle;
		$data_nc['ftype'] = $nType;
		$data_nc['ffiles'] = $ffiles;
		$data_nc['fcontent'] = $nContent;
		$data_nc['fissue_date'] = $issueDate;
		$data_nc['fsystem_type'] = $systemtype;
		$data_nc['fcreate_regid'] = session('personData.fregulatorpid');
		$data_nc['fcreate_time'] = date('Y-m-d H:i:s');
		$data_nc['fcreate_userid'] = session('personData.fid');
		$data_nc['fcreate_user'] = session('personData.fname');
		$do_nc = M('tnotice')->add($data_nc);
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'添加成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'添加失败'));
		}
	}

	/*
	* by zw
	* 报告修改
	*/
	public function reportSave(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//ID
		$nTitle = $getData['nTitle'];//标题
		$nType = $getData['nType'];//类型，11日报，12周报，13半月报，14月报，15专项报告，16季报，17半年报，18年报，19预警报告，20其他
		$nContent = $getData['nContent'];//内容
		$issueDate = $getData['issueDate']?$getData['issueDate']:date('Y-m-d');//报告日期
		$flooktype = $getData['flooktype'];//查看权限
		$area = $getData['area']?$getData['area']:330000;//行政区划
		$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区，1包含，0不包含，不传默认1
		$ffiledowntype = $getData['ffiledowntype'];//下载权限
		$ffiledown_regionid = $getData['ffiledown_regionid']?$getData['ffiledown_regionid']:330000;//行政区划
		$ffiledown_iscontain = $getData['ffiledown_iscontain'] != ''?$getData['ffiledown_iscontain']:1;//是否包含下属地区，1包含，0不包含，不传默认1
		$ffiles = $getData['ffiles'];//报告附件

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}
		if(empty($nTitle) || empty($nType) || empty($nContent)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'信息填写不完整'));
		}

		//地区查看权限
		if(!empty($iscontain)){
			$region_id_rtrim = rtrim($area,'00');//去掉地区后面两位0，判断区
	        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断国家
	        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
	        $data_nc['fregionid'] = $region_id_rtrim;
		}else{
			$data_nc['fregionid'] = $area;
		}

		//地区下载权限
		if(!empty($ffiledown_iscontain)){
			$region_id_rtrim = rtrim($ffiledown_regionid,'00');//去掉地区后面两位0，判断区
	        $region_id_rtrim = rtrim($region_id_rtrim,'000');//去掉后面三位0，判断国家
	        $region_id_rtrim = rtrim($region_id_rtrim,'00');//去掉后面两位0，判断市
	        $data_nc['ffiledown_regionid'] = $region_id_rtrim;
		}else{
			$data_nc['ffiledown_regionid'] = $ffiledown_regionid;
		}

		$data_nc['fiscontain'] = $iscontain;
		$data_nc['flooktype'] = $flooktype?$flooktype:0;
		$data_nc['ffiledown_iscontain'] = $ffiledown_iscontain;
		$data_nc['ffiledowntype'] = $ffiledowntype?$ffiledowntype:0;
		$data_nc['ftitle'] = $nTitle;
		$data_nc['ftype'] = $nType;
		$data_nc['ffiles'] = $ffiles;
		$data_nc['fcontent'] = $nContent;
		$data_nc['fissue_date'] = $issueDate;
		$data_nc['fmodify_time'] = date('Y-m-d H:i:s');
		$data_nc['fmodify_userid'] = session('personData.fid');
		$data_nc['fmodify_user'] = session('personData.fname');
		$do_nc = M('tnotice')->where(['fid'=>$fid])->save($data_nc);
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'保存成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'保存失败，信息不存在'));
		}
	}

	/*
	* by zw
	* 报告删除
	*/
	public function reportDel(){
		$getData = $this->oParam;
		$fid = $getData['fid'];//ID

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

		$do_nc = M('tnotice')->where(['fid'=>$fid])->delete();
		if(!empty($do_nc)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败，信息不存在'));
		}
	}

	/*
	* by zw
	* 报告查看
	*/
	public function reportView(){
		$systemtype = C('FORCE_NUM_CONFIG');
		$getData = $this->oParam;
		$fid = $getData['fid'];//ID

		if(empty($fid)){
			$this->ajaxReturn(array('code'=>1,'msg'=>'缺少必要参数'));
		}

		$where_nc['a.ftype'] = ['between',[11,20]];
		$where_nc['a.fid'] = $fid;
		$where_nc['a.fsystem_type'] = $systemtype;

		$data = M('tnotice a')
			->field('a.fid,a.flooktype,a.ffiledowntype,a.ffiledown_iscontain,a.ftype,a.ftitle,a.fissue_date,a.fcontent,a.fiscontain,RPAD(a.fregionid, 6, 0) fregionid,RPAD(a.ffiledown_regionid, 6, 0) ffiledown_regionid,a.ffiles,c.fname1 downregionname')
			->join('tregion b on RPAD(a.fregionid, 6, 0) = b.fid')
			->join('tregion c on RPAD(a.ffiledown_regionid, 6, 0) = c.fid')
			->where($where_nc)
			->find();
		if(!empty($data)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'获取失败，信息不存在'));
		}
	}

}
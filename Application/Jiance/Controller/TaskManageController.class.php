<?php
namespace Jiance\Controller;
use Think\Controller;
class TaskManageController extends BaseController {

	/*
	* by zw
	* 广告监测任务清单及状态
	*/
	public function taskList(){
        session_write_close();
        $FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

        $getData = $this->oParam;
        $pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
        $mediaType = $getData['mediaType']!=''?$getData['mediaType']:-1;//媒体所属（1串播单，2非串播单，3省级，4市级，5区县级,-1默认）
        $mediaClass = $getData['mediaClass'];//媒体类型，数组（'01'，'02'，'03',''默认）
        $mediaName = $getData['mediaName'];//媒体名
        $month = $getData['month'];//任务月份
        $area = $getData['area'];//行政区划
        $iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区，1包含，0不包含，不传默认1
        $fisxinyong = $getData['fisxinyong']?$getData['fisxinyong']:[];//是否信用评价，数组，1是，2否，空是全部
        $taskLink = $getData['taskLink']?$getData['taskLink']:[];//任务环节，数组，1待接收 2广告分离 3广告监测 4广告复审 5广告终审 6已归档 7中止 9退回 10关闭


        if(empty($month)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }
        
        $limitIndex = ($pageIndex-1)*$pageSize;
        $startDate = $month.'-01';
        $endDate = date('Y-m-d', strtotime($startDate.' +1 month -1 day'));

        $where_mie['_string'] = '1=1';
        //媒体所属
        switch ((int)$mediaType) {
        	case 0:
        		$where_mie['_string'] .= ' and b.fid in (select fmediaid from tmedialabel where flabelid in(446,449))';
        		break;
        	case 1:
        		$where_mie['_string'] .= ' and b.fid not in (select fmediaid from tmedialabel where flabelid in(446,449))';
        		break;
        	case 2:
        		$where_mie['f.flevel'] = 1;
                break;
        	case 3:
        		$where_mie['f.flevel'] = ['in',[2,3,4]];
                break;
        	case 4:
        		$where_mie['f.flevel'] = 5;
                break;
        }

        //是否信用评价媒体
        if(count($fisxinyong) == 1){
            if(in_array(1, $fisxinyong)){
                $where_mie['b.fisxinyong'] = 1;
            }elseif(in_array(2, $fisxinyong)){
                $where_mie['b.fisxinyong'] = 0;
            }
        }

        //地域
        if(!empty($area)){
            if(!empty($iscontain)){
                $tregion_len = get_tregionlevel($area);
                if($tregion_len == 2){//省级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,2).'%'];
                }elseif($tregion_len == 4){//市级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,4).'%'];
                }elseif($tregion_len == 6){//县级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,6).'%'];
                }
            }else{
                $where_mie['b.media_region_id'] = $area;
            }
        }
        //媒体类型
        if(!empty($mediaClass)){
            $where_mie['left(b.fmediaclassid,2)'] = ['in',$mediaClass];
        }
        //媒体名称
        if(!empty($mediaName)){
            $where_mie['b.fmedianame'] = ['like','%'.$mediaName.'%'];
        }

        //环节查询
        $taskLinkWhereArr = [];
        $taskLinkWhereArr2 = '';
        if(in_array(1, $taskLink)){
            $taskLinkWhereArr2 = 'SELECT DISTINCT(a.fmedia_id) mids FROM tmediaissue a WHERE  (a.fstatus = 0) AND a.fissue_date between "'.$startDate.'" and "'.$endDate.'"';
        }
        if(in_array(2, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 1 and c.fstatus = 1 and d.fflow_code = "mediaissue_ggfl"';
        }
        if(in_array(3, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 1 and c.fstatus = 1 and d.fflow_code = "mediaissue_ggjc"';
        }
        if(in_array(4, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 1 and c.fstatus = 1 and d.fflow_code = "mediaissue_ggfs"';
        }
        if(in_array(5, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 1 and c.fstatus = 1 and d.fflow_code = "mediaissue_ggzs"';
        }
        if(in_array(6, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 1 and c.fstatus = 5';
        }
        if(in_array(7, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 1 and c.fstatus = 9';
        }
        if(in_array(9, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 10';
        }
        if(in_array(10, $taskLink)){
            $taskLinkWhereArr[] = 'a.fstatus = 4';
        }
        if(!empty($taskLinkWhereArr)){
            $taskLinkWhere[] .= ' b.fid in (select distinct(a.fmedia_id) mids from tmediaissue a,ttask c,(select fflow,ftask_id,fflow_code from ttask_flow where fstatus = 0) d where a.fid = c.fbiz_main_id and c.fid = d.ftask_id and ('.implode(' or ', $taskLinkWhereArr).') and a.fissue_date between "'.$startDate.'" and "'.$endDate.'")';
        }
        if(!empty($taskLinkWhereArr2)){
            $taskLinkWhere[] = ' b.fid in ('.$taskLinkWhereArr2.')';
        }
        if(!empty($taskLinkWhere)){
            $where_mie['_string'] .= ' and ('.implode(' or ', $taskLinkWhere).')';
        }

        $where_mie['b.fstate'] = 1;
        $where_mie['g.fstate'] = 1;
        $where_mie['g.fcustomer'] = $FORCE_NUM_CONFIG_JIANCE;

        $count = M('tmedia')
            ->alias('b')
            ->join('tregulatormedia g on b.fid = g.fmediaid and g.fregulatorcode = '.$FORCE_NUM_CONFIG_JIANCE)
            ->join('tregion f on b.media_region_id = f.fid')
            ->where($where_mie)
            ->count();

        $mediaData = M('tmedia')
            ->alias('b')
            ->field('b.fid,(case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) fmedianame,left(b.fmediaclassid,2) media_class,f.fname regionname')
            ->join('tregulatormedia g on b.fid = g.fmediaid and g.fregulatorcode = '.$FORCE_NUM_CONFIG_JIANCE)
            ->join('tregion f on b.media_region_id = f.fid')
            ->where($where_mie)
            ->limit($limitIndex,$pageSize)
            ->order('f.fid asc,left(b.fmediaclassid,2) asc,b.fid asc')
            ->select();

        $mediaIds = array_column($mediaData, 'fid');
        $taskData = [];
        if(!empty($mediaIds)){
            $taskData = M('tmediaissue')
                ->alias('a')
                ->field('a.fmedia_id,a.fissue_date,a.fstatus,c.fstatus cstatus,d.fflow,a.fis_credit')
                ->join('ttask c on a.fid = c.fbiz_main_id','left')
                ->join('(select fflow,ftask_id,fflow_code from ttask_flow where fstatus = 0) d on c.fid = d.ftask_id','left')
                ->where(['a.fmedia_id' =>['in',$mediaIds],'a.fissue_date'=>['between',[$startDate,$endDate]]])
                ->select();

            foreach ($taskData as $key => $value) {
                if($value['fstatus'] == 1){
                    if($value['cstatus'] == 5){
                        $taskData[$key]['fflow'] = '归档';
                    }elseif($value['cstatus'] == 9){
                        $taskData[$key]['fflow'] = '中止';
                    }elseif($value['cstatus'] == 1){
                        $taskData[$key]['fflow'] = mb_substr($value['fflow'], 2,2,'utf-8');
                    }
                }elseif($value['fstatus'] == 10){
                    $taskData[$key]['fflow'] = '退回';
                }elseif($value['fstatus'] == 4){
                    $taskData[$key]['fflow'] = '关闭';
                }else{
                    $taskData[$key]['fflow'] = '待收';
                }
            }

        }
        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'mediaData'=>$mediaData,'taskData'=>$taskData));
	}

	/*
	* by zw
	* 任务修改
	*/
	public function taskAction(){
        session_write_close();
        ini_set('max_execution_time', 600);//设置超时时间
		$getData = $this->oParam;
		$fixDays = $getData['fixDays'];//媒体任务设置
        $action = $getData['action'];//操作，1添加，2删除，3退回，4流转，5任务关闭，7重新接收，8开启信用统计，9关闭信用统计
        $backContent = $getData['backContent'];//退回原因
        $flowLink = $getData['flowLink'];//操作为流转时的流转环节，此时必传，ggfl广告分离，ggjc广告监测，ggfs广告复审，ggzs广告终审
        // $fixDays = [
        //     '11000010002359'=>[
        //         '2019-11-01','2019-11-02','2019-11-03','2019-11-04'
        //     ]
        // ];
        // $action = 4;
        // $flowLink = 'ggfs';

        if(empty($fixDays)||empty($action)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }
        if($action == 3 && empty($backContent)){
            $this->ajaxReturn(['code'=>1,'msg'=>'请输入退回原因']);
        }
        if($action == 4){//流转验证
            if(empty($flowLink)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'请选择流转环节'));
            }else{
                $biz_def_flow = M('tbiz_def_flow')->where(['fbiz_def_id'=>1])->select();
                $flows = array_column($biz_def_flow,'fflow_code');
                $flowName = array_column($biz_def_flow,'fflow','fflow_code');
                if(!in_array('mediaissue_'.$flowLink, $flows) && $flowLink <> 'gggd'){
                    $this->ajaxReturn(array('code'=>1,'msg'=>'流转环节有误'));
                }
            }
        }

		$addData = [];
        $actionIds = [];
        $actionData = [];
        $updateFlowIds = [];
        $allCount = 0;
        $finishCount = 0;
        foreach ($fixDays as $key => $value) {
            $value = explode(',', $value);
            $allCount += count($value);
        	if($action == 1){//添加
                $mclass = substr(M('tmedia')->where(['fid'=>$key])->getField('fmediaclassid'),0,2);
        		$dates = M('tmediaissue')->where(['fmedia_id'=>$key,'fissue_date'=>['in',$value]])->getField('fissue_date',true);
	        	$dates = $dates?$dates:[];
	        	$dateDiff = array_diff($value, $dates);
	        	foreach ($dateDiff as $mkey => $mvalue) {
                    if($mclass == '01' || $mclass == '02'){//电视广播直接添加任务待接收
                        $addData[] = [
                            'fmedia_id' => $key,
                            'fissue_date' => $mvalue,
                            'fcreate_userid' => session('personData.fid'),
                            'fcreate_user' => session('personData.fname'),
                            'fcreate_time' => date('Y-m-d H:i:s'),
                            'fstatus' => 0,
                            'fpriority' => time(),
                        ];
                    }else{//报纸期刊直接进入分离状态
                        $addNowTask = [
                            'fmedia_id' => $key,
                            'fissue_date' => $mvalue,
                            'fcreate_userid' => session('personData.fid'),
                            'fcreate_user' => session('personData.fname'),
                            'fcreate_time' => date('Y-m-d H:i:s'),
                            'fstatus' => 1,
                            'fpriority' => time(),
                        ];
                        $mediaissue_task = M('tmediaissue')->add($addNowTask);//添加业务任务
                        if(!empty($mediaissue_task)){
                            //获取业务信息
                            $fbiz_def_code = 'zj_mediaissue';//业务编号
                            $fbiz_def_flow_code = 'mediaissue_ggfl';//环节编号
                            $where_biz['a.fcode'] = $fbiz_def_code;
                            $where_biz['b.fflow_code'] = $fbiz_def_flow_code;
                            $where_biz['a.fstatus'] = 1;
                            $do_biz = M('tbiz_def')
                                ->cache(true,3600)
                                ->field('a.fid,a.fname,b.fid flowid,b.fflow')
                                ->alias('a')
                                ->join('tbiz_def_flow b on a.fid = b.fbiz_def_id')
                                ->where($where_biz)
                                ->find();

                            if(!empty($do_biz)){
                                //判断业务任务是否存在
                                $task_view = M('ttask')->where(['fbiz_def_id'=>$do_biz['fid'],'fbiz_main_id'=>$mediaissue_task])->find();
                                if(empty($task_view)){
                                    $add_task_data = [
                                        'fbiz_def_id'=>$do_biz['fid'],
                                        'ftitle'=>$do_biz['fname'],
                                        'fbiz_main_id'=>$mediaissue_task,
                                        'fcreate_time'=>date('Y-m-d'),
                                        'fstatus'=>1
                                    ];
                                    $task_id = M('ttask')->add($add_task_data);
                                }else{
                                    $task_id = $task_view['fid'];
                                    if($task_view['fstatus'] == 5){
                                        M('ttask')->where(['fid'=>$task_id])->save(['fstatus'=>1]);
                                    }
                                }
                                    
                                if(!empty($task_id)){
                                    //需要新增的流程
                                    $add_taskflow_data = [
                                        'ftask_id' => $task_id,
                                        'fflow_code' => $fbiz_def_flow_code,
                                        'fflow' => $do_biz['fflow'],
                                        'fsend_task_flow_id' => 0,
                                        'fsend_reg' => '系统',
                                        'fsend' => 'AI',
                                        'fsend_time' => date('Y-m-d H:i:s'),
                                        'fstatus' => 0,
                                    ];

                                    //判断业务任务流程是否存在
                                    $taskflow_id = M('ttask_flow')
                                        ->where(['ftask_id'=>$task_id,'fstatus'=>0])
                                        ->getField('fid');
                                    if(!empty($taskflow_id)){
                                        //更新上一流程
                                        $flow_data['frece_id'] = 0;
                                        $flow_data['frece'] = 'AI';
                                        $flow_data['frece_time'] = date('Y-m-d H:i:s');
                                        $flow_data['fstatus'] = 2;
                                        $flow_data['ffinish_type'] = 3;
                                        M('ttask_flow')->where(['fid'=>$taskflow_id])->save($flow_data);

                                        $add_taskflow_data['fsend_msg'] = '分离重置';
                                        $add_taskflow_data['fsend_task_flow_id'] = $taskflow_id;
                                    }
                                    
                                    $add_flow = M('ttask_flow')->add($add_taskflow_data);//添加流程操作
                                    if(!empty($add_flow)){
                                        $finishCount += 1;
                                    }
                                }
                            }
                        }
                    }
                            
	        	}
        	}elseif($action == 2){//删除
        		$del_issue = M('tmediaissue')->where(['fmedia_id'=>$key,'fissue_date'=>['in',$value],'fstatus'=>['in',[0,4,10]]])->delete();
        		$finishCount += $del_issue;
        	}elseif($action == 3){//退回
                $doTask = M('tmediaissue')
                    ->alias('a')
                    ->field('a.fid,a.fmedia_id,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,a.fissue_date,d.flowid,c.fid taskid,d.fflow_code')
                    ->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
                    ->join('tregion f on b.media_region_id = f.fid')
                    ->join('ttask c on a.fid = c.fbiz_main_id','left')
                    ->join('(select fflow_code,fflow,ftask_id,fid flowid from ttask_flow where fstatus = 0) d on c.fid = d.ftask_id','left')
                    ->where(['a.fmedia_id'=>$key,'a.fissue_date'=>['in',$value],'a.fstatus'=>1])
                    ->select();
                $finishCount += count($doTask);
                foreach ($doTask as $bkey => $bvalue) {
                    if(empty($actionData[$bvalue['fmedianame']])){
                        $actionData[$bvalue['fmedianame']]['fmedia_id'] = $bvalue['fmedia_id'];//退回的媒体信息
                    }
                    $actionData[$bvalue['fmedianame']]['fissue_date'][] = $bvalue['fissue_date'];//记录退回日期
                    $actionIds[] = $bvalue['fid'];//退回的需要调整任务状态的任务ID
                    if(!empty($bvalue['flowid'])){
                        $updateFlowIds = $bvalue['flowid'];//退回的需要调整任务流程状态的流程ID
                    }
                }

            }elseif($action == 4){//流转
                $doTask = M('tmediaissue')
                    ->alias('a')
                    ->field('a.fid,a.fmedia_id,b.fmediaclassid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,a.fissue_date,d.flowid,c.fid taskid,d.fflow_code,ifnull(c.fstatus,0) cstatus')
                    ->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
                    ->join('tregion f on b.media_region_id = f.fid')
                    ->join('ttask c on a.fid = c.fbiz_main_id','left')
                    ->join('(select fflow_code,fflow,ftask_id,fid flowid from ttask_flow where fstatus = 0) d on c.fid = d.ftask_id','left')
                    ->where(['a.fmedia_id'=>$key,'a.fissue_date'=>['in',$value],'a.fstatus'=>1])
                    ->select();
                foreach ($doTask as $bkey => $bvalue) {
                    // if(!empty($bvalue['afstatus'])){
                        if($flowLink == 'gggd'){
                            if($bvalue['cstatus'] == 1 && $bvalue['fflow_code'] == 'mediaissue_ggzs'){//强制归档
                                $add_taskflow_data = [
                                    'ftask_id' => $bvalue['taskid'],
                                    'fflow_code' => 'mediaissue_ggzs',
                                    'fflow' => $flowName['mediaissue_ggzs'],
                                    'fsend_task_flow_id' => $bvalue['flowid']?$bvalue['flowid']:0,
                                    'fsend_reg_id' => 0,
                                    'fsend_reg' => '浙江工商局',
                                    'fsend_id' => session('personData.fid'),
                                    'fsend' => session('personData.fname'),
                                    'fsend_time' => date('Y-m-d H:i:s'),
                                    'fsend_msg' => '强制归档',
                                    'fstatus' => 2,
                                    'ffinish_type' => 1,
                                    'frece_id'=>session('personData.fid'),
                                    'frece'=>session('personData.fname'),
                                    'frece_time'=>date('Y-m-d H:i:s')
                                ];
                                $add_flow = M('ttask_flow')->add($add_taskflow_data);//添加流程操作
                                if(!empty($add_flow)){
                                    //强制中止上一流程
                                    if(!empty($bvalue['flowid'])){
                                        $save_flow = M('ttask_flow')->where(['fid'=>$bvalue['flowid']])->save(['fstatus'=>2,'ffinish_type'=>3,'frece_id'=>session('personData.fid'),'frece'=>session('personData.fname'),'frece_time'=>date('Y-m-d H:i:s')]);
                                    }

                                    $do_task = M('ttask')->where(['fid'=>$bvalue['taskid']])->save(['fstatus'=>5,'ffinish_time'=>date('Y-m-d H:i:s')]);//归档
                                    $finishCount += 1;
                                    if(!empty($do_task)){
                                        A('Api/ZJMediaIssue')->createIllAd($bvalue['fmedia_id'],$bvalue['fissue_date']);//生成违法广告数据
                                        D('Common/Function')->adIssueSendState($bvalue['fmedia_id'],$bvalue['fissue_date'],0,20);//更改数据发布状态为监管平台发布
                                    }
                                }
                            }
                        }elseif($bvalue['fflow_code'] != 'mediaissue_'.$flowLink){
                            $mclass = substr($bvalue['fmediaclassid'],0,2);
                            if(!empty($bvalue['flowid'])){
                                $save_flow = M('ttask_flow')->where(['fid'=>$bvalue['flowid']])->save(['fstatus'=>2,'ffinish_type'=>3,'frece_id'=>session('personData.fid'),'frece'=>session('personData.fname'),'frece_time'=>date('Y-m-d H:i:s')]);
                            }

                            $add_taskflow_data = [
                                'ftask_id' => $bvalue['taskid'],
                                'fflow_code' => 'mediaissue_'.$flowLink,
                                'fflow' => $flowName['mediaissue_'.$flowLink],
                                'fsend_task_flow_id' => $bvalue['flowid']?$bvalue['flowid']:0,
                                'fsend_reg_id' => 0,
                                'fsend_reg' => '浙江工商局',
                                'fsend_id' => session('personData.fid'),
                                'fsend' => session('personData.fname'),
                                'fsend_time' => date('Y-m-d H:i:s'),
                                'fsend_msg' => '强制流转',
                                'fstatus' => 0,
                            ];
                            
                            //如果当前环节为广告监测，则生成串播单
                            if(($bvalue['fflow_code'] == 'mediaissue_ggjc' || $bvalue['fflow_code'] == 'mediaissue_ggfl') && ($mclass == '01' || $mclass == '02')){
                                $ret = A('Api/Function')->createPlayIssueData($mclass,$key,[$bvalue['fissue_date'],date('Y-m-d',strtotime($bvalue['fissue_date'])). '23:59:59']);//重新生成监播单
                            }
                            if($bvalue['cstatus'] == 5){
                                $do_task = M('ttask')->where(['fid'=>$bvalue['taskid']])->save(['fstatus'=>1]);//取消已归档的任务
                                if(!empty($do_task)){
                                    M('tbn_illegal_ad_issue')->where(['fmedia_id'=>$bvalue['fmedia_id'],'fissue_date'=>$bvalue['fissue_date']])->delete();
                                    D('Common/Function')->adIssueSendState($bvalue['fmedia_id'],$bvalue['fissue_date'],0,10);//更改数据发布状态为监测平台发布
                                }
                            }
                            $add_flow = M('ttask_flow')->add($add_taskflow_data);//添加流程操作
                            if(!empty($add_flow)){
                                $finishCount += 1;
                            }
                        }
                    // }else{适用报纸
                    //     if(substr($bvalue['fmediaclassid'], 0,2) == '03' && $flowLink == 'ggfl'){
                    //         $task_data = [
                    //             'fstatus' => 1,
                    //             'fupdate_userid' => session('personData.fid'),
                    //             'fupdate_user' => session('personData.fname'),
                    //             'fupdate_time' => date('Y-m-d H:i:s')
                    //         ];

                    //         $mediaissue_task = M('tmediaissue')->where(['fid'=>$bvalue['fid']])->save($task_data);//添加业务任务
                    //         if(!empty($mediaissue_task)){
                    //             //获取业务信息
                    //             $fbiz_def_code = 'zj_mediaissue';//业务编号
                    //             $fbiz_def_flow_code = 'mediaissue_ggfl';//环节编号
                    //             $where_biz['a.fcode'] = $fbiz_def_code;
                    //             $where_biz['b.fflow_code'] = $fbiz_def_flow_code;
                    //             $where_biz['a.fstatus'] = 1;
                    //             $do_biz = M('tbiz_def')
                    //                 ->cache(true,3600)
                    //                 ->field('a.fid,a.fname,b.fid flowid,b.fflow')
                    //                 ->alias('a')
                    //                 ->join('tbiz_def_flow b on a.fid = b.fbiz_def_id')
                    //                 ->where($where_biz)
                    //                 ->find();

                    //             if(!empty($do_biz)){
                    //                 //判断业务任务是否存在
                    //                 $task_view = M('ttask')->where(['fbiz_def_id'=>$do_biz['fid'],'fbiz_main_id'=>$bvalue['fid']])->find();
                    //                 if(empty($task_view)){
                    //                     $add_task_data = [
                    //                         'fbiz_def_id'=>$do_biz['fid'],
                    //                         'ftitle'=>$do_biz['fname'],
                    //                         'fbiz_main_id'=>$bvalue['fid'],
                    //                         'fcreate_time'=>date('Y-m-d'),
                    //                         'fstatus'=>1
                    //                     ];
                    //                     $task_id = M('ttask')->add($add_task_data);
                    //                 }else{
                    //                     $task_id = $task_view['fid'];
                    //                     if($task_view['fstatus'] == 5){
                    //                         M('ttask')->where(['fid'=>$task_id])->save(['fstatus'=>1]);
                    //                     }
                    //                 }
                                        
                    //                 if(!empty($task_id)){
                    //                     //需要新增的流程
                    //                     $add_taskflow_data = [
                    //                         'ftask_id' => $task_id,
                    //                         'fflow_code' => $fbiz_def_flow_code,
                    //                         'fflow' => $do_biz['fflow'],
                    //                         'fsend_task_flow_id' => 0,
                    //                         'fsend_reg' => '系统',
                    //                         'fsend' => 'AI',
                    //                         'fsend_time' => date('Y-m-d H:i:s'),
                    //                         'fstatus' => 0,
                    //                     ];

                    //                     //判断业务任务流程是否存在
                    //                     $taskflow_id = M('ttask_flow')
                    //                         ->where(['ftask_id'=>$task_id,'fstatus'=>0])
                    //                         ->getField('fid');
                    //                     if(!empty($taskflow_id)){
                    //                         //更新上一流程
                    //                         $flow_data['frece_id'] = 0;
                    //                         $flow_data['frece'] = 'AI';
                    //                         $flow_data['frece_time'] = date('Y-m-d H:i:s');
                    //                         $flow_data['fstatus'] = 2;
                    //                         $flow_data['ffinish_type'] = 3;
                    //                         M('ttask_flow')->where(['fid'=>$taskflow_id])->save($flow_data);

                    //                         $add_taskflow_data['fsend_msg'] = '分离重置';
                    //                         $add_taskflow_data['fsend_task_flow_id'] = $taskflow_id;
                    //                     }
                                        
                    //                     $add_flow = M('ttask_flow')->add($add_taskflow_data);//添加流程操作
                    //                     if(!empty($add_flow)){
                    //                         $finishCount += 1;
                    //                     }
                    //                 }
                    //             }
                    //         }
                            
                    //     }
                    // }
                    
                }
            }elseif($action == 5){//关闭
                $doTask = M('tmediaissue')
                    ->alias('a')
                    ->field('a.fid,a.fissue_date')
                    ->join('ttask c on a.fid = c.fbiz_main_id')
                    ->join('ttask_flow e on c.fid = e.ftask_id')
                    ->join('(select max(a.fid) flid from ttask_flow a group by a.ftask_id) d on e.fid = d.flid')
                    ->where(['a.fmedia_id'=>$key,'a.fissue_date'=>['in',$value],'e.fflow_code'=>['in',['mediaissue_ggfl','mediaissue_ggjc']],'a.fstatus'=>['neq',4]])
                    ->group('a.fid')
                    ->select();
                if(!empty($doTask)){
                    $issueTaskIds = array_column($doTask, 'fid');
                    $issueDates = array_column($doTask, 'fissue_date');
                    $yc_issue = M('tmediaissue')->where(['fid'=>['in',$issueTaskIds]])->save(['fstatus'=>4,'fupdate_userid'=>session('personData.fid'),'fupdate_user'=>session('personData.fname'),'fupdate_time'=>date('Y-m-d H:i:s')]);
                    if(!empty($yc_issue) && !empty($issueDates)){
                        D('Common/Function')->adIssueSendState($key,$issueDates,1,0);//更改数据发布状态为收回状态
                    }
                    $finishCount += $yc_issue;
                }
            }elseif($action == 7){//重新接收
                $doTask = M('tmediaissue')
                    ->alias('a')
                    ->field('a.fid,a.fissue_date')
                    ->join('ttask c on a.fid = c.fbiz_main_id')
                    ->join('ttask_flow e on c.fid = e.ftask_id')
                    ->join('(select max(a.fid) flid from ttask_flow a group by a.ftask_id) d on e.fid = d.flid')
                    ->where(['a.fmedia_id'=>$key,'a.fissue_date'=>['in',$value],'e.fflow_code'=>['in',['mediaissue_ggfl','mediaissue_ggjc']],'a.fstatus'=>['in',[1,4]]])
                    ->group('a.fid')
                    ->select();
                if(!empty($doTask)){
                    $issueTaskIds = array_column($doTask, 'fid');
                    $issueDates = array_column($doTask, 'fissue_date');
                    $yc_issue = M('tmediaissue')->where(['fid'=>['in',$issueTaskIds]])->save(['fstatus'=>0,'fpriority'=>0,'fupdate_userid'=>session('personData.fid'),'fupdate_user'=>session('personData.fname'),'fupdate_time'=>date('Y-m-d H:i:s')]);
                    if(!empty($yc_issue) && !empty($issueDates)){
                        D('Common/Function')->adIssueSendState($key,$issueDates,1,0);//更改数据发布状态为收回状态
                    }
                    $finishCount += $yc_issue;
                }
            }elseif($action == 8){//开启信用统计
                $yc_issue = M('tmediaissue')->where(['fmedia_id'=>$key,'fissue_date'=>['in',$value],'fstatus'=>['in',[0,1,10]]])->save(['fis_credit'=>1,'fupdate_userid'=>session('personData.fid'),'fupdate_user'=>session('personData.fname'),'fupdate_time'=>date('Y-m-d H:i:s')]);
                $finishCount += $yc_issue;
            }elseif($action == 9){//关闭信用统计
                $yc_issue = M('tmediaissue')->where(['fmedia_id'=>$key,'fissue_date'=>['in',$value],'fstatus'=>['in',[0,1,10]]])->save(['fis_credit'=>0,'fupdate_userid'=>session('personData.fid'),'fupdate_user'=>session('personData.fname'),'fupdate_time'=>date('Y-m-d H:i:s')]);
                $finishCount += $yc_issue;
            }
        }
        
        if($action == 1){
        	if(!empty($addData)){
                M('tmediaissue')->addAll($addData);
                $finishCount = count($addData);
	        	//推送任务
				// $backurl = 'http://'.C('ZJServerUrl').'/Api/ZJMediaIssue/create_mediaissue_task2';
				// $pushData['taskData'] = $addData;
				// http($backurl,json_encode($pushData),'POST',false,1);
	        }
	        $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功添加'.$finishCount.'条'));
        }elseif($action == 2){
        	$this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功删除'.$finishCount.'条（注意：已接收任务无法删除）'));
        }elseif($action == 3){
            if(!empty($actionIds)){
                $save_tmediaissue = M('tmediaissue')->where(['fid'=>['in',$actionIds]])->save(['fstatus'=>10,'fupdate_userid'=>session('personData.fid'),'fupdate_user'=>session('personData.fname'),'fupdate_time'=>date('Y-m-d H:i:s'),'flog'=>['exp','CONCAT("'.session('personData.fname').'于'.date('Y-m-d H:i:s').'将数据退回，原因：'.$backContent.'；",flog)' ]]);//修改任务状态

            }
            if(!empty($save_tmediaissue) && !empty($updateFlowIds)){
                $save_flow = M('ttask_flow')->where(['fid'=>['in',$updateFlowIds]])->save(['fstatus'=>2,'ffinish_type'=>2,'frece_id'=>session('personData.fid'),'frece'=>session('personData.fname'),'frece_time'=>date('Y-m-d H:i:s')]);//修改任务流程状态
            }
            
            //退回推送
            $backurl = 'http://'.C('ZJServerUrl').'/Api/ZJMediaIssue/getBackData';
            $pushData['taskData'] = $actionData;
            $pushData['backContent'] = $backContent;
            http($backurl,json_encode($pushData),'POST',false,0);
            if(!in_array($_SERVER['HTTP_HOST'],C('TESTSERVER')) && !empty($actionIds)){
                $smsphone = [13967174397,13717722033];//提醒人员，周佳牛、李伟
                $token = 'f01671bd9464292ce29c4936336baa36e12b5a742f1cca55b6625d0c5fea8d27';
                foreach ($actionData as $key => $value) {
                    push_ddtask('浙江监测平台',"> 数据退回提醒：\n\n媒体".$key."（".$value['fmedia_id']."）的".implode('、', $value['fissue_date'])."数据因".$backContent."被退回，请注意处理。\n\n",$smsphone,$token,'markdown');
                    D('Common/Function')->adIssueSendState($value['fmedia_id'],$value['fissue_date'],1,0);//更改数据发布状态为收回状态
                }
            }

            $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功退回'.$finishCount.'条（注意：非已接收任务无法退回）'));
        }elseif($action == 4){
            if($flowLink == 'gggd'){
                $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功流转'.$finishCount.'条（注意：仅能归档已处于终审环节的任务）'));
            }else{
                $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功流转'.$finishCount.'条'));
            }
        }elseif($action == 5){
            $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功关闭'.$finishCount.'条（注意：仅能设置处在分离或监测环节的数据）'));
        }elseif($action == 7){
            $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中重新接收'.$finishCount.'条（注意：仅能重新接收处于关闭或分离或监测环节的任务）'));
        }elseif($action == 8){
            $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功开启信用统计'.$finishCount.'条（注意：无法对已开启信用统计或已关闭的任务进行操作）'));
        }elseif($action == 9){
            $this->ajaxReturn(array('code'=>0,'msg'=>$allCount.'条中成功关闭信用统计'.$finishCount.'条（注意：无法对已关闭信用统计或已关闭的任务进行操作））'));
        }else{
        	$this->ajaxReturn(array('code'=>1,'msg'=>'非法操作'));
        }
    }

	/*
	* by zw
	* 获取任务搜索条件
	*/
	// private function pbWhereMeidaIssue($getData){
	// 	$mediaType = $getData['mediaType'];//媒体所属（1串播单，2非串播单，3省级，4市级，5区县级,-1默认）
 //        $mediaClass = $getData['mediaClass'];//媒体类型（'01'，'02'，'03',''默认）
 //        $mediaName = $getData['mediaName'];//媒体名
 //        $taskTime = $getData['taskTime'];//任务日期范围，数组（开始和结束日期）
 //        $area = $getData['area'];//行政区划
	// 	$iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区，0否，1是
	// 	$status = $getData['status'] != ''?$getData['status']:-1;//状态，0待接收，1已接收，10已退回，3已关闭，-1默认
	// 	$nowFlow = $getData['nowFlow']?$getData['nowFlow']:-1;//当前环节，1数据等待，2广告分离，3广告监测，4广告复审，5广告终审，6广告归档，-1默认全部

 //        $where_mie['_string'] = '1=1';
	// 	//地域
 //        if(!empty($area)){
 //        	if(!empty($iscontain)){
	// 			$tregion_len = get_tregionlevel($area);
	// 			if($tregion_len == 2){//省级
	// 				$where_mie['b.media_region_id'] = ['like',substr($area,0,2).'%'];
	// 			}elseif($tregion_len == 4){//市级
	// 				$where_mie['b.media_region_id'] = ['like',substr($area,0,4).'%'];
	// 			}elseif($tregion_len == 6){//县级
	// 				$where_mie['b.media_region_id'] = ['like',substr($area,0,6).'%'];
	// 			}
	// 		}else{
	// 			$where_mie['b.media_region_id'] = $area;
	// 		}
 //        }
 //        //媒体类型
 //        if(!empty($mediaClass)){
 //        	$where_mie['left(b.fmediaclassid,2)'] = $mediaClass;
 //        }
 //        //媒体名称
 //        if(!empty($mediaName)){
 //        	$where_mie['b.fmedianame'] = ['like','%'.$mediaName.'%'];
 //        }
 //        //任务日期范围
 //        if(!empty($taskTime)){
 //        	$where_mie['a.fissue_date'] = ['between',[$taskTime[0],$taskTime[1]]];
 //        }
 //        //媒体所属
 //        switch ($mediaType) {
 //        	case 1:
 //        		$where_mie['_string'] .= ' and b.fid in (select fmediaid from tmedialabel where flabelid in(446,449))';
 //        		break;
 //        	case 2:
 //        		$where_mie['_string'] .= ' and b.fid not in (select fmediaid from tmedialabel where flabelid in(446,449))';
 //        		break;
 //        	case 3:
 //        		$where_mie['f.flevel'] = 1;
 //        	case 4:
 //        		$where_mie['f.flevel'] = ['in',[2,3,4]];
 //        	case 5:
 //        		$where_mie['f.flevel'] = 5;
 //        }
 //        //状态
 //        switch ($status) {
 //        	case 0:
 //        		$where_mie['a.fstatus'] = 0;
 //        		break;
 //        	case 1:
 //        		$where_mie['a.fstatus'] = 1;
 //        		break;
 //        	case 10:
 //        		$where_mie['a.fstatus'] = 10;
 //        		break;
 //        	case 3:
 //        		$where_mie['c.fstatus'] = 9;
 //        		$where_mie['a.fstatus'] = 1;
 //        		break;
 //        	case 6:
 //        		$where_mie['c.fstatus'] = 5;
 //        		$where_mie['a.fstatus'] = 1;
 //        		break;
 //        }
 //        //当前环节
 //        switch ($nowFlow) {
 //        	case 1:
 //        		$where_mie['_string'] .= ' and c.fstatus is null';
 //        		break;
 //        	case 2:
 //        		$where_mie['d.fflow_code'] = 'mediaissue_ggfl';
 //        		break;
 //        	case 3:
 //        		$where_mie['d.fflow_code'] = 'mediaissue_ggjc';
 //        		break;
 //        	case 4:
 //        		$where_mie['d.fflow_code'] = 'mediaissue_ggfs';
 //        		break;
 //        	case 5:
 //        		$where_mie['d.fflow_code'] = 'mediaissue_ggzs';
 //        		break;
 //        	case 6:
 //        		$where_mie['c.fstatus'] = 5;
 //        		break;
 //        }

 //        $where_mie['c.fbiz_def_id'] = 1;//业务ID

 //        return $where_mie;
	// }

	/*
	* by zw
	* 获取广告监测任务清单
	*/
	// public function mediaIssueTask(){
	// 	$getData = $this->oParam;
	// 	$pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
 //        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数
 //        $limitIndex = ($pageIndex-1)*$pageSize;

 //        $where_mie = $this->pbWhereMeidaIssue($getData);

 //        $count = M('tmediaissue')
 //        	->alias('a')
 //        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
 //        	->join('tregion f on b.media_region_id = f.fid')
 //        	->join('ttask c on a.fid = c.fbiz_main_id','left')
 //        	->join('(select fflow_code,fflow,ftask_id from ttask_flow where fstatus = 0) d on c.fid = d.ftask_id','left')
 //        	->where($where_mie)
 //        	->count();

 //        $data = M('tmediaissue')
 //        	->alias('a')
 //        	->field('a.fid,a.fmedia_id,a.fissue_date,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,left(b.fmediaclassid,2) fmediaclassid,d.fflow,f.ffullname regionname,a.fcreate_time,a.fstatus astatus,c.fstatus cstatus')
 //        	->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
 //        	->join('tregion f on b.media_region_id = f.fid')
 //        	->join('ttask c on a.fid = c.fbiz_main_id','left')
 //        	->join('(select fflow_code,fflow,ftask_id from ttask_flow where fstatus = 0) d on c.fid = d.ftask_id','left')
 //        	->where($where_mie)
 //        	->order('a.fissue_date desc,a.fid desc')
 //        	->limit($limitIndex,$pageSize)
 //        	->select();
 //        foreach ($data as $key => $value) {
 //        	if($value['astatus'] == 1 && $value['cstatus'] == 9){
 //        		$data[$key]['statusname'] = '关闭';
 //        	}elseif($value['astatus'] == 0){
 //        		$data[$key]['statusname'] = '待接收';
 //        	}elseif($value['astatus'] == 1){
 //        		$data[$key]['statusname'] = '已接收';
 //        	}elseif($value['astatus'] == 10){
 //        		$data[$key]['statusname'] = '已退回';
 //        	}else{
 //        		$data[$key]['statusname'] = '数据处理中';
 //        	}
 //        	if($value['cstatus'] == 5){
 //        		$data[$key]['fflow'] = '数据归档';
 //        	}elseif(empty($value['cstatus'])){
 //        		$data[$key]['fflow'] = '数据等待';
 //        	}
 //        }
 // 		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>$count,'data'=>$data));
	// }

    /*
    * by zw
    * 随机生成一键创建任务的日期
    */
    public function randOneKeyDay(){
        session_write_close();
        $FORCE_NUM_CONFIG_JIANCE = C('FORCE_NUM_CONFIG_JIANCE');

        $getData = $this->oParam;
        $mediaType = $getData['mediaType']!=''?$getData['mediaType']:-1;//媒体所属（1串播单，2非串播单，3省级，4市级，5区县级,-1默认）
        $mediaClass = $getData['mediaClass'];//媒体类型，数组（'01'，'02'，'03',''默认）
        $mediaName = $getData['mediaName'];//媒体名
        $month = $getData['month'];//任务月份
        $dates = $getData['dates'];//任务时段
        $area = $getData['area'];//行政区划
        $iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区，1包含，0不包含，不传默认1
        $fisxinyong = $getData['fisxinyong']?$getData['fisxinyong']:0;//是否信用评价，数组，1是，2否，空是全部
        $rndDays = $getData['rndDays'] ;//抽取天数
        $rndGuDing = $getData['rndGuDing'];//统一生成结果，1是，0否

// $month = '2020-02';
// $iscontain = 0;
// $fisxinyong = 1;
// $rndDays = 5;
// $mediaClass = '01';
// $mediaType = 3;

        if((empty($month) && empty($dates)) || empty($rndDays)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }

        if(!empty($dates)){
            $startDate = $dates[0];
            $endDate = $dates[1];  
        }else{
            $startDate = $month.'-01';
            $endDate = date('Y-m-d', strtotime($startDate.' +1 month -1 day'));
        }

        $stday = (int)date('j',strtotime($startDate));
        $maxday = get_day_diff($startDate,$endDate);
        if($maxday<$rndDays){
            $this->ajaxReturn(['code'=>1,'msg'=>'随机天数超过限定天数']);
        }
        
        $where_mie['_string'] = '1=1';
        //媒体所属
        switch ((int)$mediaType) {
            case 0:
                $where_mie['_string'] .= ' and b.fid in (select fmediaid from tmedialabel where flabelid in(446,449))';
                break;
            case 1:
                $where_mie['_string'] .= ' and b.fid not in (select fmediaid from tmedialabel where flabelid in(446,449))';
                break;
            case 2:
                $where_mie['f.flevel'] = 1;
                break;
            case 3:
                $where_mie['f.flevel'] = ['in',[2,3,4]];
                break;
            case 4:
                $where_mie['f.flevel'] = 5;
                break;
        }

        //是否信用评价媒体
        if(!empty($fisxinyong)){
            if($fisxinyong == 1){
                $where_mie['b.fisxinyong'] = 1;
            }elseif($fisxinyong == 2){
                $where_mie['b.fisxinyong'] = 0;
            }
        }

        //地域
        if(!empty($area)){
            if(!empty($iscontain)){
                $tregion_len = get_tregionlevel($area);
                if($tregion_len == 2){//省级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,2).'%'];
                }elseif($tregion_len == 4){//市级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,4).'%'];
                }elseif($tregion_len == 6){//县级
                    $where_mie['b.media_region_id'] = ['like',substr($area,0,6).'%'];
                }
            }else{
                $where_mie['b.media_region_id'] = $area;
            }
        }
        //媒体类型
        if(!empty($mediaClass)){
            $where_mie['left(b.fmediaclassid,2)'] = ['in',$mediaClass];
        }
        //媒体名称
        if(!empty($mediaName)){
            $where_mie['b.fmedianame'] = ['like','%'.$mediaName.'%'];
        }

        $where_mie['b.fstate'] = 1;
        $where_mie['g.fstate'] = 1;
        $where_mie['g.fcustomer'] = $FORCE_NUM_CONFIG_JIANCE;

        $mediaData = M('tmedia')
            ->alias('b')
            ->field('b.fid,(case when instr(b.fmedianame,"（") > 0 then left(b.fmedianame,instr(b.fmedianame,"（") -1) else b.fmedianame end) fmedianame,f.fname regionname')
            ->join('tregulatormedia g on b.fid = g.fmediaid and g.fregulatorcode = '.$FORCE_NUM_CONFIG_JIANCE)
            ->join('tregion f on b.media_region_id = f.fid')
            ->where($where_mie)
            ->order('f.fid asc,left(b.fmediaclassid,2) asc,b.fid asc')
            ->select();
        if(!empty($mediaData)){
            $yesRandMedia = [];//生成任务的媒体ID
            $noRandMedia = [];//不生成任务的媒体ID
            $randTaskData = [];//随机结果

            //获取已有任务的媒体，已有任务的媒体不生成任务
            $mediaIds = array_column($mediaData, 'fid');
            $taskData = M('tmediaissue')
                ->alias('a')
                ->field('a.fmedia_id,count(*) daycount')
                ->where(['a.fmedia_id'=>['in',$mediaIds],'a.fissue_date'=>['between',[$startDate,$endDate]]])
                ->group('a.fmedia_id')
                ->select();
            if(!empty($taskData)){
                $mediaDays = array_column($taskData,'daycount', 'fmedia_id');
            }

            $params['rand_count'] = $rndDays;
            $params['month_count'] = $maxday;
            $rndurl = 'http://47.96.182.117:8002/day_rand';//随机日期接口地址

            $rndarr = [];
            foreach ($mediaData as $key => $value) {
                if(!isset($mediaDays[(string)$value['fid']])){
                    if(empty($rndGuDing)){
                        $rndarr = [];
                    }

                    if(empty($rndarr)){
                        $rndnum = http($rndurl, $params, 'GET', false, 0);//获取随机数
                        $rndnum = str_replace(' ','',str_replace(']', '',str_replace('[', '', $rndnum)));
                        $dayarr = explode(',',$rndnum);
                        foreach ($dayarr as $key2 => $value2) {
                            $daytime = $value2-1+$stday;
                            $daytimes = date('Y-n',strtotime($startDate)).'-'.$daytime;
                            $rndarr[] = $daytimes;
                        }
                    }

                    $value['days'] = $rndarr;
                    $randTaskData[] = $value;
                    $yesRandMedia[] = $value['fid'];
                }else{
                    $noRandMedia[] = $value['fid'];
                }
            }
        }

        if(!empty($randTaskData)){
            $this->ajaxReturn(array('code'=>0,'msg'=>'生成完成','data'=>$randTaskData));
        }else{
            $this->ajaxReturn(array('code'=>1,'msg'=>'暂无任务可生成，请确保媒体当月无任务'));
        }
    }

    /*
    * by zw
    * 一键创建任务
    */
    public function oneKeyTask(){
        session_write_close();

        $getData = $this->oParam;
        $randTaskData = $getData['randTaskData'];//媒体任务

        if(empty($randTaskData)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }

        $addData = [];
        $finishCount = 0;
        foreach ($randTaskData as $key => $value) {
            $mclass = substr(M('tmedia')->cache(true,3600)->where(['fid'=>$value['fid']])->getField('fmediaclassid'),0,2);
            foreach ($value['days'] as $mkey => $mvalue) {
                if($mclass == '01' || $mclass == '02'){//电视广播直接添加任务待接收
                    $addData[] = [
                        'fmedia_id' => $value['fid'],
                        'fissue_date' => $mvalue,
                        'fcreate_userid' => session('personData.fid'),
                        'fcreate_user' => session('personData.fname'),
                        'fcreate_time' => date('Y-m-d H:i:s'),
                        'fstatus' => 0,
                        'fpriority' => time(),
                    ];
                }else{//报纸期刊直接进入分离状态
                    $addNowTask = [
                        'fmedia_id' => $value['fid'],
                        'fissue_date' => $mvalue,
                        'fcreate_userid' => session('personData.fid'),
                        'fcreate_user' => session('personData.fname'),
                        'fcreate_time' => date('Y-m-d H:i:s'),
                        'fstatus' => 1,
                        'fpriority' => time(),
                    ];
                    $mediaissue_task = M('tmediaissue')->add($addNowTask);//添加业务任务
                    if(!empty($mediaissue_task)){
                        //获取业务信息
                        $fbiz_def_code = 'zj_mediaissue';//业务编号
                        $fbiz_def_flow_code = 'mediaissue_ggfl';//环节编号
                        $where_biz['a.fcode'] = $fbiz_def_code;
                        $where_biz['b.fflow_code'] = $fbiz_def_flow_code;
                        $where_biz['a.fstatus'] = 1;
                        $do_biz = M('tbiz_def')
                            ->cache(true,3600)
                            ->field('a.fid,a.fname,b.fid flowid,b.fflow')
                            ->alias('a')
                            ->join('tbiz_def_flow b on a.fid = b.fbiz_def_id')
                            ->where($where_biz)
                            ->find();

                        if(!empty($do_biz)){
                            //判断业务任务是否存在
                            $task_view = M('ttask')->where(['fbiz_def_id'=>$do_biz['fid'],'fbiz_main_id'=>$mediaissue_task])->find();
                            if(empty($task_view)){
                                $add_task_data = [
                                    'fbiz_def_id'=>$do_biz['fid'],
                                    'ftitle'=>$do_biz['fname'],
                                    'fbiz_main_id'=>$mediaissue_task,
                                    'fcreate_time'=>date('Y-m-d'),
                                    'fstatus'=>1
                                ];
                                $task_id = M('ttask')->add($add_task_data);
                            }else{
                                $task_id = $task_view['fid'];
                                if($task_view['fstatus'] == 5){
                                    M('ttask')->where(['fid'=>$task_id])->save(['fstatus'=>1]);
                                }
                            }
                                
                            if(!empty($task_id)){
                                //需要新增的流程
                                $add_taskflow_data = [
                                    'ftask_id' => $task_id,
                                    'fflow_code' => $fbiz_def_flow_code,
                                    'fflow' => $do_biz['fflow'],
                                    'fsend_task_flow_id' => 0,
                                    'fsend_reg' => '系统',
                                    'fsend' => 'AI',
                                    'fsend_time' => date('Y-m-d H:i:s'),
                                    'fstatus' => 0,
                                ];

                                //判断业务任务流程是否存在
                                $taskflow_id = M('ttask_flow')
                                    ->where(['ftask_id'=>$task_id,'fstatus'=>0])
                                    ->getField('fid');
                                if(!empty($taskflow_id)){
                                    //更新上一流程
                                    $flow_data['frece_id'] = 0;
                                    $flow_data['frece'] = 'AI';
                                    $flow_data['frece_time'] = date('Y-m-d H:i:s');
                                    $flow_data['fstatus'] = 2;
                                    $flow_data['ffinish_type'] = 3;
                                    M('ttask_flow')->where(['fid'=>$taskflow_id])->save($flow_data);

                                    $add_taskflow_data['fsend_msg'] = '分离重置';
                                    $add_taskflow_data['fsend_task_flow_id'] = $taskflow_id;
                                }
                                
                                $add_flow = M('ttask_flow')->add($add_taskflow_data);//添加流程操作
                                if(!empty($add_flow)){
                                    $finishCount += 1;
                                }
                            }
                        }
                    }
                }
            }
        }

        if(!empty($addData)){
            M('tmediaissue')->addAll($addData);
            $finishCount = count($addData);
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'成功添加'.$finishCount.'条'));

    }

}
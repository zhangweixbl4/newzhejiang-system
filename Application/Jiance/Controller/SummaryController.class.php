<?php
namespace Jiance\Controller;
use Think\Controller;

/**
 * 汇总
 * by zw
 */

class SummaryController extends BaseController{

	public function index(){
		session_write_close();
		ini_set('memory_limit','2048M');
    	ini_set('max_execution_time', '600');//设置超时时间
    	$getData = $this->oParam;

		$system_num = getconfig('system_num');
	    $years      = $getData['years']?$getData['years']:date('Y');//选择年份
	    $timetypes  = $getData['timetypes'];//选择时间段
	    $timeval    = $getData['timeval'];//选择时间
	    $mclass 	= $getData['mclass']?$getData['mclass']:'';//媒体类型,数组['01','02']
	    $fadclasscode = $getData['fadclasscode'];// 广告内容类别,数组
	    $regionlevel = $getData['regionlevel']?$getData['regionlevel']:0;//查询地域类型，0各地区，1省级，4市级，5区县级
	    $area = $getData['area']?$getData['area']:'';//地区
	    $regionid = $getData['regionid']?$getData['regionid']:'';//地区，数组
	    $mediaid = $getData['mediaid']?$getData['mediaid']:'';//媒体id，数组
		$classify 	= $getData['classify']?$getData['classify']:1;//查询分类（1地域，2广告类别，3媒体，4媒介机构，5广告名）
		$dimension 	= $getData['dimension']?$getData['dimension']:1;//排序方式（1条数，2违法条数，3条数违法率，11条次，12违法条次，13条次违法率，21时长，22违法时长，23时长违法率）
		$iscontain 	= $getData['iscontain'];//是否包含下属地区
		$districtsOrder = $getData['districtsOrder'];//按地区排序
		$fisxinyong = $getData['fisxinyong']?$getData['fisxinyong']:[];//是否信用评价，数组，1是，2否，空是全部
		$outtype 	= $getData['outtype'];//是否导出

		$whereStr 	= '1=1';//总量条件字串
		$whereIllStr = '1=1';//线索条件字串

		$whereStr .= ' and a.fstate = 1 and a.fsendstate in(10,20)';//数据发布状态

		//媒体筛选
		if(is_array($mediaid) && !empty($mediaid)){
			$whereStr .= ' and f.fid in('.implode(',', $mediaid).')';
			$whereIllStr .= ' and f.fid in('.implode(',', $mediaid).')';
		}

		//是否信用评价媒体
		if(count($fisxinyong) == 1){
			if(in_array(1, $fisxinyong)){
				$whereStr .= ' and f.fisxinyong = 1';
				$whereIllStr .= ' and f.fisxinyong = 1';
			}elseif(in_array(2, $fisxinyong)){
				$whereStr .= ' and f.fisxinyong = 0';
				$whereIllStr .= ' and f.fisxinyong = 0';
			}
		}

		//地区筛选
		if(!empty($area)){
			if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$whereStr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
					$whereIllStr .= ' and f.media_region_id like "'.substr($area,0,2).'%"';
				}elseif($tregion_len == 4){//市级
					$whereStr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
					$whereIllStr .= ' and f.media_region_id like "'.substr($area,0,4).'%"';
				}elseif($tregion_len == 6){//县级
					$whereStr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
					$whereIllStr .= ' and f.media_region_id like "'.substr($area,0,6).'%"';
				}
			}else{
				$whereStr .= ' and f.media_region_id = '.$area;
				$whereIllStr .= ' and f.media_region_id = '.$area;
			}
		}
		//多地区筛选
		$arr_area = [];
		if(!empty($regionid)){
			if(!empty($iscontain)){
				foreach ($regionid as $value) {
					$gregion = D('Common/Function')->get_next_region($value,true);
					if(!empty($gregion)){
						$arr_area = array_merge($arr_area,$gregion);
					}
				}
				$arr_area = array_unique($arr_area);
			}else{
				$arr_area = $regionid;
			}
			$whereStr .= ' and f.media_region_id in('.implode(',', $arr_area).')';
			$whereIllStr .= ' and f.media_region_id in('.implode(',', $arr_area).')';
		}
		//广告类别
	    $arr_code = [];
		if(is_array($fadclasscode) && !empty($fadclasscode)){
			foreach ($fadclasscode as $key => $value) {
				$codes = D('Common/Function')->get_next_tadclasscode($value,true);//获取下级广告类别代码
				if(!empty($codes)){
					$arr_code = array_merge($arr_code,$codes);
				}else{
					array_push($arr_code,$value);
				}
			}
			$whereStr .= ' and c.fadclasscode in('.implode(',', $arr_code).')';
			$whereIllStr .= ' and b.fad_class_code  in('.implode(',', $arr_code).')';
		}

		//总量统计参数
		if($classify != 2){
			$fields2 = ',regionid,regionname';//总量字段
		}
		$joins = "
			inner join tmedia f on a.fmediaid = f.fid and f.fstate = 1
			inner join tad c on b.fadid=c.fadid and c.fstate in(1,2,9)
			inner join tadclass e on left(c.fadclasscode,2)=e.fcode
		";

	    //根据地域类型条件过滤
		if(empty($regionlevel)){
			if(session('personData.fregulatorlevel') == 30){
				$joins .= ' inner join tregion r on concat(left(f.media_region_id,2),"0000") = r.fid';
			}elseif(session('personData.fregulatorlevel') == 20){
				$joins .= ' inner join tregion r on concat(left(f.media_region_id,4),"00") = r.fid';
			}else{
				$joins .= ' inner join tregion r on f.media_region_id = r.fid';
			}
		}else{
			if(!empty($iscontain)){
				if($regionlevel == 1){
					$joins .= ' inner join tregion r on concat(left(f.media_region_id,2),"0000") = r.fid';
				}elseif($regionlevel == 4){
					$whereStr .= ' and r.flevel in(2,3,4,5)';
					$joins .= ' inner join tregion r on concat(left(f.media_region_id,4),"00") = r.fid';
				}elseif($regionlevel == 5){
					$whereStr .= ' and r.flevel = 5';
					$joins .= ' inner join tregion r on f.media_region_id = r.fid';
				}
			}else{
				if($regionlevel == 1){
					$whereStr .= ' and r.flevel = 1';
				}elseif($regionlevel == 4){
					$whereStr .= ' and r.flevel in(2,3,4)';
				}elseif($regionlevel == 5){
					$whereStr .= ' and r.flevel = 5';
				}
				$joins .= ' inner join tregion r on f.media_region_id = r.fid';
			}
		}

		//分类（1地域，2广告类别，3媒体，4媒介机构）
		switch ($classify) {
			case 2:
				$fields .= ",e.fcode keyid,e.fadclass keyname";
				$groups = "e.fcode";

				$fengleistr = '广告类别排名';
				break;
			case 3:
				$fields .= ",f.fid keyid,(case when instr(fmedianame,'（') > 0 then left(fmedianame,instr(fmedianame,'（') -1) else fmedianame end) as keyname,r.fid regionid,r.fname1 regionname";
				$groups = "f.fid";

				$fengleistr = '媒体排名';
				break;
			case 4:
				$fields .= ",g.fid keyid,g.fname keyname,r.fid regionid,r.fname1 regionname,(case when left(f.fmediaclassid,2) = '01' then '电视' when left(f.fmediaclassid,2) = '02' then '广播' when '03' then '报纸' when '04' then '期刊' end) mclassname";
				$fields2 .= ',mclassname';
				$joins .= " inner join tmediaowner g on g.fid = f.fmediaownerid";
				$groups = "f.fid";

				$fengleistr = '媒介机构排名';
				break;
			case 5:
				$fields .= ",c.fadid keyid,c.fadname keyname";
				$fields2 = '';
				$groups = "c.fadid";

				$fengleistr = '广告名排名';
				break;
			default:
				$fields .= ",r.fid keyid,r.fid regionid,r.fname1 keyname,r.fname1 regionname";
				$groups = "r.fid";

				$fengleistr = '地域排名';
				break;
		}

		//----------------发布数据查询--------------------
	    $tbArr 		= [];//参与的表
	    $where_time = gettimecondition($years,$timetypes,$timeval,'a.fissuedate',-3);//时间条件
	    if(in_array('01',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select sum(a.flength) zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from ttvissue_$years a
					inner join ttvsample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins 
					where $whereStr and $where_time  
					group by $groups
				)
		    ";
	    }
	    if(in_array('02',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select sum(a.flength) zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from tbcissue_$years a
					inner join tbcsample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins 
					where $whereStr and $where_time 
					group by $groups
				)
		    ";
	    }
	    if(in_array('03',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select 0 zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from tpaperissue_$years a
					inner join tpapersample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins
					where $whereStr and $where_time and left(f.fmediaclassid,2) = '03'
					group by $groups
				)
		    ";
	    }
	    if(in_array('04',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select 0 zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from tpaperissue_$years a
					inner join tpapersample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins
					where $whereStr and $where_time and left(f.fmediaclassid,2) = '04'
					group by $groups
				)
		    ";
	    }

	    $sqlStr = "
			select keyid,keyname,sum(ztiaoshu) ztiaoshu,sum(ztiaoci) ztiaoci,sum(zshichang) zshichang $fields2 
			from (
				".implode(" union all ", $tbArr)."
			) a
			group by keyid
		";
		$issueSummary = M()->cache(true,60)->query($sqlStr);
		//------------------------------------------------

		//----------------违法发布数据查询--------------------
	    $tbArr 		= [];//参与的表
	    $whereStr .= ' and b.fillegaltypecode > 0';
	    if(in_array('01',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select sum(a.flength) zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from ttvissue_$years a
					inner join ttvsample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins 
					where $whereStr and $where_time  
					group by $groups
				)
		    ";
	    }
	    if(in_array('02',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select sum(a.flength) zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from tbcissue_$years a
					inner join tbcsample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins 
					where $whereStr and $where_time 
					group by $groups
				)
		    ";
	    }
	    if(in_array('03',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select 0 zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from tpaperissue_$years a
					inner join tpapersample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins
					where $whereStr and $where_time and left(f.fmediaclassid,2) = '03'
					group by $groups
				)
		    ";
	    }
	    if(in_array('04',$mclass) || empty($mclass)){
	    	$tbArr[] = "
		    	(
					select 0 zshichang,count(distinct(b.fid)) ztiaoshu,sum(fquantity) ztiaoci $fields
					from tpaperissue_$years a
					inner join tpapersample b on a.fsampleid = b.fid and b.fstate in(0,1)
					$joins
					where $whereStr and $where_time and left(f.fmediaclassid,2) = '04'
					group by $groups
				)
		    ";
	    }

	    $sqlIllStr = "
			select keyid,sum(ztiaoshu) wftiaoshu,sum(ztiaoci) wftiaoci,sum(zshichang) wfshichang $fields2 
			from (
				".implode(" union all ", $tbArr)."
			) a
			group by keyid
		";
		$illSummary = M()->cache(true,60)->query($sqlIllStr);
		//------------------------------------------------

		if(!empty($issueSummary)){
			foreach ($issueSummary as $key => $value) {
				$editSummary[$value['keyid']] = $value;
			}
			foreach ($illSummary as $key => $value) {
				if(!empty($editSummary[$value['keyid']])){
					$editSummary[$value['keyid']] = array_merge($editSummary[$value['keyid']],$value);
				}
			}

			foreach ($editSummary as $key => $value) {
				//各项数据
				if(!empty($value['wftiaoshu'])){
					$value['wfltiaoshu'] = number_format($value['wftiaoshu']/$value['ztiaoshu']*100,2).'%';
					$value['wfltiaoci'] = number_format($value['wftiaoci']/$value['ztiaoci']*100,2).'%';
					$value['wflshichang'] = number_format($value['wfshichang']/$value['zshichang']*100,2).'%';
				}else{
					$value['wftiaoshu'] = '0';
					$value['wftiaoci'] = '0';
					$value['wfshichang'] = '0';
					$value['wfltiaoshu'] = '0%';
					$value['wfltiaoci'] = '0%';
					$value['wflshichang'] = '0%';
				}
				$dataSummary[] = $value;

				//合计数据
				$zSummary['ztiaoshu'] += $value['ztiaoshu'];
				$zSummary['ztiaoci'] += $value['ztiaoci'];
				$zSummary['zshichang'] += $value['zshichang'];
				$zSummary['wftiaoshu'] += $value['wftiaoshu'];
				$zSummary['wftiaoci'] += $value['wftiaoci'];
				$zSummary['wfshichang'] += $value['wfshichang'];
			}

			//合计百分比计算
			if(!empty($zSummary['wftiaoshu'])){
				$zSummary['wfltiaoshu'] = number_format($zSummary['wftiaoshu']/$zSummary['ztiaoshu']*100,2).'%';
				$zSummary['wfltiaoci'] = number_format($zSummary['wftiaoci']/$zSummary['ztiaoci']*100,2).'%';
				$zSummary['wflshichang'] = number_format($zSummary['wfshichang']/$zSummary['zshichang']*100,2).'%';
			}else{
				$zSummary['wfltiaoshu'] = '0%';
				$zSummary['wfltiaoci'] = '0%';
				$zSummary['wflshichang'] = '0%';
			}

			//排序
			switch ($dimension) {
				case 2:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'wftiaoshu',SORT_DESC):sortArrByManyField($dataSummary,'wftiaoshu',SORT_DESC);
					$fbqingkuangstr = '违法条数';
					break;
				case 3:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'wfltiaoshu',SORT_DESC):sortArrByManyField($dataSummary,'wfltiaoshu',SORT_DESC);
					$fbqingkuangstr = '条数违率';
					break;
				case 11:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'ztiaoci',SORT_DESC):sortArrByManyField($dataSummary,'ztiaoci',SORT_DESC);
					$fbqingkuangstr = '条次';
					break;
				case 12:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'wftiaoci',SORT_DESC):sortArrByManyField($dataSummary,'wftiaoci',SORT_DESC);
					$fbqingkuangstr = '违法条次';
					break;
				case 13:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'wfltiaoci',SORT_DESC):sortArrByManyField($dataSummary,'wfltiaoci',SORT_DESC);
					$fbqingkuangstr = '条次违法率';
					break;
				case 21:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'zshichang',SORT_DESC):sortArrByManyField($dataSummary,'zshichang',SORT_DESC);
					$fbqingkuangstr = '时长';
					break;
				case 22:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'wfshichang',SORT_DESC):sortArrByManyField($dataSummary,'wfshichang',SORT_DESC);
					$fbqingkuangstr = '违法时长';
					break;
				case 23:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'wflshichang',SORT_DESC):sortArrByManyField($dataSummary,'wflshichang',SORT_DESC);
					$fbqingkuangstr = '时长违法率';
					break;
				default:
					$dataSummary = $districtsOrder?sortArrByManyField($dataSummary,'regionid',SORT_ASC,'ztiaoshu',SORT_DESC):sortArrByManyField($dataSummary,'ztiaoshu',SORT_DESC);
					$fbqingkuangstr = '条数';
					break;
			}
		}

		if(!empty($dataSummary)){
			if(!empty($outtype)){
				$web_name = getconfig('web_name'); 
				$zSummary['keyname'] = '合计';
				$outdata['title'] = "$web_name - $fengleistr $fbqingkuangstr 发布情况";//文档内部标题名称
				$outdata['datalie']['序号'] = 'key';

				switch ($classify) {
					case 2:
						$outdata['datalie']['广告类别'] = 'keyname';
						$outdata['datalie']['所属地区'] = 'regionname';
						$zSummary['regionname'] = '';
						break;
					case 3:
						$outdata['datalie']['媒体名称'] = 'keyname';
						$outdata['datalie']['所属地区'] = 'regionname';
						$zSummary['regionname'] = '';
						break;
					case 4:
						$outdata['datalie']['媒介机构所在地'] = 'regionname';
						$outdata['datalie']['媒介机构名称'] = 'keyname';
						$outdata['datalie']['媒介机构类型'] = 'mclassname';
						$zSummary['regionname'] = '合计';
						$zSummary['keyname'] = '';
						$zSummary['mclassname'] = '';
						break;
					case 5:
						$outdata['datalie']['广告名称'] = 'keyname';
						$zSummary['regionname'] = '合计';
						break;
					default:
						$outdata['datalie']['地域名称'] = 'keyname';
						break;
				}
				$outdata['datalie']['总条数'] = 'ztiaoshu';
				$outdata['datalie']['违法条数'] = 'wftiaoshu';
				$outdata['datalie']['条数违法率'] = 'wfltiaoshu';
				$outdata['datalie']['总条次'] = 'ztiaoci';
				$outdata['datalie']['违法条次'] = 'wftiaoci';
				$outdata['datalie']['条次违法率'] = 'wfltiaoci';
				$outdata['datalie']['总时长'] = 'zshichang';
				$outdata['datalie']['违法时长'] = 'wfshichang';
				$outdata['datalie']['时长违法率'] = 'wflshichang';
		        
		        $dataSummary[] = $zSummary;
		        $outdata['lists'] = $dataSummary;
		        $ret = A('Api/Function')->outdata_xls($outdata);

		        $this->ajaxReturn(array('code'=>0,'msg'=>'生成成功','data'=>$ret['url']));
			}else{
				$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$dataSummary,'data_hj'=>$zSummary));
			}
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'暂无数据'));
		}
	}

}
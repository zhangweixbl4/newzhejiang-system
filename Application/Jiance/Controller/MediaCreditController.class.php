<?php
namespace Jiance\Controller;
use Think\Controller;
import('Vendor.PHPExcel');
class MediaCreditController extends BaseController
{

    //更新相关时段的信用
    public function updateCredit(){
        ini_set('max_execution_time', 600);//设置超时时间
        ini_set('memory_limit','2048M');

        $getData = $this->oParam;
        $year = $getData['year'] ? $getData['year'] : $this->ajaxReturn(array('code'=>1,'msg'=>'年份必传！'));//年
        $month = $getData['month'] ? $getData['month'] : $this->ajaxReturn(array('code'=>1,'msg'=>'月份必传！'));//月
        $media_class = $getData['media_class'] ? $getData['media_class'] : null;//媒介类型

        if(strtotime($year.'-'.$month.'-01') < strtotime('2019-11-01')){
            $this->ajaxReturn(array('code'=>1,'msg'=>'2019年11月之前数据禁止更新'));
        }

        if(!empty($media_class)){
            $map["LEFT (b.fmediaclassid, 2)"] = $media_class;
            $delmap['_string'] = 'fmedia_id in (select fid from tmedia where left(fmediaclassid,2) = "'.$media_class.'" and fstate = 1)';
        }

        $map["b.fisxinyong"] = 1;
        $mediaIds = M("tmedia b")
            ->where($map)
            ->getField('fid',true);

        $delmap['N'] = $year;
        $delmap['Y'] = $month;
        M('tmedia_credit')->where($delmap)->delete();

        foreach ($mediaIds as $value){
            M()->execute(" call SetMediaCredit(".$value.",".$year.",".$month.") ");
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'更新成功！'));

    }

    //统计显示相关时段的信用
    public function mediaCreditList(){
        $getData = $this->oParam;
        $area = $getData['area'];//行政区划
        $iscontain = $getData['iscontain'] != ''?$getData['iscontain']:1;//是否包含下属地区
        $orders = $getData['orders']?$getData['orders']:0;//0默认，10评价期初分升 11降，20本月信用评价扣分升 21降，30信用评价期末分升 31降
        
        $map = [];

        switch ($orders) {
            case 10:
                $orderstr = 'XYPJQCF desc';
                break;
            case 11:
                $orderstr = 'XYPJQCF asc';
                break;
            case 20:
                $orderstr = 'BYXYPJKF desc';
                break;
            case 21:
                $orderstr = 'BYXYPJKF asc';
                break;
            case 30:
                $orderstr = 'XYPJQMF desc';
                break;
            case 31:
                $orderstr = 'XYPJQMF asc';
                break;
            default:
                $orderstr = 'media_region_id asc,XYPJQMF desc,b.fid asc';
                break;
        }

        //地域
        if(!empty($area)){
            if(!empty($iscontain)){
                $tregion_len = get_tregionlevel($area);
                if($tregion_len == 2){//省级
                    $map['b.media_region_id'] = ['like',substr($area,0,2).'%'];
                }elseif($tregion_len == 4){//市级
                    $map['b.media_region_id'] = ['like',substr($area,0,4).'%'];
                }elseif($tregion_len == 6){//县级
                    $map['b.media_region_id'] = ['like',substr($area,0,6).'%'];
                }
            }else{
                $map['b.media_region_id'] = $area;
            }
        }


        $pageIndex = $getData['pageIndex']?$getData['pageIndex'] : 1;//当前页
        $pageSize = $getData['pageSize']?$getData['pageSize'] : 20;//每页显示数

        $limitIndex = ($pageIndex-1)*$pageSize;

        $year = $getData['year'] ? $getData['year'] : $this->ajaxReturn(array('code'=>1,'msg'=>'年份必传！'));//年
        $month = $getData['month'] ? $getData['month'] : $this->ajaxReturn(array('code'=>1,'msg'=>'月份必传！'));//月

        $is_out_excel = $getData['is_out_excel'] ? $getData['is_out_excel'] : 2;//导出excel

        if($is_out_excel == 1) {
            $pageSize = 9999999999;
            ini_set('memory_limit','3072M');
            set_time_limit(0);
        }

        $media_class = $getData['media_class'] ? $getData['media_class'] : null;//媒介类型
        $media_id = $getData['mediaId'] ? $getData['mediaId'] : null;//媒体id
        $group_name = $getData['group_name'] ? $getData['group_name'] : null;//集团名称

        $map["N"] = $year;
        $map["Y"] = $month;
        $map["b.fisxinyong"] = 1;

        if(!empty($media_class)){
            $map["LEFT (b.fmediaclassid, 2)"] = $media_class;
        }

        if(!empty($region_id)){
            $map["c.fid"] = $region_id;
        }

        if(!empty($media_id)){
            $map["b.fid"] = $media_id;
        }

        if(!empty($group_name)){
            $map["b.fmedia_group"] = $group_name;
        }

        $count = M('tmedia_credit a')
            ->join("tmedia b ON a.fmedia_id = b.fid and b.fstate = 1")
            ->join("tregion c ON c.fid = b.media_region_id")
            ->join("tmediaclass d ON d.fid = LEFT (b.fmediaclassid, 2)")
            ->where($map)
            ->count();

        $media_credit_list = M("tmedia_credit a")
            ->field("
                c.fid as REGIONID,
                c.fname1 as REGIONNAME,
                d.fclass as MEDIACLASS,
                (case when instr(fmedianame,'（') > 0 then left(fmedianame,instr(fmedianame,'（') -1) else fmedianame end) as MEDIANAME,
                b.fmedia_group as JTMC,
                a.N,
                a.Y,
                a.TJTS,
                a.ZHTS,
                a.GGSL,
                a.WFGGSL,
                a.YZWFGGSL,
                a.GGSLWFL,
                a.CSWFLKF,
                a.WFGGSLKF,
                a.YZWFGGSLKF,
                a.GGJE,
                a.WFGGJE,
                a.GGJEWFL,
                a.JEWFLKF,
                a.WFGGJEKF,
                a.GGSCMJ,
                a.WFGGSCMJ,
                a.GGSCMJWFL,
                a.SCMJWFLKF,
                a.BYZKF,
                a.SYZKF,
                a.BYXYPJKF,
                a.XYPJQCF,
                a.XYPJQMF
            ")
            ->join("tmedia b ON a.fmedia_id = b.fid and b.fstate = 1")
            ->join("tregion c ON c.fid = b.media_region_id")
            ->join("tmediaclass d ON d.fid = LEFT (b.fmediaclassid, 2)")
            ->where($map)
            ->limit($limitIndex,$pageSize)
            ->order($orderstr.',a.ID desc')
            ->select();


        if($is_out_excel == 1){
            $this->ajaxReturn(array('code'=>0,'msg'=>'导出成功！','data'=> $this->out_excel($media_credit_list,$year,$month)));
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'获取成功！','count'=>$count,'data' => $media_credit_list));

    }

    //导出EXCEL
    public function out_excel($data,$year,$month){

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")             //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")        //设置标记
            ->setCategory( "Test resultfile");                //设置类别

        $sheet = $objPHPExcel->setActiveSheetIndex(0);


        $sheet ->mergeCells("A1:G1");//基本
        $sheet ->mergeCells("H1:N1");//条次
        $sheet ->mergeCells("O1:S1");//金额
        $sheet ->mergeCells("T1:W1");//时长面积
        $sheet ->mergeCells("X1:AB1");//结果

        $sheet ->setCellValue('A1','基本 ('.$year.'年'.$month.'月)');
        $sheet ->setCellValue('H1','条次');
        $sheet ->setCellValue('O1','金额');
        $sheet ->setCellValue('T1','时长面积');
        $sheet ->setCellValue('X1','结果');
        $sheet ->setCellValue('A2','序号');
        $sheet ->setCellValue('B2','地区');
        $sheet ->setCellValue('C2','类别');
        $sheet ->setCellValue('D2','集团名称');
        $sheet ->setCellValue('E2','媒体名称');
        $sheet ->setCellValue('F2','统计天数');
        $sheet ->setCellValue('G2','折合天数');
        $sheet ->setCellValue('H2','条次');
        $sheet ->setCellValue('I2','违法条次');
        $sheet ->setCellValue('J2','严重违法条次');
        $sheet ->setCellValue('K2','条次违法率');
        $sheet ->setCellValue('L2','条次违法率扣分');
        $sheet ->setCellValue('M2','违法条次扣分');
        $sheet ->setCellValue('N2','严重违法条次扣分');
        $sheet ->setCellValue('O2','金额');
        $sheet ->setCellValue('P2','违法金额');
        $sheet ->setCellValue('Q2','金额违法率');
        $sheet ->setCellValue('R2','金额违法率扣分');
        $sheet ->setCellValue('S2','违法金额扣分');
        $sheet ->setCellValue('T2','时长面积');
        $sheet ->setCellValue('U2','违法时长面积');
        $sheet ->setCellValue('V2','时长面积违法率');
        $sheet ->setCellValue('W2','时长面积违法率扣分');
        $sheet ->setCellValue('X2','本月总扣分');
        $sheet ->setCellValue('Y2','上月总扣分');
        $sheet ->setCellValue('Z2','本月评价扣分');
        $sheet ->setCellValue('AA2','评价期初分');
        $sheet ->setCellValue('AB2','评价期末分');

        foreach ($data as $key => $value) {

            $sheet ->setCellValue('A'.($key+3),$value['REGIONID']);//序号
            $sheet ->setCellValue('B'.($key+3),$value['REGIONNAME']);//地区
            $sheet ->setCellValue('C'.($key+3),$value['MEDIACLASS']);//类别
            $sheet ->setCellValue('D'.($key+3),$value['MEDIAOWNERNAME']);//集团名称
            $sheet ->setCellValue('E'.($key+3),$value['MEDIANAME']);//媒体名称
            $sheet ->setCellValue('F'.($key+3),$value['TJTS']);//统计天数
            $sheet ->setCellValue('G'.($key+3),$value['ZHTS']);//折合天数
            $sheet ->setCellValue('H'.($key+3),$value['GGSL']);//条次
            $sheet ->setCellValue('I'.($key+3),$value['WFGGSL']);//违法条次
            $sheet ->setCellValue('J'.($key+3),$value['YZWFGGSL']);//严重违法条次
            $sheet ->setCellValue('K'.($key+3),$value['GGSLWFL']."%");//条次违法率
            $sheet ->setCellValue('L'.($key+3),$value['CSWFLKF']);//条次违法率扣分
            $sheet ->setCellValue('M'.($key+3),$value['WFGGSLKF']);//违法条次扣分
            $sheet ->setCellValue('N'.($key+3),$value['YZWFGGSLKF']);//严重违法条次扣分
            $sheet ->setCellValue('O'.($key+3),$value['GGJE']);//金额
            $sheet ->setCellValue('P'.($key+3),$value['WFGGJE']);//违法金额
            $sheet ->setCellValue('Q'.($key+3),$value['GGJEWFL']."%");//金额违法率
            $sheet ->setCellValue('R'.($key+3),$value['JEWFLKF']);//金额违法率扣分
            $sheet ->setCellValue('S'.($key+3),$value['WFGGJEKF']);//违法金额扣分
            $sheet ->setCellValue('T'.($key+3),$value['GGSCMJ']);//时长面积
            $sheet ->setCellValue('U'.($key+3),$value['WFGGSCMJ']);//违法时长面积
            $sheet ->setCellValue('V'.($key+3),$value['GGSCMJWFL']."%");//时长面积违法率
            $sheet ->setCellValue('W'.($key+3),$value['SCMJWFLKF']);//时长面积违法率扣分
            $sheet ->setCellValue('X'.($key+3),$value['BYZKF']);//本月总扣分
            $sheet ->setCellValue('Y'.($key+3),$value['SYZKF']);//上月总扣分
            $sheet ->setCellValue('Z'.($key+3),$value['BYXYPJKF']);//本月评价扣分
            $sheet ->setCellValue('AA'.($key+3),$value['XYPJQCF']);//评价期初分
            $sheet ->setCellValue('AB'.($key+3),$value['XYPJQMF']);//评价期末分

        }


        $objActSheet =$objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');//给当前活动的表设置名称
        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $date = date('Ymdhis');
        $savefile = './Public/Word/'.$date.'.xlsx';
        $savefile2 = '/Public/Word/'.$date.'.xlsx';
        $objWriter->save($savefile);
        //即时导出下载
        /*          header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition:attachment;filename="'.$date.'.xlsx"');
                    header('Cache-Control:max-age=0');
                    $objWriter =\PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save( 'php://output');*/
        $ret = A('Common/AliyunOss','Model')->file_up('agp_report/'.$date.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='));//上传云
        unlink($savefile);//删除文件

        return $ret['url'];
    }



}

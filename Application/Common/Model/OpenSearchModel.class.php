<?php
namespace Common\Model;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;

import('Vendor.OpenSearch.Autoloader.Autoloader', '', '.php');//载入sdk

class OpenSearchModel{


	/*构建开放搜索client*/
	public function client() {
		$accessKeyId = C('AL_OSSID');
		$secret = C('AL_OSSKEY');
		$endPoint = C('OPEN_SEARCH_URL');
		
		$options = array('debug' => false);
		$client = new OpenSearchClient($accessKeyId, $secret, $endPoint, $options);
		return $client;
	}
    
	/*新增、更新、删除文档*/
	public function push($tableName,$docs_to_upload,$appName=''){

		
		if($appName == ''){
			$appName = 'hzsj_dmp';
		}

		$client = $this->client();
		$documentClient = new DocumentClient($client);//创建文档操作client
		
		
		$json = json_encode($docs_to_upload);
		$ret = $documentClient->push($json, $appName, $tableName);
		return $ret;	
		
		
	}
	
	/*组装搜索字符串*/
	public function setQuery($setQuery){
		
		$queryStr = '';
		foreach($setQuery as $indexName => $query){
			
			if(is_string($query) || is_int($query)){
				$queryStr .= $indexName.':"'.$query.'"	';
			}

		}
		return $queryStr;
		
	}
	
	
	/*组装过滤字句*/
	public function setFilter($setFilter){
		
		$filterArr = array();
		foreach($setFilter as $indexName => $filter){
			$sign = strtolower($filter[0]);
			if($sign == 'egt'){//大于等于
				$filterArr[] = $indexName.'>='.$filter[1].'	';
			}elseif($sign == 'elt'){//小于等于
				$filterArr[] = $indexName.'<='.$filter[1].'	';
			}elseif($sign == 'gt'){//大于
				$filterArr[] = $indexName.'>'.$filter[1].'	';
			}elseif($sign == 'lt'){//小于
				$filterArr[] = $indexName.'<'.$filter[1].'	';
			}elseif($sign == 'in'){//存在于
				$filterArr[] = 'in('.$indexName.',"'.implode('|',$filter[1]).'")';
			}elseif($sign == 'notin'){//不存在于
				$filterArr[] = 'notin('.$indexName.',"'.implode('|',$filter[1]).'")';
			}elseif($sign == 'between'){

				$filterArr[] = $indexName.'>='.$filter[1][0].'	AND '.$indexName.'<='.$filter[1][1];
			}
			
				
				
				
				
			

		}
		return implode(' AND ',$filterArr);
		
		
	}
	
	
	
	
	
	
	
	
	

}
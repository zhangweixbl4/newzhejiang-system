<?php

namespace Common\Model;
use Think\Exception;

use Aliyun\OTS\Consts\ColumnTypeConst;
use Aliyun\OTS\Consts\PrimaryKeyTypeConst;
use Aliyun\OTS\Consts\RowExistenceExpectationConst;
use Aliyun\OTS\OTSClient as OTSClient;
use Aliyun\OTS\Consts\OperationTypeConst;

use Aliyun\OTS\Consts\DirectionConst;



import('Vendor.aliyun-tablestore.autoload', '', '.php');//


class TableStoreModel{
	
	
	
	
	/*初始化OTS*/
	public function otsClient(){
		

		$otsClient = new OTSClient (array (
											'EndPoint' => C('OTS_URL'),
											'AccessKeyID' => C('AL_OSSID'),
											'AccessKeySecret' => C('AL_OSSKEY'),
											'InstanceName' => 'hzsjdmp',
											'DebugLogHandler'	=>	false,
											'ErrorLogHandler'	=>	false,
											
										));
										
		return $otsClient;								
	}
	

	/*写入一行数据*/
	public function putRow($table_name,$primary_key,$attribute_columns){

		$request = array ('table_name' => $table_name,'primary_key' => $primary_key,'attribute_columns' => $attribute_columns,);		
		$otsClient = $this->otsClient();//初始化OTS	
		try {   
			$response = $otsClient->putRow($request);
			return true;
		} catch ( \Exception $error) {   

			return $error->getMessage();

		} 
		
		
	}
	
	/*批量操作数据*/
	public function batchWriteRow($table_name,$rows){

		$request = array ('tables'=>array(array('table_name' => $table_name,'rows' => $rows,)));		
		$otsClient = $this->otsClient();//初始化OTS	
		
		try {   
			$response = $otsClient->batchWriteRow($request);
			$successNum = 0;
			$failNum = 0;
			
			foreach($response['tables'] as $tables){
				foreach($tables['rows'] as $table){
					if($table['is_ok'] === true) $successNum++;
					if($table['is_ok'] === false) $failNum++;
					
				}
			}
			return array('successNum'=>$successNum,'failNum'=>$failNum);
		} catch ( \Exception $error) {   

			return $error->getMessage();

		} 

		
	}
	
	
	public function getRange($table_name,$startPK,$endPK,$limit,$direction='FORWARD'){
		
		
		$dataList = array();
		$otsClient = $this->otsClient();//初始化OTS	
		while (! empty ($startPK) && $limit > 0) {
		  
		  $request = array (
			  'table_name' => $table_name,
			  "max_versions" => 1,
			  'direction' => $direction, // 方向可以为 FORWARD 或者 BACKWARD
			  'inclusive_start_primary_key' => $startPK, // 开始主键
			  'exclusive_end_primary_key' => $endPK, // 结束主键
			  'limit' => $limit
		  );

		  try {   
			 $response = $otsClient->getRange ($request);
		  } catch ( \Exception $error) {   
			return false;	
			//var_dump($error);
			//exit;
		  }
		 
		  	
		  
		  foreach ($response['rows'] as $rowData) {
			$limit --;
			$tem = array();
			foreach($rowData['primary_key'] as $primary_key){
				$tem[$primary_key[0]] = $primary_key[1];
			}
			foreach($rowData['attribute_columns'] as $attribute_columns){
				$tem[$attribute_columns[0]] = $attribute_columns[1];
			}
			
			
			$dataList[] = $tem;

		  }

			$startPK = $response['next_start_primary_key'];

		}
		
		return $dataList;
		
	}
	
	
	
	
	
	



	
	
	

}
<?php
namespace Common\Model;
use OSS\OssClient;
use OSS\Core\OssException;
use OSS\Model\CorsConfig;
use OSS\Model\CorsRule;
use OSS\Model\LifecycleConfig;
use OSS\Model\LifecycleRule;
use OSS\Model\LifecycleAction;
import('Vendor.aliyun-oss-php-sdk-master.autoload','','.php');//载入阿里云OSS sdk

/**
 * 阿里云OSS(对象存储)模型类
 */
class AliyunOssModel{

	/**
	 * OSS对象
	 */
	public $ossClient;

	/**
	 * 初始化
	 */
	public function __construct(){
		$accessKeyId = 'LTAIoQ0BYDRg4VzZ';
		$accessKeySecret = 'z7J7uB3redq4SV7fefP8dz1T6L1zqH';
		$endpoint = 'http://oss-cn-shanghai.aliyuncs.com/';
		// $accessKeyId = C('AL_OSSID');
		// $accessKeySecret = C('AL_OSSKEY');
		// $endpoint = C('ENDPOINT');
		try {
			$this->ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
		} catch (OssException $e) {
			return $e->getMessage();
		}
	}

	/*初始化OSS*/
	public function OssClient(){
		$accessKeyId = C('AL_OSSID');
		$accessKeySecret = C('AL_OSSKEY');
		$endpoint = C('ENDPOINT');
		$ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
		return $ossClient;
    }
	
	/*新增生命周期规则*/
	/* public function rules_add($bucket,$prefix,$save_days){
		// 设置规则ID和文件前缀。
		$ruleId0 = $prefix;
		$matchPrefix0 = $prefix;
		$lifecycleConfig = new LifecycleConfig();
		$actions = array();
		$actions[] = new LifecycleAction(OssClient::OSS_LIFECYCLE_EXPIRATION, OssClient::OSS_LIFECYCLE_TIMING_DAYS,$save_days);
		//$actions[] = new LifecycleAction(OssClient::OSS_LIFECYCLE_EXPIRATION, OssClient::OSS_LIFECYCLE_TIMING_DAYS,$save_days);
		$lifecycleRule = new LifecycleRule($ruleId0, $matchPrefix0, "Enabled", $actions);
		$lifecycleConfig->addRule($lifecycleRule);
		$ossClient = $this->OssClient();
		try {
			$ossClient->putBucketLifecycle($bucket, $lifecycleConfig);
			$state = true;
		} catch (OssException $e) {
			$state = false;
		}
		return $state;
	} */
	
	/*追加上传*/
	public function append_up(){
		$content_array = array('Hello OSS', 'Hi OSS', 'OSS OK');
		$bucket = 'ddmmpp';
		$object = 'media_m3u8/test.media';
		try{
			$ossClient = $this->OssClient();
			$position = $ossClient->appendObject($bucket, $object, $content_array[0],0);
		} catch(OssException $e) {
			var_dump($e);
		}
	}
	
	/*设置跨域资源共享*/
	public function putBucketCors(){
		$ossClient = $this->OssClient();
		$corsConfig = new CorsConfig();
		$rule = new CorsRule();
		$rule->addAllowedHeader("*");
		$rule->addAllowedOrigin("*");
		$rule->addAllowedMethod("GET");
		$rule->setMaxAgeSeconds(10);
		$corsConfig->addRule($rule);
		try{
			$ossClient->putBucketCors('ah-gsj-public', $corsConfig);
		} catch(OssException $e) {
			printf(__FUNCTION__ . ": FAILED\n");
			printf($e->getMessage() . "\n");
			return false;
		}
		return true;
	}
	
	function modifyMetaForObject($key,$headers){
		$bucket = C('AL_OSSBUCKET');
		$ossClient = $this->OssClient();
		$copyOptions = array(OssClient::OSS_HEADERS => $headers);
		try{
			$ossClient->copyObject($bucket, $key, $bucket, $key, $copyOptions);
			return true;
		} catch(OssException $e) {
			//var_dump($e);
			return false;
		}
	}
	
	/*阿里云上传*/
	public function file_up($key,$filePath,$headers = array()){
		$ossClient = $this->OssClient();
		//["requestUrl":"OSS\OssClient":private]
		try{
			$ret = $ossClient->uploadFile(C('AL_OSSBUCKET'),$key,$filePath);
			if($headers) $this->modifyMetaForObject($key,$headers);
			return array('msg'=>'','url'=>$ret['oss-request-url']);
		} catch(OssException $error) {
			return array('msg'=>$error->getMessage(),'url'=>'');
		}
	}
	
	/*阿里云上传测试*/
	public function up_test(){
		$object = "/alidata/website-info.log";
		$ossClient = $this->OssClient();
        $ossClient->uploadFile('wwllccyy', "alidata/website-info.log", $object);
	}

	public function up_pdf($name){
		$ossClient = $this->OssClient();
        $oss = $ossClient->uploadFile('wwllccyy', "LOG/mpdf.pdf", $name);
        return $oss['info']['url'];
	}

	/*新建存储空间*/
	public function createBucket($bucket){
		$ossClient = $this->OssClient();
        $ossClient->createBucket($bucket, OssClient::OSS_ACL_TYPE_PRIVATE);
	}

	/**
	 * 列举文件
	 */
	public function listObjects($bucket = 'cunzheng'){
		$listObjectInfo = $this->ossClient->listObjects($bucket);
		return $listObjectInfo;
	}

	/**
	 * 下载文件到本地磁盘
	 */
	public function getObject($object = '', $bucket = 'cunzheng', $options = []){
		// 指定文件下载路径。
		$localfile = './LOG/'.$object.'.tar.gz';
		$options = [
			OssClient::OSS_FILE_DOWNLOAD => $localfile
		];
		try{
			$object = $this->ossClient->getObject($bucket, $object, $options);
		}catch(OssException $e) {
			return $e->getMessage();
		}
		return $object;
	}

	/**
	 * 下载文件到本地内存，返回文件内容
	 */
	public function getObjectToMem($object = '', $bucket = 'cunzheng'){
		try{
			$content = $this->ossClient->getObject($bucket, $object);
		}catch(OssException $e) {
			return $e->getMessage();
		}
		return $content;
	}

	/**
	 * 获取文件对象
	 */
	public function getObjects($objectId, $bucket = 'cunzheng'){
		$prefix = $objectId;
		$delimiter = '';
		$nextMarker = '';
		$maxkeys = 1;
		$options = [
			'delimiter' => $delimiter,
			'prefix' => $prefix,
			'max-keys' => $maxkeys,
			'marker' => $nextMarker,
		];
		try {
			$listObjectInfo = $this->ossClient->listObjects($bucket, $options);
		} catch (OssException $e) {
			printf(__FUNCTION__ . ": FAILED\n");
			printf($e->getMessage() . "\n");
			return;
		}
		$objectList = $listObjectInfo->getObjectList();
		return $objectList;
	}

	/**
	 * 判断文件是否存在
	 */
	public function isObjectExist($object, $bucket = 'cunzheng'){
		$exist = $this->ossClient->doesObjectExist($bucket, $object);
		return $exist;
	}

	/**
	 * 下载指定文件2
	 */
	public function getObject2($bucket = 'cunzheng', $object = '', $options = []){
		$bucket= 'cunzheng';
		// object 表示您在下载文件时需要指定的文件名称，如abc/efg/123.jpg。
		$object = '436558';
		// 指定文件下载路径。
		$localfile = './LOG/436558_copy.tar.gz';
		$options = [
			OssClient::OSS_FILE_DOWNLOAD => $localfile
		];
		$object = $this->ossClient->getObject($bucket, $object, $options);
		return $object;
	}

	/**
	 * 上传文件
	 */
	public function uploadFile($key, $filePath, $headers = []){
		try{
			$ret = $this->ossClient->uploadFile('cunzheng',$key,$filePath);
			if($headers) $this->modifyMetaForObject2($key,$headers);
			return [
				'msg' => '',
				'url' => $ret['oss-request-url']
			];
		} catch(OssException $error) {
			return [
				'msg' => $error->getMessage(),
				'url' => ''
			];
		}
	}

	protected function modifyMetaForObject2($key,$headers){
		$bucket = 'cunzheng';
		$ossClient = $this->OssClient;
		$copyOptions = array(OssClient::OSS_HEADERS => $headers);
		try{
			$ossClient->copyObject($bucket, $key, $bucket, $key, $copyOptions);
			return true;
		} catch(OssException $e) {
			return false;
		}
	}

	/**
	 * 授权访问URL
	 */
	public function signUrl($bucket, $object, $timeout){
		return $this->ossClient->signUrl($bucket, $object, $timeout);
	}
}
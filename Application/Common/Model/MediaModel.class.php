<?php
namespace Common\Model;
use Think\Exception;

class MediaModel{
	/*获取M3u8
		startTime:开始时间戳
		endTime:结束时间戳
		http://az.hz-data.xyz:8087/channel/11000010007779/playback.m3u8?start_time=1525510936&end_time=1525511936
	*/
	public function get_m3u8($mediaId,$startTime,$endTime,$is_source = 0){

		if($mediaId == '11000010003327' && date('Y-m-d',$startTime) == '2018-10-07'){
			$is_source = 1;
			$mediaId = '11000010006805';
		} 
		if($is_source == 0){//如果不是从源获取
			return 'http://az.hz-data.xyz:8087/channel/'.$mediaId.'/playback.m3u8?start_time='.$startTime.'&end_time='.$endTime;
		}
		$startTime = intval($startTime);
		$endTime = intval($endTime);
		
		//获取媒体数据
		$gourl = 'http://'.C('MainServerUrl').'/Api/Media/m3u8_live_mediaview';
		$pubData['fid'] = $mediaId;
		$data = json_decode(http($gourl,$pubData,'POST',false,5),true);
		
		if(!empty($data['code'])){
			$mediaInfo = M('tmedia')->cache(true,60)->field('fid,fcollecttype,fsavepath')->where(array('fid'=>$mediaId))->find();//查询媒介详情
		}else{
			$mediaInfo = $data['data'];
		}
		
		if($mediaInfo['fcollecttype'] == 10){
			$m3u8_url = U('Api/Media/get_media_m3u8@dmp.hz-data.com',array('media_id'=>$mediaInfo['fid'],'start_time'=>$startTime,'end_time'=>$endTime,'s'=>'m.m3u8'));
		}elseif($mediaInfo['fcollecttype'] == 25){
			$m3u8_url = 'http://47.96.182.117/api/getVod?id='. $mediaId . '&start='. $startTime .'&end='. $endTime.'&type=2&m=m.m3u8';//老互联网
		}elseif($mediaInfo['fcollecttype'] == 26){
			$m3u8_url = 'http://47.96.182.117/api/getVod?id='. $mediaId . '&start='. $startTime .'&end='. $endTime.'&type=4&m=m.m3u8';//新互联网
		}elseif($mediaInfo['fcollecttype'] == 27){
			$m3u8_url = 'http://47.96.182.117/api/getVod?id='. $mediaId . '&start='. $startTime .'&end='. $endTime.'&type=3&m=m.m3u8';//蜻蜓
		}else{
			$m3u8_url = '';
		}

		
		return $m3u8_url;
		
	}
	
	
	/*获取异常类型code*/
	public function get_abnormal_code($abnormal_type){
		
		$media_abnormal_type_info = M('media_abnormal_type')->where(array('abnormal_type'=>$abnormal_type))->find();//查询异常类型id
		
		
		if($media_abnormal_type_info){
			return $media_abnormal_type_info['abnormal_code'];
		}else{
			$abnormal_code = M('media_abnormal_type')->add(array('abnormal_type'=>$abnormal_type));
			return $abnormal_code;
		}
		
		
		
		
	}
	
	/*通过媒介id获取主要媒介id*/
	public function getMainMediaId($mediaId){
		

		$mediaInfo = M('tmedia')->field('fid,main_media_id') ->where(array('fid'=>$mediaId))->find();
		
		$MainMediaId = $mediaInfo['main_media_id'];
		if(!$MainMediaId){
			$MainMediaId = $mediaId;
		}
		return $MainMediaId;
	}

    /**
     * 根据媒介名称获取媒介ID,无则新建并返回ID
     */
    public function getMediaIdByName($mediaName = ''){
        if($mediaName){
            $mediaInfo = M('tmedia')
                ->field('fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,fmediaclassid')
                ->where(['fmedianame'=>$mediaName])
                ->find();
            if($mediaInfo){
                $mediaId = $mediaInfo['fid'];
            }else{
                $mediaData = [
                    'fmedianame' => $mediaName,
                    'fmediaclassid' => 13,//互联网媒介,TODO:有待写为通用方法
                    'fcreator' => '众包任务',
                    'fcreatetime' => date('Y-m-d H:i:s',time())
                ];
                $mediaId = M('tmedia')->add($mediaData);
            }
            return $mediaId;
        }
        return false;
	}
	
	/**
	 * 获取媒体列表
	 * @Param String $type 媒介类型 01-电视 02-广播 03-报纸 13-互联网 空则获取全部类型
	 * @Param String $medianame 媒介名称 进行模糊联想
	 * @Param String $limit 限制输出记录数 例：'10'取10条，'10,20'从第10条开始取20条记录，空为不限制
	 */
	public function getMediaList($type = '',$medianame = '',$fregionid = '',$limit = ''){
		$where['priority'] =  ['EGT',0];
		if($type != ''){
			$where['LEFT(fmediaclassid,2)'] = ['IN',$type];
		}
		if($medianame != ''){
			$where['fmedianame'] = ['LIKE','%'.trim($medianame).'%'];
		}
		if($fregionid != ''){
			$where['media_region_id'] = ['IN',trim($fregionid)];
		}
		$mediaList = M('tmedia')
			->cache(true,120)
			->field('fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,fmediaclassid')
			->where($where)
			->limit($limit)
			->select();
		if(is_array($mediaList) || !empty($mediaList)){
			return $mediaList;
		}else{
			return false;
		}
	}
	
	/*计算广告价格*/
	public function ad_price($mediaId=0,$startTime=0,$endTime=0){
		
		
		$adlen = $endTime - $startTime;//广告时长
		$adlen_mod = $adlen % 5 ;//时长圆整，允许时长在5秒整数倍前后允许各有1秒的偏差，对于超过1秒偏差的按实际时长计
		
		if($adlen_mod == 1){//大了一秒
			$adlen -= 1;
		}
		
		if($adlen_mod == 4){//小了一秒
			$adlen += 1;
		}
		if($adlen <= 0) return 0;//如果广告时长小于等于0秒
		
		$mediaInfo = M('tmedia')->cache(true,60)->field('fid,left(fmediaclassid,2) as media_class')->where(array('fid'=>$mediaId))->find();
		$week = date('w',$startTime);//星期几
		

		
		if($mediaInfo['media_class'] == '01'){//电视
		
			
		
			
		
			$priceInfo = M('ttvprice')
										->cache(true,60)
										->where(array(
													'fmediaid'=>$mediaId,
													'_string'=>'"'.date('H:i:s',$startTime).'" between fstarttime and fendtime and FIND_IN_SET('.$week.',fweekday) > 0',
													'fdate'=>array('elt',date('Y-m-d',$startTime)),
													
												))
										->order('fdate desc')		
										->find();
			if(	!$priceInfo) return 0;						
			$priceList = array(0=>'0');							
			foreach($priceInfo as $field => $value){//提取价格字段
				if(strstr($field,'fp') && $value > 0) $priceList[str_replace('fp','',$field)] = $value;
			}						
			//var_dump($priceList);
			if(	intval($priceList[$adlen]) > 0){//能直接匹配刊例价
				return intval($priceList[$adlen]);//直接返回价格
			}else{
				
				$min = null;//比发布时间小一个档次的价格
				$max = null;//比发布时间大一个档次的价格
				foreach($priceList as $len => $vv){//循环所有的价格
					if($len < $adlen) $min = array('len'=>$len,'price'=>$vv);//找到比发布时间小一个档次的价格
					if($len > $adlen && $max == null) $max = array('len'=>$len,'price'=>$vv);//找到比发布时间大一个档次的价格
				}
				
				if($max == null){
					return $min['price'] / $min['len'] * $adlen;//小一个档次的 价格 除以 时长 再乘以 时长
				}else{
					return $min['price'] + ($max['price'] - $min['price']) * ($adlen - $min['len'] ) / ($max['len'] - $min['len']);
				}
				
				
			}							
														
		
			
		}elseif($mediaInfo['media_class'] == '02'){//广播
			
			
			
		}elseif($mediaInfo['media_class'] == '03'){//报纸
			
			
			
		}
		

		
		
		return 0;
	}
	
	
	/*查询媒体的某个时间点是否停台检修或毕台时间*/
	public function na_time($mediaId,$time){
		$main_media_id = M('tmedia')->where(array('fid'=>$mediaId))->getField('main_media_id');
		if(!$main_media_id) $main_media_id = $mediaId;
		
		$date = date('Y-m-d',$time);//转化为日期
		$na_url = 'http://172.16.198.126/manage/channel/getChannelServiceTime?channel='.$main_media_id.'&date='.$date;
		$ret = http($na_url);//去远程查询
		if(!$ret){
			file_put_contents('LOG/media_na_time',date('Y-m-d H:i:s').'	接口超时'.$na_url."\n",FILE_APPEND);
		}
		$ret = json_decode($ret,true);//查询结果转化为数组

		foreach($ret['dataList'] as $rr){//循环查到的结果，判断是否包含查询时间
			if((($rr['start'] - 7200) < $time ) && (($rr['end'] + 7200) > $time ) && $rr['type'] > 0){
				return intval($rr['type']);
			}
		}
		return 0;
	}
	
	
	
	/*查询数据可用时间*/
	public function available_time($mediaId){
		$mediaInfo = M('tmedia')
			->cache(true,600)
			->field('fid,left(fmediaclassid,2) as media_class,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
			->where(array('fid'=>$mediaId))
			->find();//查询媒介详情
		$labelList = M('tmedialabel')
			->cache(true,600)
			->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
			->where(array('fmediaid'=>$mediaId))
			->getField('tlabel.flabel',true);//查询标签列表
		if(in_array('快剪',$labelList)){//判断是否快剪频道
			$fastCut = M('cut_task')
				->where(array(
					'fmediaid'=>$mediaId,
					//'fissuddate'=>array('gt',date('Y-m-d',time()-86400*30)),
					'fstate'=>array('in','0,1,2,3')
				))
				->order('fissuedate')
				->getField('fissuedate');//查询未完成的快剪任务
			if(!$fastCut) $fastCut = date('Y-m-d',time()-86400*3);						
			$available_time = strtotime($fastCut);					
		}else{
			if($mediaInfo['media_class'] == '01') $samTable = 'ttvsample';
			if($mediaInfo['media_class'] == '02') $samTable = 'tbcsample';
			if($mediaInfo['media_class'] == '03') $samTable = 'tpapersample';
			if(empty($samTable)){
				// TODO:网络媒体数据可用时间待完善
				return 0;
			}
			$fissuedate = M($samTable)
				->where(array(
					'fmediaid'=>$mediaId,
					'fissuedate'=>array('gt',date('Y-m-d',time()-86400*30)),
					'fstate'=>1,
					'fadid'=>0
					))
				->order('fissuedate')
				->getField('fissuedate');//查询最早的未编辑完的样本
			$sql = M($samTable)->getLastSql();
			if(!$fissuedate){//如果没有未编辑完的样本
				$editTime = time(); //把最后编辑时间设为当前	
			}else{
				$editTime = strtotime($fissuedate);//把最后编辑时间转为时间戳
			}
			$gbRet = http('http://47.96.182.117:8002/cloud_produce_status',array('ch_list'=>'["'.$mediaId.'"]'),'POST');//获取频道的最后剪辑时间
			if($gbRet){
				$cutTime = json_decode($gbRet,true)['data'][$mediaId];//获取频道的最后剪辑时间
				$formatCutTime = date('Y-m-d H:i:s',$cutTime);
				if(!$cutTime) $cutTime = time() - 86400*3;//如果没有获取到数据，则使用3天前
			}else{
				$cutTime = time() - 86400*3;
			}
			if($editTime < $cutTime){//比较编辑时间和剪辑时间的大小，使用比较小的时间
				$available_time = $editTime;//使用编辑时间
			}else{
				$available_time = $cutTime;//使用剪辑时间
			}
		}
		$available_time -= 86400;
		if((time() - $available_time) > (86400*7)){
			//$ret = push_ddtask('_','【'.$mediaInfo['fmedianame'].'】'."\n".'的数据可用日期停留在'. (date('Y年m月d日',$available_time)) .'	'."\n".'媒介ID:'.$mediaInfo['fid'].''."\n".'请及时查看并处理','13588258695','6299ea2a7184853c83972cf1df3ae1a838d23d18e1ea207cd168c6b3b92fd8cc','markdown');
		}
		return $available_time;
	}
	/**
	 * @Des: 查询媒体剪辑和录入处理时间
	 * @Edt: yuhou.wang
	 * @param {type} 
	 * @return: 
	 */
	public function getMediaProcessTime($mediaId){
		$mediaInfo = M('tmedia')
			->cache(true,600)
			->field('fid,LEFT(fmediaclassid,2) as media_class,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
			->where(array('fid'=>$mediaId))
			->find();
		$labelList = M('tmedialabel')
			->cache(true,600)
			->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
			->where(['fmediaid'=>$mediaId])
			->getField('tlabel.flabel',true);
		$labelStr = implode(',',$labelList);
		// 返回数据
		$data = [
			'fmediaid'   => $mediaId,
			'fmedianame' => $mediaInfo['fmedianame'],
			'flabel'     => $labelStr,
			'cuttime'    => 0,
			'edittime'   => 0,
			'avatime'	 => 0,
		];
		if(in_array('快剪',$labelList)){//判断是否快剪频道
			$fastCut = M('cut_task')
				->where(array(
					'fmediaid'=>$mediaId,
					'fstate'=>array('in','0,1,2,3')
				))
				->order('fissuedate')
				->getField('fissuedate');//查询未完成的快剪任务
			if(!$fastCut) $fastCut = date('Y-m-d',time()-86400*3);						
			$available_time = strtotime($fastCut);
			$data['edittime'] = date('Y-m-d H:i:s',strtotime($fastCut));
		}else{
			if($mediaInfo['media_class'] == '01') $samTable = 'ttvsample';
			if($mediaInfo['media_class'] == '02') $samTable = 'tbcsample';
			if($mediaInfo['media_class'] == '03') $samTable = 'tpapersample';
			if(empty($samTable)){
				// TODO:网络媒体数据可用时间待完善
				return 0;
			}
			$editTime = M($samTable)
				->where([
					'fmediaid'   => $mediaId,
					'fissuedate' => ['gt',date('Y-m-d',time()-86400*30)],
					'fstate'     => 1,
					'fadid'      => 0
					])
				->order('fissuedate')
				->getField('fissuedate');//查询最早的未编辑完的样本
			if(!$editTime){
				//如果没有未编辑完的样本
				$editTime = time(); //把最后编辑时间设为当前	
			}else{
				$editTime = strtotime($editTime);//把最后编辑时间转为时间戳
			}
			$gbRet = http('http://47.96.182.117:8002/cloud_produce_status',array('ch_list'=>'["'.$mediaId.'"]'),'POST');//获取频道的最后剪辑时间					
			$cutTime = json_decode($gbRet,true)['data'][$mediaId];//获取频道的最后剪辑时间
			$formatCutTime = date('Y-m-d H:i:s',$cutTime);
			if(!$cutTime) $cutTime = time() - 86400*3;//如果没有获取到数据，则使用3天前

			$data['edittime'] = date('Y-m-d H:i:s',$editTime);
			$data['cuttime'] = date('Y-m-d H:i:s',$cutTime);

			if($editTime < $cutTime){//比较编辑时间和剪辑时间的大小，使用比较小的时间
				$available_time = $editTime;//使用编辑时间
			}else{
				$available_time = $cutTime;//使用剪辑时间
			}

			$data['avatime'] = date('Y-m-d H:i:s',$available_time);
		}
		return $data;
	}
}
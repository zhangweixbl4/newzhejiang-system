<?php
namespace Common\Model;
use Think\Exception;

class ShilianModel{
	
	/*通过设备序列号和通道号获取平台号*/
	public function get_dpid($device_serial = '',$chan_no = 0 ){
		

		$shilian_server = C('SHILIAN_SERVER');

		
		
		$shilian_authorization = C('SHILIAN_AUTHORIZATION');

		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.intval($chan_no).'}';//post数据
		
		
		$url = 'http://'.$shilian_server.':'.C('SHILIAN_API_PORT').'/openapi/device/get_dpid';//接口地址
		$header = array('Content-Type: application/json','Authorization: '.$shilian_authorization);
		$rr = http($url,$post_data,'POST',$header);//请求
		
		//echo '请求内容：'.$post_data."\n";
		//echo '请求地址：'.$url."\n";
		//echo '返回内容：'.$rr."\n";
		

		
		
		$rr = json_decode($rr,true);//json数据转为数组
		
		
		return $rr;
	}
	
	/*设置录像计划*/
	public function record_plan($device_serial = '',$chan_no = 0 ){
		
		if ($device_serial == '' || $chan_no == 0) return false;
		$plan = '{
					"0":[{"st_time":"0:0","en_time":"23:59:59"}],
					"1":[{"st_time":"0:0","en_time":"23:59:59"}],
					"2":[{"st_time":"0:0","en_time":"23:59:59"}],
					"3":[{"st_time":"0:0","en_time":"23:59:59"}],
					"4":[{"st_time":"0:0","en_time":"23:59:59"}],
					"5":[{"st_time":"0:0","en_time":"23:59:59"}],
					"6":[{"st_time":"0:0","en_time":"23:59:59"}]
					
				}';
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.intval($chan_no).',"enable":1,"cur_index":1,"cur_stream":3,"plan":'.$plan.'}';//post数据
		//var_dump($post_data);
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/playback/setplan';//接口地址
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//var_dump($rr);
		$rr = json_decode($rr,true);//json数据转为数组
		
		return $rr;
	}
	
	/*停止录像计划*/
	public function stop_record_plan($device_serial = '',$chan_no = 0 ){
		
		if ($device_serial == '' || $chan_no == 0) return false;
		$plan = '{
					"0":[],
					"1":[],
					"2":[],
					"3":[],
					"4":[],
					"5":[],
					"6":[]
					
				}';
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.intval($chan_no).',"enable":0,"cur_index":1,"cur_stream":3,"plan":'.$plan.'}';//post数据
		//var_dump($post_data);
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/playback/setplan';//接口地址
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//var_dump($rr);
		$rr = json_decode($rr,true);//json数据转为数组
		//************************************************************
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.intval($chan_no).'}';//post数据
		var_dump($post_data);
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/playback/getplan';//接口地址
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		var_dump($rr);
		$rr = json_decode($rr,true);//json数据转为数组
		
		
		
		
		
		
		return $rr;
	}	
	
	/*设置设备通道的素材在七牛保存的时间*/
	public function save_time($device_serial = '',$chan_no = 0 ,$option = 0,$time = 0 ,$savepath = 'Q',$rec_mode = 0){
		
		
		//return;
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.intval($chan_no).',"option":'.intval($option).',"minute":'.intval($time).',"rec_mode":'.$rec_mode.'}';//post数据
		//echo $post_data;
		if(strtoupper($savepath) == 'Q'){
			$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/custom/v2/qiniu/html5';//七牛接口地址
		} 
		if(strtoupper($savepath) == 'A'){
			$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/custom/v2/aliyun/html5';//阿里接口地址
		}
		
		//echo $url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '请求内容：'.$post_data."\n";
		//echo '请求地址：'.$url."\n";
		//echo '返回内容：'.$rr."\n";
		
		//header('shilian1:'.$post_data);
		//header('shilian2:'.$url);
		//header('shilian3:'.$rr);
		
		$rr = json_decode($rr,true);//json数据转为数组

		return $rr;//返回存储的秒数
		
		
	}
	
	/*获取回放地址*/
	public function get_playback_url($media_id,$start_time,$end_time,$savepath = 'Q'){

		$mediaInfo = M('tmedia')
								->cache(true,60)
								->field('tdevice.fcode as device_serial,tmedia.fchannelid,m3u8_backup')
								->join('tdevice on tdevice.fid = tmedia.fdeviceid')
								->where(array('tmedia.fid'=>$media_id))
								->find();
								
					
		$m3u8_backup_arr = json_decode($mediaInfo['m3u8_backup'],true);	//获取媒介备份
		
		
		$device_serial = $mediaInfo['device_serial'];//设备序列号	
		$chan_no = $mediaInfo['fchannelid'];//通道号	
		
		foreach($m3u8_backup_arr as $m3u8_backup_info){
			if($start_time > $m3u8_backup_info['s_t'] && $start_time < $m3u8_backup_info['e_t']){//判断开始时间是否在备份的区间
				
				
				$deviceInfo = explode('@',$m3u8_backup_info['dpid']);
				
				$device_serial = $deviceInfo[0];//设备序列号	
				$chan_no = $deviceInfo[1];//通道号				
							
			
				break;//结束循环
			}
			

		}

		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.intval($chan_no).',"start_time":'.intval($start_time).',"end_time":'.intval($end_time).'}';//post数据


		if(strtoupper($savepath) == 'Q') $url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/custom/v2/qiniu/playback';//七牛接口地址
		if(strtoupper($savepath) == 'A') $url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/custom/v2/aliyun/playback';//阿里接口地址
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));

		$rr = http($url,$post_data,'POST',$header,15);//请求

		$rr = json_decode($rr,true);//json数据转为数组

		if($rr['ret'] == 0){
			$html5_url = $rr['html5_url'];
			
			return $html5_url;//返回回播地址
			
		}else{
			return false;//获取失败返回false
		}
										
		
	}

	public function get_anhui_media(){
		$fmediaid = M("tmedia")
						->field("tmedia.fid")
						->where("left(tmedia.media_region_id,2) = 34")
						->select();
	
		foreach ($fmediaid as $key => $value) {
			$anhui_media[] = $value['fid'];
		}
		return $anhui_media;
	}
	
	/*根据通道号获取通道是否在线*/
	public function device_checkonline($dpid_arr){
		//var_dump($dpid_arr);
		$dpid_json = json_encode($dpid_arr);
		$post_data = '{"dpid":'.$dpid_json.'}';//post数据
		//var_dump($post_data);
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/v2/device/checkonline';//接口地址
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header,5);//请求
		//var_dump($rr);
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
	}
	
	/*设置拾联的消息推送接口地址*/
	public function set_msg_api($push_url){
		

		$post_data = '{"account":"'.C('SHILIAN_ACCOUNT').'","push_url":"'.$push_url.'","push_context":"push_context","push_flag":7}';//post数据
		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/v2/push/set';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
	}
	
	/*添加设备*/
	public function device_add($device_serial){


		$shilian_server = C('SHILIAN_SERVER');
		$shilian_authorization = C('SHILIAN_AUTHORIZATION');
		
	
	
		$post_data = '{"device_serial":"'.$device_serial.'","device_type_id":"15","account":"'.C('SHILIAN_ACCOUNT').'"}';//post数据
		$url = 'http://'.$shilian_server.':'.C('SHILIAN_API_PORT').'/openapi/device/add';//接口地址
		
		$header = array('Content-Type: application/json','Authorization: '.$shilian_authorization);
		
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '请求内容：'.$post_data."\n";
		//echo '请求地址：'.$url."\n";
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr['device_no'];//返回
	}
	
	/*修改设备、通道名称*/
	public function device_update($device_serial,$chan_no,$name){
		
		$name = str_replace(
							array('（','）'),
							array('(',')'),
							$name
							);
		$name = str_replace(
							array('(备用)'),
							array(''),
							$name
							);
							

		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.intval($chan_no).',"name":"'.$name.'","osd":1,"account":"'.C('SHILIAN_ACCOUNT').'"}';//post数据
		
		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/device/update';//接口地址

		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		//echo '请求响应时间：'.date('H:i:s');
		
		
		return $rr['ret'];//返回
	}
	
	
	
	/*删除设备*/
	public function device_del($device_serial){
		

		$post_data = '{"device_serial":"'.$device_serial.'","account":"'.C('SHILIAN_ACCOUNT').'"}';//post数据
		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/device/del';//接口地址
		
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组

		return $rr['ret'];//返回
	}
	
	/*获取版本*/
	public function version(){
		$post_data = '{}';//post数据

		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/version';//接口地址

		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
	}
	
	/*获取温湿度信息*/
	public function custom_sensor_all(){
		$post_data = '{"account":"'.C('SHILIAN_ACCOUNT').'"}';//post数据
		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/custom/v2/sensor/all';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
	}
	
	/*获取手机直播页面*/
	public function preview_html5($device_serial,$chan_no){
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.$chan_no.'}';//post数据
		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/preview/html5';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
	}
	
	/*获取rtmp观看地址*/
	public function preview_rtmp($device_serial,$chan_no,$time_out){
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.$chan_no.',"time_out":'.$time_out.'}';//post数据
		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/preview/rtmp';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
	}
	
	
	/*获取账号授权码*/
	public function getauth(){
		
		$post_data = '{"account":"'.C('SHILIAN_ACCOUNT').'"}';//post数据
		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/account/getauth';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
	}
	
	
	
	
	/*设置移动侦测*/
	public function detection($device_serial,$chan_no,$option){
		$chan_no = intval($chan_no);
		$option = intval($option);
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.$chan_no.',"option":'.$option.'}';//post数据

		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/v3/alarm/detection';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
		
	}
	
	
	
	
	/*拾联的红外控制*/
	public function infrared_control($device_serial,$chan_no,$code,$duration = 0){

		$device_serial = strval($device_serial);//设备序列号
		$chan_no = intval($chan_no);//设备通道号
		$code = intval($code);//控制代码
		$duration = intval($duration);//持续时间（单位秒），0表示只发送一次
		
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.$chan_no.',"code":'.$code.',"duration":'.$duration.'}';//post数据

		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/custom/v3/infrared/control';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
		
	}
	
	
	/*设置视频分辨率*/
	public function preview_setquality($device_serial,$chan_no,$quality){

		$device_serial = strval($device_serial);//设备序列号
		$chan_no = intval($chan_no);//设备通道号


		
		$post_data = '{"device_serial":"'.$device_serial.'","chan_no":'.$chan_no.',"quality":'.$quality.'}';//post数据

		//echo '请求内容：'.$post_data."\n";
		$url = 'http://'.C('SHILIAN_SERVER').':'.C('SHILIAN_API_PORT').'/openapi/preview/setquality';//接口地址
		//echo '请求地址：'.$url."\n";
		$header = array('Content-Type: application/json','Authorization: '.C('SHILIAN_AUTHORIZATION'));
		$rr = http($url,$post_data,'POST',$header);//请求
		//echo '返回内容：'.$rr."\n";
		$rr = json_decode($rr,true);//json数据转为数组
		return $rr;//返回
		
	}
	
	
	
	

	
	
	
	
}
<?php
namespace Common\Model;

class FunctionModel{

	/**
	 * 上级广告类别代码
	 * $fpcode广告类别父ID，$self  是否加自己
	 * by zw
	 */
	public function get_next_tadclasscode($fpcode,$self=false) {
		$data = M('tadclass')->cache(true,600)->where(array('fpcode'=>$fpcode))->getField('fcode',true); 
		if($data && $self){
			$data[]=$fpcode;
		}
		return $data;
	}

    /**
     * 下级地区
     * by zw
     */
    public function get_next_region($area) {
        $data = [];
        if(empty($area)){
            return $data;
        }

        $tregion_len = get_tregionlevel($area);
        if($tregion_len == 2){//省级
            $whereStr = 'fid like "'.substr($area,0,2).'%"';
        }elseif($tregion_len == 4){//市级
            $whereStr = 'fid like "'.substr($area,0,4).'%"';
        }elseif($tregion_len == 6){//县级
            $data[] = $area;
            return $data;
        }

        $data = M('tregion')->cache(true,600)->where($whereStr)->getField('fid',true); 
        return $data;
    }

	/**
	 * 处理违法表现
	 * $fexpressioncodes 违法表现代码
	 * by zw
	 */
    public function sampleillegal($fexpressioncodes){
    	if(empty($fexpressioncodes)){
    		return false;
    	}
    	$codes = explode(';', $fexpressioncodes);
		foreach($codes as $v){//循环违法表现代码
			if(!empty($v)){
				$illegal_info = M('tillegal')->cache(true,86400)->where(array('fcode'=>$v,'fstate'=>1))->find();
				if($illegal_info){
					$fexpression .= $illegal_info['fexpression'].';'."\n";//把合规的违法表现加入字符串
					$fconfirmation .= $illegal_info['fconfirmation'].';'."\n";//把合规的认定依据加入字符串
					$fpunishment .= $illegal_info['fpunishment'].';'."\n";//把合规的处罚依据加入字符串
					$fpunishmenttype .= $illegal_info['fpunishmenttype'].';'."\n";//把合规的处罚种类及幅度加入字符串
				}
			}
		}
		$return_data['fexpressioncodes'] = rtrim($fexpressioncodes,';');//去掉末尾分号
		$return_data['fexpressions'] = rtrim($fexpression,';'."\n");//去掉末尾分号
		$return_data['fconfirmations'] = rtrim($fconfirmation,';'."\n");//去掉末尾分号
		$return_data['fpunishments'] = rtrim($fpunishment,';'."\n");//去掉末尾分号
		$return_data['fpunishmenttypes'] = rtrim($fpunishmenttype,';'."\n");//去掉末尾分号
		return $return_data;
    }

    /**
     * 获取广告明细数据（包含节目）
     * by zw
     * $datatype 默认所有数据（广告、节目），1广告，2节目，3节目片断和广告
     */
    public function getAdList($mclass,$mediaId,$issueDate,$fields = '',$datatype = 0){
        $data1 = [];
        $data2 = [];
        $data3 = [];
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            return false;
        }

        if(is_array($issueDate)){
            $tbYear = date('Y',strtotime($issueDate[0]));
            $where_issue['a.fissuedate'] = ['between',[date('Y-m-d',strtotime($issueDate[0])),date('Y-m-d',strtotime($issueDate[1]))]];
            $where_prog['a.fissuedate'] = ['between',[date('Y-m-d',strtotime($issueDate[0])),date('Y-m-d',strtotime($issueDate[1]))]];
            $where_prog_detail['a.fissuedate'] = ['between',[date('Y-m-d',strtotime($issueDate[0])),date('Y-m-d',strtotime($issueDate[1]))]];
        }else{
            $tbYear = date('Y',strtotime($issueDate));
            $where_issue['a.fissuedate'] = date('Y-m-d',strtotime($issueDate));
            $where_prog['a.fissuedate'] = date('Y-m-d',strtotime($issueDate));
            $where_prog_detail['a.fissuedate'] = date('Y-m-d',strtotime($issueDate));
        }

        //广告
        if($datatype == 0 || $datatype == 1 || $datatype == 3){
            $where_issue['a.fmediaid'] = $mediaId;
            $where_issue['a.fstate'] = 1;
            $where_issue['b.fstate'] = 1;
            $data1 = M('t'.$tb_str.'issue_'.$tbYear)
                ->alias('a')
                ->field('1 adtype,c.fadname title,a.fid issueid'.$fields)
                ->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)')
                ->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
                ->join('tadclass d on c.fadclasscode = d.fcode')
                ->join('tadowner e on c.fadowner = e.fid')
                ->where($where_issue)
                ->select();
        }
        
        if($mclass == '01' || $mclass == '02'){
            //节目
            if($datatype == 0 || $datatype == 2){
                $where_prog['a.fmediaid'] = $mediaId;
                $where_prog['a.fstate'] = 1;
                $data2 = M('t'.$tb_str.'prog')
                    ->alias('a')
                    ->field('2 adtype,a.fprogid issueid,a.fissuedate,a.fstart fstarttime,a.fend fendtime,a.fcontent title'.$fields)
                    ->join('tprogclass d on a.fclasscode = d.fprogclasscode')
                    ->where($where_prog)
                    ->select();
            }

            //节目片断
            if($datatype == 3){
                $where_prog_detail['a.fmediaid'] = $mediaId;
                $where_prog_detail['a.fstate'] = 1;
                $data3 = M('t'.$tb_str.'prog_detail')
                    ->alias('a')
                    ->field('2 adtype,a.fprogid issueid,a.fissuedate,a.fstart fstarttime,a.fend fendtime,a.fcontent title'.$fields)
                    ->join('tprogclass d on a.fclasscode = d.fprogclasscode')
                    ->where($where_prog_detail)
                    ->select();
            }
        }
        
        $data = array_merge($data1,$data2,$data3);
        return $data;
    }

    /**
	 * 更新广告的品牌数据
	 * by zw
	 */
    public function createSerialBroadcastList($mclass = '',$mediaId,$issueDate){
        ini_set('memory_limit','2048M');
        ini_set('max_execution_time', 600);//设置超时时间
        session_write_close();
    	if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            return false;
        }

    	$data = $this->getAdList($mclass,$mediaId,$issueDate,',a.*',3);//获取媒体当日全部数据
    	$data = pxsf($data,'fstarttime',false);//排序
    	$ggData = [];//广告数据
        $nowNum = 0;
        $prvEndTime = 0;//上一结束时间
        $prvPrg = '';//上一节目
        $nextPrg = '';//下一节目
        $prvAd = '';//前广告
        $prvAdKey = 0;//前广告key
        $nextAd = '';//后广告
        $nowKeys = [];//连续的广告ID组
    	foreach ($data as $key => $value) {
            //获取后节目
            if(empty($nextPrg) && $mclass == '01'){
                $nextPrg = $this->getJMWeiZhi(($key+1),$data);
            }
            $title1 = mb_substr($value['title'], 0, 3,'utf-8');//公益

            //更新前连续广告的位置
            if($value['title'] <> '节目预告' && $value['title'] <> '本媒体宣传' && $value['title'] <> '报时' && $value['title'] <> '台标' && $title1 <> '公益（' && $value['title'] <> '社会公益'){
                if((strtotime($value['fstarttime']) - $prvEndTime)>60){
                    foreach ($nowKeys as $nk) {
                        $data[$nk]['fadsort'] .= '/'.$nowNum;
                    }
                    $nowNum = 0;
                    $nowKeys = [];
                    $prvAd = '';
                    $prvAdKey = 0;
                }
            }

    		if($value['adtype'] == 1){
                if($value['title'] <> '节目预告' && $value['title'] <> '本媒体宣传' && $value['title'] <> '报时' && $value['title'] <> '台标' && $title1 <> '公益（' && $value['title'] <> '社会公益'){
                    $nowNum += 1;
                    $data[$key]['fpriorprg'] = $prvPrg?$prvPrg:'';//前节目
                    $data[$key]['fnextprg'] = $nextPrg?$nextPrg:'';//后节目
                    $data[$key]['fpriorad'] = $prvAd;//前广告
                    $data[$key]['fnextad'] = '';//后广告
                    //更新前一广告的后广告
                    if(!empty($prvAdKey)){
                        $data[$prvAdKey]['fnextad'] = $value['title'];
                    }
                    $data[$key]['fadsort'] = $nowNum;//位置
                    if($prvPrg<>$nextPrg || empty($prvPrg) || empty($nextPrg)){
                        $data[$key]['fissuetype'] = '平播';
                    }else{
                        $data[$key]['fissuetype'] = '插播';
                    }

                    $prvEndTime = strtotime($value['fendtime']);
                    $prvAd = $value['title'];//作后一广告的前广告
                    $nowKeys[] = $key;
                    $prvAdKey = $key;
                }else{
                    $data[$key]['fpriorprg'] = '';//前节目
                    $data[$key]['fnextprg'] = '';//后节目
                    $data[$key]['fpriorad'] = '';//前广告
                    $data[$key]['fnextad'] = '';//后广告
                    $data[$key]['fadsort'] = '';//位置
                    $data[$key]['fissuetype'] = '';//类型
                }
    		}else{
                $nextPrg = '';
    			$prvPrg = $value['title'];
                $prvAd = '';
    			$nowprgData['pid'] = $value['fprogpid'];
                $prvAdKey = 0;
    		}

            //最后一条记录时，更新前广告的位置
            if($key == (count($data)-1)){
                foreach ($nowKeys as $nk) {
                    $data[$nk]['fadsort'] .= '/'.$nowNum;
                }
            }
    	}

    	if(!empty($data)){
            if(is_array($issueDate)){
                $tbYear = date('Y',strtotime($issueDate[0]));
            }else{
                $tbYear = date('Y',strtotime($issueDate));
            }
            foreach ($data as $key => $value) {
                if($value['adtype'] == 1){
                    $ggData[] = $value;
                }
            }

	    	M('t'.$tb_str.'issue_'.$tbYear)->addAll($ggData,[],true);
    	}
    }

    /*
    * 获取节目位置
    * by zw
    */
    public function getJMWeiZhi($key,$data){
    	if(!empty($data)){
    		while ($key <count($data)) {
    			if($data[$key]['adtype'] == 2){
    				$retData = $data[$key]['title'];
    				return $retData;
    			}
	    		$key+=1;
	    	}
    	}else{
    		return false;
    	}
    }

    /*
    * 更改广告发布数据的发布状态
    * by zw
    * mediaId 媒体ID，issueDate日期，dateType日期类型（0具体日期、1日期数组、2年），state发布平台（0数据源发布，10监测平台发布，20监管平台发布）
    */
    public function adIssueSendState($mediaId,$issueDate,$dateType = 0,$state = 0){
        $doMa = M('tmedia')
            ->cache(true,3600)
            ->alias('a')
            ->field('fmediaclassid')
            ->where(['fid'=>$mediaId,'fstate'=>1])
            ->find();
        $mclass = substr($doMa['fmediaclassid'],0,2);
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }elseif($mclass == '03' || $mclass == '04'){
            $tb_str = 'paper';
        }else{
            return 0;
        }
        switch ($dateType) {
            case 0:
                $year = date('Y',strtotime($issueDate));
                $where['fissuedate'] = $issueDate;
                break;
            case 1:
                if(!is_array($issueDate)){
                    return 0;
                }
                $year = date('Y',strtotime($issueDate[0]));
                $where['fissuedate'] = ['in',$issueDate];
                break;
            case 2:
                if(strlen($issueDate) != 4){
                    return 0;
                }
                $year = $issueDate;
                break;
            default:
                return 0;
                break;
        }
        $where['fmediaid'] = $mediaId;

        $doIssue = M('t'.$tb_str.'issue_'.$year)->where($where)->save(['fsendstate'=>$state]);
        return $doIssue;
    }

    /*
    * by zw
    * 建不存在的发布表
    */
    public function isIssueTable($tbName,$mClass){
        $isTable = M()->query('SHOW TABLES LIKE "'.$tbName.'"');
        if(!empty($isTable)){
            return true;
        }else{
            if($mClass == '03' || $mClass == '04'){
                $do_tb = M()->execute("CREATE TABLE `".$tbName."` (
                    `fid` BIGINT(14) NOT NULL AUTO_INCREMENT COMMENT '发布ID',
                    `fsampleid` BIGINT(14) UNSIGNED NOT NULL DEFAULT '0' COMMENT '样本ID',
                    `fmediaid` BIGINT(14) UNSIGNED NOT NULL DEFAULT '0' COMMENT '媒体ID',
                    `fissuedate` DATETIME NOT NULL COMMENT '发布日期',
                    `fpage` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '版面',
                    `fpagetype` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '版面类别',
                    `fsize` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '规格',
                    `fissuetype` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '发布类型（黑白、套红、彩色）',
                    `fquantity` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
                    `fproportion` DOUBLE(8,2) NOT NULL DEFAULT '1.00' COMMENT '面积比例',
                    `fprice` INT(11) NOT NULL DEFAULT '0' COMMENT '金额',
                    `fcreator` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '创建人',
                    `fstate` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态（-1删除，0无效，1-有效）',
                    `fmodifier` VARCHAR(20) NULL DEFAULT '' COMMENT '修改人',
                    `fmodifytime` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
                    `fsendstate` TINYINT(2) NOT NULL DEFAULT '10' COMMENT '发布平台（0数据源发布，10监测平台发布，20监管平台发布）',
                    `fautotime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '获取时间',
                    `fidentify` BIGINT(14) NOT NULL DEFAULT '0' COMMENT '数据源主键',
                    `fself` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '数据是否私有（即是是否被用户调整过）',
                    PRIMARY KEY (`fid`),
                    INDEX `IX_TPAPERISSUE_FSAMPLEID` (`fsampleid`) USING BTREE,
                    INDEX `IX_TPAPERISSUE_MEDIAID_ISSUEDATE` (`fmediaid`, `fissuedate`) USING BTREE,
                    INDEX `IX_TPAPERISSUE_SIZE` (`fsize`) USING BTREE,
                    INDEX `fissuedate` (`fissuedate`) USING BTREE,
                    INDEX `fidentify` (`fidentify`) USING BTREE,
                    INDEX `fself` (`fself`) USING BTREE,
                    INDEX `fstate` (`fstate`) USING BTREE,
                    INDEX `IDX_MEDIA_SAMPLE_DATE_STATE` (`fstate`, `fsendstate`, `fissuedate`, `fmediaid`, `fsampleid`) USING BTREE,
                    INDEX `fsendstate` (`fsendstate`) USING BTREE
                )
                COMMENT='报纸发布'
                COLLATE='utf8_general_ci'
                ENGINE=InnoDB
                AUTO_INCREMENT=876341;
                ");
            }else{
                $do_tb = M()->execute("CREATE TABLE `".$tbName."` (
                    `fid` BIGINT(18) NOT NULL AUTO_INCREMENT COMMENT '主键',
                    `fsampleid` BIGINT(18) NOT NULL DEFAULT '0' COMMENT '样本ID',
                    `fmediaid` BIGINT(18) NOT NULL DEFAULT '0' COMMENT '媒介ID',
                    `fissuedate` DATE NOT NULL COMMENT '发布日期',
                    `fstarttime` DATETIME NOT NULL COMMENT '开始时间',
                    `fstarttime` DATETIME(3) NOT NULL COMMENT '开始时间（含毫秒）',
                    `fendtime` DATETIME NOT NULL COMMENT '结束时间',
                    `fendtime` DATETIME(3) NOT NULL COMMENT '结束时间（含毫秒）',
                    `flength` INT(11) NOT NULL DEFAULT '0' COMMENT '发布时长',
                    `fissuetype` varchar(10) NULL DEFAULT '' COMMENT '发布类型',
                    `fadsort` varchar(10) NULL COMMENT '广告段排序',
                    `fpriorad` varchar(100) NULL COMMENT '前一广告',
                    `fnextad` varchar(100) NULL COMMENT '后一广告',
                    `fpriorprg` varchar(100) NULL COMMENT '前一节目',
                    `fnextprg` varchar(100) NULL COMMENT '后一节目',
                    `fquantity` int(11) DEFAULT '1' COMMENT '数量',
                    `fprice` INT(11) NOT NULL DEFAULT '0' COMMENT '金额',
                    `fregionid` INT(11) NOT NULL DEFAULT '0' COMMENT '行政区划代码',
                    `fcreator` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '创建人',
                    `fstate` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态（-1删除，0无效，1-有效）',
                    `fautotime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '获取时间',
                    `fidentify` BIGINT(14) NOT NULL DEFAULT '0' COMMENT '数据源主键',
                    `fself` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '数据是否私有（即是是否被用户调整过）',
                    `fmodifier` VARCHAR(100) NULL DEFAULT '' COMMENT '修改人',
                    `fmodifytime` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
                    `fsendstate` TINYINT(2) NOT NULL DEFAULT '10' COMMENT '发布平台（0数据源发布，10监测平台发布，20监管平台发布）',
                    PRIMARY KEY (`fid`),
                    INDEX `fstarttime` (`fstarttime`) USING BTREE,
                    INDEX `fissuedate` (`fissuedate`) USING BTREE,
                    INDEX `fsampleid` (`fsampleid`) USING BTREE,
                    INDEX `fissuedate_fstarttime` (`fissuedate`, `fstarttime`) USING BTREE,
                    INDEX `fregionid` (`fregionid`) USING BTREE,
                    INDEX `fidentify` (`fidentify`) USING BTREE,
                    INDEX `fself` (`fself`) USING BTREE,
                    INDEX `fstate` (`fstate`) USING BTREE,
                    INDEX `IDX_MEDIA_SAMPLE_DATE_STATE` (`fstate`, `fsendstate`, `fissuedate`, `fmediaid`, `fsampleid`) USING BTREE,
                    INDEX `fmediaid` (`fmediaid`) USING BTREE,
                    INDEX `fsendstate` (`fsendstate`) USING BTREE
                )
                COMMENT='发布表'
                COLLATE='utf8_general_ci'
                ENGINE=InnoDB
                AUTO_INCREMENT=322990;
                ");
            }
            return true;
        }
    }

}
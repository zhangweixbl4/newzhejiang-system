<?php
namespace Api\Controller;
use Think\Controller;

class MediaController extends Controller {
	
	public $oParam;//公共变量
	public function _initialize() {
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		header("Content-type:text/html;charset=utf-8");
		$this->oParam = json_decode(file_get_contents('php://input'),true);//接收数据
	}

	public function live(){
		
		$fdpid = I('chnID');
		
		$mediaInfo = M('tmedia')->cache(true,60)->field('tdevice.fcode')->join('tdevice on tdevice.fid = tmedia.fdeviceid')->where(array('fdpid'=>$fdpid))->find();

		$shilian_server = C('SHILIAN_SERVER');
		$shilian_live_auth = C('SHILIAN_LIVE_AUTH');
		
		
		
		$this->assign('shilian_server',$shilian_server);
		$this->assign('shilian_live_auth',$shilian_live_auth);
		
		$this->display();
	}
	
	public function get_media_list(){
		$media_name = I('media_name');//获取媒介名称
		$fmediaclassid = I('fmediaclassid');
		$regionid = intval(I('regionid'));
		$where = array();
		$where['tmedia.fstate'] = array('neq',-1);
		$where['fmedianame'] = array('like','%'.$media_name.'%');
		if($fmediaclassid != ''){
			$where['left(fmediaclassid,2)'] = $fmediaclassid;//媒介类型
		}
		if($regionid > 0 && $regionid != 100000){

			$region_id_rtrim = str_replace('00','',$regionid);//地区ID去掉末尾的0
			$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
			$where['left(tmedia.media_region_id,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
		
		}
		$mediaList = M('tmedia')->field('tmedia.fid,(case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame')
													->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
													->where($where)->order('tmedia.fsort desc')->limit(10)->select();//查询媒介列表
		//var_dump(M('tmedia')->getLastSql());
		$this->ajaxReturn(array('code'=>0,'value'=>$mediaList));
		
	}

	public function get_live_list(){
		
		$media_name = I('media_name');//获取媒介名称
		$fmediaclassid = I('fmediaclassid');
		
		$where = array();
		$where['fstate'] = array('neq',-1);
		$where['fmedianame'] = array('like','%'.$media_name.'%');
		$where['fcollecttype'] = array('in','10,25');
		
		//$where['fdpid'] = array('gt',0);
		
		if($fmediaclassid != ''){
			$where['left(fmediaclassid,2)'] = $fmediaclassid;//媒介类型
		}
		$mediaList = M('tmedia')->field('(case when instr(tmedia.fmedianame,"（") > 0 then left(tmedia.fmedianame,instr(tmedia.fmedianame,"（") -1) else tmedia.fmedianame end) as fmedianame,fdpid,fid')->where($where)->order('fsort desc')->limit(10)->select();//查询媒介列表
		foreach($mediaList as $k => $media){
			$mediaList[$k] = $media;
			$mediaList[$k]['live_url'] = U('Api/Media/m3u8_live',array('fid'=>$media['fid']));	
		}
		//var_dump(M('tmedia')->getLastSql());
		$this->ajaxReturn(array('code'=>0,'value'=>$mediaList));
		
		
	}
	
	public function p_sort(){
		$media_name = I('media_name');//获取媒介名称
		M('tmedia')->where(array('fmedianame'=>$media_name))->save(array('fsort'=>array('exp','fsort + 1')));
		$this->ajaxReturn(array('code'=>0,'msg'=>''));
	}
	
	
	/*手机观看页面*/
	public function mobile_live(){
		
		$device_serial = I('device_serial');
		$chan_no = I('chan_no');
		$fid = I('fid');
		if(intval($fid) > 0){
			$mediaInfo = M('tmedia')->where(array('fid'=>$fid))->find();//媒介信息
		}else{
			if($chan_no == '') $_GET['chan_no'] = 1;
			$deviceInfo = M('tdevice')->where(array('fcode'=>$device_serial))->find();//设备信息
			$mediaInfo = M('tmedia')->where(array('fdeviceid'=>$deviceInfo['fid'],'fchannelid'=>$chan_no))->find();//媒介信息
		}

		$this->assign('mediaInfo',$mediaInfo);
		$this->display();
		
		
	}
	
	/*直播页面*/
	public function m3u8_live(){
		$fid = I('fid');//媒介ID
		$where = array();
		
		$where['fid'] = $fid;
		
		//获取媒体数据
		$gourl = 'http://'.C('MainServerUrl').'/Api/Media/m3u8_live_mediaview';
		$gourl = 'http://d.hz-data.com/Api/Media/m3u8_live_mediaview';
		$pubData['fid'] = $fid;
		$data = json_decode(http($gourl,$pubData,'POST',false,5),true);
		
		if(!empty($data['code'])){
			$mediaInfo = M('tmedia')->where($where)->find();
		}else{
			$mediaInfo = $data['data'];
		}

		if(str_replace(' ','',$mediaInfo['live_m3u8']) == ''){
			$mediaInfo['live_m3u8'] = U('Api/Media/get_media_m3u8@'.$_SERVER['HTTP_HOST'],array('media_id'=>$mediaInfo['fid']));
		}
		$this->assign('mediaInfo',$mediaInfo);
		$this->display();
		
	}
	
	/*多屏直播页面*/
	public function multi_live(){

		$where = array();
		$where['fstate'] = 1;

		$where['left(fmediaclassid,2)'] = array('in','01,02');
		

		$mediaList = M('tmedia')->field('fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,fdpid')->where($where)->order('fsort desc')->limit(1000)->select();//查询媒介列表
		foreach($mediaList as $k => $media){
			$mediaList[$k] = $media;
			$mediaList[$k]['live_url'] = U('Api/Media/m3u8_live',array('fid'=>$media['fid']));	
		}

		$this->assign('mediaList',$mediaList);
		$this->display();
	}
	
	/*获取媒介m3u8点播地址*/
	public function get_media_m3u8_url(){
		 
		$mediaId = I('mediaId');
		$startTime = I('startTime');
		$endTime = I('endTime');
		$is_source = I('is_source');
		if($is_source == '') $is_source = 1;
		
		
		$m3u8Url = A('Common/Media','Model')->get_m3u8($mediaId,$startTime,$endTime,$is_source);
		$this->ajaxReturn(array('code'=>0,'m3u8Url'=>$m3u8Url));
	}
	
	
	
	
	/*获取媒介m3u8点播地址*/
	public function get_media_m3u8_2(){
		session_write_close();
		if(strval(I('no')) == ''){
			header("Content-type: application/x-mpegURL; charset=utf-8");
		}
		
		
		header('Access-Control-Allow-Origin:*');
		$start_time = I('start_time');//开始时间,时间戳
		$end_time = I('end_time');//结束时间,时间戳
		$media_id = I('media_id');//媒介ID,

		if(S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time)){//判断有没有缓存
			//echo '#echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time."\n";
			echo S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time);//使用缓存
			exit;
		}
			
		$echo_m3u8 = "#EXTM3U\n";
		$echo_m3u8 .=  "#EXT-X-ALLOW-CACHE:NO\n";
		$echo_m3u8 .=  "#EXT-X-TARGETDURATION:10\n";
		$table_name = 't'.date('Y-m-d',$start_time);//m3u8表的名称
		$where = array();
		$where['fmediaid'] = $media_id;//媒介id
		$where['start_time'] = array('between',array($start_time - 20,$end_time));
		$where['end_time'] = array('between',array($start_time,$end_time + 20));

		
		if(intval($start_time) > 0){

			$m3u8_list = M($table_name,'',C('M3U8_DB2'))->where($where)->select();

		}else{
			
			$m3u8_list = M('t'.date('Y-m-d'),'',C('M3U8_DB2'))->where(array('fmediaid'=>$media_id))->order('start_time desc')->limit(4)->select();
			$m3u8_list = array_reverse($m3u8_list);
			$echo_m3u8 .=  '#EXT-X-MEDIA-SEQUENCE:'.$m3u8_list[0]['fid']."\n";
			
		}
		
		foreach($m3u8_list as $m3u8){
			$echo_m3u8 .=  "#EXTINF:". (intval($m3u8['end_time']) - intval($m3u8['start_time']) + 1) .",\n";

			$echo_m3u8 .=  $m3u8['ts_url']."\n";
			
		}
		if(intval($start_time) > 0){
			$echo_m3u8 .=  "#EXT-X-ENDLIST";
		}
		
		
		echo $echo_m3u8;
		
		if(intval($start_time) > 0){
			S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time,$echo_m3u8,3600);
		}else{
			S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time,$echo_m3u8,20);
		}
		
	}
	
	/*获取媒介m3u8点播地址*/
	public function get_media_m3u8(){
		session_write_close();
		
		if(strval(I('no')) == ''){
			header("Content-type: application/x-mpegURL; charset=utf-8");
		}
		$get_client_ip = get_client_ip();
		$clientIpArr = explode('.',$get_client_ip);
		
		$is_innernet = false;
		if(($clientIpArr[0] == '192' && $clientIpArr[1] == '168') || ($clientIpArr[0] == '172' && $clientIpArr[1] == '16')){
			$is_innernet = true;
			header("is_innernet:true");
		}
		
		header('Access-Control-Allow-Origin:*');
		$start_time = I('start_time');//开始时间,时间戳
		$end_time = I('end_time');//结束时间,时间戳
		$media_id = I('media_id');//媒介ID,
		

		if(S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time)){//判断有没有缓存
			header('cache:true');
			echo S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time);//使用缓存
			exit;
		}
			
		$echo_m3u8 = "#EXTM3U\n";
		$echo_m3u8 .=  "#EXT-X-ALLOW-CACHE:NO\n";
		$echo_m3u8 .=  "#EXT-X-TARGETDURATION:10\n";


		if(intval($start_time) > 0){

			$startPK = array (
				array('fmediaid', intval($media_id)),
				array('start_time',$start_time - 10),
				array('end_time',$start_time + 0),
				array('fdate', date('Y-m-d',$start_time)),
			);
			$endPK = array (
				array('fmediaid', intval($media_id)),
				array('start_time',$end_time - 0),
				array('end_time',$end_time + 10),
				array('fdate', date('Y-m-d',$start_time)),
			);
			//var_dump($startPK);
			$limit = intval((($end_time - $start_time) / 10 ) + 40);
			$m3u8_list = A('Common/TableStore','Model')->getRange('ts_key',$startPK,$endPK,$limit);
			//var_dump($m3u8_list);
		}else{
			$startPK = array (
				array('fmediaid', intval($media_id)),
				array('start_time',time()),
				array('end_time',time()),
				array('fdate', date('Y-m-d')),
			);
			$endPK = array (
				array('fmediaid', intval($media_id)),
				array('start_time',time() - 3600),
				array('end_time',time() - 3600),
				array('fdate', date('Y-m-d')),
			);

			$limit = 3;
			$m3u8_list = A('Common/TableStore','Model')->getRange('ts_key',$startPK,$endPK,$limit,'BACKWARD');
			$m3u8_list = array_reverse($m3u8_list);
			
			
			$echo_m3u8 .=  '#EXT-X-MEDIA-SEQUENCE:'.$m3u8_list[0]['start_time']."\n";
			
		}
		//var_dump($m3u8_list);
		foreach($m3u8_list as $m3u8){
			$echo_m3u8 .=  "#EXTINF:". (intval($m3u8['end_time']) - intval($m3u8['start_time']) + 1) .",\n";
			
			$ts_url = $m3u8['ts_url'];
			
			if($is_innernet === true){
				//$ts_url = str_replace('shilian-yunhe.oss-cn-hangzhou.aliyuncs.com','shilian-yunhe.oss-cn-hangzhou-internal.aliyuncs.com',$ts_url);
				//$ts_url = str_replace('ovsc84o05.com0.z0.glb.clouddn.com','intranet-qiniusource.hz-data.xyz',$ts_url);

			}
			$echo_m3u8 .=  $ts_url."\n";
			
		}
		if(intval($start_time) > 0){
			$echo_m3u8 .=  "#EXT-X-ENDLIST";
		}
		
		
		echo $echo_m3u8;
		//exit;
		if(intval($start_time) > 0){
			S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time,$echo_m3u8,3600);
		}else{
			S('echo_m3u8_'.$media_id.'_'.$start_time.'_'.$end_time,$echo_m3u8,15);
		}
		
	}
	
	/*提取M3U8里面的ts*/
	public function extract_m3u8_ts(){
		$m3u8_url = urldecode(I('m3u8_url','',''));//m3u8地址
		$media_id = I('media_id');//媒介ID
		$host = preg_replace('/(.*)\/{1}([^\/]*)/i', '$1', $m3u8_url);//取出域名
		$List = explode("\n",http($m3u8_url));//转换成数组
		
		foreach($List as $ts){//重新组装数组

			if(strstr($ts,'#EXTINF:') && $ts != ''){//提取时间索引
				
				$tsList .= str_replace(array('#EXTINF:',','),array("\n\n",''),$ts);
			}
			
			if(!strstr($ts,'#') && $ts != ''){//提取碎片
				if(strstr($ts,'http')){
					$tsList .= "\n".$ts;
				}else{
					$tsList .= "\n".$host.'/'.$ts;
				}
				
			}

		}

		$tsList = explode("\n\n",$tsList);
		
		foreach($tsList as $tts){
			$tts = explode("\n",$tts);
			$ts = $tts[1];

			$len +=  $tts[0] + 0;//计算总时长

			if(M('media_drift')->where(array('url'=>$ts))->count() == 0 && $ts != ''){//判断数据库不重复 且ts地址不为空
				
				if(intval(S('media_drift_'.$media_id)) > 0){//缓存媒介采集到的时间点
					S('media_drift_'.$media_id,intval(S('media_drift_'.$media_id)) + ($tts[0] * 1000) ,60);//设置媒介的采集到的时间点，单位毫秒
				}else{
					S('media_drift_'.$media_id,(time() * 1000) + $a_data['length'] ,60);//设置媒介的采集到的时间点，单位毫秒
				}
				
				$a_data = array();
				$a_data['media_id'] = $media_id;
				$a_data['url'] = $ts;
				$a_data['create_time'] = date('Y-m-d H:i:s');
				$a_data['length'] = $tts[0] * 1000;
				$a_data['start_time'] = intval(S('media_drift_'.$media_id)) - $a_data['length'];
				$a_data['end_time'] = S('media_drift_'.$media_id);
				
				M('media_drift')->add($a_data);
				
			}
		}
		
		//var_dump($len);
		
		
		
	}
	
	/*分析m3u8完整性*/
	public function analysis_m3u8(){
		$m3u8 = I('m3u8');
		
		$List = explode("\n",http($m3u8,array(),'GET',false,30));
		
		foreach($List as $ts){
			if(!strstr($ts,'#')){
				$ts = substr($ts,-25,21);
				$ts = explode('-',$ts);
				$tsList[] = $ts; 
			}
			
			
		}
		
		$this->assign('tsList',$tsList);
		$this->display();
	}
	
	
	/*m3u8广播直播*/
	public function m3u8_bc_live(){
		$mediaId = I('mediaId');
		$start_time = I('start_time');
		if($start_time == ''){
			$is_live = 'true';
		}else{

			$is_live = 'false';
		}	
		
		$mediaInfo = M('tmedia')->where(array('fid'=>$mediaId))->find();
		$m3u8_url = $mediaInfo['live_m3u8'];
		if($start_time != ''){
			
			$start = strtotime($start_time);
			$end = strtotime($start_time) + 1800;
			$m3u8_url = $mediaInfo['fsavepath'].'&start='.$start.'&end='.$end;
		}
		
		
		$this->assign('mediaInfo',$mediaInfo);
		$this->assign('m3u8_url',$m3u8_url);
		$this->assign('is_live',$is_live);
		
		
		$this->display();
	}
	
	
	/*获取发布记录原始素材播放地址*/
	public function get_tv_issue_m3u8(){
		$issueid = I('issueid');//获取发布ID
		$table_name = I('table_name');
		
		$ftvissueid = I('ftvissueid');
		
		$where = array();
		
		if($ftvissueid){
			$table_name = 'ttvissue';
			$where['ftvissueid'] = $ftvissueid;
		}else{
			$where['fid'] = $issueid;
		}
		
		
		$issueInfo = M($table_name)->alias('ttvissue')->where($where)->find();
		
		if($ftvissueid){
			$m3u8_url = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],strtotime($issueInfo['fstarttime']),strtotime($issueInfo['fendtime']));
		}else{
			$m3u8_url = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$issueInfo['fstarttime'],$issueInfo['fendtime']);
		}
		
		
		if(I('redirect') == 'true'){
			header('Location:'.$m3u8_url);
			exit;
		}
		//
		if($m3u8_url){
			$this->ajaxReturn(array('code'=>0,'msg'=>'','m3u8_url'=>$m3u8_url));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有获取到原始素材','m3u8_url'=>$m3u8_url));
		}
		 
		
	}
	
	/*获取发布记录原始素材播放地址*/
	public function get_bc_issue_m3u8(){
		$issueid = I('issueid');//获取发布ID
		$table_name = I('table_name');
		$where = array();
		$where['fid'] = $issueid;
		
		$issueInfo = M($table_name)->alias('tbcissue')->where($where)->find();
		
		$m3u8_url = A('Common/Media','Model')->get_m3u8($issueInfo['fmediaid'],$issueInfo['fstarttime'],$issueInfo['fendtime']);
		
		if(I('redirect') == 'true'){
			header('Location:'.$m3u8_url);
			exit;
		}
		//
		if($m3u8_url){
			$this->ajaxReturn(array('code'=>0,'msg'=>'','m3u8_url'=>$m3u8_url));
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'没有获取到原始素材','m3u8_url'=>$m3u8_url));
		}
		
	}
	
	
	
	
	/*获取报纸原始素材*/
	public function get_papersource(){
		
		$media_id = I('media_id');//媒介id
		$issue_date = I('issue_date');//发布时间
		$page = I('page');//版面
		
		$where = array();
		
		$where['fmediaid'] = $media_id;
		
		if($issue_date != ''){//如果发布时间为空
			$where['fissuedate'] = $issue_date;//指定当天
		}
		
		
		
		if($page){
			$where['fpage'] = array('like','%'.$page.'%');
		}
		$paperSourceList = M('tpapersource')->where($where)->order('fissuedate desc')->limit(20)->select();
		
		
		$this->assign('paperSourceList',$paperSourceList);
		
		
		$this->display();
		
		
	}
	
	
	/*红外计划任务调度*/
	public function ir_plan(){ 
		echo date('H:i:s')."	";
		$planMediaList = M('tmedia')
								//->cache(true,600)
								->field('tmedia.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,fdpid,ir_plan,tmedia.fchannelid,tdevice.infrared_code,tdevice.fcode')
								->join('tdevice on tdevice.fid = tmedia.fdeviceid')
								->where(array('ir_plan'=>array('neq','')))->select();//查询需要执行计划的媒介
		$eNum = 0;//统计执行成功的数量	

		
		foreach($planMediaList as $planMedia){//循环媒介列表
			$irPlanArr = explode("\n",$planMedia['ir_plan']);//把执行计划转为数组(单个媒介的单个计划)
			
			foreach($irPlanArr as $irPlan){
				
				$ir = explode(',',$irPlan);
				
				if(count($ir) == 2){//判断执行计划是否合法
				
					$irCacheName = $planMedia['fid'].'_'.strtotime($ir[0]).'_'.$ir[1];
					
					
					if(abs(time() - strtotime($ir[0])) < 30 && !S($irCacheName)){//判断是否在计划执行窗口内，且没有执行过
						S($irCacheName,1,70);//写入缓存，标记已经执行过
						
						if($planMedia['infrared_code'] == 'new_cloud_box_2'){//判断是否新版云盒
						
							$new_box_cmd = 0;
							if($ir[1] == '3') $new_box_cmd = 13;
							if($ir[1] == '4') $new_box_cmd = 14;
							if($ir[1] == '5') $new_box_cmd = 15;
							if($ir[1] == '6') $new_box_cmd = 16;
							if($ir[1] == '7') $new_box_cmd = 17;
							$eirState = A('Common/Shilian','Model')->infrared_control($planMedia['fcode'],$planMedia['fchannelid'],$new_box_cmd);
							//file_put_contents('LOG/eirState',$planMedia['fcode'] . $planMedia['fchannelid'] . $new_box_cmd . json_encode($eirState));
						}else{
							$eirState = A('Common/Infrared','Model')->ircontrol($ir[1],$planMedia['infrared_code'],$planMedia['fchannelid']);
						
						}
						
						
						if($eirState){
							file_put_contents('LOG/ir_plan.log',date('Y-m-d H:i:s').'	'.$planMedia['fmedianame'].'	'.$ir[0].'	'.$ir[1]."\n",FILE_APPEND);
							$eNum ++;
						}
					}
				}
			}
			
			
			
			
		}
		echo $eNum;
		
	}
	
	
		//关键字搜索媒体
	public function s_media(){
		$s_media = I('s_media');
		
		$s_regionid = I('s_regionid');
		$region_id = $s_regionid;
		$this_region_id = I('this_region_id');
		$s_media_class = I('s_media_class');
		
		$medialabel = I('medialabel');//媒介标签

		
		
		
		
		$where = array();
		$where['tmedia.fstate'] = 1;
		$where['tmedia.fid'] = array('exp',' = tmedia.main_media_id');
		if($medialabel != ''){
			
			$mediaIdList = M('tmedialabel')
										->cache(true,60)
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where(array('tlabel.flabel'=>$medialabel))
										->getField('fmediaid',true);
			if($mediaIdList){
				$where['tmedia.fid'] = array('in',$mediaIdList);	
			}else{
				$where['tmedia.fid'] = 0;
			}							
								
			
			
			
			
		}
		
		
		
		//$where['tmedia.priority'] = array('egt',0);
		if($s_media){
			$where['fmedianame'] = array('like','%'.$s_media.'%');
		}
		
		if($region_id > 0){
			if($this_region_id == 'true'){
				$where['tmedia.media_region_id'] = $region_id;//地区搜索条件
			}elseif($region_id != 100000){
				$region_id_rtrim = A('Common/System','Model')->get_region_left($region_id);//地区ID去掉末尾的0
				$region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
				$where['left(tmedia.media_region_id,'.$region_id_strlen.')'] = $region_id_rtrim;//地区搜索条件
			}
		}
		if($s_media_class){
			$where['left(tmedia.fmediaclassid,2)'] = $s_media_class;
			
		}
		
		
		$mediaList = M('tmedia')
							->field('tmedia.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
							->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')//连接媒介机构
							
							->where($where)->limit(10000)->select();
		//var_dump(M('tmedia')->getLastSql());					
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaList'=>$mediaList));
		
	}
	
	
	
	/*生成媒介发布量报告，与生产系统对比*/
	public function media_issue_contrast(){
		header("Content-type: text/html; charset=utf-8"); 
		$s_date = I('s_date');
		if($s_date == ''){
			$s_date = date('Y-m-d');
		}else{
			$s_date = date('Y-m-d',strtotime($s_date));
		}
		$s_time = strtotime($s_date);
		$e_time = strtotime($s_date) + 86399;
		$aiApiUrl = C('AI_DOMAIN').'uuid/summary';
		

		$mediaWhere = array();
		$mediaWhere['left(tmedia.priority,2)'] = array('egt',0);
		$mediaWhere['fissuedate'] = $s_date;
		$mediaList = array();
		
		$mediaList01 = M('tmedia')
								->field('tmedia.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,count(tmedia.fid) as dmp_issue_count')
								->join('ttvissue on ttvissue.fmediaid = tmedia.fid')
								->group('tmedia.fid')
								->where($mediaWhere)
								->limit(50)
								->select();
		foreach($mediaList01 as $mediaKey => $media01){
			
			$ai_request_data = array();
			$ai_request_data['channel'] = $media01['fid'];
			$ai_request_data['start'] = $s_time;
			$ai_request_data['end'] = $e_time;
			$aiRet = http($aiApiUrl,$ai_request_data,'GET',10);
			$aiRet = json_decode($aiRet,true);
			$media01['ai_issue_count'] = strval(count($aiRet[0]['issueList']));
			$mediaList[] = $media01; 

		}


		$mediaList02 = M('tmedia')
								->field('tmedia.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,count(tmedia.fid) as dmp_issue_count')
								->join('tbcissue on tbcissue.fmediaid = tmedia.fid')
								->group('tmedia.fid')
								->where($mediaWhere)
								->limit(50)
								->select();
		foreach($mediaList02 as $mediaKey => $media02){
			
			$ai_request_data = array();
			$ai_request_data['channel'] = $media02['fid'];
			$ai_request_data['start'] = $s_time;
			$ai_request_data['end'] = $e_time;
			$aiRet = http($aiApiUrl,$ai_request_data,'GET',10);
			$aiRet = json_decode($aiRet,true);
			$media02['ai_issue_count'] = strval(count($aiRet[0]['issueList']));
			$mediaList[] = $media02; 

		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','mediaList'=>$mediaList));


		
		
		

	}
	
	/*计算媒介发布数量*/
	public function dmp_media_issue_count(){
		set_time_limit(240);
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		$date = I('date');//发布日期
		if($date == ''){//判断是否传递日期
		
		
			$dateInfo = A('Common/System','Model')->important_data('dmp_media_issue_count');//获取处理过的日期
			$dateInfo = json_decode($dateInfo,true);//转为数组

			
			
			if(!$dateInfo){//如果是空的
				$date = date('Y-m-d',time() - 86400 * 90);
			}else{
				$date = date('Y-m-d',strtotime($dateInfo['issue_date']) + 86400);
			}
			A('Common/System','Model')->important_data('dmp_media_issue_count',json_encode(array('issue_date'=>$date)));//写入新的处理过的日期
			if(strtotime($date) >= strtotime(date('Y-m-d'))){
				A('Common/System','Model')->important_data('dmp_media_issue_count','{}');
				exit;
			}
			$a = A('Api/Media','Model')->dmp_media_issue_count($date);
			
			
			
			
		}else{
			$date = date('Y-m-d',strtotime($date));
			$a = A('Api/Media','Model')->dmp_media_issue_count($date);
			
			
			
		}

	}
	
	/*计算媒介发布数量*/
//	public function tbn_ad_summary_day(){
//		ini_set('memory_limit','512M');
//		set_time_limit(240);
//		header("Content-type: text/html; charset=utf-8");
//		//exit('暂停使用');
//		if(I('auto') == '1'){
//			echo '<meta http-equiv="refresh"content="0.1"/>';
//		}
//		$date = I('date');//发布日期
//
//		$dateInfo = A('Common/System','Model')->important_data('tbn_ad_summary_day');//获取处理过的日期
//		$dateInfo = json_decode($dateInfo,true);//转为数组
//
//
//
//		if(!$dateInfo){//如果是空的
//			$date = date('Y-m-d',time() - 86400 * 90);
//		}else{
//			$date = date('Y-m-d',strtotime($dateInfo['issue_date']) + 86400);
//		}
//		A('Common/System','Model')->important_data('tbn_ad_summary_day',json_encode(array('issue_date'=>$date)));//写入新的处理过的日期
//		if(strtotime($date) >= strtotime(date('Y-m-d'))){
//			A('Common/System','Model')->important_data('tbn_ad_summary_day','{}');
//			exit;
//		}
//		$a = A('Api/Media','Model')->tbn_ad_summary_day($date);
//
//	}


    public function tbn_ad_summary_day()
    {
        ini_set('memory_limit','512M');
        set_time_limit(240);
        header("Content-type: text/html; charset=utf-8");
        //exit('暂停使用');
        if(I('auto') == '1'){
            echo '<meta http-equiv="refresh"content="0.1"/>';
        }
		
		for($i=0;$i<100;$i++){
			
			$dateMediaInfo = A('Common/System','Model')->important_data('tbn_ad_summary_day');//获取处理过的日期
			$dateMediaInfo = json_decode($dateMediaInfo, true);
			if(!$dateMediaInfo['issue_date']){//如果是空的
				$dateMediaInfo['issue_date'] = date('Y-m-d',time() - 86400 * 30);
				$dateMediaInfo['media_id'] = A('Api/Media','Model')->getNextMediaId();
			}else{
				if (!$dateMediaInfo['media_id'] || $dateMediaInfo['media_id'] == 0){
					$dateMediaInfo['media_id'] = A('Api/Media','Model')->getNextMediaId();
					$dateMediaInfo['issue_date'] = date('Y-m-d',strtotime($dateMediaInfo['issue_date']) + 86400);
				}else{
					$dateMediaInfo['media_id'] = A('Api/Media','Model')->getNextMediaId($dateMediaInfo['media_id']);
				}
			}
			A('Common/System','Model')->important_data('tbn_ad_summary_day',json_encode($dateMediaInfo));//写入新的处理过的日期
			if(strtotime($dateMediaInfo['issue_date']) >= strtotime(date('Y-m-d'))){
				A('Common/System','Model')->important_data('tbn_ad_summary_day','{}');
				exit;
			}
			echo $dateMediaInfo['issue_date'] . '	' . $dateMediaInfo['media_id']."\n";
			$this->tbn_ad_summary_day_2($dateMediaInfo['issue_date'], $dateMediaInfo['media_id']);

		}
		
        
		
		
	}


	
	
	/*计算媒介发布数量*/
	public function tbn_ad_summary_day_2($date=null,$media_id=0){
		//var_dump(12313);
		if(!$date || !$media_id){
			return false;
		}
		set_time_limit(240);
		session_write_close();
		
		
		
		$mediaInfo = M('tmedia')
								->cache(true,600)
								->field('tmedia.fid,left(tmedia.fmediaclassid,2) as media_class,tmedia.media_region_id fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>$media_id))
								->find();
		
		if($mediaInfo['media_class'] == '01'){
			$table_name = 'ttvissue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2); 
			try{
				$dmp_ad_class = M($table_name)//按广告分类查询
									->alias('ttvissue')
									->field('
											ttvissue.fmediaid,
											left(tad.fadclasscode,2) as fad_class_code,
											count(*) as fad_times,
											ttvsample.fillegaltypecode,
											sum(case when ttvsample.is_long_ad = 0 then case when ttvissue.flength < 0 then ttvissue.flength + 86400 else ttvissue.flength end else 0 end) as fad_play_len,
											count(case when ttvsample.fillegaltypecode > 0 then 1 else null end) fad_illegal_times,
											
											concat(ttvsample.fid,"_01") as sam_id,
											sum(case when ttvissue.flength < 0 then ttvissue.flength + 86400 else ttvissue.flength end) as in_long_ad_play_len
											')
									->join('ttvsample on ttvsample.fid = ttvissue.fsampleid')
									->join('tad on tad.fadid = ttvsample.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> "23"')
									->where(array('ttvissue.fmediaid'=>$media_id,'ttvissue.fissuedate'=>strtotime($date)))
									->group('left(tad.fadclasscode,2),ttvsample.fid')
									->select();				
					
					
			}catch( \Exception $error) {
				$dmp_ad_class = array();	
			}
		}elseif($mediaInfo['media_class'] == '02'){
			
			$table_name = 'tbcissue_'.date('Ym',strtotime($date)).'_'.substr($mediaInfo['fregionid'],0,2); 
			
			try{
				$dmp_ad_class = M($table_name)//按广告分类查询
									->alias('tbcissue')
									->field('
											tbcissue.fmediaid,
											left(tad.fadclasscode,2) as fad_class_code,
											count(*) as fad_times,
											tbcsample.fillegaltypecode,
											sum(case when tbcsample.is_long_ad = 0 then case when tbcissue.flength < 0 then tbcissue.flength + 86400 else tbcissue.flength end else 0 end) as fad_play_len,
											count(case when tbcsample.fillegaltypecode > 0 then 1 else null end) fad_illegal_times,
											
											concat(tbcsample.fid,"_02") as sam_id,
											sum(case when tbcissue.flength < 0 then tbcissue.flength + 86400 else tbcissue.flength end) as in_long_ad_play_len
											')
									->join('tbcsample on tbcsample.fid = tbcissue.fsampleid')
									->join('tad on tad.fadid = tbcsample.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> "23"')
									->where(array('tbcissue.fmediaid'=>$media_id,'tbcissue.fissuedate'=>strtotime($date)))
									->group('left(tad.fadclasscode,2),tbcsample.fid')
									->select();				
					
					
			}catch( \Exception $error) {
				$dmp_ad_class = array();	
			}

		}elseif($mediaInfo['media_class'] == '03'  || $mediaInfo['media_class'] == '04'){
			$dmp_ad_class = M('tpaperissue')//按广告分类查询
							
							->field('
									tpaperissue.fmediaid,
									left(tad.fadclasscode,2) as fad_class_code,
									count(*) as fad_times,
									tpapersample.fillegaltypecode,
									0 as fad_play_len,
									count(case when tpapersample.fillegaltypecode > 0 then 1 else null end) fad_illegal_times,
									0 as fad_illegal_play_len,
									concat(tpaperissue.fpapersampleid,"_03") as sam_id,
									0 as in_long_ad_play_len,
									0 as in_long_ad_illegal_play_len
									')
							->join('tpapersample on tpapersample.fpapersampleid = tpaperissue.fpapersampleid')
							->join('tad on tad.fadid = tpapersample.fadid and tad.fadid<>0 and left(tad.fadclasscode,2) <> "23"')
							->where(array('tpaperissue.fmediaid'=>$media_id,'tpaperissue.fissuedate'=>$date))
							->group('tpaperissue.fmediaid,left(tad.fadclasscode,2),tpaperissue.fpapersampleid')
							->select();
		}	
		
		
		
		
		
		
		
		
		$dmp_ad_class2 = array();//广告数据按广告分类整理，初始化
		foreach($dmp_ad_class as $dmp_ad){
			$dmp_ad_class2[$dmp_ad['fad_class_code']][] = $dmp_ad;//按广告分类整理数据
		}
		$class_ad_arr = array();
		foreach($dmp_ad_class2 as $fad_class_code => $dmp_ad2){//按广告分类循环数据
			$adData0 = array();
			$adData0['fad_times'] = 0;
			$adData0['fad_illegal_times'] = intval($illAdData[$mediaid_adclass0]['fad_illegal_times']);
			$adData0['fad_play_len'] = 0;
			$adData0['fad_illegal_play_len'] = intval($illAdData[$mediaid_adclass0]['fad_illegal_play_len']);
			$adData0['in_long_ad_play_len'] = 0;
			
			$fad_sam_list = array();
			$fad_sam_illegal_list = array();
			
			
					
			foreach($dmp_ad2 as $key => $adData){
				$adData0['fad_times'] += $adData['fad_times'];
				
				if($adData['fad_play_len'] >= 0){
					$adData0['fad_play_len'] += $adData['fad_play_len'];
				}else{
					$adData0['fad_play_len'] += 0;
				}
				
				if($adData['in_long_ad_play_len'] >= 0){
					$adData0['in_long_ad_play_len'] += $adData['in_long_ad_play_len'];
				}else{
					$adData0['in_long_ad_play_len'] += 0;
				}
				
				
				
				$fad_sam_list[] = $adData['sam_id'];
				
				
				if($adData['fillegaltypecode'] > 0){
					$fad_sam_illegal_list = $adData['sam_id'];
				}
										
				
			}
			$adData0['fad_sam_list'] = implode(',',$fad_sam_list);
			$adData0['fad_sam_illegal_list'] = implode(',',$fad_sam_illegal_list);
			A('Api/Media','Model')->tbn_ad_summary_day_do($media_id,$date,$adData['fad_class_code'],array(
																									'fad_times'=>$adData0['fad_times'],
																									'fad_illegal_times'=>$adData0['fad_illegal_times'],
																									'fad_play_len'=>$adData0['fad_play_len'],
																									
																									'fad_sam_list'=>$adData0['fad_sam_list'],
																									
																									'in_long_ad_play_len' => $adData0['in_long_ad_play_len'],
																									
																								));
			$class_ad_arr[] = $adData['fad_class_code'];//赋值本次执行的分类数据		

			
		}
		
		//$class_ad_arr = array();
		if(count($class_ad_arr) == 0){
			$del_state = M('tbn_ad_summary_day')->where(array('fmediaid'=>$media_id,'fdate'=>$date))->delete();
		}else{
			$del_state = M('tbn_ad_summary_day')->where(array('fmediaid'=>$media_id,'fdate'=>$date,'fad_class_code'=>array('not in',$class_ad_arr)))->delete();
		}
		
		
		

	}







    /**
     * 根据标签获取媒体数量
     * @param $label    string      标签名称
     * @param $class    string      媒体类别, 只允许为 电视, 广播, 报纸
     */
    public function getMediaListByLabel($label, $class)
    {
        $where = $this->processWhereByLabel($label, $class);
        $page = I('page', 1);
        $total = M('tmedia')
            ->where($where)
            ->count();
        $data = M('tmedia')
            ->field('fid id, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as name')
            ->where($where)
            ->page($page, 20)
            ->select();
        $this->ajaxReturn([
            'data' => $data,
            'total' => $total,
            'where' => $where
        ]);
    }

    /**
     * 根据标签获取符合条件媒体所有id
     * @param $label    string      标签名称
     * @param $class    string      媒体类别, 只允许为 电视, 广播, 报纸
     */
    public function getMediaIdsByLabel($label, $class)
    {
        $where = $this->processWhereByLabel($label, $class);
        $data = M('tmedia')->where($where)->getField('fid', true);
        $this->ajaxReturn([
            'data' => $data
        ]);
    }

    /**
     * 处理搜索条件
     * @param $label        string      标签名称
     * @param $class    string      媒体类别, 只允许为 电视, 广播, 报纸
     * @return array    处理后的媒体表搜索条件
     */
    private function processWhereByLabel($label, $class)
    {
        // tlabel的子查询
        $subQuery = M('tlabel')
            ->field('flabelid')
            ->where([
                'flabelclass' => ['EQ', $class],
                'flabel' => ['EQ', $label]
            ])
            ->buildSql();
        // tmedialabel的子查询
        $subQuery = M('tmedialabel')
            ->field('fmediaid')
            ->where([
                'flabelid' => ['EXP', 'IN '.$subQuery]
            ])
            ->buildSql();
        // tmedia的搜索结果

        $where = [
            'fid' => ['EXP', 'IN '.$subQuery]
        ];
        if (I('name') != ''){
            $where['fmedianame'] = ['LIKE', '%'.I('name').'%'];
        }
        // 查看已确认媒体后者是已经DMP确认后的
        if (I('fids') != '' || I('isZj') != ''){
            $ids = trim(I('fids'), ',');
            if (I('isZj') == 1){
                $ids = trim(I('fids') .','. I('fids2'), ',');
                $where['fid'] = ['IN', $ids];
            }else{
                $where['fid'] = [
                    ['EXP', 'IN '.$subQuery],
                    ['IN', $ids],
                ];
            }

        }
        if (I('regionId') != ''){
            $mediaOwnerWhere = [];
            if (I('onlyThisLevel') != 1){
                $region_id_rtrim = A('Common/System','Model')->get_region_left(I('regionId'));//地区ID去掉末尾的0
                $region_id_strlen = strlen($region_id_rtrim);//去掉0后还有几位
                $mediaOwnerWhere["LEFT(fregionid, $region_id_strlen)"] = ['EQ', $region_id_rtrim];
            }else{
                $mediaOwnerWhere['fregionid'] = ['LIKE', '%'.I('regionId').'%'];
            }
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $where['fmediaownerid'] = ['EXP', 'IN '. $mediaOwnerSubQuery];//地区搜索条件
        }
        return $where;
    }

    /**
     * 通用的媒体搜索
     */
    public function searchMedia()
    {
        $form = I('where');
        $page = I('page',1);
        $pageSize = I('pageSize', 40);
        $where = [];
        $mediaOwnerWhere = [];
        if($form['region'] != ''){
            if ($form['onlyThisLevel'] == 1){
                $mediaOwnerWhere["fregionid"] = ['EQ', $form['region']];
            }else{
                $form['region'] = rtrim($form['region'], '0');
                $len = strlen($form['region']);
                $mediaOwnerWhere["LEFT(fregionid, $len)"] = ['EQ', $form['region']];
            }
        }
        if (count($mediaOwnerWhere) != 0){
            $mediaOwnerSubQuery = M('tmediaowner')
                ->field('fid')
                ->where($mediaOwnerWhere)
                ->buildSql();
            $where['fmediaownerid'] = ['EXP', 'IN '.$mediaOwnerSubQuery];
        }
        if ($form['mediaClass'] != ''){
            $len = strlen($form['mediaClass']);
            $where["LEFT(fmediaclassid, $len)"] = ['EQ', $form['mediaClass']];
        }
        if ($form['mediaName'] != ''){
            $where["fmedianame"] = ['LIKE', '%'.$form['mediaName'].'%'];
        }
        if ($form['mediaLabel'] != ''){
            $where["tmedia.fid"] = ['EXP', "IN (select fmediaid from tmedialabel where flabelid = {$form['mediaLabel']})"];
        }
        $list = M('tmedia')
            ->field('fid value, (case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as label')
            ->where($where)
            ->page($page, $pageSize)
            ->select();
        $total = M('tmedia')->where($where)->count();
        $this->ajaxReturn([
            'code' => 0,
            'data' => $list,
            'total' => $total,
            'where' => $where
        ]);
    }

    /**
     * 修改媒体发布频率
     * @param $mediaId
     * @param $frequency
     */
    public function editFrequency($mediaId, $frequency)
    {
        if (!$mediaId || !$frequency){
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '参数缺失'
            ]);
        }
        $where = [
            'fid' => ['EQ', $mediaId],
        ];
        $data = [
            'frequency' => json_encode($frequency)
        ];
        $res = M('tmedia')
            ->where($where)
            ->save($data);
        if ($res){
            $this->ajaxReturn([
                'code' => 0,
                'msg' => '修改成功'
            ]);
        }else{
            $this->ajaxReturn([
                'code' => -1,
                'msg' => '修改失败'
            ]);
        }
    }
	
	
	/*计算电视发布记录价格*/
	public function tv_price(){
		exit;
		session_write_close();
		set_time_limit(6000);
		header("Content-type: text/html; charset=utf-8");
		//exit('暂停使用');
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		
		$mod = intval(I('mod'));
		
		$price_media = M('ttvprice')->cache(true,600)->group('fmediaid')->getField('fmediaid',true);
		
		$issueDate = S('tv_price_issueDate');
		if(!$issueDate){
			$issueDate = '2018-01-01';
			S('tv_price_issueDate',$issueDate,86400);
		}
		
		$issueList = M('ttvissue')
								->field('ftvissueid,famounts,fmediaid,fstarttime,fendtime')
								->where(array('fissuedate'=>$issueDate,'fmediaid'=>array('in',$price_media),'famounts'=>0 /* ,'_string'=>'(ftvissueid mod 10) = '.$mod */ ))
								->limit(1000)
								->select();
		echo M('ttvissue')->getLastSql()."<br />\n";					
		if(count($issueList) == 0){
			S('tv_price_issueDate',date('Y-m-d',strtotime($issueDate)+86400),86400);
		}
		foreach($issueList as $issue){
			$price = intval(A('Common/Media','Model')->ad_price($issue['fmediaid'],strtotime($issue['fstarttime']),strtotime($issue['fendtime'])));
			if($price <= 0) $price = -1;
			//echo $issue['ftvissueid'] . '	' . $issue['fmediaid'].'	'.$issue['fstarttime'].'	'. $issue['fendtime'] . '	'.(strtotime($issue['fendtime']) - strtotime($issue['fstarttime'])) .'	'.$price."<br />\n";
			M('ttvissue')->where(array('ftvissueid'=>$issue['ftvissueid']))->save(array('famounts'=>$price));
			echo M('ttvissue')->getLastSql()."<br />\n";
		}
		echo count($issueList)."<br />\n";
		echo "<br />\n";
		echo date('H:i:s');
		
	}


    public function getMediaNameByMediaId($id)
    {
        $name = M('tmedia')->where([
            'fid' => ['EQ', $id]
        ])->getField('fmedianame');
        $this->ajaxReturn([
            'name' => $name
        ]);
	}

    public function searchMediaByName_V20190912($name = '')
    {
        $where = [
            'fmedianame' => ['LIKE', '%' . $name . '%'],
        ];
		$res = M('tmedia')->field('(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as value,fid')->where($where)->limit(10)->select();
		$sql = M('tmedia')->getLastSql();
        $this->ajaxReturn([
            'code' => 0,
            'data' => $res,
        ]);
	}

	/**
     * @Des: 按名称搜索媒体列表
     * @Edt: yuhou.wang
     * @param {String} $name 媒体名称
     * @param {Int} $limit 结果数量限制
     * @return: {Json} 
     * @Date: 2019-09-12 16:44:09
     */
    public function searchMediaByName($name = '',$limit = 20){
		$where = [
			'_string' => 'main_media_id = fid',
			'fstate'  => 1,
		];
		$medianameArr[] = ['NOTLIKE','测试%'];
		if(!empty($name)){
			$medianameArr[] = ['LIKE', '%'.$name.'%'];
			$medianameArr[] = 'AND';
		}
		$where['fmedianame'] = $medianameArr;
		$res = M('tmedia')->field('(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as value,fid')->where($where)->order('fmediaclassid,fmedianame')->limit($limit)->select();
		$sql = M('tmedia')->getLastSql();
        $this->ajaxReturn(['code' => 0,'data' => $res]);
	}

	/*汇总统计发布记录数量*/
	public function count_media_issue0(){
		set_time_limit(600);
		$mediaIdList = M('tmedialabel')
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where(array('tmedialabel.fstate'=>1,'flabel'=>array('not in',array('无法维护','食药局','快剪'))))
										->group('fmediaid')
										
										->getField('fmediaid',true);
		
		$mediaList = M('tmedia')
								->cache(true,600)
								->field('left(tmedia.fmediaclassid,2) as media_class,tmedia.fid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,tmedia.media_region_id fregionid')
								->join('tmediaowner on tmediaowner.fid = tmedia.fmediaownerid')
								->where(array('tmedia.fid'=>array('in',$mediaIdList),'tmedia.priority'=>array('egt',0),'left(tmedia.fmediaclassid,2)'=>array('in','01,02')))
								->select();
					
		$yesList = array();
		$noList = array();
		
		foreach($mediaList as $media){
			
			$medialabelList = M('tmedialabel')
										->cache(true,600)
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where(array('fmediaid'=>$media['fid']))
										->getField('flabel',true);
			//var_dump(M('tmedialabel')->getLastSql());							
			if(in_array('快剪',$medialabelList)){
				//var_dump($medialabelList);							
				continue;
			}							
										
			if($media['media_class'] == '01'){
				$table_name = 'ttvissue_'. date('Ym') .'_'. substr($media['fregionid'],0,2) ;
				try{
					$issueCount = M($table_name)->where(array('fissuedate'=>strtotime(date('Y-m-d')),'fmediaid'=>$media['fid']))->count();
				}catch( \Exception $e) { //
					
				} 
				
				
				if($issueCount > 0){ 
					$yesList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}else{
					$noList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}
				
			}elseif($media['media_class'] == '02'){
				$table_name = 'tbcissue_'. date('Ym') .'_'. substr($media['fregionid'],0,2) ;
				
				try{
					$issueCount = M($table_name)->where(array('fissuedate'=>strtotime(date('Y-m-d')),'fmediaid'=>$media['fid']))->count();
				}catch( \Exception $e) { //
					
				} 
				if($issueCount > 0){ 
					$yesList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}else{
					$noList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}
			}
		}
		
		
		$msgPost = array();
		$msgPost['msgtype'] = 'text';
		
		$msgPost['text']['content'] = '截止'.date('Y-m-d H:i:s').'	共分析'.count($yesList).'个媒介 其中'.count($noList).'个媒介无今日发布记录数据'."\n 点击查看:".U('Api/Media/count_media_issue2@'.$_SERVER['HTTP_HOST']);
		$msgPost['at']['atMobiles'] = array('15867123863');
		if((count($yesList) / (count($noList) + count($yesList))) < 0.6){
			A('Common/Alitongxin','Model')->alarm('13588258695','【DMP数据生产】',date('H:i:s'),'【大量媒体无发布记录】');
		}
		
		$ret = http('https://oapi.dingtalk.com/robot/send?access_token=af009e8cded4f5bcf16714def3e1888120d8b0a426dbee9dfd94afe5fb357163',json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
		
		S('todayNoIssueList',$noList,3600*24);
			
	}
	
	
	
	/*汇总统计发布记录数量*/
	public function count_media_issue(){
		set_time_limit(600);
		
		
		$tvQueue = M('ttvissue')->where(array('ftvmodelid'=>0))->count();
		$bcQueue = M('tbcissue')->where(array('fbcmodelid'=>0))->count();
		$queueCount = $tvQueue + $bcQueue;
		
		if($queueCount > 200){ 
			$msgPost = array();
			$msgPost['msgtype'] = 'text';
			
			$msgPost['text']['content'] = '发布记录同步队列达到'.$queueCount.'条,请处理';

			$msgPost['at']['atMobiles'] = array('13588258695');

			$ret = http('https://oapi.dingtalk.com/robot/send?access_token=af009e8cded4f5bcf16714def3e1888120d8b0a426dbee9dfd94afe5fb357163',json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
			
			
			A('Common/Alitongxin','Model')->alarm('13588258695','【DMP发布记录同步】',date('H:i:s'),'【未同步队列达到'.$queueCount.'条】');
			
			
			exit;
		}
		
		
		
		$media0List = http('http://47.96.182.117/index/exceitionStatusList3?page=1&pageSize=10000&is_enable=1&need=1',array(),'GET',false,30);
		
		$media0List = json_decode($media0List,true);
		
		$mediaList = $media0List['data'];
		
		if(!$mediaList){
			$msgPost = array();
			$msgPost['msgtype'] = 'text';
			
			$msgPost['text']['content'] = '查询生产媒体接口出错';

			$msgPost['at']['atMobiles'] = array('13588258695','18511418185');

			$ret = http('https://oapi.dingtalk.com/robot/send?access_token=af009e8cded4f5bcf16714def3e1888120d8b0a426dbee9dfd94afe5fb357163',json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
			exit;
		}
		
		
					
		$yesList = array();
		$noList = array();
		$errorList = array();
		$filterList = array();
		foreach($mediaList as $media){
			
			$mediaInfo = M('tmedia')
									
									->cache(true,600)
									->field('media_region_id,left(fmediaclassid,2) as media_class')
									->where(array('fid'=>$media['fid']))
									->find();
			$medialabelList = M('tmedialabel')
										->cache(true,600)
										->join('tlabel on tlabel.flabelid = tmedialabel.flabelid')
										->where(array('fmediaid'=>$media['fid']))
										->getField('flabel',true);	

			if(in_array('快剪',$medialabelList)){
				$filterList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);							
				continue;
			}
										
			$media['media_class'] = $mediaInfo['media_class'];
			$media['fregionid'] = $mediaInfo['media_region_id'];
					
										
			if($media['media_class'] == '01'){
				$table_name = 'ttvissue_'. date('Ym') .'_'. substr($media['fregionid'],0,2) ;
				try{
					$issueCount = M($table_name)->where(array('fissuedate'=>strtotime(date('Y-m-d')),'fmediaid'=>$media['fid']))->count();
				}catch( \Exception $e) { //
					
				} 
				
				
				if($issueCount > 0){ 
					$yesList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}else{
					$gap_perc = json_decode(http('http://47.96.182.117:8002/vs_gap_summary/'.$media['fid'].'/'.date('Y-m-d').'/'.date('Y-m-d').'/'),true)['data'][0]['gap_perc'];
					$noList[] = array('gap_perc'=>$gap_perc,'medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}
				
			}elseif($media['media_class'] == '02'){
				$table_name = 'tbcissue_'. date('Ym') .'_'. substr($media['fregionid'],0,2) ;
				
				try{
					$issueCount = M($table_name)->where(array('fissuedate'=>strtotime(date('Y-m-d')),'fmediaid'=>$media['fid']))->count();
				}catch( \Exception $e) { //
					
				} 
				if($issueCount > 0){ 
					$yesList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}else{
					$gap_perc = json_decode(http('http://47.96.182.117:8002/vs_gap_summary/'.$media['fid'].'/'.date('Y-m-d').'/'.date('Y-m-d').'/'),true)['data'][0]['gap_perc'];
					$noList[] = array('gap_perc'=>$gap_perc,'medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
				}
			}else{
				$errorList[] = array('medialabelList'=>$medialabelList,'mediaid'=>$media['fid'],'medianame'=>$media['fmedianame'],'count'=>$issueCount);
			}
		}
		
		
		$msgPost = array();
		$msgPost['msgtype'] = 'text';
		
		$msgPost['text']['content'] = '截止'.date('Y-m-d H:i:s').'	'."\n";
		$msgPost['text']['content'] .= '共分析'.count($mediaList).'个媒介	'."\n";
		$msgPost['text']['content'] .= '其中	'."\n";
		
		
		$msgPost['text']['content'] .= '过滤了'.count($filterList).'个媒介	'."\n";
		$msgPost['text']['content'] .= count($yesList).'个媒介有今日发布记录数据	'."\n";
		
		$msgPost['text']['content'] .= count($noList).'个媒介无今日发布记录数据	'."\n";

		$msgPost['text']['content'] .= count($errorList).'个媒介在DMP数据库无法找到	'."\n";
		
		$msgPost['text']['content'] .= '点击查看:'.U('Api/Media/count_media_issue2@'.$_SERVER['HTTP_HOST'])."\n";
		
		
		
		
		$msgPost['at']['atMobiles'] = array('15867123863');
		if((count($yesList) / (count($noList) + count($yesList))) < 0.6){
			A('Common/Alitongxin','Model')->alarm('13588258695','【DMP数据生产】',date('H:i:s'),'【大量媒体无发布记录】');
		}
		
		$ret = http('https://oapi.dingtalk.com/robot/send?access_token=af009e8cded4f5bcf16714def3e1888120d8b0a426dbee9dfd94afe5fb357163',json_encode($msgPost),'POST',array('Content-Type:application/json; charset=utf-8'));
		
		S('todayNoIssueList',$noList,3600*24);
		S('todayErrorMedia',$errorList,3600*24);
			
	}
	
	
	/*查询流完整率*/
	public function gap_perc (){
		session_write_close();
		$mediaId = I('mediaId');
		
		$gap_perc = json_decode(http('http://47.96.182.117:8002/vs_gap_summary/'.$mediaId.'/'.date('Y-m-d',time() - 86400).'/'.date('Y-m-d').'/'),true);
		$today  = 0;
		$yesterday  = 0;
		
		foreach($gap_perc['data'] as $data){
			if(date('Y-m-d',strtotime($data['date'])) == date('Y-m-d')) $today = floor((100 - floatval($data['gap_perc'])) * 100) / 100;
			if(date('Y-m-d',strtotime($data['date'])) == date('Y-m-d',time()-86400)) $yesterday = floor((100 - floatval($data['gap_perc'])) * 100) / 100;
		}
		

		$this->ajaxReturn(array('code'=>0,'msg'=>'','today'=>$today,'yesterday'=>$yesterday));
	}
	
	/*查看最后发布时间*/
	
	public function last_issue_time(){
		session_write_close();
		$mediaId = I('mediaId');
		
		$mediaInfo = M('tmedia')->cache(true,600)->field('fid,left(fmediaclassid,2) as media_class,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame,media_region_id')->where(array('fid'=>$mediaId))->find();
		
		if($mediaInfo['media_class'] == '01'){
			$tab = 'tv';
		}elseif($mediaInfo['media_class'] == '02'){
			$tab = 'bc';
		}else{
			$this->ajaxReturn(array('code'=>-1,'msg'=>'mediaId error'));
		}
		
		$last_issue_time = 0;
		$get_num = 0;
		while(!$last_issue_time && $get_num < 4){
			
			$mm = date('Ym',strtotime('-'.$get_num.' months',time()));
			$issue_table = 't'.$tab.'issue_'.$mm.'_'.substr($mediaInfo['media_region_id'],0,2);

			
			try{
				$last_issue_time = M($issue_table)
											->where(array('fmediaid'=>$mediaId))
											->order('fstarttime desc')
											->getField('fstarttime');
																						
			}catch( \Exception $e) { //
				
			} 
			$get_num ++;
		}
		
		$this->ajaxReturn(array('code'=>0,'msg'=>'','time_to_time'=>time_to_time($last_issue_time),'last_issue'=>date('Y-m-d H:i:s',$last_issue_time),'last_issue_time'=>$last_issue_time));

		
		
		
	}
	
	
	
	
	
	/*查看有发布记录问题的媒体*/
	public function count_media_issue2(){
		header("Content-type: text/html; charset=utf-8"); 
		
		foreach(S('todayNoIssueList') as $todayNoIssue){
			//echo $todayNoIssue['mediaid'].','.$todayNoIssue['medianame'].','. (implode('|',$todayNoIssue['medialabelList'])) .',断流率:'.$todayNoIssue['gap_perc'].'%'."<br />\n";
		}
		//echo '以下是找不到对应的媒体'."<br />\n";
		foreach(S('todayErrorMedia') as $todayErrorMedia){
			//echo $todayErrorMedia['mediaid'].','.$todayErrorMedia['medianame'].',' ."<br />\n";
		}
		$this->assign('todayNoIssueList',list_k(S('todayNoIssueList')));
		$this->display();
	}
	
	
	/*同步自动巡检信息*/
	public function syn_auto_inspection(){
		
		set_time_limit(600);
		// 1没有检测到台标；0有台标；-1没流；-2模型异常
		$db_link = 'mysql://root:Aadmin18003435512@192.168.0.152:3306/channelcheck';
		$remoteUrl = 'http://172.16.198.126/index/laborThrowAbnormal';
		$dataList = M('tvlogomoni','',$db_link)->field('sid,title,type,start,update_time')->select();//查询所有频道异常
		$type = array('0'=>'正常','1'=>'未检出台标','-2'=>'识别模型异常','-1'=>'断流');
		foreach($dataList as $data){//循环查询到的数据
			
			
			if($data['type'] == '0'){
				$now_collect_state = 1;//采集异常状态
			}elseif($data['type'] == '1' || $data['type'] == '-1'|| $data['type'] == '-2'){
				$now_collect_state = 0;//采集异常状态
			}	
			
			
			$mediaInfo = M('tmedia')->cache(true,600)->field('fid,now_collect_state,auto_inspection,priority')->where(array('fid'=>$data['sid']))->find();
			
			$collect_abnormal_remark = $type[$data['type']] . '_' . $data['update_time'];//采集异常状态
			
			//if(in_array($mediaInfo['now_collect_state'],array('-1','0'))) continue;//当前媒体为 确定异常 状态时不需要处理
			
			if($mediaInfo['auto_inspection'] != '1') continue;//当前媒体状态不是需要自动巡检时不处理
			

			$e_state = M('tmedia')
						->where(array('fid'=>$data['sid']))
						->save(array(
										'now_collect_state'=>$now_collect_state,//1没有检测到台标；0有台标；-1没流；-2模型异常
										'collect_abnormal_remark'=>$collect_abnormal_remark,//异常说明
									));//修改采集状态
									
									
			if($now_collect_state !== 0) continue;
			if(intval($mediaInfo['priority']) < -1) continue;
			if((A('Common/Media','Model')->na_time($mediaInfo['fid'],$data['start'])) > 0){
				file_put_contents('LOG/MediaNaTime',date('Y-m-d H:i:s').'	'.$mediaInfo['fid'].'	'.$data['start'],FILE_APPEND);
				continue;
			}
			$check_media_abnormal_2 = M('media_abnormal_2')
															->where(array(
																			'fmediaid'=>$mediaInfo['fid'],
																			'start_time'=>$data['start'],
																		))
															->count();//查询有没有相同时间的异常记录，用于排重
			if(	$check_media_abnormal_2 > 0) continue;	//如果有相同时间的异常记录时不处理
			$abnormal_type_code = 99;
			if($data['type'] == 1) $abnormal_type_code = 1;//串台
			if($data['type'] == -1) $abnormal_type_code = 2;//断流
			if($abnormal_type_code == 99) continue;
			try{//
				$media_abnormal_id = M('media_abnormal_2')->add(array(
																
															  'fmediaid'=>$mediaInfo['fid'],
															  'start_time'=>$data['start'],
															  'end_time'=>$data['start'] + 30,
															  'create_time'=>time(),
															  'update_time'=>time(),
															  'fstate'=>1,
															  'abnormal_remark'=>$type[$data['type']],
															  'abnormal_type_code'=>$abnormal_type_code,
															  
											
												));//添加异常记录
				$abnormal = 6;
				if($abnormal_type_code == 1) $abnormal = 5;//串台
				if($abnormal_type_code == 2) $abnormal = 7;//断流
				$postData = array();
				$postData['channel'] = $mediaInfo['fid'];
				$postData['start'] = $data['start'];
				$postData['end'] = $data['start'] + 30;
				$postData['submit_user'] = 'DMP';
				$postData['abnormal'] = $abnormal;//‘1’=>’欠费停机、未授权、信号异常’,‘2’=>’黑屏、画面异常’,‘3’=>’无声音’,‘4’=>’音画不同步’,‘5’=>’串台’,‘6’=>’其他’,‘7’=>’断流’,‘9’=>’噪音’|
				$postData['intro'] = $mediaInfo['fid'];
				$postData['user_type'] = 5;
				
				$ret = http($remoteUrl,$postData,'POST');
				$rr = json_decode($ret,true);
				if($rr['id']){
					M('media_abnormal_2')
									->where(array('fid'=>$media_abnormal_id))
									->save(array('syn_ret'=>$ret,'syn_state'=>1,'remote_id'=>$rr['id'],'syn_time'=>time()));
				}
			}catch( \Exception $e) { //
				
			} 
			
												
			
														
			
			
			
		}
		
		
	}
	
	/*同步异常数据给王文权*/
	public function syn_media_abnormal_2_ai(){
		if(I('auto') == '1'){
			echo '<meta http-equiv="refresh"content="0.1"/>';
		}
		set_time_limit(600);
		$media_abnormal_2_list = M('media_abnormal_2')
												->where(array('syn_state'=>0,'start_time'=>array('gt',time()-7200)))
												->limit(60)
												->select();
		
		$remoteUrl = 'http://172.16.198.126/index/laborThrowAbnormal';
		
		foreach($media_abnormal_2_list as $media_abnormal_2){
			
			$msectime = msectime();
			$abnormal = 6;
			if($media_abnormal_2['abnormal_type_code'] == 1) $abnormal = 5;//串台
			if($media_abnormal_2['abnormal_type_code'] == 2) $abnormal = 7;//断流
			$postData = array();
			$postData['channel'] = $media_abnormal_2['fmediaid'];
			$postData['start'] = $media_abnormal_2['start_time'];
			$postData['end'] = $media_abnormal_2['end_time'];
			$postData['submit_user'] = 'DMP';
			$postData['abnormal'] = $abnormal;//‘1’=>’欠费停机、未授权、信号异常’,‘2’=>’黑屏、画面异常’,‘3’=>’无声音’,‘4’=>’音画不同步’,‘5’=>’串台’,‘6’=>’其他’,‘7’=>’断流’,‘9’=>’噪音’|
			$postData['intro'] = $media_abnormal_2['fmediaid'];
			$postData['user_type'] = 5;
			
			$ret = http($remoteUrl,$postData,'POST');
			//var_dump($rr);
			echo msectime() - $msectime;
			echo "\n";
			$rr = json_decode($ret,true);
			if($rr['id']){
				M('media_abnormal_2')
									->where(array('fid'=>$media_abnormal_2['fid']))
									->save(array('syn_ret'=>$ret,'syn_state'=>1,'remote_id'=>$rr['id'],'syn_time'=>time()));
			}
		}
		
		
	}
	
	
	public function available_time(){
		$rr = A('Common/Media','Model')->available_time(I('mediaId'));
		var_dump($rr);
	}

	/**
	 * 创建生成素材任务
	 * by zw
	 */
	public function add_source_make($getData = [],$retType = 0){
		session_write_close();
		if(empty($getData)){
			$getData = $this->oParam;
		}
		$mediaId = $getData['mediaId'];//媒介id
		$source_name = $getData['source_name'];//素材名称
		$source_type = $getData['source_type']?$getData['source_type']:0;//生成类型
		$Sstart = $getData['Sstart'];//开始时间
		$Send = $getData['Send'];//结束时间
		$tid = $getData['tid']?$getData['tid']:0;//副表主键
		$creater_id = $getData['creater_id']?$getData['creater_id']:0;//创建人
		// $media_id = 11000010002362;
		// $Sstart = '2019-09-24 00:01:00';
		// $Send = '2019-09-24 00:02:00';
		// $source_name = '浙江卫视2019-09-24 00：01：00-00：02：00';

		$Sstart_time 	= strtotime($Sstart);
		$Send_time 		= strtotime($Send);

		if(empty($source_name)){
			return A('Api/Function')->selfAjaxReturn(array('code'=>1,'msg'=>'素材名称不能为空'),$retType);
		}
		$doMedia = M('tmedia')->where(['fid'=>$mediaId])->find();
		if(empty($doMedia)){
			return A('Api/Function')->selfAjaxReturn(array('code'=>1,'msg'=>'媒体不存在'),$retType);
		}else{
			$mclass = substr($doMedia['fmediaclassid'], 0, 2);
		}
		if($mclass == '01' && $mclass == '02'){
			if(($Send_time - $Sstart_time) > 7200 ){
				return A('Api/Function')->selfAjaxReturn(array('code'=>1,'msg'=>'不允许超过2小时'),$retType);
			}elseif(($Send_time - $Sstart_time) < 2){
				return A('Api/Function')->selfAjaxReturn(array('code'=>1,'msg'=>'不允许小于2秒'),$retType);
			}
		}else{
			if(($Send_time - $Sstart_time) > 86400*7 ){
				return A('Api/Function')->selfAjaxReturn(array('code'=>1,'msg'=>'不允许超过7天'),$retType);
			}
		}

		$cut_url = 'http://'.C('ZJServerUrl').'/Api/M3u8Rec/add_source_make';//剪辑任务提交地址
		$post_data = array(
			'media_id'=>$mediaId,
			'source_name'=>$source_name,
			'Sstart'=>$Sstart,
			'Send'=>$Send,
		);

		$ret = json_decode(http($cut_url,$post_data,'GET',false,30),true);
		if(!empty($ret) && empty($ret['code'])){
			$fidentify = $ret['data']['source_id'];
		}
		if(!empty($fidentify)){
			$a_data['media_id'] = $mediaId;
			$a_data['source_name'] = $source_name;
			$a_data['start_time'] = $Sstart;
			$a_data['end_time'] = $Send;
			$a_data['fissue_date'] = date('Y-m-d',strtotime($Send));
			$a_data['source_state'] = 1;
			$a_data['create_time'] = date('Y-m-d H:i:s');
			$a_data['source_type'] = $source_type;
			$a_data['source_tid'] = $tid;
			$a_data['source_mediaclass'] = $mclass;
			$a_data['fidentify'] = $fidentify;
			$a_data['creater_id'] = $creater_id;
			$source_id = M('source_make')->add($a_data);
			return A('Api/Function')->selfAjaxReturn(array('code'=>0,'msg'=>'生成任务已创建，请等待生成完成','data'=>array('source_id'=>$source_id,'source_state'=>1)),$retType);
		}else{
			return A('Api/Function')->selfAjaxReturn(array('code'=>1,'msg'=>'创建失败,未知原因'),$retType);
		}
		
	}
	
	/**
	 * 监测素材生成状态
	 * by zw
	 */
	public function get_source_make_state(){
		$getData = $this->oParam;
		$selid = $getData['selid'];//素材主键

		if(!empty($selid)){
			if(is_array($selid)){
				$where['source_id'] = array('in',$selid);
			}else{
				$where['source_id'] = $selid;
			}
			$where['source_state'] = 2;
			$do_sme = M('source_make')->field('source_id,source_url')->where($where)->select();
			if(!empty($do_sme)){
				$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$do_sme));
			}else{
				$this->ajaxReturn(array('code'=>0,'msg'=>'暂无更新','data'=>[]));
			}
		}else{
			$this->ajaxReturn(array('code'=>0,'msg'=>'暂无更新','data'=>[]));
		}
		
	}

	/**
	 * 轮循获取远程素材的生成结果  2s/次
	 * by zw
	 */
	public function sourceMakeData(){
		$doSme = M('source_make')->field('fidentify,source_type,source_tid,source_mediaclass')->where(['source_state'=>1,'fidentify'=>['neq',0]])->select();
		if(!empty($doSme)){
			$ids = array_column($doSme, 'fidentify');
			$cut_url = 'http://'.C('ZJServerUrl').'/Api/M3u8Rec/returnCutData';//剪辑任务提交地址
			$post_data = array(
				'fidentify'=>$ids
			);
			$ret = json_decode(http($cut_url,$post_data,'get',false,30),true);
			if(!empty($ret) && empty($ret['code'])){
				foreach ($doSme as $key => $value) {
					$dataSme[$value['fidentify']] = $value;
				}
				foreach ($ret['data'] as $key => $value) {
					M('source_make')->where(['fidentify'=>$value['source_id']])->save(['source_url'=>$value['source_url'],'source_state'=>$value['source_state'],'fail_cause'=>$value['fail_cause']]);
					if($dataSme[$value['source_id']]['source_type'] == 30 && !empty($dataSme[$value['source_id']]['source_tid'])){//如果是样本需要剪辑素材
						if($dataSme[$value['source_id']]['source_mediaclass'] == 1){
							M('ttvsample')->where(['fid'=>$dataSme[$value['source_id']]['source_tid'],'favifilename'=>''])->save(['favifilename'=>$value['source_url'],'fmodifier'=>'AI推送样本素材地址','fmodifytime'=>date('Y-m-d H:i:s')]);
						}elseif($dataSme[$value['source_id']]['source_mediaclass'] == 2){
							M('tbcsample')->where(['fid'=>$dataSme[$value['source_id']]['source_tid'],'favifilename'=>''])->save(['favifilename'=>$value['source_url'],'fmodifier'=>'AI推送样本素材地址','fmodifytime'=>date('Y-m-d H:i:s')]);
						}elseif($dataSme[$value['source_id']]['source_mediaclass'] == 3){
							M('tpapersample')->where(['fid'=>$dataSme[$value['source_id']]['source_tid'],'fjpgfilename'=>''])->save(['fjpgfilename'=>$value['source_url'],'fmodifier'=>'AI推送样本素材地址','fmodifytime'=>date('Y-m-d H:i:s')]);
						}else{
							continue;
						}
					}
				}
			}
			$this->ajaxReturn(array('code'=>0,'msg'=>'更新成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
		}
	}

	/**
	 * 轮循检索素材生成超时，设置失败， 600s/次
	 * by zw
	 */
	public function get_source_make_state2(){
		$sqlstr = 'select count(*) AS cc from source_make where (unix_timestamp(now())-unix_timestamp(create_time))>(unix_timestamp(end_time)-unix_timestamp(start_time)) and source_state=1';
		$do_sme = M()->query($sqlstr);
		if($do_sme[0]['cc']>0){
			M()->execute('update source_make set source_state=-1 where (unix_timestamp(now())-unix_timestamp(create_time))>(unix_timestamp(end_time)-unix_timestamp(start_time)+600) and source_state=1');
			$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'暂无更新'));
		}
	}

	/*
	* 获取素材列表
	* by zw
	*/
	public function sourceMakeList(){
		$getData = $this->oParam;
		$mediaId = $getData['mediaId'];//媒体ID
		$issueDate = $getData['issueDate'];//任务日期

		$doSM = [];
		$whereSM['fissue_date'] = $issueDate;
		$whereSM['media_id'] = $mediaId;
		$doSM = M('source_make')
			->field('source_id,start_time,end_time,source_state,create_time,source_url')
			->where($whereSM)
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$doSM));
	}

	/*
	* 素材删除
	* by zw
	*/
	public function sourceMakeDel(){
		$getData = $this->oParam;
		$source_id = $getData['source_id'];//素材ID

		$whereSM['source_id'] = $source_id;
		$doSM = M('source_make')
			->where($whereSM)
			->delete();
		if(!empty($doSM)){
			$this->ajaxReturn(array('code'=>0,'msg'=>'删除成功'));
		}else{
			$this->ajaxReturn(array('code'=>1,'msg'=>'删除失败'));
		}
	}

	/*
	* 轮循获取媒体数据
	* by zw
	*/
	public function loopMediaData(){
		$gourl = 'http://'.C('ZJServerUrl').'/Api/Media/customerMedia';
		$pubData['customer'] = C('FORCE_NUM_CONFIG');

		$doMa = M('tmedia')->select();
		if(!empty($doMa)){
			$mediaIds = array_column($doMa, 'fid');
			$mediaOwnerIds = array_column($doMa, 'fmediaownerid');
		}
		$pubData['mediaIds'] = $mediaIds;
		$pubData['mediaOwnerIds'] = $mediaOwnerIds;

		$data = json_decode(http($gourl,json_encode($pubData),'POST',false,5),true);
		if(!empty($data['mediaData'])){
			$addMa = M('tmedia')->addAll($data['mediaData']);//固定不变，直接替换
		}
		if(!empty($data['mediaOwnerData'])){
			$addMo = M('tmediaowner')->addAll($data['mediaOwnerData']);//固定不变，直接替换
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'更新成功','mediaCount'=>$data['mediaData'],'mediaOwnerCount'=>$data['mediaOwnerData']));
	}
	
}
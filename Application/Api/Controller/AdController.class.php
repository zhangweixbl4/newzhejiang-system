<?php
namespace Api\Controller;
use Think\Controller;
use OpenSearch\Client\OpenSearchClient;
use OpenSearch\Client\DocumentClient;
use OpenSearch\Client\SearchClient;
use OpenSearch\Util\SearchParamsBuilder;
class AdController extends Controller {

	/**
     * 根据广告名搜索广告信息
     * @param $adName string 广告部分或全部名称
     */
    public function searchAdByName(){
    	$adName = trim(I('adName'));
        header('Access-Control-Allow-Origin:*');//允许跨域

		$adNameList = [];
		if(!empty($adName)){
			$where['fadname'] = ['like','%'.$adName.'%'];
			$where['fstate'] = ['in',[1,2,9]];
			$result = M('tad')
				->field('fadname,fadowner,fadclasscode,fbrand')
				->cache(600)
				->where($where)
				->order('fadname="'.$adName.'" desc')
				->limit(6)
				->select();
			if(!empty($result)){
				$ownerids = array_column($result,'fadowner');
				$do_adowner = M('tadowner')->field('fid,fname')->where(['fid'=>['in',$ownerids]])->select();
				$adownerData = array_column($do_adowner,'fname','fid');
			}

			foreach ($result as $key => $value) {
				if(!empty($adownerData[$value['fadowner']])){
					$value['fadownername'] = $adownerData[$value['fadowner']];
					$adNameList[] = $value;
				}
			}
		}
		
		$this->ajaxReturn(array('code'=>0,'data'=>$adNameList));
		
	}

	/**
     * 搜索广告主
     * @param $name string 广告主部分或全部名称
     */
    public function searchAdOwner()
    {
		header('Access-Control-Allow-Origin:*');//允许跨域
		$retData = [];
    	$name = trim(I('name'));
        
		if(!empty($name)){
			$where['fstate'] = ['NEQ', -1];
			$where['fname'] = ['LIKE', '%'.$name.'%'];
	        $data = M('tadowner')->field('fname value, fid')->cache(true,600)->where($where)->limit(6)->select();
			$opsearchUrl = 'http://47.98.142.150:8080/opsearch?name='.urlencode($name).'&start=1&hits=4';
			$opsearchData = http($opsearchUrl);
			$opsearchData = json_decode($opsearchData,true);
			foreach($data as $d1){
				$retData[] = array('fid'=>$d1['fid'],'value'=>$d1['value']);
			}
			foreach($opsearchData as $d2){
				$retData[] = array('value'=>$d2['name']);
			}
		}
        $this->ajaxReturn([
            'code' => 0,
            'data' => $retData
        ]);
	}
}
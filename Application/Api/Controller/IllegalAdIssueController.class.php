<?php
namespace Api\Controller;
use Think\Controller;

class IllegalAdIssueController extends Controller {

	/*
	* 补充交办记录
	* by zw
	*/
	public function grantlog($cr_num = '',$media_id = 0){
		set_time_limit(0);
		session_write_close();
		// $cr_num = [130000];
		if(empty($cr_num)){
			return false;
			// echo "参数有误";die;
		}
		$where_cr['cr_status'] = 1;
		if($cr_num == -1){
			$where_cr['cr_num'] = ['neq',0];
		}else{
			$where_cr['cr_num'] = $cr_num;
		}
		if(!empty($media_id)){
			if(!empty($cr_num) && $cr_num != -1){
				$where_rma['fcustomer'] = $cr_num;
			}
			$where_rma['fmediaid'] = $media_id;
			$nums = M('tregulatormedia')->where($where_rma)->group('fcustomer')->getField('fcustomer',true);
			if(!empty($nums)){
				$do_cr = $nums;
			}else{
				return false;
				// echo "不存在媒体相关客户";die;
			}
		}

		//获取客户
		if(empty($do_cr)){
			$do_cr = M('customer')
				->where($where_cr)
				->group('cr_num')
				->getField('cr_num',true);
		}

		foreach ($do_cr as $fcustomer) {
			M('tbn_media_grant')->where(['fmedia_id'=>$media_id,'fcustomer'=>$fcustomer])->delete();

			if($fcustomer == 100000){
				if(!empty($media_id)){
					$where_rma['a.fmediaid'] = $media_id;
				}
				$where_rma['a.fisoutmedia'] = 0;
				$where_rma['a.fstate'] = 1;
				$where_rma['a.fcustomer'] = $fcustomer;
				$where_rma['a.fregulatorcode'] = ['like','20%'];
				$do_rma = M('tregulatormedia')
					->master(true)
					->field('a.fregulatorcode,a.fmediaid,a.fcustomer,d.fid treid,d.fname trename,e.fid grtid,c.fname tre_grantname,c.fid tre_grantid')
					->alias('a')
					->join('tmedia b on a.fmediaid = b.fid')
					->join('tregulator c on a.fregulatorcode = c.fid and c.fstate = 1 and c.fkind = 1 and c.ftype = 20')
					->join('tregulator d on d.fregionid = b.media_region_id and d.fstate = 1 and d.fkind = 1 and d.ftype = 20 and length(d.fid) = 8')
					->join('tbn_media_grant e on e.freg_id = d.fid and a.fmediaid = e.fmedia_id and e.fcustomer = a.fcustomer and e.fgrant_reg_id = c.fid','left')
					->where($where_rma)
					->select();
				$add_grt = [];
				foreach ($do_rma as $key => $value) {
					if(empty($value['grtid'])){
						$add_grt[] = [
							'freg_id'=>$value['treid'],
							'freg'=>$value['trename'],
							'fmedia_id'=>$value['fmediaid'],
							'fgrant_reg_id'=>$value['tre_grantid'],
							'fgrant_reg'=>$value['tre_grantname'],
							'fgrant_user'=>'ADMIN',
							'fgrant_time'=>date('Y-m-d H:i:s'),
							'fcustomer'=>$fcustomer,
						];
					}
				}
				if(!empty($add_grt)){
					$finish_count = M('tbn_media_grant')->addall($add_grt);
				}
			}else{
				$do_region = M('tregulator')
					->alias('a')
					->field('a.fname,a.fid')
					->join('tregion b on b.fid = a.fregionid and a.fstate = 1 and a.fkind = 1 and a.ftype = 20')
					->where(['b.fid'=>$fcustomer])
					->find();//客户信息

				//只读取客户下级的媒体授权数据
				if(!empty($media_id)){
					$where_rma['a.fmediaid'] = $media_id;
				}
				$where_rma['a.fisoutmedia'] = 0;
				$where_rma['a.fstate'] = 1;
				$where_rma['a.fcustomer'] = $fcustomer;
				$do_rma = M('tregulatormedia')
					->field('a.fmediaid,c.fid treid,c.fname trename,c.fpid trepid')
					->alias('a')
					->join('tregulator c on a.fregulatorcode = c.fid and c.fstate = 1 and c.fkind = 1 and c.ftype = 20 and c.fregionid <> '.$fcustomer)
					->where($where_rma)
					->select();
				foreach ($do_rma as $key => $value) {
					$add_grt = [];
					$trepid = $value['trepid'];//被授权机构的上级用户ID
					$freg_id = $value['treid'];//被授权机构ID
					$freg = $value['trename'];//被授权机构名称
					while(!empty($trepid)){
						//获取上级用户信息，排除客户信息（当上级为客户时，查询结果为空），并获取与本级是否存在交办关系
						$do_trem = M('tregulatormedia')
							->alias('a')
							->field('c.fid treid,c.fname trename,c.fpid trepid')
							->join('tregulator c on a.fregulatorcode = c.fid and c.fstate = 1 and c.fkind = 1 and c.ftype = 20 and c.fregionid <> '.$fcustomer)
							->where(['a.fisoutmedia'=>0,'a.fstate'=>1,'a.fcustomer'=>$fcustomer,'a.fregulatorcode = '.$trepid,'a.fmediaid'=>$value['fmediaid']])
							->find();
						if(!empty($do_trem)){//如果上级也有授权当前媒体并且上级非顶级，那么建立顶级与本机构的交办关系
							//与本级是否存在交办关系，不存在的话添加记录
							$isgrant = M('tbn_media_grant')->master(true)->where(['freg_id'=>$freg_id,'fgrant_reg_id'=>$do_trem['treid'],'fmedia_id'=>$value['fmediaid'],'fcustomer'=>$fcustomer])->count();
							if(empty($isgrant)){
								$add_grt[] = [
									'freg_id'=>$freg_id,
									'freg'=>$freg,
									'fmedia_id'=>$value['fmediaid'],
									'fgrant_reg_id'=>$do_trem['treid'],
									'fgrant_reg'=>$do_trem['trename'],
									'fgrant_user'=>'ADMIN',
									'fgrant_time'=>date('Y-m-d H:i:s'),
									'fcustomer'=>$fcustomer,
								];
							}

							//继续赋值轮循获取上级的上级数据
							$trepid = $do_trem['trepid'];
							$freg_id = $do_trem['treid'];
							$freg = $do_trem['trename'];
						}else{//如果上级是顶级，那么建立顶级与本机构的交办关系
							//与本级是否存在交办关系，不存在的话添加记录
							if($do_region['fid'] == $trepid){
								$isgrant = M('tbn_media_grant')->master(true)->where(['freg_id'=>$freg_id,'fgrant_reg_id'=>$do_region['fid'],'fmedia_id'=>$value['fmediaid'],'fcustomer'=>$fcustomer])->count();
								if(empty($isgrant)){
									$add_grt[] = [
										'freg_id'=>$freg_id,
										'freg'=>$freg,
										'fmedia_id'=>$value['fmediaid'],
										'fgrant_reg_id'=>$do_region['fid'],
										'fgrant_reg'=>$do_region['fname'],
										'fgrant_user'=>'ADMIN',
										'fgrant_time'=>date('Y-m-d H:i:s'),
										'fcustomer'=>$fcustomer,
									];
								}
							}

							//当前媒体交办关系读取结束
							$trepid = 0;
						}
					}
					//当前媒体权限读取完毕，添加交办记录
					if(!empty($add_grt)){
						M('tbn_media_grant')->addAll($add_grt);
					}
				}
			}
		}
		// echo '执行完成';
		return true;
	}

	/*
	* 执行“生成违法广告的派发记录”功能
	* by zw
	*/
	public function create_illad_sendlog(){
		set_time_limit(0);
		session_write_close();
		$cr_num = I('num');//指定更新补充某一客户
		$m = I('m');//指定某一个月
		if(empty($m) || empty($cr_num)){
			echo '参数有误';die;
		}

		$month = date("m",strtotime($m));
		$year = date("Y",strtotime($m));
		$where_illad['a.fcustomer'] = $cr_num;
		$where_illad['month(b.fissue_date)'] = $month;
		$where_illad['year(b.fissue_date)'] = $year;
		$do_illad = M('tbn_illegal_ad')
			->alias('a')
			->join('tbn_illegal_ad_issue b on a.fid = b.fillegal_ad_id')
			->where($where_illad)
			->getField('a.fid',true);
		if(!empty($do_illad)){
			$this->illegal_ad_sendlog($do_illad);
		}
		echo '执行完成';
	}

	/*
	* 生成违法广告的派发记录
	* by zw
	*/
	public function illegal_ad_sendlog($adids = []){
		set_time_limit(0);
		session_write_close();
		// $adids = [221438];
		//获取违法广告信息
		$do_ad = M('tbn_illegal_ad')
			->master(true)
			->field('a.fid illadid,a.fcustomer,b.fid treid,b.fname trename,a.identify,a.fmedia_id,a.fstatus')
			->alias('a')
			->join('tregulator b on concat("20",a.fcustomer) = b.fid and b.fstate = 1 and b.fkind = 1 and b.ftype = 20')
			->where(['a.fid'=>['in',$adids]])
			->select();
		foreach ($do_ad as $key => $value) {
			$add_sendarr = [];//重置存储待添加的派发记录（多条）
			$frece_reg_id = $value['treid'];//接收机构ID
			$frece_reg = $value['trename'];//接收机构
			$fsend_reg_id = 0;//下发机构ID
			$fsend_reg = 'AI';//下发机构
			while (!empty($frece_reg_id)) {
				$add_send = [];//重置存储待添加的派发记录（单条）

				//判断派发记录是否存在
				$do_send = M('tbn_case_send')
					->master(true)
					->field('fid')
					->where(['fillegal_ad_id'=>$value['illadid'],'frece_reg_id'=>$frece_reg_id,'fsend_reg_id'=>$fsend_reg_id])
					->find();
				if(empty($do_send)){//不存在则添加派发记录
					$add_send = [
						'fillegal_ad_id'=>$value['illadid'],
						'frece_reg_id'=>$frece_reg_id,
						'frece_reg'=>$frece_reg,
						'frece_time'=>date('Y-m-d H:i:s'),
						'fsend_reg_id'=>$fsend_reg_id,
						'fsend_reg'=>$fsend_reg,
						'fsend_time'=>date('Y-m-d H:i:s'),
						'fstatus'=>0,
						'fsendtype'=>0,
					];
				}

				if($value['identify'] != 'new_2'){//监管媒体违法广告数据执行
					$fsend_reg_id = $frece_reg_id;
					$fsend_reg = $frece_reg;

					//检查媒体是否交办，如果有媒体交办记录，需要将前一条派发记录状态更新为已处理（派发）状态
					$do_grant = M('tbn_media_grant')
						->cache(true,2)
						->field('freg_id,freg')
						->where(['fgrant_reg_id'=>$fsend_reg_id,'fmedia_id'=>$value['fmedia_id'],'fcustomer'=>$value['fcustomer']])
						->find();
					if(!empty($do_grant)){
						if(!empty($add_send)){
							$add_send['fstatus'] = 20;//因交办存在，所以下级会有派发记录，设置当前派发记录为已派发
						}
						$frece_reg_id = $do_grant['freg_id'];
						$frece_reg = $do_grant['freg'];
					}else{
						$frece_reg_id = 0;//用于跳出while
					}
				}else{//本地广告主，外省媒体播出的违法广告数据执行
					$frece_reg_id = 0;//用于跳出while
				}
				

				//将要退出前，判断该记录是否又被派发到下级
				if(empty($frece_reg_id)){
					if(!empty($add_send)){
						$add_send['fstatus'] = $value['fstatus'];//下级有派发记录
					}

					//检查是否还有下派记录
					$count_send = M('tbn_case_send')
						->master(true)
						->where(['fillegal_ad_id'=>$value['illadid'],'fsend_reg_id'=>$fsend_reg_id])
						->count();
					if(!empty($count_send)){//如果有，要把当前记录设置为已下派
						if(!empty($add_send)){
							$add_send['fstatus'] = 20;//下级有派发记录
						}else{
							M('tbn_case_send')->where(['fid'=>$do_send['fid']])->save(['fstatus'=>20]);
						}
					}
				}

				//存放派发记录
				if(!empty($add_send)){
					$add_sendarr[] = $add_send;
				}
			}

			//为避免一个数组出现多条相同ID，需要在循环内部先添完，后再继续下一轮循环，此影响效率，如不考虑会出现重复违法广告ID时可放循环外面
			if(!empty($add_sendarr)){
				M('tbn_case_send')->addall($add_sendarr);
			}
		}
		
		return true;
	}
	
	/*
	* 违法广告再次发布提醒，轮循1天/次
	* by zw
	*/
	public function illadSmsRegulator(){
		$customerurl = ['zibo.hz-data.xyz'];
		foreach ($customerurl as $key => $value) {
			$do_systemset = M('customer_systemset')
				->alias('a')
				->field('a.*,b.cr_num')
				->join('customer b on a.cst_crid = b.cr_id')
				->where(['cr_url'=>$value])
				->select();
			$system_num = $do_systemset[0]['cr_num'];
			$set = array_column($do_systemset,'cst_val','cst_name');
			$isrelease = $set['isrelease']?$set['isrelease']:0;
			$isexamine = $set['isexamine']?$set['isexamine']:0;
			$media_class = $set['media_class']?$set['media_class']:[];
			$illDistinguishPlatfo = $set['illDistinguishPlatfo']?$set['illDistinguishPlatfo']:0;

			$where_time = '1=1';
			$where_tia['_string'] = '1=1';
	        if($isrelease == 2 || $isrelease == 1){
	            $where_time .= ' and tbn_illegal_ad_issue.fsend_status = 2 ';
	        }
	        //国家局系统只显示有打国家局标签媒体的数据
		    if($system_num == '100000'){
		      $wherestr = ' and tn.flevel in (1,2,3)';
		    }
	        if($isexamine == 10 || $isexamine == 20){
	          $where_time .= ' and tbn_illegal_ad.fexamine = 10';
	        }
	        if($system_num == '100000'){
	            $where_tia['_string'] = ' AND in (select fid from tregion where flevel in (1,2,3)) ';
	        }
	        if(!empty($illDistinguishPlatfo) && !empty($media_class)){
		      $where2 = ' and left(b.fmediaclassid,2) in('.implode(',', $media_class).')';
		    }
		    $where_time .= ' and tbn_illegal_ad_issue.fissue_date between "'.date('Y-m-01').'" and "'.date('Y-m-d',strtotime(date('Y-m-01').' +1 month -1 day')).'"';
		    $where_tia['a.fstatus'] = 0;
		    $where_tia['a.fcustomer'] = $system_num;
		    $nowdata = M('tbn_illegal_ad')
		      ->alias('a')
		      ->field('ifnull(x.fcount,0) as fcount,a.fsample_id,a.fid,a.fad_name')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
		      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
		      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
		      ->where($where_tia)
		      ->select();

		    $where_tia['a.fstatus'] = ['gt',0];
		    $where_time .= ' and tbn_illegal_ad_issue.fissue_date between "'.date('Y-m-d',strtotime(date('Y-m-01').' -1 month')).'" and "'.date('Y-m-d',strtotime(date('Y-m-01').' -1 day')).'"';
		    $olddata = M('tbn_illegal_ad')
		      ->alias('a')
		      ->join('tadclass b on a.fad_class_code=b.fcode')
		      ->join('tmedia c on a.fmedia_id=c.fid and c.fid=c.main_media_id ')
		      ->join('tregion tn on a.fregion_id=tn.fid'.$wherestr)
		      ->join('(select count(*) as fcount,fillegal_ad_id,DATE_FORMAT(min(fissue_date),"%Y-%m-%d") as fstarttime,DATE_FORMAT(max(fissue_date),"%Y-%m-%d") as fendtime,timestampdiff(week,min(fissue_date),DATE_ADD(max(fissue_date),INTERVAL 7 DAY)) as diffweek from tbn_illegal_ad_issue,tbn_illegal_ad where '.$where_time.' and tbn_illegal_ad.fid = tbn_illegal_ad_issue.fillegal_ad_id and tbn_illegal_ad.fcustomer = "'.$system_num.'" group by fillegal_ad_id) as x on a.fid=x.fillegal_ad_id')
		      ->where($where_tia)
		      ->group('a.fsample_id')
		      ->getField('fsample_id',true);

		    $smsRegulator = [];//需要通知的机构
		    $smsData = [];//通知内容
		    foreach ($nowdata as $key => $value) {
		    	if(in_array($value['fsample_id'], $olddata)){
		    		$do_send = M('tbn_case_send')->field('frece_reg_id')->where(['fillegal_ad_id'=>$value['fid']])->select();
		    		foreach ($do_send as $key2 => $value2) {
		    			if(!in_array($value2['frece_reg_id'], $smsRegulator)){
		    				$smsRegulator[] = $value2['frece_reg_id'];
		    				$smsData[$value2['frece_reg_id']][] = ['ad'=>$value['fad_name'],'count'=>$value['fcount']];
		    			}else{
		    				$smsData[$value2['frece_reg_id']][] = ['ad'=>$value['fad_name'],'count'=>$value['fcount']];
		    			}
		    		}
		    	}
		    }

		    if(!empty($smsRegulator)){
		    	$where_tregulator['_string'] = '(plbcount = 0 or plbcount is null or plbcount2 = 1)';
				$where_tregulator['a.fid'] = ['in',$smsRegulator];
			    $do_tregulator = M('tregulator')
			    	->alias('a')
			    	->field('a.fid,b.fmobile')
			    	->join('tregulatorperson b on a.fid = b.fregulatorid and length(b.fmobile) = 11')
			    	->join('(select count(*) as plbcount,sum(case when fcustomer = "'.$system_num.'" then 1 else 0 end) as plbcount2,fpersonid from tpersonlabel  where fstate = 1 group by fpersonid) lb on b.fid = lb.fpersonid','left')
			    	->where($where_tregulator)
			    	->select();

	            foreach ($do_tregulator as $key => $value) {
	            	$smsstr = '';
	            	if(!empty($smsData[$value['fid']])){
	            		if(count($smsData[$value['fid']])<=5){
	        				foreach ($smsData[$value['fid']] as $key2 => $value2) {
		            			if(!empty($smsstr)){
		            				$smsstr .= '、'.$value2['ad'].'（'.$value2['count'].'）';
		            			}else{
		            				$smsstr = $value2['ad'].'（'.$value2['count'].'）';
		            			}
		            		}
		            		$smsstr = '发现往期已处理过的'.$smsstr.'广告仍有播出情况';
	            		}else{
	            			$smsstr = '多条往期已处理过的涉嫌违法广告在本月仍有播出情况';
	            		}
		            	$ret = A('Common/Alitongxin','Model')->usertask_sms($value['fmobile'],'省广告监测平台监测提醒。'.$smsstr);
	            	}
	            }
		    }
			
		}
        
	}
	
	/*
	* 补充违法认定依据
	* by zw
	*/
	public function addconfirmations(){
		set_time_limit(0);
		session_write_close();
		$mclass = I('mclass');
		if(!in_array($mclass, ['tv','bc','paper'])){
			exit('参数有误');
		}
		
		$doSample = M('t'.$mclass.'sample')->field('fid,fexpressioncodes')->where(['fexpressioncodes'=>['neq',''],'fillegaltypecode'=>['gt',0],'_string'=>'fconfirmations is null or fconfirmations = ""'])->select();
		foreach ($doSample as $key => $value) {
			$illegalData = D('Common/Function')->sampleillegal($value['fexpressioncodes']);//获取违法信息

            M()->execute('update t'.$mclass.'sample set fconfirmations = "'.$illegalData['fconfirmations'].'" where fid = '.$value['fid']);
		}
		echo count($doSample);
	}
	
}
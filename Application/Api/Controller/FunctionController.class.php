<?php
namespace Api\Controller;
use Think\Controller;
import('Vendor.PHPExcel');
class FunctionController extends Controller {

	public $oParam;//公共变量

	public function _initialize(){
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		header("Content-type: text/html; charset=utf-8"); 
		$this->oParam = json_decode(file_get_contents('php://input'),true);//接收数据
	}

	//提示返回方式
	public function selfAjaxReturn($res = [],$retType = 0){
		if(!empty($retType)){
			return $res;
		}else{
			$this->ajaxReturn($res);
		}
	}

	/**
	* 获取所有广告类别
	 * by zw
	 */
	public function getAllAdClass(){
		$data = M('tadclass')->cache(true,86399)->field('fcode,fpcode,fadclass')->where(array('fstate'=>1))->select();//所有列表
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	* 获取节目类别
	 * by zw
	 */
	public function getAllProgClass(){
		$getData = $this->oParam;
		$mclass = $getData['mclass'];
		if($mclass == '01'){
			$where['fmediaclass'] = 'tv';
		}elseif($mclass == '02'){
			$where['fmediaclass'] = 'bc';
		}
		$where['fstate'] = 1;
		$data = M('tprogclass')->cache(true,86399)->field('fprogclasscode fcode,"" fpcode,fprogclass fadclass')->where(array('fstate'=>1))->select();//所有列表
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	* 获取违法表现，私有遍历方法
	* by zw
	*/
	private function tillegal($pid=''){
		$datatl=M('tillegal')->field('fid,fcode,fexpression,fconfirmation,fstate')->where(['fpcode'=>$pid,'fstate'=>1])->select();
		if(!empty($datatl)){
			if(empty($pid)){
				foreach ($datatl as $key => $value) {
					$datatl[$key]['list_2']=$this->tillegal($value['fcode']);;
				}
			}else{
				return $datatl;
			}
		}
		return $datatl;
	}

	/**
	* 获取违法表现
	* by zw
	*/
	public function getTillegal(){
		$data = $this->tillegal();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	 * 获取浙江所有地区
	 * by zw
	 */
	public function getRegionList(){
		$getData = $this->oParam;
		$selalltype = $getData['selalltype'];//是否有全选功能
		$parent = $getData['parent']?$getData['parent']:33;//所属地域
		$iscontain = $getData['iscontain']?$getData['iscontain']:0;//是否含下级，0不包含，1包含
	    $regionlevel = $getData['regionlevel']?$getData['regionlevel']:0;//查询地域类型，1省级，4市级，5区县级

		$where['fstate'] 	= 1;
		$where['fid'] = array('like',$parent.'%');
		//地域类型过滤
    	switch ($regionlevel) {
    		case 1:
    			if(empty($iscontain)){
    				$where['flevel'] = 1;
    			}
    			break;
    		case 4:
    			if(empty($iscontain)){
    				$where['flevel'] = ['in',[2,3,4]];
    			}else{
    				$where['flevel'] = ['in',[2,3,4,5]];
    			}
    			break;
    		case 5:
				$where['flevel'] = 5;
    			break;
    	}
    	if(!empty(session('regulatorpersonInfo.fid'))){
        	$where['_string'] = ' a.fid in (select distinct(b.media_region_id) from tmedia_temp a,tmedia b where a.fmediaid = b.fid and a.ftype=1 and a.fcustomer = "'.C('FORCE_NUM_CONFIG').'" and a.fuserid='.session('regulatorpersonInfo.fid').')';
        }

		$data = M('tregion')
			->alias('a')
			->field('fid,fpid,fname1 as fname,ffullname')
			->cache(true,60)
			->where($where)
			->order('fid asc')
			->select();//查询地区
		if(!empty($selalltype)){
			$regionData = [];
			$selallData = [];
			if($regionlevel == 5){
				$regionData = array_column($data, 'fid');
				if(!empty($regionData)){
					$selallData[] = [
						'fid'=>implode(',', $regionData),
						'fname'=>'全部',
						'ffullname'=>'全部',
						'fpid'=>0
					];
				}
			}else{
				foreach ($data as $key => $value) {
					if($value['fpid']<>100000 && !empty($value['fpid'])){
						$regionData[$value['fpid']][] = $value['fid'];
					}
				}
				foreach ($regionData as $key => $value) {
					if(count($value)>1){
						$selallData[] = [
							'fid'=>implode(',', $value),
							'fname'=>'全部',
							'ffullname'=>'全部',
							'fpid'=>$key
						];
					}
				}
			}
			
			$data = array_merge($selallData,$data);
		}
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>$data));
	}

	/**
	* 获取媒体列表
	* by zw
	*/
	public function getMediaList(){
		$getData = $this->oParam;
		$from = $getData['from'];//媒体类别，pList 串播单媒体 ppList 监播单媒体
		$area = $getData['area'];//行政区划
		$regionid = $getData['regionid']?$getData['regionid']:'';//地区，数组
		$iscontain = $getData['iscontain']?$getData['iscontain']:0;//是否包含下属地区
		$mclass 	= $getData['mclass']?$getData['mclass']:'';//媒体类型,数组['01','02']
	    $regionlevel = $getData['regionlevel']?$getData['regionlevel']:0;//查询地域类型，1省级，4市级，5区县级
	    $fisxinyong = $getData['fisxinyong']?$getData['fisxinyong']:[];//是否信用评价，数组，1是，2否，空是全部

		$where_media['a.fstate'] = 1;
		$where_media['_string'] = 'a.fid = a.main_media_id';
		//媒体类型过滤
		if(!empty($getData['mediaClass'])){
			$where_media['left(a.fmediaclassid,2)'] = $getData['mediaClass'];
		}
		//媒体类型过滤，数组
        if(is_array($mclass)){
        	$where_media['left(a.fmediaclassid,2)'] = ['in',$mclass];
        }
		//媒体名称过滤
		if(!empty($getData['mediaName'])){
			$where_media['a.fmedianame'] = ['like','%'.$getData['mediaName'].'%'];
		}
		//指定地域过滤
		if(!empty($area)){
        	if(!empty($iscontain)){
				$tregion_len = get_tregionlevel($area);
				if($tregion_len == 2){//省级
					$where_media['a.media_region_id'] = ['like',substr($area,0,2).'%'];
				}elseif($tregion_len == 4){//市级
					$where_media['a.media_region_id'] = ['like',substr($area,0,4).'%'];
				}elseif($tregion_len == 6){//县级
					$where_media['a.media_region_id'] = ['like',substr($area,0,6).'%'];
				}
			}else{
				$where_media['a.media_region_id'] = $area;
			}
        }

        //是否信用评价媒体
		if(count($fisxinyong) == 1){
			if(in_array(1, $fisxinyong)){
				$where_media['_string'] .= ' and a.fisxinyong = 1';
			}elseif(in_array(2, $fisxinyong)){
				$where_media['_string'] .= ' and a.fisxinyong = 0';
			}
		}

        //多地区筛选
		$arr_area = [];
		if(!empty($regionid)){
			if(!empty($iscontain)){
				foreach ($regionid as $value) {
					$gregion = D('Common/Function')->get_next_region($value,true);
					if(!empty($gregion)){
						$arr_area = array_merge($arr_area,$gregion);
					}
				}
				$arr_area = array_unique($arr_area);
			}else{
				$arr_area = $regionid;
			}
			$where_media['_string'] .= ' and a.media_region_id in('.implode(',', $arr_area).')';
		}
        //地域类型过滤
    	switch ($regionlevel) {
    		case 1:
    			if(empty($iscontain)){
    				$where_media['b.flevel'] = 1;
    			}
    			break;
    		case 4:
    			if(empty($iscontain)){
    				$where_media['b.flevel'] = ['in',[2,3,4]];
    			}else{
    				$where_media['b.flevel'] = ['in',[2,3,4,5]];
    			}
    			break;
    		case 5:
				$where_media['b.flevel'] = 5;
    			break;
    	}
        //串播单媒体过滤
        if($from == 'pList' || $from == 'ppList'){
        	$where_media['_string'] .= ' and a.fid in(select fmediaid from tmedialabel where flabelid in(446,449))';
        }
        if(!empty(session('regulatorpersonInfo.fid'))){
        	$where_media['_string'] .= ' and a.fid in (select distinct(fmediaid) from tmedia_temp where ftype=1 and fcustomer = "'.C('FORCE_NUM_CONFIG').'" and fuserid='.session('regulatorpersonInfo.fid').')';
        }
		$do_media = M('tmedia')
			->cache(true,600)
			->alias('a')
			->field('a.fid,a.fmediaclassid,a.fmediaownerid,(case when instr(a.fmedianame,"（") > 0 then left(a.fmedianame,instr(a.fmedianame,"（") -1) else a.fmedianame end) as fmedianame')
			->join('tregion b on a.media_region_id = b.fid')
			->where($where_media)
			->order('a.media_region_id asc,a.fmediaclassid asc')
			->select();
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','count'=>count($do_media),'data'=>$do_media));
	}

	/**
	 * 数据以xls导出公共方法
	 * by zw
	 */
	public function outdata_xls($outdata){
		session_write_close();

		//传参示例
        // $outdata['datalie'] = ['序号','地域','发布媒体'];
        // $outdata['datazd'] = [['type'=>'zwif','data'=>[['{fmedia_class} == 1 || {fmedia_class} == 2 || {fmedia_class} == 3','广告名：{fad_name}'],['','未备']],'ffullname','fmedianame'];

		$title = $outdata['title'];//标题
		$endtitle = $outdata['endtitle'];//结束标题
		$filenames = $outdata['filenames']?$outdata['filenames']:date('Ymdhis');//保存的文件名称
		$sheetname = $outdata['sheetnames']?$outdata['sheetnames']:'Sheet1';//表格1标签名
		$datalists = $outdata['lists'];//需要导出的数据
		$datalie = $outdata['datalie'];//数据列

		$liecount = count($datalie);//计划一共有多少列数据

		$objPHPExcel = new \PHPExcel();  
	    $objPHPExcel
			->getProperties()  //获得文件属性对象，给下文提供设置资源
			->setCreator( "MaartenBalliauw")             //设置文件的创建者
			->setLastModifiedBy( "MaartenBalliauw")       //设置最后修改者
			->setTitle( "Office2007 XLSX Test Document" )    //设置标题
			->setSubject( "Office2007 XLSX Test Document" )  //设置主题
			->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
			->setKeywords( "office 2007 openxmlphp")        //设置标记
			->setCategory( "Test resultfile");                //设置类别

		$sheet = $objPHPExcel->setActiveSheetIndex(0);

		$nowline = 0;
		if(!empty($title)){
			if(is_array($title)){
				$stitle = $title;
			}else{
				$stitle[] = $title;
			}
			foreach ($stitle as $value) {
				$nowline ++;
				$sheet -> mergeCells(chr(65).$nowline.':'.chr(64+$liecount).$nowline);
				$sheet ->getStyle(chr(65).$nowline.':'.chr(64+$liecount).$nowline)->applyFromArray(array('font' => array ('bold' => true),'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
				$sheet ->setCellValue(chr(65).$nowline,$value);
			}
		}

		$lienum = 0;
		$nowline ++;
		foreach ($datalie as $key => $value) {
			$lienum ++;
			$sheet ->setCellValue(chr((64+$lienum)).$nowline,$key);
		}

		$pattern="/{(.*)}/iU";
		foreach ($datalists as $key => $value) {//生成数据
			$lienum = 0;
			$nowline ++;
			foreach ($datalie as $liekey => $lievalue) {
				$lienum ++;
				if(is_array($lievalue)){
					if($lievalue['type'] == 'zwhebing'){//zwhebing
						$patternstrexec = $lievalue['data'];
						if(!empty($patternstrexec)){
							if(empty($patternarrexec[$lienum])){
    							preg_match_all($pattern,$patternstrexec,$patternarrexec[$lienum]);
							}
    						foreach ($patternarrexec[$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
						      $patternstrexec = str_replace($value3, $value[$patternarrexec[$lienum][1][$key3]], $patternstrexec);
						    }
						    $sheet ->setCellValue(chr((64+$lienum)).$nowline,$patternstrexec);
						}
					}else{//zwif
						$excel_value = '';
						foreach ($lievalue['data'] as $key2 => $value2) {
							$patternstr = $value2[0];
							if(!empty($patternstr)){
								if(empty($patternarr[$key2][$lienum])){
		    						preg_match_all($pattern,$patternstr,$patternarr[$key2][$lienum]);
								}
							    foreach ($patternarr[$key2][$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
								    $patternstr = str_replace($value3, '$value["'.$patternarr[$key2][$lienum][1][$key3].'"]', $patternstr);
								    eval('$panduan = '.$patternstr.';');
								    if($panduan){
								      	$patternstrexec = $value2[1];
										if(!empty($patternstrexec)){
											if(empty($patternarrexec[$key2][$lienum])){
				    							preg_match_all($pattern,$patternstrexec,$patternarrexec[$key2][$lienum]);
											}
											if(!empty(($patternarrexec[$key2][$lienum][0]))){
												foreach ($patternarrexec[$key2][$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
											      $patternstrexec = str_replace($value3, $value[$patternarrexec[$key2][$lienum][1][$key3]], $patternstrexec);
											    }
											}
											$excel_value = $patternstrexec;
										    $sheet ->setCellValue(chr((64+$lienum)).$nowline,$excel_value);
										}
							      		break;
							      	}
							    }
							}else{
					      		$patternstrexec2 = $value2[1];
								if(!empty($patternstrexec2)){
									if(empty($patternarrexec2[$key2][$lienum])){
		    							preg_match_all($pattern,$patternstrexec2,$patternarrexec2[$key2][$lienum]);
									}
		    						foreach ($patternarrexec2[$key2][$lienum][0] as $key3 => $value3) {//获取组装好的判断条件
								      $patternstrexec2 = str_replace($value3, $value[$patternarrexec2[$key2][$lienum][1][$key3]], $patternstrexec2);
								    }
								    $excel_value = $patternstrexec2;
								    $sheet ->setCellValue(chr((64+$lienum)).$nowline,$excel_value);
								}
						      	break;
							}
							if(!empty($excel_value)){
								break;
							}
						}
					}
				}else{
					if($lievalue != 'key'){
						$sheet ->setCellValue(chr((64+$lienum)).$nowline,$value[$lievalue]);
					}else{
						$sheet ->setCellValue(chr((64+$lienum)).$nowline,($key+1));
					}
				}
			}
		}

		//结束语
		if(!empty($endtitle)){
			$nowline ++;
			$sheet -> mergeCells(chr(65).$nowline.':'.chr(64+$liecount).$nowline);
			$sheet ->getStyle(chr(65).$nowline.':'.chr(64+$liecount).$nowline)->applyFromArray(array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)));
			$sheet ->setCellValue(chr(65).$nowline,$endtitle);
		}

		$objActSheet =$objPHPExcel->getActiveSheet();
		//给当前活动的表设置名称
		$objActSheet->setTitle($sheetname);

		$objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$savefile = './LOG/'.date('Ymdhis').'.xlsx';
		$objWriter->save($savefile);

		$ret = A('Common/AliyunOss','Model')->file_up('tem/'.$filenames.'.xlsx',$savefile,array('Content-Disposition' => 'attachment;filename='.$filenames.'.xlsx'));//上传云

		//无法上传云的情况，放服务器
        if(empty($ret['url'])){
            $ret['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/LOG/'.$date('Ymdhis').'.xlsx';
        }else{
            unlink($savefile);//删除文件
        }

		return $ret;
	}

	/*
    * 获取断流时段
    * by zw
    */
    public function getInterruptFlow(){
    	$getData = $this->oParam;
    	$startTime = $getData['startTime'];
    	$endTime = $getData['endTime'];
    	$mediaId = $getData['mediaId'];
        if(empty($startTime) || empty($endTime) || empty($mediaId)){
            $this->ajaxReturn(['code'=>1,'msg'=>'参数有误']);
        }
        $data = http('http://az.hz-data.xyz:8087/channel/'.$mediaId.'/m3u8.gap?start_time='.$startTime.'&end_time='.$endTime,[],'get',false,10);
        $this->ajaxReturn(['code'=>0,'msg'=>'获取成功','data'=>$data]);
    }

    /*
    * 获取断流时段
    * by zw
    */
    public function getInterruptFlow2(){
        $data = $this->tohttp('http://az.hz-data.xyz:8087/channel/11000010002360/m3u8.gap?start_time=1577721600&end_time=1577807999',[],'POST',false,0);
        dump($data);
    }

    public function uploadTest(){
    	$ret = A('Common/AliyunOss','Model')->file_up('tem/'.time().'.xlsx','./LOG/20200115112550.xlsx',array('Content-Disposition' => 'attachment;filename'.time().'.xlsx'));//上传云
    	var_dump($ret);
    }

    /**
	 * 发送HTTP请求方法，目前只支持CURL发送请求
	 * @param  string  $url    请求URL
	 * @param  array   $params 请求参数
	 * @param  string  $method 请求方法GET/POST
	 * @return array   $data   响应数据
	 */
	public function tohttp($url, $params = array(), $method = 'GET',$header = false,$CURLOPT_TIMEOUT = 2,$CURLOPT_HEADER = false){
		$msectime = msectime();
		$http_log = '';
		$http_log .= date('H:i:s').'	'.$url.'	';
		set_time_limit(360);
		$opts = array(
			CURLOPT_TIMEOUT        => $CURLOPT_TIMEOUT,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
		);
		switch(strtoupper($method)){
			case 'GET':
				$getQuerys = !empty($params) ? '?'. http_build_query($params) : '';
				$opts[CURLOPT_URL] = $url . $getQuerys;
				//var_dump($opts[CURLOPT_URL]);
				break;
			case 'POST':
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_POST] = 1;
				$opts[CURLOPT_POSTFIELDS] = $params;
				break;
			case "DELETE":
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;

		}
		/* 初始化并执行curl请求 */
		$ch = curl_init();
		// CURLOPT_HEADER启用时会将头文件的信息作为数据流输出。
		curl_setopt ( $ch, CURLOPT_HEADER, $CURLOPT_HEADER );// yuhou.wang

		if($header) curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );

		curl_setopt_array($ch, $opts);
		$dd['data']   = curl_exec($ch);
		$dd['err']    = curl_errno($ch);
		$dd['errmsg'] = curl_error($ch);
		curl_close($ch);

		$http_log .= msectime() - $msectime;
		$http_log .= "\n";
		//file_put_contents('LOG/http_log'.date('Ymd').'.log',$http_log,FILE_APPEND);
		
			return $dd;
	}

    /**
	 * 上传附件参数
	 * by zw
	 */
	public function getFileUp(){
		$this->ajaxReturn(array('code'=>0,'msg'=>'获取成功','data'=>file_up()));
	}

	/*
	* by zw
	* 生成串播数据
	*/
	public function createPlayIssueData($mclass = '',$mediaId,$issueDate){
		ini_set('max_execution_time', 600);//设置超时时间
		session_write_close();
        if($mclass == '01'){
            $tb_str = 'tv';
        }elseif($mclass == '02'){
            $tb_str = 'bc';
        }else{
            return false;
        }
		
		//需要读取的字段
		$fields = '1 adType,a.fid issueid,a.fissuedate,a.fsampleid,b.fadid,b.fillegaltypecode,b.fillegalcontent,b.fexpressioncodes,b.fapprno,b.fmanuno,b.fspokesman,b.fexpressions,b.fversion,c.fadowner,c.fadname,c.fbrand,c.fadclasscode,d.ffullname fadclassname,e.fname fadownername';
    	$fields2 = ',a.fendtime,a.flength,b.is_long_ad,a.fstarttime,b.favifilename';

    	$data1 = [];
    	$data2 = [];
		$where_issue['a.fstate'] = 1;//发布数据必须是有效的
		$where_issue['a.fissuedate'] = ['between',$issueDate];
		$where_issue['a.fmediaid'] = $mediaId;
		$data1 = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate[0])))
			->alias('a')
			->field($fields.$fields2)
			->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in(0,1)')
			->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
			->join('tadclass d on c.fadclasscode = d.fcode')
			->join('tadowner e on c.fadowner = e.fid')
			->where($where_issue)
			->select();

		$where_prog['fissuedate'] = ['between',$issueDate];
		$where_prog['fmediaid'] = $mediaId;
		$where_prog['a.fstate'] = 1;
		$data2 = M('t'.$tb_str.'prog')
			->alias('a')
			->field('2 adType,a.fprogid issueid,a.fissuedate,0 fsampleid,0 fadid,0 fillegaltypecode,"" fillegalcontent,"" fexpressioncodes,"" fapprno,"" fmanuno,"" fspokesman,"" fexpressions,"" fversion,"" fadowner,fcontent fadname,"" fbrand,fclasscode fadclasscode,d.fprogclass fadclassname,"" fadownername,a.fend fendtime,(unix_timestamp(a.fend)-unix_timestamp(a.fstart)) flength,0 is_long_ad,a.fstart fstarttime,"" favifilename')
			->join('tprogclass d on a.fclasscode = d.fprogclasscode')
			->where($where_prog)
			->select();
    	$resultdata = $this->adPlayListOrder($data1,$issueDate,0,99999,$data2,0);
    	if(!empty($resultdata)){
    		foreach ($resultdata as $key => $value) {
	        	if($value['adType'] == 2){
	        		if(strtotime($value['fendtime'])-strtotime($value['fstarttime'])>2){
	        			$add_prog_detail[] = [
							'fprogpid'=> $value['issueid'],
							'fmediaid'=> $mediaId,
							'fissuedate'=> date('Y-m-d',strtotime($value['fstarttime'])),
							'fcontent'=> $value['fadname'],
							'fclasscode'=> $value['fadclasscode'],
							'fstart'=> $value['fstarttime'],
							'fend'=> $value['fendtime'],
							'fstate'=> 1,
							'fidentify'=> strtotime($value['fendtime']),
						];
	        		}
	        		
	        	}
	        }
    	}

		M('t'.$tb_str.'prog_detail')->where(['fmediaid'=>$mediaId,'fissuedate'=>['between',$issueDate]])->delete();
    	if(!empty($add_prog_detail)){
    		M('t'.$tb_str.'prog_detail')->addAll($add_prog_detail);
    	}

    	D('Common/Function')->createSerialBroadcastList($mclass,$mediaId,$issueDate);//重新生成串播单
        return true;
	}

	/**
	 * 整理串播单
	 * by zw
	 */
	public function adPlayListOrder($adData = [], $issueDate = [],$limitIndex = 20,$pageSize = 1,$adData2 = [],$again = 0,$orderType = 0){
		ini_set('memory_limit','2048M');
        ini_set('max_execution_time', 600);//设置超时时间
        session_write_close();
		$adData = pxsf($adData,'fstarttime',false);
        if(!empty($adData)){
            $adList = [];
            foreach($adData as $key=>$row){
                if($key == 0){//第一条
                    $max_endtime = $row['fendtime'];
                    $interval = strtotime($row['fstarttime']) - strtotime($issueDate[0]); //与起始时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $issueDate[0],
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }elseif($key > 0 && $key <= (count($adData)-1)){
                    //两个广告时间间隔大于3秒,则创建一个空时间段
                    if($max_endtime<=$adData[$key-1]['fendtime']){
                        $max_endtime = $adData[$key-1]['fendtime'];
                    }
                   
                    $interval = strtotime($row['fstarttime']) - strtotime($max_endtime);
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $max_endtime,
                            'fendtime'      => $row['fstarttime'],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
                $row['nevtime'] = $max_endtime;
                array_push($adList,$row);
                //最后一条后的空时间段
                if($key == (count($adData)-1)){
                    $interval = strtotime($issueDate[1]) - strtotime($row['fendtime']); //与结束时间间隔大于3秒,则创建一个空时间段
                    if($interval > 3){
                        $adNull = [
                            'adType'        => 0,
                            'fadname'       => '',
                            'fadclasscode'  => 0,
                            'fstarttime'    => $row['fendtime'],
                            'fendtime'      => $issueDate[1],
                            'flength'       => $interval
                        ];
                        array_push($adList,$adNull);
                    }
                }
            }
        }else{
            $adNull = [
                'adType'        => 0,
                'fadname'       => '',
                'fadclasscode'  => 0,
                'fstarttime'    => $issueDate[0],
                'fendtime'      => $issueDate[1],
                'flength'       => $interval
            ];
            array_push($adList,$adNull);
        }
		

        //如果有节目的话
        if(!empty($adData2)){
        	$adData2 = pxsf($adData2,'fendtime',false);
        	$addData = [];
        	if(!empty($adList)){
        		foreach ($adList as $key => $value) {
		        	if(!empty($value['adType'])){//非空时段跳出本次循环
		        		continue;
		        	}else{
		        		unset($adList[$key]);
		        	}
		        	foreach ($adData2 as $key2 => $value2) {
		        		$timeContain = timeContain(strtotime($value['fstarttime']),strtotime($value['fendtime']),strtotime($value2['fstarttime']),strtotime($value2['fendtime']));
                        if(!empty($timeContain['use'])){
                            $longData = $value2;
                            $longData['fstarttime_long'] = $value2['fstarttime'];
                            $longData['fendtime_long'] = $value2['fendtime'];
                            $longData['fstarttime'] = date('Y-m-d H:i:s',$timeContain['st']);
                            $longData['fendtime'] = date('Y-m-d H:i:s',$timeContain['ed']);
                            $longData['flength'] = $timeContain['ed']-$timeContain['st'];
                            $addData[] = $longData;
                        }
		        	}
		        }
		        $adList = array_merge($adList,$addData);
		        if(empty($again)){
			        $adList = $this->adPlayListOrder($adList,$issueDate,0,0,[],1);
		        }
        	}else{
        		$adList = $adData2;
        	}
        }

        if(empty($again)){
	        $data = $adList;
        }else{
        	return $adList;
        }
		return $data;
	}

}
<?php
namespace Api\Controller;
use Think\Controller;
class ZJMediaIssueController extends Controller {

	/**
	* 接口接收的JSON参数并解码为对象
	*/
	protected $oParam;

	/**
	 * 构造函数
	 * @Param	Mixed variable
	 * @Return	void 返回的数据
	 */
	public function _initialize(){
		header('Access-Control-Allow-Origin:*');//允许所有域名跨域访问
		//请求方式为options时，不返回结果
		if(strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS'){
	        exit;
	    }
		$this->oParam = json_decode(file_get_contents('php://input'),true);
	}

	/*
	* by zw
	* 接收广告数据
	*/
	public function loopWaitTask(){
		$me = I('me');// 媒体ID
		$dt = I('dt');//日期
// 		$me = 11100010001068;
// $dt = '2019-12-20';
		$whereTask['fstatus'] = ['in',[0,10]];
		$whereTask['fissue_date'] = ['lt',date('Y-m-d')];

		if(!empty($me) && !empty($dt)){
			$whereTask['fissue_date'] = $dt;
			$whereTask['fmedia_id'] = $me;
		}
		$doTask = M('tmediaissue')
			->field('fid,fmedia_id zdmedia,fissue_date zdissue')
			->where($whereTask)
			->order('fpriority asc')
			->find();
		if(!empty($doTask)){
			$ret = $this->receMediaIssue($doTask);
			if(!empty($me) && !empty($dt)){
				$this->ajaxReturn(['code'=>0,'msg'=>'已执行','data'=>$doTask,'returnData'=>$ret]);
			}
			$saveData['fpriority'] = time();
			if(S('task_'.$doTask['fid']) != $ret['msg']){
				S('task_'.$doTask['fid'],$ret['msg'],86400);
				if($ret['code'] != 10){
					$saveData['flog'] = ['exp','CONCAT("时间：'.date('Y-m-d H:i:s').' 事件：'.$ret['msg'].'；",flog)'];
				}else{
					$saveData['flog'] = ['exp','CONCAT("时间：'.date('Y-m-d H:i:s').' 事件：'.$ret['msg'].'->",flog)'];
				}
			}
			M('tmediaissue')->where(['fid'=>$doTask['fid']])->save($saveData);
			$this->ajaxReturn(['code'=>0,'msg'=>'已执行','data'=>$doTask,'returnData'=>$ret]);
		}else{
			$this->ajaxReturn(['code'=>1,'msg'=>'无任务']);
		}
	}

	/*
	* by zw
	* 接收广告数据
	*/
	public function testreceMediaIssue(){
		session_write_close();
		set_time_limit(0);
		$me = I('me');// 媒体ID
		$dt = I('dt');

		//获取数据
		$gourl = 'http://'.C('ZJServerUrl').'/Api/ZJMediaIssue/push_mediaissue';
		$pubData['zdmedia'] = $me;
		$pubData['zdissue'] = $dt;
		$pubData['ztest'] = 1;
		$data = http($gourl,json_encode($pubData),'POST',false,0);
		dump($data);
	}

	/*
	* by zw
	* 接收广告数据
	*/
	public function receMediaIssue($postData = []){
		session_write_close();
		set_time_limit(0);
		$zdmedia = $postData['zdmedia']?$postData['zdmedia']:'';// 媒体ID
		$zdissue = $postData['zdissue']?$postData['zdissue']:'';//更新日期
// $zdmedia = 11000010002362;
// $zdissue = '2020-01-01';

		//获取数据
		$gourl = 'http://'.C('ZJServerUrl').'/Api/ZJMediaIssue/push_mediaissue';
		$pubData['zdmedia'] = $zdmedia;
		$pubData['zdissue'] = $zdissue;
		$data = json_decode(http($gourl,json_encode($pubData),'POST',false,0),true);
		
		if(empty($data['code']) && !empty($data)){
			$getData = $data['data'];
			$task = $getData['task'];//任务信息

			$issue = $getData['issue'];//接收的发布记录
			$media = $getData['media'];//接收的媒体数据
			$papersource = $getData['papersource'];//报纸素材数据
			$mediaowner = $getData['mediaowner'];//接收的媒体机构数据
			$sample = $getData['sample'];//接收的样本
			$ad = $getData['ad'];//接收的广告
			$adowner = $getData['adowner'];//接收的广告主
			$prog = $getData['prog'];//接收的节目
			$prog_detail = $getData['prog_detail'];//接收的节目片断
			$do_task = M('tmediaissue')->where(['fmedia_id'=>$task['fmedia_id'],'fissue_date'=>$task['fissue_date']])->find();

			if(empty($do_task)){
				$returnData = array('code'=>1,'msg'=>'任务不存在');
			}else{
				if($do_task['fstatus'] != 0 && $do_task['fstatus'] != 10){
					//如已处理过任务则中断
			        $where_mie['c.fbiz_main_id'] = $do_task['fid'];
			        $where_mie['d.fflow_code'] = ['in',['mediaissue_ggfl','mediaissue_ggjc']];
			        $finishcount = M('ttask')
			        	->alias('c')
			        	->field('d.fstatus')
			        	->join('ttask_flow d on c.fid = d.ftask_id')
			        	->where($where_mie)
			        	->order('d.fid desc')
			        	->find();
			        if(empty($finishcount)){
			        	$returnData = array('code'=>1,'msg'=>'任务不存在，无法接收');
			        	goto X;
			        }

			        if($finishcount['fstatus'] == 2){
			        	$returnData = array('code'=>1,'msg'=>'任务已作过处理，无法接收');
			        	goto X;
			        }
				}
				
				$mclass = substr($media['fmediaclassid'],0,2);
				if($mclass == '01'){
					$tb_issue = 'ttvissue_'.date('Y',strtotime($task['fissue_date']));
					$tb_sample = 'ttvsample';
					$tb_prog = 'ttvprog';
					$tb_prog_detail = 'ttvprog_detail';
				}elseif($mclass == '02'){
					$tb_issue = 'tbcissue_'.date('Y',strtotime($task['fissue_date']));
					$tb_sample = 'tbcsample';
					$tb_prog = 'tbcprog';
					$tb_prog_detail = 'tbcprog_detail';
				}elseif($mclass == '03' || $mclass == '04'){
					$tb_issue = 'tpaperissue_'.date('Y',strtotime($task['fissue_date']));
					$tb_sample = 'tpapersample';
				}

				//验证表是否存在
				if(empty(S('isTable_'.$tb_issue))){
					D('Common/Function','Model')->isIssueTable($tb_issue,$mclass);
					S('isTable_'.$tb_issue,'1',86400);
				}

				//广告主数据检索、添加
				if(!empty($adowner)){
					$adowner_fidentify = array_column($adowner,'fid');//获取所有远程数据的标识
					$adowner_selfdata = M('tadowner')->where(['fidentify'=>['in',$adowner_fidentify]])->select();//获取本地存在的远程数据
					if(!empty($adowner_selfdata)){
						$adowner_data = array_column($adowner_selfdata,'fid','fidentify');//获取本地和远程数据的对应关系
						$adowner_nowfidentify = array_column($adowner_selfdata,'fidentify');//获取本地存在的远程标识数据

						//获取需要同步的数据
						foreach ($adowner_selfdata as $key => $value) {
							$adowner_update[] = $value['fidentify'];//允许更新的数据
						}
					}else{
						$adowner_nowfidentify = [];
					}

					foreach ($adowner as $key => $value) {
						if(!in_array($value['fid'], $adowner_nowfidentify)){//筛选远程标识数据
							$value['fidentify'] = $value['fid'];
							unset($value['fid']);
							//添加远程数据到本地
							$addid = M('tadowner')->add($value);
							$adowner_data[$value['fidentify']] = $addid?$addid:0;//记录本地和远程数据的对应关系
						}elseif(in_array($value['fid'], $adowner_nowfidentify) && in_array($value['fid'], $adowner_update)){
							$editdata = $value;
							$editdata['fid'] = $adowner_data[$value['fid']];
							$editdata['fidentify'] = $value['fid'];
							$adowner_editdata[] = $editdata;
						}
					}
					
					if(!empty($adowner_editdata)){
						M('tadowner')->addAll($adowner_editdata,[],true);
					}
				}

				//广告数据检索、添加
				if(!empty($ad)){
					$ad_udpate = [];
					$ad_fidentify = array_column($ad,'fadid');//获取所有远程数据的标识
					$ad_selfdata = M('tad')->where(['fidentify'=>['in',$ad_fidentify]])->select();//获取本地存在的远程数据
					if(!empty($ad_selfdata)){
						$ad_data = array_column($ad_selfdata,'fadid','fidentify');//获取本地和远程数据的对应关系
						$ad_nowfidentify = array_column($ad_selfdata,'fidentify');//获取本地存在的远程标识数据

						//获取需要同步的数据
						foreach ($ad_selfdata as $key => $value) {
							if($value['fself'] == 0){
								$ad_udpate[] = $value['fidentify'];//允许更新的数据
							}
						}
					}else{
						$ad_nowfidentify = [];
					}

					foreach ($ad as $key => $value) {
						$ad[$key]['fadname'] = str_replace('公益广告（','公益（',$value['fadname']);//公益广告名称转换
						if(!empty($value['fadid'])){
							if(!in_array($value['fadid'], $ad_nowfidentify)){//筛选远程标识数据
								$value['fadowner'] = $adowner_data[$value['fadowner']]?$adowner_data[$value['fadowner']]:0;
								$value['fidentify'] = (string)$value['fadid'];
								unset($value['fadid']);
								$value['fadowner'] = $adowner_data[$value['fadowner']]?$adowner_data[$value['fadowner']]:0;//获取实际广告主
								//添加远程数据到本地
								$addid = M('tad')->add($value);
								$ad_data[(string)$value['fidentify']] = $addid?$addid:0;//新增的广告需要标识本地和远程数据的对应关系
							}elseif(in_array($value['fadid'], $ad_nowfidentify) && in_array($value['fadid'], $ad_udpate)){
								$editdata = $value;
								$editdata['fadowner'] = $adowner_data[$value['fadowner']]?$adowner_data[$value['fadowner']]:0;
								$editdata['fadid'] = $ad_data[(string)$value['fadid']];
								$editdata['fidentify'] = $value['fadid'];
								$ad_editdata[] = $editdata;
							}
						}else{
							$ad_data[(string)$value['fadid']] = 0;
						}
					}

					if(!empty($ad_editdata)){
						M('tad')->addAll($ad_editdata,[],true);
					}
				}

				//报纸素材数据检索、添加
				if(!empty($papersource) && ($mclass == '03' || $mclass == '04')){
					$source_update = [];
					$source_fidentify = array_column($papersource,'fid');//获取所有远程数据的标识
					$source_selfdata = M('tpapersource')->where(['fidentify'=>['in',$source_fidentify]])->select();//获取本地存在的远程数据
					if(!empty($source_selfdata)){
						$source_data = array_column($source_selfdata,'fid','fidentify');//获取本地和远程数据的对应关系
						$source_nowfidentify = array_column($source_selfdata,'fidentify');//获取本地存在的远程标识数据

						//获取需要同步的数据
						foreach ($source_selfdata as $key => $value) {
							$source_update[] = $value['fidentify'];//允许更新的数据
						}
					}else{
						$source_nowfidentify = [];
					}

					foreach ($papersource as $key => $value) {
						if(!in_array($value['fid'], $source_nowfidentify)){//筛选远程标识数据
							$value['fidentify'] = $value['fid'];
							unset($value['fid']);
							//添加远程数据到本地
							$addid = M('tpapersource')->add($value);
							$source_data[$value['fidentify']] = $addid?$addid:0;//记录本地和远程数据的对应关系
						}elseif(in_array($value['fid'], $source_nowfidentify) && in_array($value['fid'], $source_update)){
							$editdata = $value;
							$editdata['fid'] = $source_data[$value['fid']];
							$editdata['fidentify'] = $value['fid'];
							$source_editdata[] = $editdata;
						}
					}
					
					if(!empty($source_editdata)){
						M('tpapersource')->addAll($source_editdata,[],true);
					}
				}

				//样本数据检索、添加
				if(!empty($sample)){
					$sample_updata = [];
					$sample_fidentify = array_column($sample,'fid');//获取所有远程数据的标识
					$sample_selfdata = M($tb_sample)->where(['fidentify'=>['in',$sample_fidentify],'fstate'=>['in',[0,1]]])->select();//获取本地存在的远程数据
					if(!empty($sample_selfdata)){
						$sample_data = array_column($sample_selfdata,'fid','fidentify');//获取本地和远程数据的对应关系
						$sample_nowfidentify = array_column($sample_selfdata,'fidentify');//获取本地存在的远程标识数据

						//获取需要同步的数据
						foreach ($sample_selfdata as $key => $value) {
							if($value['fself'] == 0){
								$sample_updata[] = $value['fidentify'];//允许更新的数据
							}
						}
					}else{
						$sample_nowfidentify = [];
					}

					foreach ($sample as $key => $value) {
						//违法数据不作同步
						$value['fillegaltypecode'] = 0;
						$value['fillegalcontent'] = '';
						$value['fexpressioncodes'] = '';
						$value['fexpressions'] = '';
						$value['fconfirmations'] = '';
						if(!in_array($value['fid'], $sample_nowfidentify)){//筛选远程标识数据
							$value['fadid'] = $ad_data[(string)$value['fadid']]?$ad_data[(string)$value['fadid']]:0;//获取实际广告主
							$value['fidentify'] = $value['fid'];

							unset($value['fid']);
							//添加远程数据到本地
							$addid = M($tb_sample)->add($value);
							$sample_data[$value['fidentify']] = $addid?$addid:0;//记录本地和远程数据的对应关系
						}elseif(in_array($value['fid'], $sample_nowfidentify) && in_array($value['fid'], $sample_updata)){
							$editdata = $value;
							if($mclass == '03' || $mclass == '04'){
								$editdata['sourceid'] = $source_data[$value['sourceid']];
							}
							$editdata['fadid'] = $ad_data[(string)$value['fadid']]?$ad_data[(string)$value['fadid']]:0;//获取实际广告主
							$editdata['fid'] = $sample_data[$value['fid']];
							$editdata['fidentify'] = $value['fid'];
							$sample_editdata[] = $editdata;
						}
					}

					if(!empty($sample_editdata)){
						M($tb_sample)->addAll($sample_editdata,[],true);
					}
				}

				//广告发布数据检索、添加
				if(!empty($issue)){
					$addIssue = [];
					$where_issue['fmediaid'] = $task['fmedia_id'];
					$where_issue['fissuedate'] = $task['fissue_date'];
					$where_issue['fstate'] = 1;
					$where_issue['fself'] = 0;
					M($tb_issue)->where($where_issue)->delete();
					if($mclass == '01' || $mclass == '02'){
						$issue = pxsf($issue,'fstarttime',false);
					}

					foreach ($issue as $key => $value) {
						if((int)(strtotime($value['fendtime']) - strtotime($value['fstarttime'])) < 3){
							continue;
						}

						if(!empty($sample_data[$value['fsampleid']])){
							$value['fidentify'] = $value['fid'];
							unset($value['fid']);
							$value['fsampleid'] = $sample_data[$value['fsampleid']];

							//圆整
							$prvIssue = $addIssue[count($addIssue)-1];
							if(!empty($prvIssue)){
								$prvlength = $prvIssue['flength'];
								$npsttime = strtotime($value['fstarttime']);
								$npedtime = strtotime($prvIssue['fendtime']);
								$npdifftimes = $npsttime - $npedtime;
								if($npdifftimes<=2 && $npdifftimes>0){
									if($prvlength%5 == 0){
										$value['fstarttime'] = date('Y-m-d H:i:s',$npedtime);
										$value['flength'] = strtotime($value['fendtime']) - strtotime($value['fstarttime']);
									}else{
										$addIssue[count($addIssue)-1]['fendtime'] = date('Y-m-d H:i:s',$npsttime);
										$addIssue[count($addIssue)-1]['flength'] = strtotime($addIssue[count($addIssue)-1]['fendtime'])-strtotime($addIssue[count($addIssue)-1]['fstarttime']);
									}
								}
							}

							$addIssue[] = $value;
						}
					}
					M($tb_issue)->master(true)->addAll($addIssue);
				}

				//数据过滤
				$frList = M('tfilter')->field('fstime,fetime,fadname,fadclasscode,flength')->where(['fmediaid'=>$task['fmedia_id'],'fstatus'=>1])->select();
				if(!empty($frList)){
					$waitDeleteIssue = [];
					foreach ($frList as $key => $value) {
						$whereNI = [];
						$whereNI['a.fstate'] = 1;
						$whereNI['b.fstate'] = ['in',[0,1]];
						$whereNI['b.fstate'] = ['in',[1,2,9]];
						$whereNI['a.fmediaid'] = $task['fmedia_id'];
						$whereNI['a.fissuedate'] = $task['fissue_date'];
						if(!empty($value['fadname'])){
							$whereNI['c.fadname'] = ['like',$value['fadname']];
						}
						if(!empty($value['flength'])){
							$whereNI['a.flength'] = ['elt',$value['flength']];
						}
						if(!empty($value['fadclasscode'])){
							$codes = D('Common/Function')->get_next_tadclasscode($value['fadclasscode'],true);//获取下级广告类别代码
							$codes = $codes?$codes:[$value['fadclasscode']];
							if(!empty($codes)){
								$whereNI['c.fadclasscode'] = ['in',$codes];
							}
						}
						if(($mclass == '01' || $mclass == '02') && !empty($value['fstime']) && !empty($value['fetime'])){
							$whereNI['DATE_FORMAT(a.fstarttime,"%H:%i:%s")'] = ['between',[date('H:i:s',strtotime($value['fstime'])),date('H:i:s',strtotime($value['fetime']))]];
						}
						$newIssueList = M($tb_issue)
							->alias('a')
							->field('a.fid')
							->join($tb_sample.' b on a.fsampleid = b.fid')
							->join('tad c on b.fadid = c.fadid')
							->join('tadclass f on c.fadclasscode = f.fcode')
							->where($whereNI)
							->select();
						if(!empty($newIssueList)){
							$waitDeleteIssue = array_merge($waitDeleteIssue,array_column($newIssueList, 'fid'));
						}
					}
					//删除过滤出的数据
					if(!empty($waitDeleteIssue)){
						M($tb_issue)->where(['fid'=>['in',$waitDeleteIssue]])->delete();
					}
				}
					
				//节目数据检索、添加
				if(!empty($prog) && $mclass == '01'){
					$where_issue['fmediaid'] = $task['fmedia_id'];
					$where_issue['fissuedate'] = $task['fissue_date'];
					$where_issue['fstate'] = 1;
					$where_issue['fself'] = 0;
					M($tb_prog)->where($where_issue)->delete();
					foreach ($prog as $key => $value) {
						$prog[$key]['fidentify'] = $value['fprogid'];
						unset($prog[$key]['fprogid']);
					}
					M($tb_prog)->addAll($prog);
				}

				//节目片断数据检索、添加
				if(!empty($prog_detail) && $mclass == '01'){
					$where_issue['fmediaid'] = $task['fmedia_id'];
					$where_issue['fissuedate'] = $task['fissue_date'];
					$where_issue['fstate'] = 1;
					$where_issue['fself'] = 0;
					M($tb_prog_detail)->where($where_issue)->delete();
					foreach ($prog_detail as $key => $value) {
						$prog_detail[$key]['fidentify'] = $value['fprogid'];
						unset($prog_detail[$key]['fprogid']);
					}
					M($tb_prog_detail)->addAll($prog_detail);
				}

				$countMa = M('tmedia')->where(['fid'=>$task['fmedia_id']])->count();
				if(empty($countMa)){
					M('tmedia')->add($media);//固定不变，直接替换
				}
				$countMar = M('tmediaowner')->where(['fid'=>$media['fmediaownerid']])->count();
				if(empty($countMar)){
					M('tmediaowner')->add($mediaowner);//固定不变，直接替换
				}
				
				$this->delRepeatData($task['fmedia_id'],$task['fissue_date']);//删除重复数据
				D('Common/Function')->adIssueSendState($task['fmedia_id'],$task['fissue_date'],0,10);//更改数据发布状态
				D('Common/Function')->createSerialBroadcastList($mclass,$task['fmedia_id'],$task['fissue_date']);//整理串播单数据

				if($mclass == '01' || $mclass == '02'){
					$doProgDetail = M($tb_prog_detail)
						->field('fprogpid,min(fstart) sttime,max(fend) edtime')
						->where(['fmediaid'=>$task['fmedia_id'],'fissuedate'=>$task['fissue_date']])
						->group('fprogpid')
						->select();
					foreach ($doProgDetail as $key => $value) {
						M()->execute('update '.$tb_prog.' set fstart = "'.$value['sttime'].'",fend = "'.$value['edtime'].'" where fprogid = '.$value['fprogpid']);
					}
				}

				$task_data = [
					'fstatus' => 1,
					'fupdate_userid' => 0,
					'fupdate_user' => 'AI',
					'fupdate_time' => date('Y-m-d H:i:s')
				];

				$mediaissue_task = M('tmediaissue')->where(['fid'=>$do_task['fid']])->save($task_data);//添加业务任务
				if(!empty($mediaissue_task)){
					//获取业务信息
					$fbiz_def_code = 'zj_mediaissue';//业务编号
					$where_biz['a.fcode'] = $fbiz_def_code;
					if($mclass == '01' || $mclass == '02'){
						$fflowcode = 'mediaissue_ggjc';
					}else{
						$fflowcode = 'mediaissue_ggfl';
					}
					$where_biz['b.fflow_code'] = $fflowcode;
					$where_biz['a.fstatus'] = 1;
					$do_biz = M('tbiz_def')
						->field('a.fid,a.fname,b.fflow')
						->alias('a')
						->join('tbiz_def_flow b on a.fid = b.fbiz_def_id')
						->where($where_biz)
						->find();

					if(!empty($do_biz)){
						//判断业务任务是否存在
						$task_view = M('ttask')->where(['fbiz_def_id'=>$do_biz['fid'],'fbiz_main_id'=>$do_task['fid']])->find();
						if(empty($task_view)){
							$add_task_data = [
								'fbiz_def_id'=>$do_biz['fid'],
								'ftitle'=>$do_biz['fname'],
								'fbiz_main_id'=>$do_task['fid'],
								'fcreate_time'=>date('Y-m-d'),
								'fstatus'=>1
							];
							$task_id = M('ttask')->add($add_task_data);
						}else{
							$task_id = $task_view['fid'];
							if($task_view['fstatus'] == 5){
								M('ttask')->where(['fid'=>$task_id])->save(['fstatus'=>1]);
							}
						}
							
						if(!empty($task_id)){
							//需要新增的流程
							$add_taskflow_data = [
								'ftask_id' => $task_id,
								'fflow_code' => $fflowcode,
								'fflow' => $do_biz['fflow'],
								'fsend_task_flow_id' => 0,
								'fsend_reg' => '系统',
								'fsend' => 'AI',
								'fsend_time' => date('Y-m-d H:i:s'),
								'fstatus' => 0,
							];

							//判断业务任务流程是否存在
							$taskflow_id = M('ttask_flow')
								->where(['ftask_id'=>$task_id,'fstatus'=>0])
								->getField('fid');
							if(!empty($taskflow_id)){
								//更新上一流程
								$flow_data['frece_id'] = 0;
						        $flow_data['frece'] = 'AI';
						        $flow_data['frece_time'] = date('Y-m-d H:i:s');
						        $flow_data['fstatus'] = 2;
						        $flow_data['ffinish_type'] = 3;
								M('ttask_flow')->where(['fid'=>$taskflow_id])->save($flow_data);

								$add_taskflow_data['fsend_msg'] = '数据重推';
								$add_taskflow_data['fsend_task_flow_id'] = $taskflow_id;
							}
							
							M('ttask_flow')->add($add_taskflow_data);//添加流程操作
							
							A('Api/Function')->createPlayIssueData($mclass,$task['fmedia_id'],[$task['fissue_date'],date('Y-m-d',strtotime($task['fissue_date'])). ' 23:59:59']);//重新生成监播单
						}else{
							$returnData = array('code'=>1,'msg'=>'接收失败，任务创建失败');
						}
					}else{
						$returnData = array('code'=>1,'msg'=>'业务不存在');
					}
				}
				$returnData = array('code'=>0,'msg'=>'接收成功','data'=>$task);
			}
		}else{
			$returnData = array('code'=>$data['code']?$data['code']:1,'msg'=>$data['msg']?$data['msg']:'接收失败，未知原因');
		}
        X:
		//反馈接收状态
		// $backurl = 'http://'.C('ZJServerUrl').'/Api/ZJMediaIssue/getPushStatus';
		// $backPostData['zdmedia'] = $task['fmedia_id'];
		// $backPostData['zdissue'] = $task['fissue_date'];
		// $backPostData['returnData'] = $returnData;
		// $backData = http($backurl,json_encode($backPostData),'POST',false,0);
		// $this->ajaxReturn($returnData);
		return $returnData;
	}

	/*
	* by zw
	* 轮循删除重复数据
	*/
	public function loopdelRepeatData(){
		session_write_close();
		set_time_limit(0);
		$enddate = date('Y-m-d',strtotime('-3 day'));
		if(!empty(I('restart'))){
			S('delRepeatData',NULL);
		}
		if(!empty(S('delRepeatData'))){
			$startdate = S('delRepeatData');
		}else{
			$startdate = I('startdate')?I('startdate'):date('Y-m-01');
		}
		if($startdate>=$enddate){
			$this->ajaxReturn(array('code'=>0,'msg'=>'finish'));
		}else{
			$do_issuetask = M('tmediaissue')
				->alias('a')
				->field('a.fmedia_id,a.fissue_date')
				->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
				->where(['a.fissue_date'=>$startdate])
				->select();
			foreach ($do_issuetask as $key => $value) {
				$returnData = $this->delRepeatData($value['fmedia_id'],$value['fissue_date']);
				$do_issuetask[$key]['returnData'] = $returnData;
			}
			S('delRepeatData',date('Y-m-d',strtotime($startdate.' +1 day')),86400);
			$this->ajaxReturn(array('code'=>0,'msg'=>'next','data'=>$do_issuetask));
		}

	}

	/*
	* by zw
	* 删除重复数据
	*/
	private function delRepeatData($mediaId = 0,$issueDate = ''){
		$where_task['a.fmedia_id'] = $mediaId;
		$where_task['a.fissue_date'] = $issueDate;
		$do_task = M('tmediaissue')
			->alias('a')
			->field('b.fmediaclassid,b.media_region_id,c.flabelid,(case when instr(fmedianame,"（") > 0 then left(fmedianame,instr(fmedianame,"（") -1) else fmedianame end) as fmedianame')
			->join('tmedia b on a.fmedia_id = b.fid and b.fstate = 1')
			->join('tmedialabel c on b.fid = c.fmediaid and c.flabelid in(446,449)','left')
			->where($where_task)
			->find();
		if(!empty($do_task)){
			$mclass = substr($do_task['fmediaclassid'],0,2);
			if($mclass == '01'){
				$tb_issue = 'ttvissue_'.date('Y',strtotime($issueDate));
				$tb_sample = 'ttvsample';
				$tb_prog = 'ttvprog';
				$tb_prog_detail = 'ttvprog_detail';
			}elseif($mclass == '02'){
				$tb_issue = 'tbcissue_'.date('Y',strtotime($issueDate));
				$tb_sample = 'tbcsample';
				$tb_prog = 'tbcprog';
				$tb_prog_detail = 'tbcprog_detail';
			}elseif($mclass == '03' || $mclass == '04'){
				$tb_issue = 'tpaperissue_'.date('Y',strtotime($issueDate));
				$tb_sample = 'tpapersample';
			}

			if($mclass == '01' || $mclass == '02'){
				//广告
				$where_issue['a.fissuedate'] = $issueDate;
				$where_issue['a.fmediaid'] = $mediaId;
				$data['delIssueIds'] = [];//待删除ID
				$do_issue = M($tb_issue)
					->alias('a')
					->field('a.fid,a.fstarttime,a.fendtime,a.fself,a.fidentify,c.fadname')
					->join($tb_sample.' b on a.fsampleid = b.fid and b.fstate in(0,1)')
					->join('tad c on b.fadid = c.fadid and c.fstate in(1,2,9)')
					->join('tadclass d on c.fadclasscode = d.fcode')
					->join('tadowner e on c.fadowner = e.fid')
					->where($where_issue)
					->select();
				$noselfData = [];
				$selfData = [];
				//用户和系统的数据进行分类
				foreach ($do_issue as $key => $value) {
					if(!in_array($do_task['flabelid'], [446,449]) && ($value['fadname'] == '本媒体宣传' || $value['fadname'] == '节目预告')){//非浙江省、杭州市的 广告名为 本媒体宣传、节目预告的在接口上过滤掉
						$data['delIssueIds'][] = $value['fid'];
					}else{
						if($value['fself'] == 1){
							$selfData[] = $value;
						}else{
							$noselfData[] = $value;
						}
					}
				}
				//重复数据罗列
				foreach ($selfData as $key => $value) {
					foreach ($noselfData as $nokey => $novalue) {
						if(abs(strtotime($novalue['fstarttime'])-strtotime($value['fstarttime']))<3 && abs(strtotime($novalue['fendtime'])-strtotime($value['fendtime']))<3){
							$data['delIssueIds'][] = $novalue['fid'];
							break;
						}
					}
				}
				if(!empty($data['delIssueIds'])){
					M($tb_issue)->where(['fid'=>['in',$data['delIssueIds']]])->delete();
				}

				// //节目片断
				// $where_issue['a.fissuedate'] = $issueDate;
				// $where_issue['a.fmediaid'] = $mediaId;
				// $data['delProgDetailIds'] = [];//待删除ID
				// $do_issue = M($tb_prog_detail)
				// 	->alias('a')
				// 	->field('a.fprogid,a.fstart,a.fend,a.fself,a.fidentify')
				// 	->where($where_issue)
				// 	->select();
				// $noselfData = [];
				// $selfData = [];
				// //用户和系统的数据进行分类
				// foreach ($do_issue as $key => $value) {
				// 	if($value['fself'] == 1 && $value['fidentify'] == 0){
				// 		$selfData[] = $value;
				// 	}else{
				// 		$noselfData[] = $value;
				// 	}
				// }
				// //重复数据罗列
				// foreach ($selfData as $key => $value) {
				// 	foreach ($noselfData as $nokey => $novalue) {
				// 		if(abs(strtotime($novalue['fstart'])-strtotime($value['fend']))<3 && abs(strtotime($novalue['fstart'])-strtotime($value['fend']))<3){
				// 			$data['delProgDetailIds'][] = $novalue['fprogid'];
				// 			break;
				// 		}
				// 	}
				// }
				// if(!empty($data['delProgDetailIds'])){
				// 	M($tb_prog_detail)->where(['fprogid'=>['in',$data['delProgDetailIds']]])->delete();
				// }

				//节目
				$where_issue['a.fissuedate'] = $issueDate;
				$where_issue['a.fmediaid'] = $mediaId;
				$data['delProgIds'] = [];//待删除ID
				$do_issue = M($tb_prog)
					->alias('a')
					->field('a.fprogid,a.fstart,a.fend,a.fself,a.fidentify')
					->where($where_issue)
					->select();
				$noselfData = [];
				$selfData = [];
				//用户和系统的数据进行分类
				foreach ($do_issue as $key => $value) {
					if($value['fself'] == 1){
						$selfData[] = $value;
					}else{
						$noselfData[] = $value;
					}
				}
				//重复数据罗列
				foreach ($selfData as $key => $value) {
					foreach ($noselfData as $nokey => $novalue) {
						if(abs(strtotime($novalue['fstart'])-strtotime($value['fend']))<60 && abs(strtotime($novalue['fstart'])-strtotime($value['fend']))<60){
							$data['delProgIds'][] = $novalue['fprogid'];
							break;
						}
						if($value['fidentify'] == $novalue['fidentify']){
							$data['delProgIds'][] = $novalue['fprogid'];
						}
					}
				}
				if(!empty($data['delProgIds'])){
					M($tb_prog)->where(['fprogid'=>['in',$data['delProgIds']]])->delete();
				}
			}
		}
		return $data;
	}

	// /*补充已调整数据的任务*/
	// public function check_task(){
	// 	$sqlstr = '(
	// 		select a.fmediaid,a.fissuedate from ttvissue_2019 a
	// 		where a.fself = 1  
	// 		group by a.fmediaid,a.fissuedate
	// 		)
	// 		UNION ALL
	// 		(
	// 		select a.fmediaid,a.fissuedate from tbcissue_2019 a
	// 		where a.fself = 1  
	// 		group by a.fmediaid,a.fissuedate
	// 		)';
	// 	$data = M()->query($sqlstr);
	// 	foreach ($data as $key => $value) {
	// 		$istask = M('tmediaissue')->where(['fmedia_id'=>$value['fmediaid'],'fissue_date'=>$value['fissuedate']])->count();
	// 		if(empty($istask)){
	// 			$task_data = [
	// 				'fmedia_id' => $value['fmediaid'],
	// 				'fissue_date' => $value['fissuedate'],
	// 				'fcreate_userid' => 0,
	// 				'fcreate_user' => 'AI',
	// 				'fcreate_time' => date('Y-m-d H:i:s')
	// 			];
	// 			$mediaissue_task_id = M('tmediaissue')->add($task_data);
	// 			var_dump($mediaissue_task_id);
	// 			if(!empty($mediaissue_task_id)){
	// 				$add_task_data = [
	// 					'fbiz_def_id'=>1,
	// 					'ftitle'=>'广告监测',
	// 					'fbiz_main_id'=>$mediaissue_task_id,
	// 					'fcreate_time'=>date('Y-m-d'),
	// 					'fstatus'=>1
	// 				];
	// 				$task_id = M('ttask')->add($add_task_data);
	// 				if(!empty($task_id)){
	// 					$add_taskflow_data = [
	// 						'ftask_id' => $task_id,
	// 						'fflow_code' => 'mediaissue_ggjc',
	// 						'fflow' => '广告分离',
	// 						'fsend_task_flow_id' => 0,
	// 						'fsend_reg' => '系统',
	// 						'fsend' => 'AI',
	// 						'fsend_time' => date('Y-m-d H:i:s'),
	// 						'fstatus' => 0,
	// 					];
	// 					M('ttask_flow')->add($add_taskflow_data);
	// 				}
	// 			}
	// 		}
	// 	}
	// 	echo 'finish';
	// }

	/*
	* by zw
	* 生成违法广告数据
	*/
	public function createIllAd($mediaId,$issueDate){
		$system_num = C('FORCE_NUM_CONFIG');
		$addOldIssue = [];//待添加的案件发布记录
		$addData = [];//新线索数据
		$do_mie = M('tmedia')->field('fmediaownerid,fmediaclassid,media_region_id')->where(['fid'=>$mediaId,'fstate'=>1])->find();
		if(!empty($do_mie)){
			M('tbn_illegal_ad_issue')->where(['fmedia_id'=>$mediaId,'fissue_date'=>$issueDate])->delete();
			$mclass = substr($do_mie['fmediaclassid'],0,2);
			if($mclass == '01'){
				$tb_str = 'tv';
				$fields = 'a.fquantity,a.fstarttime,a.fendtime,a.fid,a.fsampleid,c.fadclasscode,c.fadname,d.fname adownername,d.fid adownerid,d.fregionid adownerregion,b.fexpressioncodes,b.fexpressions,b.fillegalcontent,b.favifilename';
			}elseif($mclass == '02'){
				$tb_str = 'bc';
				$fields = 'a.fquantity,a.fstarttime,a.fendtime,a.fid,a.fsampleid,c.fadclasscode,c.fadname,d.fname adownername,d.fid adownerid,d.fregionid adownerregion,b.fexpressioncodes,b.fexpressions,b.fillegalcontent,b.favifilename';
			}elseif($mclass == '03' || $mclass == '04'){
				$tb_str = 'paper';
				$fields = 'a.fquantity,a.fpage,a.fid,a.fsampleid,c.fadclasscode,c.fadname,d.fname adownername,d.fid adownerid,d.fregionid adownerregion,b.fexpressioncodes,b.fexpressions,b.fillegalcontent,b.fjpgfilename';
			}else{
				return false;
			}

			//获取媒体当天的所有发布记录
			$where_issue['b.fillegaltypecode'] = ['gt',0];
			$where_issue['a.fmediaid'] = $mediaId;
			$where_issue['a.fstate'] = 1;
			$where_issue['a.fissuedate'] = $issueDate;
			$do_issue = M('t'.$tb_str.'issue_'.date('Y',strtotime($issueDate)))
				->alias('a')
				->field($fields)
				->join('t'.$tb_str.'sample b on a.fsampleid = b.fid and b.fstate in (0,1)')
				->join('tad c on b.fadid = c.fadid')
				->join('tadowner d on c.fadowner = d.fid')
				->where($where_issue)
				->select();

			//获取当前媒体还未处理的违法案件样本ID，数组
			$oldIllIds = M('tbn_illegal_ad')
				->field('fid,fsample_id')
				->where(['fmedia_id'=>$mediaId,'fstatus'=>0,'fcustomer'=>$system_num])
				->group('fsample_id')
				->select();
			foreach ($do_issue as $key => $value) {

				//验证是否有未处理的案件，有的话将发布记录放进案件中
				$oldIllId = 0;
				foreach ($oldIllIds as $key2 => $value2) {
					if($value['fsampleid'] == $value2['fsample_id']){
						$oldIllId = $value2['fid'];
						continue;
					}
				}

				//待添加的发布记录
				$addIssue = [];
				$addIssue['fillegal_ad_id'] = $oldIllId;
				$addIssue['fmedia_id'] = $mediaId;
				$addIssue['fissue_date'] = $issueDate;
				if($mclass == '01' || $mclass == '02'){
					$addIssue['fstarttime'] = $value['fstarttime'];
					$addIssue['fendtime'] = $value['fendtime'];
				}elseif($mclass == '03' || $mclass == '04'){
					$addIssue['fpage'] = $value['fpage'];
				}
				$addIssue['fmedia_class'] = (int)$mclass;
				$addIssue['fmediaownerid'] = $do_mie['fmediaownerid'];
				$addIssue['fsend_status'] = 0;
				$addIssue['fquantity'] = $value['fquantity'];
				
				if(!empty($oldIllId)){//预存已有案件需要新增的发布记录
					$addOldIssue[] = $addIssue;
				}else{
					//需要新增的案件线索
					if(empty($addData[$value['fsampleid']])){
						$newData = [
							'fregion_id' => $do_mie['media_region_id'],
							'fmedia_class' => (int)$mclass,
							'fmedia_id' => $mediaId,
							'fad_class_code' => $value['fadclasscode'],
							'fsample_id' => $value['fsampleid'],
							'fad_name' => $value['fadname'],
							'fadowner' => $value['adownername'],
							'fadownerid' => $value['adownerid'],
							'adowner_regionid' => $value['adownerregion'],
							'fillegal_code' => $value['fexpressioncodes'],
							'fexpressions' => $value['fexpressions'],
							'fillegal' => $value['fillegalcontent'],
							'main_id' => $value['fid'],
							'fcustomer' => $system_num,
							'create_time' => date('Y-m-d H:i:s'),
							'last_syn_time' => date('Y-m-d H:i:s'),
						];
						if($mclass == '01' || $mclass == '02'){
							$newData['favifilename'] = $value['favifilename'];
							$newData['fjpgfilename'] = $value['favifilename'];
						}elseif($mclass == '03' || $mclass == '04'){
							$newData['fjpgfilename'] = $value['fjpgfilename'];
						}
						$addData[$value['fsampleid']] = $newData;
					}
					$addData[$value['fsampleid']]['issue'][] = $addIssue;
				}
				
			}

			foreach ($addData as $key => $value) {
				$waitAddIssue = $value['issue'];
				unset($value['issue']);
				$illAdId = M('tbn_illegal_ad')->add($value);
				if(!empty($illAdId)){
					$do_send = $this->illegal_ad_sendlog($illAdId);
					foreach ($waitAddIssue as $key2 => $value2) {
						$value2['fillegal_ad_id'] = $illAdId;
						$addOldIssue[] = $value2;
					}
				}
			}

			if(!empty($addOldIssue)){
				M('tbn_illegal_ad_issue')->addAll($addOldIssue);
			}
			return true;
		}else{
			return false;
		}
	}

	/*
	* 生成违法广告的派发记录
	* by zw
	*/
	public function illegal_ad_sendlog($adids = []){
		set_time_limit(0);
		session_write_close();
		if(!empty(I('s'))){
			$adids = [19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,38,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,36,37,39,72,73,74];
		}
		//获取违法广告信息
		$do_ad = M('tbn_illegal_ad')
			->field('a.fid illadid,a.fcustomer,b.fid treid,b.fname trename,a.identify,a.fmedia_id,a.fstatus')
			->alias('a')
			->join('tregulator b on concat("20",a.fcustomer) = b.fid and b.fstate = 1 and b.fkind = 1 and b.ftype = 20')
			->where(['a.fid'=>['in',$adids]])
			->select();
		foreach ($do_ad as $key => $value) {
			$add_sendarr = [];//重置存储待添加的派发记录（多条）
			$frece_reg_id = $value['treid'];//接收机构ID
			$frece_reg = $value['trename'];//接收机构
			$fsend_reg_id = 0;//下发机构ID
			$fsend_reg = 'AI';//下发机构
			while (!empty($frece_reg_id)) {
				$add_send = [];//重置存储待添加的派发记录（单条）

				//判断派发记录是否存在
				$do_send = M('tbn_case_send')
					->field('fid')
					->where(['fillegal_ad_id'=>$value['illadid'],'frece_reg_id'=>$frece_reg_id,'fsend_reg_id'=>$fsend_reg_id])
					->find();
				if(empty($do_send)){//不存在则添加派发记录
					$add_send = [
						'fillegal_ad_id'=>$value['illadid'],
						'frece_reg_id'=>$frece_reg_id,
						'frece_reg'=>$frece_reg,
						'frece_time'=>date('Y-m-d H:i:s'),
						'fsend_reg_id'=>$fsend_reg_id,
						'fsend_reg'=>$fsend_reg,
						'fsend_time'=>date('Y-m-d H:i:s'),
						'fstatus'=>0,
						'fsendtype'=>0,
					];
				}

				if($value['identify'] != 'new_2'){//监管媒体违法广告数据执行
					$fsend_reg_id = $frece_reg_id;
					$fsend_reg = $frece_reg;

					//检查媒体是否交办，如果有媒体交办记录，需要将前一条派发记录状态更新为已处理（派发）状态
					$do_grant = M('tbn_media_grant')
						->cache(true,2)
						->field('freg_id,freg')
						->where(['fgrant_reg_id'=>$fsend_reg_id,'fmedia_id'=>$value['fmedia_id'],'fcustomer'=>$value['fcustomer']])
						->find();
					if(!empty($do_grant)){
						if(!empty($add_send)){
							$add_send['fstatus'] = 20;//因交办存在，所以下级会有派发记录，设置当前派发记录为已派发
						}
						$frece_reg_id = $do_grant['freg_id'];
						$frece_reg = $do_grant['freg'];
					}else{
						$frece_reg_id = 0;//用于跳出while
					}
				}else{//本地广告主，外省媒体播出的违法广告数据执行
					$frece_reg_id = 0;//用于跳出while
				}
				

				//将要退出前，判断该记录是否又被派发到下级
				if(empty($frece_reg_id)){
					if(!empty($add_send)){
						$add_send['fstatus'] = $value['fstatus'];//下级有派发记录
					}

					//检查是否还有下派记录
					$count_send = M('tbn_case_send')
						->where(['fillegal_ad_id'=>$value['illadid'],'fsend_reg_id'=>$fsend_reg_id])
						->count();
					if(!empty($count_send)){//如果有，要把当前记录设置为已下派
						if(!empty($add_send)){
							$add_send['fstatus'] = 20;//下级有派发记录
						}else{
							M('tbn_case_send')->where(['fid'=>$do_send['fid']])->save(['fstatus'=>20]);
						}
					}
				}

				//存放派发记录
				if(!empty($add_send)){
					$add_sendarr[] = $add_send;
				}
			}

			//为避免一个数组出现多条相同ID，需要在循环内部先添完，后再继续下一轮循环，此影响效率，如不考虑会出现重复违法广告ID时可放循环外面
			if(!empty($add_sendarr)){
				M('tbn_case_send')->addall($add_sendarr);
			}
		}
		
		return true;
	}

	/*
	* by zw
	* 删除重复广告
	*/
	public function loopdelRepeatAdData(){
		$sqlstr = '
			select fadname,group_concat(fadid) allid,max(case when fidentify>0 then fadid else 0 end) oldid,max(fadid) newid
			from tad 
			where fadid<>0 and fstate >0
			group by fadname
			having count(*)>1 and count(*)<60
		';
		$adData = M()->query($sqlstr);
		foreach ($adData as $key => $value) {
			$adid_arr = [];
			$useid = $value['oldid']>0?$value['oldid']:$value['newid'];
			M()->execute('update ttvsample set fadid = '.$useid.' where fadid in('.$value['allid'].')');
			M()->execute('update tbcsample set fadid = '.$useid.' where fadid in('.$value['allid'].')');
			M()->execute('delete from tad where fadid in('.$value['allid'].') and fadid<>'.$useid);
			var_dump($useid);
		}
	}

	/*
	* by zw
	* 获取个媒体某天的视频的mp3
	*/
	public function getMediaMp3(){
		$postData = $this->oParam;
		$zdmedia = $postData['zdmedia']?$postData['zdmedia']:'';// 媒体ID
		$zdissue = $postData['zdissue']?$postData['zdissue']:'';//更新日期

		if(empty($zdmedia) || empty($zdissue)){
			$this->ajaxReturn(['code'=>1,'msg'=>'缺少必要参数']);
		}

		//获取数据
		$gourl = 'http://118.31.55.177:8002/fast/combine_mp3_v1/'.$zdmedia.'/'.$zdissue.'/';
		$pubData['zdmedia'] = $zdmedia;
		$pubData['zdissue'] = $zdissue;
		$data = json_decode(http($gourl,json_encode($pubData),'GET',false,0),true);
		if(!empty($data)){
			$data['code'] = 0;
			$this->ajaxReturn($data);
		}else{
			$data = [];
			$data['code'] = 1;
			$this->ajaxReturn($data);
		}
	}
}
<?php

//访问外部接口
function http_api($url,$headers,$params = [], $token = '',$type = 'post')
{
    $httpInfo = array();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Data');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

/*    $headers = array(
        'Content-type: application/json',
        'accessKey:'.config('admin_info.access_key'),
        'appCode:'.config('admin_info.app_code'),
        'timestamp:'.$msectime,
        'token:'.$token
    );*/
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //判断前端请求类型
    switch ($type){
        case "get" :
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            break;
        case "post":
            curl_setopt($ch, CURLOPT_POST,true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
            break;
        case "put" :
            curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
            break;
        case "delete":
            curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
            break;
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    $response = curl_exec($ch);
    if ($response === FALSE) {
        return false;
    }
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
    curl_close($ch);
    $response = json_decode($response,true);
    $result = [];
    $result['httpCode'] = $httpCode;
    $result['info'] = $response;
    return $result;
}


function http_for_net($url,$params,$heades = ['Content-Type' => 'application/x-www-form-urlencoded'],$method = HTTP_METH_POST){

    $request = new HttpRequest();
    $request->setUrl($url);
    $request->setMethod($method);
    $request->setHeaders($heades);
    $request->setPostFields($params);
    try {
        $response = $request->send();
        return $response->getBody();
    } catch (HttpException $ex) {
        return $ex;
    }
}

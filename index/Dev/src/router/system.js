const system = [{
    path: '/logManagement',
    name: 'logManagement',
    component: () => import('../components/system/logManagement'),
    meta: ['系统管理', '日志管理']
  },
  {
    path: '/levelManagement',
    name: 'levelManagement',
    component: () => import('../components/system/levelManagement'),
    meta: ['系统管理', '机构管理']
  },
  {
    path: '/bulletin',
    name: 'bulletin',
    component: () => import('../components/system/Bulletin/index'),
    meta: ['系统管理', '公告管理']
  },
  {
    path: '/addBulletin',
    name: 'addBulletin',
    component: () => import('../components/system/Bulletin/bulletinCon/addBulletin'),
    meta: ['系统管理', '添加公告']
  },
  {
    path: '/bulletinCon',
    name: 'bulletinCon',
    component: () => import('../components/system/Bulletin/bulletinCon/bulletinCon'),
    meta: ['系统管理', '查看公告']
  },
  {
    path: '/smsmanage',
    name: 'smsmanage',
    component: () => import('../components/system/smsmanage'),
    meta: ['系统管理', '发送短信']
  },
  {
    path: '/documentmodelset',
    name: 'documentmodelset',
    component: () => import('../components/system/documentmodelset'),
    meta: ['系统管理', '文书模板']
  },
  {
    path: '/gdatasend',
    name: 'gdatasend',
    component: () => import('../components/system/gdatasend'),
    meta: ['系统管理', '发布数据']
  },
  {
    path: '/inspectiontask',
    name: 'inspectiontask',
    component: () => import('../components/system/inspectiontask'),
    meta: ['系统管理', '检查计划']
  },
  {
    path: '/ginspectresult3',
    name: 'ginspectresult3',
    component: () => import('../components/system/ginspectresult3'),
    meta: ['系统管理', '第三方数据复核']
  },
  {
    path: '/inspectiontask3',
    name: 'inspectiontask3',
    component: () => import('../components/system/inspectiontask3'),
    meta: []
  },
  {
    path: '/inspectiontask2',
    name: 'inspectiontask2',
    component: () => import('../components/system/inspectiontask2'),
    meta: ['系统管理', '数据复核']
  },
  {
    path: '/screenlive',
    name: 'screenlive',
    component: () => import('../components/system/screenlive'),
    meta: ['系统管理', '多屏演示']
  }
]

export default system

const report = [{
    path: '/monitorReport',
    name: 'monitorReport',
    component: () => import('../components/report/monitorReport'),
    meta: ['数据报告', '监测报告']
  }
]
export default report

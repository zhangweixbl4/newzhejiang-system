const Ggsjzx = [{
    path: '/jchuizong',
    name: 'jchuizong',
    component: () => import('../components/Ggsjzx/aggregation/jchuizong'),
    meta: ['广告监测汇总情况', '']
  },
  {
    path: '/cluefx',
    name: 'cluefx',
    component: () => import('../components/Ggsjzx/aggregation/cluefx'),
    meta: ['线索分析',]
  },
  // {
  //   path: '/wfchachu',
  //   name: 'wfchachu',
  //   component: () => import('../components/Ggsjzx/aggregation/wfchachu'),
  //   meta: ['广告监测汇总情况', '']
  // },
  {
    path: '/regionRank',
    name: 'regionRank',
    component: () => import('../components/Ggsjzx/aggregation/wfchachu'),
    meta: ['数据统计', '地域排名']
  },
  {
    path: '/adCategoryRank',
    name: 'adCategoryRank',
    component: () => import('../components/Ggsjzx/aggregation/wfchachu'),
    meta: ['数据统计', '广告类别排名']
  },
  {
    path: '/mediaRank',
    name: 'mediaRank',
    component: () => import('../components/Ggsjzx/aggregation/wfchachu'),
    meta: ['数据统计', '媒体排名']
  },
  {
    path: '/mediaOrgRank',
    name: 'mediaOrgRank',
    component: () => import('../components/Ggsjzx/aggregation/wfchachu'),
    meta: ['数据统计', '媒介机构排名']
  },
  {
    path: '/diagram',
    name: 'diagram',
    component: () => import('../components/Ggsjzx/aggregation/diagram'),
    meta: ['广告监测汇总情况', '广告刊播分布图']
  },
  {
    path: '/sbchuli',
    name: 'sbchuli',
    component: () => import('../components/Ggsjzx/Investigation/sbchuli'),
    meta: ['查办处理情况', '处理结果上报']
  },
  {
    path: '/ginspectcheck',
    name: 'ginspectcheck',
    component: () => import('../components/Ggsjzx/Investigation/ginspectcheck'),
    meta: ['数据复核']
  },
  {
    path: '/shsjjc',
    name: 'shsjjc',
    component: () => import('../components/Ggsjzx/Investigation/shsjjc'),
    meta: ['查办处理情况', '数据检查']
  },
  {
    path: '/sjjcah',
    name: 'sjjcah',
    component: () => import('../components/Ggsjzx/Investigation/sjjcah'),
    meta: ['查办处理情况', '数据检查']
  },
  {
    path: '/historicalstatistics',
    name: 'historicalstatistics',
    component: () => import('../components/Ggsjzx/Investigation/historicalstatistics'),
    meta: ['查办处理情况', '历史统计']
  },
  {
    path: '/shsjxq',
    name: 'shsjxq',
    component: () => import('../components/Ggsjzx/Investigation/shsjxq'),
    meta: ['查办处理情况', '检查详情']
  },
  {
    path: '/sjjcahxq',
    name: 'sjjcahxq',
    component: () => import('../components/Ggsjzx/Investigation/sjjcahxq'),
    meta: ['查办处理情况', '检查详情']
  },
  {
    path: '/jcjgah',
    name: 'jcjgah',
    component: () => import('../components/Ggsjzx/Investigation/jcjgah'),
    meta: ['查办处理情况', '检查结果']
  },
  {
    path: '/shadissue',
    name: 'shadissue',
    component: () => import('../components/Ggsjzx/Investigation/shadissue'),
    meta: ['查办处理情况', '广告发布']
  },
  {
    path: '/shadsample',
    name: 'shadsample',
    component: () => import('../components/Ggsjzx/Investigation/shadsample'),
    meta: ['查办处理情况', '样本管理']
  },
  {
    path: '/shadcut',
    name: 'shadcut',
    component: () => import('../components/Ggsjzx/Investigation/shadcut'),
    meta: ['查办处理情况', '片段截取']
  },
  {
    path: '/ginspectresult',
    name: 'ginspectresult',
    component: () => import('../components/Ggsjzx/Investigation/ginspectresult'),
    meta: ['查办处理情况', '检查结果']
  },{
    path: '/lajatj',
    name: 'lajatj',
    component: () => import('../components/Ggsjzx/aggregation/lajatj'),
    meta: ['查办处理情况', '线索统计']
  },
  {
    path: '/ginspectresult2',
    name: 'ginspectresult2',
    component: () => import('../components/Ggsjzx/Investigation/ginspectresult2'),
    meta: ['数据复核']
  },
  {
    path: '/gdatainspect',
    name: 'gdatainspect',
    component: () => import('../components/Ggsjzx/Investigation/gdatainspect'),
    meta: ['查办处理情况', '数据检查']
  },
  {
    path: '/filladlist',
    name: 'filladlist',
    component: () => import('../components/Ggsjzx/Investigation/filladlist'),
    meta: ['查办处理情况', '违法广告线索审核']
  },
  {
    path: '/ckchuli',
    name: 'ckchuli',
    component: () => import('../components/Ggsjzx/Investigation/ckchuli'),
    meta: ['查办处理情况', '违法广告清单']
  },
  {
    path: '/wfhuizong',
    name: 'wfhuizong',
    component: () => import('../components/Ggsjzx/Investigation/wfhuizong'),
    meta: ['查办处理情况', '违法广告汇总']
  },
  {
    path: '/ckwfmingxi',
    name: 'ckwfmingxi',
    component: () => import('../components/Ggsjzx/Investigation/ckwfmingxi'),
    meta: ['查办处理情况', '违法广告明细']
  },
  {
    path: '/lachuli',
    name: 'lachuli',
    component: () => import('../components/Ggsjzx/Investigation/lachuli'),
    meta: ['查办处理情况', '立案调查处理']
  },
  {
    path: '/jbananjian',
    name: 'jbananjian',
    component: () => import('../components/Ggsjzx/Investigation/jbananjian'),
    meta: ['查办处理情况', '交办线索']
  },
  {
    path: '/sjfuhe',
    name: 'sjfuhe',
    component: () => import('../components/Ggsjzx/Investigation/sjfuhe'),
    meta: ['数据复核', '数据复核 ']
  },
  {
    path: '/sjfuhejg',
    name: 'sjfuhejg',
    component: () => import('../components/Ggsjzx/Investigation/sjfuhejg'),
    meta: ['数据复核', '复核结果']
  },
  {
    path: '/zdychaxun',
    name: 'zdychaxun',
    component: () => import('../components/Ggsjzx/statistics/zdychaxun'),
    meta: ['统计分析情况', '自定义查询']
  },
  {
    path: '/ggmcchaxun',
    name: 'ggmcchaxun',
    component: () => import('../components/Ggsjzx/statistics/ggmcchaxun'),
    meta: ['统计分析情况', '广告名称查询']
  },
  {
    path: '/jbanjian',
    name: 'jbanjian',
    component: () => import('../components/Ggsjzx/case/jbanjian'),
    meta: ['重点线索', '线索交办']
  },
  {
    path: '/lbanjian',
    name: 'lbanjian',
    component: () => import('../components/Ggsjzx/case/lbanjian'),
    meta: ['重点线索', '线索列表']
  },
  {
    path: '/anjiancgx',
    name: 'anjiancgx',
    component: () => import('../components/Ggsjzx/case/anjiancgx'),
    meta: ['重点线索', '线索草稿箱']
  },
  {
    path: '/sbzdanjian',
    name: 'sbzdanjian',
    component: () => import('../components/Ggsjzx/case/sbzdanjian'),
    meta: ['重点线索', '处理结果上报']
  },
  {
    path: '/kdyanjian',
    name: 'kdyanjian',
    component: () => import('../components/Ggsjzx/case/kdyanjian'),
    meta: ['重点线索', '跨地域线索派发列表']
  },
  {
    path: '/kdyjsanjian',
    name: 'kdyjsanjian',
    component: () => import('../components/Ggsjzx/case/kdyjsanjian'),
    meta: ['重点线索', '跨地域处理结果上报']
  },
  {
    path: '/ajxsxq',
    name: 'ajxsxq',
    component: () => import('../components/Ggsjzx/case/ajxsxq'),
    meta: ['重点线索', '线索详情']
  },
  {
    path: '/tbqingkuang',
    name: 'tbqingkuang',
    component: () => import('../components/Ggsjzx/monitoringSituation/tbqingkuang'),
    meta: ['监测情况通报', ' 监测情况通报']
  },
  {
    path: '/tbqingkuanglb',
    name: 'tbqingkuanglb',
    component: () => import('../components/Ggsjzx/monitoringSituation/tbqingkuanglb'),
    meta: ['监测情况通报', '监测情况通报列表']
  },
  {
    path: '/fsxxjiaoliu',
    name: 'fsxxjiaoliu',
    component: () => import('../components/Ggsjzx/dynamic/fsxxjiaoliu'),
    meta: ['监管动态交流', '发送信息']
  },
  {
    path: '/xxlbjiaoliu',
    name: 'xxlbjiaoliu',
    component: () => import('../components/Ggsjzx/dynamic/xxlbjiaoliu'),
    meta: ['监管动态交流', '信息列表']
  },
  {
    path: '/glyonghu',
    name: 'glyonghu',
    component: () => import('../components/Ggsjzx/account/glyonghu'),
    meta: ['系统管理', '用户管理']
  }
]
export default Ggsjzx

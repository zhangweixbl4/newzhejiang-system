const monitoringdata = [
  {
    path: '/monitoringData',
    name: 'monitoringData',
    component: () => import('../components/monitoringdata/monitoringData'),
    meta: ['监测数据', '广告明细 ']
  },
  {
    path: '/illmonitoringData',
    name: 'illmonitoringData',
    component: () => import('../components/monitoringdata/monitoringData'),
    meta: ['监测数据', '违法广告明细 ']
  },
  {
    path: '/superviseResult',
    name: 'superviseResult',
    component: () => import('../components/monitoringdata/superviseResult'),
    meta: ['监测数据', '监管处置 ']
  },
  {
    path: '/monitoringDatazb',
    name: 'monitoringDatazb',
    component: () => import('../components/monitoringdata/monitoringDatazb'),
    meta: ['监测数据', '广告明细（非监测）']
  },
  {
    path: '/monitoringData2',
    name: 'monitoringData2',
    component: () => import('../components/monitoringdata/monitoringData2'),
    meta: ['监测数据', '广告明细']
  },
  {
    path: '/fzbnetdata',
    name: 'fzbnetdata',
    component: () => import('../components/monitoringdata/fzbnetdata'),
    meta: ['监测数据', '移动互联网数据 ']
  },
  {
    path: '/monitoringDataZS',
    name: 'monitoringDataZS',
    component: () => import('../components/monitoringdata/monitoringDataZS'),
    meta: ['监测数据', '15-17年违法明细']
  },
  {
    path: '/warning',
    name: 'warning',
    component: () => import('../components/monitoringdata/warning'),
    meta: ['监测数据', '监测预警 ']
  },
  {
    path: '/sampledetails',
    name: 'sampledetails',
    component: () => import('../components/monitoringdata/sampledetails'),
    meta: ['监测数据', '版本明细']
  },
  {
    path: '/gmonitoring',
    name: 'gmonitoring',
    component: () => import('../components/monitoringdata/gmonitoring'),
    meta: ['监测数据', '广告串播单']
  },
  {
    path: '/monitoringMaterial',
    name: 'monitoringMaterial',
    component: () => import('../components/monitoringdata/monitoringMaterial'),
    meta: ['监测数据', '监测素材']
  },
  {
    path: '/publishlist',
    name: 'publishlist',
    component: () => import('../components/monitoringdata/publishlist'),
    meta: ['系统管理', '发布清单']
  },
  {
    path: '/adinquire',
    name: 'adinquire',
    component: () => import('../components/monitoringdata/adinquire'),
    meta: ['广告查询']
  }
]
export default monitoringdata

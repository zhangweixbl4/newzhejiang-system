const review = [{
    path: '/clueApplication',
    name: 'clueApplication',
    component: () => import('../components/review/clueApplication'),
    meta: ['数据复核', '复核台账']
  },
  {
    path: '/clueTask',
    name: 'clueTask',
    component: () => import('../components/review/clueTask'),
    meta: ['数据复核', '复核任务']
  },
  {
    path: '/clueReview',
    name: 'clueReview',
    component: () => import('../components/review/clueReview'),
    meta: ['数据复核', '复核数据']
  },
  {
    path: '/fjsjfuhe',
    name: 'fjsjfuhe',
    component: () => import('../components/review/fjsjfuhe'),
    meta: ['数据复核', '复核数据']
  },
  {
    path: '/fjFuhetz',
    name: 'fjFuhetz',
    component: () => import('../components/review/fjFuhetz'),
    meta: ['线索复核', '复核台账']
  },
  {
    path: '/fjFuhecl',
    name: 'fjFuhecl',
    component: () => import('../components/review/fjFuhecl'),
    meta: ['线索复核', '复核处理']
  },
  {
    path: '/fjFuhe',
    name: 'fjFuhe',
    component: () => import('../components/review/fjFuhe'),
    meta: ['线索复核', '复核数据']
  }
]

export default review
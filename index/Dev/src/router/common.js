const common = [{
    path: '/home',
    name: 'home',
    component: () => import('../components/Home'),
    meta: ['首页']
  },
  {
    path: '/ghome',
    name: 'ghome',
    component: () => import('../components/ghome'),
    meta: ['首页']
  },
  {
    path: '/medialist',
    name: 'medialist',
    component: () => import('../components/medialist/medialist'),
    meta: ['媒体列表']
  },
  {
    path: '/feedback',
    name: 'feedback',
    component: () => import('../components/FeedBack'),
    meta: ['在线留言']
  },
  {
    path: '/commonquestion',
    name: 'commonquestion',
    component: () => import('../components/commonquestion'),
    meta: ['常见问题']
  }
]
export default common

import Vue from 'vue'
import store from '../store/store'
import Router from 'vue-router'
import { Message } from 'element-ui'
import common from './common'
import report from './report'
import ggjcFJ from './ggjcFJ'
import clueFJ from './clueFJ'
import clue from './clue'
import review from './review'
import leagal from './leagal'
import monitoringdata from './monitoringdata'
import system from './system'
import Ggsjzx from './Ggsjzx'
import credit from './credit'
import analysis from './analysis'
import evidence from './evidence'
Vue.use(Router)

let router = new Router({
  routes: [{
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../components/Login')
    },
    {
      path: '/loginjump',
      name: 'loginjump',
      component: () => import('../components/loginJump')
    },
    {
      path: '/review',
      name: 'review',
      component: () => import('../components/common/review'),
    },
    {
      path: '/manage',
      name: '',
      component: () => import('../components/Manage'),
      children: [
        ...common,
        ...report,
        ...ggjcFJ,
        ...clueFJ,
        ...clue,
        ...review,
        ...leagal,
        ...monitoringdata,
        ...system,
        ...Ggsjzx,
        ...credit,
        ...analysis,
        ...evidence
      ]
    }
  ]
})
const numMenu = ['home','ghome','sbchuli','lachuli','sjfuhe','tbqingkuanglb','xxlbjiaoliu','ginspectresult2', 'clueTask']
// 开发环境不做限制
  router.beforeEach((to, from, next) => {
    let menu = store.state.menu
    menu.forEach(i => {
      if (i.menu_url === to.name) {
        store.commit('setActiveMenu', i)
      } else {
        if (i.list && i.list.length) {
          i.list.forEach(v => {
            if (v.menu_url === to.name) {
              store.commit('setActiveMenu', i)
            }
          })
        }
      }
    })
    if (process.env.NODE_ENV !== 'development') {
      //有角标的菜单重新请求角标
      if (numMenu.includes(to.name)) {
        let param = (to.name === 'home' || to.name === 'ghome') ? to.name : `${to.name}|1`
        store.dispatch('_getDB',param)
      }
      let MenuPermission = JSON.parse(localStorage.getItem('MenuPermission')) || ['login', 'loginjump']
      if (MenuPermission.includes(to.name)) {
        // 正常情况下页面跳转
        if (to.name === 'login' && from.name !== 'login' && from.name !== 'loginjump' && !to.params.jump) {
          next({
            name: from.name
          })
        } else {
          next()
        }
      } else if (!MenuPermission.includes(to.name) && from.name === null) {
        // 当从新的浏览器标签页直接粘贴无权限的系统地址时
        localStorage.setItem('MenuPermission', JSON.stringify(['login', 'loginjump']))
        next({
          name: to.name === 'loginjump' ? 'loginjump' : 'login'
        })
      } else {
        Message.error('无菜单权限，请联系管理员')
        next({
          name: from.name
        })
      }
    }else{
      next()
    }
  })


export default router

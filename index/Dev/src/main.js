// The Vue build version to load with the `import` command (runtime-only or
// standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import func from '../static/js/func'
import dayjs from 'dayjs'
require('../static/css/common.css')

import VueClipboards from 'vue-clipboards'

import './assets/element-ui.scss'

// 引入v-viewer
import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
// 引入图片懒加载插件
import VueLazyload from 'vue-lazyload'
//element-ui引入
import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
// import '../theme/index.css'
import '@/assets/theme/index.css'

import bTitle from '@/component/bTitle'

import 'vue-mark-calendar/lib/vue-mark-calendar.css'
Vue.use(VueLazyload, {
  preLoad: 1.3, //预加载的宽高比
  error: '../static/img/zw.png', //图片加载失败时使用的图片源
  loading: '../static/img/zw.png', //图片加载的路径
  attempt: 1 //尝试加载次数
})
Vue.use(ElementUI, {
  size: 'mini'
})
Vue.use(Viewer)

Vue.component('b-title', bTitle)

Vue.use(VueClipboards)

const _str = process.env.BASE_URL
Vue.prototype.$_ = _str
Vue.prototype.$url = 'adlife.com.cn'
Vue.prototype.$func = func
Vue.prototype.$dayjs = dayjs
Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})

const pntype = [{
  label: '日报',
  value: '11',
  img: './static/img/day.jpg'
}, {
  label: '周报',
  value: '12',
  img: './static/img/week.jpg'
}, {
  label: '半月报',
  value: '13',
  img: './static/img/month.jpg'
},{
  label: '月报',
  value: '14',
  img: './static/img/month.jpg'
}, {
  label: '专项报告',
  value: '15',
  img: './static/img/month.jpg'
}, {
  label: '其他',
  value: '20',
  img: './static/img/month.jpg'
}]

const pntype2 = {
  '11': '日报',
  '12': '周报',
  '13': '半月报',
  '14': '月报',
  '15': '专项报告',
  '16': '季报',
  '17': '半年报',
  '18': '年报',
  '19': '预警报告',
  '20': '其他'
}

const noticeType = {
  '1': {
    label: '安全',
    type: 'success'
  },
  '2': {
    label: '通知',
    type: 'primary'
  },
  '3': {
    label: '升级',
    type: 'warning'
  },
}
const lookType = {
  '0': {
    label: '全部用户',
  },
  '1': {
    label: '市监用户',
  },
  '3': {
    label: '媒体用户',
  },
}

const pnfiletype = [{
  label: 'WORD',
  value: '10',
  img: './static/img/word.jpg'
}, {
  label: 'EXCEL',
  value: '30',
  img: './static/img/excel.jpg'
}]

const pnfiletype2 = {
  '10': {
    label: 'WORD',
    img: './static/img/word.jpg'
  },
  '30': {
    label: 'EXCEL',
    img: './static/img/excel.jpg'
  }
}

export {
  pntype,
  pntype2,
  pnfiletype,
  pnfiletype2,
  noticeType,
  lookType
}

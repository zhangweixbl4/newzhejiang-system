// 第一层级：页面路由 wfchachu线索汇总，jchuizong检测汇总
// 第二层级： 是否是国家局 isgj 0/1
// 第三层级： 是否是显示国家局 is_show_sj 0/1
// 第四层级： 用户地域等级 flevel 0:总局，1省级，2副省，3计划单列市，4市，5区县
const jchuizong = {
  '0': '各地区',
  '1': '各省',
  '2': '各副省',
  '3': '各计划单列市',
  '6': '各省会(非监测)',
}
const levelchild = {
  'wfchachu': {
    '1': {
      '0': {
        '1': {
          '0': '各地区',
          '1': '本级',
          '2': '副省级',
          '3': '计划单列市',
        },
        '2': {
          '0': '各地区',
          '2': '本级',
        },
        '3': {
          '0': '各地区',
          '3': '本级',
        },
        '4': {
          '0': '各地区',
          '4': '本级',
        },
        '5': {
          '0': '各地区',
          '5': '本级',
        },
      },
      '1': {
        '1': {
          '0': '各地区',
          '1': '本级',
          '2': '副省级',
          '3': '计划单列市',
        },
        '2': {
          '0': '各地区',
          '2': '本级',
        },
        '3': {
          '0': '各地区',
          '3': '本级',
        },
        '4': {
          '0': '各地区',
          '4': '本级',
        },
        '5': {
          '0': '各地区',
          '5': '本级',
        },
      }
    },
    '0': {
      '0': {
        '0': {
          '0': '各地区',
          '4': '各市',
          '5': '各区县',
          '1': '各省',
        },
        '1': {
          '0': '各地区',
          '1': '本级',
          '4': '各市',
          '5': '各区县',
        },
        '2': {
          '0': '各地区',
          '5': '各区县',
          '2': '本级',
        },
        '3': {
          '0': '各地区',
          '5': '各区县',
          '3': '本级',
        },
        '4': {
          '0': '各地区',
          '5': '各区县',
          '4': '本级',
        },
        '5': {
          '5': '',
        },
      },
      '1': {
        '1': {
          '0': '各地区',
          '4': '各市',
          '5': '各区县',
          '1': '本级',
        },
        '2': {
          '0': '各地区',
          '5': '各区县',
          '1': '省级',
          '2': '本级',
        },
        '3': {
          '0': '各地区',
          '5': '各区县',
          '1': '省级',
          '3': '本级',
        },
        '4': {
          '0': '各地区',
          '5': '各区县',
          '1': '省级',
          '4': '本级',
        },
        '5': {
          '0': '各地区',
          '5': '本级',
          '4': '市级',
        },
      }
    }
  },
  'jchuizong': {
    '1': {
      '0': {
        '0': jchuizong,
        '1': jchuizong,
        '2': jchuizong,
        '3': jchuizong,
        '4': jchuizong,
        '5': jchuizong,
      },
      '1': {
        '0': jchuizong,
        '1': jchuizong,
        '2': jchuizong,
        '3': jchuizong,
        '4': jchuizong,
        '5': jchuizong,
      }
    },
    '0': {
      '0': {
        '0': jchuizong,
        '1': jchuizong,
        '2': jchuizong,
        '3': jchuizong,
        '4': jchuizong,
        '5': jchuizong,
      },
      '1': {
        '0': jchuizong,
        '1': jchuizong,
        '2': jchuizong,
        '3': jchuizong,
        '4': jchuizong,
        '5': jchuizong,
      }
    }
  },
  'zhejiang': {
    '0': '各地区',
    '-1': '本级',
    '1': '省级',
    '4': '市级',
    '41': '普通市',
    '42': '副省级',
    '43': '计划单列市',
    '5': '区县级',
  },
  'zhejiang1': {
   '20': {
      '0': '各地区',
      '1': '省级',
      '4': '市级',
      '5': '区县级',
    },
    '10': {
      '0': '各地区',
      '4': '市级',
      '5': '区县级',
    },
    '0': {
      '5': '区县级',
    }
  },
}

const clueFrom = [{
  label: '广告监测',
  value: '10'
}, {
  label: '网络或书面投申诉举报',
  value: '20'
}, {
  label: '上级机关交办',
  value: '30'
}, {
  label: '其他机关移送',
  value: '40'
}]
const clueFrom2 = {
  '20': '网络或书面投申诉举报',
  '30': '上级机关交办',
  '40': '其他机关移送',
}
const fregulatorlevel = {
  30: '国家',
  20: '本省',
  10: '本市',
  0: '本区县',
  1: '',
}

const propsOptions = {
  value: 'fcode',
  label: 'fadclass',
  children: 'children',
  checkStrictly: true,
}

const propsOptions2 = {
  value: 'fcode',
  label: 'fadclass',
  children: 'children',
  checkStrictly: true,
  multiple: true,
}

const areaOptions = {
  value: 'fid',
  label: 'fname',
  children: 'children',
  checkStrictly: true,
}

const czfs = {
  '10': '自办',
  '20': '移交',
  '30': '转送',
  '40': '处置',
}
const selectData = [{
    value: "10",
    label: "媒体分类"
  },
  {
    value: "20",
    label: "广告类别"
  },
  {
    value: "30",
    label: "广告名称"
  },
  {
    value: "40",
    label: "发布媒体"
  },
  {
    value: "50",
    label: "违法原因"
  }
]
const sfbj = {
  '0': '全部',
  '10': '已办结',
  '20': '未办结'
}

const rwzt = {
  '10': '未定',
  '20': '一般',
  '30': '紧急'
}

const xsly = {
  '10': '广告监测',
  '20': '网络或书面投申诉举报',
  '30': '上级机关交办',
  '40': '其他机关移送',
}

const gzlx = {
  '0': {
    label: '督办',
    type: 'info'
  },
  '10': {
    label: '催办',
    type: 'warning'
  },
  '20': {
    label: '执法监督',
    type: 'danger'
  },
}

const wfcd = {
  '10': '一般违法',
  '30': '严重违法',
}

const yjlx = {
  '10': '违法预警'
}
const czzt = {
  '0': '草稿箱',
  '20': '已交办',
  '30': '已上报',
  '40': '查证中',
  '41': '重新查证中',
  '42': '完成查证',
}
const timeDate = [
  {
    starttime: '00:00:00',
    endtime: '03:59:59',
    start: '0',
    end: ' 4 时'
  },
  {
    starttime: '04:00:00',
    endtime: '07:59:59',
    start: '4',
    end: ' 8 时'
  },
  {
    starttime: '08:00:00',
    endtime: '09:59:59',
    start: '8',
    end: '10时'
  },
  {
    starttime: '10:00:00',
    endtime: '11:59:59',
    start: '10',
    end: '12时'
  },
  {
    starttime: '12:00:00',
    endtime: '13:59:59',
    start: '12',
    end: '14时'
  },
  {
    starttime: '14:00:00',
    endtime: '15:59:59',
    start: '14',
    end: '16时'
  },
  {
    starttime: '16:00:00',
    endtime: '17:59:59',
    start: '16',
    end: '18时'
  },
  {
    starttime: '18:00:00',
    endtime: '18:59:59',
    start: '18',
    end: '19时'
  },
  {
    starttime: '19:00:00',
    endtime: '19:59:59',
    start: '19',
    end: '20时'
  },
  {
    starttime: '20:00:00',
    endtime: '21:59:59',
    start: '20',
    end: '22时'
  },
  {
    starttime: '22:00:00',
    endtime: '23:59:59',
    start: '22',
    end: '24时'
  },
]
export {
  levelchild,
  clueFrom,
  clueFrom2,
  fregulatorlevel,
  propsOptions,
  propsOptions2,
  areaOptions,
  czfs,
  selectData,
  sfbj,
  rwzt,
  xsly,
  gzlx,
  wfcd,
  yjlx,
  czzt,
  timeDate
}

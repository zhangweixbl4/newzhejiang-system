const netadtype = {
  '1': 'PC端',
  '2': '移动端',
  '9': '微信端',
}

const allmediatype = {
  '': '全部',
  'tv': '电视',
  '1': '电视',
  '01': '电视',
  'bc': '广播',
  '2': '广播',
  '02': '广播',
  'paper': '报纸',
  '3': '报纸',
  '03': '报纸',
  '4': '期刊',
  '04': '期刊',
  'net': '互联网',
  '13': '互联网',
  'od': '户外',
  '05': '户外',
}
const typeMap = {
  '01': 'tv',
  '1': 'tv',
  'tv': 'tv',
  '02': 'bc',
  '2': 'bc',
  'bc': 'bc',
  '03': 'paper',
  '3': 'paper',
  'paper': 'paper',
  '13': 'net',
  'net': 'net',
  '05': 'outdoor',
  'od': 'outdoor',
}

export {
  netadtype,
  allmediatype,
  typeMap
}

import axios from 'axios'
import qs from 'qs'
import store from './store'
import func from '../../static/js/func'

const _str = process.env.BASE_URL

const actions = {
  _userMenujurisdiction({
    commit
  }, name) {
    axios.post(_str + '/Agp/Core/user_menujurisdiction')
      .then(res => {
        if (res.data.code === 0) {
          let data = res.data
          res.data.name = name
          commit('_userMenujurisdiction', data)
        } else {
          func._error(res.data)
        }
      })
  },
  _getDB({
    commit,state
  }, route) {
    let path = ''
    let permission = JSON.parse(localStorage.getItem('MenuPermission')) || []
    if (route) {
      path = route
    } else if (permission.includes('home')) {
      path = 'home'
    } else if (permission.includes('ghome')) {
      path = 'ghome'
    } else {
      return false
    }
    if (state.firstGetTask) {
      path = ''
      commit('changeTaskState',false)
    }
    let data = {
      gettypes: path
    }
    axios.post(_str + '/Agp/Index/getwaittask', qs.stringify(data))
      .then(req => {
        commit('_getDB', {
          data: req.data.data || [],
          route: path
        })
      })
  },
  _getTregion({ commit }) {
    axios.post(_str + '/Agp/Core/get_tregion')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getTregion', res.data)
        }
      })
  },
  _getTregion2({
    commit
  }) {
    axios.post(_str + '/Agp/Core/get_tregion2')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getTregion2', res.data)
        }
      })
  },
  _getTregion3({
    commit
  }) {
    axios.post(_str + '/Agp/Core/get_tregion3')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getTregion3', res.data)
        }
      })
  },
  _tregionList({
    commit
  }) {
    axios.post(_str + '/Agp/Core/tregionList')
      .then(res => {
        if (res.data.code === 0) {
          commit('_tregionList', res.data)
        }
      })
  },
  _getAllClass({
    commit
  }) {
    axios.post(_str + '/Agp/Core/get_all_adclass_list')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getAllClass', res.data)
        }
      })
  },
  _getExpression({
    commit
  }) {
    axios.post(_str + '/Api/Function/getTillegal')
      .then(res => {
        commit('_getExpression', res.data)
      })
  },
  _getTitle({
    commit
  }) {
    axios.post(_str + '/Agp/Core/get_webname')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getTitle', res.data)
        }
      })
  },
  _getMediatregion({
    commit
  }) {
    axios.post(_str + '/Agp/Core/get_mediatregion')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getMediatregion', res.data)
        }
      })
  },
  _getMediatregion2({
    commit
  }) {
    axios.post(_str + '/Agp/Core/get_mediatregion2')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getMediatregion2', res.data)
        }
      })
  },
  _getMediatregion3({
    commit
  }) {
    axios.post(_str + '/Agp/Core/get_mediatregion3')
      .then(res => {
        if (res.data.code === 0) {
          commit('_getMediatregion3', res.data)
        }
      })
  },
}

export default actions

import Vue from 'vue'
import router from '../router'
import store from './store'
import { Tree } from '../../static/js/Tree'
const mediaMap = {
  '01': {
    label: '电视',
    value: 'tv',
    value3: '1',
    value5: '01',
  },
  '02': {
    label: '广播',
    value: 'bc',
    value3: '2',
    value5: '02',
  },
  '03': {
    label: '报纸',
    value: 'paper',
    value3: '3',
    value5: '03',
  },
  '04': {
    label: '期刊',
    // value: 'bc',
    value3: '4',
    value5: '04',
  },
  '05': {
    label: '户外',
    value: 'od',
    value3: '05',
    value5: '05',
  },
  '13': {
    label: '互联网',
    value: 'net',
    value3: '13',
    value5: '13',
  },
}
const getMediaType = (arr = []) => {
  const mediaType = []
  const mediaType3 = []
  const mediaType5 = []
  mediaType.push({value: '',label: '全部'})
  arr.forEach(i => {
    mediaType.push({
      value: mediaMap[i].value,
      label: mediaMap[i].label
    })

    mediaType3.push({
      value: mediaMap[i].value3,
      label: mediaMap[i].label
    })

    mediaType5.push({
      value: mediaMap[i].value5,
      label: mediaMap[i].label
    })
  })
  return {
    mediaType,
    mediaType3,
    mediaType5,
  }
}
const mutations = {
  pushTimeDate(state, val) {
    state.timeDate.push(val)
  },
  changeTaskState(state, value = false) {
    state.firstGetTask = value
  },
  showIncludeSub(state, isshow) {
    state.showIncludeSub = isshow
  },
  _getTitle(state, res) {
    if (!res.logowz) {
      state.showError = true
    }
    state.Gtitle = res.data
    state.loginwz = res.loginwz
    state.logowz = res.logowz
    state.isgj = Number(res.system_num) === 100000 ? 1 : 0
    state.fjxsgl = res.fjxsgl
    state.loginurl = res.loginurl || ''
    state.loginurl2 = res.loginurl2 || ''
    state.tdata = res.tdata || {}
    state.title = res.data
    document.title = state.title
  },
  _setFJiscontain(state, value) {
    state.FJiscontain = value
  },
  _setSvalue(state, obj) {
    state.Gsearch.years = obj.years
    state.Gsearch.timetypes = obj.timetypes
    state.Gsearch.timeval = obj.timeval
  },
  _setFormValue(state, obj) {
    state.Gsresult.years = obj.years
    state.Gsresult.timetypes = obj.timetypes
    state.Gsresult.timeval = obj.timeval
    state.Gsresult.fisxinyong = obj.fisxinyong
  },
  _setSearchTabForm(state, obj) {
    state.tabSearchData = {...obj}
  },
  _setTabActive(state, num) {
    state.tabActive = num
  },
  _setstime(state, obj) {
    state.selection = obj
  },
  _setselection(state, obj) {
    state.selection[obj.key] = obj.value
  },
  _setisstime(state, obj) {
    state[obj.key] = obj.value
  },
  // _setselectionData(state, obj) {
  //   state.selectionData = obj
  // },
  // 设置汇总时间查询条件数据
  _setGData(state, obj) {
    state.Gtime = obj.Gtime
    state.Adata = obj.Adata
  },
  _setGtime(state, obj) {
    state.Gtime[obj.name] = obj.value
  },
  _userMenujurisdiction(state, res) {
    if (res.userdata.system_num === 310000) {
      document.querySelector('#chatBtn').remove()
    }
    state.isgj = Number(res.userdata.system_num) === 100000 ? 1 : 0
    state.userInfo = res.userdata
    state.menudata = res.menudata
    const { mediaType, mediaType3, mediaType5 } = getMediaType(res.userdata.media_class)
    // mediaType.unshift({value: 'all', label: '全部'})
    state.mediaType = mediaType
    state.mediaType3 = mediaType3
    state.mediaType5 = mediaType5
    if(!state.userInfo.is_show_tj) state.userInfo.is_show_tj = 0
    if(!state.userInfo.is_show_sj) state.userInfo.is_show_sj = 0
    if (res.userdata.media_class.includes('13')) {
      state.havenet = true
    } else {
      state.havenet = false
    }
    // 开发环境不做判断
    if (process.env.NODE_ENV !== 'development') {
      let userphone = res.userdata.userphone.split('_')
      const regex = new RegExp(/^1[0-9]{10}$/, 'gi')
      state.safePWD = Number(userphone[0]) > 2 ? true : false
      state.trueNUM = regex.test(userphone[1])
    }
    let v = res.data
    // 菜单为空返回登陆页
    if (!v.length) {
      router.push({
        path: 'login'
      })
      return false
    }
    v.map(item => {
      if (state.isgj && !Number(res.userdata.role)) {
        // 国家局平台非管理员用户过滤用户管理菜单
        if (item.menu_code === 'xtgl') {
          item.list = item.list.filter(c => {
            return c.menu_url !== 'glyonghu'
          })
        }
      } else if (state.isgj && ((res.userdata.fregulatorlevel + 10) === res.userdata.tregionlevel)) {
        // 国家局屏蔽线索汇总菜单
        if (item.menu_id === '226') {
          item.list = item.list.filter(c => {
            return c.menu_id !== '235'
          })
        }
      }
      if (item.menu_url === res.name) {
        state.activeMenu = item
      } else {
        if (item.list && item.list.length) {
          item.list.forEach(c => {
            if (c.menu_url === res.name) {
              state.activeMenu = item
            }
          })
        }
      }
      return item
    })
    if (!Number(res.userdata.role)) {
      let MenuPermission = JSON.parse(localStorage.getItem(`MenuPermission`)).filter(item => {
        return item !== 'glyonghu'
      })
      localStorage.setItem('MenuPermission', JSON.stringify(MenuPermission))
    }
    state.menu = v.filter(i => {
      return !(i.menu_id === '233' && (!i.list || !i.list.length))
    })
    state.cmenu = JSON.stringify(state.menu)
    if (res.name === 'home') {
      if (v[0]['list'] && v[0]['list']['length']) {
        router.push(v[0]['list'][0]['menu_url'])
      } else {
        router.push(v[0]['menu_url'])
      }
    }
    if (!state.dbData.length) {
      store.dispatch('_getDB','ghome')
    }
  },
  _getTregion(state, res) {
    let rid = res.data[0]['fid']
    let pid = ''
    let list = res.data.filter(item => {
      if (item.fid === rid) {
        pid = item.fpid
      }
      return item.fid === rid || item.fpid === rid
    })
    state.allArea = new Tree(list, 'fpid', 'fid').init(pid)
    state.areaList = (res.data.length > 1) ? new Tree(res.data, 'fpid', 'fid').init(rid) : res.data
    state.areaData = res.data
    let a = state.allArea
    if (a.fid === "100000") {
      a.fname = '全国'
    }
    let b = a['children']
    if (b) {
      delete a.children
      state.allArea2 = Array.of(a).concat(b)
    } else {
      state.allArea2 = state.allArea
    }
  },
  _getDB(state, res) {
    if (!state.cmenu) return
    let menu = JSON.parse(state.cmenu)
    let data = res.data
    state.dbData = data
    state.ActiveTab = {}
    data.forEach(item => {
      Vue.set(state.ActiveTab, `${item.furl.replace(/\|/,'')}`, item.ftaskcount)
    })
    data.forEach(item => {
      menu.map(v => {
        if (v.menu_code === item.fmenucode) {
          if (v.taskcount) {
            v.taskcount = Number(v.taskcount) + Number(item.ftaskcount)
          } else {
            v.taskcount = item.ftaskcount
          }
        } else if (v.menu_code !== item.fmenucode && v.list && v.list.length) {
          v.list.map(s => {
            if (s.menu_code === item.fmenucode) {
              if (s.taskcount) {
                s.taskcount = Number(s.taskcount) + Number(item.ftaskcount)
              } else {
                s.taskcount = item.ftaskcount
              }
              if (!v.taskcount) {
                v.taskcount = s.taskcount
              }
            }
            return s
          })
        }
        return v
      })
    })
    state.menu = menu
  },
  _getTregion2(state, res) {
    state.GList = new Tree(res.data, 'fpid', 'fid').init(res.data[0].fpid)
  },
  _getTregion3(state, res) {
    state.GData = new Tree(res.data, 'fpid', 'fid').init(res.data[0].fpid)
  },
  _tregionList(state, res) {
    if (res.data.length) {
      let pid = res.data[0].fpid
      state.xinxiTree = new Tree(res.data, 'fpid', 'fid').init(pid)
      state.xinxiList = res.data
    }
  },
  _getAllClass(state, res) {
    state.adClassOptions = new Tree(res.list, 'fpcode', 'fcode').init('');
    state.Option = {
      fadclass: "全部",
      fcode: "",
      fpcode: "",
    }
    state.Option = Array.of(state.Option).concat(state.adClassOptions[0], state.adClassOptions[1], state.adClassOptions[2], state.adClassOptions[4], state.adClassOptions[5])
    state.allAdClass = res.list
  },
  _getExpression(state, res) {
    state.IllegalList = res
  },
  _getMediatregion(state, res) {
    state.areaArr = res.data
    let arr = []
    if (res.data.length > 1) {
      arr = new Tree(res.data, 'fpid', 'fid').init(res.data[0]['fpid'])
    } else {
      arr = res.data
    }
    if (arr.length === 1 && arr[0].children) {
      let a = arr[0]
      let b = a['children']
      delete a.children
      state.mediaArea = Array.of(a).concat(b)
    } else {
      state.mediaArea = arr
    }
  },
  _getMediatregion2(state, res) {
    if (res.data.length > 1) {
      state.wfqdarea = new Tree(res.data, 'fpid', 'fid').init(res.data[0]['fpid'])
    } else {
      state.wfqdarea = res.data
    }
  },
  _getMediatregion3(state, res) {
    if (res.data.length > 1) {
      state.wfqdarea2 = new Tree(res.data, 'fpid', 'fid').init(res.data[0]['fpid'])
    } else {
      state.wfqdarea2 = res.data
    }
  },
  _bloading(state, val) {
    state.bloading = val
  },
  setGTableHeight(state,height) {
    state.GTableHeight = document.body.clientHeight - 40 - height
  },
  setActiveMenu(state, menu) {
    state.activeMenu = menu
  },
}

export default mutations

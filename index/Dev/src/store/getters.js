const getters = {
  activeMenu: state => state.activeMenu,
  typeMap: state => state.typeMap,
}
export default getters
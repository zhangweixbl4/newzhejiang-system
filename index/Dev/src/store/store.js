import Vue from 'vue'
import Vuex from 'vuex'
import ueditorConfig from './module/ueditorConfig'
import month from './module/month'
import mutations from './mutations'
import actions from './actions'
import dayjs from 'dayjs'
import getters from './getters'
import {
  levelchild,
  clueFrom,
  clueFrom2,
  fregulatorlevel,
  propsOptions,
  propsOptions2,
  areaOptions,
  czfs,
  selectData,
  sfbj,
  rwzt,
  xsly,
  gzlx,
  wfcd,
  yjlx,
  czzt,
  timeDate
} from './module/options'
import {
  pntype,
  pntype2,
  noticeType,
  lookType,
  pnfiletype,
  pnfiletype2
} from './module/report'
import {
  netadtype,
  allmediatype,
  typeMap
} from './module/mediatype'
import {
  Gtime,
  Adata
} from './module/condition'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    firstGetTask: true,
    userInfo: {
      fregulatorlevel: -1,
      thirdlink: []
    },
    showIncludeSub: true,
    bloading: false,
    leagalOptions: [{"fcode":"0","fillegaltype":"\u4e0d\u8fdd\u6cd5"},{"fcode":"30","fillegaltype":"\u8fdd\u6cd5"}],
    adClassOptions: [],
    allAdClass: [{
      children: []
    }],
    IllegalList: [],
    allArea: [{
      children: []
    }],
    areaList: [{
      children: []
    }],
    allArea2: [{
      fid: ''
    }],
    areaArr: [],
    GList: [],
    GData: [],
    wfqdarea: [],
    wfqdarea2: [],
    xinxiTree: [],
    xinxiList: [],
    isG: false,
    fregulatorlevel: fregulatorlevel,
    propsOptions: propsOptions,
    propsOptions2: propsOptions2,
    areaOptions: areaOptions,
    mediaType: [],
    mediaType3: [],
    mediaType5: [],
    netadtype: netadtype,
    allmediatype: allmediatype,
    typeMap: typeMap,
    levelchild: levelchild,
    clueFrom: clueFrom,
    clueFrom2: clueFrom2,
    pntype: pntype,
    pntype2: pntype2,
    noticeType: noticeType,
    lookType: lookType,
    pnfiletype: pnfiletype,
    pnfiletype2: pnfiletype2,
    month: month,
    czfs: czfs,
    timeDate: timeDate,
    selectData: selectData,
    menudata: [],
    menu: [{
      menu_id: '',
      menu_url: "",
      list: []
    }],
    cmenu: '',
    Gsearch: {
      years: '0',
      timetypes: 30,
      timeval: new Date().getMonth() + 1
    },
    Gsresult: {
      years: new Date().getFullYear(),
      timetypes: '30',
      timeval: new Date().getMonth() + 1,
    },
    Gtime: Gtime,
    Adata: Adata,
    Gtitle: '',
    loginwz: '',
    logowz: '',
    dbData: [],
    ActiveTab:{},
    tdata: {},
    fjxsgl: '',
    mediaArea: [{
      fid: ''
    }],
    selection: {
      year: dayjs().subtract(1, 'months').startOf('month').format('YYYY'),
      times_table: 'tbn_ad_summary_month',
      table_condition: dayjs().subtract(1, 'months').startOf('month').format('MM-DD'),
      is_show_longad: '1',
      isfixeddata: '0',
      is_show_fsend: '2',
      is_include_sub: '1',
      datatype: '1',
    },
    tabSearchData: {
      regionlevel: '0',
      regionid: [],
      iscontain: true,
      mclass: '',
      mediaid: [],
      fadclasscode: [],
      dimension: '1',
      districtsOrder: true,
      fisxinyong: ['1'],
    },
    tabActive: '1',
    FJisstime: [],
    Gisstime: [],
    FJiscontain: '',
    havenet: false,
    ueditorConfig: ueditorConfig,
    fwidth: {
      width: '99%'
    },
    isgj: '',
    sfbj: sfbj,
    rwzt: rwzt,
    xsly: xsly,
    gzlx: gzlx,
    wfcd: wfcd,
    yjlx: yjlx,
    czzt: czzt,
    // logo图片
    loginurl: '',
    loginurl2: '',
    showError: false,
    safePWD: true, //密码安全登记符合要求
    trueNUM: true, //正确的手机号
    GTableHeight: null,
    activeMenu: {},
  },
  mutations,
  actions,
  getters
})

export default store

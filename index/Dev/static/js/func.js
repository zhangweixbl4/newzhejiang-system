import axios from 'axios'
import qs from 'qs'
import router from '../../src/router/index'
import dayjs from 'dayjs'
import {
  Message
} from 'element-ui'


const _str = process.env.BASE_URL
const quarterType = {
  '1': '-01-01',
  '2': '-04-01',
  '3': '-07-01',
  '4': '-10-01',
}

const halfYear = {
  '01-01': '06-30',
  '07-01': '12-31'
}

const func = {
  /**
   * 验证手机号
   * @param {String} number 手机号
   */
  _verifyPhone(number) {
    const regex = new RegExp(/^1[0-9]{10}$/, 'gi')
    return regex.test(number)
  },
  /**
   * 从对象数组中获取某个值的数组
   * @param {Array} list 源数据
   * @param {String} name 处理字段名
   */
  getIdArr(list, name) {
    let arr = []
    list.forEach(item => {
      arr.push(item[name])
    })
    return arr
  },
  /**
   * 验证手机号是否符合要求，6-16位，包含数字与字母
   * @param {String} pwd 手机号
   */
  _verifyPwd(pwd) {
    const regex = new RegExp(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/)
    return regex.test(pwd)
  },
  /**
   * 校验两次密码是否相同
   * @param {String} pwd 密码
   * @param {String} npwd 密码
   */
  _verifySamePwd(pwd, npwd) {
    return pwd === npwd
  },
  /**
   * 获取上月第一天与最后一天
   * @param {String} format 格式化格式 'YYYY-MM-DD 00:00:00'
   */
  getPreDate(format) {
    return [dayjs().subtract(1, 'months').startOf('months').format(format),dayjs().subtract(1, 'months').endOf('months').format(format)]
  },
   /**
   * 获取当月第一天与最后一天
   * @param {String} format 格式化格式 'YYYY-MM-DD 00:00:00'
   */
  getNowDate(format) {
    return [dayjs().startOf('months').format(format),dayjs().endOf('months').format(format)]
  },
  /**
   * 过滤报告类型
   * @param {Array} arr 报告类型数组
   * @param {Object} vue vue实例
   */
  setRType(arr, vue) {
    let obj = {}
    arr.forEach(item => {
      obj[item] = vue.$store.state.pntype2[item]
    })
    return obj
  },
  /**
   * 标识案件已查看
   * @param {Object} data 包括url(请求地址),name(参数名),value(参数值)
   */
  isWatch(data) {
    let obj = {}
    obj[data.name] = data.value
    axios.post(_str + data.url, qs.stringify(obj))
  },
  clearAllCookie() {
    let keys = document.cookie.match(/[^ =;]+(?=\=)/g)
    if(keys) {
      for (let i = keys.length; i--;)
        document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString()
    }
  },
  /**
   * 统一报错处理
   * @param {Object || String} res 传入请求的返回结果或错误提醒文字
   */
  _error(res) {
    let data = {}
    if (res) {
      if (typeof res === 'object') {
        data = res
      } else {
        data = {
          msg: res
        }
      }
    } else {
      data = {
        msg: '网络错误'
      }
    }
    if (res && res.code === 401) {
      Message({
        type: 'error',
        message: `${data.msg}`
      })
      axios.post(_str + '/Agp/Core/quietlogin')
        .then(req => {
          router.push({
            'name': 'login',
            params: {
              jump: true
            }
          })
        })
    } else {
      if (data.msg !== '网络错误') {
        Message({
          type: 'error',
          message: data.msg
        })
      }
    }
  },
  /**
   * 统一成功处理
   * @param {Object || String} res 传入请求的返回结果或自定义文字
   */
  success(res) {
    let data = {}
    if (res) {
      if (typeof res === 'object') {
        data = res
      } else {
        data = {
          msg: res
        }
      }
    } else {
      data = {
        msg: '操作成功'
      }
    }
    if (data.msg !== '网络错误') {
      Message({
        type: 'success',
        message: data.msg
      })
    }
  },
  /**
   * 拼接两个时间
   * @param {String} format 格式化后的日期格式 'yyyy 年 MM 月 dd 日'
   * @param {Array} datearr 时间数组,
   * @param {String} joinString 连接两个时间的符号
   */
  handleDate(format, datearr, joinString) {
    let start = dayjs(new Date(datearr[0])).format(format)
    let end = dayjs(new Date(datearr[1] || datearr[0]), ).format(format)
    let year = start.substring(0, 6)
    let _start = start.substring(7)
    let _end = end.substring(7)
    return _start === _end ? `${year} ${_start}` : `${year} ${_start} ${joinString} ${_end}`
  },
  /**
   * 报告处理函数
   * @param {String} type 报告类型
   * @param {String || Array} time 时间，根据报告类型不同数据类型不同
   */
  setDuringTime(type, time) {
    let data = null
    switch (type) {
      case '20':
        data = {
          s_time: dayjs(time).subtract(1, 'days').format('YYYY-MM-DD'),
          e_time: dayjs(time).add(5, 'days').format('YYYY-MM-DD'),
        }
        break
      case '25':
        let date = dayjs(time).format('DD')
        if (date === '01') {
          data = {
            s_time: time,
            e_time: dayjs(time).format('YYYY-MM-') + '15',
          }
        } else {
          data = {
            s_time: time,
            e_time: dayjs(time).endOf('month').format('YYYY-MM-DD'),
          }
        }
        break
      case '30':
        data = {
          s_time: dayjs(time).startOf('month').format('YYYY-MM-DD'),
          e_time: dayjs(time).endOf('month').format('YYYY-MM-DD'),
        }
        break
      case '40':
        data = {
          s_time: time[0],
          e_time: time[1],
        }
        break
      case '70':
        data = {
          s_time: `${time[0]}${quarterType[time[1]]}`,
          e_time: dayjs(time[0] + quarterType[time[1]]).add(2, 'months').endOf('month').format('YYYY-MM-DD'),
        }
        break
      case '75':
        let year = dayjs(time).format('YYYY')
        let stime = dayjs(time).format('MM-DD')
        data = {
          s_time: time,
          e_time: `${year}-${halfYear[stime]}`
        }
        break
      case '80':
        data = {
          s_time: `${time}-01-01`,
          e_time: `${time}-12-31`,
        }
        break
    }
    return data
  },
  /**
   * 生成某个时间段内的 m3u8 播放地址
   * @param {string} channel 媒体id
   * @param {string,number} start 开始时间（unix 时间戳）
   * @param {string,number} end 结束时间（unix 时间戳）
   */
  setUrl(channel,start,end){
    return `http://az.hz-data.xyz:8087/channel/${channel}/playback.m3u8?start_time=${Number(start) / 1000}&end_time=${Number(end) / 1000}`
  },
  /**
   * 根据 m3u8 区间与该区间内的断流区间，生成不断流的区间数组
   * @param {array} arr m3u8 断流区间数组
   * @param {string, number} fstarttime m3u8 区间的开始时间（2019-01-01 11:11:11 或 js 时间戳）
   * @param {string, number} fendtime  m3u8 区间的结束时间（2019-01-01 11:11:11 或 js 时间戳）
   * @param {string, number} channel 媒体id
   */
  getM3u8List(arr = [], fstarttime, fendtime, channel){
    let result = []
    if(arr.length === 0){
      result.push({
        starttime: dayjs(fstarttime).format('HH:mm:ss'),
        endtime: dayjs(fendtime).format('HH:mm:ss'),
        start: fstarttime,
        end: fendtime,
        url: func.setUrl(channel,fstarttime,fendtime)
      })
      return result
    }else if(arr.length === 1){
      result.push({
        starttime: dayjs(fstarttime).format('HH:mm:ss'),
        endtime: dayjs(arr[0].start).format('HH:mm:ss'),
        start: fstarttime,
        end: arr[0].start,
        url: func.setUrl(channel,fstarttime,arr[0].start)
      },{
        starttime: dayjs(arr[0].end).format('HH:mm:ss'),
        endtime: dayjs(fendtime).format('HH:mm:ss'),
        start: arr[0].end,
        end: fendtime,
        url: func.setUrl(channel,arr[0].end,fendtime)
      })
      return result
    }
    arr.forEach((item, i) => {
      if(i === 0){
        if(fstarttime !== item.start){
          result.push({
            starttime: dayjs(fstarttime).format('HH:mm:ss'),
            endtime: dayjs(item.start).format('HH:mm:ss'),
            start: fstarttime,
            end: item.start,
            url: func.setUrl(channel,fstarttime,item.start)
          })
        }
      }else if(i === (arr.length - 1)){
        result.push({
          starttime: dayjs(arr[i - 1].end).format('HH:mm:ss'),
          endtime: dayjs(item.start).format('HH:mm:ss'),
          start: arr[i - 1].end,
          end: item.start,
          url: func.setUrl(channel,arr[i - 1].end,item.start)
        })
        if(fendtime !== item.end){
          result.push({
            starttime: dayjs(item.end).format('HH:mm:ss'),
            endtime: dayjs(fendtime).format('HH:mm:ss'),
            start: item.end,
            end: fendtime,
            url: func.setUrl(channel,item.end,fendtime)
          })
        }
      }else{
        if(item.end !== arr[i + 1].start){
          result.push({
            starttime: dayjs(arr[i - 1].end).format('HH:mm:ss'),
            endtime: dayjs(item.start).format('HH:mm:ss'),
            start: arr[i - 1].end,
            end: item.start,
            url: func.setUrl(channel,arr[i - 1].end,item.start)
          })
        }
      }
    })
    return result
  },
  /**
   * 将过长的秒数秒转化为分秒或时分秒
   * @param {string,number} s 秒数
   */
  dealTime(s){
    let time = Number(s)
    if(time <= 60){
      return `${time}''`
    }
    if(time > 60 && time <=3600){
      let m = Math.floor(time/60)
      let s = time % 60
      return `${m}'${s}''`
    }
    if(time > 3600){
      let h = Math.floor(time / 3600)
      let m = Math.floor((time % 3600) / 60)
      let s = (time % 3600) % 60
      return `${h}'${m}'${s}''`
    }
  },
  /**
   * 获取 m3u8 地址中，视频最早开始播放的时间点
   * @param {string} url m3u8 地址
   */
  getM3u8SartTime(url) {
    const matchReg = /http.*?\.ts/gi
    const matchReg1 = /http.*?\.aac/gi
    return new Promise((resolve,reject) => {
      axios.get(url).then(res => {
        if (res.data.length > 91) {
          const a = data.match(matchReg) != null ? data.match(matchReg)[0] : data.match(matchReg1)[0]
          const arr = a.split('/')
          resolve({
            Stime: Number(arr[arr.length - 1].substring(0, 10)) * 1000,
            abtime: url.substr(75, 10) - Number(arr1[arr1.length - 1].substring(0, 10))
          })
        }
      })
    })
  },
  /**
   * 计算预计开始时间与实际 m3u8 开始时间 的时间差
   * @param {string} date m3u8 区间的开始日期
   * @param {*} starttime 根据 getM3u8SartTime 获取到的 m3u8 的开始时间
   */
  getAbsTime(date, starttime) {
    let s = new Date(date + ' ' + '00:00:00').getTime()
    let e = starttime
    return (s - e) / 1000
  },
  /**
   * 返回 time 所在 m3u8 的 index
   * @param {number} time js 时间戳
   * @param {array} arr 源数组
   */
  getUrlIndex(time, arr){
    let _index = 0
    arr.forEach((i,index) => {
      if(time >= i.start && time < i.end){
        _index = index
      }
    })
    return _index
  },
  /**
     * 将字符串类型的 adclasscode 转为数组类型
     * @param {string || number} code 类型值
     */
  getAdClassCode(code) {
    let _code = code.toString()
    let length = _code.length
    let result = []
    for (var i = 2; i <= length; i += 2) {
      result.push(_code.substr(0, i));
    }
    return result
  }
}

export default func

export {
  _error
}

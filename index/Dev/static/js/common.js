import axios from 'axios'
import qs from 'qs'

export function _getClueList(obj, arr,mclass) {
  if (arr === null) {
    obj.clueList = []
    return false
  }
  axios.post(obj.$_ + '/Agp/Fxsdengji/get_tbn_illegal_ad_details',qs.stringify({fillegaladid: arr,mclass:mclass}))
  .then(res => {
    if(res.data.code === 0){
      obj.clueList = res.data.data.list
      let arr = []
      res.data.data.list.forEach(item => {
        arr.push(item.fadname)
      })
      if (!obj.registrationForm.fregistercontents) {
        obj.registrationForm.fregistercontents = arr.join(',')
        obj.registrationForm.fcluename = arr.join(',')
      }
    }
  })
}

export function _getClueList2(obj, arr,mclass) {
  if (arr === null) {
    obj.clueList = []
    return false
  }
  axios.post(obj.$_ + '/Agp/Ffjdengji/get_tbn_illegal_ad_details',qs.stringify({fillegaladid: arr,mclass:mclass}))
  .then(res => {
    if(res.data.code === 0){
      obj.clueList = res.data.data.list

      let arr = []
      let arr2 = []
      let arr3 = []
      let arr4 = []
      res.data.data.list.forEach(item => {
        arr.push(item.fadname)
        arr2.push(item.fmedianame)
        arr3.push(item.fillegalcontent)
        arr4.push(item.fexpressions)
      })

      if (!obj.registrationForm.fregistercontents) {
        obj.registrationForm.fcluename = arr.join(',')
        obj.registrationForm.freportedpeople = arr2.join(',')
        obj.registrationForm.fappeal = arr3.join('\n')
        obj.registrationForm.fregistercontents = arr4.join('\n')
      }
    }
  })
}
// 南京
export function _getClueListnj(obj, arr) {
  if (arr === null) {
    obj.clueList = []
    return false
  }
  axios.post(obj.$_ + '/Agp/Ntsjubao/get_tbn_illegal_ad_details',qs.stringify({fillegaladid: arr}))
  .then(res => {
    if(res.data.code === 0){
      obj.clueList = res.data.data.list
    }
  })
}
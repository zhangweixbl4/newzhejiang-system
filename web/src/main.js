import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'

import dayjs from 'dayjs'
import Notify from '@/utils/notify.js'

import '@/assets/base.scss'

import fetch from '@/utils/fetch.js'
import { getToken, setToken, removeToken, setC, getC, removeC } from '@/utils/cookie.js'

import './directive'
import '@/permission' // permission control

import func from '@/utils/func.js'

import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
Vue.use(Viewer)

Vue.config.productionTip = false

Vue.prototype.$Notify = Notify
Vue.prototype.$dayjs = dayjs
Vue.prototype.$fetch = fetch
Vue.prototype.$getToken = getToken
Vue.prototype.$setToken = setToken
Vue.prototype.$removeToken = removeToken
Vue.prototype.$setC = setC
Vue.prototype.$getC = getC
Vue.prototype.$removeC = removeC
Vue.prototype.$func = func
// 设置 table 高度
Vue.prototype.getTH = (vue, height = 194) => {
  let formHeight = vue.$refs.searchForm ? vue.$refs.searchForm.$el.clientHeight : 0
  return document.body.clientHeight - formHeight - height + 'px'
}


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

import Vue from 'vue'
import Router from 'vue-router'
import layout from '@/views/layout'

Vue.use(Router)

export const asyncRoutes = [
  {
    name: 'Home',
    path: '/home',
    component: layout,
    meta: { title: '首页', icon: 'iconfont icon-home' },
    children: [
      {
        name: 'home',
        path: '/home',
        component: () => import('@/views/Home'),
        meta: { title: '首页', icon: 'iconfont icon-home' }
      },
    ]
  },
  {
    name: 'Detach',
    path: '/Detach/tv',
    component: layout,
    meta: { title: '广告分离', icon: 'iconfont icon-fenli' },
    children: [
      {
        name: 'tvdetach',
        path: '/Detach/tv',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '电视分离' },
      },
      {
        name: 'bcdetach',
        path: '/Detach/bc',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '广播分离' },
      },
      {
        name: 'paperdetach',
        path: '/Detach/paper',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '报纸分离' },
      },
      {
        name: 'journaldetach',
        path: '/Detach/journal',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '期刊分离' },
      },
    ]
  },
  {
    name: 'Monitor',
    path: '/Monitor/tv',
    component: layout,
    meta: { title: '广告监测', icon: 'iconfont icon-zhudongjiance' },
    children: [
      {
        name: 'tvmonitor',
        path: '/Monitor/tv',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '电视监测' },
      },
      {
        name: 'bcmonitor',
        path: '/Monitor/bc',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '广播监测' },
      },
      {
        name: 'papermonitor',
        path: '/Monitor/paper',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '报纸监测' },
      },
      {
        name: 'journalmonitor',
        path: '/Monitor/journal',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '期刊监测' },
      },
    ]
  },
  {
    name: 'Recheck',
    path: '/Recheck/tv',
    component: layout,
    meta: { title: '广告复审', icon: 'iconfont icon-shenhe2' },
    children: [
      {
        name: 'tvrecheck',
        path: '/Recheck/tv',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '电视复审' },
      },
      {
        name: 'bcrecheck',
        path: '/Recheck/bc',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '广播复审' },
      },
      {
        name: 'paperrecheck',
        path: '/Recheck/paper',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '报纸复审' },
      },
      {
        name: 'journalrecheck',
        path: '/Recheck/journal',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '期刊复审' },
      },
    ]
  },
  {
    name: 'Judgment',
    path: '/Judgment/tv',
    component: layout,
    meta: { title: '广告终审', icon: 'iconfont icon-weibiaoti525' },
    children: [
      {
        name: 'tvjudgment',
        path: '/Judgment/tv',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '电视终审' },
      },
      {
        name: 'bcjudgment',
        path: '/Judgment/bc',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '广播终审' },
      },
      {
        name: 'paperjudgment',
        path: '/Judgment/paper',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '报纸终审' },
      },
      {
        name: 'journaljudgment',
        path: '/Judgment/journal',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '期刊终审' },
      },
    ]
  },
  {
    name: 'Verification',
    path: '/Verification/tv',
    component: layout,
    meta: { title: '广告核定', icon: 'iconfont icon-heding' },
    children: [
      {
        name: 'tvverification',
        path: '/Verification/tv',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '电视核定' },
      },
      {
        name: 'bcverification',
        path: '/Verification/bc',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '广播核定' },
      },
      {
        name: 'paperverification',
        path: '/Verification/paper',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '报纸核定' },
      },
      {
        name: 'journalverification',
        path: '/Verification/journal',
        component: () => import('@/views/MonitorRecheck'),
        meta: { title: '期刊核定' },
      },
    ]
  },
  {
    name: 'MonitorRecheck',
    path: '/MonitorRecheck',
    component: layout,
    meta: { show: false },
    children: [
      {
        name: 'tvbccheck',
        path: '/MonitorRecheck/tvbcCheck',
        component: () => import('@/views/MonitorRecheck/tvbcCheck'),
        meta: { title: '' },
      },
      {
        name: 'tvbccheckold',
        path: '/MonitorRecheck/tvbcCheckOld',
        component: () => import('@/views/MonitorRecheck/tvbcCheckOld'),
        meta: { title: '' },
      },
      {
        name: 'papercheck',
        path: '/MonitorRecheck/paperCheck',
        component: () => import('@/views/MonitorRecheck/paperCheck'),
        meta: { title: '' },
      },
    ]
  },
  {
    name: 'Inquire',
    path: '/Inquire/tv',
    component: layout,
    meta: { title: '广告查询', icon: 'iconfont icon-shenhe' },
    children: [
      {
        name: 'tvinquire',
        path: '/Inquire/tv',
        component: () => import('@/views/Inquire'),
        meta: { title: '电视明细' },
      },
      {
        name: 'bcinquire',
        path: '/Inquire/bc',
        component: () => import('@/views/Inquire'),
        meta: { title: '广播明细' },
      },
      {
        name: 'paperinquire',
        path: '/Inquire/paper',
        component: () => import('@/views/Inquire'),
        meta: { title: '报纸明细' },
      },
      {
        name: 'journalinquire',
        path: '/Inquire/journal',
        component: () => import('@/views/Inquire'),
        meta: { title: '期刊明细' },
      },
    ]
  },
  {
    name: 'report',
    path: '/playList',
    component: layout,
    meta: { title: '商业报表', icon: 'iconfont icon-shenhe' },
    children: [
      {
        name: 'playList',
        path: '/playList',
        component: () => import('@/views/PlayList/playList'),
        meta: { title: '串播单', icon: 'iconfont icon-liebiao'}
      },
      {
        name: 'brandList',
        path: '/brandList',
        component: () => import('@/views/BrandList/brandList'),
        meta: { title: '品牌监播单', icon: 'iconfont icon-plus-listview'}
      },
      {
        name: 'reportManage',
        path: '/reportManage',
        component: () => import('@/views/reportManage/index'),
        meta: { title: '报表管理', icon: 'iconfont icon-et-the-sample-of-recipients'}
      },
    ]
  },
  {
    name: 'Statistics',
    path: '/Statistics/tv',
    component: layout,
    meta: { title: '广告统计', icon: 'iconfont icon-tongji1' },
    children: [
      {
        name: 'allstatistics',
        path: '/Statistics/all',
        component: () => import('@/views/adStatistics'),
        meta: { title: '综合分析' },
      },
      {
        name: 'tvstatistics',
        path: '/Statistics/tv',
        component: () => import('@/views/adStatistics'),
        meta: { title: '电视分析' },
      },
      {
        name: 'bcstatistics',
        path: '/Statistics/bc',
        component: () => import('@/views/adStatistics'),
        meta: { title: '广播分析' },
      },
      {
        name: 'paperstatistics',
        path: '/Statistics/paper',
        component: () => import('@/views/adStatistics'),
        meta: { title: '报纸分析' },
      },
      {
        name: 'journalstatistics',
        path: '/Statistics/journal',
        component: () => import('@/views/adStatistics'),
        meta: { title: '期刊分析' },
      },
    ]
  },
  {
    name: 'Credit',
    path: '/mediaCredit',
    component: layout,
    meta: { title: '信用管理', icon: 'iconfont icon-xinyong1' },
    children: [
      {
        name: 'MediaCredit',
        path: '/mediaCredit',
        component: () => import('@/views/Credit/mediaCredit'),
        meta: { title: '媒体信用' }
      },
      {
        name: 'GroupCredit',
        path: '/groupCredit',
        component: () => import('@/views/Credit/groupCredit'),
        meta: { title: '集团信用' }
      },
    ]
  },
  {
    name: 'Task',
    path: '/task',
    component: layout,
    meta: { title: '任务管理', icon: 'iconfont icon-renwu' },
    children: [
    ]
  },
  {
    name: 'Sample',
    path: '/sample',
    component: layout,
    meta: { title: '样本管理', icon: 'iconfont icon-xingzhuang' },
    children: [
    ]
  },
  {
    name: 'System',
    path: '/system',
    component: layout,
    meta: { title: '系统管理', icon: 'iconfont icon-xingzhuang' },
    children: [
      {
        name: 'bulletin',
        path: '/bulletin',
        component: () => import('@/views/Bulletin'),
        meta: { title: '公告管理', icon: 'iconfont icon-gonggao' }
      },
      {
        name: 'addbulletin',
        path: '/addbulletin',
        component: () => import('@/views/Bulletin/bulletinCon/addBulletin'),
        meta: { title: '', icon: 'iconfont icon-gonggao', show: false }
      },
      {
        name: 'bulletinCon',
        path: '/bulletinCon',
        component: () => import('@/views/Bulletin/bulletinCon/bulletinCon'),
        meta: { title: '查看公告', icon: 'iconfont icon-gonggao', show: false }
      },
      {
        name: 'monitorReport',
        path: '/monitorReport',
        component: () => import('@/views/monitorReport'),
        meta: { title: '监测报告', icon: 'iconfont icon-gonggao' }
      },
      {
        name: 'addMonitoReport',
        path: '/addMonitoReport',
        component: () => import('@/views/monitorReport/components/addReport'),
        meta: { title: '', icon: 'iconfont icon-gonggao', show: false }
      },
      {
        name: 'monitoReportCon',
        path: '/monitoReportCon',
        component: () => import('@/views/monitorReport/components/detailsReport'),
        meta: { title: '查看报告', icon: 'iconfont icon-gonggao', show: false }
      },

      {
        name: 'sampleManage',
        path: '/sampleManage',
        component: () => import('@/views/Sample/sampleManage'),
        meta: { title: '样本管理', icon: 'iconfont icon-et-the-sample-of-recipients'}
      },

      {
        name: 'tvSample',
        path: '/tvSample',
        component: () => import('@/views/Sample/tvbcSample'),
        meta: { title: '电视样本信息', icon: 'iconfont icon-et-the-sample-of-recipients', show: false}
      },
      {
        name: 'bcSample',
        path: '/bcSample',
        component: () => import('@/views/Sample/tvbcSample'),
        meta: { title: '广播样本信息', icon: 'iconfont icon-et-the-sample-of-recipients', show: false}
      },
      // {
      //   name: 'paperSample',
      //   path: '/paperSample',
      //   component: () => import('@/views/Sample/paperSample'),
      //   meta: { title: '报纸样本信息', icon: 'iconfont icon-et-the-sample-of-recipients'}
      // },

      {
        name: 'adManage',
        path: '/adManage',
        component: () => import('@/views/adManage'),
        meta: { title: '广告管理', icon: 'iconfont icon-et-the-sample-of-recipients'}
      },

      {
        name: 'taskManage',
        path: '/taskManage',
        component: () => import('@/views/Task/taskManage'),
        meta: { title: '任务管理', icon: 'iconfont icon-renwu'}
      },
      {
        name: 'specification',
        path: '/specification',
        component: () => import('@/views/System/specification'),
        meta: { title: '报纸规格', icon: 'iconfont icon-et-the-sample-of-recipients'}
      },
      {
        name: 'permission',
        path: '/permission',
        component: () => import('@/views/System/permission'),
        meta: { title: '权限管理', icon: 'iconfont icon-et-the-sample-of-recipients'}
      },
      {
        name: 'mediaManage',
        path: '/mediaManage',
        component: () => import('@/views/mediaManage'),
        meta: { title: '媒体管理', icon: 'iconfont icon-renwu'}
      },
      {
        name: 'receiveFilter',
        path: '/receiveFilter',
        component: () => import('@/views/System/receiveFilter'),
        meta: { title: '接收过滤', icon: 'iconfont icon-renwu'}
      },
    ]
  },
]

export const constantRoutes = [
  {
    name: 'login',
    path: '/login',
    component: () => import('@/views/Login'),
  },
  {
    path: '/',
    redirect: '/login',
    component: () => import('@/views/Login'),
  },
]

// store.commit('setMenu', asyncRoutes)


const router = new Router({
  routes: constantRoutes
})

export default router

/**
 * @param {Array} a 原始数组
 * @param {String} pid 上级id 字段名
 * @param {String} id 本级 id 字段名
 * @param {String} str 根节点字段名或索引
 * 调用方法 new Tree(a,pid,id).init(str)
 */
function Tree(a, pid, id) {
  this._pid = pid
  this._id = id
  this.tree = a || []
  this.groups = {}
}
Tree.prototype = {
  init: function(str) {
    this.group()
    return this.getDom(this.groups[str])
  },
  group: function() {
    for (var i = 0; i < this.tree.length; i++) {
      if (this.groups[this.tree[i][this._pid]]) {
        this.groups[this.tree[i][this._pid]].push(this.tree[i])
      } else {
        this.groups[this.tree[i][this._pid]] = []
        this.groups[this.tree[i][this._pid]].push(this.tree[i])
      }
    }
  },
  getDom: function(a) {
    if (!a) {
      return ''
    }
    a.map((item, index) => {
      const code = item[this._id]
      if (this.groups[code]) {
        item.children = this.groups[code]
      }
      this.getDom(item.children)
      return item
    })
    return a
  }
}
export default Tree

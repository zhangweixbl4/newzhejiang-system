import axios from 'axios'
// import { getToken } from './cookie.js'
import { removeToken } from './cookie.js'
import Notify from '@/utils/notify.js'
import router from '@/router'

/*******************************************/
// 创建axios实例
const service = axios.create({
	baseURL: process.env.NODE_ENV === 'development' ? "/dev" : "", // api 的 base_url
	timeout: 600000, // 请求超时时间
	// headers: {
	// 	'Content-Type': 'application/x-www-form-urlencoded',
	// }
})
// request拦截器
service.interceptors.request.use(config => {
	return config
}, error => {
	//	debugger
	// Do something with request error
	Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
	response => {
		const res = response.data
		if (+res.code === 401) {
			Notify.error('登录超时请重新登录')
			removeToken()
			router.push({name: 'login'})
			return Promise.reject(res.message)
		}
		if (res.code === 0) {
			return Promise.resolve(res)
		} else {
			Notify.error(res.msg)
			return Promise.reject(res.msg)
		}
	},
	error => {
		return Promise.reject(error)
	}
)
export default service

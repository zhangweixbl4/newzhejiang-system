export function encrypt(str) {
  const publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQQhPZehS7ZC8loDOdW0zzzBtpBuVQPtM381MlxzrWnFCyihDvWVdDVCakPqx5R2lf5W25R5QTTLvJaTOiLqH+lT51LK+YGg4SNj/FfsdyjHO1Ne85oeoNjf/rYdQR5uz0l+yIEl6qguCnQvTxb65jLoazmkEGXcIGxI2R19KbGwIDAQAB"
  // eslint-disable-next-line no-undef
  let encrypt = new JSEncrypt()
  encrypt.setPublicKey(publicKey)
  return encrypt.encryptLong(str)
}
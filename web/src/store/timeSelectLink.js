import dayjs from "dayjs"

function setYear() {
  let now = new Date()
  let nowyear = now.getFullYear()
  const yearArr = []
  for (let index = 0; index <= (nowyear -2019); index++) {
    let obj = {}
    obj.label = `${index + 2019} 年`
    obj.value = String(index + 2019)
    yearArr.unshift(obj)
  }
  return yearArr
}

function setWeeks() {
  let weeks = []
  for (let i = 1; i < 54; i++) {
    let obj = {}
    obj.label = `第${i}周`
    obj.value = i
    weeks.push(obj)
  }
  return weeks
}

function setHalfMounth() {
  let halfmonthArr = ['月下半月', '月上半月']
  for (let i = 1; i < 25; i++) {
    let obj = {}
    obj.label = Math.ceil(i / 2) + halfmonthArr[i % 2]
    obj.value = i
    halfmonthArr.push(obj)
  }
  halfmonthArr.splice(0,2)
  return halfmonthArr
}

function setMonth() {
  let monthArr = []
  for (let i = 1; i < 13; i++) {
    let obj = {}
    obj.label = `${i}月`
    obj.value = i
    monthArr.push(obj)
  }
  return monthArr
}

function setSeason() {
  const season = []
  for (let i = 1; i < 5; i++) {
    let obj = {}
    obj.label = `第${i}季度`
    obj.value = i
    season.push(obj)
  }
  return season
}

const timeSelectLink = {
  'timetypes': {
    0: '天',
    10: '周',
    20: '半月',
    30: '月',
    40: '季度',
    50: '半年',
    60: '年',
  },
  'years': setYear(),
  'timeval': {
    '0': [dayjs().format('YYYY-MM-DD'),dayjs().format('YYYY-MM-DD')],
    '10': setWeeks(),
    '20': setHalfMounth(),
    '30': setMonth(),
    '40': setSeason(),
    '50': [ { label: '上半年', value: 1}, { label: '下半年', value: 2} ],
    '60': [{ label: '全年',value: 1 }],
  }
}

export default timeSelectLink
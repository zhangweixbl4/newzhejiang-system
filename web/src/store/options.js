export default  {
  routerMap: {
    'tvmonitor': '01',
    'bcmonitor': '02',
    'papermonitor': '03',
    'journalmonitor': '04',
    'tvrecheck': '01',
    'bcrecheck': '02',
    'paperrecheck': '03',
    'journalrecheck': '04',
    'tvstatistics': '01',
    'bcstatistics': '02',
    'paperstatistics': '03',
    'journalstatistics': '04',
    'tvinquire': '01',
    'bcinquire': '02',
    'paperinquire': '03',
    'journalinquire': '04',
    'tvdetach': '01',
    'bcdetach': '02',
    'paperdetach': '03',
    'journaldetach': '04',
    'tvjudgment': '01',
    'bcjudgment': '02',
    'paperjudgment': '03',
    'journaljudgment': '04',
    'tvSample': '01',
    'bcSample': '02',
    'paperSample': '03',
    'journalSample': '04',
    'tvverification': '01',
    'bcverification': '02',
    'paperverification': '03',
    'journalverification': '04',
  },
  isLongAd: {
    '0': {
      label: '否',
    },
    '1': {
      label: '是',
    },
  },
  noticeType: {
    '1': {
      label: '安全',
    },
    '2': {
      label: '通知',
    },
    '3': {
      label: '升级',
    },
    '4': {
      label: '监测报告',
    },
  },
  adType: {
    '1': {
      label: '广告',
      _label: '否'
    },
    '2': {
      label: '节目',
      _label: '是'
    },
  },
  dataType: {
    '1': {
      label: '全部发布广告'
    },
    '2': {
      label: '播出广告'
    },
    '3': {
      label: '新广告及违法广告'
    },
  },
  paperDataType: {
    '1': {
      label: '全部发布广告'
    },
    '3': {
      label: '违法广告'
    },
  },
  taskType: {
    'ggjc': {
      label: '广告监测'
    },
    'ggfs': {
      label: '广告复审'
    },
    'bzfl': {
      label: '报纸分离'
    },
  },
  mediaType: {
    '01': {
      label: '电视'
    },
    '02': {
      label: '广播'
    },
    '03': {
      label: '报纸'
    },
    '04': {
      label: '期刊'
    },
  },
  mediaType2: {
    '01': {
      label: '电视'
    },
    '02': {
      label: '广播'
    },
  },
  creditMedia: {
    '1' : {
      label: '信用评价媒体'
    },
    '2' : {
      label: '非信用评价媒体'
    },
  },
  illType: {
    '-30': {
      label: '待核实',
      color: 'yellow',
    },
    '0': {
      label: '不违法',
      color: 'green',
    },
    '30': {
      label: '违法',
      color: 'red',
    },
  },
  bulletinType: {
    '1': {
      label: '安全',
      type: 'success'
    },
    '2': {
      label: '通知',
      type: 'primary'
    },
    '3': {
      label: '升级',
      type: 'warning'
    },
    '4': {
      label: '监测报告',
      type: 'info'
    },
  },
  mReportType: {
    '11': {
      label: '日报',
      type: 'primary'
    },
    '12': {
      label: '周报',
      type: 'primary'
    },
    '13': {
      label: '半月报',
      type: 'primary'
    },
    '14': {
      label: '月报',
      type: 'primary'
    },
    '15': {
      label: '专项报告',
      type: 'primary'
    },
    '16': {
      label: '季报',
      type: 'primary'
    },
    '17': {
      label: '半年报',
      type: 'primary'
    },
    '18': {
      label: '年报',
      type: 'primary'
    },
    '19': {
      label: '预警报告',
      type: 'primary'
    },
    '20': {
      label: '其他',
      type: 'primary'
    },
  },
  mLookType: {
    '0': {
      label: '全部用户',
      type: 'danger'
    },
    '1': {
      label: '市监用户',
      type: 'primary'
    },
    '3': {
      label: '媒体用户',
      type: 'success'
    },
  },
  reportType: {
    '1': {
      label: '报纸广告投放明细（费用）'
    },
    '2': {
      label: '报纸广告投放明细（面积）'
    },
    '3': {
      label: '报纸广告投放明细（条次）'
    },
    '4': {
      label: '报纸品牌构成',
    },
    '5': {
      label: '报纸监测媒体广告市场情况',
    },
    '6': {
      label: '报纸广告收入周时分布',
    },
    '7': {
      label: '报纸广告规格分布（收入）',
    },
    '8': {
      label: '报纸广告规格分布（次数）',
    },
    '9': {
      label: '报纸广告规格分布（面积）',
    },
    '10': {
      label: '报纸广告套色分布（收入）',
    },
    '11': {
      label: '报纸广告套色分布（次数）',
    },
    '12': {
      label: '报纸广告套色分布（面积）',
    },
    '13': {
      label: '报纸广告行业分布',
    },
    '101': {
      label: '电视广告播出记录单',
    },
  },
  adClassOptions: {
    value: 'fcode',
    label: 'fadclass',
    children: 'children'
  },
  adClassOptions2: {
    value: 'fcode',
    label: 'fadclass',
    children: 'children',
    emitPath: false,
  },
  areaOptions: {
    value: 'fid',
    label: 'fname',
    children: 'children',
    checkStrictly: true,
  },

  mediaBelong: {
    '0': {
      label: '串播单'
    },
    '1': {
      label: '非串播单'
    },
    '2': {
      label: '省级'
    },
    '3': {
      label: '市级'
    },
    '4': {
      label: '区县级'
    },
  },
  taskStatus: {
    '-1': {
      label: '全部'
    },
    '0': {
      label: '待接收'
    },
    '1': {
      label: '已接收'
    },
    '10': {
      label: '已退回'
    },
    '3': {
      label: '已关闭'
    },
  },
  taskManageStatus: {
    '0': {
      label: '待处理',
      class: 'archive0'
    },
    '1': {
      label: '处理完成',
      class: 'archive1'
    },
    '4': {
      label: '任务异常',
      class: 'archive4'
    },
    '10': {
      label: '已退回',
      class: 'archive10'
    },
    'null': {
      label: '无任务',
      class: 'archivenull'
    }
  },
  linkType: {
    '0': {
      label: '广告分离',
      value: 'ggfl',
    },
    '1': {
      label: '广告监测',
      value: 'ggjc',
    },
    '2': {
      label: '广告复审',
      value: 'ggfs',
    },
    '3': {
      label: '广告终审',
      value: 'ggzs',
    },
    '4': {
      label: '归档',
      value: 'gggd',
    },
  },
  flowType: {
    'ggfl': {
      label: '广告分离',
    },
    'ggjc': {
      label: '广告监测',
    },
    'ggfs': {
      label: '广告复审',
    },
    'ggzs': {
      label: '广告终审',
    },
    'gghd': {
      label: '已归档',
    },
  },
  zhejiang: {
    '20': {
       '0': '各地区',
       '1': '省级',
       '4': '市级',
       '5': '区县级',
     },
     '10': {
       '0': '各地区',
       '4': '市级',
       '5': '区县级',
     },
     '0': {
       '5': '区县级',
     }
   },
  onther_class: {
    "1": "条数排名",
    "2": "违法条数排名",
    "3": "条数违法率排名",
    "11": "条次排名",
    "12": "违法条次排名",
    "13": "条次违法率排名",
    "21": "时长排名",
    "22": "违法时长排名",
    "23": "时长违法率排名",
    // "comprehensive_score":"综合得分"
  },
  pickerOptions: {
    disabledDate(time) {
      return time.getTime() > Date.now();
    }
  },
}
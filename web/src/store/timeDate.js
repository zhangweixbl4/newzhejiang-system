export const oldTimeDate = [
  {
    starttime: '00:00:00',
    endtime: '23:59:59',
    start: '0',
    end: '24时段'
  },
  {
    starttime: '00:00:00',
    endtime: '03:59:59',
    start: '0',
    end: ' 4 时段'
  },
  {
    starttime: '04:00:00',
    endtime: '07:59:59',
    start: '4',
    end: ' 8 时段'
  },
  {
    starttime: '08:00:00',
    endtime: '09:59:59',
    start: '8',
    end: '10时段'
  },
  {
    starttime: '10:00:00',
    endtime: '11:59:59',
    start: '10',
    end: '12时段'
  },
  {
    starttime: '12:00:00',
    endtime: '13:59:59',
    start: '12',
    end: '14时段'
  },
  {
    starttime: '14:00:00',
    endtime: '15:59:59',
    start: '14',
    end: '16时段'
  },
  {
    starttime: '16:00:00',
    endtime: '17:59:59',
    start: '16',
    end: '18时段'
  },
  {
    starttime: '18:00:00',
    endtime: '18:59:59',
    start: '18',
    end: '19时段'
  },
  {
    starttime: '19:00:00',
    endtime: '19:59:59',
    start: '19',
    end: '20时段'
  },
  {
    starttime: '20:00:00',
    endtime: '21:59:59',
    start: '20',
    end: '22时段'
  },
  {
    starttime: '22:00:00',
    endtime: '23:59:59',
    start: '22',
    end: '24时段'
  },
]
export const timeDate = [
  {
    starttime: '00:00:00',
    endtime: '04:59:59',
    start: '0',
    end: ' 5 时段'
  },
  {
    starttime: '04:00:00',
    endtime: '8:59:59',
    start: '4',
    end: ' 9 时段'
  },
  {
    starttime: '8:00:00',
    endtime: '12:59:59',
    start: '8',
    end: '13时段'
  },
  {
    starttime: '12:00:00',
    endtime: '16:59:59',
    start: '12',
    end: '17时段'
  },
  {
    starttime: '16:00:00',
    endtime: '20:59:59',
    start: '16',
    end: '21时段'
  },
  {
    starttime: '20:00:00',
    endtime: '23:59:59',
    start: '20',
    end: '24时段'
  },
]

export default timeDate
import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import { getAllAdClass, getTillegal, getAllProgClass } from '@/api/Function.js'
import { getMediatregion } from "@/api/MediaIssue.js"
import options from './options'

import { asyncRoutes, constantRoutes } from '@/router'


import { userView } from '@/api/Login.js'
import Tree from '@/utils/Tree.js'
import timeSelectLink from './timeSelectLink'

Vue.use(Vuex)

/**
 * 使用 meta.role 确定当前用户是否具有权限
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.name && !route.children) {
    // 根据 route.meta.roles 来判断该用户是否拥有路由权限
    if ((route.name === 'addbulletin' || route.name === 'bulletinCon') && roles.includes('bulletin')) {
      return true
    }

    if ((route.name === 'tvSample' || route.name === 'bcSample') && roles.includes('sampleManage')) {
      return true
    }

    if (route.name === 'tvbccheck' || route.name ===  'papercheck' || route.name === 'tvbccheckold') {
      return true
    }

    if ((route.name === 'addMonitoReport' || route.name === 'monitoReportCon') && roles.includes('monitorReport')) {
      return true
    }

    return roles.includes(route.name)
  } else {
    // 默认返回 true
    return true
  }
}

/**
 * 通过递归过滤异步路由表
 * @param routes asyncRoutes
 * @param roles 权限数组
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    // 如果有权限
    if (hasPermission(roles, tmp)) {
      // 判断当前路由下是否还有子路由，如果有，则进行递归再次过滤
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })
  return res
}

export default new Vuex.Store({
  state: {
    routes: [],
    addRoutes: [],
    menus: [],
    adClassTree: [],
    mediaRegion: [],
    progClass: [],
    illegalTree: [],
    userInfo: {},
    sampleParam: {
      sampleAdName: '',
      fsampleid: '',
    },
    useSampleData: {},
    sampleDisabled: false,
    timeSelectLink: timeSelectLink,
    timeLineData: {
      m3u8: '',
      seekTime: 0,
      m3u8Start: 0,
      regions: [],
      index: 0,
      m3u8List: [],
      showSurfer: false
    },
    ...options,
    sloading: false,
    tempFlag: true, // 分离页面缓存flag
  },
  mutations: {
    SET_ROUTES: (state, routes) => {
      // 过滤出来的路由
      state.addRoutes = routes
      // 白名单路由 + 过滤出来的路由
      state.routes = constantRoutes.concat(routes)
      state.menus = routes
    },
    toggleTempFlag(state, val = true) {
      state.tempFlag = val
    },
    setMenu(state, menus) {
      state.menus = menus
    },
    setAdClassTree(state, adClassTree) {
      state.adClassTree = adClassTree
    },
    setMediaRegion(state, mediaRegion) {
      state.mediaRegion = mediaRegion
    },
    setProgClass(state, progClass) {
      state.progClass = progClass
    },
    setIllegalTree(state, illegalTree) {
      state.illegalTree = illegalTree
    },
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo
    },
    setFsampleid(state, fsampleid) {
      state.sampleParam.fsampleid = fsampleid
    },
    setSampleAdName(state, sampleAdName) {
      state.sampleParam.sampleAdName = sampleAdName
    },
    setUseSampleData(state, useSampleData) {
      state.useSampleData = useSampleData
    },
    setSampleDisabled(state, sampleDisabled) {
      state.sampleDisabled = sampleDisabled
    },
    setM3U8(state, m3u8) {
      state.timeLineData.m3u8 = m3u8
    },
    setSeekTime(state, time) {
      state.timeLineData.seekTime = time
    },
    setM3u8Start(state, time) {
      state.timeLineData.m3u8Start = time
    },
    setWaveRegions(state, regions) {
      state.timeLineData.regions = regions
    },
    setIndex(state, index) {
      state.timeLineData.index = index
    },
    setM3u8List(state, m3u8List) {
      state.timeLineData.m3u8List = m3u8List
    },
    setShowSurfer(state, show = true) {
      state.timeLineData.showSurfer = show
    },
    resetTimeLineData(state) {
      state.timeLineData = {
        m3u8: '',
        seekTime: 0,
        m3u8Start: 0,
        regions: [],
        index: 0,
        m3u8List: [],
        showSurfer: false
      }
    },
    sloading(state, val) {
      state.sloading = val
    },
  },
  actions: {
    generateRoutes({ commit }, roles) {
      return new Promise(resolve => {
        // 开发环境加载所有路由
        let accessedRoutes = process.env.NODE_ENV === 'development' ? asyncRoutes : filterAsyncRoutes(asyncRoutes, roles)
        commit('SET_ROUTES', accessedRoutes)
        resolve(accessedRoutes)
      })
    },
    getUserView({ commit }) {
      return new Promise((revolve, reject) => {
        userView().then(res => {
          commit('setUserInfo', res.data)
          revolve(res.data.menujurisdictioncode)
        }).catch(() => reject())
      })
    },
    getAllProgClass({ commit }) {
      getAllProgClass().then(res => {
        commit('setProgClass', res.data)
      })
    },
    getAdClassTree({ commit }) {
      getAllAdClass().then(res => {
        let tree = new Tree(res.data, 'fpcode', 'fcode').init('')
        commit('setAdClassTree', tree)
      })
    },
    getMediaRegion({ commit }) {
      getMediatregion().then(res => {
        let mediatree = new Tree(res.data, 'fpid', 'fid').init(res.data[0].fpid)
        commit('setMediaRegion', mediatree)
      })
    },
    getTillegal({ commit }) {
      getTillegal().then(res => {
        commit('setIllegalTree', res.data.map(i => {
          i.disabled = true
          return i
        }))
      })
    },
  },
  getters,
})

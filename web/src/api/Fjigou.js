import fetch from '@/utils/fetch.js'

export function getJigoulist(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/get_jigoulist',
    data: data
  })
}

export function deleteJigou(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/delete_jigou',
    data: data
  })
}

export function editJigou(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/edit_jigou',
    data: data
  })
}

export function createJigou(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/create_jigou',
    data: data
  })
}

export function getJigoumenulist(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/get_jigoumenulist',
    data: data
  })
}

export function getJigoumedialist(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/get_jigoumedialist',
    data: data
  })
}

export function getRenyuanlist(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/get_renyuanlist',
    data: data
  })
}

export function deleteRenyuan(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/delete_renyuan',
    data: data
  })
}

export function editRenyuan(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/edit_renyuan',
    data: data
  })
}

export function createRenyuan(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Fjigou/create_renyuan',
    data: data
  })
}
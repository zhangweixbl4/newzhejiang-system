import fetch from '@/utils/fetch.js'

// 获取公告列表
export function noticeList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Notice/noticeList',
    data: data
  })
}

// 添加公告
export function noticeCreate(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Notice/noticeCreate',
    data: data
  })
}

// 修改公告
export function noticeSave(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Notice/noticeSave',
    data: data
  })
}

// 公告删除
export function noticeDel(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Notice/noticeDel',
    data: data
  })
}

// 公告详情
export function noticeView(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Notice/noticeView',
    data: data
  })
}
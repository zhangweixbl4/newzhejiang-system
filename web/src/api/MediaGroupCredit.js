import fetch from '@/utils/fetch.js'

// 信用列表，导出
export function groupCreditList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaGroupCredit/groupCreditList',
    data: data
  })
}

// 信用更新
export function updateGroupCredit(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaGroupCredit/updateGroupCredit',
    data: data
  })
}

// 信用媒体列表
export function groupMediaCreditList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaGroupCredit/groupMediaCreditList',
    data: data
  })
}

import fetch from '@/utils/fetch.js'

// 地域列表
export function getMediatregion(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Core/getRegionList',
    data: data
  })
}

// 广告监测任务列表
export function mediaIssueList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/mediaIssueList',
    data: data
  })
}

// 任务的发布数据列表
export function issueDataList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/issueDataList',
    data: data
  })
}

// 任务列表
export function mediaIssueTask(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/mediaIssueTask',
    data: data
  })
}

// 任务退回
export function mediaIssueTaskAction(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/mediaIssueTaskAction',
    data: data
  })
}

// 任务完成提交
export function issueDataSubmit(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/issueDataSubmit',
    data: data
  })
}

// 任务数据保存
export function issueDataSave(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/issueDataSave',
    data: data
  })
}

// 添加
export function issueDataAdd(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/issueDataAdd',
    data: data
  })
}

// 删除
export function issueDataDel(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/issueDataDel',
    data: data
  })
}

// 任务数据软删除
export function issueDataSampleDel(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/issueDataSampleDel',
    data: data
  })
}

// 获取媒体任务（移动端）
export function getMediaIssue(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/getMediaIssue',
    data: data
  })
}

// 接收报纸分离任务（移动端）
export function recePaperMediaIssue(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/recePaperMediaIssue',
    data: data
  })
}

// 报纸版面确认
export function issuePageConfirm(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/issuePageConfirm',
    data: data
  })
}

// 报纸版面确认
export function mediaIssueTaskBackAction(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/mediaIssueTaskBackAction',
    data: data
  })
}

// 广告分离模块时间偏移接口
export function mediaIssueTimeEdit(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/mediaIssueTimeEdit',
    data: data
  })
}

// 操作信息接口
export function sampleillegalLog(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaIssue/sampleillegalLog',
    data: data
  })
}


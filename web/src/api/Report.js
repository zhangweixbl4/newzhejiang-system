import fetch from '@/utils/fetch.js'

// 获取公告列表
export function reportList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Report/reportList',
    data: data
  })
}

// 添加公告
export function reportCreate(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Report/reportCreate ',
    data: data
  })
}

// 修改公告
export function reportSave(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Report/reportSave',
    data: data
  })
}

// 公告删除
export function reportDel(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Report/reportDel',
    data: data
  })
}

// 公告详情
export function reportView(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Report/reportView',
    data: data
  })
}
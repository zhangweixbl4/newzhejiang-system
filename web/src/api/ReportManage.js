import fetch from '@/utils/fetch.js'

// 报表列表
export function reportList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/ReportManage/reportList',
    data: data
  })
}
// 报表生成
export function bZShiChangReport(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/ReportManage/bZShiChangReport',
    data: data
  })
}
// 报表删除
export function reportDel(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/ReportManage/reportDel',
    data: data
  })
}

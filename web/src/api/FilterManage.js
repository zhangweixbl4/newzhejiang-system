import fetch from '@/utils/fetch.js'

// 接收过滤列表
export function filterList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/FilterManage/filterList',
    data: data
  })
}

// 添加
export function filterCreate(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/FilterManage/filterCreate',
    data: data
  })
}

// 删除
export function filterDelete(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/FilterManage/filterDelete',
    data: data
  })
}


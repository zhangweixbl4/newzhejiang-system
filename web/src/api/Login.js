import fetch from '@/utils/fetch.js'

// 登录
export function ajaxLogin(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Login/ajaxLogin',
    data: data
  })
}

// 获取用户信息
export function userView(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Login/userView',
    data: data
  })
}

// 退出登录
export function outLogin(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Login/outLogin',
    data: data
  })
}

import fetch from '@/utils/fetch.js'

// 广告监测明细列表
export function issueList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/IssueMonitoring/issueList',
    data: data
  })
}

// 监测明细详情
export function getIssueView(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/IssueMonitoring/getIssueView',
    data: data
  })
}
import fetch from '@/utils/fetch.js'

// 获取任务列表
export function taskList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/TaskManage/taskList',
    data: data
  })
}

// 任务操作
export function taskAction(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/TaskManage/taskAction',
    data: data
  })
}

// 生成随机任务清单
export function randOneKeyDay(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/TaskManage/randOneKeyDay',
    data: data
  })
}

// 创建任务
export function oneKeyTask(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/TaskManage/oneKeyTask',
    data: data
  })
}
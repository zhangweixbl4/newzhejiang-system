import fetch from '@/utils/fetch.js'

// 获取传播单列表
export function paperSizeList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PaperInput/paperSizeList',
    data: data
  })
}
// 报纸版面提交
export function addPaperSource(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PaperInput/addPaperSource',
    data: data
  })
}

// 添加规格
export function addPaperSize(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PaperInput/addPaperSize',
    data: data
  })
}

// 编辑规格
export function editPaperSize(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PaperInput/editPaperSize',
    data: data
  })
}

// 删除规格
export function delPaperSize(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PaperInput/delPaperSize',
    data: data
  })
}

// 规格类别
export function getPaperSize(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PaperInput/getPaperSize',
    data: data
  })
}

// 规格类别
export function delPaperSource(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PaperInput/delPaperSource',
    data: data
  })
}

import fetch from '@/utils/fetch.js'

// 广告核定模块素材列表
export function sourceMakeList(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Media/sourceMakeList',
    data: data
  })
}

// 生成素材
export function addSourceMake(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Media/add_source_make',
    data: data
  })
}
// 素材生成状态
export function getSourceMakeState(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Media/get_source_make_state',
    data: data
  })
}


// 删除素材
export function sourceMakeDel(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Media/sourceMakeDel',
    data: data
  })
}


// 媒体列表
export function getMedialist(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Media/get_medialist',
    data: data
  })
}

// 媒体列表编辑
export function editMediaAction(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Media/editMediaAction',
    data: data
  })
}
import fetch from '@/utils/fetch.js'

export function adList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Ad/adList',
    data: data
  })
}

export function adAction(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/Ad/adAction',
    data: data
  })
}

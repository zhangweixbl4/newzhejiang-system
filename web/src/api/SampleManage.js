import fetch from '@/utils/fetch.js'

// 获取样本列表
export function sampleList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/SampleManage/sampleList',
    data: data
  })
}

// 获取样本记录
export function sampleMediaIssueList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/SampleManage/sampleMediaIssueList',
    data: data
  })
}

// 获取样本详情
export function sampleDetail(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/SampleManage/sampleDetail',
    data: data
  })
}

// 保存样本
export function sampleSave(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/SampleManage/sampleSave',
    data: data
  })
}

// 验证广告信息是否匹配
export function checkAdDetail(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/SampleManage/checkAdDetail',
    data: data
  })
}

// 样本参照列表
export function sampleLookList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/SampleManage/sampleLookList',
    data: data
  })
}

// 样本参照列表
export function sampleDetectResult(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/SampleManage/sampleDetectResult',
    data: data
  })
}
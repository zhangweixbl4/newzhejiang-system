import fetch from '@/utils/fetch.js'

// 信用列表，导出
export function mediaCreditList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaCredit/mediaCreditList',
    data: data
  })
}

// 信用更新
export function updateCredit(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/MediaCredit/updateCredit',
    data: data
  })
}

import fetch from '@/utils/fetch.js'

// 获取传播单列表
export function playList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PlayManage/playList',
    data: data
  })
}

// 获取监播单列表
export function ppPlayList(data = '') {
  return fetch({
    method: 'post',
    url: '/Jiance/PlayManage/ppPlayList',
    data: data
  })
}

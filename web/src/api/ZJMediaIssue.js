import fetch from '@/utils/fetch.js'

// 获取 mp3 地址
export function getMediaMp3(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/ZJMediaIssue/getMediaMp3',
    data: data
  })
}
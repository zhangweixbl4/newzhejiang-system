import fetch from '@/utils/fetch.js'

// 获取所有节目类别
export function getAllProgClass(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Function/getAllProgClass',
    data: data
  })
}

// 获取所有广告类别
export function getAllAdClass(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Function/getAllAdClass',
    data: data
  })
}

// 获取违法表现
export function getTillegal(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Function/getTillegal',
    data: data
  })
}

// 获取媒体列表
export function getMediaList(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Function/getMediaList',
    data: data
  })
}

// 获取断流时间
export function getInterruptFlow(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Function/getInterruptFlow',
    data: data
  })
}

// 获取七牛云上传参数
export function getFileUp(data = '') {
  return fetch({
    method: 'get',
    url: '/Api/Function/getFileUp',
    data: data
  })
}

// 获取广告名称模糊搜索
export function searchAdByName(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Ad/searchAdByName',
    data: data,

    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
  })
}

// 获取广告主模糊搜索
export function searchAdOwner(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Ad/searchAdOwner',
    data: data,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
  })
}

// 获取地域
export function getRegionList(data = '') {
  return fetch({
    method: 'post',
    url: '/Api/Function/getRegionList',
    data: data
  })
}
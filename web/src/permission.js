import router from './router'
import store from './store'



router.beforeEach(async(to, from, next) => {
  if (to.name === 'login') {
    next()
  } else {
    if (!store.state.mediaRegion.length) {
      store.dispatch('getMediaRegion')
    }
    if (!store.state.adClassTree.length) {
      store.dispatch('getAdClassTree')
    }
    if (!store.state.progClass.length) {
      store.dispatch('getAllProgClass')
    }
    if (!store.state.illegalTree.length) {
      store.dispatch('getTillegal')
    }
    if ( store.getters.userInfo.fcode ) {
      next()
    } else {
      try {
        const roles = await store.dispatch('getUserView')
        const accessRoutes = await store.dispatch('generateRoutes', roles)
        router.addRoutes(accessRoutes)
        next({ ...to, replace: true })
      } catch (error) {
        console.log(error)
      }
    }
  }
})
<?php

//$time = microtime();
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用入口文件
//file_put_contents('LOG/ff',$_SERVER["REQUEST_URI"].'	'.$time.'-'.microtime()."\n",FILE_APPEND);
//file_put_contents('LOG/ll','http://'.$_SERVER['SERVER_NAME'].$_SERVER["REQUEST_URI"].'	'.file_get_contents('php://input').'	'.date('Y-m-d H:i:s').'	'. 0 ."\n",FILE_APPEND);

/* if($_SERVER['HTTP_HOST'] == 'gjjggjg.hz-data.xyz' && time() < strtotime('2018-08-01')){
	header("Content-type: text/html; charset=utf-8");
	exit('<h5>平台升级中</h5>');
} */

if(
	strstr(urldecode($_SERVER["REQUEST_URI"]),'<script>') || 
	strstr(urldecode($_SERVER["REQUEST_URI"]),'</script>') || 
	strstr(urldecode($_SERVER["REQUEST_URI"]),'onmouseover')
){
	header('HTTP/1.1 404 Not Found');
    header("status: 404 Not Found");
	exit('404');
}

// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
if(in_array($_SERVER["SERVER_NAME"],['gjjggjg.hz-data.xyz','118.31.124.99'])){
	define('APP_DEBUG', false);
}else{
	define('APP_DEBUG', TRUE);
}

// 定义应用目录
define('APP_PATH','./Application/');

//绑定不存在的新模块,用于快速生成模块目录结构,第一次运行模块生成完后要立刻注释掉解除绑定
// define('BIND_MODULE','NetAdInput');

// 引入ThinkPHP入口文件
require './ThinkPHP/ThinkPHP.php';
//file_put_contents('LOG/ff',$_SERVER["REQUEST_URI"].'	'.$time.'-'.microtime()."\n",FILE_APPEND);
// 亲^_^ 后面不需要任何代码了 就是如此简单